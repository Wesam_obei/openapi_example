// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'serializers.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add($PaginatedDocumented.serializer)
      ..add($SuccessResponse.serializer)
      ..add(Action.serializer)
      ..add(ActivityLogControllerLastLogin200Response.serializer)
      ..add(ActivityLogControllerUserActivity200Response.serializer)
      ..add(AnswerRequest.serializer)
      ..add(AppErrorCodesEnum.serializer)
      ..add(AuthControllerGetProfile200Response.serializer)
      ..add(AuthControllerLogin200Response.serializer)
      ..add(AuthControllerLogout200Response.serializer)
      ..add(AuthControllerRefreshToken200Response.serializer)
      ..add(BaseDataRequest.serializer)
      ..add(CommentCreateRequest.serializer)
      ..add(CoreControllerUploadFile200Response.serializer)
      ..add(CoreEnum.serializer)
      ..add(EntitiesControllerGet200Response.serializer)
      ..add(EntitiesControllerList200Response.serializer)
      ..add(EntityCreateRequest.serializer)
      ..add(EntityUpdateRequest.serializer)
      ..add(Envelope.serializer)
      ..add(EquipmentCreateRequest.serializer)
      ..add(EquipmentStorePointCreateRequest.serializer)
      ..add(EquipmentStorePointUpdateRequest.serializer)
      ..add(EquipmentTypeCreateRequest.serializer)
      ..add(EquipmentTypeUpdateRequest.serializer)
      ..add(EquipmentUpdateRequest.serializer)
      ..add(EquipmentsControllerGet200Response.serializer)
      ..add(EquipmentsControllerList200Response.serializer)
      ..add(EquipmentsStorePointsControllerGet200Response.serializer)
      ..add(EquipmentsStorePointsControllerList200Response.serializer)
      ..add(EquipmentsTypesControllerGet200Response.serializer)
      ..add(EquipmentsTypesControllerList200Response.serializer)
      ..add(ErrorDetails.serializer)
      ..add(ErrorResponse.serializer)
      ..add(FormComponentsRequest.serializer)
      ..add(FormCreateRequest.serializer)
      ..add(FormDuplicateRequest.serializer)
      ..add(FormResponseDataTypeEnum.serializer)
      ..add(FormUpdateRequest.serializer)
      ..add(FormsControllerCreate200Response.serializer)
      ..add(FormsControllerGet200Response.serializer)
      ..add(FormsControllerList200Response.serializer)
      ..add(FormsControllerUpdate200Response.serializer)
      ..add(GetAuditLogEventDataResponse.serializer)
      ..add(GetAuditLogHeaderDataResponse.serializer)
      ..add(GetAuditLogPayloadDataResponse.serializer)
      ..add(GetAuditLogRequestDataResponse.serializer)
      ..add(GetEntityDataResponse.serializer)
      ..add(GetEquipmentDataResponse.serializer)
      ..add(GetEquipmentStorePointDataResponse.serializer)
      ..add(GetEquipmentTypeDataResponse.serializer)
      ..add(GetFileDataResponse.serializer)
      ..add(GetFormComponentDataResponse.serializer)
      ..add(GetFormComponentResponseDataType.serializer)
      ..add(GetFormDataResponse.serializer)
      ..add(GetNotificationResponseData.serializer)
      ..add(GetNotificationTypeResponseData.serializer)
      ..add(GetPlanDataResponse.serializer)
      ..add(GetPlanVersionDataResponse.serializer)
      ..add(GetRaqmyInfo.serializer)
      ..add(GetRoleDataResponse.serializer)
      ..add(GetSurveyDataResponse.serializer)
      ..add(GetSurveyRecipientViewDataResponse.serializer)
      ..add(GetSurveyResponseDataResponse.serializer)
      ..add(GetUserActivityLogDataResponse.serializer)
      ..add(GetUserDashboardDataResponse.serializer)
      ..add(GetUserDataResponse.serializer)
      ..add(HealthControllerCheck200Response.serializer)
      ..add(HealthDataResponse.serializer)
      ..add(IncidentReportsDataRequest.serializer)
      ..add(ListCommentReponseDto.serializer)
      ..add(ListEmployeeUserDataResponse.serializer)
      ..add(ListEntitiesDataResponse.serializer)
      ..add(ListEquipmentDataResponse.serializer)
      ..add(ListEquipmentStorePointDataResponse.serializer)
      ..add(ListEquipmentTypeDataResponse.serializer)
      ..add(ListFormDataResponse.serializer)
      ..add(ListPermissionCategoryDataResponse.serializer)
      ..add(ListPlanDataResponse.serializer)
      ..add(ListRoleDataResponse.serializer)
      ..add(ListSurveyDataResponse.serializer)
      ..add(ListSurveyResponseDataResponse.serializer)
      ..add(ListUserActivityLogDataResponse.serializer)
      ..add(ListUserDashboardDataResponse.serializer)
      ..add(ListUserDataResponse.serializer)
      ..add(ListUserNotificationsResponseDataResponse.serializer)
      ..add(LocationData.serializer)
      ..add(LoginRequest.serializer)
      ..add(LoginResponse.serializer)
      ..add(LogoutRequest.serializer)
      ..add(MapLayersControllerGetBridgesTunnelsSitesMap200Response.serializer)
      ..add(MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData
          .serializer)
      ..add(
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner
              .serializer)
      ..add(
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties
              .serializer)
      ..add(
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .serializer)
      ..add(
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum
              .serializer)
      ..add(MapLayersControllerGetCablesMap200Response.serializer)
      ..add(MapLayersControllerGetCablesMap200ResponseAllOfData.serializer)
      ..add(MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner
          .serializer)
      ..add(
          MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerProperties
              .serializer)
      ..add(
          MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .serializer)
      ..add(MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum
          .serializer)
      ..add(MapLayersControllerGetConstructionLicensesMap200Response.serializer)
      ..add(MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData
          .serializer)
      ..add(
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner
              .serializer)
      ..add(
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties
              .serializer)
      ..add(
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .serializer)
      ..add(
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum
              .serializer)
      ..add(MapLayersControllerGetDistrictBoundariesMap200Response.serializer)
      ..add(MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData
          .serializer)
      ..add(
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner
              .serializer)
      ..add(
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
              .serializer)
      ..add(
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .serializer)
      ..add(
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum
              .serializer)
      ..add(MapLayersControllerGetElectricalFacilitiesMap200Response.serializer)
      ..add(MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData
          .serializer)
      ..add(
          MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInner
              .serializer)
      ..add(
          MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerProperties
              .serializer)
      ..add(
          MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .serializer)
      ..add(
          MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum
              .serializer)
      ..add(MapLayersControllerGetIncidentReportsMap200Response.serializer)
      ..add(MapLayersControllerGetIncidentReportsMap200ResponseAllOfData
          .serializer)
      ..add(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInner
              .serializer)
      ..add(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties
              .serializer)
      ..add(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId
              .serializer)
      ..add(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .serializer)
      ..add(MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum
          .serializer)
      ..add(MapLayersControllerGetLandBaseParcelMap200Response.serializer)
      ..add(MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData
          .serializer)
      ..add(
          MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner
              .serializer)
      ..add(
          MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties
              .serializer)
      ..add(
          MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .serializer)
      ..add(MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum
          .serializer)
      ..add(MapLayersControllerGetLightPolesMap200Response.serializer)
      ..add(MapLayersControllerGetLightPolesMap200ResponseAllOfData.serializer)
      ..add(MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner
          .serializer)
      ..add(
          MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerProperties
              .serializer)
      ..add(
          MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .serializer)
      ..add(MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum
          .serializer)
      ..add(MapLayersControllerGetManholeMap200Response.serializer)
      ..add(MapLayersControllerGetManholeMap200ResponseAllOfData.serializer)
      ..add(MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner
          .serializer)
      ..add(
          MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties
              .serializer)
      ..add(
          MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .serializer)
      ..add(MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum
          .serializer)
      ..add(MapLayersControllerGetMunicipalitiesMap200Response.serializer)
      ..add(MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData
          .serializer)
      ..add(
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner
              .serializer)
      ..add(
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerProperties
              .serializer)
      ..add(
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .serializer)
      ..add(MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum
          .serializer)
      ..add(MapLayersControllerGetPlanDataMap200Response.serializer)
      ..add(MapLayersControllerGetPlanDataMap200ResponseAllOfData.serializer)
      ..add(MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner
          .serializer)
      ..add(
          MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties
              .serializer)
      ..add(
          MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .serializer)
      ..add(MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum
          .serializer)
      ..add(MapLayersControllerGetSeaOutMap200Response.serializer)
      ..add(MapLayersControllerGetSeaOutMap200ResponseAllOfData.serializer)
      ..add(MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInner
          .serializer)
      ..add(
          MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerProperties
              .serializer)
      ..add(
          MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .serializer)
      ..add(MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum
          .serializer)
      ..add(MapLayersControllerGetStorageLocationsMap200Response.serializer)
      ..add(MapLayersControllerGetStorageLocationsMap200ResponseAllOfData
          .serializer)
      ..add(
          MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner
              .serializer)
      ..add(
          MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .serializer)
      ..add(
          MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum
              .serializer)
      ..add(MapLayersControllerGetStreetNamesMap200Response.serializer)
      ..add(MapLayersControllerGetStreetNamesMap200ResponseAllOfData.serializer)
      ..add(
          MapLayersControllerGetStreetNamesMap200ResponseAllOfDataFeaturesInner
              .serializer)
      ..add(
          MapLayersControllerGetStreetNamesMap200ResponseAllOfDataFeaturesInnerProperties
              .serializer)
      ..add(
          MapLayersControllerGetStreetNamesMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .serializer)
      ..add(MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum
          .serializer)
      ..add(MapLayersControllerGetTransportTracksMap200Response.serializer)
      ..add(MapLayersControllerGetTransportTracksMap200ResponseAllOfData
          .serializer)
      ..add(
          MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner
              .serializer)
      ..add(
          MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerProperties
              .serializer)
      ..add(
          MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .serializer)
      ..add(MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum
          .serializer)
      ..add(MapLayersControllerGetUrbanAreaBoundariesMap200Response.serializer)
      ..add(MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData
          .serializer)
      ..add(
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner
              .serializer)
      ..add(
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
              .serializer)
      ..add(
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .serializer)
      ..add(
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum
              .serializer)
      ..add(NotificationsControllerCountUnread200Response.serializer)
      ..add(NotificationsControllerMyNotifications200Response.serializer)
      ..add(PaginatedLinksDocumented.serializer)
      ..add(PaginatedMetaDocumented.serializer)
      ..add(PermissionDataResponse.serializer)
      ..add(PermissionsCategoriesControllerList200Response.serializer)
      ..add(PermissionsEnum.serializer)
      ..add(PlanCreateRequest.serializer)
      ..add(PlanUpdateRequest.serializer)
      ..add(PlanVersionCreateRequest.serializer)
      ..add(PlansControllerGet200Response.serializer)
      ..add(PlansControllerList200Response.serializer)
      ..add(PlansVersionsControllerGet200Response.serializer)
      ..add(ProfileResponse.serializer)
      ..add(RecipientResponseRequest.serializer)
      ..add(RecipientResponseRequestStatusEnum.serializer)
      ..add(RecipientType.serializer)
      ..add(RefreshTokenRequest.serializer)
      ..add(RefreshTokenResponse.serializer)
      ..add(RoleCreateRequest.serializer)
      ..add(RoleUpdateRequest.serializer)
      ..add(RolesControllerGet200Response.serializer)
      ..add(RolesControllerList200Response.serializer)
      ..add(SortingByColumnsInnerInner.serializer)
      ..add(StatusResponse.serializer)
      ..add(StatusResponseNameEnum.serializer)
      ..add(SubscribeRequest.serializer)
      ..add(SurveyCommentsControllerList200Response.serializer)
      ..add(SurveyCreateRequest.serializer)
      ..add(SurveyRequestDataResponse.serializer)
      ..add(SurveyResponseAnswerDataResponse.serializer)
      ..add(SurveyResponsesControllerList200Response.serializer)
      ..add(SurveyResponsesControllerResponseDetails200Response.serializer)
      ..add(SurveyUpdateRequest.serializer)
      ..add(SurveysControllerGet200Response.serializer)
      ..add(SurveysControllerList200Response.serializer)
      ..add(SurveysControllerRequests200Response.serializer)
      ..add(SurveysControllerRequestsDetails200Response.serializer)
      ..add(UnsubscribeRequest.serializer)
      ..add(UploadFileResponse.serializer)
      ..add(UserCreateRequest.serializer)
      ..add(UserDashboardCreateRequest.serializer)
      ..add(UserDashboardUpdateRequest.serializer)
      ..add(UserDashboardsControllerCreate200Response.serializer)
      ..add(UserDashboardsControllerGetDefaultDashboard200Response.serializer)
      ..add(UserDashboardsControllerList200Response.serializer)
      ..add(UserUpdateRequest.serializer)
      ..add(UsersControllerEmployeesList200Response.serializer)
      ..add(UsersControllerGet200Response.serializer)
      ..add(UsersControllerList200Response.serializer)
      ..add(VerifierResponseRequest.serializer)
      ..add(VerifierResponseRequestStatusEnum.serializer)
      ..add(WebPushPayload.serializer)
      ..add(WebPushPayloadData.serializer)
      ..add(WidgetConfig.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(AnswerRequest)]),
          () => new ListBuilder<AnswerRequest>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(num)]),
          () => new ListBuilder<num>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [
            const FullType(
                BuiltList, const [const FullType(SortingByColumnsInnerInner)])
          ]),
          () => new ListBuilder<BuiltList<SortingByColumnsInnerInner>>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(FormComponentsRequest)]),
          () => new ListBuilder<FormComponentsRequest>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(FormComponentsRequest)]),
          () => new ListBuilder<FormComponentsRequest>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(FormComponentsRequest)]),
          () => new ListBuilder<FormComponentsRequest>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(num)]),
          () => new ListBuilder<num>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GetFormComponentDataResponse)]),
          () => new ListBuilder<GetFormComponentDataResponse>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GetFormComponentDataResponse)]),
          () => new ListBuilder<GetFormComponentDataResponse>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GetFormComponentDataResponse)]),
          () => new ListBuilder<GetFormComponentDataResponse>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GetPlanVersionDataResponse)]),
          () => new ListBuilder<GetPlanVersionDataResponse>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GetPlanVersionDataResponse)]),
          () => new ListBuilder<GetPlanVersionDataResponse>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GetRoleDataResponse)]),
          () => new ListBuilder<GetRoleDataResponse>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(PermissionDataResponse)]),
          () => new ListBuilder<PermissionDataResponse>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GetEntityDataResponse)]),
          () => new ListBuilder<GetEntityDataResponse>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GetRoleDataResponse)]),
          () => new ListBuilder<GetRoleDataResponse>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(PermissionDataResponse)]),
          () => new ListBuilder<PermissionDataResponse>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GetEntityDataResponse)]),
          () => new ListBuilder<GetEntityDataResponse>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GetRoleDataResponse)]),
          () => new ListBuilder<GetRoleDataResponse>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(PermissionDataResponse)]),
          () => new ListBuilder<PermissionDataResponse>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GetEntityDataResponse)]),
          () => new ListBuilder<GetEntityDataResponse>())
      ..addBuilderFactory(
          const FullType(BuiltList,
              const [const FullType(GetSurveyRecipientViewDataResponse)]),
          () => new ListBuilder<GetSurveyRecipientViewDataResponse>())
      ..addBuilderFactory(
          const FullType(BuiltList,
              const [const FullType(GetSurveyRecipientViewDataResponse)]),
          () => new ListBuilder<GetSurveyRecipientViewDataResponse>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(JsonObject)]),
          () => new ListBuilder<JsonObject>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(JsonObject)]),
          () => new ListBuilder<JsonObject>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(JsonObject)]),
          () => new ListBuilder<JsonObject>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(JsonObject)]),
          () => new ListBuilder<JsonObject>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(JsonObject)]),
          () => new ListBuilder<JsonObject>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(JsonObject)]),
          () => new ListBuilder<JsonObject>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(JsonObject)]),
          () => new ListBuilder<JsonObject>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(JsonObject)]),
          () => new ListBuilder<JsonObject>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(JsonObject)]),
          () => new ListBuilder<JsonObject>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(JsonObject)]),
          () => new ListBuilder<JsonObject>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(JsonObject)]),
          () => new ListBuilder<JsonObject>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(JsonObject)]),
          () => new ListBuilder<JsonObject>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(JsonObject)]),
          () => new ListBuilder<JsonObject>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(JsonObject)]),
          () => new ListBuilder<JsonObject>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(JsonObject)]),
          () => new ListBuilder<JsonObject>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(JsonObject)]),
          () => new ListBuilder<JsonObject>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [
            const FullType(
                MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner)
          ]),
          () => new ListBuilder<
              MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [
            const FullType(
                MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner)
          ]),
          () => new ListBuilder<
              MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [
            const FullType(
                MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner)
          ]),
          () => new ListBuilder<
              MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [
            const FullType(
                MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner)
          ]),
          () => new ListBuilder<
              MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [
            const FullType(
                MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInner)
          ]),
          () => new ListBuilder<
              MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInner>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [
            const FullType(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInner)
          ]),
          () => new ListBuilder<
              MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInner>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [
            const FullType(
                MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner)
          ]),
          () => new ListBuilder<
              MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [
            const FullType(
                MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner)
          ]),
          () => new ListBuilder<
              MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [
            const FullType(
                MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner)
          ]),
          () => new ListBuilder<
              MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [
            const FullType(
                MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner)
          ]),
          () => new ListBuilder<
              MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [
            const FullType(
                MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner)
          ]),
          () => new ListBuilder<
              MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [
            const FullType(
                MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInner)
          ]),
          () => new ListBuilder<
              MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInner>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [
            const FullType(
                MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner)
          ]),
          () => new ListBuilder<
              MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [
            const FullType(
                MapLayersControllerGetStreetNamesMap200ResponseAllOfDataFeaturesInner)
          ]),
          () => new ListBuilder<
              MapLayersControllerGetStreetNamesMap200ResponseAllOfDataFeaturesInner>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [
            const FullType(
                MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner)
          ]),
          () => new ListBuilder<
              MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [
            const FullType(
                MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner)
          ]),
          () => new ListBuilder<
              MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(PermissionDataResponse)]),
          () => new ListBuilder<PermissionDataResponse>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(PermissionDataResponse)]),
          () => new ListBuilder<PermissionDataResponse>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(PermissionDataResponse)]),
          () => new ListBuilder<PermissionDataResponse>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList,
              const [const FullType(SurveyResponseAnswerDataResponse)]),
          () => new ListBuilder<SurveyResponseAnswerDataResponse>())
      ..addBuilderFactory(
          const FullType(BuiltList,
              const [const FullType(SurveyResponseAnswerDataResponse)]),
          () => new ListBuilder<SurveyResponseAnswerDataResponse>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(WidgetConfig)]),
          () => new ListBuilder<WidgetConfig>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(WidgetConfig)]),
          () => new ListBuilder<WidgetConfig>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(WidgetConfig)]),
          () => new ListBuilder<WidgetConfig>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(num)]),
          () => new ListBuilder<num>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(num)]),
          () => new ListBuilder<num>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(num)]),
          () => new ListBuilder<num>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(num)]),
          () => new ListBuilder<num>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(num)]),
          () => new ListBuilder<num>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(num)]),
          () => new ListBuilder<num>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(num)]),
          () => new ListBuilder<num>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(num)]),
          () => new ListBuilder<num>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(num)]),
          () => new ListBuilder<num>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(num)]),
          () => new ListBuilder<num>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(num)]),
          () => new ListBuilder<num>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(num)]),
          () => new ListBuilder<num>()))
    .build();

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
