//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:dio/dio.dart';
import 'package:built_value/serializer.dart';
import 'package:qarar_api/src/serializers.dart';
import 'package:qarar_api/src/auth/api_key_auth.dart';
import 'package:qarar_api/src/auth/basic_auth.dart';
import 'package:qarar_api/src/auth/bearer_auth.dart';
import 'package:qarar_api/src/auth/oauth.dart';
import 'package:qarar_api/src/api/activity_log_api.dart';
import 'package:qarar_api/src/api/auth_api.dart';
import 'package:qarar_api/src/api/core_api.dart';
import 'package:qarar_api/src/api/entities_api.dart';
import 'package:qarar_api/src/api/equipments_api.dart';
import 'package:qarar_api/src/api/equipments_store_points_api.dart';
import 'package:qarar_api/src/api/equipments_types_api.dart';
import 'package:qarar_api/src/api/forms_api.dart';
import 'package:qarar_api/src/api/health_api.dart';
import 'package:qarar_api/src/api/map_layers_api.dart';
import 'package:qarar_api/src/api/notifications_api.dart';
import 'package:qarar_api/src/api/permissions_categories_api.dart';
import 'package:qarar_api/src/api/plans_api.dart';
import 'package:qarar_api/src/api/plans_versions_api.dart';
import 'package:qarar_api/src/api/roles_api.dart';
import 'package:qarar_api/src/api/subscriptions_api.dart';
import 'package:qarar_api/src/api/survey_comments_api.dart';
import 'package:qarar_api/src/api/survey_responses_api.dart';
import 'package:qarar_api/src/api/surveys_api.dart';
import 'package:qarar_api/src/api/user_dashboards_api.dart';
import 'package:qarar_api/src/api/users_api.dart';

class QararApi {
  static const String basePath = r'http://localhost';

  final Dio dio;
  final Serializers serializers;

  QararApi({
    Dio? dio,
    Serializers? serializers,
    String? basePathOverride,
    List<Interceptor>? interceptors,
  })  : this.serializers = serializers ?? standardSerializers,
        this.dio = dio ??
            Dio(BaseOptions(
              baseUrl: basePathOverride ?? basePath,
              connectTimeout: const Duration(milliseconds: 5000),
              receiveTimeout: const Duration(milliseconds: 3000),
            )) {
    if (interceptors == null) {
      this.dio.interceptors.addAll([
        OAuthInterceptor(),
        BasicAuthInterceptor(),
        BearerAuthInterceptor(),
        ApiKeyAuthInterceptor(),
      ]);
    } else {
      this.dio.interceptors.addAll(interceptors);
    }
  }

  void setOAuthToken(String name, String token) {
    if (this.dio.interceptors.any((i) => i is OAuthInterceptor)) {
      (this.dio.interceptors.firstWhere((i) => i is OAuthInterceptor)
              as OAuthInterceptor)
          .tokens[name] = token;
    }
  }

  void setBearerAuth(String name, String token) {
    if (this.dio.interceptors.any((i) => i is BearerAuthInterceptor)) {
      (this.dio.interceptors.firstWhere((i) => i is BearerAuthInterceptor)
              as BearerAuthInterceptor)
          .tokens[name] = token;
    }
  }

  void setBasicAuth(String name, String username, String password) {
    if (this.dio.interceptors.any((i) => i is BasicAuthInterceptor)) {
      (this.dio.interceptors.firstWhere((i) => i is BasicAuthInterceptor)
              as BasicAuthInterceptor)
          .authInfo[name] = BasicAuthInfo(username, password);
    }
  }

  void setApiKey(String name, String apiKey) {
    if (this.dio.interceptors.any((i) => i is ApiKeyAuthInterceptor)) {
      (this
                  .dio
                  .interceptors
                  .firstWhere((element) => element is ApiKeyAuthInterceptor)
              as ApiKeyAuthInterceptor)
          .apiKeys[name] = apiKey;
    }
  }

  /// Get ActivityLogApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  ActivityLogApi getActivityLogApi() {
    return ActivityLogApi(dio, serializers);
  }

  /// Get AuthApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  AuthApi getAuthApi() {
    return AuthApi(dio, serializers);
  }

  /// Get CoreApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  CoreApi getCoreApi() {
    return CoreApi(dio, serializers);
  }

  /// Get EntitiesApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  EntitiesApi getEntitiesApi() {
    return EntitiesApi(dio, serializers);
  }

  /// Get EquipmentsApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  EquipmentsApi getEquipmentsApi() {
    return EquipmentsApi(dio, serializers);
  }

  /// Get EquipmentsStorePointsApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  EquipmentsStorePointsApi getEquipmentsStorePointsApi() {
    return EquipmentsStorePointsApi(dio, serializers);
  }

  /// Get EquipmentsTypesApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  EquipmentsTypesApi getEquipmentsTypesApi() {
    return EquipmentsTypesApi(dio, serializers);
  }

  /// Get FormsApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  FormsApi getFormsApi() {
    return FormsApi(dio, serializers);
  }

  /// Get HealthApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  HealthApi getHealthApi() {
    return HealthApi(dio, serializers);
  }

  /// Get MapLayersApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  MapLayersApi getMapLayersApi() {
    return MapLayersApi(dio, serializers);
  }

  /// Get NotificationsApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  NotificationsApi getNotificationsApi() {
    return NotificationsApi(dio, serializers);
  }

  /// Get PermissionsCategoriesApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  PermissionsCategoriesApi getPermissionsCategoriesApi() {
    return PermissionsCategoriesApi(dio, serializers);
  }

  /// Get PlansApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  PlansApi getPlansApi() {
    return PlansApi(dio, serializers);
  }

  /// Get PlansVersionsApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  PlansVersionsApi getPlansVersionsApi() {
    return PlansVersionsApi(dio, serializers);
  }

  /// Get RolesApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  RolesApi getRolesApi() {
    return RolesApi(dio, serializers);
  }

  /// Get SubscriptionsApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  SubscriptionsApi getSubscriptionsApi() {
    return SubscriptionsApi(dio, serializers);
  }

  /// Get SurveyCommentsApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  SurveyCommentsApi getSurveyCommentsApi() {
    return SurveyCommentsApi(dio, serializers);
  }

  /// Get SurveyResponsesApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  SurveyResponsesApi getSurveyResponsesApi() {
    return SurveyResponsesApi(dio, serializers);
  }

  /// Get SurveysApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  SurveysApi getSurveysApi() {
    return SurveysApi(dio, serializers);
  }

  /// Get UserDashboardsApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  UserDashboardsApi getUserDashboardsApi() {
    return UserDashboardsApi(dio, serializers);
  }

  /// Get UsersApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  UsersApi getUsersApi() {
    return UsersApi(dio, serializers);
  }
}
