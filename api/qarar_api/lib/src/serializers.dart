//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_import

import 'package:one_of_serializer/any_of_serializer.dart';
import 'package:one_of_serializer/one_of_serializer.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:built_value/iso_8601_date_time_serializer.dart';
import 'package:qarar_api/src/date_serializer.dart';
import 'package:qarar_api/src/model/date.dart';

import 'package:qarar_api/src/model/action.dart';
import 'package:qarar_api/src/model/activity_log_controller_last_login200_response.dart';
import 'package:qarar_api/src/model/activity_log_controller_user_activity200_response.dart';
import 'package:qarar_api/src/model/answer_request.dart';
import 'package:qarar_api/src/model/app_error_codes_enum.dart';
import 'package:qarar_api/src/model/auth_controller_get_profile200_response.dart';
import 'package:qarar_api/src/model/auth_controller_login200_response.dart';
import 'package:qarar_api/src/model/auth_controller_logout200_response.dart';
import 'package:qarar_api/src/model/auth_controller_refresh_token200_response.dart';
import 'package:qarar_api/src/model/base_data_request.dart';
import 'package:qarar_api/src/model/comment_create_request.dart';
import 'package:qarar_api/src/model/core_controller_upload_file200_response.dart';
import 'package:qarar_api/src/model/core_enum.dart';
import 'package:qarar_api/src/model/entities_controller_get200_response.dart';
import 'package:qarar_api/src/model/entities_controller_list200_response.dart';
import 'package:qarar_api/src/model/entity_create_request.dart';
import 'package:qarar_api/src/model/entity_update_request.dart';
import 'package:qarar_api/src/model/envelope.dart';
import 'package:qarar_api/src/model/equipment_create_request.dart';
import 'package:qarar_api/src/model/equipment_store_point_create_request.dart';
import 'package:qarar_api/src/model/equipment_store_point_update_request.dart';
import 'package:qarar_api/src/model/equipment_type_create_request.dart';
import 'package:qarar_api/src/model/equipment_type_update_request.dart';
import 'package:qarar_api/src/model/equipment_update_request.dart';
import 'package:qarar_api/src/model/equipments_controller_get200_response.dart';
import 'package:qarar_api/src/model/equipments_controller_list200_response.dart';
import 'package:qarar_api/src/model/equipments_store_points_controller_get200_response.dart';
import 'package:qarar_api/src/model/equipments_store_points_controller_list200_response.dart';
import 'package:qarar_api/src/model/equipments_types_controller_get200_response.dart';
import 'package:qarar_api/src/model/equipments_types_controller_list200_response.dart';
import 'package:qarar_api/src/model/error_details.dart';
import 'package:qarar_api/src/model/error_response.dart';
import 'package:qarar_api/src/model/form_components_request.dart';
import 'package:qarar_api/src/model/form_create_request.dart';
import 'package:qarar_api/src/model/form_duplicate_request.dart';
import 'package:qarar_api/src/model/form_response_data_type_enum.dart';
import 'package:qarar_api/src/model/form_update_request.dart';
import 'package:qarar_api/src/model/forms_controller_create200_response.dart';
import 'package:qarar_api/src/model/forms_controller_get200_response.dart';
import 'package:qarar_api/src/model/forms_controller_list200_response.dart';
import 'package:qarar_api/src/model/forms_controller_update200_response.dart';
import 'package:qarar_api/src/model/get_audit_log_event_data_response.dart';
import 'package:qarar_api/src/model/get_audit_log_header_data_response.dart';
import 'package:qarar_api/src/model/get_audit_log_payload_data_response.dart';
import 'package:qarar_api/src/model/get_audit_log_request_data_response.dart';
import 'package:qarar_api/src/model/get_entity_data_response.dart';
import 'package:qarar_api/src/model/get_equipment_data_response.dart';
import 'package:qarar_api/src/model/get_equipment_store_point_data_response.dart';
import 'package:qarar_api/src/model/get_equipment_type_data_response.dart';
import 'package:qarar_api/src/model/get_file_data_response.dart';
import 'package:qarar_api/src/model/get_form_component_data_response.dart';
import 'package:qarar_api/src/model/get_form_component_response_data_type.dart';
import 'package:qarar_api/src/model/get_form_data_response.dart';
import 'package:qarar_api/src/model/get_notification_response_data.dart';
import 'package:qarar_api/src/model/get_notification_type_response_data.dart';
import 'package:qarar_api/src/model/get_plan_data_response.dart';
import 'package:qarar_api/src/model/get_plan_version_data_response.dart';
import 'package:qarar_api/src/model/get_raqmy_info.dart';
import 'package:qarar_api/src/model/get_role_data_response.dart';
import 'package:qarar_api/src/model/get_survey_data_response.dart';
import 'package:qarar_api/src/model/get_survey_recipient_view_data_response.dart';
import 'package:qarar_api/src/model/get_survey_response_data_response.dart';
import 'package:qarar_api/src/model/get_user_activity_log_data_response.dart';
import 'package:qarar_api/src/model/get_user_dashboard_data_response.dart';
import 'package:qarar_api/src/model/get_user_data_response.dart';
import 'package:qarar_api/src/model/health_controller_check200_response.dart';
import 'package:qarar_api/src/model/health_data_response.dart';
import 'package:qarar_api/src/model/incident_reports_data_request.dart';
import 'package:qarar_api/src/model/list_comment_reponse_dto.dart';
import 'package:qarar_api/src/model/list_employee_user_data_response.dart';
import 'package:qarar_api/src/model/list_entities_data_response.dart';
import 'package:qarar_api/src/model/list_equipment_data_response.dart';
import 'package:qarar_api/src/model/list_equipment_store_point_data_response.dart';
import 'package:qarar_api/src/model/list_equipment_type_data_response.dart';
import 'package:qarar_api/src/model/list_form_data_response.dart';
import 'package:qarar_api/src/model/list_permission_category_data_response.dart';
import 'package:qarar_api/src/model/list_plan_data_response.dart';
import 'package:qarar_api/src/model/list_role_data_response.dart';
import 'package:qarar_api/src/model/list_survey_data_response.dart';
import 'package:qarar_api/src/model/list_survey_response_data_response.dart';
import 'package:qarar_api/src/model/list_user_activity_log_data_response.dart';
import 'package:qarar_api/src/model/list_user_dashboard_data_response.dart';
import 'package:qarar_api/src/model/list_user_data_response.dart';
import 'package:qarar_api/src/model/list_user_notifications_response_data_response.dart';
import 'package:qarar_api/src/model/location_data.dart';
import 'package:qarar_api/src/model/login_request.dart';
import 'package:qarar_api/src/model/login_response.dart';
import 'package:qarar_api/src/model/logout_request.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_bridges_tunnels_sites_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_bridges_tunnels_sites_map200_response_all_of_data.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_bridges_tunnels_sites_map200_response_all_of_data_features_inner.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_bridges_tunnels_sites_map200_response_all_of_data_features_inner_properties.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_cables_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_cables_map200_response_all_of_data.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_cables_map200_response_all_of_data_features_inner.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_cables_map200_response_all_of_data_features_inner_properties.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_construction_licenses_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_construction_licenses_map200_response_all_of_data.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_construction_licenses_map200_response_all_of_data_features_inner.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_construction_licenses_map200_response_all_of_data_features_inner_properties.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_district_boundaries_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_district_boundaries_map200_response_all_of_data.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_district_boundaries_map200_response_all_of_data_features_inner.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_district_boundaries_map200_response_all_of_data_features_inner_properties.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_electrical_facilities_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_electrical_facilities_map200_response_all_of_data.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_electrical_facilities_map200_response_all_of_data_features_inner.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_electrical_facilities_map200_response_all_of_data_features_inner_properties.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_incident_reports_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_incident_reports_map200_response_all_of_data.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_incident_reports_map200_response_all_of_data_features_inner.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_incident_reports_map200_response_all_of_data_features_inner_properties.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_incident_reports_map200_response_all_of_data_features_inner_properties_id.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_land_base_parcel_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_land_base_parcel_map200_response_all_of_data.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_land_base_parcel_map200_response_all_of_data_features_inner.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_land_base_parcel_map200_response_all_of_data_features_inner_properties.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_light_poles_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_light_poles_map200_response_all_of_data.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_light_poles_map200_response_all_of_data_features_inner.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_light_poles_map200_response_all_of_data_features_inner_properties.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_manhole_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_manhole_map200_response_all_of_data.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_manhole_map200_response_all_of_data_features_inner.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_manhole_map200_response_all_of_data_features_inner_properties.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_municipalities_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_municipalities_map200_response_all_of_data.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_municipalities_map200_response_all_of_data_features_inner.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_municipalities_map200_response_all_of_data_features_inner_properties.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_plan_data_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_plan_data_map200_response_all_of_data.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_plan_data_map200_response_all_of_data_features_inner.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_plan_data_map200_response_all_of_data_features_inner_properties.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_sea_out_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_sea_out_map200_response_all_of_data.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_sea_out_map200_response_all_of_data_features_inner.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_sea_out_map200_response_all_of_data_features_inner_properties.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_storage_locations_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_storage_locations_map200_response_all_of_data.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_storage_locations_map200_response_all_of_data_features_inner.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_street_names_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_street_names_map200_response_all_of_data.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_street_names_map200_response_all_of_data_features_inner.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_street_names_map200_response_all_of_data_features_inner_properties.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_transport_tracks_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_transport_tracks_map200_response_all_of_data.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_transport_tracks_map200_response_all_of_data_features_inner.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_transport_tracks_map200_response_all_of_data_features_inner_properties.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_urban_area_boundaries_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_urban_area_boundaries_map200_response_all_of_data.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_urban_area_boundaries_map200_response_all_of_data_features_inner.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_urban_area_boundaries_map200_response_all_of_data_features_inner_properties.dart';
import 'package:qarar_api/src/model/notifications_controller_count_unread200_response.dart';
import 'package:qarar_api/src/model/notifications_controller_my_notifications200_response.dart';
import 'package:qarar_api/src/model/paginated_documented.dart';
import 'package:qarar_api/src/model/paginated_links_documented.dart';
import 'package:qarar_api/src/model/paginated_meta_documented.dart';
import 'package:qarar_api/src/model/permission_data_response.dart';
import 'package:qarar_api/src/model/permissions_categories_controller_list200_response.dart';
import 'package:qarar_api/src/model/permissions_enum.dart';
import 'package:qarar_api/src/model/plan_create_request.dart';
import 'package:qarar_api/src/model/plan_update_request.dart';
import 'package:qarar_api/src/model/plan_version_create_request.dart';
import 'package:qarar_api/src/model/plans_controller_get200_response.dart';
import 'package:qarar_api/src/model/plans_controller_list200_response.dart';
import 'package:qarar_api/src/model/plans_versions_controller_get200_response.dart';
import 'package:qarar_api/src/model/profile_response.dart';
import 'package:qarar_api/src/model/recipient_response_request.dart';
import 'package:qarar_api/src/model/recipient_type.dart';
import 'package:qarar_api/src/model/refresh_token_request.dart';
import 'package:qarar_api/src/model/refresh_token_response.dart';
import 'package:qarar_api/src/model/role_create_request.dart';
import 'package:qarar_api/src/model/role_update_request.dart';
import 'package:qarar_api/src/model/roles_controller_get200_response.dart';
import 'package:qarar_api/src/model/roles_controller_list200_response.dart';
import 'package:qarar_api/src/model/sorting_by_columns_inner_inner.dart';
import 'package:qarar_api/src/model/status_response.dart';
import 'package:qarar_api/src/model/subscribe_request.dart';
import 'package:qarar_api/src/model/success_response.dart';
import 'package:qarar_api/src/model/survey_comments_controller_list200_response.dart';
import 'package:qarar_api/src/model/survey_create_request.dart';
import 'package:qarar_api/src/model/survey_request_data_response.dart';
import 'package:qarar_api/src/model/survey_response_answer_data_response.dart';
import 'package:qarar_api/src/model/survey_responses_controller_list200_response.dart';
import 'package:qarar_api/src/model/survey_responses_controller_response_details200_response.dart';
import 'package:qarar_api/src/model/survey_update_request.dart';
import 'package:qarar_api/src/model/surveys_controller_get200_response.dart';
import 'package:qarar_api/src/model/surveys_controller_list200_response.dart';
import 'package:qarar_api/src/model/surveys_controller_requests200_response.dart';
import 'package:qarar_api/src/model/surveys_controller_requests_details200_response.dart';
import 'package:qarar_api/src/model/unsubscribe_request.dart';
import 'package:qarar_api/src/model/upload_file_response.dart';
import 'package:qarar_api/src/model/user_create_request.dart';
import 'package:qarar_api/src/model/user_dashboard_create_request.dart';
import 'package:qarar_api/src/model/user_dashboard_update_request.dart';
import 'package:qarar_api/src/model/user_dashboards_controller_create200_response.dart';
import 'package:qarar_api/src/model/user_dashboards_controller_get_default_dashboard200_response.dart';
import 'package:qarar_api/src/model/user_dashboards_controller_list200_response.dart';
import 'package:qarar_api/src/model/user_update_request.dart';
import 'package:qarar_api/src/model/users_controller_employees_list200_response.dart';
import 'package:qarar_api/src/model/users_controller_get200_response.dart';
import 'package:qarar_api/src/model/users_controller_list200_response.dart';
import 'package:qarar_api/src/model/verifier_response_request.dart';
import 'package:qarar_api/src/model/web_push_payload.dart';
import 'package:qarar_api/src/model/web_push_payload_data.dart';
import 'package:qarar_api/src/model/widget_config.dart';

part 'serializers.g.dart';

@SerializersFor([
  Action,
  ActivityLogControllerLastLogin200Response,
  ActivityLogControllerUserActivity200Response,
  AnswerRequest,
  AppErrorCodesEnum,
  AuthControllerGetProfile200Response,
  AuthControllerLogin200Response,
  AuthControllerLogout200Response,
  AuthControllerRefreshToken200Response,
  BaseDataRequest,
  CommentCreateRequest,
  CoreControllerUploadFile200Response,
  CoreEnum,
  EntitiesControllerGet200Response,
  EntitiesControllerList200Response,
  EntityCreateRequest,
  EntityUpdateRequest,
  Envelope,
  EquipmentCreateRequest,
  EquipmentStorePointCreateRequest,
  EquipmentStorePointUpdateRequest,
  EquipmentTypeCreateRequest,
  EquipmentTypeUpdateRequest,
  EquipmentUpdateRequest,
  EquipmentsControllerGet200Response,
  EquipmentsControllerList200Response,
  EquipmentsStorePointsControllerGet200Response,
  EquipmentsStorePointsControllerList200Response,
  EquipmentsTypesControllerGet200Response,
  EquipmentsTypesControllerList200Response,
  ErrorDetails,
  ErrorResponse,
  FormComponentsRequest,
  FormCreateRequest,
  FormDuplicateRequest,
  FormResponseDataTypeEnum,
  FormUpdateRequest,
  FormsControllerCreate200Response,
  FormsControllerGet200Response,
  FormsControllerList200Response,
  FormsControllerUpdate200Response,
  GetAuditLogEventDataResponse,
  GetAuditLogHeaderDataResponse,
  GetAuditLogPayloadDataResponse,
  GetAuditLogRequestDataResponse,
  GetEntityDataResponse,
  GetEquipmentDataResponse,
  GetEquipmentStorePointDataResponse,
  GetEquipmentTypeDataResponse,
  GetFileDataResponse,
  GetFormComponentDataResponse,
  GetFormComponentResponseDataType,
  GetFormDataResponse,
  GetNotificationResponseData,
  GetNotificationTypeResponseData,
  GetPlanDataResponse,
  GetPlanVersionDataResponse,
  GetRaqmyInfo,
  GetRoleDataResponse,
  GetSurveyDataResponse,
  GetSurveyRecipientViewDataResponse,
  GetSurveyResponseDataResponse,
  GetUserActivityLogDataResponse,
  GetUserDashboardDataResponse,
  GetUserDataResponse,
  HealthControllerCheck200Response,
  HealthDataResponse,
  IncidentReportsDataRequest,
  ListCommentReponseDto,
  ListEmployeeUserDataResponse,
  ListEntitiesDataResponse,
  ListEquipmentDataResponse,
  ListEquipmentStorePointDataResponse,
  ListEquipmentTypeDataResponse,
  ListFormDataResponse,
  ListPermissionCategoryDataResponse,
  ListPlanDataResponse,
  ListRoleDataResponse,
  ListSurveyDataResponse,
  ListSurveyResponseDataResponse,
  ListUserActivityLogDataResponse,
  ListUserDashboardDataResponse,
  ListUserDataResponse,
  ListUserNotificationsResponseDataResponse,
  LocationData,
  LoginRequest,
  LoginResponse,
  LogoutRequest,
  MapLayersControllerGetBridgesTunnelsSitesMap200Response,
  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData,
  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner,
  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties,
  MapLayersControllerGetCablesMap200Response,
  MapLayersControllerGetCablesMap200ResponseAllOfData,
  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner,
  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerProperties,
  MapLayersControllerGetConstructionLicensesMap200Response,
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData,
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner,
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties,
  MapLayersControllerGetDistrictBoundariesMap200Response,
  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData,
  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner,
  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties,
  MapLayersControllerGetElectricalFacilitiesMap200Response,
  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData,
  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInner,
  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerProperties,
  MapLayersControllerGetIncidentReportsMap200Response,
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfData,
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInner,
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties,
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId,
  MapLayersControllerGetLandBaseParcelMap200Response,
  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData,
  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner,
  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties,
  MapLayersControllerGetLightPolesMap200Response,
  MapLayersControllerGetLightPolesMap200ResponseAllOfData,
  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner,
  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerProperties,
  MapLayersControllerGetManholeMap200Response,
  MapLayersControllerGetManholeMap200ResponseAllOfData,
  MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner,
  MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties,
  MapLayersControllerGetMunicipalitiesMap200Response,
  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData,
  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner,
  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerProperties,
  MapLayersControllerGetPlanDataMap200Response,
  MapLayersControllerGetPlanDataMap200ResponseAllOfData,
  MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner,
  MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties,
  MapLayersControllerGetSeaOutMap200Response,
  MapLayersControllerGetSeaOutMap200ResponseAllOfData,
  MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInner,
  MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerProperties,
  MapLayersControllerGetStorageLocationsMap200Response,
  MapLayersControllerGetStorageLocationsMap200ResponseAllOfData,
  MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner,
  MapLayersControllerGetStreetNamesMap200Response,
  MapLayersControllerGetStreetNamesMap200ResponseAllOfData,
  MapLayersControllerGetStreetNamesMap200ResponseAllOfDataFeaturesInner,
  MapLayersControllerGetStreetNamesMap200ResponseAllOfDataFeaturesInnerProperties,
  MapLayersControllerGetTransportTracksMap200Response,
  MapLayersControllerGetTransportTracksMap200ResponseAllOfData,
  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner,
  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerProperties,
  MapLayersControllerGetUrbanAreaBoundariesMap200Response,
  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData,
  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner,
  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties,
  NotificationsControllerCountUnread200Response,
  NotificationsControllerMyNotifications200Response,
  PaginatedDocumented,
  $PaginatedDocumented,
  PaginatedLinksDocumented,
  PaginatedMetaDocumented,
  PermissionDataResponse,
  PermissionsCategoriesControllerList200Response,
  PermissionsEnum,
  PlanCreateRequest,
  PlanUpdateRequest,
  PlanVersionCreateRequest,
  PlansControllerGet200Response,
  PlansControllerList200Response,
  PlansVersionsControllerGet200Response,
  ProfileResponse,
  RecipientResponseRequest,
  RecipientType,
  RefreshTokenRequest,
  RefreshTokenResponse,
  RoleCreateRequest,
  RoleUpdateRequest,
  RolesControllerGet200Response,
  RolesControllerList200Response,
  SortingByColumnsInnerInner,
  StatusResponse,
  SubscribeRequest,
  SuccessResponse,
  $SuccessResponse,
  SurveyCommentsControllerList200Response,
  SurveyCreateRequest,
  SurveyRequestDataResponse,
  SurveyResponseAnswerDataResponse,
  SurveyResponsesControllerList200Response,
  SurveyResponsesControllerResponseDetails200Response,
  SurveyUpdateRequest,
  SurveysControllerGet200Response,
  SurveysControllerList200Response,
  SurveysControllerRequests200Response,
  SurveysControllerRequestsDetails200Response,
  UnsubscribeRequest,
  UploadFileResponse,
  UserCreateRequest,
  UserDashboardCreateRequest,
  UserDashboardUpdateRequest,
  UserDashboardsControllerCreate200Response,
  UserDashboardsControllerGetDefaultDashboard200Response,
  UserDashboardsControllerList200Response,
  UserUpdateRequest,
  UsersControllerEmployeesList200Response,
  UsersControllerGet200Response,
  UsersControllerList200Response,
  VerifierResponseRequest,
  WebPushPayload,
  WebPushPayloadData,
  WidgetConfig,
])
Serializers serializers = (_$serializers.toBuilder()
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(String)]),
        () => ListBuilder<String>(),
      )
      ..add(PaginatedDocumented.serializer)
      ..add(SuccessResponse.serializer)
      ..add(const OneOfSerializer())
      ..add(const AnyOfSerializer())
      ..add(const DateSerializer())
      ..add(Iso8601DateTimeSerializer()))
    .build();

Serializers standardSerializers =
    (serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
