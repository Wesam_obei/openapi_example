// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_district_boundaries_map200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetDistrictBoundariesMap200Response
    extends MapLayersControllerGetDistrictBoundariesMap200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$MapLayersControllerGetDistrictBoundariesMap200Response(
          [void Function(
                  MapLayersControllerGetDistrictBoundariesMap200ResponseBuilder)?
              updates]) =>
      (new MapLayersControllerGetDistrictBoundariesMap200ResponseBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetDistrictBoundariesMap200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(success,
        r'MapLayersControllerGetDistrictBoundariesMap200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(data,
        r'MapLayersControllerGetDistrictBoundariesMap200Response', 'data');
  }

  @override
  MapLayersControllerGetDistrictBoundariesMap200Response rebuild(
          void Function(
                  MapLayersControllerGetDistrictBoundariesMap200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetDistrictBoundariesMap200ResponseBuilder toBuilder() =>
      new MapLayersControllerGetDistrictBoundariesMap200ResponseBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapLayersControllerGetDistrictBoundariesMap200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetDistrictBoundariesMap200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class MapLayersControllerGetDistrictBoundariesMap200ResponseBuilder
    implements
        Builder<MapLayersControllerGetDistrictBoundariesMap200Response,
            MapLayersControllerGetDistrictBoundariesMap200ResponseBuilder>,
        SuccessResponseBuilder {
  _$MapLayersControllerGetDistrictBoundariesMap200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  MapLayersControllerGetDistrictBoundariesMap200ResponseBuilder() {
    MapLayersControllerGetDistrictBoundariesMap200Response._defaults(this);
  }

  MapLayersControllerGetDistrictBoundariesMap200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      covariant MapLayersControllerGetDistrictBoundariesMap200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapLayersControllerGetDistrictBoundariesMap200Response;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetDistrictBoundariesMap200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetDistrictBoundariesMap200Response build() => _build();

  _$MapLayersControllerGetDistrictBoundariesMap200Response _build() {
    final _$result = _$v ??
        new _$MapLayersControllerGetDistrictBoundariesMap200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success,
                r'MapLayersControllerGetDistrictBoundariesMap200Response',
                'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data,
                r'MapLayersControllerGetDistrictBoundariesMap200Response',
                'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
