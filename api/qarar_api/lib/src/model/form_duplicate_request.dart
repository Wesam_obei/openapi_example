//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'form_duplicate_request.g.dart';

/// FormDuplicateRequest
///
/// Properties:
/// * [fromId]
/// * [name]
/// * [fileId]
/// * [fileToken]
@BuiltValue()
abstract class FormDuplicateRequest
    implements Built<FormDuplicateRequest, FormDuplicateRequestBuilder> {
  @BuiltValueField(wireName: r'fromId')
  num get fromId;

  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'fileId')
  num? get fileId;

  @BuiltValueField(wireName: r'file_token')
  String? get fileToken;

  FormDuplicateRequest._();

  factory FormDuplicateRequest([void updates(FormDuplicateRequestBuilder b)]) =
      _$FormDuplicateRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(FormDuplicateRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<FormDuplicateRequest> get serializer =>
      _$FormDuplicateRequestSerializer();
}

class _$FormDuplicateRequestSerializer
    implements PrimitiveSerializer<FormDuplicateRequest> {
  @override
  final Iterable<Type> types = const [
    FormDuplicateRequest,
    _$FormDuplicateRequest
  ];

  @override
  final String wireName = r'FormDuplicateRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    FormDuplicateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'fromId';
    yield serializers.serialize(
      object.fromId,
      specifiedType: const FullType(num),
    );
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    if (object.fileId != null) {
      yield r'fileId';
      yield serializers.serialize(
        object.fileId,
        specifiedType: const FullType(num),
      );
    }
    if (object.fileToken != null) {
      yield r'file_token';
      yield serializers.serialize(
        object.fileToken,
        specifiedType: const FullType(String),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    FormDuplicateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required FormDuplicateRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'fromId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.fromId = valueDes;
          break;
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'fileId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.fileId = valueDes;
          break;
        case r'file_token':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.fileToken = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  FormDuplicateRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = FormDuplicateRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
