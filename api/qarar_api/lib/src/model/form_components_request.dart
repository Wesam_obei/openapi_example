//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/form_response_data_type_enum.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'form_components_request.g.dart';

/// FormComponentsRequest
///
/// Properties:
/// * [responseDataType]
/// * [isArray]
/// * [label]
/// * [placeholder]
/// * [description]
/// * [isRequired]
/// * [allowMultipleResponse]
/// * [isGroup]
/// * [config]
/// * [order]
/// * [id]
/// * [children]
@BuiltValue()
abstract class FormComponentsRequest
    implements Built<FormComponentsRequest, FormComponentsRequestBuilder> {
  @BuiltValueField(wireName: r'responseDataType')
  FormResponseDataTypeEnum get responseDataType;
  // enum responseDataTypeEnum {  null,  string,  integer,  number,  date,  datetime,  time,  file,  range-date,  range-datetime,  range-time,  coordinate,  };

  @BuiltValueField(wireName: r'is_array')
  bool get isArray;

  @BuiltValueField(wireName: r'label')
  String get label;

  @BuiltValueField(wireName: r'placeholder')
  String? get placeholder;

  @BuiltValueField(wireName: r'description')
  String get description;

  @BuiltValueField(wireName: r'is_required')
  bool get isRequired;

  @BuiltValueField(wireName: r'allow_multiple_response')
  bool get allowMultipleResponse;

  @BuiltValueField(wireName: r'is_group')
  bool get isGroup;

  @BuiltValueField(wireName: r'config')
  bool get config;

  @BuiltValueField(wireName: r'order')
  num get order;

  @BuiltValueField(wireName: r'id')
  num? get id;

  @BuiltValueField(wireName: r'children')
  BuiltList<FormComponentsRequest>? get children;

  FormComponentsRequest._();

  factory FormComponentsRequest(
      [void updates(FormComponentsRequestBuilder b)]) = _$FormComponentsRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(FormComponentsRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<FormComponentsRequest> get serializer =>
      _$FormComponentsRequestSerializer();
}

class _$FormComponentsRequestSerializer
    implements PrimitiveSerializer<FormComponentsRequest> {
  @override
  final Iterable<Type> types = const [
    FormComponentsRequest,
    _$FormComponentsRequest
  ];

  @override
  final String wireName = r'FormComponentsRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    FormComponentsRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'responseDataType';
    yield serializers.serialize(
      object.responseDataType,
      specifiedType: const FullType(FormResponseDataTypeEnum),
    );
    yield r'is_array';
    yield serializers.serialize(
      object.isArray,
      specifiedType: const FullType(bool),
    );
    yield r'label';
    yield serializers.serialize(
      object.label,
      specifiedType: const FullType(String),
    );
    yield r'placeholder';
    yield object.placeholder == null
        ? null
        : serializers.serialize(
            object.placeholder,
            specifiedType: const FullType.nullable(String),
          );
    yield r'description';
    yield serializers.serialize(
      object.description,
      specifiedType: const FullType(String),
    );
    yield r'is_required';
    yield serializers.serialize(
      object.isRequired,
      specifiedType: const FullType(bool),
    );
    yield r'allow_multiple_response';
    yield serializers.serialize(
      object.allowMultipleResponse,
      specifiedType: const FullType(bool),
    );
    yield r'is_group';
    yield serializers.serialize(
      object.isGroup,
      specifiedType: const FullType(bool),
    );
    yield r'config';
    yield serializers.serialize(
      object.config,
      specifiedType: const FullType(bool),
    );
    yield r'order';
    yield serializers.serialize(
      object.order,
      specifiedType: const FullType(num),
    );
    if (object.id != null) {
      yield r'id';
      yield serializers.serialize(
        object.id,
        specifiedType: const FullType(num),
      );
    }
    if (object.children != null) {
      yield r'children';
      yield serializers.serialize(
        object.children,
        specifiedType:
            const FullType(BuiltList, [FullType(FormComponentsRequest)]),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    FormComponentsRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required FormComponentsRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'responseDataType':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(FormResponseDataTypeEnum),
          ) as FormResponseDataTypeEnum;
          result.responseDataType = valueDes;
          break;
        case r'is_array':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.isArray = valueDes;
          break;
        case r'label':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.label = valueDes;
          break;
        case r'placeholder':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(String),
          ) as String?;
          if (valueDes == null) continue;
          result.placeholder = valueDes;
          break;
        case r'description':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.description = valueDes;
          break;
        case r'is_required':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.isRequired = valueDes;
          break;
        case r'allow_multiple_response':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.allowMultipleResponse = valueDes;
          break;
        case r'is_group':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.isGroup = valueDes;
          break;
        case r'config':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.config = valueDes;
          break;
        case r'order':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.order = valueDes;
          break;
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'children':
          final valueDes = serializers.deserialize(
            value,
            specifiedType:
                const FullType(BuiltList, [FullType(FormComponentsRequest)]),
          ) as BuiltList<FormComponentsRequest>;
          result.children.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  FormComponentsRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = FormComponentsRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
