// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_form_component_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetFormComponentDataResponse extends GetFormComponentDataResponse {
  @override
  final num id;
  @override
  final String label;
  @override
  final String? placeholder;
  @override
  final String description;
  @override
  final bool isRequired;
  @override
  final bool isActive;
  @override
  final num responseDataTypeId;
  @override
  final num parentComponentId;
  @override
  final bool isArray;
  @override
  final bool isGroup;
  @override
  final bool allowMultipleResponse;
  @override
  final num order;
  @override
  final JsonObject config;
  @override
  final DateTime createdAt;
  @override
  final GetUserDataResponse creator;
  @override
  final GetFormComponentResponseDataType responseDataType;
  @override
  final BuiltList<GetFormComponentDataResponse> children;

  factory _$GetFormComponentDataResponse(
          [void Function(GetFormComponentDataResponseBuilder)? updates]) =>
      (new GetFormComponentDataResponseBuilder()..update(updates))._build();

  _$GetFormComponentDataResponse._(
      {required this.id,
      required this.label,
      this.placeholder,
      required this.description,
      required this.isRequired,
      required this.isActive,
      required this.responseDataTypeId,
      required this.parentComponentId,
      required this.isArray,
      required this.isGroup,
      required this.allowMultipleResponse,
      required this.order,
      required this.config,
      required this.createdAt,
      required this.creator,
      required this.responseDataType,
      required this.children})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'GetFormComponentDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        label, r'GetFormComponentDataResponse', 'label');
    BuiltValueNullFieldError.checkNotNull(
        description, r'GetFormComponentDataResponse', 'description');
    BuiltValueNullFieldError.checkNotNull(
        isRequired, r'GetFormComponentDataResponse', 'isRequired');
    BuiltValueNullFieldError.checkNotNull(
        isActive, r'GetFormComponentDataResponse', 'isActive');
    BuiltValueNullFieldError.checkNotNull(responseDataTypeId,
        r'GetFormComponentDataResponse', 'responseDataTypeId');
    BuiltValueNullFieldError.checkNotNull(parentComponentId,
        r'GetFormComponentDataResponse', 'parentComponentId');
    BuiltValueNullFieldError.checkNotNull(
        isArray, r'GetFormComponentDataResponse', 'isArray');
    BuiltValueNullFieldError.checkNotNull(
        isGroup, r'GetFormComponentDataResponse', 'isGroup');
    BuiltValueNullFieldError.checkNotNull(allowMultipleResponse,
        r'GetFormComponentDataResponse', 'allowMultipleResponse');
    BuiltValueNullFieldError.checkNotNull(
        order, r'GetFormComponentDataResponse', 'order');
    BuiltValueNullFieldError.checkNotNull(
        config, r'GetFormComponentDataResponse', 'config');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'GetFormComponentDataResponse', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        creator, r'GetFormComponentDataResponse', 'creator');
    BuiltValueNullFieldError.checkNotNull(
        responseDataType, r'GetFormComponentDataResponse', 'responseDataType');
    BuiltValueNullFieldError.checkNotNull(
        children, r'GetFormComponentDataResponse', 'children');
  }

  @override
  GetFormComponentDataResponse rebuild(
          void Function(GetFormComponentDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetFormComponentDataResponseBuilder toBuilder() =>
      new GetFormComponentDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetFormComponentDataResponse &&
        id == other.id &&
        label == other.label &&
        placeholder == other.placeholder &&
        description == other.description &&
        isRequired == other.isRequired &&
        isActive == other.isActive &&
        responseDataTypeId == other.responseDataTypeId &&
        parentComponentId == other.parentComponentId &&
        isArray == other.isArray &&
        isGroup == other.isGroup &&
        allowMultipleResponse == other.allowMultipleResponse &&
        order == other.order &&
        config == other.config &&
        createdAt == other.createdAt &&
        creator == other.creator &&
        responseDataType == other.responseDataType &&
        children == other.children;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, label.hashCode);
    _$hash = $jc(_$hash, placeholder.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, isRequired.hashCode);
    _$hash = $jc(_$hash, isActive.hashCode);
    _$hash = $jc(_$hash, responseDataTypeId.hashCode);
    _$hash = $jc(_$hash, parentComponentId.hashCode);
    _$hash = $jc(_$hash, isArray.hashCode);
    _$hash = $jc(_$hash, isGroup.hashCode);
    _$hash = $jc(_$hash, allowMultipleResponse.hashCode);
    _$hash = $jc(_$hash, order.hashCode);
    _$hash = $jc(_$hash, config.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, creator.hashCode);
    _$hash = $jc(_$hash, responseDataType.hashCode);
    _$hash = $jc(_$hash, children.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetFormComponentDataResponse')
          ..add('id', id)
          ..add('label', label)
          ..add('placeholder', placeholder)
          ..add('description', description)
          ..add('isRequired', isRequired)
          ..add('isActive', isActive)
          ..add('responseDataTypeId', responseDataTypeId)
          ..add('parentComponentId', parentComponentId)
          ..add('isArray', isArray)
          ..add('isGroup', isGroup)
          ..add('allowMultipleResponse', allowMultipleResponse)
          ..add('order', order)
          ..add('config', config)
          ..add('createdAt', createdAt)
          ..add('creator', creator)
          ..add('responseDataType', responseDataType)
          ..add('children', children))
        .toString();
  }
}

class GetFormComponentDataResponseBuilder
    implements
        Builder<GetFormComponentDataResponse,
            GetFormComponentDataResponseBuilder> {
  _$GetFormComponentDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _label;
  String? get label => _$this._label;
  set label(String? label) => _$this._label = label;

  String? _placeholder;
  String? get placeholder => _$this._placeholder;
  set placeholder(String? placeholder) => _$this._placeholder = placeholder;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  bool? _isRequired;
  bool? get isRequired => _$this._isRequired;
  set isRequired(bool? isRequired) => _$this._isRequired = isRequired;

  bool? _isActive;
  bool? get isActive => _$this._isActive;
  set isActive(bool? isActive) => _$this._isActive = isActive;

  num? _responseDataTypeId;
  num? get responseDataTypeId => _$this._responseDataTypeId;
  set responseDataTypeId(num? responseDataTypeId) =>
      _$this._responseDataTypeId = responseDataTypeId;

  num? _parentComponentId;
  num? get parentComponentId => _$this._parentComponentId;
  set parentComponentId(num? parentComponentId) =>
      _$this._parentComponentId = parentComponentId;

  bool? _isArray;
  bool? get isArray => _$this._isArray;
  set isArray(bool? isArray) => _$this._isArray = isArray;

  bool? _isGroup;
  bool? get isGroup => _$this._isGroup;
  set isGroup(bool? isGroup) => _$this._isGroup = isGroup;

  bool? _allowMultipleResponse;
  bool? get allowMultipleResponse => _$this._allowMultipleResponse;
  set allowMultipleResponse(bool? allowMultipleResponse) =>
      _$this._allowMultipleResponse = allowMultipleResponse;

  num? _order;
  num? get order => _$this._order;
  set order(num? order) => _$this._order = order;

  JsonObject? _config;
  JsonObject? get config => _$this._config;
  set config(JsonObject? config) => _$this._config = config;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  GetUserDataResponseBuilder? _creator;
  GetUserDataResponseBuilder get creator =>
      _$this._creator ??= new GetUserDataResponseBuilder();
  set creator(GetUserDataResponseBuilder? creator) => _$this._creator = creator;

  GetFormComponentResponseDataTypeBuilder? _responseDataType;
  GetFormComponentResponseDataTypeBuilder get responseDataType =>
      _$this._responseDataType ??=
          new GetFormComponentResponseDataTypeBuilder();
  set responseDataType(
          GetFormComponentResponseDataTypeBuilder? responseDataType) =>
      _$this._responseDataType = responseDataType;

  ListBuilder<GetFormComponentDataResponse>? _children;
  ListBuilder<GetFormComponentDataResponse> get children =>
      _$this._children ??= new ListBuilder<GetFormComponentDataResponse>();
  set children(ListBuilder<GetFormComponentDataResponse>? children) =>
      _$this._children = children;

  GetFormComponentDataResponseBuilder() {
    GetFormComponentDataResponse._defaults(this);
  }

  GetFormComponentDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _label = $v.label;
      _placeholder = $v.placeholder;
      _description = $v.description;
      _isRequired = $v.isRequired;
      _isActive = $v.isActive;
      _responseDataTypeId = $v.responseDataTypeId;
      _parentComponentId = $v.parentComponentId;
      _isArray = $v.isArray;
      _isGroup = $v.isGroup;
      _allowMultipleResponse = $v.allowMultipleResponse;
      _order = $v.order;
      _config = $v.config;
      _createdAt = $v.createdAt;
      _creator = $v.creator.toBuilder();
      _responseDataType = $v.responseDataType.toBuilder();
      _children = $v.children.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetFormComponentDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetFormComponentDataResponse;
  }

  @override
  void update(void Function(GetFormComponentDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetFormComponentDataResponse build() => _build();

  _$GetFormComponentDataResponse _build() {
    _$GetFormComponentDataResponse _$result;
    try {
      _$result = _$v ??
          new _$GetFormComponentDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'GetFormComponentDataResponse', 'id'),
              label: BuiltValueNullFieldError.checkNotNull(
                  label, r'GetFormComponentDataResponse', 'label'),
              placeholder: placeholder,
              description: BuiltValueNullFieldError.checkNotNull(
                  description, r'GetFormComponentDataResponse', 'description'),
              isRequired: BuiltValueNullFieldError.checkNotNull(
                  isRequired, r'GetFormComponentDataResponse', 'isRequired'),
              isActive: BuiltValueNullFieldError.checkNotNull(
                  isActive, r'GetFormComponentDataResponse', 'isActive'),
              responseDataTypeId: BuiltValueNullFieldError.checkNotNull(
                  responseDataTypeId,
                  r'GetFormComponentDataResponse',
                  'responseDataTypeId'),
              parentComponentId: BuiltValueNullFieldError.checkNotNull(
                  parentComponentId, r'GetFormComponentDataResponse', 'parentComponentId'),
              isArray: BuiltValueNullFieldError.checkNotNull(isArray, r'GetFormComponentDataResponse', 'isArray'),
              isGroup: BuiltValueNullFieldError.checkNotNull(isGroup, r'GetFormComponentDataResponse', 'isGroup'),
              allowMultipleResponse: BuiltValueNullFieldError.checkNotNull(allowMultipleResponse, r'GetFormComponentDataResponse', 'allowMultipleResponse'),
              order: BuiltValueNullFieldError.checkNotNull(order, r'GetFormComponentDataResponse', 'order'),
              config: BuiltValueNullFieldError.checkNotNull(config, r'GetFormComponentDataResponse', 'config'),
              createdAt: BuiltValueNullFieldError.checkNotNull(createdAt, r'GetFormComponentDataResponse', 'createdAt'),
              creator: creator.build(),
              responseDataType: responseDataType.build(),
              children: children.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'creator';
        creator.build();
        _$failedField = 'responseDataType';
        responseDataType.build();
        _$failedField = 'children';
        children.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'GetFormComponentDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
