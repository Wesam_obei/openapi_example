// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'health_controller_check200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$HealthControllerCheck200Response
    extends HealthControllerCheck200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$HealthControllerCheck200Response(
          [void Function(HealthControllerCheck200ResponseBuilder)? updates]) =>
      (new HealthControllerCheck200ResponseBuilder()..update(updates))._build();

  _$HealthControllerCheck200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'HealthControllerCheck200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'HealthControllerCheck200Response', 'data');
  }

  @override
  HealthControllerCheck200Response rebuild(
          void Function(HealthControllerCheck200ResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  HealthControllerCheck200ResponseBuilder toBuilder() =>
      new HealthControllerCheck200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is HealthControllerCheck200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'HealthControllerCheck200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class HealthControllerCheck200ResponseBuilder
    implements
        Builder<HealthControllerCheck200Response,
            HealthControllerCheck200ResponseBuilder>,
        SuccessResponseBuilder {
  _$HealthControllerCheck200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  HealthControllerCheck200ResponseBuilder() {
    HealthControllerCheck200Response._defaults(this);
  }

  HealthControllerCheck200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant HealthControllerCheck200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$HealthControllerCheck200Response;
  }

  @override
  void update(void Function(HealthControllerCheck200ResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  HealthControllerCheck200Response build() => _build();

  _$HealthControllerCheck200Response _build() {
    final _$result = _$v ??
        new _$HealthControllerCheck200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success, r'HealthControllerCheck200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'HealthControllerCheck200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
