// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'core_enum.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$CoreEnum extends CoreEnum {
  @override
  final AppErrorCodesEnum appErrorCodes;
  @override
  final PermissionsEnum permissions;

  factory _$CoreEnum([void Function(CoreEnumBuilder)? updates]) =>
      (new CoreEnumBuilder()..update(updates))._build();

  _$CoreEnum._({required this.appErrorCodes, required this.permissions})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        appErrorCodes, r'CoreEnum', 'appErrorCodes');
    BuiltValueNullFieldError.checkNotNull(
        permissions, r'CoreEnum', 'permissions');
  }

  @override
  CoreEnum rebuild(void Function(CoreEnumBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CoreEnumBuilder toBuilder() => new CoreEnumBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CoreEnum &&
        appErrorCodes == other.appErrorCodes &&
        permissions == other.permissions;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, appErrorCodes.hashCode);
    _$hash = $jc(_$hash, permissions.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'CoreEnum')
          ..add('appErrorCodes', appErrorCodes)
          ..add('permissions', permissions))
        .toString();
  }
}

class CoreEnumBuilder implements Builder<CoreEnum, CoreEnumBuilder> {
  _$CoreEnum? _$v;

  AppErrorCodesEnum? _appErrorCodes;
  AppErrorCodesEnum? get appErrorCodes => _$this._appErrorCodes;
  set appErrorCodes(AppErrorCodesEnum? appErrorCodes) =>
      _$this._appErrorCodes = appErrorCodes;

  PermissionsEnum? _permissions;
  PermissionsEnum? get permissions => _$this._permissions;
  set permissions(PermissionsEnum? permissions) =>
      _$this._permissions = permissions;

  CoreEnumBuilder() {
    CoreEnum._defaults(this);
  }

  CoreEnumBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _appErrorCodes = $v.appErrorCodes;
      _permissions = $v.permissions;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CoreEnum other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$CoreEnum;
  }

  @override
  void update(void Function(CoreEnumBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  CoreEnum build() => _build();

  _$CoreEnum _build() {
    final _$result = _$v ??
        new _$CoreEnum._(
            appErrorCodes: BuiltValueNullFieldError.checkNotNull(
                appErrorCodes, r'CoreEnum', 'appErrorCodes'),
            permissions: BuiltValueNullFieldError.checkNotNull(
                permissions, r'CoreEnum', 'permissions'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
