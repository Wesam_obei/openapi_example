//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/success_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_municipalities_map200_response_all_of_data.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_municipalities_map200_response.g.dart';

/// My custom description
///
/// Properties:
/// * [success]
/// * [data]
@BuiltValue()
abstract class MapLayersControllerGetMunicipalitiesMap200Response
    implements
        SuccessResponse,
        Built<MapLayersControllerGetMunicipalitiesMap200Response,
            MapLayersControllerGetMunicipalitiesMap200ResponseBuilder> {
  MapLayersControllerGetMunicipalitiesMap200Response._();

  factory MapLayersControllerGetMunicipalitiesMap200Response(
          [void updates(
              MapLayersControllerGetMunicipalitiesMap200ResponseBuilder b)]) =
      _$MapLayersControllerGetMunicipalitiesMap200Response;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetMunicipalitiesMap200ResponseBuilder b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<MapLayersControllerGetMunicipalitiesMap200Response>
      get serializer =>
          _$MapLayersControllerGetMunicipalitiesMap200ResponseSerializer();
}

class _$MapLayersControllerGetMunicipalitiesMap200ResponseSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetMunicipalitiesMap200Response> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetMunicipalitiesMap200Response,
    _$MapLayersControllerGetMunicipalitiesMap200Response
  ];

  @override
  final String wireName = r'MapLayersControllerGetMunicipalitiesMap200Response';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetMunicipalitiesMap200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'data';
    yield serializers.serialize(
      object.data,
      specifiedType: const FullType(JsonObject),
    );
    yield r'success';
    yield serializers.serialize(
      object.success,
      specifiedType: const FullType(bool),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetMunicipalitiesMap200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetMunicipalitiesMap200ResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'data':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.data = valueDes;
          break;
        case r'success':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.success = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetMunicipalitiesMap200Response deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = MapLayersControllerGetMunicipalitiesMap200ResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
