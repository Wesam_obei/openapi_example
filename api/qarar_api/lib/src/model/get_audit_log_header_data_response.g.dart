// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_audit_log_header_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetAuditLogHeaderDataResponse extends GetAuditLogHeaderDataResponse {
  @override
  final String userAgent;
  @override
  final String referer;
  @override
  final String xLanguage;

  factory _$GetAuditLogHeaderDataResponse(
          [void Function(GetAuditLogHeaderDataResponseBuilder)? updates]) =>
      (new GetAuditLogHeaderDataResponseBuilder()..update(updates))._build();

  _$GetAuditLogHeaderDataResponse._(
      {required this.userAgent, required this.referer, required this.xLanguage})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        userAgent, r'GetAuditLogHeaderDataResponse', 'userAgent');
    BuiltValueNullFieldError.checkNotNull(
        referer, r'GetAuditLogHeaderDataResponse', 'referer');
    BuiltValueNullFieldError.checkNotNull(
        xLanguage, r'GetAuditLogHeaderDataResponse', 'xLanguage');
  }

  @override
  GetAuditLogHeaderDataResponse rebuild(
          void Function(GetAuditLogHeaderDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetAuditLogHeaderDataResponseBuilder toBuilder() =>
      new GetAuditLogHeaderDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetAuditLogHeaderDataResponse &&
        userAgent == other.userAgent &&
        referer == other.referer &&
        xLanguage == other.xLanguage;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, userAgent.hashCode);
    _$hash = $jc(_$hash, referer.hashCode);
    _$hash = $jc(_$hash, xLanguage.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetAuditLogHeaderDataResponse')
          ..add('userAgent', userAgent)
          ..add('referer', referer)
          ..add('xLanguage', xLanguage))
        .toString();
  }
}

class GetAuditLogHeaderDataResponseBuilder
    implements
        Builder<GetAuditLogHeaderDataResponse,
            GetAuditLogHeaderDataResponseBuilder> {
  _$GetAuditLogHeaderDataResponse? _$v;

  String? _userAgent;
  String? get userAgent => _$this._userAgent;
  set userAgent(String? userAgent) => _$this._userAgent = userAgent;

  String? _referer;
  String? get referer => _$this._referer;
  set referer(String? referer) => _$this._referer = referer;

  String? _xLanguage;
  String? get xLanguage => _$this._xLanguage;
  set xLanguage(String? xLanguage) => _$this._xLanguage = xLanguage;

  GetAuditLogHeaderDataResponseBuilder() {
    GetAuditLogHeaderDataResponse._defaults(this);
  }

  GetAuditLogHeaderDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _userAgent = $v.userAgent;
      _referer = $v.referer;
      _xLanguage = $v.xLanguage;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetAuditLogHeaderDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetAuditLogHeaderDataResponse;
  }

  @override
  void update(void Function(GetAuditLogHeaderDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetAuditLogHeaderDataResponse build() => _build();

  _$GetAuditLogHeaderDataResponse _build() {
    final _$result = _$v ??
        new _$GetAuditLogHeaderDataResponse._(
            userAgent: BuiltValueNullFieldError.checkNotNull(
                userAgent, r'GetAuditLogHeaderDataResponse', 'userAgent'),
            referer: BuiltValueNullFieldError.checkNotNull(
                referer, r'GetAuditLogHeaderDataResponse', 'referer'),
            xLanguage: BuiltValueNullFieldError.checkNotNull(
                xLanguage, r'GetAuditLogHeaderDataResponse', 'xLanguage'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
