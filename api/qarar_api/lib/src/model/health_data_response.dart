//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/json_object.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'health_data_response.g.dart';

/// HealthDataResponse
///
/// Properties:
/// * [status]
/// * [info]
/// * [error]
/// * [details]
@BuiltValue()
abstract class HealthDataResponse
    implements Built<HealthDataResponse, HealthDataResponseBuilder> {
  @BuiltValueField(wireName: r'status')
  String get status;

  @BuiltValueField(wireName: r'info')
  JsonObject get info;

  @BuiltValueField(wireName: r'error')
  JsonObject get error;

  @BuiltValueField(wireName: r'details')
  JsonObject get details;

  HealthDataResponse._();

  factory HealthDataResponse([void updates(HealthDataResponseBuilder b)]) =
      _$HealthDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(HealthDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<HealthDataResponse> get serializer =>
      _$HealthDataResponseSerializer();
}

class _$HealthDataResponseSerializer
    implements PrimitiveSerializer<HealthDataResponse> {
  @override
  final Iterable<Type> types = const [HealthDataResponse, _$HealthDataResponse];

  @override
  final String wireName = r'HealthDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    HealthDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'status';
    yield serializers.serialize(
      object.status,
      specifiedType: const FullType(String),
    );
    yield r'info';
    yield serializers.serialize(
      object.info,
      specifiedType: const FullType(JsonObject),
    );
    yield r'error';
    yield serializers.serialize(
      object.error,
      specifiedType: const FullType(JsonObject),
    );
    yield r'details';
    yield serializers.serialize(
      object.details,
      specifiedType: const FullType(JsonObject),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    HealthDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required HealthDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'status':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.status = valueDes;
          break;
        case r'info':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.info = valueDes;
          break;
        case r'error':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.error = valueDes;
          break;
        case r'details':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.details = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  HealthDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = HealthDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
