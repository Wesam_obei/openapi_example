//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/map_layers_controller_get_incident_reports_map200_response_all_of_data_features_inner_properties_id.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_district_boundaries_map200_response_all_of_data_features_inner_properties.g.dart';

/// MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
///
/// Properties:
/// * [id]
/// * [municipalityName]
/// * [municipalitySubName]
/// * [districtName]
/// * [districtEngName]
/// * [remarks]
/// * [areaName]
/// * [cityName]
@BuiltValue()
abstract class MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
    implements
        Built<
            MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties,
            MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder> {
  @BuiltValueField(wireName: r'id')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get id;

  @BuiltValueField(wireName: r'municipalityName')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get municipalityName;

  @BuiltValueField(wireName: r'municipalitySubName')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get municipalitySubName;

  @BuiltValueField(wireName: r'districtName')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get districtName;

  @BuiltValueField(wireName: r'districtEngName')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get districtEngName;

  @BuiltValueField(wireName: r'remarks')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get remarks;

  @BuiltValueField(wireName: r'areaName')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get areaName;

  @BuiltValueField(wireName: r'cityName')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get cityName;

  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties._();

  factory MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties(
          [void updates(
              MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
                  b)]) =
      _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
              b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties>
      get serializer =>
          _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesSerializer();
}

class _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties,
    _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
        object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.id != null) {
      yield r'id';
      yield serializers.serialize(
        object.id,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.municipalityName != null) {
      yield r'municipalityName';
      yield serializers.serialize(
        object.municipalityName,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.municipalitySubName != null) {
      yield r'municipalitySubName';
      yield serializers.serialize(
        object.municipalitySubName,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.districtName != null) {
      yield r'districtName';
      yield serializers.serialize(
        object.districtName,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.districtEngName != null) {
      yield r'districtEngName';
      yield serializers.serialize(
        object.districtEngName,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.remarks != null) {
      yield r'remarks';
      yield serializers.serialize(
        object.remarks,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.areaName != null) {
      yield r'areaName';
      yield serializers.serialize(
        object.areaName,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.cityName != null) {
      yield r'cityName';
      yield serializers.serialize(
        object.cityName,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
        object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.id.replace(valueDes);
          break;
        case r'municipalityName':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.municipalityName.replace(valueDes);
          break;
        case r'municipalitySubName':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.municipalitySubName.replace(valueDes);
          break;
        case r'districtName':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.districtName.replace(valueDes);
          break;
        case r'districtEngName':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.districtEngName.replace(valueDes);
          break;
        case r'remarks':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.remarks.replace(valueDes);
          break;
        case r'areaName':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.areaName.replace(valueDes);
          break;
        case r'cityName':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.cityName.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
      deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
