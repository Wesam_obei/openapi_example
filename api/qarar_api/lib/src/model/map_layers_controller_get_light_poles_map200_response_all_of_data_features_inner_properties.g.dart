// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_light_poles_map200_response_all_of_data_features_inner_properties.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerProperties
    extends MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerProperties {
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      id;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      cityName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      streetName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      cableId;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      enabled;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      poleId;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      poleInstallationDate;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      poleEnabled;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      poleLampsCount;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      poleMaterialType;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      poleHeight;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      poleLampType;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      poleUsage;

  factory _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerProperties(
          [void Function(
                  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
              updates]) =>
      (new MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerProperties._(
      {this.id,
      this.cityName,
      this.streetName,
      this.cableId,
      this.enabled,
      this.poleId,
      this.poleInstallationDate,
      this.poleEnabled,
      this.poleLampsCount,
      this.poleMaterialType,
      this.poleHeight,
      this.poleLampType,
      this.poleUsage})
      : super._();

  @override
  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerProperties
      rebuild(
              void Function(
                      MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)
                  updates) =>
          (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      toBuilder() =>
          new MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerProperties &&
        id == other.id &&
        cityName == other.cityName &&
        streetName == other.streetName &&
        cableId == other.cableId &&
        enabled == other.enabled &&
        poleId == other.poleId &&
        poleInstallationDate == other.poleInstallationDate &&
        poleEnabled == other.poleEnabled &&
        poleLampsCount == other.poleLampsCount &&
        poleMaterialType == other.poleMaterialType &&
        poleHeight == other.poleHeight &&
        poleLampType == other.poleLampType &&
        poleUsage == other.poleUsage;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, cityName.hashCode);
    _$hash = $jc(_$hash, streetName.hashCode);
    _$hash = $jc(_$hash, cableId.hashCode);
    _$hash = $jc(_$hash, enabled.hashCode);
    _$hash = $jc(_$hash, poleId.hashCode);
    _$hash = $jc(_$hash, poleInstallationDate.hashCode);
    _$hash = $jc(_$hash, poleEnabled.hashCode);
    _$hash = $jc(_$hash, poleLampsCount.hashCode);
    _$hash = $jc(_$hash, poleMaterialType.hashCode);
    _$hash = $jc(_$hash, poleHeight.hashCode);
    _$hash = $jc(_$hash, poleLampType.hashCode);
    _$hash = $jc(_$hash, poleUsage.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerProperties')
          ..add('id', id)
          ..add('cityName', cityName)
          ..add('streetName', streetName)
          ..add('cableId', cableId)
          ..add('enabled', enabled)
          ..add('poleId', poleId)
          ..add('poleInstallationDate', poleInstallationDate)
          ..add('poleEnabled', poleEnabled)
          ..add('poleLampsCount', poleLampsCount)
          ..add('poleMaterialType', poleMaterialType)
          ..add('poleHeight', poleHeight)
          ..add('poleLampType', poleLampType)
          ..add('poleUsage', poleUsage))
        .toString();
  }
}

class MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
    implements
        Builder<
            MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerProperties,
            MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder> {
  _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerProperties?
      _$v;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _id;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get id => _$this._id ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set id(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              id) =>
      _$this._id = id;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _cityName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get cityName => _$this._cityName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set cityName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              cityName) =>
      _$this._cityName = cityName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _streetName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get streetName => _$this._streetName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set streetName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              streetName) =>
      _$this._streetName = streetName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _cableId;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get cableId => _$this._cableId ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set cableId(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              cableId) =>
      _$this._cableId = cableId;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _enabled;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get enabled => _$this._enabled ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set enabled(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              enabled) =>
      _$this._enabled = enabled;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _poleId;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get poleId => _$this._poleId ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set poleId(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              poleId) =>
      _$this._poleId = poleId;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _poleInstallationDate;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get poleInstallationDate => _$this._poleInstallationDate ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set poleInstallationDate(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              poleInstallationDate) =>
      _$this._poleInstallationDate = poleInstallationDate;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _poleEnabled;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get poleEnabled => _$this._poleEnabled ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set poleEnabled(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              poleEnabled) =>
      _$this._poleEnabled = poleEnabled;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _poleLampsCount;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get poleLampsCount => _$this._poleLampsCount ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set poleLampsCount(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              poleLampsCount) =>
      _$this._poleLampsCount = poleLampsCount;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _poleMaterialType;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get poleMaterialType => _$this._poleMaterialType ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set poleMaterialType(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              poleMaterialType) =>
      _$this._poleMaterialType = poleMaterialType;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _poleHeight;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get poleHeight => _$this._poleHeight ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set poleHeight(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              poleHeight) =>
      _$this._poleHeight = poleHeight;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _poleLampType;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get poleLampType => _$this._poleLampType ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set poleLampType(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              poleLampType) =>
      _$this._poleLampType = poleLampType;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _poleUsage;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get poleUsage => _$this._poleUsage ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set poleUsage(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              poleUsage) =>
      _$this._poleUsage = poleUsage;

  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder() {
    MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerProperties
        ._defaults(this);
  }

  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id?.toBuilder();
      _cityName = $v.cityName?.toBuilder();
      _streetName = $v.streetName?.toBuilder();
      _cableId = $v.cableId?.toBuilder();
      _enabled = $v.enabled?.toBuilder();
      _poleId = $v.poleId?.toBuilder();
      _poleInstallationDate = $v.poleInstallationDate?.toBuilder();
      _poleEnabled = $v.poleEnabled?.toBuilder();
      _poleLampsCount = $v.poleLampsCount?.toBuilder();
      _poleMaterialType = $v.poleMaterialType?.toBuilder();
      _poleHeight = $v.poleHeight?.toBuilder();
      _poleLampType = $v.poleLampType?.toBuilder();
      _poleUsage = $v.poleUsage?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerProperties
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerProperties;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerProperties
      build() => _build();

  _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerProperties
      _build() {
    _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerProperties
        _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerProperties
              ._(
              id: _id?.build(),
              cityName: _cityName?.build(),
              streetName: _streetName?.build(),
              cableId: _cableId?.build(),
              enabled: _enabled?.build(),
              poleId: _poleId?.build(),
              poleInstallationDate: _poleInstallationDate?.build(),
              poleEnabled: _poleEnabled?.build(),
              poleLampsCount: _poleLampsCount?.build(),
              poleMaterialType: _poleMaterialType?.build(),
              poleHeight: _poleHeight?.build(),
              poleLampType: _poleLampType?.build(),
              poleUsage: _poleUsage?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'id';
        _id?.build();
        _$failedField = 'cityName';
        _cityName?.build();
        _$failedField = 'streetName';
        _streetName?.build();
        _$failedField = 'cableId';
        _cableId?.build();
        _$failedField = 'enabled';
        _enabled?.build();
        _$failedField = 'poleId';
        _poleId?.build();
        _$failedField = 'poleInstallationDate';
        _poleInstallationDate?.build();
        _$failedField = 'poleEnabled';
        _poleEnabled?.build();
        _$failedField = 'poleLampsCount';
        _poleLampsCount?.build();
        _$failedField = 'poleMaterialType';
        _poleMaterialType?.build();
        _$failedField = 'poleHeight';
        _poleHeight?.build();
        _$failedField = 'poleLampType';
        _poleLampType?.build();
        _$failedField = 'poleUsage';
        _poleUsage?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerProperties',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
