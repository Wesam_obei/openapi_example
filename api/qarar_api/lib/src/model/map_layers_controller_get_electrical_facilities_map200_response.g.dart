// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_electrical_facilities_map200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetElectricalFacilitiesMap200Response
    extends MapLayersControllerGetElectricalFacilitiesMap200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$MapLayersControllerGetElectricalFacilitiesMap200Response(
          [void Function(
                  MapLayersControllerGetElectricalFacilitiesMap200ResponseBuilder)?
              updates]) =>
      (new MapLayersControllerGetElectricalFacilitiesMap200ResponseBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetElectricalFacilitiesMap200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(success,
        r'MapLayersControllerGetElectricalFacilitiesMap200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(data,
        r'MapLayersControllerGetElectricalFacilitiesMap200Response', 'data');
  }

  @override
  MapLayersControllerGetElectricalFacilitiesMap200Response rebuild(
          void Function(
                  MapLayersControllerGetElectricalFacilitiesMap200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetElectricalFacilitiesMap200ResponseBuilder toBuilder() =>
      new MapLayersControllerGetElectricalFacilitiesMap200ResponseBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapLayersControllerGetElectricalFacilitiesMap200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetElectricalFacilitiesMap200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class MapLayersControllerGetElectricalFacilitiesMap200ResponseBuilder
    implements
        Builder<MapLayersControllerGetElectricalFacilitiesMap200Response,
            MapLayersControllerGetElectricalFacilitiesMap200ResponseBuilder>,
        SuccessResponseBuilder {
  _$MapLayersControllerGetElectricalFacilitiesMap200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  MapLayersControllerGetElectricalFacilitiesMap200ResponseBuilder() {
    MapLayersControllerGetElectricalFacilitiesMap200Response._defaults(this);
  }

  MapLayersControllerGetElectricalFacilitiesMap200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      covariant MapLayersControllerGetElectricalFacilitiesMap200Response
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapLayersControllerGetElectricalFacilitiesMap200Response;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetElectricalFacilitiesMap200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetElectricalFacilitiesMap200Response build() => _build();

  _$MapLayersControllerGetElectricalFacilitiesMap200Response _build() {
    final _$result = _$v ??
        new _$MapLayersControllerGetElectricalFacilitiesMap200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success,
                r'MapLayersControllerGetElectricalFacilitiesMap200Response',
                'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data,
                r'MapLayersControllerGetElectricalFacilitiesMap200Response',
                'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
