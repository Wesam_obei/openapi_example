//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'plan_update_request.g.dart';

/// PlanUpdateRequest
///
/// Properties:
/// * [name]
/// * [description]
@BuiltValue()
abstract class PlanUpdateRequest
    implements Built<PlanUpdateRequest, PlanUpdateRequestBuilder> {
  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'description')
  String get description;

  PlanUpdateRequest._();

  factory PlanUpdateRequest([void updates(PlanUpdateRequestBuilder b)]) =
      _$PlanUpdateRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(PlanUpdateRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<PlanUpdateRequest> get serializer =>
      _$PlanUpdateRequestSerializer();
}

class _$PlanUpdateRequestSerializer
    implements PrimitiveSerializer<PlanUpdateRequest> {
  @override
  final Iterable<Type> types = const [PlanUpdateRequest, _$PlanUpdateRequest];

  @override
  final String wireName = r'PlanUpdateRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    PlanUpdateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    yield r'description';
    yield serializers.serialize(
      object.description,
      specifiedType: const FullType(String),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    PlanUpdateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required PlanUpdateRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'description':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.description = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  PlanUpdateRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = PlanUpdateRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
