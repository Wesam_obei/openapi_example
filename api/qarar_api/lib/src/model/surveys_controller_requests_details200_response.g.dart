// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'surveys_controller_requests_details200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SurveysControllerRequestsDetails200Response
    extends SurveysControllerRequestsDetails200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$SurveysControllerRequestsDetails200Response(
          [void Function(SurveysControllerRequestsDetails200ResponseBuilder)?
              updates]) =>
      (new SurveysControllerRequestsDetails200ResponseBuilder()
            ..update(updates))
          ._build();

  _$SurveysControllerRequestsDetails200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'SurveysControllerRequestsDetails200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'SurveysControllerRequestsDetails200Response', 'data');
  }

  @override
  SurveysControllerRequestsDetails200Response rebuild(
          void Function(SurveysControllerRequestsDetails200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SurveysControllerRequestsDetails200ResponseBuilder toBuilder() =>
      new SurveysControllerRequestsDetails200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SurveysControllerRequestsDetails200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'SurveysControllerRequestsDetails200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class SurveysControllerRequestsDetails200ResponseBuilder
    implements
        Builder<SurveysControllerRequestsDetails200Response,
            SurveysControllerRequestsDetails200ResponseBuilder>,
        SuccessResponseBuilder {
  _$SurveysControllerRequestsDetails200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  SurveysControllerRequestsDetails200ResponseBuilder() {
    SurveysControllerRequestsDetails200Response._defaults(this);
  }

  SurveysControllerRequestsDetails200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant SurveysControllerRequestsDetails200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SurveysControllerRequestsDetails200Response;
  }

  @override
  void update(
      void Function(SurveysControllerRequestsDetails200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  SurveysControllerRequestsDetails200Response build() => _build();

  _$SurveysControllerRequestsDetails200Response _build() {
    final _$result = _$v ??
        new _$SurveysControllerRequestsDetails200Response._(
            success: BuiltValueNullFieldError.checkNotNull(success,
                r'SurveysControllerRequestsDetails200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'SurveysControllerRequestsDetails200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
