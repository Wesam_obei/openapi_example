// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'users_controller_list200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$UsersControllerList200Response extends UsersControllerList200Response {
  @override
  final bool success;
  @override
  final BuiltList<JsonObject> data;
  @override
  final PaginatedMetaDocumented meta;
  @override
  final PaginatedLinksDocumented links;

  factory _$UsersControllerList200Response(
          [void Function(UsersControllerList200ResponseBuilder)? updates]) =>
      (new UsersControllerList200ResponseBuilder()..update(updates))._build();

  _$UsersControllerList200Response._(
      {required this.success,
      required this.data,
      required this.meta,
      required this.links})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'UsersControllerList200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'UsersControllerList200Response', 'data');
    BuiltValueNullFieldError.checkNotNull(
        meta, r'UsersControllerList200Response', 'meta');
    BuiltValueNullFieldError.checkNotNull(
        links, r'UsersControllerList200Response', 'links');
  }

  @override
  UsersControllerList200Response rebuild(
          void Function(UsersControllerList200ResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UsersControllerList200ResponseBuilder toBuilder() =>
      new UsersControllerList200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UsersControllerList200Response &&
        success == other.success &&
        data == other.data &&
        meta == other.meta &&
        links == other.links;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jc(_$hash, meta.hashCode);
    _$hash = $jc(_$hash, links.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'UsersControllerList200Response')
          ..add('success', success)
          ..add('data', data)
          ..add('meta', meta)
          ..add('links', links))
        .toString();
  }
}

class UsersControllerList200ResponseBuilder
    implements
        Builder<UsersControllerList200Response,
            UsersControllerList200ResponseBuilder>,
        PaginatedDocumentedBuilder {
  _$UsersControllerList200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  ListBuilder<JsonObject>? _data;
  ListBuilder<JsonObject> get data =>
      _$this._data ??= new ListBuilder<JsonObject>();
  set data(covariant ListBuilder<JsonObject>? data) => _$this._data = data;

  PaginatedMetaDocumentedBuilder? _meta;
  PaginatedMetaDocumentedBuilder get meta =>
      _$this._meta ??= new PaginatedMetaDocumentedBuilder();
  set meta(covariant PaginatedMetaDocumentedBuilder? meta) =>
      _$this._meta = meta;

  PaginatedLinksDocumentedBuilder? _links;
  PaginatedLinksDocumentedBuilder get links =>
      _$this._links ??= new PaginatedLinksDocumentedBuilder();
  set links(covariant PaginatedLinksDocumentedBuilder? links) =>
      _$this._links = links;

  UsersControllerList200ResponseBuilder() {
    UsersControllerList200Response._defaults(this);
  }

  UsersControllerList200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data.toBuilder();
      _meta = $v.meta.toBuilder();
      _links = $v.links.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant UsersControllerList200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$UsersControllerList200Response;
  }

  @override
  void update(void Function(UsersControllerList200ResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  UsersControllerList200Response build() => _build();

  _$UsersControllerList200Response _build() {
    _$UsersControllerList200Response _$result;
    try {
      _$result = _$v ??
          new _$UsersControllerList200Response._(
              success: BuiltValueNullFieldError.checkNotNull(
                  success, r'UsersControllerList200Response', 'success'),
              data: data.build(),
              meta: meta.build(),
              links: links.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'data';
        data.build();
        _$failedField = 'meta';
        meta.build();
        _$failedField = 'links';
        links.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'UsersControllerList200Response', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
