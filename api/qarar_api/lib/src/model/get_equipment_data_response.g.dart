// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_equipment_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetEquipmentDataResponse extends GetEquipmentDataResponse {
  @override
  final num id;
  @override
  final String brand;
  @override
  final String model;
  @override
  final String plateNumber;
  @override
  final String vinNumber;
  @override
  final num localNumber;
  @override
  final num serialNumber;
  @override
  final num manufacturingYear;
  @override
  final DateTime registrationExpiryDate;
  @override
  final DateTime ownershipDate;
  @override
  final DateTime createdAt;
  @override
  final GetEntityDataResponse entity;
  @override
  final GetEquipmentTypeDataResponse equipmentsType;
  @override
  final GetEquipmentStorePointDataResponse equipmentsStorePoint;

  factory _$GetEquipmentDataResponse(
          [void Function(GetEquipmentDataResponseBuilder)? updates]) =>
      (new GetEquipmentDataResponseBuilder()..update(updates))._build();

  _$GetEquipmentDataResponse._(
      {required this.id,
      required this.brand,
      required this.model,
      required this.plateNumber,
      required this.vinNumber,
      required this.localNumber,
      required this.serialNumber,
      required this.manufacturingYear,
      required this.registrationExpiryDate,
      required this.ownershipDate,
      required this.createdAt,
      required this.entity,
      required this.equipmentsType,
      required this.equipmentsStorePoint})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'GetEquipmentDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        brand, r'GetEquipmentDataResponse', 'brand');
    BuiltValueNullFieldError.checkNotNull(
        model, r'GetEquipmentDataResponse', 'model');
    BuiltValueNullFieldError.checkNotNull(
        plateNumber, r'GetEquipmentDataResponse', 'plateNumber');
    BuiltValueNullFieldError.checkNotNull(
        vinNumber, r'GetEquipmentDataResponse', 'vinNumber');
    BuiltValueNullFieldError.checkNotNull(
        localNumber, r'GetEquipmentDataResponse', 'localNumber');
    BuiltValueNullFieldError.checkNotNull(
        serialNumber, r'GetEquipmentDataResponse', 'serialNumber');
    BuiltValueNullFieldError.checkNotNull(
        manufacturingYear, r'GetEquipmentDataResponse', 'manufacturingYear');
    BuiltValueNullFieldError.checkNotNull(registrationExpiryDate,
        r'GetEquipmentDataResponse', 'registrationExpiryDate');
    BuiltValueNullFieldError.checkNotNull(
        ownershipDate, r'GetEquipmentDataResponse', 'ownershipDate');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'GetEquipmentDataResponse', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        entity, r'GetEquipmentDataResponse', 'entity');
    BuiltValueNullFieldError.checkNotNull(
        equipmentsType, r'GetEquipmentDataResponse', 'equipmentsType');
    BuiltValueNullFieldError.checkNotNull(equipmentsStorePoint,
        r'GetEquipmentDataResponse', 'equipmentsStorePoint');
  }

  @override
  GetEquipmentDataResponse rebuild(
          void Function(GetEquipmentDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetEquipmentDataResponseBuilder toBuilder() =>
      new GetEquipmentDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetEquipmentDataResponse &&
        id == other.id &&
        brand == other.brand &&
        model == other.model &&
        plateNumber == other.plateNumber &&
        vinNumber == other.vinNumber &&
        localNumber == other.localNumber &&
        serialNumber == other.serialNumber &&
        manufacturingYear == other.manufacturingYear &&
        registrationExpiryDate == other.registrationExpiryDate &&
        ownershipDate == other.ownershipDate &&
        createdAt == other.createdAt &&
        entity == other.entity &&
        equipmentsType == other.equipmentsType &&
        equipmentsStorePoint == other.equipmentsStorePoint;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, brand.hashCode);
    _$hash = $jc(_$hash, model.hashCode);
    _$hash = $jc(_$hash, plateNumber.hashCode);
    _$hash = $jc(_$hash, vinNumber.hashCode);
    _$hash = $jc(_$hash, localNumber.hashCode);
    _$hash = $jc(_$hash, serialNumber.hashCode);
    _$hash = $jc(_$hash, manufacturingYear.hashCode);
    _$hash = $jc(_$hash, registrationExpiryDate.hashCode);
    _$hash = $jc(_$hash, ownershipDate.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, entity.hashCode);
    _$hash = $jc(_$hash, equipmentsType.hashCode);
    _$hash = $jc(_$hash, equipmentsStorePoint.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetEquipmentDataResponse')
          ..add('id', id)
          ..add('brand', brand)
          ..add('model', model)
          ..add('plateNumber', plateNumber)
          ..add('vinNumber', vinNumber)
          ..add('localNumber', localNumber)
          ..add('serialNumber', serialNumber)
          ..add('manufacturingYear', manufacturingYear)
          ..add('registrationExpiryDate', registrationExpiryDate)
          ..add('ownershipDate', ownershipDate)
          ..add('createdAt', createdAt)
          ..add('entity', entity)
          ..add('equipmentsType', equipmentsType)
          ..add('equipmentsStorePoint', equipmentsStorePoint))
        .toString();
  }
}

class GetEquipmentDataResponseBuilder
    implements
        Builder<GetEquipmentDataResponse, GetEquipmentDataResponseBuilder> {
  _$GetEquipmentDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _brand;
  String? get brand => _$this._brand;
  set brand(String? brand) => _$this._brand = brand;

  String? _model;
  String? get model => _$this._model;
  set model(String? model) => _$this._model = model;

  String? _plateNumber;
  String? get plateNumber => _$this._plateNumber;
  set plateNumber(String? plateNumber) => _$this._plateNumber = plateNumber;

  String? _vinNumber;
  String? get vinNumber => _$this._vinNumber;
  set vinNumber(String? vinNumber) => _$this._vinNumber = vinNumber;

  num? _localNumber;
  num? get localNumber => _$this._localNumber;
  set localNumber(num? localNumber) => _$this._localNumber = localNumber;

  num? _serialNumber;
  num? get serialNumber => _$this._serialNumber;
  set serialNumber(num? serialNumber) => _$this._serialNumber = serialNumber;

  num? _manufacturingYear;
  num? get manufacturingYear => _$this._manufacturingYear;
  set manufacturingYear(num? manufacturingYear) =>
      _$this._manufacturingYear = manufacturingYear;

  DateTime? _registrationExpiryDate;
  DateTime? get registrationExpiryDate => _$this._registrationExpiryDate;
  set registrationExpiryDate(DateTime? registrationExpiryDate) =>
      _$this._registrationExpiryDate = registrationExpiryDate;

  DateTime? _ownershipDate;
  DateTime? get ownershipDate => _$this._ownershipDate;
  set ownershipDate(DateTime? ownershipDate) =>
      _$this._ownershipDate = ownershipDate;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  GetEntityDataResponseBuilder? _entity;
  GetEntityDataResponseBuilder get entity =>
      _$this._entity ??= new GetEntityDataResponseBuilder();
  set entity(GetEntityDataResponseBuilder? entity) => _$this._entity = entity;

  GetEquipmentTypeDataResponseBuilder? _equipmentsType;
  GetEquipmentTypeDataResponseBuilder get equipmentsType =>
      _$this._equipmentsType ??= new GetEquipmentTypeDataResponseBuilder();
  set equipmentsType(GetEquipmentTypeDataResponseBuilder? equipmentsType) =>
      _$this._equipmentsType = equipmentsType;

  GetEquipmentStorePointDataResponseBuilder? _equipmentsStorePoint;
  GetEquipmentStorePointDataResponseBuilder get equipmentsStorePoint =>
      _$this._equipmentsStorePoint ??=
          new GetEquipmentStorePointDataResponseBuilder();
  set equipmentsStorePoint(
          GetEquipmentStorePointDataResponseBuilder? equipmentsStorePoint) =>
      _$this._equipmentsStorePoint = equipmentsStorePoint;

  GetEquipmentDataResponseBuilder() {
    GetEquipmentDataResponse._defaults(this);
  }

  GetEquipmentDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _brand = $v.brand;
      _model = $v.model;
      _plateNumber = $v.plateNumber;
      _vinNumber = $v.vinNumber;
      _localNumber = $v.localNumber;
      _serialNumber = $v.serialNumber;
      _manufacturingYear = $v.manufacturingYear;
      _registrationExpiryDate = $v.registrationExpiryDate;
      _ownershipDate = $v.ownershipDate;
      _createdAt = $v.createdAt;
      _entity = $v.entity.toBuilder();
      _equipmentsType = $v.equipmentsType.toBuilder();
      _equipmentsStorePoint = $v.equipmentsStorePoint.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetEquipmentDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetEquipmentDataResponse;
  }

  @override
  void update(void Function(GetEquipmentDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetEquipmentDataResponse build() => _build();

  _$GetEquipmentDataResponse _build() {
    _$GetEquipmentDataResponse _$result;
    try {
      _$result = _$v ??
          new _$GetEquipmentDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'GetEquipmentDataResponse', 'id'),
              brand: BuiltValueNullFieldError.checkNotNull(
                  brand, r'GetEquipmentDataResponse', 'brand'),
              model: BuiltValueNullFieldError.checkNotNull(
                  model, r'GetEquipmentDataResponse', 'model'),
              plateNumber: BuiltValueNullFieldError.checkNotNull(
                  plateNumber, r'GetEquipmentDataResponse', 'plateNumber'),
              vinNumber: BuiltValueNullFieldError.checkNotNull(
                  vinNumber, r'GetEquipmentDataResponse', 'vinNumber'),
              localNumber: BuiltValueNullFieldError.checkNotNull(
                  localNumber, r'GetEquipmentDataResponse', 'localNumber'),
              serialNumber: BuiltValueNullFieldError.checkNotNull(
                  serialNumber, r'GetEquipmentDataResponse', 'serialNumber'),
              manufacturingYear: BuiltValueNullFieldError.checkNotNull(
                  manufacturingYear, r'GetEquipmentDataResponse', 'manufacturingYear'),
              registrationExpiryDate:
                  BuiltValueNullFieldError.checkNotNull(registrationExpiryDate, r'GetEquipmentDataResponse', 'registrationExpiryDate'),
              ownershipDate: BuiltValueNullFieldError.checkNotNull(ownershipDate, r'GetEquipmentDataResponse', 'ownershipDate'),
              createdAt: BuiltValueNullFieldError.checkNotNull(createdAt, r'GetEquipmentDataResponse', 'createdAt'),
              entity: entity.build(),
              equipmentsType: equipmentsType.build(),
              equipmentsStorePoint: equipmentsStorePoint.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'entity';
        entity.build();
        _$failedField = 'equipmentsType';
        equipmentsType.build();
        _$failedField = 'equipmentsStorePoint';
        equipmentsStorePoint.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'GetEquipmentDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
