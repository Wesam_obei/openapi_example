// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_role_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ListRoleDataResponse extends ListRoleDataResponse {
  @override
  final num id;
  @override
  final String name;
  @override
  final String description;
  @override
  final DateTime createdAt;
  @override
  final BuiltList<PermissionDataResponse> permissions;

  factory _$ListRoleDataResponse(
          [void Function(ListRoleDataResponseBuilder)? updates]) =>
      (new ListRoleDataResponseBuilder()..update(updates))._build();

  _$ListRoleDataResponse._(
      {required this.id,
      required this.name,
      required this.description,
      required this.createdAt,
      required this.permissions})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, r'ListRoleDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, r'ListRoleDataResponse', 'name');
    BuiltValueNullFieldError.checkNotNull(
        description, r'ListRoleDataResponse', 'description');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'ListRoleDataResponse', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        permissions, r'ListRoleDataResponse', 'permissions');
  }

  @override
  ListRoleDataResponse rebuild(
          void Function(ListRoleDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ListRoleDataResponseBuilder toBuilder() =>
      new ListRoleDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ListRoleDataResponse &&
        id == other.id &&
        name == other.name &&
        description == other.description &&
        createdAt == other.createdAt &&
        permissions == other.permissions;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, permissions.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ListRoleDataResponse')
          ..add('id', id)
          ..add('name', name)
          ..add('description', description)
          ..add('createdAt', createdAt)
          ..add('permissions', permissions))
        .toString();
  }
}

class ListRoleDataResponseBuilder
    implements Builder<ListRoleDataResponse, ListRoleDataResponseBuilder> {
  _$ListRoleDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  ListBuilder<PermissionDataResponse>? _permissions;
  ListBuilder<PermissionDataResponse> get permissions =>
      _$this._permissions ??= new ListBuilder<PermissionDataResponse>();
  set permissions(ListBuilder<PermissionDataResponse>? permissions) =>
      _$this._permissions = permissions;

  ListRoleDataResponseBuilder() {
    ListRoleDataResponse._defaults(this);
  }

  ListRoleDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _description = $v.description;
      _createdAt = $v.createdAt;
      _permissions = $v.permissions.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ListRoleDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ListRoleDataResponse;
  }

  @override
  void update(void Function(ListRoleDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ListRoleDataResponse build() => _build();

  _$ListRoleDataResponse _build() {
    _$ListRoleDataResponse _$result;
    try {
      _$result = _$v ??
          new _$ListRoleDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'ListRoleDataResponse', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'ListRoleDataResponse', 'name'),
              description: BuiltValueNullFieldError.checkNotNull(
                  description, r'ListRoleDataResponse', 'description'),
              createdAt: BuiltValueNullFieldError.checkNotNull(
                  createdAt, r'ListRoleDataResponse', 'createdAt'),
              permissions: permissions.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'permissions';
        permissions.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ListRoleDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
