//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/paginated_links_documented.dart';
import 'package:qarar_api/src/model/paginated_meta_documented.dart';
import 'package:qarar_api/src/model/paginated_documented.dart';
import 'package:qarar_api/src/model/list_user_activity_log_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'activity_log_controller_user_activity200_response.g.dart';

/// ActivityLogControllerUserActivity200Response
///
/// Properties:
/// * [success]
/// * [data]
/// * [meta]
/// * [links]
@BuiltValue()
abstract class ActivityLogControllerUserActivity200Response
    implements
        PaginatedDocumented,
        Built<ActivityLogControllerUserActivity200Response,
            ActivityLogControllerUserActivity200ResponseBuilder> {
  ActivityLogControllerUserActivity200Response._();

  factory ActivityLogControllerUserActivity200Response(
          [void updates(
              ActivityLogControllerUserActivity200ResponseBuilder b)]) =
      _$ActivityLogControllerUserActivity200Response;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          ActivityLogControllerUserActivity200ResponseBuilder b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ActivityLogControllerUserActivity200Response>
      get serializer =>
          _$ActivityLogControllerUserActivity200ResponseSerializer();
}

class _$ActivityLogControllerUserActivity200ResponseSerializer
    implements
        PrimitiveSerializer<ActivityLogControllerUserActivity200Response> {
  @override
  final Iterable<Type> types = const [
    ActivityLogControllerUserActivity200Response,
    _$ActivityLogControllerUserActivity200Response
  ];

  @override
  final String wireName = r'ActivityLogControllerUserActivity200Response';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ActivityLogControllerUserActivity200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'links';
    yield serializers.serialize(
      object.links,
      specifiedType: const FullType(PaginatedLinksDocumented),
    );
    yield r'data';
    yield serializers.serialize(
      object.data,
      specifiedType: const FullType(BuiltList, [FullType(JsonObject)]),
    );
    yield r'success';
    yield serializers.serialize(
      object.success,
      specifiedType: const FullType(bool),
    );
    yield r'meta';
    yield serializers.serialize(
      object.meta,
      specifiedType: const FullType(PaginatedMetaDocumented),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ActivityLogControllerUserActivity200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ActivityLogControllerUserActivity200ResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'links':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(PaginatedLinksDocumented),
          ) as PaginatedLinksDocumented;
          result.links.replace(valueDes);
          break;
        case r'data':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(JsonObject)]),
          ) as BuiltList<JsonObject>;
          result.data.replace(valueDes);
          break;
        case r'success':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.success = valueDes;
          break;
        case r'meta':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(PaginatedMetaDocumented),
          ) as PaginatedMetaDocumented;
          result.meta.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ActivityLogControllerUserActivity200Response deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ActivityLogControllerUserActivity200ResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
