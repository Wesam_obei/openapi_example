// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'equipments_controller_get200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$EquipmentsControllerGet200Response
    extends EquipmentsControllerGet200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$EquipmentsControllerGet200Response(
          [void Function(EquipmentsControllerGet200ResponseBuilder)?
              updates]) =>
      (new EquipmentsControllerGet200ResponseBuilder()..update(updates))
          ._build();

  _$EquipmentsControllerGet200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'EquipmentsControllerGet200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'EquipmentsControllerGet200Response', 'data');
  }

  @override
  EquipmentsControllerGet200Response rebuild(
          void Function(EquipmentsControllerGet200ResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  EquipmentsControllerGet200ResponseBuilder toBuilder() =>
      new EquipmentsControllerGet200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EquipmentsControllerGet200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'EquipmentsControllerGet200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class EquipmentsControllerGet200ResponseBuilder
    implements
        Builder<EquipmentsControllerGet200Response,
            EquipmentsControllerGet200ResponseBuilder>,
        SuccessResponseBuilder {
  _$EquipmentsControllerGet200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  EquipmentsControllerGet200ResponseBuilder() {
    EquipmentsControllerGet200Response._defaults(this);
  }

  EquipmentsControllerGet200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant EquipmentsControllerGet200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$EquipmentsControllerGet200Response;
  }

  @override
  void update(
      void Function(EquipmentsControllerGet200ResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  EquipmentsControllerGet200Response build() => _build();

  _$EquipmentsControllerGet200Response _build() {
    final _$result = _$v ??
        new _$EquipmentsControllerGet200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success, r'EquipmentsControllerGet200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'EquipmentsControllerGet200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
