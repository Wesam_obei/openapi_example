//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_municipalities_map200_response_all_of_data_features_inner.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_municipalities_map200_response_all_of_data.g.dart';

/// MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData
///
/// Properties:
/// * [type]
/// * [features]
@BuiltValue()
abstract class MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData
    implements
        Built<MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData,
            MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataBuilder> {
  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum get type;
  // enum typeEnum {  FeatureCollection,  };

  @BuiltValueField(wireName: r'features')
  BuiltList<
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner>
      get features;

  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData._();

  factory MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData(
          [void updates(
              MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataBuilder
                  b)]) =
      _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataBuilder
              b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData>
      get serializer =>
          _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataSerializer();
}

class _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData,
    _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum),
    );
    yield r'features';
    yield serializers.serialize(
      object.features,
      specifiedType: const FullType(BuiltList, [
        FullType(
            MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner)
      ]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum),
          ) as MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum;
          result.type = valueDes;
          break;
        case r'features':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [
              FullType(
                  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner)
            ]),
          ) as BuiltList<
              MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner>;
          result.features.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum
    extends EnumClass {
  @BuiltValueEnumConst(wireName: r'FeatureCollection', fallback: true)
  static const MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum
      featureCollection =
      _$mapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum_featureCollection;

  static Serializer<
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum>
      get serializer =>
          _$mapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnumSerializer;

  const MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum._(
      String name)
      : super(name);

  static BuiltSet<
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum>
      get values =>
          _$mapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnumValues;
  static MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum
      valueOf(String name) =>
          _$mapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnumValueOf(
              name);
}
