//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/json_object.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'answer_request.g.dart';

/// AnswerRequest
///
/// Properties:
/// * [order]
/// * [formComponentId]
/// * [value]
/// * [id]
@BuiltValue()
abstract class AnswerRequest
    implements Built<AnswerRequest, AnswerRequestBuilder> {
  @BuiltValueField(wireName: r'order')
  num get order;

  @BuiltValueField(wireName: r'form_component_id')
  num get formComponentId;

  @BuiltValueField(wireName: r'value')
  JsonObject get value;

  @BuiltValueField(wireName: r'id')
  num? get id;

  AnswerRequest._();

  factory AnswerRequest([void updates(AnswerRequestBuilder b)]) =
      _$AnswerRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(AnswerRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<AnswerRequest> get serializer =>
      _$AnswerRequestSerializer();
}

class _$AnswerRequestSerializer implements PrimitiveSerializer<AnswerRequest> {
  @override
  final Iterable<Type> types = const [AnswerRequest, _$AnswerRequest];

  @override
  final String wireName = r'AnswerRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    AnswerRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'order';
    yield serializers.serialize(
      object.order,
      specifiedType: const FullType(num),
    );
    yield r'form_component_id';
    yield serializers.serialize(
      object.formComponentId,
      specifiedType: const FullType(num),
    );
    yield r'value';
    yield serializers.serialize(
      object.value,
      specifiedType: const FullType(JsonObject),
    );
    if (object.id != null) {
      yield r'id';
      yield serializers.serialize(
        object.id,
        specifiedType: const FullType(num),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    AnswerRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required AnswerRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'order':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.order = valueDes;
          break;
        case r'form_component_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.formComponentId = valueDes;
          break;
        case r'value':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.value = valueDes;
          break;
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  AnswerRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = AnswerRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
