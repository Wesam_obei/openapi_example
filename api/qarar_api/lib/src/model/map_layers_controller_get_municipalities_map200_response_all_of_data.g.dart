// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_municipalities_map200_response_all_of_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum_featureCollection =
    const MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum._(
        'featureCollection');

MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'featureCollection':
      return _$mapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum_featureCollection;
    default:
      return _$mapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum_featureCollection;
  }
}

final BuiltSet<
        MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum>(const <MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum>[
  _$mapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum_featureCollection,
]);

Serializer<MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnumSerializer =
    new _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnumSerializer();

class _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'featureCollection': 'FeatureCollection',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'FeatureCollection': 'featureCollection',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum';

  @override
  Object serialize(
          Serializers serializers,
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum
              object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum
      deserialize(Serializers serializers, Object serialized,
              {FullType specifiedType = FullType.unspecified}) =>
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum
              .valueOf(_fromWire[serialized] ??
                  (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData
    extends MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData {
  @override
  final MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum
      type;
  @override
  final BuiltList<
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner>
      features;

  factory _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData(
          [void Function(
                  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataBuilder)?
              updates]) =>
      (new MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData._(
      {required this.type, required this.features})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(type,
        r'MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData', 'type');
    BuiltValueNullFieldError.checkNotNull(
        features,
        r'MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData',
        'features');
  }

  @override
  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData rebuild(
          void Function(
                  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataBuilder
      toBuilder() =>
          new MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData &&
        type == other.type &&
        features == other.features;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, features.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData')
          ..add('type', type)
          ..add('features', features))
        .toString();
  }
}

class MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataBuilder
    implements
        Builder<MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData,
            MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataBuilder> {
  _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData? _$v;

  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum? _type;
  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum?
      get type => _$this._type;
  set type(
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataTypeEnum?
              type) =>
      _$this._type = type;

  ListBuilder<
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner>?
      _features;
  ListBuilder<
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner>
      get features => _$this._features ??= new ListBuilder<
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner>();
  set features(
          ListBuilder<
                  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner>?
              features) =>
      _$this._features = features;

  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataBuilder() {
    MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData._defaults(this);
  }

  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _features = $v.features.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v =
        other as _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData build() =>
      _build();

  _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData _build() {
    _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData',
                  'type'),
              features: features.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'features';
        features.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetMunicipalitiesMap200ResponseAllOfData',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
