// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_equipment_type_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ListEquipmentTypeDataResponse extends ListEquipmentTypeDataResponse {
  @override
  final num id;
  @override
  final String name;
  @override
  final DateTime createdAt;
  @override
  final GetFileDataResponse? file;

  factory _$ListEquipmentTypeDataResponse(
          [void Function(ListEquipmentTypeDataResponseBuilder)? updates]) =>
      (new ListEquipmentTypeDataResponseBuilder()..update(updates))._build();

  _$ListEquipmentTypeDataResponse._(
      {required this.id,
      required this.name,
      required this.createdAt,
      this.file})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'ListEquipmentTypeDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, r'ListEquipmentTypeDataResponse', 'name');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'ListEquipmentTypeDataResponse', 'createdAt');
  }

  @override
  ListEquipmentTypeDataResponse rebuild(
          void Function(ListEquipmentTypeDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ListEquipmentTypeDataResponseBuilder toBuilder() =>
      new ListEquipmentTypeDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ListEquipmentTypeDataResponse &&
        id == other.id &&
        name == other.name &&
        createdAt == other.createdAt &&
        file == other.file;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, file.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ListEquipmentTypeDataResponse')
          ..add('id', id)
          ..add('name', name)
          ..add('createdAt', createdAt)
          ..add('file', file))
        .toString();
  }
}

class ListEquipmentTypeDataResponseBuilder
    implements
        Builder<ListEquipmentTypeDataResponse,
            ListEquipmentTypeDataResponseBuilder> {
  _$ListEquipmentTypeDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  GetFileDataResponseBuilder? _file;
  GetFileDataResponseBuilder get file =>
      _$this._file ??= new GetFileDataResponseBuilder();
  set file(GetFileDataResponseBuilder? file) => _$this._file = file;

  ListEquipmentTypeDataResponseBuilder() {
    ListEquipmentTypeDataResponse._defaults(this);
  }

  ListEquipmentTypeDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _createdAt = $v.createdAt;
      _file = $v.file?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ListEquipmentTypeDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ListEquipmentTypeDataResponse;
  }

  @override
  void update(void Function(ListEquipmentTypeDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ListEquipmentTypeDataResponse build() => _build();

  _$ListEquipmentTypeDataResponse _build() {
    _$ListEquipmentTypeDataResponse _$result;
    try {
      _$result = _$v ??
          new _$ListEquipmentTypeDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'ListEquipmentTypeDataResponse', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'ListEquipmentTypeDataResponse', 'name'),
              createdAt: BuiltValueNullFieldError.checkNotNull(
                  createdAt, r'ListEquipmentTypeDataResponse', 'createdAt'),
              file: _file?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'file';
        _file?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ListEquipmentTypeDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
