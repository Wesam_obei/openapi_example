// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'equipment_update_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$EquipmentUpdateRequest extends EquipmentUpdateRequest {
  @override
  final num equipmentTypeId;
  @override
  final num equipmentStorePointId;
  @override
  final num entityId;
  @override
  final String brand;
  @override
  final String model;
  @override
  final String plateNumber;
  @override
  final String vinNumber;
  @override
  final num localNumber;
  @override
  final num serialNumber;
  @override
  final num manufacturingYear;
  @override
  final String? registrationExpiryDate;
  @override
  final String? ownershipDate;

  factory _$EquipmentUpdateRequest(
          [void Function(EquipmentUpdateRequestBuilder)? updates]) =>
      (new EquipmentUpdateRequestBuilder()..update(updates))._build();

  _$EquipmentUpdateRequest._(
      {required this.equipmentTypeId,
      required this.equipmentStorePointId,
      required this.entityId,
      required this.brand,
      required this.model,
      required this.plateNumber,
      required this.vinNumber,
      required this.localNumber,
      required this.serialNumber,
      required this.manufacturingYear,
      this.registrationExpiryDate,
      this.ownershipDate})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        equipmentTypeId, r'EquipmentUpdateRequest', 'equipmentTypeId');
    BuiltValueNullFieldError.checkNotNull(equipmentStorePointId,
        r'EquipmentUpdateRequest', 'equipmentStorePointId');
    BuiltValueNullFieldError.checkNotNull(
        entityId, r'EquipmentUpdateRequest', 'entityId');
    BuiltValueNullFieldError.checkNotNull(
        brand, r'EquipmentUpdateRequest', 'brand');
    BuiltValueNullFieldError.checkNotNull(
        model, r'EquipmentUpdateRequest', 'model');
    BuiltValueNullFieldError.checkNotNull(
        plateNumber, r'EquipmentUpdateRequest', 'plateNumber');
    BuiltValueNullFieldError.checkNotNull(
        vinNumber, r'EquipmentUpdateRequest', 'vinNumber');
    BuiltValueNullFieldError.checkNotNull(
        localNumber, r'EquipmentUpdateRequest', 'localNumber');
    BuiltValueNullFieldError.checkNotNull(
        serialNumber, r'EquipmentUpdateRequest', 'serialNumber');
    BuiltValueNullFieldError.checkNotNull(
        manufacturingYear, r'EquipmentUpdateRequest', 'manufacturingYear');
  }

  @override
  EquipmentUpdateRequest rebuild(
          void Function(EquipmentUpdateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  EquipmentUpdateRequestBuilder toBuilder() =>
      new EquipmentUpdateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EquipmentUpdateRequest &&
        equipmentTypeId == other.equipmentTypeId &&
        equipmentStorePointId == other.equipmentStorePointId &&
        entityId == other.entityId &&
        brand == other.brand &&
        model == other.model &&
        plateNumber == other.plateNumber &&
        vinNumber == other.vinNumber &&
        localNumber == other.localNumber &&
        serialNumber == other.serialNumber &&
        manufacturingYear == other.manufacturingYear &&
        registrationExpiryDate == other.registrationExpiryDate &&
        ownershipDate == other.ownershipDate;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, equipmentTypeId.hashCode);
    _$hash = $jc(_$hash, equipmentStorePointId.hashCode);
    _$hash = $jc(_$hash, entityId.hashCode);
    _$hash = $jc(_$hash, brand.hashCode);
    _$hash = $jc(_$hash, model.hashCode);
    _$hash = $jc(_$hash, plateNumber.hashCode);
    _$hash = $jc(_$hash, vinNumber.hashCode);
    _$hash = $jc(_$hash, localNumber.hashCode);
    _$hash = $jc(_$hash, serialNumber.hashCode);
    _$hash = $jc(_$hash, manufacturingYear.hashCode);
    _$hash = $jc(_$hash, registrationExpiryDate.hashCode);
    _$hash = $jc(_$hash, ownershipDate.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'EquipmentUpdateRequest')
          ..add('equipmentTypeId', equipmentTypeId)
          ..add('equipmentStorePointId', equipmentStorePointId)
          ..add('entityId', entityId)
          ..add('brand', brand)
          ..add('model', model)
          ..add('plateNumber', plateNumber)
          ..add('vinNumber', vinNumber)
          ..add('localNumber', localNumber)
          ..add('serialNumber', serialNumber)
          ..add('manufacturingYear', manufacturingYear)
          ..add('registrationExpiryDate', registrationExpiryDate)
          ..add('ownershipDate', ownershipDate))
        .toString();
  }
}

class EquipmentUpdateRequestBuilder
    implements Builder<EquipmentUpdateRequest, EquipmentUpdateRequestBuilder> {
  _$EquipmentUpdateRequest? _$v;

  num? _equipmentTypeId;
  num? get equipmentTypeId => _$this._equipmentTypeId;
  set equipmentTypeId(num? equipmentTypeId) =>
      _$this._equipmentTypeId = equipmentTypeId;

  num? _equipmentStorePointId;
  num? get equipmentStorePointId => _$this._equipmentStorePointId;
  set equipmentStorePointId(num? equipmentStorePointId) =>
      _$this._equipmentStorePointId = equipmentStorePointId;

  num? _entityId;
  num? get entityId => _$this._entityId;
  set entityId(num? entityId) => _$this._entityId = entityId;

  String? _brand;
  String? get brand => _$this._brand;
  set brand(String? brand) => _$this._brand = brand;

  String? _model;
  String? get model => _$this._model;
  set model(String? model) => _$this._model = model;

  String? _plateNumber;
  String? get plateNumber => _$this._plateNumber;
  set plateNumber(String? plateNumber) => _$this._plateNumber = plateNumber;

  String? _vinNumber;
  String? get vinNumber => _$this._vinNumber;
  set vinNumber(String? vinNumber) => _$this._vinNumber = vinNumber;

  num? _localNumber;
  num? get localNumber => _$this._localNumber;
  set localNumber(num? localNumber) => _$this._localNumber = localNumber;

  num? _serialNumber;
  num? get serialNumber => _$this._serialNumber;
  set serialNumber(num? serialNumber) => _$this._serialNumber = serialNumber;

  num? _manufacturingYear;
  num? get manufacturingYear => _$this._manufacturingYear;
  set manufacturingYear(num? manufacturingYear) =>
      _$this._manufacturingYear = manufacturingYear;

  String? _registrationExpiryDate;
  String? get registrationExpiryDate => _$this._registrationExpiryDate;
  set registrationExpiryDate(String? registrationExpiryDate) =>
      _$this._registrationExpiryDate = registrationExpiryDate;

  String? _ownershipDate;
  String? get ownershipDate => _$this._ownershipDate;
  set ownershipDate(String? ownershipDate) =>
      _$this._ownershipDate = ownershipDate;

  EquipmentUpdateRequestBuilder() {
    EquipmentUpdateRequest._defaults(this);
  }

  EquipmentUpdateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _equipmentTypeId = $v.equipmentTypeId;
      _equipmentStorePointId = $v.equipmentStorePointId;
      _entityId = $v.entityId;
      _brand = $v.brand;
      _model = $v.model;
      _plateNumber = $v.plateNumber;
      _vinNumber = $v.vinNumber;
      _localNumber = $v.localNumber;
      _serialNumber = $v.serialNumber;
      _manufacturingYear = $v.manufacturingYear;
      _registrationExpiryDate = $v.registrationExpiryDate;
      _ownershipDate = $v.ownershipDate;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(EquipmentUpdateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$EquipmentUpdateRequest;
  }

  @override
  void update(void Function(EquipmentUpdateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  EquipmentUpdateRequest build() => _build();

  _$EquipmentUpdateRequest _build() {
    final _$result = _$v ??
        new _$EquipmentUpdateRequest._(
            equipmentTypeId: BuiltValueNullFieldError.checkNotNull(
                equipmentTypeId, r'EquipmentUpdateRequest', 'equipmentTypeId'),
            equipmentStorePointId: BuiltValueNullFieldError.checkNotNull(
                equipmentStorePointId,
                r'EquipmentUpdateRequest',
                'equipmentStorePointId'),
            entityId: BuiltValueNullFieldError.checkNotNull(
                entityId, r'EquipmentUpdateRequest', 'entityId'),
            brand: BuiltValueNullFieldError.checkNotNull(
                brand, r'EquipmentUpdateRequest', 'brand'),
            model: BuiltValueNullFieldError.checkNotNull(
                model, r'EquipmentUpdateRequest', 'model'),
            plateNumber: BuiltValueNullFieldError.checkNotNull(
                plateNumber, r'EquipmentUpdateRequest', 'plateNumber'),
            vinNumber: BuiltValueNullFieldError.checkNotNull(
                vinNumber, r'EquipmentUpdateRequest', 'vinNumber'),
            localNumber: BuiltValueNullFieldError.checkNotNull(localNumber, r'EquipmentUpdateRequest', 'localNumber'),
            serialNumber: BuiltValueNullFieldError.checkNotNull(serialNumber, r'EquipmentUpdateRequest', 'serialNumber'),
            manufacturingYear: BuiltValueNullFieldError.checkNotNull(manufacturingYear, r'EquipmentUpdateRequest', 'manufacturingYear'),
            registrationExpiryDate: registrationExpiryDate,
            ownershipDate: ownershipDate);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
