// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_district_boundaries_map200_response_all_of_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum_featureCollection =
    const MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum
        ._('featureCollection');

MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'featureCollection':
      return _$mapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum_featureCollection;
    default:
      return _$mapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum_featureCollection;
  }
}

final BuiltSet<
        MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum>(const <MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum>[
  _$mapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum_featureCollection,
]);

Serializer<
        MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnumSerializer =
    new _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnumSerializer();

class _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'featureCollection': 'FeatureCollection',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'FeatureCollection': 'featureCollection',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum';

  @override
  Object serialize(
          Serializers serializers,
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum
              object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum
      deserialize(Serializers serializers, Object serialized,
              {FullType specifiedType = FullType.unspecified}) =>
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum
              .valueOf(_fromWire[serialized] ??
                  (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData
    extends MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData {
  @override
  final MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum
      type;
  @override
  final BuiltList<
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner>
      features;

  factory _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData(
          [void Function(
                  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataBuilder)?
              updates]) =>
      (new MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData._(
      {required this.type, required this.features})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        type,
        r'MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData',
        'type');
    BuiltValueNullFieldError.checkNotNull(
        features,
        r'MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData',
        'features');
  }

  @override
  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData rebuild(
          void Function(
                  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataBuilder
      toBuilder() =>
          new MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData &&
        type == other.type &&
        features == other.features;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, features.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData')
          ..add('type', type)
          ..add('features', features))
        .toString();
  }
}

class MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataBuilder
    implements
        Builder<MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData,
            MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataBuilder> {
  _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData? _$v;

  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum?
      _type;
  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum?
      get type => _$this._type;
  set type(
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum?
              type) =>
      _$this._type = type;

  ListBuilder<
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner>?
      _features;
  ListBuilder<
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner>
      get features => _$this._features ??= new ListBuilder<
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner>();
  set features(
          ListBuilder<
                  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner>?
              features) =>
      _$this._features = features;

  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataBuilder() {
    MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData._defaults(
        this);
  }

  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _features = $v.features.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData build() =>
      _build();

  _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData _build() {
    _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData
              ._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData',
                  'type'),
              features: features.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'features';
        features.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
