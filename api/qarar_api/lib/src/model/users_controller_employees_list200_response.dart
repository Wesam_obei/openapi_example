//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/success_response.dart';
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/list_employee_user_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'users_controller_employees_list200_response.g.dart';

/// My custom description
///
/// Properties:
/// * [success]
/// * [data]
@BuiltValue()
abstract class UsersControllerEmployeesList200Response
    implements
        SuccessResponse,
        Built<UsersControllerEmployeesList200Response,
            UsersControllerEmployeesList200ResponseBuilder> {
  UsersControllerEmployeesList200Response._();

  factory UsersControllerEmployeesList200Response(
          [void updates(UsersControllerEmployeesList200ResponseBuilder b)]) =
      _$UsersControllerEmployeesList200Response;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(UsersControllerEmployeesList200ResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<UsersControllerEmployeesList200Response> get serializer =>
      _$UsersControllerEmployeesList200ResponseSerializer();
}

class _$UsersControllerEmployeesList200ResponseSerializer
    implements PrimitiveSerializer<UsersControllerEmployeesList200Response> {
  @override
  final Iterable<Type> types = const [
    UsersControllerEmployeesList200Response,
    _$UsersControllerEmployeesList200Response
  ];

  @override
  final String wireName = r'UsersControllerEmployeesList200Response';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    UsersControllerEmployeesList200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'data';
    yield serializers.serialize(
      object.data,
      specifiedType: const FullType(JsonObject),
    );
    yield r'success';
    yield serializers.serialize(
      object.success,
      specifiedType: const FullType(bool),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    UsersControllerEmployeesList200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required UsersControllerEmployeesList200ResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'data':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.data = valueDes;
          break;
        case r'success':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.success = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  UsersControllerEmployeesList200Response deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = UsersControllerEmployeesList200ResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
