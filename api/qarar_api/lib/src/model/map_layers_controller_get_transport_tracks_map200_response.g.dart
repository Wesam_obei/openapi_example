// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_transport_tracks_map200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetTransportTracksMap200Response
    extends MapLayersControllerGetTransportTracksMap200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$MapLayersControllerGetTransportTracksMap200Response(
          [void Function(
                  MapLayersControllerGetTransportTracksMap200ResponseBuilder)?
              updates]) =>
      (new MapLayersControllerGetTransportTracksMap200ResponseBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetTransportTracksMap200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(success,
        r'MapLayersControllerGetTransportTracksMap200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'MapLayersControllerGetTransportTracksMap200Response', 'data');
  }

  @override
  MapLayersControllerGetTransportTracksMap200Response rebuild(
          void Function(
                  MapLayersControllerGetTransportTracksMap200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetTransportTracksMap200ResponseBuilder toBuilder() =>
      new MapLayersControllerGetTransportTracksMap200ResponseBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapLayersControllerGetTransportTracksMap200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetTransportTracksMap200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class MapLayersControllerGetTransportTracksMap200ResponseBuilder
    implements
        Builder<MapLayersControllerGetTransportTracksMap200Response,
            MapLayersControllerGetTransportTracksMap200ResponseBuilder>,
        SuccessResponseBuilder {
  _$MapLayersControllerGetTransportTracksMap200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  MapLayersControllerGetTransportTracksMap200ResponseBuilder() {
    MapLayersControllerGetTransportTracksMap200Response._defaults(this);
  }

  MapLayersControllerGetTransportTracksMap200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      covariant MapLayersControllerGetTransportTracksMap200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapLayersControllerGetTransportTracksMap200Response;
  }

  @override
  void update(
      void Function(MapLayersControllerGetTransportTracksMap200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetTransportTracksMap200Response build() => _build();

  _$MapLayersControllerGetTransportTracksMap200Response _build() {
    final _$result = _$v ??
        new _$MapLayersControllerGetTransportTracksMap200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success,
                r'MapLayersControllerGetTransportTracksMap200Response',
                'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data,
                r'MapLayersControllerGetTransportTracksMap200Response',
                'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
