// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_storage_locations_map200_response_all_of_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum_featureCollection =
    const MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum
        ._('featureCollection');

MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'featureCollection':
      return _$mapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum_featureCollection;
    default:
      return _$mapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum_featureCollection;
  }
}

final BuiltSet<
        MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum>(const <MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum>[
  _$mapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum_featureCollection,
]);

Serializer<
        MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnumSerializer =
    new _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnumSerializer();

class _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'featureCollection': 'FeatureCollection',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'FeatureCollection': 'featureCollection',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum';

  @override
  Object serialize(
          Serializers serializers,
          MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum
              object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum
      deserialize(
              Serializers serializers, Object serialized,
              {FullType specifiedType = FullType.unspecified}) =>
          MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum
              .valueOf(_fromWire[serialized] ??
                  (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfData
    extends MapLayersControllerGetStorageLocationsMap200ResponseAllOfData {
  @override
  final MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum
      type;
  @override
  final BuiltList<
          MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner>
      features;

  factory _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfData(
          [void Function(
                  MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataBuilder)?
              updates]) =>
      (new MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfData._(
      {required this.type, required this.features})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        type,
        r'MapLayersControllerGetStorageLocationsMap200ResponseAllOfData',
        'type');
    BuiltValueNullFieldError.checkNotNull(
        features,
        r'MapLayersControllerGetStorageLocationsMap200ResponseAllOfData',
        'features');
  }

  @override
  MapLayersControllerGetStorageLocationsMap200ResponseAllOfData rebuild(
          void Function(
                  MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataBuilder
      toBuilder() =>
          new MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetStorageLocationsMap200ResponseAllOfData &&
        type == other.type &&
        features == other.features;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, features.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetStorageLocationsMap200ResponseAllOfData')
          ..add('type', type)
          ..add('features', features))
        .toString();
  }
}

class MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataBuilder
    implements
        Builder<MapLayersControllerGetStorageLocationsMap200ResponseAllOfData,
            MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataBuilder> {
  _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfData? _$v;

  MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum? _type;
  MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum?
      get type => _$this._type;
  set type(
          MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum?
              type) =>
      _$this._type = type;

  ListBuilder<
          MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner>?
      _features;
  ListBuilder<
          MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner>
      get features => _$this._features ??= new ListBuilder<
          MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner>();
  set features(
          ListBuilder<
                  MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner>?
              features) =>
      _$this._features = features;

  MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataBuilder() {
    MapLayersControllerGetStorageLocationsMap200ResponseAllOfData._defaults(
        this);
  }

  MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _features = $v.features.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetStorageLocationsMap200ResponseAllOfData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfData;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetStorageLocationsMap200ResponseAllOfData build() =>
      _build();

  _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfData _build() {
    _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfData _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfData._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetStorageLocationsMap200ResponseAllOfData',
                  'type'),
              features: features.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'features';
        features.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetStorageLocationsMap200ResponseAllOfData',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
