// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_manhole_map200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetManholeMap200Response
    extends MapLayersControllerGetManholeMap200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$MapLayersControllerGetManholeMap200Response(
          [void Function(MapLayersControllerGetManholeMap200ResponseBuilder)?
              updates]) =>
      (new MapLayersControllerGetManholeMap200ResponseBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetManholeMap200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'MapLayersControllerGetManholeMap200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'MapLayersControllerGetManholeMap200Response', 'data');
  }

  @override
  MapLayersControllerGetManholeMap200Response rebuild(
          void Function(MapLayersControllerGetManholeMap200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetManholeMap200ResponseBuilder toBuilder() =>
      new MapLayersControllerGetManholeMap200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapLayersControllerGetManholeMap200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetManholeMap200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class MapLayersControllerGetManholeMap200ResponseBuilder
    implements
        Builder<MapLayersControllerGetManholeMap200Response,
            MapLayersControllerGetManholeMap200ResponseBuilder>,
        SuccessResponseBuilder {
  _$MapLayersControllerGetManholeMap200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  MapLayersControllerGetManholeMap200ResponseBuilder() {
    MapLayersControllerGetManholeMap200Response._defaults(this);
  }

  MapLayersControllerGetManholeMap200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant MapLayersControllerGetManholeMap200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapLayersControllerGetManholeMap200Response;
  }

  @override
  void update(
      void Function(MapLayersControllerGetManholeMap200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetManholeMap200Response build() => _build();

  _$MapLayersControllerGetManholeMap200Response _build() {
    final _$result = _$v ??
        new _$MapLayersControllerGetManholeMap200Response._(
            success: BuiltValueNullFieldError.checkNotNull(success,
                r'MapLayersControllerGetManholeMap200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'MapLayersControllerGetManholeMap200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
