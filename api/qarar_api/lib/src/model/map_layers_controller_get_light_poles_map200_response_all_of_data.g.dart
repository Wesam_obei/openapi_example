// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_light_poles_map200_response_all_of_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum_featureCollection =
    const MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum._(
        'featureCollection');

MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'featureCollection':
      return _$mapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum_featureCollection;
    default:
      return _$mapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum_featureCollection;
  }
}

final BuiltSet<MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum>(const <MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum>[
  _$mapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum_featureCollection,
]);

Serializer<MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnumSerializer =
    new _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnumSerializer();

class _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'featureCollection': 'FeatureCollection',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'FeatureCollection': 'featureCollection',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum';

  @override
  Object serialize(
          Serializers serializers,
          MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum
              object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum deserialize(
          Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetLightPolesMap200ResponseAllOfData
    extends MapLayersControllerGetLightPolesMap200ResponseAllOfData {
  @override
  final MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum type;
  @override
  final BuiltList<
          MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner>
      features;

  factory _$MapLayersControllerGetLightPolesMap200ResponseAllOfData(
          [void Function(
                  MapLayersControllerGetLightPolesMap200ResponseAllOfDataBuilder)?
              updates]) =>
      (new MapLayersControllerGetLightPolesMap200ResponseAllOfDataBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetLightPolesMap200ResponseAllOfData._(
      {required this.type, required this.features})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(type,
        r'MapLayersControllerGetLightPolesMap200ResponseAllOfData', 'type');
    BuiltValueNullFieldError.checkNotNull(features,
        r'MapLayersControllerGetLightPolesMap200ResponseAllOfData', 'features');
  }

  @override
  MapLayersControllerGetLightPolesMap200ResponseAllOfData rebuild(
          void Function(
                  MapLayersControllerGetLightPolesMap200ResponseAllOfDataBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetLightPolesMap200ResponseAllOfDataBuilder toBuilder() =>
      new MapLayersControllerGetLightPolesMap200ResponseAllOfDataBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapLayersControllerGetLightPolesMap200ResponseAllOfData &&
        type == other.type &&
        features == other.features;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, features.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetLightPolesMap200ResponseAllOfData')
          ..add('type', type)
          ..add('features', features))
        .toString();
  }
}

class MapLayersControllerGetLightPolesMap200ResponseAllOfDataBuilder
    implements
        Builder<MapLayersControllerGetLightPolesMap200ResponseAllOfData,
            MapLayersControllerGetLightPolesMap200ResponseAllOfDataBuilder> {
  _$MapLayersControllerGetLightPolesMap200ResponseAllOfData? _$v;

  MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum? _type;
  MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum? get type =>
      _$this._type;
  set type(
          MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum?
              type) =>
      _$this._type = type;

  ListBuilder<
          MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner>?
      _features;
  ListBuilder<
          MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner>
      get features => _$this._features ??= new ListBuilder<
          MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner>();
  set features(
          ListBuilder<
                  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner>?
              features) =>
      _$this._features = features;

  MapLayersControllerGetLightPolesMap200ResponseAllOfDataBuilder() {
    MapLayersControllerGetLightPolesMap200ResponseAllOfData._defaults(this);
  }

  MapLayersControllerGetLightPolesMap200ResponseAllOfDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _features = $v.features.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MapLayersControllerGetLightPolesMap200ResponseAllOfData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapLayersControllerGetLightPolesMap200ResponseAllOfData;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetLightPolesMap200ResponseAllOfDataBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetLightPolesMap200ResponseAllOfData build() => _build();

  _$MapLayersControllerGetLightPolesMap200ResponseAllOfData _build() {
    _$MapLayersControllerGetLightPolesMap200ResponseAllOfData _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetLightPolesMap200ResponseAllOfData._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetLightPolesMap200ResponseAllOfData',
                  'type'),
              features: features.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'features';
        features.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetLightPolesMap200ResponseAllOfData',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
