// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sorting_by_columns_inner_inner.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SortingByColumnsInnerInner extends SortingByColumnsInnerInner {
  @override
  final OneOf oneOf;

  factory _$SortingByColumnsInnerInner(
          [void Function(SortingByColumnsInnerInnerBuilder)? updates]) =>
      (new SortingByColumnsInnerInnerBuilder()..update(updates))._build();

  _$SortingByColumnsInnerInner._({required this.oneOf}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        oneOf, r'SortingByColumnsInnerInner', 'oneOf');
  }

  @override
  SortingByColumnsInnerInner rebuild(
          void Function(SortingByColumnsInnerInnerBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SortingByColumnsInnerInnerBuilder toBuilder() =>
      new SortingByColumnsInnerInnerBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SortingByColumnsInnerInner && oneOf == other.oneOf;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, oneOf.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SortingByColumnsInnerInner')
          ..add('oneOf', oneOf))
        .toString();
  }
}

class SortingByColumnsInnerInnerBuilder
    implements
        Builder<SortingByColumnsInnerInner, SortingByColumnsInnerInnerBuilder> {
  _$SortingByColumnsInnerInner? _$v;

  OneOf? _oneOf;
  OneOf? get oneOf => _$this._oneOf;
  set oneOf(OneOf? oneOf) => _$this._oneOf = oneOf;

  SortingByColumnsInnerInnerBuilder() {
    SortingByColumnsInnerInner._defaults(this);
  }

  SortingByColumnsInnerInnerBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _oneOf = $v.oneOf;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SortingByColumnsInnerInner other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SortingByColumnsInnerInner;
  }

  @override
  void update(void Function(SortingByColumnsInnerInnerBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SortingByColumnsInnerInner build() => _build();

  _$SortingByColumnsInnerInner _build() {
    final _$result = _$v ??
        new _$SortingByColumnsInnerInner._(
            oneOf: BuiltValueNullFieldError.checkNotNull(
                oneOf, r'SortingByColumnsInnerInner', 'oneOf'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
