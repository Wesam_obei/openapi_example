//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_plan_data_map200_response_all_of_data_features_inner.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_plan_data_map200_response_all_of_data.g.dart';

/// MapLayersControllerGetPlanDataMap200ResponseAllOfData
///
/// Properties:
/// * [type]
/// * [features]
@BuiltValue()
abstract class MapLayersControllerGetPlanDataMap200ResponseAllOfData
    implements
        Built<MapLayersControllerGetPlanDataMap200ResponseAllOfData,
            MapLayersControllerGetPlanDataMap200ResponseAllOfDataBuilder> {
  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum get type;
  // enum typeEnum {  FeatureCollection,  };

  @BuiltValueField(wireName: r'features')
  BuiltList<MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner>
      get features;

  MapLayersControllerGetPlanDataMap200ResponseAllOfData._();

  factory MapLayersControllerGetPlanDataMap200ResponseAllOfData(
      [void updates(
          MapLayersControllerGetPlanDataMap200ResponseAllOfDataBuilder
              b)]) = _$MapLayersControllerGetPlanDataMap200ResponseAllOfData;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetPlanDataMap200ResponseAllOfDataBuilder b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<MapLayersControllerGetPlanDataMap200ResponseAllOfData>
      get serializer =>
          _$MapLayersControllerGetPlanDataMap200ResponseAllOfDataSerializer();
}

class _$MapLayersControllerGetPlanDataMap200ResponseAllOfDataSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetPlanDataMap200ResponseAllOfData> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetPlanDataMap200ResponseAllOfData,
    _$MapLayersControllerGetPlanDataMap200ResponseAllOfData
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetPlanDataMap200ResponseAllOfData';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetPlanDataMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(
          MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum),
    );
    yield r'features';
    yield serializers.serialize(
      object.features,
      specifiedType: const FullType(BuiltList, [
        FullType(
            MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner)
      ]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetPlanDataMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetPlanDataMap200ResponseAllOfDataBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum),
          ) as MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum;
          result.type = valueDes;
          break;
        case r'features':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [
              FullType(
                  MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner)
            ]),
          ) as BuiltList<
              MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner>;
          result.features.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetPlanDataMap200ResponseAllOfData deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetPlanDataMap200ResponseAllOfDataBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum
    extends EnumClass {
  @BuiltValueEnumConst(wireName: r'FeatureCollection', fallback: true)
  static const MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum
      featureCollection =
      _$mapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum_featureCollection;

  static Serializer<
          MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum>
      get serializer =>
          _$mapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnumSerializer;

  const MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum._(
      String name)
      : super(name);

  static BuiltSet<MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum>
      get values =>
          _$mapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnumValues;
  static MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum valueOf(
          String name) =>
      _$mapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnumValueOf(
          name);
}
