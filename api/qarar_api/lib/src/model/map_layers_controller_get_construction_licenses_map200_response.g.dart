// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_construction_licenses_map200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetConstructionLicensesMap200Response
    extends MapLayersControllerGetConstructionLicensesMap200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$MapLayersControllerGetConstructionLicensesMap200Response(
          [void Function(
                  MapLayersControllerGetConstructionLicensesMap200ResponseBuilder)?
              updates]) =>
      (new MapLayersControllerGetConstructionLicensesMap200ResponseBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetConstructionLicensesMap200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(success,
        r'MapLayersControllerGetConstructionLicensesMap200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(data,
        r'MapLayersControllerGetConstructionLicensesMap200Response', 'data');
  }

  @override
  MapLayersControllerGetConstructionLicensesMap200Response rebuild(
          void Function(
                  MapLayersControllerGetConstructionLicensesMap200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetConstructionLicensesMap200ResponseBuilder toBuilder() =>
      new MapLayersControllerGetConstructionLicensesMap200ResponseBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapLayersControllerGetConstructionLicensesMap200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetConstructionLicensesMap200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class MapLayersControllerGetConstructionLicensesMap200ResponseBuilder
    implements
        Builder<MapLayersControllerGetConstructionLicensesMap200Response,
            MapLayersControllerGetConstructionLicensesMap200ResponseBuilder>,
        SuccessResponseBuilder {
  _$MapLayersControllerGetConstructionLicensesMap200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  MapLayersControllerGetConstructionLicensesMap200ResponseBuilder() {
    MapLayersControllerGetConstructionLicensesMap200Response._defaults(this);
  }

  MapLayersControllerGetConstructionLicensesMap200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      covariant MapLayersControllerGetConstructionLicensesMap200Response
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapLayersControllerGetConstructionLicensesMap200Response;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetConstructionLicensesMap200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetConstructionLicensesMap200Response build() => _build();

  _$MapLayersControllerGetConstructionLicensesMap200Response _build() {
    final _$result = _$v ??
        new _$MapLayersControllerGetConstructionLicensesMap200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success,
                r'MapLayersControllerGetConstructionLicensesMap200Response',
                'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data,
                r'MapLayersControllerGetConstructionLicensesMap200Response',
                'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
