//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/map_layers_controller_get_incident_reports_map200_response_all_of_data_features_inner_properties_id.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_land_base_parcel_map200_response_all_of_data_features_inner_properties.g.dart';

/// MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties
///
/// Properties:
/// * [id]
/// * [parcelPlanNo]
/// * [parcelMainLuse]
/// * [parcelSubLuse]
/// * [oldParcelPlanNO]
/// * [USING_SYMBOL]
/// * [UNITS_NUMBER]
/// * [PLAN_NO]
/// * [BLDG_CONDITIONS]
/// * [municipality]
@BuiltValue()
abstract class MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties
    implements
        Built<
            MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties,
            MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder> {
  @BuiltValueField(wireName: r'id')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get id;

  @BuiltValueField(wireName: r'parcelPlanNo')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get parcelPlanNo;

  @BuiltValueField(wireName: r'parcelMainLuse')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get parcelMainLuse;

  @BuiltValueField(wireName: r'parcelSubLuse')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get parcelSubLuse;

  @BuiltValueField(wireName: r'oldParcelPlanNO')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get oldParcelPlanNO;

  @BuiltValueField(wireName: r'USING_SYMBOL')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get USING_SYMBOL;

  @BuiltValueField(wireName: r'UNITS_NUMBER')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get UNITS_NUMBER;

  @BuiltValueField(wireName: r'PLAN_NO')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get PLAN_NO;

  @BuiltValueField(wireName: r'BLDG_CONDITIONS')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get BLDG_CONDITIONS;

  @BuiltValueField(wireName: r'municipality')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get municipality;

  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties._();

  factory MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties(
          [void updates(
              MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
                  b)]) =
      _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
              b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<
          MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties>
      get serializer =>
          _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesSerializer();
}

class _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties,
    _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties
        object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.id != null) {
      yield r'id';
      yield serializers.serialize(
        object.id,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.parcelPlanNo != null) {
      yield r'parcelPlanNo';
      yield serializers.serialize(
        object.parcelPlanNo,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.parcelMainLuse != null) {
      yield r'parcelMainLuse';
      yield serializers.serialize(
        object.parcelMainLuse,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.parcelSubLuse != null) {
      yield r'parcelSubLuse';
      yield serializers.serialize(
        object.parcelSubLuse,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.oldParcelPlanNO != null) {
      yield r'oldParcelPlanNO';
      yield serializers.serialize(
        object.oldParcelPlanNO,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.USING_SYMBOL != null) {
      yield r'USING_SYMBOL';
      yield serializers.serialize(
        object.USING_SYMBOL,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.UNITS_NUMBER != null) {
      yield r'UNITS_NUMBER';
      yield serializers.serialize(
        object.UNITS_NUMBER,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.PLAN_NO != null) {
      yield r'PLAN_NO';
      yield serializers.serialize(
        object.PLAN_NO,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.BLDG_CONDITIONS != null) {
      yield r'BLDG_CONDITIONS';
      yield serializers.serialize(
        object.BLDG_CONDITIONS,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.municipality != null) {
      yield r'municipality';
      yield serializers.serialize(
        object.municipality,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties
        object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.id.replace(valueDes);
          break;
        case r'parcelPlanNo':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.parcelPlanNo.replace(valueDes);
          break;
        case r'parcelMainLuse':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.parcelMainLuse.replace(valueDes);
          break;
        case r'parcelSubLuse':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.parcelSubLuse.replace(valueDes);
          break;
        case r'oldParcelPlanNO':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.oldParcelPlanNO.replace(valueDes);
          break;
        case r'USING_SYMBOL':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.USING_SYMBOL.replace(valueDes);
          break;
        case r'UNITS_NUMBER':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.UNITS_NUMBER.replace(valueDes);
          break;
        case r'PLAN_NO':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.PLAN_NO.replace(valueDes);
          break;
        case r'BLDG_CONDITIONS':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.BLDG_CONDITIONS.replace(valueDes);
          break;
        case r'municipality':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.municipality.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties
      deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
