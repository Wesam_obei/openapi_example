//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'equipment_type_update_request.g.dart';

/// EquipmentTypeUpdateRequest
///
/// Properties:
/// * [name]
/// * [fileToken]
/// * [deleteFile]
@BuiltValue()
abstract class EquipmentTypeUpdateRequest
    implements
        Built<EquipmentTypeUpdateRequest, EquipmentTypeUpdateRequestBuilder> {
  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'file_token')
  String? get fileToken;

  @BuiltValueField(wireName: r'delete_file')
  bool? get deleteFile;

  EquipmentTypeUpdateRequest._();

  factory EquipmentTypeUpdateRequest(
          [void updates(EquipmentTypeUpdateRequestBuilder b)]) =
      _$EquipmentTypeUpdateRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(EquipmentTypeUpdateRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<EquipmentTypeUpdateRequest> get serializer =>
      _$EquipmentTypeUpdateRequestSerializer();
}

class _$EquipmentTypeUpdateRequestSerializer
    implements PrimitiveSerializer<EquipmentTypeUpdateRequest> {
  @override
  final Iterable<Type> types = const [
    EquipmentTypeUpdateRequest,
    _$EquipmentTypeUpdateRequest
  ];

  @override
  final String wireName = r'EquipmentTypeUpdateRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    EquipmentTypeUpdateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    if (object.fileToken != null) {
      yield r'file_token';
      yield serializers.serialize(
        object.fileToken,
        specifiedType: const FullType(String),
      );
    }
    if (object.deleteFile != null) {
      yield r'delete_file';
      yield serializers.serialize(
        object.deleteFile,
        specifiedType: const FullType(bool),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    EquipmentTypeUpdateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required EquipmentTypeUpdateRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'file_token':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.fileToken = valueDes;
          break;
        case r'delete_file':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.deleteFile = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  EquipmentTypeUpdateRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = EquipmentTypeUpdateRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
