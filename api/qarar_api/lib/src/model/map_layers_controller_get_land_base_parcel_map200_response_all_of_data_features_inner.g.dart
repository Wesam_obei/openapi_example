// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_land_base_parcel_map200_response_all_of_data_features_inner.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum
    _$mapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature =
    const MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum
        ._('feature');

MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum
    _$mapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'feature':
      return _$mapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature;
    default:
      return _$mapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature;
  }
}

final BuiltSet<
        MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum>
    _$mapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum>(const <MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum>[
  _$mapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature,
]);

Serializer<
        MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum>
    _$mapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer =
    new _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer();

class _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'feature': 'Feature',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'Feature': 'feature',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum';

  @override
  Object serialize(
          Serializers serializers,
          MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum
              object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum
      deserialize(Serializers serializers, Object serialized,
              {FullType specifiedType = FullType.unspecified}) =>
          MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .valueOf(_fromWire[serialized] ??
                  (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner
    extends MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner {
  @override
  final MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum
      type;
  @override
  final int id;
  @override
  final JsonObject? geometry;
  @override
  final MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties
      properties;

  factory _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner(
          [void Function(
                  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerBuilder)?
              updates]) =>
      (new MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner._(
      {required this.type,
      required this.id,
      this.geometry,
      required this.properties})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        type,
        r'MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner',
        'type');
    BuiltValueNullFieldError.checkNotNull(
        id,
        r'MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner',
        'id');
    BuiltValueNullFieldError.checkNotNull(
        properties,
        r'MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner',
        'properties');
  }

  @override
  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner rebuild(
          void Function(
                  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerBuilder
      toBuilder() =>
          new MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner &&
        type == other.type &&
        id == other.id &&
        geometry == other.geometry &&
        properties == other.properties;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, geometry.hashCode);
    _$hash = $jc(_$hash, properties.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner')
          ..add('type', type)
          ..add('id', id)
          ..add('geometry', geometry)
          ..add('properties', properties))
        .toString();
  }
}

class MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerBuilder
    implements
        Builder<
            MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner,
            MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerBuilder> {
  _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner?
      _$v;

  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum?
      _type;
  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum?
      get type => _$this._type;
  set type(
          MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerTypeEnum?
              type) =>
      _$this._type = type;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  JsonObject? _geometry;
  JsonObject? get geometry => _$this._geometry;
  set geometry(JsonObject? geometry) => _$this._geometry = geometry;

  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder?
      _properties;
  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      get properties => _$this._properties ??=
          new MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder();
  set properties(
          MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder?
              properties) =>
      _$this._properties = properties;

  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerBuilder() {
    MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner
        ._defaults(this);
  }

  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _id = $v.id;
      _geometry = $v.geometry;
      _properties = $v.properties.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner
      build() => _build();

  _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner
      _build() {
    _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner
        _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner',
                  'type'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id,
                  r'MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner',
                  'id'),
              geometry: geometry,
              properties: properties.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'properties';
        properties.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
