//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'app_error_codes_enum.g.dart';

class AppErrorCodesEnum extends EnumClass {
  @BuiltValueEnumConst(wireName: r'deleted_components_forbidden')
  static const AppErrorCodesEnum deletedComponentsForbidden =
      _$deletedComponentsForbidden;
  @BuiltValueEnumConst(wireName: r'delete_survey_with_responses')
  static const AppErrorCodesEnum deleteSurveyWithResponses =
      _$deleteSurveyWithResponses;
  @BuiltValueEnumConst(wireName: r'delete_form_with_surveys')
  static const AppErrorCodesEnum deleteFormWithSurveys =
      _$deleteFormWithSurveys;
  @BuiltValueEnumConst(wireName: r'delete_entity_with_users')
  static const AppErrorCodesEnum deleteEntityWithUsers =
      _$deleteEntityWithUsers;
  @BuiltValueEnumConst(wireName: r'delete_entity_with_equipments')
  static const AppErrorCodesEnum deleteEntityWithEquipments =
      _$deleteEntityWithEquipments;
  @BuiltValueEnumConst(wireName: r'delete_entity_with_equipmentStorePoints')
  static const AppErrorCodesEnum deleteEntityWithEquipmentStorePoints =
      _$deleteEntityWithEquipmentStorePoints;
  @BuiltValueEnumConst(wireName: r'delete_role_with_users')
  static const AppErrorCodesEnum deleteRoleWithUsers = _$deleteRoleWithUsers;
  @BuiltValueEnumConst(wireName: r'delete_equipment_type_with_equipments')
  static const AppErrorCodesEnum deleteEquipmentTypeWithEquipments =
      _$deleteEquipmentTypeWithEquipments;
  @BuiltValueEnumConst(
      wireName: r'delete_equipment_store_point_with_equipments')
  static const AppErrorCodesEnum deleteEquipmentStorePointWithEquipments =
      _$deleteEquipmentStorePointWithEquipments;
  @BuiltValueEnumConst(wireName: r'delete_your_account')
  static const AppErrorCodesEnum deleteYourAccount = _$deleteYourAccount;
  @BuiltValueEnumConst(wireName: r'duplicate_role_name')
  static const AppErrorCodesEnum duplicateRoleName = _$duplicateRoleName;
  @BuiltValueEnumConst(wireName: r'duplicate_form_name')
  static const AppErrorCodesEnum duplicateFormName = _$duplicateFormName;
  @BuiltValueEnumConst(wireName: r'duplicate_survey_name')
  static const AppErrorCodesEnum duplicateSurveyName = _$duplicateSurveyName;
  @BuiltValueEnumConst(wireName: r'duplicate_entity_name')
  static const AppErrorCodesEnum duplicateEntityName = _$duplicateEntityName;
  @BuiltValueEnumConst(wireName: r'duplicate_user_dashboard_name')
  static const AppErrorCodesEnum duplicateUserDashboardName =
      _$duplicateUserDashboardName;
  @BuiltValueEnumConst(wireName: r'duplicate_plan_name')
  static const AppErrorCodesEnum duplicatePlanName = _$duplicatePlanName;
  @BuiltValueEnumConst(wireName: r'duplicate_equipment_store_point_name')
  static const AppErrorCodesEnum duplicateEquipmentStorePointName =
      _$duplicateEquipmentStorePointName;
  @BuiltValueEnumConst(wireName: r'duplicate_equipment_type_name')
  static const AppErrorCodesEnum duplicateEquipmentTypeName =
      _$duplicateEquipmentTypeName;
  @BuiltValueEnumConst(wireName: r'duplicate_plans_version_name')
  static const AppErrorCodesEnum duplicatePlansVersionName =
      _$duplicatePlansVersionName;
  @BuiltValueEnumConst(wireName: r'duplicate_user_email')
  static const AppErrorCodesEnum duplicateUserEmail = _$duplicateUserEmail;
  @BuiltValueEnumConst(wireName: r'permission_not_found')
  static const AppErrorCodesEnum permissionNotFound = _$permissionNotFound;
  @BuiltValueEnumConst(wireName: r'user_not_found')
  static const AppErrorCodesEnum userNotFound = _$userNotFound;
  @BuiltValueEnumConst(wireName: r'file_name_not_found')
  static const AppErrorCodesEnum fileNameNotFound = _$fileNameNotFound;
  @BuiltValueEnumConst(wireName: r'role_not_found')
  static const AppErrorCodesEnum roleNotFound = _$roleNotFound;
  @BuiltValueEnumConst(wireName: r'entity_not_found')
  static const AppErrorCodesEnum entityNotFound = _$entityNotFound;
  @BuiltValueEnumConst(wireName: r'equipment_store_point_not_found')
  static const AppErrorCodesEnum equipmentStorePointNotFound =
      _$equipmentStorePointNotFound;
  @BuiltValueEnumConst(wireName: r'equipment_type_not_found')
  static const AppErrorCodesEnum equipmentTypeNotFound =
      _$equipmentTypeNotFound;
  @BuiltValueEnumConst(wireName: r'equipment_not_found')
  static const AppErrorCodesEnum equipmentNotFound = _$equipmentNotFound;
  @BuiltValueEnumConst(wireName: r'survey_not_found')
  static const AppErrorCodesEnum surveyNotFound = _$surveyNotFound;
  @BuiltValueEnumConst(wireName: r'form_not_found')
  static const AppErrorCodesEnum formNotFound = _$formNotFound;
  @BuiltValueEnumConst(wireName: r'plan_not_found')
  static const AppErrorCodesEnum planNotFound = _$planNotFound;
  @BuiltValueEnumConst(wireName: r'plan_version_not_found')
  static const AppErrorCodesEnum planVersionNotFound = _$planVersionNotFound;
  @BuiltValueEnumConst(wireName: r'user_dashboard_not_found')
  static const AppErrorCodesEnum userDashboardNotFound =
      _$userDashboardNotFound;
  @BuiltValueEnumConst(wireName: r'update_form_with_responses')
  static const AppErrorCodesEnum updateFormWithResponses =
      _$updateFormWithResponses;
  @BuiltValueEnumConst(wireName: r'user_not_active')
  static const AppErrorCodesEnum userNotActive = _$userNotActive;
  @BuiltValueEnumConst(wireName: r'validator_error')
  static const AppErrorCodesEnum validatorError = _$validatorError;
  @BuiltValueEnumConst(wireName: r'isRequired')
  static const AppErrorCodesEnum isRequired = _$isRequired;
  @BuiltValueEnumConst(wireName: r'isArray')
  static const AppErrorCodesEnum isArray = _$isArray;
  @BuiltValueEnumConst(wireName: r'stringDataType')
  static const AppErrorCodesEnum stringDataType = _$stringDataType;
  @BuiltValueEnumConst(wireName: r'numberDataType')
  static const AppErrorCodesEnum numberDataType = _$numberDataType;
  @BuiltValueEnumConst(wireName: r'integerDataType')
  static const AppErrorCodesEnum integerDataType = _$integerDataType;
  @BuiltValueEnumConst(wireName: r'nullDataType')
  static const AppErrorCodesEnum nullDataType = _$nullDataType;
  @BuiltValueEnumConst(wireName: r'fileDataType')
  static const AppErrorCodesEnum fileDataType = _$fileDataType;
  @BuiltValueEnumConst(wireName: r'dateDataType')
  static const AppErrorCodesEnum dateDataType = _$dateDataType;
  @BuiltValueEnumConst(wireName: r'dateTimeDataType')
  static const AppErrorCodesEnum dateTimeDataType = _$dateTimeDataType;
  @BuiltValueEnumConst(wireName: r'timeDataType')
  static const AppErrorCodesEnum timeDataType = _$timeDataType;
  @BuiltValueEnumConst(wireName: r'coordinateDataType')
  static const AppErrorCodesEnum coordinateDataType = _$coordinateDataType;
  @BuiltValueEnumConst(wireName: r'response_already_submitted_or_approved')
  static const AppErrorCodesEnum responseAlreadySubmittedOrApproved =
      _$responseAlreadySubmittedOrApproved;
  @BuiltValueEnumConst(wireName: r'response_not_submitted')
  static const AppErrorCodesEnum responseNotSubmitted = _$responseNotSubmitted;
  @BuiltValueEnumConst(wireName: r'edit_question_has_answers')
  static const AppErrorCodesEnum editQuestionHasAnswers =
      _$editQuestionHasAnswers;
  @BuiltValueEnumConst(wireName: r'question_has_already_answers')
  static const AppErrorCodesEnum questionHasAlreadyAnswers =
      _$questionHasAlreadyAnswers;
  @BuiltValueEnumConst(wireName: r'invalid_subscription')
  static const AppErrorCodesEnum invalidSubscription = _$invalidSubscription;
  @BuiltValueEnumConst(wireName: r'file_creator_not_match')
  static const AppErrorCodesEnum fileCreatorNotMatch = _$fileCreatorNotMatch;
  @BuiltValueEnumConst(wireName: r'invalid_includes')
  static const AppErrorCodesEnum invalidIncludes = _$invalidIncludes;
  @BuiltValueEnumConst(wireName: r'service_unavailable')
  static const AppErrorCodesEnum serviceUnavailable = _$serviceUnavailable;
  @BuiltValueEnumConst(wireName: r'service_schema_validator_error')
  static const AppErrorCodesEnum serviceSchemaValidatorError =
      _$serviceSchemaValidatorError;
  @BuiltValueEnumConst(wireName: r'unauthorized')
  static const AppErrorCodesEnum unauthorized = _$unauthorized;
  @BuiltValueEnumConst(wireName: r'raqame_unauthorized')
  static const AppErrorCodesEnum raqameUnauthorized = _$raqameUnauthorized;
  @BuiltValueEnumConst(wireName: r'missing_user')
  static const AppErrorCodesEnum missingUser = _$missingUser;
  @BuiltValueEnumConst(wireName: r'generic_not_found')
  static const AppErrorCodesEnum genericNotFound = _$genericNotFound;
  @BuiltValueEnumConst(wireName: r'forbidden')
  static const AppErrorCodesEnum forbidden = _$forbidden;
  @BuiltValueEnumConst(wireName: r'server_error')
  static const AppErrorCodesEnum serverError = _$serverError;
  @BuiltValueEnumConst(wireName: r'unknown', fallback: true)
  static const AppErrorCodesEnum unknown = _$unknown;

  static Serializer<AppErrorCodesEnum> get serializer =>
      _$appErrorCodesEnumSerializer;

  const AppErrorCodesEnum._(String name) : super(name);

  static BuiltSet<AppErrorCodesEnum> get values => _$values;
  static AppErrorCodesEnum valueOf(String name) => _$valueOf(name);
}

/// Optionally, enum_class can generate a mixin to go with your enum for use
/// with Angular. It exposes your enum constants as getters. So, if you mix it
/// in to your Dart component class, the values become available to the
/// corresponding Angular template.
///
/// Trigger mixin generation by writing a line like this one next to your enum.
abstract class AppErrorCodesEnumMixin = Object with _$AppErrorCodesEnumMixin;
