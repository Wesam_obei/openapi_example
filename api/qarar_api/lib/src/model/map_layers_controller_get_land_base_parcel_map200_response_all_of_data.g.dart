// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_land_base_parcel_map200_response_all_of_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum_featureCollection =
    const MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum._(
        'featureCollection');

MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'featureCollection':
      return _$mapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum_featureCollection;
    default:
      return _$mapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum_featureCollection;
  }
}

final BuiltSet<
        MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum>(const <MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum>[
  _$mapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum_featureCollection,
]);

Serializer<MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnumSerializer =
    new _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnumSerializer();

class _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'featureCollection': 'FeatureCollection',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'FeatureCollection': 'featureCollection',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum';

  @override
  Object serialize(
          Serializers serializers,
          MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum
              object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum
      deserialize(Serializers serializers, Object serialized,
              {FullType specifiedType = FullType.unspecified}) =>
          MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum
              .valueOf(_fromWire[serialized] ??
                  (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData
    extends MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData {
  @override
  final MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum
      type;
  @override
  final BuiltList<
          MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner>
      features;

  factory _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData(
          [void Function(
                  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataBuilder)?
              updates]) =>
      (new MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData._(
      {required this.type, required this.features})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(type,
        r'MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData', 'type');
    BuiltValueNullFieldError.checkNotNull(
        features,
        r'MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData',
        'features');
  }

  @override
  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData rebuild(
          void Function(
                  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataBuilder
      toBuilder() =>
          new MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData &&
        type == other.type &&
        features == other.features;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, features.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData')
          ..add('type', type)
          ..add('features', features))
        .toString();
  }
}

class MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataBuilder
    implements
        Builder<MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData,
            MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataBuilder> {
  _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData? _$v;

  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum? _type;
  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum?
      get type => _$this._type;
  set type(
          MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum?
              type) =>
      _$this._type = type;

  ListBuilder<
          MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner>?
      _features;
  ListBuilder<
          MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner>
      get features => _$this._features ??= new ListBuilder<
          MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner>();
  set features(
          ListBuilder<
                  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner>?
              features) =>
      _$this._features = features;

  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataBuilder() {
    MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData._defaults(this);
  }

  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _features = $v.features.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v =
        other as _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData build() =>
      _build();

  _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData _build() {
    _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData',
                  'type'),
              features: features.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'features';
        features.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
