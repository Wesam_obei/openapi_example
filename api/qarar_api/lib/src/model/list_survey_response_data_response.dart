//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/status_response.dart';
import 'package:qarar_api/src/model/survey_response_answer_data_response.dart';
import 'package:qarar_api/src/model/get_user_data_response.dart';
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/get_survey_recipient_view_data_response.dart';
import 'package:qarar_api/src/model/get_survey_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'list_survey_response_data_response.g.dart';

/// ListSurveyResponseDataResponse
///
/// Properties:
/// * [id]
/// * [createdAt]
/// * [recipientId]
/// * [answeredBy]
/// * [responseStatus]
/// * [surveyResponseAnswer]
/// * [survey]
/// * [recipient]
/// * [canAppliedReview]
@BuiltValue()
abstract class ListSurveyResponseDataResponse
    implements
        Built<ListSurveyResponseDataResponse,
            ListSurveyResponseDataResponseBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'created_at')
  DateTime get createdAt;

  @BuiltValueField(wireName: r'recipient_id')
  num get recipientId;

  @BuiltValueField(wireName: r'answeredBy')
  GetUserDataResponse get answeredBy;

  @BuiltValueField(wireName: r'responseStatus')
  StatusResponse get responseStatus;

  @BuiltValueField(wireName: r'surveyResponseAnswer')
  BuiltList<SurveyResponseAnswerDataResponse> get surveyResponseAnswer;

  @BuiltValueField(wireName: r'survey')
  GetSurveyDataResponse get survey;

  @BuiltValueField(wireName: r'recipient')
  GetSurveyRecipientViewDataResponse get recipient;

  @BuiltValueField(wireName: r'can_applied_review')
  bool? get canAppliedReview;

  ListSurveyResponseDataResponse._();

  factory ListSurveyResponseDataResponse(
          [void updates(ListSurveyResponseDataResponseBuilder b)]) =
      _$ListSurveyResponseDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ListSurveyResponseDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ListSurveyResponseDataResponse> get serializer =>
      _$ListSurveyResponseDataResponseSerializer();
}

class _$ListSurveyResponseDataResponseSerializer
    implements PrimitiveSerializer<ListSurveyResponseDataResponse> {
  @override
  final Iterable<Type> types = const [
    ListSurveyResponseDataResponse,
    _$ListSurveyResponseDataResponse
  ];

  @override
  final String wireName = r'ListSurveyResponseDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ListSurveyResponseDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'created_at';
    yield serializers.serialize(
      object.createdAt,
      specifiedType: const FullType(DateTime),
    );
    yield r'recipient_id';
    yield serializers.serialize(
      object.recipientId,
      specifiedType: const FullType(num),
    );
    yield r'answeredBy';
    yield serializers.serialize(
      object.answeredBy,
      specifiedType: const FullType(GetUserDataResponse),
    );
    yield r'responseStatus';
    yield serializers.serialize(
      object.responseStatus,
      specifiedType: const FullType(StatusResponse),
    );
    yield r'surveyResponseAnswer';
    yield serializers.serialize(
      object.surveyResponseAnswer,
      specifiedType: const FullType(
          BuiltList, [FullType(SurveyResponseAnswerDataResponse)]),
    );
    yield r'survey';
    yield serializers.serialize(
      object.survey,
      specifiedType: const FullType(GetSurveyDataResponse),
    );
    yield r'recipient';
    yield serializers.serialize(
      object.recipient,
      specifiedType: const FullType(GetSurveyRecipientViewDataResponse),
    );
    if (object.canAppliedReview != null) {
      yield r'can_applied_review';
      yield serializers.serialize(
        object.canAppliedReview,
        specifiedType: const FullType(bool),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ListSurveyResponseDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ListSurveyResponseDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'created_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.createdAt = valueDes;
          break;
        case r'recipient_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.recipientId = valueDes;
          break;
        case r'answeredBy':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetUserDataResponse),
          ) as GetUserDataResponse;
          result.answeredBy.replace(valueDes);
          break;
        case r'responseStatus':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(StatusResponse),
          ) as StatusResponse;
          result.responseStatus.replace(valueDes);
          break;
        case r'surveyResponseAnswer':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                BuiltList, [FullType(SurveyResponseAnswerDataResponse)]),
          ) as BuiltList<SurveyResponseAnswerDataResponse>;
          result.surveyResponseAnswer.replace(valueDes);
          break;
        case r'survey':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetSurveyDataResponse),
          ) as GetSurveyDataResponse;
          result.survey.replace(valueDes);
          break;
        case r'recipient':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetSurveyRecipientViewDataResponse),
          ) as GetSurveyRecipientViewDataResponse;
          result.recipient.replace(valueDes);
          break;
        case r'can_applied_review':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.canAppliedReview = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ListSurveyResponseDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ListSurveyResponseDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
