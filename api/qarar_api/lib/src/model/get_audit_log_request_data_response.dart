//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/get_audit_log_header_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'get_audit_log_request_data_response.g.dart';

/// GetAuditLogRequestDataResponse
///
/// Properties:
/// * [headers]
@BuiltValue()
abstract class GetAuditLogRequestDataResponse
    implements
        Built<GetAuditLogRequestDataResponse,
            GetAuditLogRequestDataResponseBuilder> {
  @BuiltValueField(wireName: r'headers')
  GetAuditLogHeaderDataResponse get headers;

  GetAuditLogRequestDataResponse._();

  factory GetAuditLogRequestDataResponse(
          [void updates(GetAuditLogRequestDataResponseBuilder b)]) =
      _$GetAuditLogRequestDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(GetAuditLogRequestDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<GetAuditLogRequestDataResponse> get serializer =>
      _$GetAuditLogRequestDataResponseSerializer();
}

class _$GetAuditLogRequestDataResponseSerializer
    implements PrimitiveSerializer<GetAuditLogRequestDataResponse> {
  @override
  final Iterable<Type> types = const [
    GetAuditLogRequestDataResponse,
    _$GetAuditLogRequestDataResponse
  ];

  @override
  final String wireName = r'GetAuditLogRequestDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    GetAuditLogRequestDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'headers';
    yield serializers.serialize(
      object.headers,
      specifiedType: const FullType(GetAuditLogHeaderDataResponse),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    GetAuditLogRequestDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required GetAuditLogRequestDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'headers':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetAuditLogHeaderDataResponse),
          ) as GetAuditLogHeaderDataResponse;
          result.headers.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  GetAuditLogRequestDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = GetAuditLogRequestDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
