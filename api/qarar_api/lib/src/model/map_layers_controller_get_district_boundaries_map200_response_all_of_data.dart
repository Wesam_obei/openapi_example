//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_district_boundaries_map200_response_all_of_data_features_inner.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_district_boundaries_map200_response_all_of_data.g.dart';

/// MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData
///
/// Properties:
/// * [type]
/// * [features]
@BuiltValue()
abstract class MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData
    implements
        Built<MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData,
            MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataBuilder> {
  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum
      get type;
  // enum typeEnum {  FeatureCollection,  };

  @BuiltValueField(wireName: r'features')
  BuiltList<
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner>
      get features;

  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData._();

  factory MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData(
          [void updates(
              MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataBuilder
                  b)]) =
      _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataBuilder
              b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData>
      get serializer =>
          _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataSerializer();
}

class _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData,
    _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum),
    );
    yield r'features';
    yield serializers.serialize(
      object.features,
      specifiedType: const FullType(BuiltList, [
        FullType(
            MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner)
      ]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum),
          ) as MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum;
          result.type = valueDes;
          break;
        case r'features':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [
              FullType(
                  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner)
            ]),
          ) as BuiltList<
              MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner>;
          result.features.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfData deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum
    extends EnumClass {
  @BuiltValueEnumConst(wireName: r'FeatureCollection', fallback: true)
  static const MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum
      featureCollection =
      _$mapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum_featureCollection;

  static Serializer<
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum>
      get serializer =>
          _$mapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnumSerializer;

  const MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum._(
      String name)
      : super(name);

  static BuiltSet<
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum>
      get values =>
          _$mapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnumValues;
  static MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnum
      valueOf(String name) =>
          _$mapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataTypeEnumValueOf(
              name);
}
