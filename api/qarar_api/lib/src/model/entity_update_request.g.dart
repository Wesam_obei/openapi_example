// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'entity_update_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$EntityUpdateRequest extends EntityUpdateRequest {
  @override
  final String name;
  @override
  final BuiltList<num> users;
  @override
  final String? fileToken;
  @override
  final bool? deleteFile;

  factory _$EntityUpdateRequest(
          [void Function(EntityUpdateRequestBuilder)? updates]) =>
      (new EntityUpdateRequestBuilder()..update(updates))._build();

  _$EntityUpdateRequest._(
      {required this.name,
      required this.users,
      this.fileToken,
      this.deleteFile})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(name, r'EntityUpdateRequest', 'name');
    BuiltValueNullFieldError.checkNotNull(
        users, r'EntityUpdateRequest', 'users');
  }

  @override
  EntityUpdateRequest rebuild(
          void Function(EntityUpdateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  EntityUpdateRequestBuilder toBuilder() =>
      new EntityUpdateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EntityUpdateRequest &&
        name == other.name &&
        users == other.users &&
        fileToken == other.fileToken &&
        deleteFile == other.deleteFile;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, users.hashCode);
    _$hash = $jc(_$hash, fileToken.hashCode);
    _$hash = $jc(_$hash, deleteFile.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'EntityUpdateRequest')
          ..add('name', name)
          ..add('users', users)
          ..add('fileToken', fileToken)
          ..add('deleteFile', deleteFile))
        .toString();
  }
}

class EntityUpdateRequestBuilder
    implements Builder<EntityUpdateRequest, EntityUpdateRequestBuilder> {
  _$EntityUpdateRequest? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  ListBuilder<num>? _users;
  ListBuilder<num> get users => _$this._users ??= new ListBuilder<num>();
  set users(ListBuilder<num>? users) => _$this._users = users;

  String? _fileToken;
  String? get fileToken => _$this._fileToken;
  set fileToken(String? fileToken) => _$this._fileToken = fileToken;

  bool? _deleteFile;
  bool? get deleteFile => _$this._deleteFile;
  set deleteFile(bool? deleteFile) => _$this._deleteFile = deleteFile;

  EntityUpdateRequestBuilder() {
    EntityUpdateRequest._defaults(this);
  }

  EntityUpdateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _users = $v.users.toBuilder();
      _fileToken = $v.fileToken;
      _deleteFile = $v.deleteFile;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(EntityUpdateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$EntityUpdateRequest;
  }

  @override
  void update(void Function(EntityUpdateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  EntityUpdateRequest build() => _build();

  _$EntityUpdateRequest _build() {
    _$EntityUpdateRequest _$result;
    try {
      _$result = _$v ??
          new _$EntityUpdateRequest._(
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'EntityUpdateRequest', 'name'),
              users: users.build(),
              fileToken: fileToken,
              deleteFile: deleteFile);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'users';
        users.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'EntityUpdateRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
