// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'paginated_meta_documented.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PaginatedMetaDocumented extends PaginatedMetaDocumented {
  @override
  final num itemsPerPage;
  @override
  final num totalItems;
  @override
  final num currentPage;
  @override
  final num totalPages;
  @override
  final BuiltList<BuiltList<SortingByColumnsInnerInner>>? sortBy;
  @override
  final BuiltList<String>? searchBy;
  @override
  final String? search;
  @override
  final BuiltList<String>? select;
  @override
  final JsonObject? filter;
  @override
  final BuiltList<String>? includes;

  factory _$PaginatedMetaDocumented(
          [void Function(PaginatedMetaDocumentedBuilder)? updates]) =>
      (new PaginatedMetaDocumentedBuilder()..update(updates))._build();

  _$PaginatedMetaDocumented._(
      {required this.itemsPerPage,
      required this.totalItems,
      required this.currentPage,
      required this.totalPages,
      this.sortBy,
      this.searchBy,
      this.search,
      this.select,
      this.filter,
      this.includes})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        itemsPerPage, r'PaginatedMetaDocumented', 'itemsPerPage');
    BuiltValueNullFieldError.checkNotNull(
        totalItems, r'PaginatedMetaDocumented', 'totalItems');
    BuiltValueNullFieldError.checkNotNull(
        currentPage, r'PaginatedMetaDocumented', 'currentPage');
    BuiltValueNullFieldError.checkNotNull(
        totalPages, r'PaginatedMetaDocumented', 'totalPages');
  }

  @override
  PaginatedMetaDocumented rebuild(
          void Function(PaginatedMetaDocumentedBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PaginatedMetaDocumentedBuilder toBuilder() =>
      new PaginatedMetaDocumentedBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PaginatedMetaDocumented &&
        itemsPerPage == other.itemsPerPage &&
        totalItems == other.totalItems &&
        currentPage == other.currentPage &&
        totalPages == other.totalPages &&
        sortBy == other.sortBy &&
        searchBy == other.searchBy &&
        search == other.search &&
        select == other.select &&
        filter == other.filter &&
        includes == other.includes;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, itemsPerPage.hashCode);
    _$hash = $jc(_$hash, totalItems.hashCode);
    _$hash = $jc(_$hash, currentPage.hashCode);
    _$hash = $jc(_$hash, totalPages.hashCode);
    _$hash = $jc(_$hash, sortBy.hashCode);
    _$hash = $jc(_$hash, searchBy.hashCode);
    _$hash = $jc(_$hash, search.hashCode);
    _$hash = $jc(_$hash, select.hashCode);
    _$hash = $jc(_$hash, filter.hashCode);
    _$hash = $jc(_$hash, includes.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'PaginatedMetaDocumented')
          ..add('itemsPerPage', itemsPerPage)
          ..add('totalItems', totalItems)
          ..add('currentPage', currentPage)
          ..add('totalPages', totalPages)
          ..add('sortBy', sortBy)
          ..add('searchBy', searchBy)
          ..add('search', search)
          ..add('select', select)
          ..add('filter', filter)
          ..add('includes', includes))
        .toString();
  }
}

class PaginatedMetaDocumentedBuilder
    implements
        Builder<PaginatedMetaDocumented, PaginatedMetaDocumentedBuilder> {
  _$PaginatedMetaDocumented? _$v;

  num? _itemsPerPage;
  num? get itemsPerPage => _$this._itemsPerPage;
  set itemsPerPage(num? itemsPerPage) => _$this._itemsPerPage = itemsPerPage;

  num? _totalItems;
  num? get totalItems => _$this._totalItems;
  set totalItems(num? totalItems) => _$this._totalItems = totalItems;

  num? _currentPage;
  num? get currentPage => _$this._currentPage;
  set currentPage(num? currentPage) => _$this._currentPage = currentPage;

  num? _totalPages;
  num? get totalPages => _$this._totalPages;
  set totalPages(num? totalPages) => _$this._totalPages = totalPages;

  ListBuilder<BuiltList<SortingByColumnsInnerInner>>? _sortBy;
  ListBuilder<BuiltList<SortingByColumnsInnerInner>> get sortBy =>
      _$this._sortBy ??=
          new ListBuilder<BuiltList<SortingByColumnsInnerInner>>();
  set sortBy(ListBuilder<BuiltList<SortingByColumnsInnerInner>>? sortBy) =>
      _$this._sortBy = sortBy;

  ListBuilder<String>? _searchBy;
  ListBuilder<String> get searchBy =>
      _$this._searchBy ??= new ListBuilder<String>();
  set searchBy(ListBuilder<String>? searchBy) => _$this._searchBy = searchBy;

  String? _search;
  String? get search => _$this._search;
  set search(String? search) => _$this._search = search;

  ListBuilder<String>? _select;
  ListBuilder<String> get select =>
      _$this._select ??= new ListBuilder<String>();
  set select(ListBuilder<String>? select) => _$this._select = select;

  JsonObject? _filter;
  JsonObject? get filter => _$this._filter;
  set filter(JsonObject? filter) => _$this._filter = filter;

  ListBuilder<String>? _includes;
  ListBuilder<String> get includes =>
      _$this._includes ??= new ListBuilder<String>();
  set includes(ListBuilder<String>? includes) => _$this._includes = includes;

  PaginatedMetaDocumentedBuilder() {
    PaginatedMetaDocumented._defaults(this);
  }

  PaginatedMetaDocumentedBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _itemsPerPage = $v.itemsPerPage;
      _totalItems = $v.totalItems;
      _currentPage = $v.currentPage;
      _totalPages = $v.totalPages;
      _sortBy = $v.sortBy?.toBuilder();
      _searchBy = $v.searchBy?.toBuilder();
      _search = $v.search;
      _select = $v.select?.toBuilder();
      _filter = $v.filter;
      _includes = $v.includes?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PaginatedMetaDocumented other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$PaginatedMetaDocumented;
  }

  @override
  void update(void Function(PaginatedMetaDocumentedBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  PaginatedMetaDocumented build() => _build();

  _$PaginatedMetaDocumented _build() {
    _$PaginatedMetaDocumented _$result;
    try {
      _$result = _$v ??
          new _$PaginatedMetaDocumented._(
              itemsPerPage: BuiltValueNullFieldError.checkNotNull(
                  itemsPerPage, r'PaginatedMetaDocumented', 'itemsPerPage'),
              totalItems: BuiltValueNullFieldError.checkNotNull(
                  totalItems, r'PaginatedMetaDocumented', 'totalItems'),
              currentPage: BuiltValueNullFieldError.checkNotNull(
                  currentPage, r'PaginatedMetaDocumented', 'currentPage'),
              totalPages: BuiltValueNullFieldError.checkNotNull(
                  totalPages, r'PaginatedMetaDocumented', 'totalPages'),
              sortBy: _sortBy?.build(),
              searchBy: _searchBy?.build(),
              search: search,
              select: _select?.build(),
              filter: filter,
              includes: _includes?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'sortBy';
        _sortBy?.build();
        _$failedField = 'searchBy';
        _searchBy?.build();

        _$failedField = 'select';
        _select?.build();

        _$failedField = 'includes';
        _includes?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'PaginatedMetaDocumented', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
