// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_update_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$UserUpdateRequest extends UserUpdateRequest {
  @override
  final bool isActive;
  @override
  final BuiltList<num> roles;
  @override
  final BuiltList<String> permissions;
  @override
  final BuiltList<num> entities;

  factory _$UserUpdateRequest(
          [void Function(UserUpdateRequestBuilder)? updates]) =>
      (new UserUpdateRequestBuilder()..update(updates))._build();

  _$UserUpdateRequest._(
      {required this.isActive,
      required this.roles,
      required this.permissions,
      required this.entities})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        isActive, r'UserUpdateRequest', 'isActive');
    BuiltValueNullFieldError.checkNotNull(roles, r'UserUpdateRequest', 'roles');
    BuiltValueNullFieldError.checkNotNull(
        permissions, r'UserUpdateRequest', 'permissions');
    BuiltValueNullFieldError.checkNotNull(
        entities, r'UserUpdateRequest', 'entities');
  }

  @override
  UserUpdateRequest rebuild(void Function(UserUpdateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserUpdateRequestBuilder toBuilder() =>
      new UserUpdateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserUpdateRequest &&
        isActive == other.isActive &&
        roles == other.roles &&
        permissions == other.permissions &&
        entities == other.entities;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, isActive.hashCode);
    _$hash = $jc(_$hash, roles.hashCode);
    _$hash = $jc(_$hash, permissions.hashCode);
    _$hash = $jc(_$hash, entities.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'UserUpdateRequest')
          ..add('isActive', isActive)
          ..add('roles', roles)
          ..add('permissions', permissions)
          ..add('entities', entities))
        .toString();
  }
}

class UserUpdateRequestBuilder
    implements Builder<UserUpdateRequest, UserUpdateRequestBuilder> {
  _$UserUpdateRequest? _$v;

  bool? _isActive;
  bool? get isActive => _$this._isActive;
  set isActive(bool? isActive) => _$this._isActive = isActive;

  ListBuilder<num>? _roles;
  ListBuilder<num> get roles => _$this._roles ??= new ListBuilder<num>();
  set roles(ListBuilder<num>? roles) => _$this._roles = roles;

  ListBuilder<String>? _permissions;
  ListBuilder<String> get permissions =>
      _$this._permissions ??= new ListBuilder<String>();
  set permissions(ListBuilder<String>? permissions) =>
      _$this._permissions = permissions;

  ListBuilder<num>? _entities;
  ListBuilder<num> get entities => _$this._entities ??= new ListBuilder<num>();
  set entities(ListBuilder<num>? entities) => _$this._entities = entities;

  UserUpdateRequestBuilder() {
    UserUpdateRequest._defaults(this);
  }

  UserUpdateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _isActive = $v.isActive;
      _roles = $v.roles.toBuilder();
      _permissions = $v.permissions.toBuilder();
      _entities = $v.entities.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserUpdateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$UserUpdateRequest;
  }

  @override
  void update(void Function(UserUpdateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  UserUpdateRequest build() => _build();

  _$UserUpdateRequest _build() {
    _$UserUpdateRequest _$result;
    try {
      _$result = _$v ??
          new _$UserUpdateRequest._(
              isActive: BuiltValueNullFieldError.checkNotNull(
                  isActive, r'UserUpdateRequest', 'isActive'),
              roles: roles.build(),
              permissions: permissions.build(),
              entities: entities.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'roles';
        roles.build();
        _$failedField = 'permissions';
        permissions.build();
        _$failedField = 'entities';
        entities.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'UserUpdateRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
