//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/permissions_enum.dart';
import 'package:qarar_api/src/model/app_error_codes_enum.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'core_enum.g.dart';

/// CoreEnum
///
/// Properties:
/// * [appErrorCodes]
/// * [permissions]
@BuiltValue()
abstract class CoreEnum implements Built<CoreEnum, CoreEnumBuilder> {
  @BuiltValueField(wireName: r'appErrorCodes')
  AppErrorCodesEnum get appErrorCodes;
  // enum appErrorCodesEnum {  deleted_components_forbidden,  delete_survey_with_responses,  delete_form_with_surveys,  delete_entity_with_users,  delete_entity_with_equipments,  delete_entity_with_equipmentStorePoints,  delete_role_with_users,  delete_equipment_type_with_equipments,  delete_equipment_store_point_with_equipments,  delete_your_account,  duplicate_role_name,  duplicate_form_name,  duplicate_survey_name,  duplicate_entity_name,  duplicate_user_dashboard_name,  duplicate_plan_name,  duplicate_equipment_store_point_name,  duplicate_equipment_type_name,  duplicate_plans_version_name,  duplicate_user_email,  permission_not_found,  user_not_found,  file_name_not_found,  role_not_found,  entity_not_found,  equipment_store_point_not_found,  equipment_type_not_found,  equipment_not_found,  survey_not_found,  form_not_found,  plan_not_found,  plan_version_not_found,  user_dashboard_not_found,  update_form_with_responses,  user_not_active,  validator_error,  isRequired,  isArray,  stringDataType,  numberDataType,  integerDataType,  nullDataType,  fileDataType,  dateDataType,  dateTimeDataType,  timeDataType,  coordinateDataType,  response_already_submitted_or_approved,  response_not_submitted,  edit_question_has_answers,  question_has_already_answers,  invalid_subscription,  file_creator_not_match,  invalid_includes,  service_unavailable,  service_schema_validator_error,  unauthorized,  raqame_unauthorized,  missing_user,  generic_not_found,  forbidden,  server_error,  unknown,  };

  @BuiltValueField(wireName: r'permissions')
  PermissionsEnum get permissions;
  // enum permissionsEnum {  create_user,  view_user,  update_user,  delete_user,  create_role,  view_role,  update_role,  delete_role,  edit_user_dashboard,  view_user_dashboard,  delete_user_dashboard,  view_maater_layer,  view_incidents940_layer,  view_construction_license_layer,  view_light_poles_layer,  view_cables_layer,  view_electrical_facilities_layer,  view_bridges_tunnels_sites_layer,  view_store_points_layer,  view_response_layer,  view_critical_points_layer,  view_transport_track_layer,  view_street_naming_layer,  view_municipality_boundary_layer,  view_land_base_parcel_layer,  view_district_boundary_layer,  view_urban_area_boundary_layer,  view_plan_data_layer,  view_manhole_layer,  view_sea_out_layer,  create_plan,  view_plan,  update_plan,  delete_plan,  create_equipment_store_point,  view_equipment_store_point,  update_equipment_store_point,  delete_equipment_store_point,  create_equipment_type,  view_equipment_type,  update_equipment_type,  delete_equipment_type,  create_equipment,  view_equipment,  update_equipment,  delete_equipment,  create_form,  update_form,  delete_form,  view_form,  create_survey,  update_survey,  delete_survey,  view_survey,  respond_survey_for_entity,  survey_verified_responses,  create_entity,  update_entity,  delete_entity,  view_entity,  };

  CoreEnum._();

  factory CoreEnum([void updates(CoreEnumBuilder b)]) = _$CoreEnum;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(CoreEnumBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<CoreEnum> get serializer => _$CoreEnumSerializer();
}

class _$CoreEnumSerializer implements PrimitiveSerializer<CoreEnum> {
  @override
  final Iterable<Type> types = const [CoreEnum, _$CoreEnum];

  @override
  final String wireName = r'CoreEnum';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    CoreEnum object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'appErrorCodes';
    yield serializers.serialize(
      object.appErrorCodes,
      specifiedType: const FullType(AppErrorCodesEnum),
    );
    yield r'permissions';
    yield serializers.serialize(
      object.permissions,
      specifiedType: const FullType(PermissionsEnum),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    CoreEnum object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required CoreEnumBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'appErrorCodes':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(AppErrorCodesEnum),
          ) as AppErrorCodesEnum;
          result.appErrorCodes = valueDes;
          break;
        case r'permissions':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(PermissionsEnum),
          ) as PermissionsEnum;
          result.permissions = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  CoreEnum deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = CoreEnumBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
