// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'survey_create_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SurveyCreateRequest extends SurveyCreateRequest {
  @override
  final String name;
  @override
  final num formId;
  @override
  final bool isActive;
  @override
  final DateTime startAt;
  @override
  final DateTime endAt;
  @override
  final BuiltList<num> usersIds;
  @override
  final BuiltList<num> entitiesIds;
  @override
  final String? description;

  factory _$SurveyCreateRequest(
          [void Function(SurveyCreateRequestBuilder)? updates]) =>
      (new SurveyCreateRequestBuilder()..update(updates))._build();

  _$SurveyCreateRequest._(
      {required this.name,
      required this.formId,
      required this.isActive,
      required this.startAt,
      required this.endAt,
      required this.usersIds,
      required this.entitiesIds,
      this.description})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(name, r'SurveyCreateRequest', 'name');
    BuiltValueNullFieldError.checkNotNull(
        formId, r'SurveyCreateRequest', 'formId');
    BuiltValueNullFieldError.checkNotNull(
        isActive, r'SurveyCreateRequest', 'isActive');
    BuiltValueNullFieldError.checkNotNull(
        startAt, r'SurveyCreateRequest', 'startAt');
    BuiltValueNullFieldError.checkNotNull(
        endAt, r'SurveyCreateRequest', 'endAt');
    BuiltValueNullFieldError.checkNotNull(
        usersIds, r'SurveyCreateRequest', 'usersIds');
    BuiltValueNullFieldError.checkNotNull(
        entitiesIds, r'SurveyCreateRequest', 'entitiesIds');
  }

  @override
  SurveyCreateRequest rebuild(
          void Function(SurveyCreateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SurveyCreateRequestBuilder toBuilder() =>
      new SurveyCreateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SurveyCreateRequest &&
        name == other.name &&
        formId == other.formId &&
        isActive == other.isActive &&
        startAt == other.startAt &&
        endAt == other.endAt &&
        usersIds == other.usersIds &&
        entitiesIds == other.entitiesIds &&
        description == other.description;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, formId.hashCode);
    _$hash = $jc(_$hash, isActive.hashCode);
    _$hash = $jc(_$hash, startAt.hashCode);
    _$hash = $jc(_$hash, endAt.hashCode);
    _$hash = $jc(_$hash, usersIds.hashCode);
    _$hash = $jc(_$hash, entitiesIds.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SurveyCreateRequest')
          ..add('name', name)
          ..add('formId', formId)
          ..add('isActive', isActive)
          ..add('startAt', startAt)
          ..add('endAt', endAt)
          ..add('usersIds', usersIds)
          ..add('entitiesIds', entitiesIds)
          ..add('description', description))
        .toString();
  }
}

class SurveyCreateRequestBuilder
    implements Builder<SurveyCreateRequest, SurveyCreateRequestBuilder> {
  _$SurveyCreateRequest? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  num? _formId;
  num? get formId => _$this._formId;
  set formId(num? formId) => _$this._formId = formId;

  bool? _isActive;
  bool? get isActive => _$this._isActive;
  set isActive(bool? isActive) => _$this._isActive = isActive;

  DateTime? _startAt;
  DateTime? get startAt => _$this._startAt;
  set startAt(DateTime? startAt) => _$this._startAt = startAt;

  DateTime? _endAt;
  DateTime? get endAt => _$this._endAt;
  set endAt(DateTime? endAt) => _$this._endAt = endAt;

  ListBuilder<num>? _usersIds;
  ListBuilder<num> get usersIds => _$this._usersIds ??= new ListBuilder<num>();
  set usersIds(ListBuilder<num>? usersIds) => _$this._usersIds = usersIds;

  ListBuilder<num>? _entitiesIds;
  ListBuilder<num> get entitiesIds =>
      _$this._entitiesIds ??= new ListBuilder<num>();
  set entitiesIds(ListBuilder<num>? entitiesIds) =>
      _$this._entitiesIds = entitiesIds;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  SurveyCreateRequestBuilder() {
    SurveyCreateRequest._defaults(this);
  }

  SurveyCreateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _formId = $v.formId;
      _isActive = $v.isActive;
      _startAt = $v.startAt;
      _endAt = $v.endAt;
      _usersIds = $v.usersIds.toBuilder();
      _entitiesIds = $v.entitiesIds.toBuilder();
      _description = $v.description;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SurveyCreateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SurveyCreateRequest;
  }

  @override
  void update(void Function(SurveyCreateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SurveyCreateRequest build() => _build();

  _$SurveyCreateRequest _build() {
    _$SurveyCreateRequest _$result;
    try {
      _$result = _$v ??
          new _$SurveyCreateRequest._(
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'SurveyCreateRequest', 'name'),
              formId: BuiltValueNullFieldError.checkNotNull(
                  formId, r'SurveyCreateRequest', 'formId'),
              isActive: BuiltValueNullFieldError.checkNotNull(
                  isActive, r'SurveyCreateRequest', 'isActive'),
              startAt: BuiltValueNullFieldError.checkNotNull(
                  startAt, r'SurveyCreateRequest', 'startAt'),
              endAt: BuiltValueNullFieldError.checkNotNull(
                  endAt, r'SurveyCreateRequest', 'endAt'),
              usersIds: usersIds.build(),
              entitiesIds: entitiesIds.build(),
              description: description);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'usersIds';
        usersIds.build();
        _$failedField = 'entitiesIds';
        entitiesIds.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'SurveyCreateRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
