//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'equipment_update_request.g.dart';

/// EquipmentUpdateRequest
///
/// Properties:
/// * [equipmentTypeId]
/// * [equipmentStorePointId]
/// * [entityId]
/// * [brand]
/// * [model]
/// * [plateNumber]
/// * [vinNumber]
/// * [localNumber]
/// * [serialNumber]
/// * [manufacturingYear]
/// * [registrationExpiryDate]
/// * [ownershipDate]
@BuiltValue()
abstract class EquipmentUpdateRequest
    implements Built<EquipmentUpdateRequest, EquipmentUpdateRequestBuilder> {
  @BuiltValueField(wireName: r'equipment_type_id')
  num get equipmentTypeId;

  @BuiltValueField(wireName: r'equipment_store_point_id')
  num get equipmentStorePointId;

  @BuiltValueField(wireName: r'entity_id')
  num get entityId;

  @BuiltValueField(wireName: r'brand')
  String get brand;

  @BuiltValueField(wireName: r'model')
  String get model;

  @BuiltValueField(wireName: r'plate_number')
  String get plateNumber;

  @BuiltValueField(wireName: r'vin_number')
  String get vinNumber;

  @BuiltValueField(wireName: r'local_number')
  num get localNumber;

  @BuiltValueField(wireName: r'serial_number')
  num get serialNumber;

  @BuiltValueField(wireName: r'manufacturing_year')
  num get manufacturingYear;

  @BuiltValueField(wireName: r'registration_expiry_date')
  String? get registrationExpiryDate;

  @BuiltValueField(wireName: r'ownership_date')
  String? get ownershipDate;

  EquipmentUpdateRequest._();

  factory EquipmentUpdateRequest(
          [void updates(EquipmentUpdateRequestBuilder b)]) =
      _$EquipmentUpdateRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(EquipmentUpdateRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<EquipmentUpdateRequest> get serializer =>
      _$EquipmentUpdateRequestSerializer();
}

class _$EquipmentUpdateRequestSerializer
    implements PrimitiveSerializer<EquipmentUpdateRequest> {
  @override
  final Iterable<Type> types = const [
    EquipmentUpdateRequest,
    _$EquipmentUpdateRequest
  ];

  @override
  final String wireName = r'EquipmentUpdateRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    EquipmentUpdateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'equipment_type_id';
    yield serializers.serialize(
      object.equipmentTypeId,
      specifiedType: const FullType(num),
    );
    yield r'equipment_store_point_id';
    yield serializers.serialize(
      object.equipmentStorePointId,
      specifiedType: const FullType(num),
    );
    yield r'entity_id';
    yield serializers.serialize(
      object.entityId,
      specifiedType: const FullType(num),
    );
    yield r'brand';
    yield serializers.serialize(
      object.brand,
      specifiedType: const FullType(String),
    );
    yield r'model';
    yield serializers.serialize(
      object.model,
      specifiedType: const FullType(String),
    );
    yield r'plate_number';
    yield serializers.serialize(
      object.plateNumber,
      specifiedType: const FullType(String),
    );
    yield r'vin_number';
    yield serializers.serialize(
      object.vinNumber,
      specifiedType: const FullType(String),
    );
    yield r'local_number';
    yield serializers.serialize(
      object.localNumber,
      specifiedType: const FullType(num),
    );
    yield r'serial_number';
    yield serializers.serialize(
      object.serialNumber,
      specifiedType: const FullType(num),
    );
    yield r'manufacturing_year';
    yield serializers.serialize(
      object.manufacturingYear,
      specifiedType: const FullType(num),
    );
    if (object.registrationExpiryDate != null) {
      yield r'registration_expiry_date';
      yield serializers.serialize(
        object.registrationExpiryDate,
        specifiedType: const FullType.nullable(String),
      );
    }
    if (object.ownershipDate != null) {
      yield r'ownership_date';
      yield serializers.serialize(
        object.ownershipDate,
        specifiedType: const FullType.nullable(String),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    EquipmentUpdateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required EquipmentUpdateRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'equipment_type_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.equipmentTypeId = valueDes;
          break;
        case r'equipment_store_point_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.equipmentStorePointId = valueDes;
          break;
        case r'entity_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.entityId = valueDes;
          break;
        case r'brand':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.brand = valueDes;
          break;
        case r'model':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.model = valueDes;
          break;
        case r'plate_number':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.plateNumber = valueDes;
          break;
        case r'vin_number':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.vinNumber = valueDes;
          break;
        case r'local_number':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.localNumber = valueDes;
          break;
        case r'serial_number':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.serialNumber = valueDes;
          break;
        case r'manufacturing_year':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.manufacturingYear = valueDes;
          break;
        case r'registration_expiry_date':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(String),
          ) as String?;
          if (valueDes == null) continue;
          result.registrationExpiryDate = valueDes;
          break;
        case r'ownership_date':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(String),
          ) as String?;
          if (valueDes == null) continue;
          result.ownershipDate = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  EquipmentUpdateRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = EquipmentUpdateRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
