// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_construction_licenses_map200_response_all_of_data_features_inner.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum
    _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature =
    const MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum
        ._('feature');

MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum
    _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'feature':
      return _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature;
    default:
      return _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature;
  }
}

final BuiltSet<
        MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum>
    _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum>(const <MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum>[
  _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature,
]);

Serializer<
        MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum>
    _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer =
    new _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer();

class _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'feature': 'Feature',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'Feature': 'feature',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum';

  @override
  Object serialize(
          Serializers serializers,
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum
              object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum
      deserialize(Serializers serializers, Object serialized,
              {FullType specifiedType = FullType.unspecified}) =>
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .valueOf(_fromWire[serialized] ??
                  (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner
    extends MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner {
  @override
  final MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum
      type;
  @override
  final int id;
  @override
  final JsonObject? geometry;
  @override
  final MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties
      properties;

  factory _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner(
          [void Function(
                  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerBuilder)?
              updates]) =>
      (new MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner._(
      {required this.type,
      required this.id,
      this.geometry,
      required this.properties})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        type,
        r'MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner',
        'type');
    BuiltValueNullFieldError.checkNotNull(
        id,
        r'MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner',
        'id');
    BuiltValueNullFieldError.checkNotNull(
        properties,
        r'MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner',
        'properties');
  }

  @override
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner
      rebuild(
              void Function(
                      MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerBuilder)
                  updates) =>
          (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerBuilder
      toBuilder() =>
          new MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner &&
        type == other.type &&
        id == other.id &&
        geometry == other.geometry &&
        properties == other.properties;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, geometry.hashCode);
    _$hash = $jc(_$hash, properties.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner')
          ..add('type', type)
          ..add('id', id)
          ..add('geometry', geometry)
          ..add('properties', properties))
        .toString();
  }
}

class MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerBuilder
    implements
        Builder<
            MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner,
            MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerBuilder> {
  _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner?
      _$v;

  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum?
      _type;
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum?
      get type => _$this._type;
  set type(
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum?
              type) =>
      _$this._type = type;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  JsonObject? _geometry;
  JsonObject? get geometry => _$this._geometry;
  set geometry(JsonObject? geometry) => _$this._geometry = geometry;

  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder?
      _properties;
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      get properties => _$this._properties ??=
          new MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder();
  set properties(
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder?
              properties) =>
      _$this._properties = properties;

  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerBuilder() {
    MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner
        ._defaults(this);
  }

  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _id = $v.id;
      _geometry = $v.geometry;
      _properties = $v.properties.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner
      build() => _build();

  _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner
      _build() {
    _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner
        _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner',
                  'type'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id,
                  r'MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner',
                  'id'),
              geometry: geometry,
              properties: properties.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'properties';
        properties.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
