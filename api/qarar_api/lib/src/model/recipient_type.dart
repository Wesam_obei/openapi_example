//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'recipient_type.g.dart';

class RecipientType extends EnumClass {
  @BuiltValueEnumConst(wireName: r'users')
  static const RecipientType users = _$users;
  @BuiltValueEnumConst(wireName: r'entities', fallback: true)
  static const RecipientType entities = _$entities;

  static Serializer<RecipientType> get serializer => _$recipientTypeSerializer;

  const RecipientType._(String name) : super(name);

  static BuiltSet<RecipientType> get values => _$values;
  static RecipientType valueOf(String name) => _$valueOf(name);
}

/// Optionally, enum_class can generate a mixin to go with your enum for use
/// with Angular. It exposes your enum constants as getters. So, if you mix it
/// in to your Dart component class, the values become available to the
/// corresponding Angular template.
///
/// Trigger mixin generation by writing a line like this one next to your enum.
abstract class RecipientTypeMixin = Object with _$RecipientTypeMixin;
