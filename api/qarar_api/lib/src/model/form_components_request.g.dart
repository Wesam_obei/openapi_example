// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'form_components_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$FormComponentsRequest extends FormComponentsRequest {
  @override
  final FormResponseDataTypeEnum responseDataType;
  @override
  final bool isArray;
  @override
  final String label;
  @override
  final String? placeholder;
  @override
  final String description;
  @override
  final bool isRequired;
  @override
  final bool allowMultipleResponse;
  @override
  final bool isGroup;
  @override
  final bool config;
  @override
  final num order;
  @override
  final num? id;
  @override
  final BuiltList<FormComponentsRequest>? children;

  factory _$FormComponentsRequest(
          [void Function(FormComponentsRequestBuilder)? updates]) =>
      (new FormComponentsRequestBuilder()..update(updates))._build();

  _$FormComponentsRequest._(
      {required this.responseDataType,
      required this.isArray,
      required this.label,
      this.placeholder,
      required this.description,
      required this.isRequired,
      required this.allowMultipleResponse,
      required this.isGroup,
      required this.config,
      required this.order,
      this.id,
      this.children})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        responseDataType, r'FormComponentsRequest', 'responseDataType');
    BuiltValueNullFieldError.checkNotNull(
        isArray, r'FormComponentsRequest', 'isArray');
    BuiltValueNullFieldError.checkNotNull(
        label, r'FormComponentsRequest', 'label');
    BuiltValueNullFieldError.checkNotNull(
        description, r'FormComponentsRequest', 'description');
    BuiltValueNullFieldError.checkNotNull(
        isRequired, r'FormComponentsRequest', 'isRequired');
    BuiltValueNullFieldError.checkNotNull(allowMultipleResponse,
        r'FormComponentsRequest', 'allowMultipleResponse');
    BuiltValueNullFieldError.checkNotNull(
        isGroup, r'FormComponentsRequest', 'isGroup');
    BuiltValueNullFieldError.checkNotNull(
        config, r'FormComponentsRequest', 'config');
    BuiltValueNullFieldError.checkNotNull(
        order, r'FormComponentsRequest', 'order');
  }

  @override
  FormComponentsRequest rebuild(
          void Function(FormComponentsRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  FormComponentsRequestBuilder toBuilder() =>
      new FormComponentsRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is FormComponentsRequest &&
        responseDataType == other.responseDataType &&
        isArray == other.isArray &&
        label == other.label &&
        placeholder == other.placeholder &&
        description == other.description &&
        isRequired == other.isRequired &&
        allowMultipleResponse == other.allowMultipleResponse &&
        isGroup == other.isGroup &&
        config == other.config &&
        order == other.order &&
        id == other.id &&
        children == other.children;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, responseDataType.hashCode);
    _$hash = $jc(_$hash, isArray.hashCode);
    _$hash = $jc(_$hash, label.hashCode);
    _$hash = $jc(_$hash, placeholder.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, isRequired.hashCode);
    _$hash = $jc(_$hash, allowMultipleResponse.hashCode);
    _$hash = $jc(_$hash, isGroup.hashCode);
    _$hash = $jc(_$hash, config.hashCode);
    _$hash = $jc(_$hash, order.hashCode);
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, children.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'FormComponentsRequest')
          ..add('responseDataType', responseDataType)
          ..add('isArray', isArray)
          ..add('label', label)
          ..add('placeholder', placeholder)
          ..add('description', description)
          ..add('isRequired', isRequired)
          ..add('allowMultipleResponse', allowMultipleResponse)
          ..add('isGroup', isGroup)
          ..add('config', config)
          ..add('order', order)
          ..add('id', id)
          ..add('children', children))
        .toString();
  }
}

class FormComponentsRequestBuilder
    implements Builder<FormComponentsRequest, FormComponentsRequestBuilder> {
  _$FormComponentsRequest? _$v;

  FormResponseDataTypeEnum? _responseDataType;
  FormResponseDataTypeEnum? get responseDataType => _$this._responseDataType;
  set responseDataType(FormResponseDataTypeEnum? responseDataType) =>
      _$this._responseDataType = responseDataType;

  bool? _isArray;
  bool? get isArray => _$this._isArray;
  set isArray(bool? isArray) => _$this._isArray = isArray;

  String? _label;
  String? get label => _$this._label;
  set label(String? label) => _$this._label = label;

  String? _placeholder;
  String? get placeholder => _$this._placeholder;
  set placeholder(String? placeholder) => _$this._placeholder = placeholder;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  bool? _isRequired;
  bool? get isRequired => _$this._isRequired;
  set isRequired(bool? isRequired) => _$this._isRequired = isRequired;

  bool? _allowMultipleResponse;
  bool? get allowMultipleResponse => _$this._allowMultipleResponse;
  set allowMultipleResponse(bool? allowMultipleResponse) =>
      _$this._allowMultipleResponse = allowMultipleResponse;

  bool? _isGroup;
  bool? get isGroup => _$this._isGroup;
  set isGroup(bool? isGroup) => _$this._isGroup = isGroup;

  bool? _config;
  bool? get config => _$this._config;
  set config(bool? config) => _$this._config = config;

  num? _order;
  num? get order => _$this._order;
  set order(num? order) => _$this._order = order;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  ListBuilder<FormComponentsRequest>? _children;
  ListBuilder<FormComponentsRequest> get children =>
      _$this._children ??= new ListBuilder<FormComponentsRequest>();
  set children(ListBuilder<FormComponentsRequest>? children) =>
      _$this._children = children;

  FormComponentsRequestBuilder() {
    FormComponentsRequest._defaults(this);
  }

  FormComponentsRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _responseDataType = $v.responseDataType;
      _isArray = $v.isArray;
      _label = $v.label;
      _placeholder = $v.placeholder;
      _description = $v.description;
      _isRequired = $v.isRequired;
      _allowMultipleResponse = $v.allowMultipleResponse;
      _isGroup = $v.isGroup;
      _config = $v.config;
      _order = $v.order;
      _id = $v.id;
      _children = $v.children?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(FormComponentsRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$FormComponentsRequest;
  }

  @override
  void update(void Function(FormComponentsRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  FormComponentsRequest build() => _build();

  _$FormComponentsRequest _build() {
    _$FormComponentsRequest _$result;
    try {
      _$result = _$v ??
          new _$FormComponentsRequest._(
              responseDataType: BuiltValueNullFieldError.checkNotNull(
                  responseDataType, r'FormComponentsRequest', 'responseDataType'),
              isArray: BuiltValueNullFieldError.checkNotNull(
                  isArray, r'FormComponentsRequest', 'isArray'),
              label: BuiltValueNullFieldError.checkNotNull(
                  label, r'FormComponentsRequest', 'label'),
              placeholder: placeholder,
              description: BuiltValueNullFieldError.checkNotNull(
                  description, r'FormComponentsRequest', 'description'),
              isRequired: BuiltValueNullFieldError.checkNotNull(
                  isRequired, r'FormComponentsRequest', 'isRequired'),
              allowMultipleResponse: BuiltValueNullFieldError.checkNotNull(
                  allowMultipleResponse,
                  r'FormComponentsRequest',
                  'allowMultipleResponse'),
              isGroup: BuiltValueNullFieldError.checkNotNull(
                  isGroup, r'FormComponentsRequest', 'isGroup'),
              config: BuiltValueNullFieldError.checkNotNull(config, r'FormComponentsRequest', 'config'),
              order: BuiltValueNullFieldError.checkNotNull(order, r'FormComponentsRequest', 'order'),
              id: id,
              children: _children?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'children';
        _children?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'FormComponentsRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
