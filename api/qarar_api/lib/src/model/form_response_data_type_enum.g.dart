// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'form_response_data_type_enum.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const FormResponseDataTypeEnum _$null_ =
    const FormResponseDataTypeEnum._('null_');
const FormResponseDataTypeEnum _$string =
    const FormResponseDataTypeEnum._('string');
const FormResponseDataTypeEnum _$integer =
    const FormResponseDataTypeEnum._('integer');
const FormResponseDataTypeEnum _$number =
    const FormResponseDataTypeEnum._('number');
const FormResponseDataTypeEnum _$date =
    const FormResponseDataTypeEnum._('date');
const FormResponseDataTypeEnum _$datetime =
    const FormResponseDataTypeEnum._('datetime');
const FormResponseDataTypeEnum _$time =
    const FormResponseDataTypeEnum._('time');
const FormResponseDataTypeEnum _$file =
    const FormResponseDataTypeEnum._('file');
const FormResponseDataTypeEnum _$rangeDate =
    const FormResponseDataTypeEnum._('rangeDate');
const FormResponseDataTypeEnum _$rangeDatetime =
    const FormResponseDataTypeEnum._('rangeDatetime');
const FormResponseDataTypeEnum _$rangeTime =
    const FormResponseDataTypeEnum._('rangeTime');
const FormResponseDataTypeEnum _$coordinate =
    const FormResponseDataTypeEnum._('coordinate');

FormResponseDataTypeEnum _$valueOf(String name) {
  switch (name) {
    case 'null_':
      return _$null_;
    case 'string':
      return _$string;
    case 'integer':
      return _$integer;
    case 'number':
      return _$number;
    case 'date':
      return _$date;
    case 'datetime':
      return _$datetime;
    case 'time':
      return _$time;
    case 'file':
      return _$file;
    case 'rangeDate':
      return _$rangeDate;
    case 'rangeDatetime':
      return _$rangeDatetime;
    case 'rangeTime':
      return _$rangeTime;
    case 'coordinate':
      return _$coordinate;
    default:
      return _$coordinate;
  }
}

final BuiltSet<FormResponseDataTypeEnum> _$values =
    new BuiltSet<FormResponseDataTypeEnum>(const <FormResponseDataTypeEnum>[
  _$null_,
  _$string,
  _$integer,
  _$number,
  _$date,
  _$datetime,
  _$time,
  _$file,
  _$rangeDate,
  _$rangeDatetime,
  _$rangeTime,
  _$coordinate,
]);

class _$FormResponseDataTypeEnumMeta {
  const _$FormResponseDataTypeEnumMeta();
  FormResponseDataTypeEnum get null_ => _$null_;
  FormResponseDataTypeEnum get string => _$string;
  FormResponseDataTypeEnum get integer => _$integer;
  FormResponseDataTypeEnum get number => _$number;
  FormResponseDataTypeEnum get date => _$date;
  FormResponseDataTypeEnum get datetime => _$datetime;
  FormResponseDataTypeEnum get time => _$time;
  FormResponseDataTypeEnum get file => _$file;
  FormResponseDataTypeEnum get rangeDate => _$rangeDate;
  FormResponseDataTypeEnum get rangeDatetime => _$rangeDatetime;
  FormResponseDataTypeEnum get rangeTime => _$rangeTime;
  FormResponseDataTypeEnum get coordinate => _$coordinate;
  FormResponseDataTypeEnum valueOf(String name) => _$valueOf(name);
  BuiltSet<FormResponseDataTypeEnum> get values => _$values;
}

abstract class _$FormResponseDataTypeEnumMixin {
  // ignore: non_constant_identifier_names
  _$FormResponseDataTypeEnumMeta get FormResponseDataTypeEnum =>
      const _$FormResponseDataTypeEnumMeta();
}

Serializer<FormResponseDataTypeEnum> _$formResponseDataTypeEnumSerializer =
    new _$FormResponseDataTypeEnumSerializer();

class _$FormResponseDataTypeEnumSerializer
    implements PrimitiveSerializer<FormResponseDataTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'null_': 'null',
    'string': 'string',
    'integer': 'integer',
    'number': 'number',
    'date': 'date',
    'datetime': 'datetime',
    'time': 'time',
    'file': 'file',
    'rangeDate': 'range-date',
    'rangeDatetime': 'range-datetime',
    'rangeTime': 'range-time',
    'coordinate': 'coordinate',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'null': 'null_',
    'string': 'string',
    'integer': 'integer',
    'number': 'number',
    'date': 'date',
    'datetime': 'datetime',
    'time': 'time',
    'file': 'file',
    'range-date': 'rangeDate',
    'range-datetime': 'rangeDatetime',
    'range-time': 'rangeTime',
    'coordinate': 'coordinate',
  };

  @override
  final Iterable<Type> types = const <Type>[FormResponseDataTypeEnum];
  @override
  final String wireName = 'FormResponseDataTypeEnum';

  @override
  Object serialize(Serializers serializers, FormResponseDataTypeEnum object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  FormResponseDataTypeEnum deserialize(
          Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      FormResponseDataTypeEnum.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
