//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/widget_config.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'get_user_dashboard_data_response.g.dart';

/// GetUserDashboardDataResponse
///
/// Properties:
/// * [id]
/// * [name]
/// * [isDefault]
/// * [settings]
@BuiltValue()
abstract class GetUserDashboardDataResponse
    implements
        Built<GetUserDashboardDataResponse,
            GetUserDashboardDataResponseBuilder> {
  @BuiltValueField(wireName: r'id')
  String get id;

  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'is_default')
  bool get isDefault;

  @BuiltValueField(wireName: r'settings')
  BuiltList<WidgetConfig>? get settings;

  GetUserDashboardDataResponse._();

  factory GetUserDashboardDataResponse(
          [void updates(GetUserDashboardDataResponseBuilder b)]) =
      _$GetUserDashboardDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(GetUserDashboardDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<GetUserDashboardDataResponse> get serializer =>
      _$GetUserDashboardDataResponseSerializer();
}

class _$GetUserDashboardDataResponseSerializer
    implements PrimitiveSerializer<GetUserDashboardDataResponse> {
  @override
  final Iterable<Type> types = const [
    GetUserDashboardDataResponse,
    _$GetUserDashboardDataResponse
  ];

  @override
  final String wireName = r'GetUserDashboardDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    GetUserDashboardDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(String),
    );
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    yield r'is_default';
    yield serializers.serialize(
      object.isDefault,
      specifiedType: const FullType(bool),
    );
    yield r'settings';
    yield object.settings == null
        ? null
        : serializers.serialize(
            object.settings,
            specifiedType:
                const FullType.nullable(BuiltList, [FullType(WidgetConfig)]),
          );
  }

  @override
  Object serialize(
    Serializers serializers,
    GetUserDashboardDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required GetUserDashboardDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.id = valueDes;
          break;
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'is_default':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.isDefault = valueDes;
          break;
        case r'settings':
          final valueDes = serializers.deserialize(
            value,
            specifiedType:
                const FullType.nullable(BuiltList, [FullType(WidgetConfig)]),
          ) as BuiltList<WidgetConfig>?;
          if (valueDes == null) continue;
          result.settings.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  GetUserDashboardDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = GetUserDashboardDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
