//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'comment_create_request.g.dart';

/// CommentCreateRequest
///
/// Properties:
/// * [surveyResponseAnswerId]
/// * [comment]
@BuiltValue()
abstract class CommentCreateRequest
    implements Built<CommentCreateRequest, CommentCreateRequestBuilder> {
  @BuiltValueField(wireName: r'survey_response_answer_id')
  num get surveyResponseAnswerId;

  @BuiltValueField(wireName: r'comment')
  String get comment;

  CommentCreateRequest._();

  factory CommentCreateRequest([void updates(CommentCreateRequestBuilder b)]) =
      _$CommentCreateRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(CommentCreateRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<CommentCreateRequest> get serializer =>
      _$CommentCreateRequestSerializer();
}

class _$CommentCreateRequestSerializer
    implements PrimitiveSerializer<CommentCreateRequest> {
  @override
  final Iterable<Type> types = const [
    CommentCreateRequest,
    _$CommentCreateRequest
  ];

  @override
  final String wireName = r'CommentCreateRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    CommentCreateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'survey_response_answer_id';
    yield serializers.serialize(
      object.surveyResponseAnswerId,
      specifiedType: const FullType(num),
    );
    yield r'comment';
    yield serializers.serialize(
      object.comment,
      specifiedType: const FullType(String),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    CommentCreateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required CommentCreateRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'survey_response_answer_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.surveyResponseAnswerId = valueDes;
          break;
        case r'comment':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.comment = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  CommentCreateRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = CommentCreateRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
