// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_bridges_tunnels_sites_map200_response_all_of_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum_featureCollection =
    const MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum
        ._('featureCollection');

MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'featureCollection':
      return _$mapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum_featureCollection;
    default:
      return _$mapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum_featureCollection;
  }
}

final BuiltSet<
        MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum>(const <MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum>[
  _$mapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum_featureCollection,
]);

Serializer<
        MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnumSerializer =
    new _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnumSerializer();

class _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'featureCollection': 'FeatureCollection',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'FeatureCollection': 'featureCollection',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum';

  @override
  Object serialize(
          Serializers serializers,
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum
              object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum
      deserialize(Serializers serializers, Object serialized,
              {FullType specifiedType = FullType.unspecified}) =>
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum
              .valueOf(_fromWire[serialized] ??
                  (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData
    extends MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData {
  @override
  final MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum
      type;
  @override
  final BuiltList<
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner>
      features;

  factory _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData(
          [void Function(
                  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataBuilder)?
              updates]) =>
      (new MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData._(
      {required this.type, required this.features})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        type,
        r'MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData',
        'type');
    BuiltValueNullFieldError.checkNotNull(
        features,
        r'MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData',
        'features');
  }

  @override
  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData rebuild(
          void Function(
                  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataBuilder
      toBuilder() =>
          new MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData &&
        type == other.type &&
        features == other.features;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, features.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData')
          ..add('type', type)
          ..add('features', features))
        .toString();
  }
}

class MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataBuilder
    implements
        Builder<
            MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData,
            MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataBuilder> {
  _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData? _$v;

  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum?
      _type;
  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum?
      get type => _$this._type;
  set type(
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum?
              type) =>
      _$this._type = type;

  ListBuilder<
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner>?
      _features;
  ListBuilder<
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner>
      get features => _$this._features ??= new ListBuilder<
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner>();
  set features(
          ListBuilder<
                  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner>?
              features) =>
      _$this._features = features;

  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataBuilder() {
    MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData._defaults(
        this);
  }

  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _features = $v.features.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData build() =>
      _build();

  _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData _build() {
    _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData
              ._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData',
                  'type'),
              features: features.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'features';
        features.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
