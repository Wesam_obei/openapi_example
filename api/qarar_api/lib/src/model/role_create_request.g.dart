// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'role_create_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$RoleCreateRequest extends RoleCreateRequest {
  @override
  final String name;
  @override
  final String description;
  @override
  final BuiltList<String> permissions;

  factory _$RoleCreateRequest(
          [void Function(RoleCreateRequestBuilder)? updates]) =>
      (new RoleCreateRequestBuilder()..update(updates))._build();

  _$RoleCreateRequest._(
      {required this.name,
      required this.description,
      required this.permissions})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(name, r'RoleCreateRequest', 'name');
    BuiltValueNullFieldError.checkNotNull(
        description, r'RoleCreateRequest', 'description');
    BuiltValueNullFieldError.checkNotNull(
        permissions, r'RoleCreateRequest', 'permissions');
  }

  @override
  RoleCreateRequest rebuild(void Function(RoleCreateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RoleCreateRequestBuilder toBuilder() =>
      new RoleCreateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RoleCreateRequest &&
        name == other.name &&
        description == other.description &&
        permissions == other.permissions;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, permissions.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'RoleCreateRequest')
          ..add('name', name)
          ..add('description', description)
          ..add('permissions', permissions))
        .toString();
  }
}

class RoleCreateRequestBuilder
    implements Builder<RoleCreateRequest, RoleCreateRequestBuilder> {
  _$RoleCreateRequest? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  ListBuilder<String>? _permissions;
  ListBuilder<String> get permissions =>
      _$this._permissions ??= new ListBuilder<String>();
  set permissions(ListBuilder<String>? permissions) =>
      _$this._permissions = permissions;

  RoleCreateRequestBuilder() {
    RoleCreateRequest._defaults(this);
  }

  RoleCreateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _description = $v.description;
      _permissions = $v.permissions.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RoleCreateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$RoleCreateRequest;
  }

  @override
  void update(void Function(RoleCreateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  RoleCreateRequest build() => _build();

  _$RoleCreateRequest _build() {
    _$RoleCreateRequest _$result;
    try {
      _$result = _$v ??
          new _$RoleCreateRequest._(
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'RoleCreateRequest', 'name'),
              description: BuiltValueNullFieldError.checkNotNull(
                  description, r'RoleCreateRequest', 'description'),
              permissions: permissions.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'permissions';
        permissions.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'RoleCreateRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
