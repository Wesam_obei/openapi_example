// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_urban_area_boundaries_map200_response_all_of_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum_featureCollection =
    const MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum
        ._('featureCollection');

MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'featureCollection':
      return _$mapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum_featureCollection;
    default:
      return _$mapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum_featureCollection;
  }
}

final BuiltSet<
        MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum>(const <MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum>[
  _$mapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum_featureCollection,
]);

Serializer<
        MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnumSerializer =
    new _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnumSerializer();

class _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'featureCollection': 'FeatureCollection',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'FeatureCollection': 'featureCollection',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum';

  @override
  Object serialize(
          Serializers serializers,
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum
              object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum
      deserialize(Serializers serializers, Object serialized,
              {FullType specifiedType = FullType.unspecified}) =>
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum
              .valueOf(_fromWire[serialized] ??
                  (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData
    extends MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData {
  @override
  final MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum
      type;
  @override
  final BuiltList<
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner>
      features;

  factory _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData(
          [void Function(
                  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataBuilder)?
              updates]) =>
      (new MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData._(
      {required this.type, required this.features})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        type,
        r'MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData',
        'type');
    BuiltValueNullFieldError.checkNotNull(
        features,
        r'MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData',
        'features');
  }

  @override
  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData rebuild(
          void Function(
                  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataBuilder
      toBuilder() =>
          new MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData &&
        type == other.type &&
        features == other.features;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, features.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData')
          ..add('type', type)
          ..add('features', features))
        .toString();
  }
}

class MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataBuilder
    implements
        Builder<
            MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData,
            MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataBuilder> {
  _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData? _$v;

  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum?
      _type;
  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum?
      get type => _$this._type;
  set type(
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum?
              type) =>
      _$this._type = type;

  ListBuilder<
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner>?
      _features;
  ListBuilder<
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner>
      get features => _$this._features ??= new ListBuilder<
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner>();
  set features(
          ListBuilder<
                  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner>?
              features) =>
      _$this._features = features;

  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataBuilder() {
    MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData._defaults(
        this);
  }

  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _features = $v.features.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData build() =>
      _build();

  _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData _build() {
    _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData
              ._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData',
                  'type'),
              features: features.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'features';
        features.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
