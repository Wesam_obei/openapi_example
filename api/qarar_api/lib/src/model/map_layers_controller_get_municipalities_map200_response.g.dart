// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_municipalities_map200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetMunicipalitiesMap200Response
    extends MapLayersControllerGetMunicipalitiesMap200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$MapLayersControllerGetMunicipalitiesMap200Response(
          [void Function(
                  MapLayersControllerGetMunicipalitiesMap200ResponseBuilder)?
              updates]) =>
      (new MapLayersControllerGetMunicipalitiesMap200ResponseBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetMunicipalitiesMap200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(success,
        r'MapLayersControllerGetMunicipalitiesMap200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'MapLayersControllerGetMunicipalitiesMap200Response', 'data');
  }

  @override
  MapLayersControllerGetMunicipalitiesMap200Response rebuild(
          void Function(
                  MapLayersControllerGetMunicipalitiesMap200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetMunicipalitiesMap200ResponseBuilder toBuilder() =>
      new MapLayersControllerGetMunicipalitiesMap200ResponseBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapLayersControllerGetMunicipalitiesMap200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetMunicipalitiesMap200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class MapLayersControllerGetMunicipalitiesMap200ResponseBuilder
    implements
        Builder<MapLayersControllerGetMunicipalitiesMap200Response,
            MapLayersControllerGetMunicipalitiesMap200ResponseBuilder>,
        SuccessResponseBuilder {
  _$MapLayersControllerGetMunicipalitiesMap200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  MapLayersControllerGetMunicipalitiesMap200ResponseBuilder() {
    MapLayersControllerGetMunicipalitiesMap200Response._defaults(this);
  }

  MapLayersControllerGetMunicipalitiesMap200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      covariant MapLayersControllerGetMunicipalitiesMap200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapLayersControllerGetMunicipalitiesMap200Response;
  }

  @override
  void update(
      void Function(MapLayersControllerGetMunicipalitiesMap200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetMunicipalitiesMap200Response build() => _build();

  _$MapLayersControllerGetMunicipalitiesMap200Response _build() {
    final _$result = _$v ??
        new _$MapLayersControllerGetMunicipalitiesMap200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success,
                r'MapLayersControllerGetMunicipalitiesMap200Response',
                'success'),
            data: BuiltValueNullFieldError.checkNotNull(data,
                r'MapLayersControllerGetMunicipalitiesMap200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
