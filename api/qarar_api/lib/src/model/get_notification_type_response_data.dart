//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'get_notification_type_response_data.g.dart';

/// GetNotificationTypeResponseData
///
/// Properties:
/// * [id]
/// * [name]
@BuiltValue()
abstract class GetNotificationTypeResponseData
    implements
        Built<GetNotificationTypeResponseData,
            GetNotificationTypeResponseDataBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'name')
  String get name;

  GetNotificationTypeResponseData._();

  factory GetNotificationTypeResponseData(
          [void updates(GetNotificationTypeResponseDataBuilder b)]) =
      _$GetNotificationTypeResponseData;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(GetNotificationTypeResponseDataBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<GetNotificationTypeResponseData> get serializer =>
      _$GetNotificationTypeResponseDataSerializer();
}

class _$GetNotificationTypeResponseDataSerializer
    implements PrimitiveSerializer<GetNotificationTypeResponseData> {
  @override
  final Iterable<Type> types = const [
    GetNotificationTypeResponseData,
    _$GetNotificationTypeResponseData
  ];

  @override
  final String wireName = r'GetNotificationTypeResponseData';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    GetNotificationTypeResponseData object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    GetNotificationTypeResponseData object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required GetNotificationTypeResponseDataBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  GetNotificationTypeResponseData deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = GetNotificationTypeResponseDataBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
