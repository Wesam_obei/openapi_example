//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'user_dashboard_create_request.g.dart';

/// UserDashboardCreateRequest
///
/// Properties:
/// * [name]
@BuiltValue()
abstract class UserDashboardCreateRequest
    implements
        Built<UserDashboardCreateRequest, UserDashboardCreateRequestBuilder> {
  @BuiltValueField(wireName: r'name')
  String get name;

  UserDashboardCreateRequest._();

  factory UserDashboardCreateRequest(
          [void updates(UserDashboardCreateRequestBuilder b)]) =
      _$UserDashboardCreateRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(UserDashboardCreateRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<UserDashboardCreateRequest> get serializer =>
      _$UserDashboardCreateRequestSerializer();
}

class _$UserDashboardCreateRequestSerializer
    implements PrimitiveSerializer<UserDashboardCreateRequest> {
  @override
  final Iterable<Type> types = const [
    UserDashboardCreateRequest,
    _$UserDashboardCreateRequest
  ];

  @override
  final String wireName = r'UserDashboardCreateRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    UserDashboardCreateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    UserDashboardCreateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required UserDashboardCreateRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  UserDashboardCreateRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = UserDashboardCreateRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
