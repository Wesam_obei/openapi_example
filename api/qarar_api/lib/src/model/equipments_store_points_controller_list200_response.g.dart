// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'equipments_store_points_controller_list200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$EquipmentsStorePointsControllerList200Response
    extends EquipmentsStorePointsControllerList200Response {
  @override
  final bool success;
  @override
  final BuiltList<JsonObject> data;
  @override
  final PaginatedMetaDocumented meta;
  @override
  final PaginatedLinksDocumented links;

  factory _$EquipmentsStorePointsControllerList200Response(
          [void Function(EquipmentsStorePointsControllerList200ResponseBuilder)?
              updates]) =>
      (new EquipmentsStorePointsControllerList200ResponseBuilder()
            ..update(updates))
          ._build();

  _$EquipmentsStorePointsControllerList200Response._(
      {required this.success,
      required this.data,
      required this.meta,
      required this.links})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'EquipmentsStorePointsControllerList200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'EquipmentsStorePointsControllerList200Response', 'data');
    BuiltValueNullFieldError.checkNotNull(
        meta, r'EquipmentsStorePointsControllerList200Response', 'meta');
    BuiltValueNullFieldError.checkNotNull(
        links, r'EquipmentsStorePointsControllerList200Response', 'links');
  }

  @override
  EquipmentsStorePointsControllerList200Response rebuild(
          void Function(EquipmentsStorePointsControllerList200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  EquipmentsStorePointsControllerList200ResponseBuilder toBuilder() =>
      new EquipmentsStorePointsControllerList200ResponseBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EquipmentsStorePointsControllerList200Response &&
        success == other.success &&
        data == other.data &&
        meta == other.meta &&
        links == other.links;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jc(_$hash, meta.hashCode);
    _$hash = $jc(_$hash, links.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'EquipmentsStorePointsControllerList200Response')
          ..add('success', success)
          ..add('data', data)
          ..add('meta', meta)
          ..add('links', links))
        .toString();
  }
}

class EquipmentsStorePointsControllerList200ResponseBuilder
    implements
        Builder<EquipmentsStorePointsControllerList200Response,
            EquipmentsStorePointsControllerList200ResponseBuilder>,
        PaginatedDocumentedBuilder {
  _$EquipmentsStorePointsControllerList200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  ListBuilder<JsonObject>? _data;
  ListBuilder<JsonObject> get data =>
      _$this._data ??= new ListBuilder<JsonObject>();
  set data(covariant ListBuilder<JsonObject>? data) => _$this._data = data;

  PaginatedMetaDocumentedBuilder? _meta;
  PaginatedMetaDocumentedBuilder get meta =>
      _$this._meta ??= new PaginatedMetaDocumentedBuilder();
  set meta(covariant PaginatedMetaDocumentedBuilder? meta) =>
      _$this._meta = meta;

  PaginatedLinksDocumentedBuilder? _links;
  PaginatedLinksDocumentedBuilder get links =>
      _$this._links ??= new PaginatedLinksDocumentedBuilder();
  set links(covariant PaginatedLinksDocumentedBuilder? links) =>
      _$this._links = links;

  EquipmentsStorePointsControllerList200ResponseBuilder() {
    EquipmentsStorePointsControllerList200Response._defaults(this);
  }

  EquipmentsStorePointsControllerList200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data.toBuilder();
      _meta = $v.meta.toBuilder();
      _links = $v.links.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant EquipmentsStorePointsControllerList200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$EquipmentsStorePointsControllerList200Response;
  }

  @override
  void update(
      void Function(EquipmentsStorePointsControllerList200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  EquipmentsStorePointsControllerList200Response build() => _build();

  _$EquipmentsStorePointsControllerList200Response _build() {
    _$EquipmentsStorePointsControllerList200Response _$result;
    try {
      _$result = _$v ??
          new _$EquipmentsStorePointsControllerList200Response._(
              success: BuiltValueNullFieldError.checkNotNull(success,
                  r'EquipmentsStorePointsControllerList200Response', 'success'),
              data: data.build(),
              meta: meta.build(),
              links: links.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'data';
        data.build();
        _$failedField = 'meta';
        meta.build();
        _$failedField = 'links';
        links.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'EquipmentsStorePointsControllerList200Response',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
