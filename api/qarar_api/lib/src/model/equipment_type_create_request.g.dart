// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'equipment_type_create_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$EquipmentTypeCreateRequest extends EquipmentTypeCreateRequest {
  @override
  final String name;
  @override
  final String? fileToken;

  factory _$EquipmentTypeCreateRequest(
          [void Function(EquipmentTypeCreateRequestBuilder)? updates]) =>
      (new EquipmentTypeCreateRequestBuilder()..update(updates))._build();

  _$EquipmentTypeCreateRequest._({required this.name, this.fileToken})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        name, r'EquipmentTypeCreateRequest', 'name');
  }

  @override
  EquipmentTypeCreateRequest rebuild(
          void Function(EquipmentTypeCreateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  EquipmentTypeCreateRequestBuilder toBuilder() =>
      new EquipmentTypeCreateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EquipmentTypeCreateRequest &&
        name == other.name &&
        fileToken == other.fileToken;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, fileToken.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'EquipmentTypeCreateRequest')
          ..add('name', name)
          ..add('fileToken', fileToken))
        .toString();
  }
}

class EquipmentTypeCreateRequestBuilder
    implements
        Builder<EquipmentTypeCreateRequest, EquipmentTypeCreateRequestBuilder> {
  _$EquipmentTypeCreateRequest? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _fileToken;
  String? get fileToken => _$this._fileToken;
  set fileToken(String? fileToken) => _$this._fileToken = fileToken;

  EquipmentTypeCreateRequestBuilder() {
    EquipmentTypeCreateRequest._defaults(this);
  }

  EquipmentTypeCreateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _fileToken = $v.fileToken;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(EquipmentTypeCreateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$EquipmentTypeCreateRequest;
  }

  @override
  void update(void Function(EquipmentTypeCreateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  EquipmentTypeCreateRequest build() => _build();

  _$EquipmentTypeCreateRequest _build() {
    final _$result = _$v ??
        new _$EquipmentTypeCreateRequest._(
            name: BuiltValueNullFieldError.checkNotNull(
                name, r'EquipmentTypeCreateRequest', 'name'),
            fileToken: fileToken);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
