// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'refresh_token_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$RefreshTokenResponse extends RefreshTokenResponse {
  @override
  final String accessToken;
  @override
  final DateTime accessTokenExpiredDate;

  factory _$RefreshTokenResponse(
          [void Function(RefreshTokenResponseBuilder)? updates]) =>
      (new RefreshTokenResponseBuilder()..update(updates))._build();

  _$RefreshTokenResponse._(
      {required this.accessToken, required this.accessTokenExpiredDate})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        accessToken, r'RefreshTokenResponse', 'accessToken');
    BuiltValueNullFieldError.checkNotNull(accessTokenExpiredDate,
        r'RefreshTokenResponse', 'accessTokenExpiredDate');
  }

  @override
  RefreshTokenResponse rebuild(
          void Function(RefreshTokenResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RefreshTokenResponseBuilder toBuilder() =>
      new RefreshTokenResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RefreshTokenResponse &&
        accessToken == other.accessToken &&
        accessTokenExpiredDate == other.accessTokenExpiredDate;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, accessToken.hashCode);
    _$hash = $jc(_$hash, accessTokenExpiredDate.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'RefreshTokenResponse')
          ..add('accessToken', accessToken)
          ..add('accessTokenExpiredDate', accessTokenExpiredDate))
        .toString();
  }
}

class RefreshTokenResponseBuilder
    implements Builder<RefreshTokenResponse, RefreshTokenResponseBuilder> {
  _$RefreshTokenResponse? _$v;

  String? _accessToken;
  String? get accessToken => _$this._accessToken;
  set accessToken(String? accessToken) => _$this._accessToken = accessToken;

  DateTime? _accessTokenExpiredDate;
  DateTime? get accessTokenExpiredDate => _$this._accessTokenExpiredDate;
  set accessTokenExpiredDate(DateTime? accessTokenExpiredDate) =>
      _$this._accessTokenExpiredDate = accessTokenExpiredDate;

  RefreshTokenResponseBuilder() {
    RefreshTokenResponse._defaults(this);
  }

  RefreshTokenResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _accessToken = $v.accessToken;
      _accessTokenExpiredDate = $v.accessTokenExpiredDate;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RefreshTokenResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$RefreshTokenResponse;
  }

  @override
  void update(void Function(RefreshTokenResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  RefreshTokenResponse build() => _build();

  _$RefreshTokenResponse _build() {
    final _$result = _$v ??
        new _$RefreshTokenResponse._(
            accessToken: BuiltValueNullFieldError.checkNotNull(
                accessToken, r'RefreshTokenResponse', 'accessToken'),
            accessTokenExpiredDate: BuiltValueNullFieldError.checkNotNull(
                accessTokenExpiredDate,
                r'RefreshTokenResponse',
                'accessTokenExpiredDate'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
