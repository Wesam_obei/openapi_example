// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_light_poles_map200_response_all_of_data_features_inner.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum
    _$mapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature =
    const MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum
        ._('feature');

MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum
    _$mapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'feature':
      return _$mapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature;
    default:
      return _$mapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature;
  }
}

final BuiltSet<
        MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum>
    _$mapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum>(const <MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum>[
  _$mapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature,
]);

Serializer<
        MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum>
    _$mapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer =
    new _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer();

class _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'feature': 'Feature',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'Feature': 'feature',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum';

  @override
  Object serialize(
          Serializers serializers,
          MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum
              object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum
      deserialize(Serializers serializers, Object serialized,
              {FullType specifiedType = FullType.unspecified}) =>
          MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .valueOf(_fromWire[serialized] ??
                  (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner
    extends MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner {
  @override
  final MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum
      type;
  @override
  final int id;
  @override
  final JsonObject? geometry;
  @override
  final MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerProperties
      properties;

  factory _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner(
          [void Function(
                  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerBuilder)?
              updates]) =>
      (new MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner._(
      {required this.type,
      required this.id,
      this.geometry,
      required this.properties})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        type,
        r'MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner',
        'type');
    BuiltValueNullFieldError.checkNotNull(
        id,
        r'MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner',
        'id');
    BuiltValueNullFieldError.checkNotNull(
        properties,
        r'MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner',
        'properties');
  }

  @override
  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner rebuild(
          void Function(
                  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerBuilder
      toBuilder() =>
          new MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner &&
        type == other.type &&
        id == other.id &&
        geometry == other.geometry &&
        properties == other.properties;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, geometry.hashCode);
    _$hash = $jc(_$hash, properties.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner')
          ..add('type', type)
          ..add('id', id)
          ..add('geometry', geometry)
          ..add('properties', properties))
        .toString();
  }
}

class MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerBuilder
    implements
        Builder<
            MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner,
            MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerBuilder> {
  _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner? _$v;

  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum?
      _type;
  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum?
      get type => _$this._type;
  set type(
          MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerTypeEnum?
              type) =>
      _$this._type = type;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  JsonObject? _geometry;
  JsonObject? get geometry => _$this._geometry;
  set geometry(JsonObject? geometry) => _$this._geometry = geometry;

  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder?
      _properties;
  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      get properties => _$this._properties ??=
          new MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder();
  set properties(
          MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder?
              properties) =>
      _$this._properties = properties;

  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerBuilder() {
    MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner
        ._defaults(this);
  }

  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _id = $v.id;
      _geometry = $v.geometry;
      _properties = $v.properties.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner
      build() => _build();

  _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner
      _build() {
    _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner
        _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner',
                  'type'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id,
                  r'MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner',
                  'id'),
              geometry: geometry,
              properties: properties.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'properties';
        properties.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
