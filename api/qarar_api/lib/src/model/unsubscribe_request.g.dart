// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'unsubscribe_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$UnsubscribeRequest extends UnsubscribeRequest {
  @override
  final String sessionId;

  factory _$UnsubscribeRequest(
          [void Function(UnsubscribeRequestBuilder)? updates]) =>
      (new UnsubscribeRequestBuilder()..update(updates))._build();

  _$UnsubscribeRequest._({required this.sessionId}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        sessionId, r'UnsubscribeRequest', 'sessionId');
  }

  @override
  UnsubscribeRequest rebuild(
          void Function(UnsubscribeRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UnsubscribeRequestBuilder toBuilder() =>
      new UnsubscribeRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UnsubscribeRequest && sessionId == other.sessionId;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, sessionId.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'UnsubscribeRequest')
          ..add('sessionId', sessionId))
        .toString();
  }
}

class UnsubscribeRequestBuilder
    implements Builder<UnsubscribeRequest, UnsubscribeRequestBuilder> {
  _$UnsubscribeRequest? _$v;

  String? _sessionId;
  String? get sessionId => _$this._sessionId;
  set sessionId(String? sessionId) => _$this._sessionId = sessionId;

  UnsubscribeRequestBuilder() {
    UnsubscribeRequest._defaults(this);
  }

  UnsubscribeRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _sessionId = $v.sessionId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UnsubscribeRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$UnsubscribeRequest;
  }

  @override
  void update(void Function(UnsubscribeRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  UnsubscribeRequest build() => _build();

  _$UnsubscribeRequest _build() {
    final _$result = _$v ??
        new _$UnsubscribeRequest._(
            sessionId: BuiltValueNullFieldError.checkNotNull(
                sessionId, r'UnsubscribeRequest', 'sessionId'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
