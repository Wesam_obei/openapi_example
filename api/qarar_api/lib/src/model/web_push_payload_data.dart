//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/action.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'web_push_payload_data.g.dart';

/// WebPushPayloadData
///
/// Properties:
/// * [action]
@BuiltValue()
abstract class WebPushPayloadData
    implements Built<WebPushPayloadData, WebPushPayloadDataBuilder> {
  @BuiltValueField(wireName: r'action')
  Action get action;

  WebPushPayloadData._();

  factory WebPushPayloadData([void updates(WebPushPayloadDataBuilder b)]) =
      _$WebPushPayloadData;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(WebPushPayloadDataBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<WebPushPayloadData> get serializer =>
      _$WebPushPayloadDataSerializer();
}

class _$WebPushPayloadDataSerializer
    implements PrimitiveSerializer<WebPushPayloadData> {
  @override
  final Iterable<Type> types = const [WebPushPayloadData, _$WebPushPayloadData];

  @override
  final String wireName = r'WebPushPayloadData';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    WebPushPayloadData object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'action';
    yield serializers.serialize(
      object.action,
      specifiedType: const FullType(Action),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    WebPushPayloadData object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required WebPushPayloadDataBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'action':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(Action),
          ) as Action;
          result.action.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  WebPushPayloadData deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = WebPushPayloadDataBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
