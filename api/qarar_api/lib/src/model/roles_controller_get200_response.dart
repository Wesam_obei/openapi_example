//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/success_response.dart';
import 'package:qarar_api/src/model/get_role_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'roles_controller_get200_response.g.dart';

/// My custom description
///
/// Properties:
/// * [success]
/// * [data]
@BuiltValue()
abstract class RolesControllerGet200Response
    implements
        SuccessResponse,
        Built<RolesControllerGet200Response,
            RolesControllerGet200ResponseBuilder> {
  RolesControllerGet200Response._();

  factory RolesControllerGet200Response(
          [void updates(RolesControllerGet200ResponseBuilder b)]) =
      _$RolesControllerGet200Response;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(RolesControllerGet200ResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<RolesControllerGet200Response> get serializer =>
      _$RolesControllerGet200ResponseSerializer();
}

class _$RolesControllerGet200ResponseSerializer
    implements PrimitiveSerializer<RolesControllerGet200Response> {
  @override
  final Iterable<Type> types = const [
    RolesControllerGet200Response,
    _$RolesControllerGet200Response
  ];

  @override
  final String wireName = r'RolesControllerGet200Response';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    RolesControllerGet200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'data';
    yield serializers.serialize(
      object.data,
      specifiedType: const FullType(JsonObject),
    );
    yield r'success';
    yield serializers.serialize(
      object.success,
      specifiedType: const FullType(bool),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    RolesControllerGet200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required RolesControllerGet200ResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'data':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.data = valueDes;
          break;
        case r'success':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.success = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  RolesControllerGet200Response deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = RolesControllerGet200ResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
