// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'form_duplicate_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$FormDuplicateRequest extends FormDuplicateRequest {
  @override
  final num fromId;
  @override
  final String name;
  @override
  final num? fileId;
  @override
  final String? fileToken;

  factory _$FormDuplicateRequest(
          [void Function(FormDuplicateRequestBuilder)? updates]) =>
      (new FormDuplicateRequestBuilder()..update(updates))._build();

  _$FormDuplicateRequest._(
      {required this.fromId, required this.name, this.fileId, this.fileToken})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        fromId, r'FormDuplicateRequest', 'fromId');
    BuiltValueNullFieldError.checkNotNull(
        name, r'FormDuplicateRequest', 'name');
  }

  @override
  FormDuplicateRequest rebuild(
          void Function(FormDuplicateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  FormDuplicateRequestBuilder toBuilder() =>
      new FormDuplicateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is FormDuplicateRequest &&
        fromId == other.fromId &&
        name == other.name &&
        fileId == other.fileId &&
        fileToken == other.fileToken;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, fromId.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, fileId.hashCode);
    _$hash = $jc(_$hash, fileToken.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'FormDuplicateRequest')
          ..add('fromId', fromId)
          ..add('name', name)
          ..add('fileId', fileId)
          ..add('fileToken', fileToken))
        .toString();
  }
}

class FormDuplicateRequestBuilder
    implements Builder<FormDuplicateRequest, FormDuplicateRequestBuilder> {
  _$FormDuplicateRequest? _$v;

  num? _fromId;
  num? get fromId => _$this._fromId;
  set fromId(num? fromId) => _$this._fromId = fromId;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  num? _fileId;
  num? get fileId => _$this._fileId;
  set fileId(num? fileId) => _$this._fileId = fileId;

  String? _fileToken;
  String? get fileToken => _$this._fileToken;
  set fileToken(String? fileToken) => _$this._fileToken = fileToken;

  FormDuplicateRequestBuilder() {
    FormDuplicateRequest._defaults(this);
  }

  FormDuplicateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _fromId = $v.fromId;
      _name = $v.name;
      _fileId = $v.fileId;
      _fileToken = $v.fileToken;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(FormDuplicateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$FormDuplicateRequest;
  }

  @override
  void update(void Function(FormDuplicateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  FormDuplicateRequest build() => _build();

  _$FormDuplicateRequest _build() {
    final _$result = _$v ??
        new _$FormDuplicateRequest._(
            fromId: BuiltValueNullFieldError.checkNotNull(
                fromId, r'FormDuplicateRequest', 'fromId'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, r'FormDuplicateRequest', 'name'),
            fileId: fileId,
            fileToken: fileToken);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
