// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'status_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const StatusResponseNameEnum _$statusResponseNameEnum_draft =
    const StatusResponseNameEnum._('draft');
const StatusResponseNameEnum _$statusResponseNameEnum_submitted =
    const StatusResponseNameEnum._('submitted');
const StatusResponseNameEnum _$statusResponseNameEnum_approved =
    const StatusResponseNameEnum._('approved');
const StatusResponseNameEnum _$statusResponseNameEnum_needChange =
    const StatusResponseNameEnum._('needChange');

StatusResponseNameEnum _$statusResponseNameEnumValueOf(String name) {
  switch (name) {
    case 'draft':
      return _$statusResponseNameEnum_draft;
    case 'submitted':
      return _$statusResponseNameEnum_submitted;
    case 'approved':
      return _$statusResponseNameEnum_approved;
    case 'needChange':
      return _$statusResponseNameEnum_needChange;
    default:
      return _$statusResponseNameEnum_needChange;
  }
}

final BuiltSet<StatusResponseNameEnum> _$statusResponseNameEnumValues =
    new BuiltSet<StatusResponseNameEnum>(const <StatusResponseNameEnum>[
  _$statusResponseNameEnum_draft,
  _$statusResponseNameEnum_submitted,
  _$statusResponseNameEnum_approved,
  _$statusResponseNameEnum_needChange,
]);

Serializer<StatusResponseNameEnum> _$statusResponseNameEnumSerializer =
    new _$StatusResponseNameEnumSerializer();

class _$StatusResponseNameEnumSerializer
    implements PrimitiveSerializer<StatusResponseNameEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'draft': 'draft',
    'submitted': 'submitted',
    'approved': 'approved',
    'needChange': 'need-change',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'draft': 'draft',
    'submitted': 'submitted',
    'approved': 'approved',
    'need-change': 'needChange',
  };

  @override
  final Iterable<Type> types = const <Type>[StatusResponseNameEnum];
  @override
  final String wireName = 'StatusResponseNameEnum';

  @override
  Object serialize(Serializers serializers, StatusResponseNameEnum object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  StatusResponseNameEnum deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      StatusResponseNameEnum.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

class _$StatusResponse extends StatusResponse {
  @override
  final num id;
  @override
  final StatusResponseNameEnum name;

  factory _$StatusResponse([void Function(StatusResponseBuilder)? updates]) =>
      (new StatusResponseBuilder()..update(updates))._build();

  _$StatusResponse._({required this.id, required this.name}) : super._() {
    BuiltValueNullFieldError.checkNotNull(id, r'StatusResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(name, r'StatusResponse', 'name');
  }

  @override
  StatusResponse rebuild(void Function(StatusResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  StatusResponseBuilder toBuilder() =>
      new StatusResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is StatusResponse && id == other.id && name == other.name;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'StatusResponse')
          ..add('id', id)
          ..add('name', name))
        .toString();
  }
}

class StatusResponseBuilder
    implements Builder<StatusResponse, StatusResponseBuilder> {
  _$StatusResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  StatusResponseNameEnum? _name;
  StatusResponseNameEnum? get name => _$this._name;
  set name(StatusResponseNameEnum? name) => _$this._name = name;

  StatusResponseBuilder() {
    StatusResponse._defaults(this);
  }

  StatusResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(StatusResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$StatusResponse;
  }

  @override
  void update(void Function(StatusResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  StatusResponse build() => _build();

  _$StatusResponse _build() {
    final _$result = _$v ??
        new _$StatusResponse._(
            id: BuiltValueNullFieldError.checkNotNull(
                id, r'StatusResponse', 'id'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, r'StatusResponse', 'name'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
