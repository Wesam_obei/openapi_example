//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'permissions_enum.g.dart';

class PermissionsEnum extends EnumClass {
  @BuiltValueEnumConst(wireName: r'create_user')
  static const PermissionsEnum createUser = _$createUser;
  @BuiltValueEnumConst(wireName: r'view_user')
  static const PermissionsEnum viewUser = _$viewUser;
  @BuiltValueEnumConst(wireName: r'update_user')
  static const PermissionsEnum updateUser = _$updateUser;
  @BuiltValueEnumConst(wireName: r'delete_user')
  static const PermissionsEnum deleteUser = _$deleteUser;
  @BuiltValueEnumConst(wireName: r'create_role')
  static const PermissionsEnum createRole = _$createRole;
  @BuiltValueEnumConst(wireName: r'view_role')
  static const PermissionsEnum viewRole = _$viewRole;
  @BuiltValueEnumConst(wireName: r'update_role')
  static const PermissionsEnum updateRole = _$updateRole;
  @BuiltValueEnumConst(wireName: r'delete_role')
  static const PermissionsEnum deleteRole = _$deleteRole;
  @BuiltValueEnumConst(wireName: r'edit_user_dashboard')
  static const PermissionsEnum editUserDashboard = _$editUserDashboard;
  @BuiltValueEnumConst(wireName: r'view_user_dashboard')
  static const PermissionsEnum viewUserDashboard = _$viewUserDashboard;
  @BuiltValueEnumConst(wireName: r'delete_user_dashboard')
  static const PermissionsEnum deleteUserDashboard = _$deleteUserDashboard;
  @BuiltValueEnumConst(wireName: r'view_maater_layer')
  static const PermissionsEnum viewMaaterLayer = _$viewMaaterLayer;
  @BuiltValueEnumConst(wireName: r'view_incidents940_layer')
  static const PermissionsEnum viewIncidents940Layer = _$viewIncidents940Layer;
  @BuiltValueEnumConst(wireName: r'view_construction_license_layer')
  static const PermissionsEnum viewConstructionLicenseLayer =
      _$viewConstructionLicenseLayer;
  @BuiltValueEnumConst(wireName: r'view_light_poles_layer')
  static const PermissionsEnum viewLightPolesLayer = _$viewLightPolesLayer;
  @BuiltValueEnumConst(wireName: r'view_cables_layer')
  static const PermissionsEnum viewCablesLayer = _$viewCablesLayer;
  @BuiltValueEnumConst(wireName: r'view_electrical_facilities_layer')
  static const PermissionsEnum viewElectricalFacilitiesLayer =
      _$viewElectricalFacilitiesLayer;
  @BuiltValueEnumConst(wireName: r'view_bridges_tunnels_sites_layer')
  static const PermissionsEnum viewBridgesTunnelsSitesLayer =
      _$viewBridgesTunnelsSitesLayer;
  @BuiltValueEnumConst(wireName: r'view_store_points_layer')
  static const PermissionsEnum viewStorePointsLayer = _$viewStorePointsLayer;
  @BuiltValueEnumConst(wireName: r'view_response_layer')
  static const PermissionsEnum viewResponseLayer = _$viewResponseLayer;
  @BuiltValueEnumConst(wireName: r'view_critical_points_layer')
  static const PermissionsEnum viewCriticalPointsLayer =
      _$viewCriticalPointsLayer;
  @BuiltValueEnumConst(wireName: r'view_transport_track_layer')
  static const PermissionsEnum viewTransportTrackLayer =
      _$viewTransportTrackLayer;
  @BuiltValueEnumConst(wireName: r'view_street_naming_layer')
  static const PermissionsEnum viewStreetNamingLayer = _$viewStreetNamingLayer;
  @BuiltValueEnumConst(wireName: r'view_municipality_boundary_layer')
  static const PermissionsEnum viewMunicipalityBoundaryLayer =
      _$viewMunicipalityBoundaryLayer;
  @BuiltValueEnumConst(wireName: r'view_land_base_parcel_layer')
  static const PermissionsEnum viewLandBaseParcelLayer =
      _$viewLandBaseParcelLayer;
  @BuiltValueEnumConst(wireName: r'view_district_boundary_layer')
  static const PermissionsEnum viewDistrictBoundaryLayer =
      _$viewDistrictBoundaryLayer;
  @BuiltValueEnumConst(wireName: r'view_urban_area_boundary_layer')
  static const PermissionsEnum viewUrbanAreaBoundaryLayer =
      _$viewUrbanAreaBoundaryLayer;
  @BuiltValueEnumConst(wireName: r'view_plan_data_layer')
  static const PermissionsEnum viewPlanDataLayer = _$viewPlanDataLayer;
  @BuiltValueEnumConst(wireName: r'view_manhole_layer')
  static const PermissionsEnum viewManholeLayer = _$viewManholeLayer;
  @BuiltValueEnumConst(wireName: r'view_sea_out_layer')
  static const PermissionsEnum viewSeaOutLayer = _$viewSeaOutLayer;
  @BuiltValueEnumConst(wireName: r'create_plan')
  static const PermissionsEnum createPlan = _$createPlan;
  @BuiltValueEnumConst(wireName: r'view_plan')
  static const PermissionsEnum viewPlan = _$viewPlan;
  @BuiltValueEnumConst(wireName: r'update_plan')
  static const PermissionsEnum updatePlan = _$updatePlan;
  @BuiltValueEnumConst(wireName: r'delete_plan')
  static const PermissionsEnum deletePlan = _$deletePlan;
  @BuiltValueEnumConst(wireName: r'create_equipment_store_point')
  static const PermissionsEnum createEquipmentStorePoint =
      _$createEquipmentStorePoint;
  @BuiltValueEnumConst(wireName: r'view_equipment_store_point')
  static const PermissionsEnum viewEquipmentStorePoint =
      _$viewEquipmentStorePoint;
  @BuiltValueEnumConst(wireName: r'update_equipment_store_point')
  static const PermissionsEnum updateEquipmentStorePoint =
      _$updateEquipmentStorePoint;
  @BuiltValueEnumConst(wireName: r'delete_equipment_store_point')
  static const PermissionsEnum deleteEquipmentStorePoint =
      _$deleteEquipmentStorePoint;
  @BuiltValueEnumConst(wireName: r'create_equipment_type')
  static const PermissionsEnum createEquipmentType = _$createEquipmentType;
  @BuiltValueEnumConst(wireName: r'view_equipment_type')
  static const PermissionsEnum viewEquipmentType = _$viewEquipmentType;
  @BuiltValueEnumConst(wireName: r'update_equipment_type')
  static const PermissionsEnum updateEquipmentType = _$updateEquipmentType;
  @BuiltValueEnumConst(wireName: r'delete_equipment_type')
  static const PermissionsEnum deleteEquipmentType = _$deleteEquipmentType;
  @BuiltValueEnumConst(wireName: r'create_equipment')
  static const PermissionsEnum createEquipment = _$createEquipment;
  @BuiltValueEnumConst(wireName: r'view_equipment')
  static const PermissionsEnum viewEquipment = _$viewEquipment;
  @BuiltValueEnumConst(wireName: r'update_equipment')
  static const PermissionsEnum updateEquipment = _$updateEquipment;
  @BuiltValueEnumConst(wireName: r'delete_equipment')
  static const PermissionsEnum deleteEquipment = _$deleteEquipment;
  @BuiltValueEnumConst(wireName: r'create_form')
  static const PermissionsEnum createForm = _$createForm;
  @BuiltValueEnumConst(wireName: r'update_form')
  static const PermissionsEnum updateForm = _$updateForm;
  @BuiltValueEnumConst(wireName: r'delete_form')
  static const PermissionsEnum deleteForm = _$deleteForm;
  @BuiltValueEnumConst(wireName: r'view_form')
  static const PermissionsEnum viewForm = _$viewForm;
  @BuiltValueEnumConst(wireName: r'create_survey')
  static const PermissionsEnum createSurvey = _$createSurvey;
  @BuiltValueEnumConst(wireName: r'update_survey')
  static const PermissionsEnum updateSurvey = _$updateSurvey;
  @BuiltValueEnumConst(wireName: r'delete_survey')
  static const PermissionsEnum deleteSurvey = _$deleteSurvey;
  @BuiltValueEnumConst(wireName: r'view_survey')
  static const PermissionsEnum viewSurvey = _$viewSurvey;
  @BuiltValueEnumConst(wireName: r'respond_survey_for_entity')
  static const PermissionsEnum respondSurveyForEntity =
      _$respondSurveyForEntity;
  @BuiltValueEnumConst(wireName: r'survey_verified_responses')
  static const PermissionsEnum surveyVerifiedResponses =
      _$surveyVerifiedResponses;
  @BuiltValueEnumConst(wireName: r'create_entity')
  static const PermissionsEnum createEntity = _$createEntity;
  @BuiltValueEnumConst(wireName: r'update_entity')
  static const PermissionsEnum updateEntity = _$updateEntity;
  @BuiltValueEnumConst(wireName: r'delete_entity')
  static const PermissionsEnum deleteEntity = _$deleteEntity;
  @BuiltValueEnumConst(wireName: r'view_entity', fallback: true)
  static const PermissionsEnum viewEntity = _$viewEntity;

  static Serializer<PermissionsEnum> get serializer =>
      _$permissionsEnumSerializer;

  const PermissionsEnum._(String name) : super(name);

  static BuiltSet<PermissionsEnum> get values => _$values;
  static PermissionsEnum valueOf(String name) => _$valueOf(name);
}

/// Optionally, enum_class can generate a mixin to go with your enum for use
/// with Angular. It exposes your enum constants as getters. So, if you mix it
/// in to your Dart component class, the values become available to the
/// corresponding Angular template.
///
/// Trigger mixin generation by writing a line like this one next to your enum.
abstract class PermissionsEnumMixin = Object with _$PermissionsEnumMixin;
