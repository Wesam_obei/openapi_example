// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_street_names_map200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetStreetNamesMap200Response
    extends MapLayersControllerGetStreetNamesMap200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$MapLayersControllerGetStreetNamesMap200Response(
          [void Function(
                  MapLayersControllerGetStreetNamesMap200ResponseBuilder)?
              updates]) =>
      (new MapLayersControllerGetStreetNamesMap200ResponseBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetStreetNamesMap200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'MapLayersControllerGetStreetNamesMap200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'MapLayersControllerGetStreetNamesMap200Response', 'data');
  }

  @override
  MapLayersControllerGetStreetNamesMap200Response rebuild(
          void Function(MapLayersControllerGetStreetNamesMap200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetStreetNamesMap200ResponseBuilder toBuilder() =>
      new MapLayersControllerGetStreetNamesMap200ResponseBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapLayersControllerGetStreetNamesMap200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetStreetNamesMap200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class MapLayersControllerGetStreetNamesMap200ResponseBuilder
    implements
        Builder<MapLayersControllerGetStreetNamesMap200Response,
            MapLayersControllerGetStreetNamesMap200ResponseBuilder>,
        SuccessResponseBuilder {
  _$MapLayersControllerGetStreetNamesMap200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  MapLayersControllerGetStreetNamesMap200ResponseBuilder() {
    MapLayersControllerGetStreetNamesMap200Response._defaults(this);
  }

  MapLayersControllerGetStreetNamesMap200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      covariant MapLayersControllerGetStreetNamesMap200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapLayersControllerGetStreetNamesMap200Response;
  }

  @override
  void update(
      void Function(MapLayersControllerGetStreetNamesMap200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetStreetNamesMap200Response build() => _build();

  _$MapLayersControllerGetStreetNamesMap200Response _build() {
    final _$result = _$v ??
        new _$MapLayersControllerGetStreetNamesMap200Response._(
            success: BuiltValueNullFieldError.checkNotNull(success,
                r'MapLayersControllerGetStreetNamesMap200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(data,
                r'MapLayersControllerGetStreetNamesMap200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
