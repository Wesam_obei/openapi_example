// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_cables_map200_response_all_of_data_features_inner.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum
    _$mapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature =
    const MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum
        ._('feature');

MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum
    _$mapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'feature':
      return _$mapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature;
    default:
      return _$mapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature;
  }
}

final BuiltSet<
        MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum>
    _$mapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum>(const <MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum>[
  _$mapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature,
]);

Serializer<
        MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum>
    _$mapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer =
    new _$MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer();

class _$MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'feature': 'Feature',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'Feature': 'feature',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum';

  @override
  Object serialize(
          Serializers serializers,
          MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum
              object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum
      deserialize(Serializers serializers, Object serialized,
              {FullType specifiedType = FullType.unspecified}) =>
          MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .valueOf(_fromWire[serialized] ??
                  (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner
    extends MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner {
  @override
  final MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum
      type;
  @override
  final int id;
  @override
  final JsonObject? geometry;
  @override
  final MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerProperties
      properties;

  factory _$MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner(
          [void Function(
                  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerBuilder)?
              updates]) =>
      (new MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner._(
      {required this.type,
      required this.id,
      this.geometry,
      required this.properties})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        type,
        r'MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner',
        'type');
    BuiltValueNullFieldError.checkNotNull(
        id,
        r'MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner',
        'id');
    BuiltValueNullFieldError.checkNotNull(
        properties,
        r'MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner',
        'properties');
  }

  @override
  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner rebuild(
          void Function(
                  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerBuilder
      toBuilder() =>
          new MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner &&
        type == other.type &&
        id == other.id &&
        geometry == other.geometry &&
        properties == other.properties;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, geometry.hashCode);
    _$hash = $jc(_$hash, properties.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner')
          ..add('type', type)
          ..add('id', id)
          ..add('geometry', geometry)
          ..add('properties', properties))
        .toString();
  }
}

class MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerBuilder
    implements
        Builder<
            MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner,
            MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerBuilder> {
  _$MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner? _$v;

  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum?
      _type;
  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum?
      get type => _$this._type;
  set type(
          MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerTypeEnum?
              type) =>
      _$this._type = type;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  JsonObject? _geometry;
  JsonObject? get geometry => _$this._geometry;
  set geometry(JsonObject? geometry) => _$this._geometry = geometry;

  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder?
      _properties;
  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      get properties => _$this._properties ??=
          new MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder();
  set properties(
          MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder?
              properties) =>
      _$this._properties = properties;

  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerBuilder() {
    MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner._defaults(
        this);
  }

  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _id = $v.id;
      _geometry = $v.geometry;
      _properties = $v.properties.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner build() =>
      _build();

  _$MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner _build() {
    _$MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner',
                  'type'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id,
                  r'MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner',
                  'id'),
              geometry: geometry,
              properties: properties.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'properties';
        properties.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
