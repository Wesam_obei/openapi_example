//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/get_entity_data_response.dart';
import 'package:qarar_api/src/model/permission_data_response.dart';
import 'package:qarar_api/src/model/get_raqmy_info.dart';
import 'package:qarar_api/src/model/get_role_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'profile_response.g.dart';

/// ProfileResponse
///
/// Properties:
/// * [id]
/// * [email]
/// * [fullName]
/// * [createdAt]
/// * [isActive]
/// * [roles]
/// * [permissions]
/// * [entities]
/// * [raqmyInfo]
@BuiltValue()
abstract class ProfileResponse
    implements Built<ProfileResponse, ProfileResponseBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'email')
  String get email;

  @BuiltValueField(wireName: r'full_name')
  String get fullName;

  @BuiltValueField(wireName: r'created_at')
  DateTime get createdAt;

  @BuiltValueField(wireName: r'is_active')
  bool get isActive;

  @BuiltValueField(wireName: r'roles')
  BuiltList<GetRoleDataResponse> get roles;

  @BuiltValueField(wireName: r'permissions')
  BuiltList<PermissionDataResponse> get permissions;

  @BuiltValueField(wireName: r'entities')
  BuiltList<GetEntityDataResponse> get entities;

  @BuiltValueField(wireName: r'raqmyInfo')
  GetRaqmyInfo get raqmyInfo;

  ProfileResponse._();

  factory ProfileResponse([void updates(ProfileResponseBuilder b)]) =
      _$ProfileResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ProfileResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ProfileResponse> get serializer =>
      _$ProfileResponseSerializer();
}

class _$ProfileResponseSerializer
    implements PrimitiveSerializer<ProfileResponse> {
  @override
  final Iterable<Type> types = const [ProfileResponse, _$ProfileResponse];

  @override
  final String wireName = r'ProfileResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ProfileResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'email';
    yield serializers.serialize(
      object.email,
      specifiedType: const FullType(String),
    );
    yield r'full_name';
    yield serializers.serialize(
      object.fullName,
      specifiedType: const FullType(String),
    );
    yield r'created_at';
    yield serializers.serialize(
      object.createdAt,
      specifiedType: const FullType(DateTime),
    );
    yield r'is_active';
    yield serializers.serialize(
      object.isActive,
      specifiedType: const FullType(bool),
    );
    yield r'roles';
    yield serializers.serialize(
      object.roles,
      specifiedType: const FullType(BuiltList, [FullType(GetRoleDataResponse)]),
    );
    yield r'permissions';
    yield serializers.serialize(
      object.permissions,
      specifiedType:
          const FullType(BuiltList, [FullType(PermissionDataResponse)]),
    );
    yield r'entities';
    yield serializers.serialize(
      object.entities,
      specifiedType:
          const FullType(BuiltList, [FullType(GetEntityDataResponse)]),
    );
    yield r'raqmyInfo';
    yield serializers.serialize(
      object.raqmyInfo,
      specifiedType: const FullType(GetRaqmyInfo),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ProfileResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ProfileResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'email':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.email = valueDes;
          break;
        case r'full_name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.fullName = valueDes;
          break;
        case r'created_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.createdAt = valueDes;
          break;
        case r'is_active':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.isActive = valueDes;
          break;
        case r'roles':
          final valueDes = serializers.deserialize(
            value,
            specifiedType:
                const FullType(BuiltList, [FullType(GetRoleDataResponse)]),
          ) as BuiltList<GetRoleDataResponse>;
          result.roles.replace(valueDes);
          break;
        case r'permissions':
          final valueDes = serializers.deserialize(
            value,
            specifiedType:
                const FullType(BuiltList, [FullType(PermissionDataResponse)]),
          ) as BuiltList<PermissionDataResponse>;
          result.permissions.replace(valueDes);
          break;
        case r'entities':
          final valueDes = serializers.deserialize(
            value,
            specifiedType:
                const FullType(BuiltList, [FullType(GetEntityDataResponse)]),
          ) as BuiltList<GetEntityDataResponse>;
          result.entities.replace(valueDes);
          break;
        case r'raqmyInfo':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetRaqmyInfo),
          ) as GetRaqmyInfo;
          result.raqmyInfo.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ProfileResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ProfileResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
