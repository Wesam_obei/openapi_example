//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/get_user_data_response.dart';
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/get_form_component_data_response.dart';
import 'package:qarar_api/src/model/get_file_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'get_form_data_response.g.dart';

/// GetFormDataResponse
///
/// Properties:
/// * [id]
/// * [name]
/// * [description]
/// * [isActive]
/// * [editable]
/// * [createdAt]
/// * [file]
/// * [creator]
/// * [children]
@BuiltValue()
abstract class GetFormDataResponse
    implements Built<GetFormDataResponse, GetFormDataResponseBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'description')
  String get description;

  @BuiltValueField(wireName: r'is_active')
  bool get isActive;

  @BuiltValueField(wireName: r'editable')
  bool get editable;

  @BuiltValueField(wireName: r'created_at')
  DateTime get createdAt;

  @BuiltValueField(wireName: r'file')
  GetFileDataResponse get file;

  @BuiltValueField(wireName: r'creator')
  GetUserDataResponse get creator;

  @BuiltValueField(wireName: r'children')
  BuiltList<GetFormComponentDataResponse> get children;

  GetFormDataResponse._();

  factory GetFormDataResponse([void updates(GetFormDataResponseBuilder b)]) =
      _$GetFormDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(GetFormDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<GetFormDataResponse> get serializer =>
      _$GetFormDataResponseSerializer();
}

class _$GetFormDataResponseSerializer
    implements PrimitiveSerializer<GetFormDataResponse> {
  @override
  final Iterable<Type> types = const [
    GetFormDataResponse,
    _$GetFormDataResponse
  ];

  @override
  final String wireName = r'GetFormDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    GetFormDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    yield r'description';
    yield serializers.serialize(
      object.description,
      specifiedType: const FullType(String),
    );
    yield r'is_active';
    yield serializers.serialize(
      object.isActive,
      specifiedType: const FullType(bool),
    );
    yield r'editable';
    yield serializers.serialize(
      object.editable,
      specifiedType: const FullType(bool),
    );
    yield r'created_at';
    yield serializers.serialize(
      object.createdAt,
      specifiedType: const FullType(DateTime),
    );
    yield r'file';
    yield serializers.serialize(
      object.file,
      specifiedType: const FullType(GetFileDataResponse),
    );
    yield r'creator';
    yield serializers.serialize(
      object.creator,
      specifiedType: const FullType(GetUserDataResponse),
    );
    yield r'children';
    yield serializers.serialize(
      object.children,
      specifiedType:
          const FullType(BuiltList, [FullType(GetFormComponentDataResponse)]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    GetFormDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required GetFormDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'description':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.description = valueDes;
          break;
        case r'is_active':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.isActive = valueDes;
          break;
        case r'editable':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.editable = valueDes;
          break;
        case r'created_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.createdAt = valueDes;
          break;
        case r'file':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetFileDataResponse),
          ) as GetFileDataResponse;
          result.file.replace(valueDes);
          break;
        case r'creator':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetUserDataResponse),
          ) as GetUserDataResponse;
          result.creator.replace(valueDes);
          break;
        case r'children':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                BuiltList, [FullType(GetFormComponentDataResponse)]),
          ) as BuiltList<GetFormComponentDataResponse>;
          result.children.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  GetFormDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = GetFormDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
