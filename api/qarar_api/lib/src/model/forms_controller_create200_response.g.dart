// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'forms_controller_create200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$FormsControllerCreate200Response
    extends FormsControllerCreate200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$FormsControllerCreate200Response(
          [void Function(FormsControllerCreate200ResponseBuilder)? updates]) =>
      (new FormsControllerCreate200ResponseBuilder()..update(updates))._build();

  _$FormsControllerCreate200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'FormsControllerCreate200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'FormsControllerCreate200Response', 'data');
  }

  @override
  FormsControllerCreate200Response rebuild(
          void Function(FormsControllerCreate200ResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  FormsControllerCreate200ResponseBuilder toBuilder() =>
      new FormsControllerCreate200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is FormsControllerCreate200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'FormsControllerCreate200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class FormsControllerCreate200ResponseBuilder
    implements
        Builder<FormsControllerCreate200Response,
            FormsControllerCreate200ResponseBuilder>,
        SuccessResponseBuilder {
  _$FormsControllerCreate200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  FormsControllerCreate200ResponseBuilder() {
    FormsControllerCreate200Response._defaults(this);
  }

  FormsControllerCreate200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant FormsControllerCreate200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$FormsControllerCreate200Response;
  }

  @override
  void update(void Function(FormsControllerCreate200ResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  FormsControllerCreate200Response build() => _build();

  _$FormsControllerCreate200Response _build() {
    final _$result = _$v ??
        new _$FormsControllerCreate200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success, r'FormsControllerCreate200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'FormsControllerCreate200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
