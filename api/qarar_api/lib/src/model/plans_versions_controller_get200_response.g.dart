// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'plans_versions_controller_get200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PlansVersionsControllerGet200Response
    extends PlansVersionsControllerGet200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$PlansVersionsControllerGet200Response(
          [void Function(PlansVersionsControllerGet200ResponseBuilder)?
              updates]) =>
      (new PlansVersionsControllerGet200ResponseBuilder()..update(updates))
          ._build();

  _$PlansVersionsControllerGet200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'PlansVersionsControllerGet200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'PlansVersionsControllerGet200Response', 'data');
  }

  @override
  PlansVersionsControllerGet200Response rebuild(
          void Function(PlansVersionsControllerGet200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PlansVersionsControllerGet200ResponseBuilder toBuilder() =>
      new PlansVersionsControllerGet200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PlansVersionsControllerGet200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'PlansVersionsControllerGet200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class PlansVersionsControllerGet200ResponseBuilder
    implements
        Builder<PlansVersionsControllerGet200Response,
            PlansVersionsControllerGet200ResponseBuilder>,
        SuccessResponseBuilder {
  _$PlansVersionsControllerGet200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  PlansVersionsControllerGet200ResponseBuilder() {
    PlansVersionsControllerGet200Response._defaults(this);
  }

  PlansVersionsControllerGet200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant PlansVersionsControllerGet200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$PlansVersionsControllerGet200Response;
  }

  @override
  void update(
      void Function(PlansVersionsControllerGet200ResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  PlansVersionsControllerGet200Response build() => _build();

  _$PlansVersionsControllerGet200Response _build() {
    final _$result = _$v ??
        new _$PlansVersionsControllerGet200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success, r'PlansVersionsControllerGet200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'PlansVersionsControllerGet200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
