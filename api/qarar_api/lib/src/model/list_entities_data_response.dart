//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/get_file_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'list_entities_data_response.g.dart';

/// ListEntitiesDataResponse
///
/// Properties:
/// * [id]
/// * [name]
/// * [createdAt]
/// * [users]
/// * [file]
@BuiltValue()
abstract class ListEntitiesDataResponse
    implements
        Built<ListEntitiesDataResponse, ListEntitiesDataResponseBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'created_at')
  DateTime get createdAt;

  @BuiltValueField(wireName: r'users')
  BuiltList<String> get users;

  @BuiltValueField(wireName: r'file')
  GetFileDataResponse? get file;

  ListEntitiesDataResponse._();

  factory ListEntitiesDataResponse(
          [void updates(ListEntitiesDataResponseBuilder b)]) =
      _$ListEntitiesDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ListEntitiesDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ListEntitiesDataResponse> get serializer =>
      _$ListEntitiesDataResponseSerializer();
}

class _$ListEntitiesDataResponseSerializer
    implements PrimitiveSerializer<ListEntitiesDataResponse> {
  @override
  final Iterable<Type> types = const [
    ListEntitiesDataResponse,
    _$ListEntitiesDataResponse
  ];

  @override
  final String wireName = r'ListEntitiesDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ListEntitiesDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    yield r'created_at';
    yield serializers.serialize(
      object.createdAt,
      specifiedType: const FullType(DateTime),
    );
    yield r'users';
    yield serializers.serialize(
      object.users,
      specifiedType: const FullType(BuiltList, [FullType(String)]),
    );
    if (object.file != null) {
      yield r'file';
      yield serializers.serialize(
        object.file,
        specifiedType: const FullType(GetFileDataResponse),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ListEntitiesDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ListEntitiesDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'created_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.createdAt = valueDes;
          break;
        case r'users':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(String)]),
          ) as BuiltList<String>;
          result.users.replace(valueDes);
          break;
        case r'file':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetFileDataResponse),
          ) as GetFileDataResponse;
          result.file.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ListEntitiesDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ListEntitiesDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
