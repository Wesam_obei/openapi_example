// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_file_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetFileDataResponse extends GetFileDataResponse {
  @override
  final num id;
  @override
  final String filename;
  @override
  final num size;
  @override
  final String mimeType;
  @override
  final String path;

  factory _$GetFileDataResponse(
          [void Function(GetFileDataResponseBuilder)? updates]) =>
      (new GetFileDataResponseBuilder()..update(updates))._build();

  _$GetFileDataResponse._(
      {required this.id,
      required this.filename,
      required this.size,
      required this.mimeType,
      required this.path})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, r'GetFileDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        filename, r'GetFileDataResponse', 'filename');
    BuiltValueNullFieldError.checkNotNull(size, r'GetFileDataResponse', 'size');
    BuiltValueNullFieldError.checkNotNull(
        mimeType, r'GetFileDataResponse', 'mimeType');
    BuiltValueNullFieldError.checkNotNull(path, r'GetFileDataResponse', 'path');
  }

  @override
  GetFileDataResponse rebuild(
          void Function(GetFileDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetFileDataResponseBuilder toBuilder() =>
      new GetFileDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetFileDataResponse &&
        id == other.id &&
        filename == other.filename &&
        size == other.size &&
        mimeType == other.mimeType &&
        path == other.path;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, filename.hashCode);
    _$hash = $jc(_$hash, size.hashCode);
    _$hash = $jc(_$hash, mimeType.hashCode);
    _$hash = $jc(_$hash, path.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetFileDataResponse')
          ..add('id', id)
          ..add('filename', filename)
          ..add('size', size)
          ..add('mimeType', mimeType)
          ..add('path', path))
        .toString();
  }
}

class GetFileDataResponseBuilder
    implements Builder<GetFileDataResponse, GetFileDataResponseBuilder> {
  _$GetFileDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _filename;
  String? get filename => _$this._filename;
  set filename(String? filename) => _$this._filename = filename;

  num? _size;
  num? get size => _$this._size;
  set size(num? size) => _$this._size = size;

  String? _mimeType;
  String? get mimeType => _$this._mimeType;
  set mimeType(String? mimeType) => _$this._mimeType = mimeType;

  String? _path;
  String? get path => _$this._path;
  set path(String? path) => _$this._path = path;

  GetFileDataResponseBuilder() {
    GetFileDataResponse._defaults(this);
  }

  GetFileDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _filename = $v.filename;
      _size = $v.size;
      _mimeType = $v.mimeType;
      _path = $v.path;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetFileDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetFileDataResponse;
  }

  @override
  void update(void Function(GetFileDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetFileDataResponse build() => _build();

  _$GetFileDataResponse _build() {
    final _$result = _$v ??
        new _$GetFileDataResponse._(
            id: BuiltValueNullFieldError.checkNotNull(
                id, r'GetFileDataResponse', 'id'),
            filename: BuiltValueNullFieldError.checkNotNull(
                filename, r'GetFileDataResponse', 'filename'),
            size: BuiltValueNullFieldError.checkNotNull(
                size, r'GetFileDataResponse', 'size'),
            mimeType: BuiltValueNullFieldError.checkNotNull(
                mimeType, r'GetFileDataResponse', 'mimeType'),
            path: BuiltValueNullFieldError.checkNotNull(
                path, r'GetFileDataResponse', 'path'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
