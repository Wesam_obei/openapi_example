//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/json_object.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'widget_config.g.dart';

/// WidgetConfig
///
/// Properties:
/// * [id]
/// * [view]
/// * [style]
/// * [config]
@BuiltValue()
abstract class WidgetConfig
    implements Built<WidgetConfig, WidgetConfigBuilder> {
  @BuiltValueField(wireName: r'id')
  String get id;

  @BuiltValueField(wireName: r'view')
  String get view;

  @BuiltValueField(wireName: r'style')
  JsonObject get style;

  @BuiltValueField(wireName: r'config')
  JsonObject get config;

  WidgetConfig._();

  factory WidgetConfig([void updates(WidgetConfigBuilder b)]) = _$WidgetConfig;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(WidgetConfigBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<WidgetConfig> get serializer => _$WidgetConfigSerializer();
}

class _$WidgetConfigSerializer implements PrimitiveSerializer<WidgetConfig> {
  @override
  final Iterable<Type> types = const [WidgetConfig, _$WidgetConfig];

  @override
  final String wireName = r'WidgetConfig';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    WidgetConfig object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(String),
    );
    yield r'view';
    yield serializers.serialize(
      object.view,
      specifiedType: const FullType(String),
    );
    yield r'style';
    yield serializers.serialize(
      object.style,
      specifiedType: const FullType(JsonObject),
    );
    yield r'config';
    yield serializers.serialize(
      object.config,
      specifiedType: const FullType(JsonObject),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    WidgetConfig object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required WidgetConfigBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.id = valueDes;
          break;
        case r'view':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.view = valueDes;
          break;
        case r'style':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.style = valueDes;
          break;
        case r'config':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.config = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  WidgetConfig deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = WidgetConfigBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
