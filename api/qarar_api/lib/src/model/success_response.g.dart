// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'success_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

abstract class SuccessResponseBuilder {
  void replace(SuccessResponse other);
  void update(void Function(SuccessResponseBuilder) updates);
  bool? get success;
  set success(bool? success);

  JsonObject? get data;
  set data(JsonObject? data);
}

class _$$SuccessResponse extends $SuccessResponse {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$$SuccessResponse(
          [void Function($SuccessResponseBuilder)? updates]) =>
      (new $SuccessResponseBuilder()..update(updates))._build();

  _$$SuccessResponse._({required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'$SuccessResponse', 'success');
    BuiltValueNullFieldError.checkNotNull(data, r'$SuccessResponse', 'data');
  }

  @override
  $SuccessResponse rebuild(void Function($SuccessResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  $SuccessResponseBuilder toBuilder() =>
      new $SuccessResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is $SuccessResponse &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'$SuccessResponse')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class $SuccessResponseBuilder
    implements
        Builder<$SuccessResponse, $SuccessResponseBuilder>,
        SuccessResponseBuilder {
  _$$SuccessResponse? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  $SuccessResponseBuilder() {
    $SuccessResponse._defaults(this);
  }

  $SuccessResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant $SuccessResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$$SuccessResponse;
  }

  @override
  void update(void Function($SuccessResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  $SuccessResponse build() => _build();

  _$$SuccessResponse _build() {
    final _$result = _$v ??
        new _$$SuccessResponse._(
            success: BuiltValueNullFieldError.checkNotNull(
                success, r'$SuccessResponse', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'$SuccessResponse', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
