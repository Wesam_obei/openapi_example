// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'surveys_controller_list200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SurveysControllerList200Response
    extends SurveysControllerList200Response {
  @override
  final bool success;
  @override
  final BuiltList<JsonObject> data;
  @override
  final PaginatedMetaDocumented meta;
  @override
  final PaginatedLinksDocumented links;

  factory _$SurveysControllerList200Response(
          [void Function(SurveysControllerList200ResponseBuilder)? updates]) =>
      (new SurveysControllerList200ResponseBuilder()..update(updates))._build();

  _$SurveysControllerList200Response._(
      {required this.success,
      required this.data,
      required this.meta,
      required this.links})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'SurveysControllerList200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'SurveysControllerList200Response', 'data');
    BuiltValueNullFieldError.checkNotNull(
        meta, r'SurveysControllerList200Response', 'meta');
    BuiltValueNullFieldError.checkNotNull(
        links, r'SurveysControllerList200Response', 'links');
  }

  @override
  SurveysControllerList200Response rebuild(
          void Function(SurveysControllerList200ResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SurveysControllerList200ResponseBuilder toBuilder() =>
      new SurveysControllerList200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SurveysControllerList200Response &&
        success == other.success &&
        data == other.data &&
        meta == other.meta &&
        links == other.links;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jc(_$hash, meta.hashCode);
    _$hash = $jc(_$hash, links.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SurveysControllerList200Response')
          ..add('success', success)
          ..add('data', data)
          ..add('meta', meta)
          ..add('links', links))
        .toString();
  }
}

class SurveysControllerList200ResponseBuilder
    implements
        Builder<SurveysControllerList200Response,
            SurveysControllerList200ResponseBuilder>,
        PaginatedDocumentedBuilder {
  _$SurveysControllerList200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  ListBuilder<JsonObject>? _data;
  ListBuilder<JsonObject> get data =>
      _$this._data ??= new ListBuilder<JsonObject>();
  set data(covariant ListBuilder<JsonObject>? data) => _$this._data = data;

  PaginatedMetaDocumentedBuilder? _meta;
  PaginatedMetaDocumentedBuilder get meta =>
      _$this._meta ??= new PaginatedMetaDocumentedBuilder();
  set meta(covariant PaginatedMetaDocumentedBuilder? meta) =>
      _$this._meta = meta;

  PaginatedLinksDocumentedBuilder? _links;
  PaginatedLinksDocumentedBuilder get links =>
      _$this._links ??= new PaginatedLinksDocumentedBuilder();
  set links(covariant PaginatedLinksDocumentedBuilder? links) =>
      _$this._links = links;

  SurveysControllerList200ResponseBuilder() {
    SurveysControllerList200Response._defaults(this);
  }

  SurveysControllerList200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data.toBuilder();
      _meta = $v.meta.toBuilder();
      _links = $v.links.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant SurveysControllerList200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SurveysControllerList200Response;
  }

  @override
  void update(void Function(SurveysControllerList200ResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SurveysControllerList200Response build() => _build();

  _$SurveysControllerList200Response _build() {
    _$SurveysControllerList200Response _$result;
    try {
      _$result = _$v ??
          new _$SurveysControllerList200Response._(
              success: BuiltValueNullFieldError.checkNotNull(
                  success, r'SurveysControllerList200Response', 'success'),
              data: data.build(),
              meta: meta.build(),
              links: links.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'data';
        data.build();
        _$failedField = 'meta';
        meta.build();
        _$failedField = 'links';
        links.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'SurveysControllerList200Response', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
