//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_cables_map200_response_all_of_data_features_inner.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_cables_map200_response_all_of_data.g.dart';

/// MapLayersControllerGetCablesMap200ResponseAllOfData
///
/// Properties:
/// * [type]
/// * [features]
@BuiltValue()
abstract class MapLayersControllerGetCablesMap200ResponseAllOfData
    implements
        Built<MapLayersControllerGetCablesMap200ResponseAllOfData,
            MapLayersControllerGetCablesMap200ResponseAllOfDataBuilder> {
  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum get type;
  // enum typeEnum {  FeatureCollection,  };

  @BuiltValueField(wireName: r'features')
  BuiltList<MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner>
      get features;

  MapLayersControllerGetCablesMap200ResponseAllOfData._();

  factory MapLayersControllerGetCablesMap200ResponseAllOfData(
          [void updates(
              MapLayersControllerGetCablesMap200ResponseAllOfDataBuilder b)]) =
      _$MapLayersControllerGetCablesMap200ResponseAllOfData;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetCablesMap200ResponseAllOfDataBuilder b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<MapLayersControllerGetCablesMap200ResponseAllOfData>
      get serializer =>
          _$MapLayersControllerGetCablesMap200ResponseAllOfDataSerializer();
}

class _$MapLayersControllerGetCablesMap200ResponseAllOfDataSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetCablesMap200ResponseAllOfData> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetCablesMap200ResponseAllOfData,
    _$MapLayersControllerGetCablesMap200ResponseAllOfData
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetCablesMap200ResponseAllOfData';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetCablesMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(
          MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum),
    );
    yield r'features';
    yield serializers.serialize(
      object.features,
      specifiedType: const FullType(BuiltList, [
        FullType(
            MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner)
      ]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetCablesMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetCablesMap200ResponseAllOfDataBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum),
          ) as MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum;
          result.type = valueDes;
          break;
        case r'features':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [
              FullType(
                  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner)
            ]),
          ) as BuiltList<
              MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner>;
          result.features.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetCablesMap200ResponseAllOfData deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = MapLayersControllerGetCablesMap200ResponseAllOfDataBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum
    extends EnumClass {
  @BuiltValueEnumConst(wireName: r'FeatureCollection', fallback: true)
  static const MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum
      featureCollection =
      _$mapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum_featureCollection;

  static Serializer<MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum>
      get serializer =>
          _$mapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnumSerializer;

  const MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum._(
      String name)
      : super(name);

  static BuiltSet<MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum>
      get values =>
          _$mapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnumValues;
  static MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum valueOf(
          String name) =>
      _$mapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnumValueOf(
          name);
}
