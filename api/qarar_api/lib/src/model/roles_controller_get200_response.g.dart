// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'roles_controller_get200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$RolesControllerGet200Response extends RolesControllerGet200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$RolesControllerGet200Response(
          [void Function(RolesControllerGet200ResponseBuilder)? updates]) =>
      (new RolesControllerGet200ResponseBuilder()..update(updates))._build();

  _$RolesControllerGet200Response._({required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'RolesControllerGet200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'RolesControllerGet200Response', 'data');
  }

  @override
  RolesControllerGet200Response rebuild(
          void Function(RolesControllerGet200ResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RolesControllerGet200ResponseBuilder toBuilder() =>
      new RolesControllerGet200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RolesControllerGet200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'RolesControllerGet200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class RolesControllerGet200ResponseBuilder
    implements
        Builder<RolesControllerGet200Response,
            RolesControllerGet200ResponseBuilder>,
        SuccessResponseBuilder {
  _$RolesControllerGet200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  RolesControllerGet200ResponseBuilder() {
    RolesControllerGet200Response._defaults(this);
  }

  RolesControllerGet200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant RolesControllerGet200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$RolesControllerGet200Response;
  }

  @override
  void update(void Function(RolesControllerGet200ResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  RolesControllerGet200Response build() => _build();

  _$RolesControllerGet200Response _build() {
    final _$result = _$v ??
        new _$RolesControllerGet200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success, r'RolesControllerGet200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'RolesControllerGet200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
