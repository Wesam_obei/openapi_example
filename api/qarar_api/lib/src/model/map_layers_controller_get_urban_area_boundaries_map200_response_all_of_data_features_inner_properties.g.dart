// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_urban_area_boundaries_map200_response_all_of_data_features_inner_properties.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
    extends MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties {
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      id;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      provinceName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      urbanBoundaryType;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      municipalityName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      remarks;

  factory _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties(
          [void Function(
                  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
              updates]) =>
      (new MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties._(
      {this.id,
      this.provinceName,
      this.urbanBoundaryType,
      this.municipalityName,
      this.remarks})
      : super._();

  @override
  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
      rebuild(
              void Function(
                      MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)
                  updates) =>
          (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      toBuilder() =>
          new MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties &&
        id == other.id &&
        provinceName == other.provinceName &&
        urbanBoundaryType == other.urbanBoundaryType &&
        municipalityName == other.municipalityName &&
        remarks == other.remarks;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, provinceName.hashCode);
    _$hash = $jc(_$hash, urbanBoundaryType.hashCode);
    _$hash = $jc(_$hash, municipalityName.hashCode);
    _$hash = $jc(_$hash, remarks.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties')
          ..add('id', id)
          ..add('provinceName', provinceName)
          ..add('urbanBoundaryType', urbanBoundaryType)
          ..add('municipalityName', municipalityName)
          ..add('remarks', remarks))
        .toString();
  }
}

class MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
    implements
        Builder<
            MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties,
            MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder> {
  _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties?
      _$v;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _id;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get id => _$this._id ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set id(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              id) =>
      _$this._id = id;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _provinceName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get provinceName => _$this._provinceName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set provinceName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              provinceName) =>
      _$this._provinceName = provinceName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _urbanBoundaryType;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get urbanBoundaryType => _$this._urbanBoundaryType ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set urbanBoundaryType(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              urbanBoundaryType) =>
      _$this._urbanBoundaryType = urbanBoundaryType;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _municipalityName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get municipalityName => _$this._municipalityName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set municipalityName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              municipalityName) =>
      _$this._municipalityName = municipalityName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _remarks;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get remarks => _$this._remarks ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set remarks(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              remarks) =>
      _$this._remarks = remarks;

  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder() {
    MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
        ._defaults(this);
  }

  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id?.toBuilder();
      _provinceName = $v.provinceName?.toBuilder();
      _urbanBoundaryType = $v.urbanBoundaryType?.toBuilder();
      _municipalityName = $v.municipalityName?.toBuilder();
      _remarks = $v.remarks?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
      build() => _build();

  _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
      _build() {
    _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
        _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
              ._(
              id: _id?.build(),
              provinceName: _provinceName?.build(),
              urbanBoundaryType: _urbanBoundaryType?.build(),
              municipalityName: _municipalityName?.build(),
              remarks: _remarks?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'id';
        _id?.build();
        _$failedField = 'provinceName';
        _provinceName?.build();
        _$failedField = 'urbanBoundaryType';
        _urbanBoundaryType?.build();
        _$failedField = 'municipalityName';
        _municipalityName?.build();
        _$failedField = 'remarks';
        _remarks?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
