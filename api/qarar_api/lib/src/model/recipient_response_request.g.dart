// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recipient_response_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const RecipientResponseRequestStatusEnum
    _$recipientResponseRequestStatusEnum_draft =
    const RecipientResponseRequestStatusEnum._('draft');
const RecipientResponseRequestStatusEnum
    _$recipientResponseRequestStatusEnum_submitted =
    const RecipientResponseRequestStatusEnum._('submitted');

RecipientResponseRequestStatusEnum _$recipientResponseRequestStatusEnumValueOf(
    String name) {
  switch (name) {
    case 'draft':
      return _$recipientResponseRequestStatusEnum_draft;
    case 'submitted':
      return _$recipientResponseRequestStatusEnum_submitted;
    default:
      return _$recipientResponseRequestStatusEnum_submitted;
  }
}

final BuiltSet<RecipientResponseRequestStatusEnum>
    _$recipientResponseRequestStatusEnumValues = new BuiltSet<
        RecipientResponseRequestStatusEnum>(const <RecipientResponseRequestStatusEnum>[
  _$recipientResponseRequestStatusEnum_draft,
  _$recipientResponseRequestStatusEnum_submitted,
]);

Serializer<RecipientResponseRequestStatusEnum>
    _$recipientResponseRequestStatusEnumSerializer =
    new _$RecipientResponseRequestStatusEnumSerializer();

class _$RecipientResponseRequestStatusEnumSerializer
    implements PrimitiveSerializer<RecipientResponseRequestStatusEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'draft': 'draft',
    'submitted': 'submitted',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'draft': 'draft',
    'submitted': 'submitted',
  };

  @override
  final Iterable<Type> types = const <Type>[RecipientResponseRequestStatusEnum];
  @override
  final String wireName = 'RecipientResponseRequestStatusEnum';

  @override
  Object serialize(
          Serializers serializers, RecipientResponseRequestStatusEnum object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  RecipientResponseRequestStatusEnum deserialize(
          Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      RecipientResponseRequestStatusEnum.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

class _$RecipientResponseRequest extends RecipientResponseRequest {
  @override
  final num recipientId;
  @override
  final RecipientResponseRequestStatusEnum status;
  @override
  final BuiltList<AnswerRequest> answers;
  @override
  final BuiltList<num> deletedMultiAnswerIds;

  factory _$RecipientResponseRequest(
          [void Function(RecipientResponseRequestBuilder)? updates]) =>
      (new RecipientResponseRequestBuilder()..update(updates))._build();

  _$RecipientResponseRequest._(
      {required this.recipientId,
      required this.status,
      required this.answers,
      required this.deletedMultiAnswerIds})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        recipientId, r'RecipientResponseRequest', 'recipientId');
    BuiltValueNullFieldError.checkNotNull(
        status, r'RecipientResponseRequest', 'status');
    BuiltValueNullFieldError.checkNotNull(
        answers, r'RecipientResponseRequest', 'answers');
    BuiltValueNullFieldError.checkNotNull(deletedMultiAnswerIds,
        r'RecipientResponseRequest', 'deletedMultiAnswerIds');
  }

  @override
  RecipientResponseRequest rebuild(
          void Function(RecipientResponseRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RecipientResponseRequestBuilder toBuilder() =>
      new RecipientResponseRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RecipientResponseRequest &&
        recipientId == other.recipientId &&
        status == other.status &&
        answers == other.answers &&
        deletedMultiAnswerIds == other.deletedMultiAnswerIds;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, recipientId.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jc(_$hash, answers.hashCode);
    _$hash = $jc(_$hash, deletedMultiAnswerIds.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'RecipientResponseRequest')
          ..add('recipientId', recipientId)
          ..add('status', status)
          ..add('answers', answers)
          ..add('deletedMultiAnswerIds', deletedMultiAnswerIds))
        .toString();
  }
}

class RecipientResponseRequestBuilder
    implements
        Builder<RecipientResponseRequest, RecipientResponseRequestBuilder> {
  _$RecipientResponseRequest? _$v;

  num? _recipientId;
  num? get recipientId => _$this._recipientId;
  set recipientId(num? recipientId) => _$this._recipientId = recipientId;

  RecipientResponseRequestStatusEnum? _status;
  RecipientResponseRequestStatusEnum? get status => _$this._status;
  set status(RecipientResponseRequestStatusEnum? status) =>
      _$this._status = status;

  ListBuilder<AnswerRequest>? _answers;
  ListBuilder<AnswerRequest> get answers =>
      _$this._answers ??= new ListBuilder<AnswerRequest>();
  set answers(ListBuilder<AnswerRequest>? answers) => _$this._answers = answers;

  ListBuilder<num>? _deletedMultiAnswerIds;
  ListBuilder<num> get deletedMultiAnswerIds =>
      _$this._deletedMultiAnswerIds ??= new ListBuilder<num>();
  set deletedMultiAnswerIds(ListBuilder<num>? deletedMultiAnswerIds) =>
      _$this._deletedMultiAnswerIds = deletedMultiAnswerIds;

  RecipientResponseRequestBuilder() {
    RecipientResponseRequest._defaults(this);
  }

  RecipientResponseRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _recipientId = $v.recipientId;
      _status = $v.status;
      _answers = $v.answers.toBuilder();
      _deletedMultiAnswerIds = $v.deletedMultiAnswerIds.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RecipientResponseRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$RecipientResponseRequest;
  }

  @override
  void update(void Function(RecipientResponseRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  RecipientResponseRequest build() => _build();

  _$RecipientResponseRequest _build() {
    _$RecipientResponseRequest _$result;
    try {
      _$result = _$v ??
          new _$RecipientResponseRequest._(
              recipientId: BuiltValueNullFieldError.checkNotNull(
                  recipientId, r'RecipientResponseRequest', 'recipientId'),
              status: BuiltValueNullFieldError.checkNotNull(
                  status, r'RecipientResponseRequest', 'status'),
              answers: answers.build(),
              deletedMultiAnswerIds: deletedMultiAnswerIds.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'answers';
        answers.build();
        _$failedField = 'deletedMultiAnswerIds';
        deletedMultiAnswerIds.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'RecipientResponseRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
