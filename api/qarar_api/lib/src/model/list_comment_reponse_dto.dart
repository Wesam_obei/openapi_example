//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/survey_response_answer_data_response.dart';
import 'package:qarar_api/src/model/get_user_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'list_comment_reponse_dto.g.dart';

/// ListCommentReponseDto
///
/// Properties:
/// * [id]
/// * [surveyResponseAnswer]
/// * [comment]
/// * [commentBy]
/// * [createdAt]
@BuiltValue()
abstract class ListCommentReponseDto
    implements Built<ListCommentReponseDto, ListCommentReponseDtoBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'surveyResponseAnswer')
  SurveyResponseAnswerDataResponse get surveyResponseAnswer;

  @BuiltValueField(wireName: r'comment')
  String get comment;

  @BuiltValueField(wireName: r'commentBy')
  GetUserDataResponse get commentBy;

  @BuiltValueField(wireName: r'created_at')
  DateTime get createdAt;

  ListCommentReponseDto._();

  factory ListCommentReponseDto(
      [void updates(ListCommentReponseDtoBuilder b)]) = _$ListCommentReponseDto;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ListCommentReponseDtoBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ListCommentReponseDto> get serializer =>
      _$ListCommentReponseDtoSerializer();
}

class _$ListCommentReponseDtoSerializer
    implements PrimitiveSerializer<ListCommentReponseDto> {
  @override
  final Iterable<Type> types = const [
    ListCommentReponseDto,
    _$ListCommentReponseDto
  ];

  @override
  final String wireName = r'ListCommentReponseDto';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ListCommentReponseDto object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'surveyResponseAnswer';
    yield serializers.serialize(
      object.surveyResponseAnswer,
      specifiedType: const FullType(SurveyResponseAnswerDataResponse),
    );
    yield r'comment';
    yield serializers.serialize(
      object.comment,
      specifiedType: const FullType(String),
    );
    yield r'commentBy';
    yield serializers.serialize(
      object.commentBy,
      specifiedType: const FullType(GetUserDataResponse),
    );
    yield r'created_at';
    yield serializers.serialize(
      object.createdAt,
      specifiedType: const FullType(DateTime),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ListCommentReponseDto object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ListCommentReponseDtoBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'surveyResponseAnswer':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(SurveyResponseAnswerDataResponse),
          ) as SurveyResponseAnswerDataResponse;
          result.surveyResponseAnswer.replace(valueDes);
          break;
        case r'comment':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.comment = valueDes;
          break;
        case r'commentBy':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetUserDataResponse),
          ) as GetUserDataResponse;
          result.commentBy.replace(valueDes);
          break;
        case r'created_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.createdAt = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ListCommentReponseDto deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ListCommentReponseDtoBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
