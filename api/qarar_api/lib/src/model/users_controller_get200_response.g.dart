// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'users_controller_get200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$UsersControllerGet200Response extends UsersControllerGet200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$UsersControllerGet200Response(
          [void Function(UsersControllerGet200ResponseBuilder)? updates]) =>
      (new UsersControllerGet200ResponseBuilder()..update(updates))._build();

  _$UsersControllerGet200Response._({required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'UsersControllerGet200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'UsersControllerGet200Response', 'data');
  }

  @override
  UsersControllerGet200Response rebuild(
          void Function(UsersControllerGet200ResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UsersControllerGet200ResponseBuilder toBuilder() =>
      new UsersControllerGet200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UsersControllerGet200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'UsersControllerGet200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class UsersControllerGet200ResponseBuilder
    implements
        Builder<UsersControllerGet200Response,
            UsersControllerGet200ResponseBuilder>,
        SuccessResponseBuilder {
  _$UsersControllerGet200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  UsersControllerGet200ResponseBuilder() {
    UsersControllerGet200Response._defaults(this);
  }

  UsersControllerGet200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant UsersControllerGet200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$UsersControllerGet200Response;
  }

  @override
  void update(void Function(UsersControllerGet200ResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  UsersControllerGet200Response build() => _build();

  _$UsersControllerGet200Response _build() {
    final _$result = _$v ??
        new _$UsersControllerGet200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success, r'UsersControllerGet200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'UsersControllerGet200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
