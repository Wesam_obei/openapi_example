// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'permission_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PermissionDataResponse extends PermissionDataResponse {
  @override
  final num id;
  @override
  final PermissionsEnum name;
  @override
  final String label;
  @override
  final String description;
  @override
  final num order;
  @override
  final DateTime createdAt;

  factory _$PermissionDataResponse(
          [void Function(PermissionDataResponseBuilder)? updates]) =>
      (new PermissionDataResponseBuilder()..update(updates))._build();

  _$PermissionDataResponse._(
      {required this.id,
      required this.name,
      required this.label,
      required this.description,
      required this.order,
      required this.createdAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, r'PermissionDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, r'PermissionDataResponse', 'name');
    BuiltValueNullFieldError.checkNotNull(
        label, r'PermissionDataResponse', 'label');
    BuiltValueNullFieldError.checkNotNull(
        description, r'PermissionDataResponse', 'description');
    BuiltValueNullFieldError.checkNotNull(
        order, r'PermissionDataResponse', 'order');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'PermissionDataResponse', 'createdAt');
  }

  @override
  PermissionDataResponse rebuild(
          void Function(PermissionDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PermissionDataResponseBuilder toBuilder() =>
      new PermissionDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PermissionDataResponse &&
        id == other.id &&
        name == other.name &&
        label == other.label &&
        description == other.description &&
        order == other.order &&
        createdAt == other.createdAt;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, label.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, order.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'PermissionDataResponse')
          ..add('id', id)
          ..add('name', name)
          ..add('label', label)
          ..add('description', description)
          ..add('order', order)
          ..add('createdAt', createdAt))
        .toString();
  }
}

class PermissionDataResponseBuilder
    implements Builder<PermissionDataResponse, PermissionDataResponseBuilder> {
  _$PermissionDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  PermissionsEnum? _name;
  PermissionsEnum? get name => _$this._name;
  set name(PermissionsEnum? name) => _$this._name = name;

  String? _label;
  String? get label => _$this._label;
  set label(String? label) => _$this._label = label;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  num? _order;
  num? get order => _$this._order;
  set order(num? order) => _$this._order = order;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  PermissionDataResponseBuilder() {
    PermissionDataResponse._defaults(this);
  }

  PermissionDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _label = $v.label;
      _description = $v.description;
      _order = $v.order;
      _createdAt = $v.createdAt;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PermissionDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$PermissionDataResponse;
  }

  @override
  void update(void Function(PermissionDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  PermissionDataResponse build() => _build();

  _$PermissionDataResponse _build() {
    final _$result = _$v ??
        new _$PermissionDataResponse._(
            id: BuiltValueNullFieldError.checkNotNull(
                id, r'PermissionDataResponse', 'id'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, r'PermissionDataResponse', 'name'),
            label: BuiltValueNullFieldError.checkNotNull(
                label, r'PermissionDataResponse', 'label'),
            description: BuiltValueNullFieldError.checkNotNull(
                description, r'PermissionDataResponse', 'description'),
            order: BuiltValueNullFieldError.checkNotNull(
                order, r'PermissionDataResponse', 'order'),
            createdAt: BuiltValueNullFieldError.checkNotNull(
                createdAt, r'PermissionDataResponse', 'createdAt'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
