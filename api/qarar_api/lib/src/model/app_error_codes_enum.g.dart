// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_error_codes_enum.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const AppErrorCodesEnum _$deletedComponentsForbidden =
    const AppErrorCodesEnum._('deletedComponentsForbidden');
const AppErrorCodesEnum _$deleteSurveyWithResponses =
    const AppErrorCodesEnum._('deleteSurveyWithResponses');
const AppErrorCodesEnum _$deleteFormWithSurveys =
    const AppErrorCodesEnum._('deleteFormWithSurveys');
const AppErrorCodesEnum _$deleteEntityWithUsers =
    const AppErrorCodesEnum._('deleteEntityWithUsers');
const AppErrorCodesEnum _$deleteEntityWithEquipments =
    const AppErrorCodesEnum._('deleteEntityWithEquipments');
const AppErrorCodesEnum _$deleteEntityWithEquipmentStorePoints =
    const AppErrorCodesEnum._('deleteEntityWithEquipmentStorePoints');
const AppErrorCodesEnum _$deleteRoleWithUsers =
    const AppErrorCodesEnum._('deleteRoleWithUsers');
const AppErrorCodesEnum _$deleteEquipmentTypeWithEquipments =
    const AppErrorCodesEnum._('deleteEquipmentTypeWithEquipments');
const AppErrorCodesEnum _$deleteEquipmentStorePointWithEquipments =
    const AppErrorCodesEnum._('deleteEquipmentStorePointWithEquipments');
const AppErrorCodesEnum _$deleteYourAccount =
    const AppErrorCodesEnum._('deleteYourAccount');
const AppErrorCodesEnum _$duplicateRoleName =
    const AppErrorCodesEnum._('duplicateRoleName');
const AppErrorCodesEnum _$duplicateFormName =
    const AppErrorCodesEnum._('duplicateFormName');
const AppErrorCodesEnum _$duplicateSurveyName =
    const AppErrorCodesEnum._('duplicateSurveyName');
const AppErrorCodesEnum _$duplicateEntityName =
    const AppErrorCodesEnum._('duplicateEntityName');
const AppErrorCodesEnum _$duplicateUserDashboardName =
    const AppErrorCodesEnum._('duplicateUserDashboardName');
const AppErrorCodesEnum _$duplicatePlanName =
    const AppErrorCodesEnum._('duplicatePlanName');
const AppErrorCodesEnum _$duplicateEquipmentStorePointName =
    const AppErrorCodesEnum._('duplicateEquipmentStorePointName');
const AppErrorCodesEnum _$duplicateEquipmentTypeName =
    const AppErrorCodesEnum._('duplicateEquipmentTypeName');
const AppErrorCodesEnum _$duplicatePlansVersionName =
    const AppErrorCodesEnum._('duplicatePlansVersionName');
const AppErrorCodesEnum _$duplicateUserEmail =
    const AppErrorCodesEnum._('duplicateUserEmail');
const AppErrorCodesEnum _$permissionNotFound =
    const AppErrorCodesEnum._('permissionNotFound');
const AppErrorCodesEnum _$userNotFound =
    const AppErrorCodesEnum._('userNotFound');
const AppErrorCodesEnum _$fileNameNotFound =
    const AppErrorCodesEnum._('fileNameNotFound');
const AppErrorCodesEnum _$roleNotFound =
    const AppErrorCodesEnum._('roleNotFound');
const AppErrorCodesEnum _$entityNotFound =
    const AppErrorCodesEnum._('entityNotFound');
const AppErrorCodesEnum _$equipmentStorePointNotFound =
    const AppErrorCodesEnum._('equipmentStorePointNotFound');
const AppErrorCodesEnum _$equipmentTypeNotFound =
    const AppErrorCodesEnum._('equipmentTypeNotFound');
const AppErrorCodesEnum _$equipmentNotFound =
    const AppErrorCodesEnum._('equipmentNotFound');
const AppErrorCodesEnum _$surveyNotFound =
    const AppErrorCodesEnum._('surveyNotFound');
const AppErrorCodesEnum _$formNotFound =
    const AppErrorCodesEnum._('formNotFound');
const AppErrorCodesEnum _$planNotFound =
    const AppErrorCodesEnum._('planNotFound');
const AppErrorCodesEnum _$planVersionNotFound =
    const AppErrorCodesEnum._('planVersionNotFound');
const AppErrorCodesEnum _$userDashboardNotFound =
    const AppErrorCodesEnum._('userDashboardNotFound');
const AppErrorCodesEnum _$updateFormWithResponses =
    const AppErrorCodesEnum._('updateFormWithResponses');
const AppErrorCodesEnum _$userNotActive =
    const AppErrorCodesEnum._('userNotActive');
const AppErrorCodesEnum _$validatorError =
    const AppErrorCodesEnum._('validatorError');
const AppErrorCodesEnum _$isRequired = const AppErrorCodesEnum._('isRequired');
const AppErrorCodesEnum _$isArray = const AppErrorCodesEnum._('isArray');
const AppErrorCodesEnum _$stringDataType =
    const AppErrorCodesEnum._('stringDataType');
const AppErrorCodesEnum _$numberDataType =
    const AppErrorCodesEnum._('numberDataType');
const AppErrorCodesEnum _$integerDataType =
    const AppErrorCodesEnum._('integerDataType');
const AppErrorCodesEnum _$nullDataType =
    const AppErrorCodesEnum._('nullDataType');
const AppErrorCodesEnum _$fileDataType =
    const AppErrorCodesEnum._('fileDataType');
const AppErrorCodesEnum _$dateDataType =
    const AppErrorCodesEnum._('dateDataType');
const AppErrorCodesEnum _$dateTimeDataType =
    const AppErrorCodesEnum._('dateTimeDataType');
const AppErrorCodesEnum _$timeDataType =
    const AppErrorCodesEnum._('timeDataType');
const AppErrorCodesEnum _$coordinateDataType =
    const AppErrorCodesEnum._('coordinateDataType');
const AppErrorCodesEnum _$responseAlreadySubmittedOrApproved =
    const AppErrorCodesEnum._('responseAlreadySubmittedOrApproved');
const AppErrorCodesEnum _$responseNotSubmitted =
    const AppErrorCodesEnum._('responseNotSubmitted');
const AppErrorCodesEnum _$editQuestionHasAnswers =
    const AppErrorCodesEnum._('editQuestionHasAnswers');
const AppErrorCodesEnum _$questionHasAlreadyAnswers =
    const AppErrorCodesEnum._('questionHasAlreadyAnswers');
const AppErrorCodesEnum _$invalidSubscription =
    const AppErrorCodesEnum._('invalidSubscription');
const AppErrorCodesEnum _$fileCreatorNotMatch =
    const AppErrorCodesEnum._('fileCreatorNotMatch');
const AppErrorCodesEnum _$invalidIncludes =
    const AppErrorCodesEnum._('invalidIncludes');
const AppErrorCodesEnum _$serviceUnavailable =
    const AppErrorCodesEnum._('serviceUnavailable');
const AppErrorCodesEnum _$serviceSchemaValidatorError =
    const AppErrorCodesEnum._('serviceSchemaValidatorError');
const AppErrorCodesEnum _$unauthorized =
    const AppErrorCodesEnum._('unauthorized');
const AppErrorCodesEnum _$raqameUnauthorized =
    const AppErrorCodesEnum._('raqameUnauthorized');
const AppErrorCodesEnum _$missingUser =
    const AppErrorCodesEnum._('missingUser');
const AppErrorCodesEnum _$genericNotFound =
    const AppErrorCodesEnum._('genericNotFound');
const AppErrorCodesEnum _$forbidden = const AppErrorCodesEnum._('forbidden');
const AppErrorCodesEnum _$serverError =
    const AppErrorCodesEnum._('serverError');
const AppErrorCodesEnum _$unknown = const AppErrorCodesEnum._('unknown');

AppErrorCodesEnum _$valueOf(String name) {
  switch (name) {
    case 'deletedComponentsForbidden':
      return _$deletedComponentsForbidden;
    case 'deleteSurveyWithResponses':
      return _$deleteSurveyWithResponses;
    case 'deleteFormWithSurveys':
      return _$deleteFormWithSurveys;
    case 'deleteEntityWithUsers':
      return _$deleteEntityWithUsers;
    case 'deleteEntityWithEquipments':
      return _$deleteEntityWithEquipments;
    case 'deleteEntityWithEquipmentStorePoints':
      return _$deleteEntityWithEquipmentStorePoints;
    case 'deleteRoleWithUsers':
      return _$deleteRoleWithUsers;
    case 'deleteEquipmentTypeWithEquipments':
      return _$deleteEquipmentTypeWithEquipments;
    case 'deleteEquipmentStorePointWithEquipments':
      return _$deleteEquipmentStorePointWithEquipments;
    case 'deleteYourAccount':
      return _$deleteYourAccount;
    case 'duplicateRoleName':
      return _$duplicateRoleName;
    case 'duplicateFormName':
      return _$duplicateFormName;
    case 'duplicateSurveyName':
      return _$duplicateSurveyName;
    case 'duplicateEntityName':
      return _$duplicateEntityName;
    case 'duplicateUserDashboardName':
      return _$duplicateUserDashboardName;
    case 'duplicatePlanName':
      return _$duplicatePlanName;
    case 'duplicateEquipmentStorePointName':
      return _$duplicateEquipmentStorePointName;
    case 'duplicateEquipmentTypeName':
      return _$duplicateEquipmentTypeName;
    case 'duplicatePlansVersionName':
      return _$duplicatePlansVersionName;
    case 'duplicateUserEmail':
      return _$duplicateUserEmail;
    case 'permissionNotFound':
      return _$permissionNotFound;
    case 'userNotFound':
      return _$userNotFound;
    case 'fileNameNotFound':
      return _$fileNameNotFound;
    case 'roleNotFound':
      return _$roleNotFound;
    case 'entityNotFound':
      return _$entityNotFound;
    case 'equipmentStorePointNotFound':
      return _$equipmentStorePointNotFound;
    case 'equipmentTypeNotFound':
      return _$equipmentTypeNotFound;
    case 'equipmentNotFound':
      return _$equipmentNotFound;
    case 'surveyNotFound':
      return _$surveyNotFound;
    case 'formNotFound':
      return _$formNotFound;
    case 'planNotFound':
      return _$planNotFound;
    case 'planVersionNotFound':
      return _$planVersionNotFound;
    case 'userDashboardNotFound':
      return _$userDashboardNotFound;
    case 'updateFormWithResponses':
      return _$updateFormWithResponses;
    case 'userNotActive':
      return _$userNotActive;
    case 'validatorError':
      return _$validatorError;
    case 'isRequired':
      return _$isRequired;
    case 'isArray':
      return _$isArray;
    case 'stringDataType':
      return _$stringDataType;
    case 'numberDataType':
      return _$numberDataType;
    case 'integerDataType':
      return _$integerDataType;
    case 'nullDataType':
      return _$nullDataType;
    case 'fileDataType':
      return _$fileDataType;
    case 'dateDataType':
      return _$dateDataType;
    case 'dateTimeDataType':
      return _$dateTimeDataType;
    case 'timeDataType':
      return _$timeDataType;
    case 'coordinateDataType':
      return _$coordinateDataType;
    case 'responseAlreadySubmittedOrApproved':
      return _$responseAlreadySubmittedOrApproved;
    case 'responseNotSubmitted':
      return _$responseNotSubmitted;
    case 'editQuestionHasAnswers':
      return _$editQuestionHasAnswers;
    case 'questionHasAlreadyAnswers':
      return _$questionHasAlreadyAnswers;
    case 'invalidSubscription':
      return _$invalidSubscription;
    case 'fileCreatorNotMatch':
      return _$fileCreatorNotMatch;
    case 'invalidIncludes':
      return _$invalidIncludes;
    case 'serviceUnavailable':
      return _$serviceUnavailable;
    case 'serviceSchemaValidatorError':
      return _$serviceSchemaValidatorError;
    case 'unauthorized':
      return _$unauthorized;
    case 'raqameUnauthorized':
      return _$raqameUnauthorized;
    case 'missingUser':
      return _$missingUser;
    case 'genericNotFound':
      return _$genericNotFound;
    case 'forbidden':
      return _$forbidden;
    case 'serverError':
      return _$serverError;
    case 'unknown':
      return _$unknown;
    default:
      return _$unknown;
  }
}

final BuiltSet<AppErrorCodesEnum> _$values =
    new BuiltSet<AppErrorCodesEnum>(const <AppErrorCodesEnum>[
  _$deletedComponentsForbidden,
  _$deleteSurveyWithResponses,
  _$deleteFormWithSurveys,
  _$deleteEntityWithUsers,
  _$deleteEntityWithEquipments,
  _$deleteEntityWithEquipmentStorePoints,
  _$deleteRoleWithUsers,
  _$deleteEquipmentTypeWithEquipments,
  _$deleteEquipmentStorePointWithEquipments,
  _$deleteYourAccount,
  _$duplicateRoleName,
  _$duplicateFormName,
  _$duplicateSurveyName,
  _$duplicateEntityName,
  _$duplicateUserDashboardName,
  _$duplicatePlanName,
  _$duplicateEquipmentStorePointName,
  _$duplicateEquipmentTypeName,
  _$duplicatePlansVersionName,
  _$duplicateUserEmail,
  _$permissionNotFound,
  _$userNotFound,
  _$fileNameNotFound,
  _$roleNotFound,
  _$entityNotFound,
  _$equipmentStorePointNotFound,
  _$equipmentTypeNotFound,
  _$equipmentNotFound,
  _$surveyNotFound,
  _$formNotFound,
  _$planNotFound,
  _$planVersionNotFound,
  _$userDashboardNotFound,
  _$updateFormWithResponses,
  _$userNotActive,
  _$validatorError,
  _$isRequired,
  _$isArray,
  _$stringDataType,
  _$numberDataType,
  _$integerDataType,
  _$nullDataType,
  _$fileDataType,
  _$dateDataType,
  _$dateTimeDataType,
  _$timeDataType,
  _$coordinateDataType,
  _$responseAlreadySubmittedOrApproved,
  _$responseNotSubmitted,
  _$editQuestionHasAnswers,
  _$questionHasAlreadyAnswers,
  _$invalidSubscription,
  _$fileCreatorNotMatch,
  _$invalidIncludes,
  _$serviceUnavailable,
  _$serviceSchemaValidatorError,
  _$unauthorized,
  _$raqameUnauthorized,
  _$missingUser,
  _$genericNotFound,
  _$forbidden,
  _$serverError,
  _$unknown,
]);

class _$AppErrorCodesEnumMeta {
  const _$AppErrorCodesEnumMeta();
  AppErrorCodesEnum get deletedComponentsForbidden =>
      _$deletedComponentsForbidden;
  AppErrorCodesEnum get deleteSurveyWithResponses =>
      _$deleteSurveyWithResponses;
  AppErrorCodesEnum get deleteFormWithSurveys => _$deleteFormWithSurveys;
  AppErrorCodesEnum get deleteEntityWithUsers => _$deleteEntityWithUsers;
  AppErrorCodesEnum get deleteEntityWithEquipments =>
      _$deleteEntityWithEquipments;
  AppErrorCodesEnum get deleteEntityWithEquipmentStorePoints =>
      _$deleteEntityWithEquipmentStorePoints;
  AppErrorCodesEnum get deleteRoleWithUsers => _$deleteRoleWithUsers;
  AppErrorCodesEnum get deleteEquipmentTypeWithEquipments =>
      _$deleteEquipmentTypeWithEquipments;
  AppErrorCodesEnum get deleteEquipmentStorePointWithEquipments =>
      _$deleteEquipmentStorePointWithEquipments;
  AppErrorCodesEnum get deleteYourAccount => _$deleteYourAccount;
  AppErrorCodesEnum get duplicateRoleName => _$duplicateRoleName;
  AppErrorCodesEnum get duplicateFormName => _$duplicateFormName;
  AppErrorCodesEnum get duplicateSurveyName => _$duplicateSurveyName;
  AppErrorCodesEnum get duplicateEntityName => _$duplicateEntityName;
  AppErrorCodesEnum get duplicateUserDashboardName =>
      _$duplicateUserDashboardName;
  AppErrorCodesEnum get duplicatePlanName => _$duplicatePlanName;
  AppErrorCodesEnum get duplicateEquipmentStorePointName =>
      _$duplicateEquipmentStorePointName;
  AppErrorCodesEnum get duplicateEquipmentTypeName =>
      _$duplicateEquipmentTypeName;
  AppErrorCodesEnum get duplicatePlansVersionName =>
      _$duplicatePlansVersionName;
  AppErrorCodesEnum get duplicateUserEmail => _$duplicateUserEmail;
  AppErrorCodesEnum get permissionNotFound => _$permissionNotFound;
  AppErrorCodesEnum get userNotFound => _$userNotFound;
  AppErrorCodesEnum get fileNameNotFound => _$fileNameNotFound;
  AppErrorCodesEnum get roleNotFound => _$roleNotFound;
  AppErrorCodesEnum get entityNotFound => _$entityNotFound;
  AppErrorCodesEnum get equipmentStorePointNotFound =>
      _$equipmentStorePointNotFound;
  AppErrorCodesEnum get equipmentTypeNotFound => _$equipmentTypeNotFound;
  AppErrorCodesEnum get equipmentNotFound => _$equipmentNotFound;
  AppErrorCodesEnum get surveyNotFound => _$surveyNotFound;
  AppErrorCodesEnum get formNotFound => _$formNotFound;
  AppErrorCodesEnum get planNotFound => _$planNotFound;
  AppErrorCodesEnum get planVersionNotFound => _$planVersionNotFound;
  AppErrorCodesEnum get userDashboardNotFound => _$userDashboardNotFound;
  AppErrorCodesEnum get updateFormWithResponses => _$updateFormWithResponses;
  AppErrorCodesEnum get userNotActive => _$userNotActive;
  AppErrorCodesEnum get validatorError => _$validatorError;
  AppErrorCodesEnum get isRequired => _$isRequired;
  AppErrorCodesEnum get isArray => _$isArray;
  AppErrorCodesEnum get stringDataType => _$stringDataType;
  AppErrorCodesEnum get numberDataType => _$numberDataType;
  AppErrorCodesEnum get integerDataType => _$integerDataType;
  AppErrorCodesEnum get nullDataType => _$nullDataType;
  AppErrorCodesEnum get fileDataType => _$fileDataType;
  AppErrorCodesEnum get dateDataType => _$dateDataType;
  AppErrorCodesEnum get dateTimeDataType => _$dateTimeDataType;
  AppErrorCodesEnum get timeDataType => _$timeDataType;
  AppErrorCodesEnum get coordinateDataType => _$coordinateDataType;
  AppErrorCodesEnum get responseAlreadySubmittedOrApproved =>
      _$responseAlreadySubmittedOrApproved;
  AppErrorCodesEnum get responseNotSubmitted => _$responseNotSubmitted;
  AppErrorCodesEnum get editQuestionHasAnswers => _$editQuestionHasAnswers;
  AppErrorCodesEnum get questionHasAlreadyAnswers =>
      _$questionHasAlreadyAnswers;
  AppErrorCodesEnum get invalidSubscription => _$invalidSubscription;
  AppErrorCodesEnum get fileCreatorNotMatch => _$fileCreatorNotMatch;
  AppErrorCodesEnum get invalidIncludes => _$invalidIncludes;
  AppErrorCodesEnum get serviceUnavailable => _$serviceUnavailable;
  AppErrorCodesEnum get serviceSchemaValidatorError =>
      _$serviceSchemaValidatorError;
  AppErrorCodesEnum get unauthorized => _$unauthorized;
  AppErrorCodesEnum get raqameUnauthorized => _$raqameUnauthorized;
  AppErrorCodesEnum get missingUser => _$missingUser;
  AppErrorCodesEnum get genericNotFound => _$genericNotFound;
  AppErrorCodesEnum get forbidden => _$forbidden;
  AppErrorCodesEnum get serverError => _$serverError;
  AppErrorCodesEnum get unknown => _$unknown;
  AppErrorCodesEnum valueOf(String name) => _$valueOf(name);
  BuiltSet<AppErrorCodesEnum> get values => _$values;
}

abstract class _$AppErrorCodesEnumMixin {
  // ignore: non_constant_identifier_names
  _$AppErrorCodesEnumMeta get AppErrorCodesEnum =>
      const _$AppErrorCodesEnumMeta();
}

Serializer<AppErrorCodesEnum> _$appErrorCodesEnumSerializer =
    new _$AppErrorCodesEnumSerializer();

class _$AppErrorCodesEnumSerializer
    implements PrimitiveSerializer<AppErrorCodesEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'deletedComponentsForbidden': 'deleted_components_forbidden',
    'deleteSurveyWithResponses': 'delete_survey_with_responses',
    'deleteFormWithSurveys': 'delete_form_with_surveys',
    'deleteEntityWithUsers': 'delete_entity_with_users',
    'deleteEntityWithEquipments': 'delete_entity_with_equipments',
    'deleteEntityWithEquipmentStorePoints':
        'delete_entity_with_equipmentStorePoints',
    'deleteRoleWithUsers': 'delete_role_with_users',
    'deleteEquipmentTypeWithEquipments':
        'delete_equipment_type_with_equipments',
    'deleteEquipmentStorePointWithEquipments':
        'delete_equipment_store_point_with_equipments',
    'deleteYourAccount': 'delete_your_account',
    'duplicateRoleName': 'duplicate_role_name',
    'duplicateFormName': 'duplicate_form_name',
    'duplicateSurveyName': 'duplicate_survey_name',
    'duplicateEntityName': 'duplicate_entity_name',
    'duplicateUserDashboardName': 'duplicate_user_dashboard_name',
    'duplicatePlanName': 'duplicate_plan_name',
    'duplicateEquipmentStorePointName': 'duplicate_equipment_store_point_name',
    'duplicateEquipmentTypeName': 'duplicate_equipment_type_name',
    'duplicatePlansVersionName': 'duplicate_plans_version_name',
    'duplicateUserEmail': 'duplicate_user_email',
    'permissionNotFound': 'permission_not_found',
    'userNotFound': 'user_not_found',
    'fileNameNotFound': 'file_name_not_found',
    'roleNotFound': 'role_not_found',
    'entityNotFound': 'entity_not_found',
    'equipmentStorePointNotFound': 'equipment_store_point_not_found',
    'equipmentTypeNotFound': 'equipment_type_not_found',
    'equipmentNotFound': 'equipment_not_found',
    'surveyNotFound': 'survey_not_found',
    'formNotFound': 'form_not_found',
    'planNotFound': 'plan_not_found',
    'planVersionNotFound': 'plan_version_not_found',
    'userDashboardNotFound': 'user_dashboard_not_found',
    'updateFormWithResponses': 'update_form_with_responses',
    'userNotActive': 'user_not_active',
    'validatorError': 'validator_error',
    'isRequired': 'isRequired',
    'isArray': 'isArray',
    'stringDataType': 'stringDataType',
    'numberDataType': 'numberDataType',
    'integerDataType': 'integerDataType',
    'nullDataType': 'nullDataType',
    'fileDataType': 'fileDataType',
    'dateDataType': 'dateDataType',
    'dateTimeDataType': 'dateTimeDataType',
    'timeDataType': 'timeDataType',
    'coordinateDataType': 'coordinateDataType',
    'responseAlreadySubmittedOrApproved':
        'response_already_submitted_or_approved',
    'responseNotSubmitted': 'response_not_submitted',
    'editQuestionHasAnswers': 'edit_question_has_answers',
    'questionHasAlreadyAnswers': 'question_has_already_answers',
    'invalidSubscription': 'invalid_subscription',
    'fileCreatorNotMatch': 'file_creator_not_match',
    'invalidIncludes': 'invalid_includes',
    'serviceUnavailable': 'service_unavailable',
    'serviceSchemaValidatorError': 'service_schema_validator_error',
    'unauthorized': 'unauthorized',
    'raqameUnauthorized': 'raqame_unauthorized',
    'missingUser': 'missing_user',
    'genericNotFound': 'generic_not_found',
    'forbidden': 'forbidden',
    'serverError': 'server_error',
    'unknown': 'unknown',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'deleted_components_forbidden': 'deletedComponentsForbidden',
    'delete_survey_with_responses': 'deleteSurveyWithResponses',
    'delete_form_with_surveys': 'deleteFormWithSurveys',
    'delete_entity_with_users': 'deleteEntityWithUsers',
    'delete_entity_with_equipments': 'deleteEntityWithEquipments',
    'delete_entity_with_equipmentStorePoints':
        'deleteEntityWithEquipmentStorePoints',
    'delete_role_with_users': 'deleteRoleWithUsers',
    'delete_equipment_type_with_equipments':
        'deleteEquipmentTypeWithEquipments',
    'delete_equipment_store_point_with_equipments':
        'deleteEquipmentStorePointWithEquipments',
    'delete_your_account': 'deleteYourAccount',
    'duplicate_role_name': 'duplicateRoleName',
    'duplicate_form_name': 'duplicateFormName',
    'duplicate_survey_name': 'duplicateSurveyName',
    'duplicate_entity_name': 'duplicateEntityName',
    'duplicate_user_dashboard_name': 'duplicateUserDashboardName',
    'duplicate_plan_name': 'duplicatePlanName',
    'duplicate_equipment_store_point_name': 'duplicateEquipmentStorePointName',
    'duplicate_equipment_type_name': 'duplicateEquipmentTypeName',
    'duplicate_plans_version_name': 'duplicatePlansVersionName',
    'duplicate_user_email': 'duplicateUserEmail',
    'permission_not_found': 'permissionNotFound',
    'user_not_found': 'userNotFound',
    'file_name_not_found': 'fileNameNotFound',
    'role_not_found': 'roleNotFound',
    'entity_not_found': 'entityNotFound',
    'equipment_store_point_not_found': 'equipmentStorePointNotFound',
    'equipment_type_not_found': 'equipmentTypeNotFound',
    'equipment_not_found': 'equipmentNotFound',
    'survey_not_found': 'surveyNotFound',
    'form_not_found': 'formNotFound',
    'plan_not_found': 'planNotFound',
    'plan_version_not_found': 'planVersionNotFound',
    'user_dashboard_not_found': 'userDashboardNotFound',
    'update_form_with_responses': 'updateFormWithResponses',
    'user_not_active': 'userNotActive',
    'validator_error': 'validatorError',
    'isRequired': 'isRequired',
    'isArray': 'isArray',
    'stringDataType': 'stringDataType',
    'numberDataType': 'numberDataType',
    'integerDataType': 'integerDataType',
    'nullDataType': 'nullDataType',
    'fileDataType': 'fileDataType',
    'dateDataType': 'dateDataType',
    'dateTimeDataType': 'dateTimeDataType',
    'timeDataType': 'timeDataType',
    'coordinateDataType': 'coordinateDataType',
    'response_already_submitted_or_approved':
        'responseAlreadySubmittedOrApproved',
    'response_not_submitted': 'responseNotSubmitted',
    'edit_question_has_answers': 'editQuestionHasAnswers',
    'question_has_already_answers': 'questionHasAlreadyAnswers',
    'invalid_subscription': 'invalidSubscription',
    'file_creator_not_match': 'fileCreatorNotMatch',
    'invalid_includes': 'invalidIncludes',
    'service_unavailable': 'serviceUnavailable',
    'service_schema_validator_error': 'serviceSchemaValidatorError',
    'unauthorized': 'unauthorized',
    'raqame_unauthorized': 'raqameUnauthorized',
    'missing_user': 'missingUser',
    'generic_not_found': 'genericNotFound',
    'forbidden': 'forbidden',
    'server_error': 'serverError',
    'unknown': 'unknown',
  };

  @override
  final Iterable<Type> types = const <Type>[AppErrorCodesEnum];
  @override
  final String wireName = 'AppErrorCodesEnum';

  @override
  Object serialize(Serializers serializers, AppErrorCodesEnum object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  AppErrorCodesEnum deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      AppErrorCodesEnum.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
