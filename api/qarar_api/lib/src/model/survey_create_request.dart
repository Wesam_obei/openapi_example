//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'survey_create_request.g.dart';

/// SurveyCreateRequest
///
/// Properties:
/// * [name]
/// * [formId]
/// * [isActive]
/// * [startAt]
/// * [endAt]
/// * [usersIds]
/// * [entitiesIds]
/// * [description]
@BuiltValue()
abstract class SurveyCreateRequest
    implements Built<SurveyCreateRequest, SurveyCreateRequestBuilder> {
  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'form_id')
  num get formId;

  @BuiltValueField(wireName: r'is_active')
  bool get isActive;

  @BuiltValueField(wireName: r'start_at')
  DateTime get startAt;

  @BuiltValueField(wireName: r'end_at')
  DateTime get endAt;

  @BuiltValueField(wireName: r'usersIds')
  BuiltList<num> get usersIds;

  @BuiltValueField(wireName: r'entitiesIds')
  BuiltList<num> get entitiesIds;

  @BuiltValueField(wireName: r'description')
  String? get description;

  SurveyCreateRequest._();

  factory SurveyCreateRequest([void updates(SurveyCreateRequestBuilder b)]) =
      _$SurveyCreateRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(SurveyCreateRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<SurveyCreateRequest> get serializer =>
      _$SurveyCreateRequestSerializer();
}

class _$SurveyCreateRequestSerializer
    implements PrimitiveSerializer<SurveyCreateRequest> {
  @override
  final Iterable<Type> types = const [
    SurveyCreateRequest,
    _$SurveyCreateRequest
  ];

  @override
  final String wireName = r'SurveyCreateRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    SurveyCreateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    yield r'form_id';
    yield serializers.serialize(
      object.formId,
      specifiedType: const FullType(num),
    );
    yield r'is_active';
    yield serializers.serialize(
      object.isActive,
      specifiedType: const FullType(bool),
    );
    yield r'start_at';
    yield serializers.serialize(
      object.startAt,
      specifiedType: const FullType(DateTime),
    );
    yield r'end_at';
    yield serializers.serialize(
      object.endAt,
      specifiedType: const FullType(DateTime),
    );
    yield r'usersIds';
    yield serializers.serialize(
      object.usersIds,
      specifiedType: const FullType(BuiltList, [FullType(num)]),
    );
    yield r'entitiesIds';
    yield serializers.serialize(
      object.entitiesIds,
      specifiedType: const FullType(BuiltList, [FullType(num)]),
    );
    if (object.description != null) {
      yield r'description';
      yield serializers.serialize(
        object.description,
        specifiedType: const FullType(String),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    SurveyCreateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required SurveyCreateRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'form_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.formId = valueDes;
          break;
        case r'is_active':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.isActive = valueDes;
          break;
        case r'start_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.startAt = valueDes;
          break;
        case r'end_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.endAt = valueDes;
          break;
        case r'usersIds':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(num)]),
          ) as BuiltList<num>;
          result.usersIds.replace(valueDes);
          break;
        case r'entitiesIds':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(num)]),
          ) as BuiltList<num>;
          result.entitiesIds.replace(valueDes);
          break;
        case r'description':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.description = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  SurveyCreateRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = SurveyCreateRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
