//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_manhole_map200_response_all_of_data_features_inner.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_manhole_map200_response_all_of_data.g.dart';

/// MapLayersControllerGetManholeMap200ResponseAllOfData
///
/// Properties:
/// * [type]
/// * [features]
@BuiltValue()
abstract class MapLayersControllerGetManholeMap200ResponseAllOfData
    implements
        Built<MapLayersControllerGetManholeMap200ResponseAllOfData,
            MapLayersControllerGetManholeMap200ResponseAllOfDataBuilder> {
  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum get type;
  // enum typeEnum {  FeatureCollection,  };

  @BuiltValueField(wireName: r'features')
  BuiltList<MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner>
      get features;

  MapLayersControllerGetManholeMap200ResponseAllOfData._();

  factory MapLayersControllerGetManholeMap200ResponseAllOfData(
          [void updates(
              MapLayersControllerGetManholeMap200ResponseAllOfDataBuilder b)]) =
      _$MapLayersControllerGetManholeMap200ResponseAllOfData;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetManholeMap200ResponseAllOfDataBuilder b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<MapLayersControllerGetManholeMap200ResponseAllOfData>
      get serializer =>
          _$MapLayersControllerGetManholeMap200ResponseAllOfDataSerializer();
}

class _$MapLayersControllerGetManholeMap200ResponseAllOfDataSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetManholeMap200ResponseAllOfData> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetManholeMap200ResponseAllOfData,
    _$MapLayersControllerGetManholeMap200ResponseAllOfData
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetManholeMap200ResponseAllOfData';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetManholeMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(
          MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum),
    );
    yield r'features';
    yield serializers.serialize(
      object.features,
      specifiedType: const FullType(BuiltList, [
        FullType(
            MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner)
      ]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetManholeMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetManholeMap200ResponseAllOfDataBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum),
          ) as MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum;
          result.type = valueDes;
          break;
        case r'features':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [
              FullType(
                  MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner)
            ]),
          ) as BuiltList<
              MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner>;
          result.features.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetManholeMap200ResponseAllOfData deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetManholeMap200ResponseAllOfDataBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum
    extends EnumClass {
  @BuiltValueEnumConst(wireName: r'FeatureCollection', fallback: true)
  static const MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum
      featureCollection =
      _$mapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum_featureCollection;

  static Serializer<
          MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum>
      get serializer =>
          _$mapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnumSerializer;

  const MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum._(
      String name)
      : super(name);

  static BuiltSet<MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum>
      get values =>
          _$mapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnumValues;
  static MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum valueOf(
          String name) =>
      _$mapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnumValueOf(
          name);
}
