// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$LoginRequest extends LoginRequest {
  @override
  final String sessionID;
  @override
  final String privateToken;

  factory _$LoginRequest([void Function(LoginRequestBuilder)? updates]) =>
      (new LoginRequestBuilder()..update(updates))._build();

  _$LoginRequest._({required this.sessionID, required this.privateToken})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        sessionID, r'LoginRequest', 'sessionID');
    BuiltValueNullFieldError.checkNotNull(
        privateToken, r'LoginRequest', 'privateToken');
  }

  @override
  LoginRequest rebuild(void Function(LoginRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LoginRequestBuilder toBuilder() => new LoginRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LoginRequest &&
        sessionID == other.sessionID &&
        privateToken == other.privateToken;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, sessionID.hashCode);
    _$hash = $jc(_$hash, privateToken.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'LoginRequest')
          ..add('sessionID', sessionID)
          ..add('privateToken', privateToken))
        .toString();
  }
}

class LoginRequestBuilder
    implements Builder<LoginRequest, LoginRequestBuilder> {
  _$LoginRequest? _$v;

  String? _sessionID;
  String? get sessionID => _$this._sessionID;
  set sessionID(String? sessionID) => _$this._sessionID = sessionID;

  String? _privateToken;
  String? get privateToken => _$this._privateToken;
  set privateToken(String? privateToken) => _$this._privateToken = privateToken;

  LoginRequestBuilder() {
    LoginRequest._defaults(this);
  }

  LoginRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _sessionID = $v.sessionID;
      _privateToken = $v.privateToken;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LoginRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$LoginRequest;
  }

  @override
  void update(void Function(LoginRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  LoginRequest build() => _build();

  _$LoginRequest _build() {
    final _$result = _$v ??
        new _$LoginRequest._(
            sessionID: BuiltValueNullFieldError.checkNotNull(
                sessionID, r'LoginRequest', 'sessionID'),
            privateToken: BuiltValueNullFieldError.checkNotNull(
                privateToken, r'LoginRequest', 'privateToken'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
