// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'surveys_controller_get200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SurveysControllerGet200Response
    extends SurveysControllerGet200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$SurveysControllerGet200Response(
          [void Function(SurveysControllerGet200ResponseBuilder)? updates]) =>
      (new SurveysControllerGet200ResponseBuilder()..update(updates))._build();

  _$SurveysControllerGet200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'SurveysControllerGet200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'SurveysControllerGet200Response', 'data');
  }

  @override
  SurveysControllerGet200Response rebuild(
          void Function(SurveysControllerGet200ResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SurveysControllerGet200ResponseBuilder toBuilder() =>
      new SurveysControllerGet200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SurveysControllerGet200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SurveysControllerGet200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class SurveysControllerGet200ResponseBuilder
    implements
        Builder<SurveysControllerGet200Response,
            SurveysControllerGet200ResponseBuilder>,
        SuccessResponseBuilder {
  _$SurveysControllerGet200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  SurveysControllerGet200ResponseBuilder() {
    SurveysControllerGet200Response._defaults(this);
  }

  SurveysControllerGet200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant SurveysControllerGet200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SurveysControllerGet200Response;
  }

  @override
  void update(void Function(SurveysControllerGet200ResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SurveysControllerGet200Response build() => _build();

  _$SurveysControllerGet200Response _build() {
    final _$result = _$v ??
        new _$SurveysControllerGet200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success, r'SurveysControllerGet200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'SurveysControllerGet200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
