// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_electrical_facilities_map200_response_all_of_data_features_inner_properties.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerProperties
    extends MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerProperties {
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      id;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      facilityId;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      facilityDescription;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      enabled;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      facilityType;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      facilityName;

  factory _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerProperties(
          [void Function(
                  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
              updates]) =>
      (new MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerProperties._(
      {this.id,
      this.facilityId,
      this.facilityDescription,
      this.enabled,
      this.facilityType,
      this.facilityName})
      : super._();

  @override
  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerProperties
      rebuild(
              void Function(
                      MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)
                  updates) =>
          (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      toBuilder() =>
          new MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerProperties &&
        id == other.id &&
        facilityId == other.facilityId &&
        facilityDescription == other.facilityDescription &&
        enabled == other.enabled &&
        facilityType == other.facilityType &&
        facilityName == other.facilityName;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, facilityId.hashCode);
    _$hash = $jc(_$hash, facilityDescription.hashCode);
    _$hash = $jc(_$hash, enabled.hashCode);
    _$hash = $jc(_$hash, facilityType.hashCode);
    _$hash = $jc(_$hash, facilityName.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerProperties')
          ..add('id', id)
          ..add('facilityId', facilityId)
          ..add('facilityDescription', facilityDescription)
          ..add('enabled', enabled)
          ..add('facilityType', facilityType)
          ..add('facilityName', facilityName))
        .toString();
  }
}

class MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
    implements
        Builder<
            MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerProperties,
            MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder> {
  _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerProperties?
      _$v;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _id;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get id => _$this._id ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set id(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              id) =>
      _$this._id = id;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _facilityId;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get facilityId => _$this._facilityId ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set facilityId(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              facilityId) =>
      _$this._facilityId = facilityId;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _facilityDescription;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get facilityDescription => _$this._facilityDescription ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set facilityDescription(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              facilityDescription) =>
      _$this._facilityDescription = facilityDescription;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _enabled;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get enabled => _$this._enabled ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set enabled(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              enabled) =>
      _$this._enabled = enabled;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _facilityType;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get facilityType => _$this._facilityType ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set facilityType(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              facilityType) =>
      _$this._facilityType = facilityType;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _facilityName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get facilityName => _$this._facilityName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set facilityName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              facilityName) =>
      _$this._facilityName = facilityName;

  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder() {
    MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerProperties
        ._defaults(this);
  }

  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id?.toBuilder();
      _facilityId = $v.facilityId?.toBuilder();
      _facilityDescription = $v.facilityDescription?.toBuilder();
      _enabled = $v.enabled?.toBuilder();
      _facilityType = $v.facilityType?.toBuilder();
      _facilityName = $v.facilityName?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerProperties
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerProperties;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerProperties
      build() => _build();

  _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerProperties
      _build() {
    _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerProperties
        _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerProperties
              ._(
              id: _id?.build(),
              facilityId: _facilityId?.build(),
              facilityDescription: _facilityDescription?.build(),
              enabled: _enabled?.build(),
              facilityType: _facilityType?.build(),
              facilityName: _facilityName?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'id';
        _id?.build();
        _$failedField = 'facilityId';
        _facilityId?.build();
        _$failedField = 'facilityDescription';
        _facilityDescription?.build();
        _$failedField = 'enabled';
        _enabled?.build();
        _$failedField = 'facilityType';
        _facilityType?.build();
        _$failedField = 'facilityName';
        _facilityName?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInnerProperties',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
