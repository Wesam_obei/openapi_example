//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/paginated_links_documented.dart';
import 'package:qarar_api/src/model/paginated_meta_documented.dart';
import 'package:qarar_api/src/model/paginated_documented.dart';
import 'package:qarar_api/src/model/list_user_notifications_response_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'notifications_controller_my_notifications200_response.g.dart';

/// NotificationsControllerMyNotifications200Response
///
/// Properties:
/// * [success]
/// * [data]
/// * [meta]
/// * [links]
@BuiltValue()
abstract class NotificationsControllerMyNotifications200Response
    implements
        PaginatedDocumented,
        Built<NotificationsControllerMyNotifications200Response,
            NotificationsControllerMyNotifications200ResponseBuilder> {
  NotificationsControllerMyNotifications200Response._();

  factory NotificationsControllerMyNotifications200Response(
          [void updates(
              NotificationsControllerMyNotifications200ResponseBuilder b)]) =
      _$NotificationsControllerMyNotifications200Response;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          NotificationsControllerMyNotifications200ResponseBuilder b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<NotificationsControllerMyNotifications200Response>
      get serializer =>
          _$NotificationsControllerMyNotifications200ResponseSerializer();
}

class _$NotificationsControllerMyNotifications200ResponseSerializer
    implements
        PrimitiveSerializer<NotificationsControllerMyNotifications200Response> {
  @override
  final Iterable<Type> types = const [
    NotificationsControllerMyNotifications200Response,
    _$NotificationsControllerMyNotifications200Response
  ];

  @override
  final String wireName = r'NotificationsControllerMyNotifications200Response';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    NotificationsControllerMyNotifications200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'links';
    yield serializers.serialize(
      object.links,
      specifiedType: const FullType(PaginatedLinksDocumented),
    );
    yield r'data';
    yield serializers.serialize(
      object.data,
      specifiedType: const FullType(BuiltList, [FullType(JsonObject)]),
    );
    yield r'success';
    yield serializers.serialize(
      object.success,
      specifiedType: const FullType(bool),
    );
    yield r'meta';
    yield serializers.serialize(
      object.meta,
      specifiedType: const FullType(PaginatedMetaDocumented),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    NotificationsControllerMyNotifications200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required NotificationsControllerMyNotifications200ResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'links':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(PaginatedLinksDocumented),
          ) as PaginatedLinksDocumented;
          result.links.replace(valueDes);
          break;
        case r'data':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(JsonObject)]),
          ) as BuiltList<JsonObject>;
          result.data.replace(valueDes);
          break;
        case r'success':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.success = valueDes;
          break;
        case r'meta':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(PaginatedMetaDocumented),
          ) as PaginatedMetaDocumented;
          result.meta.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  NotificationsControllerMyNotifications200Response deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = NotificationsControllerMyNotifications200ResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
