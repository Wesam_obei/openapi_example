// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_plan_version_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetPlanVersionDataResponse extends GetPlanVersionDataResponse {
  @override
  final num id;
  @override
  final num revisionNumber;
  @override
  final num revisionYear;
  @override
  final String issuedAt;
  @override
  final GetFileDataResponse file;

  factory _$GetPlanVersionDataResponse(
          [void Function(GetPlanVersionDataResponseBuilder)? updates]) =>
      (new GetPlanVersionDataResponseBuilder()..update(updates))._build();

  _$GetPlanVersionDataResponse._(
      {required this.id,
      required this.revisionNumber,
      required this.revisionYear,
      required this.issuedAt,
      required this.file})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'GetPlanVersionDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        revisionNumber, r'GetPlanVersionDataResponse', 'revisionNumber');
    BuiltValueNullFieldError.checkNotNull(
        revisionYear, r'GetPlanVersionDataResponse', 'revisionYear');
    BuiltValueNullFieldError.checkNotNull(
        issuedAt, r'GetPlanVersionDataResponse', 'issuedAt');
    BuiltValueNullFieldError.checkNotNull(
        file, r'GetPlanVersionDataResponse', 'file');
  }

  @override
  GetPlanVersionDataResponse rebuild(
          void Function(GetPlanVersionDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetPlanVersionDataResponseBuilder toBuilder() =>
      new GetPlanVersionDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetPlanVersionDataResponse &&
        id == other.id &&
        revisionNumber == other.revisionNumber &&
        revisionYear == other.revisionYear &&
        issuedAt == other.issuedAt &&
        file == other.file;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, revisionNumber.hashCode);
    _$hash = $jc(_$hash, revisionYear.hashCode);
    _$hash = $jc(_$hash, issuedAt.hashCode);
    _$hash = $jc(_$hash, file.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetPlanVersionDataResponse')
          ..add('id', id)
          ..add('revisionNumber', revisionNumber)
          ..add('revisionYear', revisionYear)
          ..add('issuedAt', issuedAt)
          ..add('file', file))
        .toString();
  }
}

class GetPlanVersionDataResponseBuilder
    implements
        Builder<GetPlanVersionDataResponse, GetPlanVersionDataResponseBuilder> {
  _$GetPlanVersionDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  num? _revisionNumber;
  num? get revisionNumber => _$this._revisionNumber;
  set revisionNumber(num? revisionNumber) =>
      _$this._revisionNumber = revisionNumber;

  num? _revisionYear;
  num? get revisionYear => _$this._revisionYear;
  set revisionYear(num? revisionYear) => _$this._revisionYear = revisionYear;

  String? _issuedAt;
  String? get issuedAt => _$this._issuedAt;
  set issuedAt(String? issuedAt) => _$this._issuedAt = issuedAt;

  GetFileDataResponseBuilder? _file;
  GetFileDataResponseBuilder get file =>
      _$this._file ??= new GetFileDataResponseBuilder();
  set file(GetFileDataResponseBuilder? file) => _$this._file = file;

  GetPlanVersionDataResponseBuilder() {
    GetPlanVersionDataResponse._defaults(this);
  }

  GetPlanVersionDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _revisionNumber = $v.revisionNumber;
      _revisionYear = $v.revisionYear;
      _issuedAt = $v.issuedAt;
      _file = $v.file.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetPlanVersionDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetPlanVersionDataResponse;
  }

  @override
  void update(void Function(GetPlanVersionDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetPlanVersionDataResponse build() => _build();

  _$GetPlanVersionDataResponse _build() {
    _$GetPlanVersionDataResponse _$result;
    try {
      _$result = _$v ??
          new _$GetPlanVersionDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'GetPlanVersionDataResponse', 'id'),
              revisionNumber: BuiltValueNullFieldError.checkNotNull(
                  revisionNumber,
                  r'GetPlanVersionDataResponse',
                  'revisionNumber'),
              revisionYear: BuiltValueNullFieldError.checkNotNull(
                  revisionYear, r'GetPlanVersionDataResponse', 'revisionYear'),
              issuedAt: BuiltValueNullFieldError.checkNotNull(
                  issuedAt, r'GetPlanVersionDataResponse', 'issuedAt'),
              file: file.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'file';
        file.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'GetPlanVersionDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
