// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_construction_licenses_map200_response_all_of_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum_featureCollection =
    const MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum
        ._('featureCollection');

MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'featureCollection':
      return _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum_featureCollection;
    default:
      return _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum_featureCollection;
  }
}

final BuiltSet<
        MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum>(const <MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum>[
  _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum_featureCollection,
]);

Serializer<
        MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnumSerializer =
    new _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnumSerializer();

class _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'featureCollection': 'FeatureCollection',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'FeatureCollection': 'featureCollection',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum';

  @override
  Object serialize(
          Serializers serializers,
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum
              object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum
      deserialize(Serializers serializers, Object serialized,
              {FullType specifiedType = FullType.unspecified}) =>
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum
              .valueOf(_fromWire[serialized] ??
                  (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData
    extends MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData {
  @override
  final MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum
      type;
  @override
  final BuiltList<
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner>
      features;

  factory _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData(
          [void Function(
                  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataBuilder)?
              updates]) =>
      (new MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData._(
      {required this.type, required this.features})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        type,
        r'MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData',
        'type');
    BuiltValueNullFieldError.checkNotNull(
        features,
        r'MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData',
        'features');
  }

  @override
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData rebuild(
          void Function(
                  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataBuilder
      toBuilder() =>
          new MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData &&
        type == other.type &&
        features == other.features;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, features.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData')
          ..add('type', type)
          ..add('features', features))
        .toString();
  }
}

class MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataBuilder
    implements
        Builder<
            MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData,
            MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataBuilder> {
  _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData? _$v;

  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum?
      _type;
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum?
      get type => _$this._type;
  set type(
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum?
              type) =>
      _$this._type = type;

  ListBuilder<
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner>?
      _features;
  ListBuilder<
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner>
      get features => _$this._features ??= new ListBuilder<
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner>();
  set features(
          ListBuilder<
                  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner>?
              features) =>
      _$this._features = features;

  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataBuilder() {
    MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData._defaults(
        this);
  }

  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _features = $v.features.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData build() =>
      _build();

  _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData _build() {
    _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData
        _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData
              ._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData',
                  'type'),
              features: features.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'features';
        features.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
