// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'equipment_store_point_create_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$EquipmentStorePointCreateRequest
    extends EquipmentStorePointCreateRequest {
  @override
  final String name;
  @override
  final LocationData location;
  @override
  final num entityId;
  @override
  final String? description;
  @override
  final String? streetName;

  factory _$EquipmentStorePointCreateRequest(
          [void Function(EquipmentStorePointCreateRequestBuilder)? updates]) =>
      (new EquipmentStorePointCreateRequestBuilder()..update(updates))._build();

  _$EquipmentStorePointCreateRequest._(
      {required this.name,
      required this.location,
      required this.entityId,
      this.description,
      this.streetName})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        name, r'EquipmentStorePointCreateRequest', 'name');
    BuiltValueNullFieldError.checkNotNull(
        location, r'EquipmentStorePointCreateRequest', 'location');
    BuiltValueNullFieldError.checkNotNull(
        entityId, r'EquipmentStorePointCreateRequest', 'entityId');
  }

  @override
  EquipmentStorePointCreateRequest rebuild(
          void Function(EquipmentStorePointCreateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  EquipmentStorePointCreateRequestBuilder toBuilder() =>
      new EquipmentStorePointCreateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EquipmentStorePointCreateRequest &&
        name == other.name &&
        location == other.location &&
        entityId == other.entityId &&
        description == other.description &&
        streetName == other.streetName;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, location.hashCode);
    _$hash = $jc(_$hash, entityId.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, streetName.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'EquipmentStorePointCreateRequest')
          ..add('name', name)
          ..add('location', location)
          ..add('entityId', entityId)
          ..add('description', description)
          ..add('streetName', streetName))
        .toString();
  }
}

class EquipmentStorePointCreateRequestBuilder
    implements
        Builder<EquipmentStorePointCreateRequest,
            EquipmentStorePointCreateRequestBuilder> {
  _$EquipmentStorePointCreateRequest? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  LocationDataBuilder? _location;
  LocationDataBuilder get location =>
      _$this._location ??= new LocationDataBuilder();
  set location(LocationDataBuilder? location) => _$this._location = location;

  num? _entityId;
  num? get entityId => _$this._entityId;
  set entityId(num? entityId) => _$this._entityId = entityId;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  String? _streetName;
  String? get streetName => _$this._streetName;
  set streetName(String? streetName) => _$this._streetName = streetName;

  EquipmentStorePointCreateRequestBuilder() {
    EquipmentStorePointCreateRequest._defaults(this);
  }

  EquipmentStorePointCreateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _location = $v.location.toBuilder();
      _entityId = $v.entityId;
      _description = $v.description;
      _streetName = $v.streetName;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(EquipmentStorePointCreateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$EquipmentStorePointCreateRequest;
  }

  @override
  void update(void Function(EquipmentStorePointCreateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  EquipmentStorePointCreateRequest build() => _build();

  _$EquipmentStorePointCreateRequest _build() {
    _$EquipmentStorePointCreateRequest _$result;
    try {
      _$result = _$v ??
          new _$EquipmentStorePointCreateRequest._(
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'EquipmentStorePointCreateRequest', 'name'),
              location: location.build(),
              entityId: BuiltValueNullFieldError.checkNotNull(
                  entityId, r'EquipmentStorePointCreateRequest', 'entityId'),
              description: description,
              streetName: streetName);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'location';
        location.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'EquipmentStorePointCreateRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
