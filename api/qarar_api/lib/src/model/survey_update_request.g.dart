// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'survey_update_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SurveyUpdateRequest extends SurveyUpdateRequest {
  @override
  final String name;
  @override
  final num formId;
  @override
  final bool isActive;
  @override
  final DateTime startAt;
  @override
  final DateTime endAt;
  @override
  final BuiltList<num> usersIds;
  @override
  final BuiltList<num> entitiesIds;
  @override
  final String? description;
  @override
  final BuiltList<num>? deletedUsersIds;
  @override
  final BuiltList<num>? deletedEntitiesIds;

  factory _$SurveyUpdateRequest(
          [void Function(SurveyUpdateRequestBuilder)? updates]) =>
      (new SurveyUpdateRequestBuilder()..update(updates))._build();

  _$SurveyUpdateRequest._(
      {required this.name,
      required this.formId,
      required this.isActive,
      required this.startAt,
      required this.endAt,
      required this.usersIds,
      required this.entitiesIds,
      this.description,
      this.deletedUsersIds,
      this.deletedEntitiesIds})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(name, r'SurveyUpdateRequest', 'name');
    BuiltValueNullFieldError.checkNotNull(
        formId, r'SurveyUpdateRequest', 'formId');
    BuiltValueNullFieldError.checkNotNull(
        isActive, r'SurveyUpdateRequest', 'isActive');
    BuiltValueNullFieldError.checkNotNull(
        startAt, r'SurveyUpdateRequest', 'startAt');
    BuiltValueNullFieldError.checkNotNull(
        endAt, r'SurveyUpdateRequest', 'endAt');
    BuiltValueNullFieldError.checkNotNull(
        usersIds, r'SurveyUpdateRequest', 'usersIds');
    BuiltValueNullFieldError.checkNotNull(
        entitiesIds, r'SurveyUpdateRequest', 'entitiesIds');
  }

  @override
  SurveyUpdateRequest rebuild(
          void Function(SurveyUpdateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SurveyUpdateRequestBuilder toBuilder() =>
      new SurveyUpdateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SurveyUpdateRequest &&
        name == other.name &&
        formId == other.formId &&
        isActive == other.isActive &&
        startAt == other.startAt &&
        endAt == other.endAt &&
        usersIds == other.usersIds &&
        entitiesIds == other.entitiesIds &&
        description == other.description &&
        deletedUsersIds == other.deletedUsersIds &&
        deletedEntitiesIds == other.deletedEntitiesIds;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, formId.hashCode);
    _$hash = $jc(_$hash, isActive.hashCode);
    _$hash = $jc(_$hash, startAt.hashCode);
    _$hash = $jc(_$hash, endAt.hashCode);
    _$hash = $jc(_$hash, usersIds.hashCode);
    _$hash = $jc(_$hash, entitiesIds.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, deletedUsersIds.hashCode);
    _$hash = $jc(_$hash, deletedEntitiesIds.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SurveyUpdateRequest')
          ..add('name', name)
          ..add('formId', formId)
          ..add('isActive', isActive)
          ..add('startAt', startAt)
          ..add('endAt', endAt)
          ..add('usersIds', usersIds)
          ..add('entitiesIds', entitiesIds)
          ..add('description', description)
          ..add('deletedUsersIds', deletedUsersIds)
          ..add('deletedEntitiesIds', deletedEntitiesIds))
        .toString();
  }
}

class SurveyUpdateRequestBuilder
    implements Builder<SurveyUpdateRequest, SurveyUpdateRequestBuilder> {
  _$SurveyUpdateRequest? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  num? _formId;
  num? get formId => _$this._formId;
  set formId(num? formId) => _$this._formId = formId;

  bool? _isActive;
  bool? get isActive => _$this._isActive;
  set isActive(bool? isActive) => _$this._isActive = isActive;

  DateTime? _startAt;
  DateTime? get startAt => _$this._startAt;
  set startAt(DateTime? startAt) => _$this._startAt = startAt;

  DateTime? _endAt;
  DateTime? get endAt => _$this._endAt;
  set endAt(DateTime? endAt) => _$this._endAt = endAt;

  ListBuilder<num>? _usersIds;
  ListBuilder<num> get usersIds => _$this._usersIds ??= new ListBuilder<num>();
  set usersIds(ListBuilder<num>? usersIds) => _$this._usersIds = usersIds;

  ListBuilder<num>? _entitiesIds;
  ListBuilder<num> get entitiesIds =>
      _$this._entitiesIds ??= new ListBuilder<num>();
  set entitiesIds(ListBuilder<num>? entitiesIds) =>
      _$this._entitiesIds = entitiesIds;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  ListBuilder<num>? _deletedUsersIds;
  ListBuilder<num> get deletedUsersIds =>
      _$this._deletedUsersIds ??= new ListBuilder<num>();
  set deletedUsersIds(ListBuilder<num>? deletedUsersIds) =>
      _$this._deletedUsersIds = deletedUsersIds;

  ListBuilder<num>? _deletedEntitiesIds;
  ListBuilder<num> get deletedEntitiesIds =>
      _$this._deletedEntitiesIds ??= new ListBuilder<num>();
  set deletedEntitiesIds(ListBuilder<num>? deletedEntitiesIds) =>
      _$this._deletedEntitiesIds = deletedEntitiesIds;

  SurveyUpdateRequestBuilder() {
    SurveyUpdateRequest._defaults(this);
  }

  SurveyUpdateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _formId = $v.formId;
      _isActive = $v.isActive;
      _startAt = $v.startAt;
      _endAt = $v.endAt;
      _usersIds = $v.usersIds.toBuilder();
      _entitiesIds = $v.entitiesIds.toBuilder();
      _description = $v.description;
      _deletedUsersIds = $v.deletedUsersIds?.toBuilder();
      _deletedEntitiesIds = $v.deletedEntitiesIds?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SurveyUpdateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SurveyUpdateRequest;
  }

  @override
  void update(void Function(SurveyUpdateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SurveyUpdateRequest build() => _build();

  _$SurveyUpdateRequest _build() {
    _$SurveyUpdateRequest _$result;
    try {
      _$result = _$v ??
          new _$SurveyUpdateRequest._(
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'SurveyUpdateRequest', 'name'),
              formId: BuiltValueNullFieldError.checkNotNull(
                  formId, r'SurveyUpdateRequest', 'formId'),
              isActive: BuiltValueNullFieldError.checkNotNull(
                  isActive, r'SurveyUpdateRequest', 'isActive'),
              startAt: BuiltValueNullFieldError.checkNotNull(
                  startAt, r'SurveyUpdateRequest', 'startAt'),
              endAt: BuiltValueNullFieldError.checkNotNull(
                  endAt, r'SurveyUpdateRequest', 'endAt'),
              usersIds: usersIds.build(),
              entitiesIds: entitiesIds.build(),
              description: description,
              deletedUsersIds: _deletedUsersIds?.build(),
              deletedEntitiesIds: _deletedEntitiesIds?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'usersIds';
        usersIds.build();
        _$failedField = 'entitiesIds';
        entitiesIds.build();

        _$failedField = 'deletedUsersIds';
        _deletedUsersIds?.build();
        _$failedField = 'deletedEntitiesIds';
        _deletedEntitiesIds?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'SurveyUpdateRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
