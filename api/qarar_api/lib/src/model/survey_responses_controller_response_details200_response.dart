//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/success_response.dart';
import 'package:qarar_api/src/model/get_survey_response_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'survey_responses_controller_response_details200_response.g.dart';

/// My custom description
///
/// Properties:
/// * [success]
/// * [data]
@BuiltValue()
abstract class SurveyResponsesControllerResponseDetails200Response
    implements
        SuccessResponse,
        Built<SurveyResponsesControllerResponseDetails200Response,
            SurveyResponsesControllerResponseDetails200ResponseBuilder> {
  SurveyResponsesControllerResponseDetails200Response._();

  factory SurveyResponsesControllerResponseDetails200Response(
          [void updates(
              SurveyResponsesControllerResponseDetails200ResponseBuilder b)]) =
      _$SurveyResponsesControllerResponseDetails200Response;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          SurveyResponsesControllerResponseDetails200ResponseBuilder b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<SurveyResponsesControllerResponseDetails200Response>
      get serializer =>
          _$SurveyResponsesControllerResponseDetails200ResponseSerializer();
}

class _$SurveyResponsesControllerResponseDetails200ResponseSerializer
    implements
        PrimitiveSerializer<
            SurveyResponsesControllerResponseDetails200Response> {
  @override
  final Iterable<Type> types = const [
    SurveyResponsesControllerResponseDetails200Response,
    _$SurveyResponsesControllerResponseDetails200Response
  ];

  @override
  final String wireName =
      r'SurveyResponsesControllerResponseDetails200Response';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    SurveyResponsesControllerResponseDetails200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'data';
    yield serializers.serialize(
      object.data,
      specifiedType: const FullType(JsonObject),
    );
    yield r'success';
    yield serializers.serialize(
      object.success,
      specifiedType: const FullType(bool),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    SurveyResponsesControllerResponseDetails200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required SurveyResponsesControllerResponseDetails200ResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'data':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.data = valueDes;
          break;
        case r'success':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.success = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  SurveyResponsesControllerResponseDetails200Response deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = SurveyResponsesControllerResponseDetails200ResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
