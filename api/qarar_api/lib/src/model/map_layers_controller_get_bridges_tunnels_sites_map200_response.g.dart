// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_bridges_tunnels_sites_map200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetBridgesTunnelsSitesMap200Response
    extends MapLayersControllerGetBridgesTunnelsSitesMap200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$MapLayersControllerGetBridgesTunnelsSitesMap200Response(
          [void Function(
                  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseBuilder)?
              updates]) =>
      (new MapLayersControllerGetBridgesTunnelsSitesMap200ResponseBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetBridgesTunnelsSitesMap200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(success,
        r'MapLayersControllerGetBridgesTunnelsSitesMap200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(data,
        r'MapLayersControllerGetBridgesTunnelsSitesMap200Response', 'data');
  }

  @override
  MapLayersControllerGetBridgesTunnelsSitesMap200Response rebuild(
          void Function(
                  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseBuilder toBuilder() =>
      new MapLayersControllerGetBridgesTunnelsSitesMap200ResponseBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapLayersControllerGetBridgesTunnelsSitesMap200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetBridgesTunnelsSitesMap200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class MapLayersControllerGetBridgesTunnelsSitesMap200ResponseBuilder
    implements
        Builder<MapLayersControllerGetBridgesTunnelsSitesMap200Response,
            MapLayersControllerGetBridgesTunnelsSitesMap200ResponseBuilder>,
        SuccessResponseBuilder {
  _$MapLayersControllerGetBridgesTunnelsSitesMap200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseBuilder() {
    MapLayersControllerGetBridgesTunnelsSitesMap200Response._defaults(this);
  }

  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      covariant MapLayersControllerGetBridgesTunnelsSitesMap200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapLayersControllerGetBridgesTunnelsSitesMap200Response;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetBridgesTunnelsSitesMap200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetBridgesTunnelsSitesMap200Response build() => _build();

  _$MapLayersControllerGetBridgesTunnelsSitesMap200Response _build() {
    final _$result = _$v ??
        new _$MapLayersControllerGetBridgesTunnelsSitesMap200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success,
                r'MapLayersControllerGetBridgesTunnelsSitesMap200Response',
                'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data,
                r'MapLayersControllerGetBridgesTunnelsSitesMap200Response',
                'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
