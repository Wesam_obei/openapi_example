// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activity_log_controller_last_login200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ActivityLogControllerLastLogin200Response
    extends ActivityLogControllerLastLogin200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$ActivityLogControllerLastLogin200Response(
          [void Function(ActivityLogControllerLastLogin200ResponseBuilder)?
              updates]) =>
      (new ActivityLogControllerLastLogin200ResponseBuilder()..update(updates))
          ._build();

  _$ActivityLogControllerLastLogin200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'ActivityLogControllerLastLogin200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'ActivityLogControllerLastLogin200Response', 'data');
  }

  @override
  ActivityLogControllerLastLogin200Response rebuild(
          void Function(ActivityLogControllerLastLogin200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ActivityLogControllerLastLogin200ResponseBuilder toBuilder() =>
      new ActivityLogControllerLastLogin200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ActivityLogControllerLastLogin200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'ActivityLogControllerLastLogin200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class ActivityLogControllerLastLogin200ResponseBuilder
    implements
        Builder<ActivityLogControllerLastLogin200Response,
            ActivityLogControllerLastLogin200ResponseBuilder>,
        SuccessResponseBuilder {
  _$ActivityLogControllerLastLogin200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  ActivityLogControllerLastLogin200ResponseBuilder() {
    ActivityLogControllerLastLogin200Response._defaults(this);
  }

  ActivityLogControllerLastLogin200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant ActivityLogControllerLastLogin200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ActivityLogControllerLastLogin200Response;
  }

  @override
  void update(
      void Function(ActivityLogControllerLastLogin200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  ActivityLogControllerLastLogin200Response build() => _build();

  _$ActivityLogControllerLastLogin200Response _build() {
    final _$result = _$v ??
        new _$ActivityLogControllerLastLogin200Response._(
            success: BuiltValueNullFieldError.checkNotNull(success,
                r'ActivityLogControllerLastLogin200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'ActivityLogControllerLastLogin200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
