// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_survey_response_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetSurveyResponseDataResponse extends GetSurveyResponseDataResponse {
  @override
  final num id;
  @override
  final DateTime createdAt;
  @override
  final num recipientId;
  @override
  final GetUserDataResponse answeredBy;
  @override
  final StatusResponse responseStatus;
  @override
  final BuiltList<SurveyResponseAnswerDataResponse> surveyResponseAnswer;
  @override
  final GetSurveyDataResponse survey;
  @override
  final GetSurveyRecipientViewDataResponse recipient;
  @override
  final bool? canAppliedReview;

  factory _$GetSurveyResponseDataResponse(
          [void Function(GetSurveyResponseDataResponseBuilder)? updates]) =>
      (new GetSurveyResponseDataResponseBuilder()..update(updates))._build();

  _$GetSurveyResponseDataResponse._(
      {required this.id,
      required this.createdAt,
      required this.recipientId,
      required this.answeredBy,
      required this.responseStatus,
      required this.surveyResponseAnswer,
      required this.survey,
      required this.recipient,
      this.canAppliedReview})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'GetSurveyResponseDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'GetSurveyResponseDataResponse', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        recipientId, r'GetSurveyResponseDataResponse', 'recipientId');
    BuiltValueNullFieldError.checkNotNull(
        answeredBy, r'GetSurveyResponseDataResponse', 'answeredBy');
    BuiltValueNullFieldError.checkNotNull(
        responseStatus, r'GetSurveyResponseDataResponse', 'responseStatus');
    BuiltValueNullFieldError.checkNotNull(surveyResponseAnswer,
        r'GetSurveyResponseDataResponse', 'surveyResponseAnswer');
    BuiltValueNullFieldError.checkNotNull(
        survey, r'GetSurveyResponseDataResponse', 'survey');
    BuiltValueNullFieldError.checkNotNull(
        recipient, r'GetSurveyResponseDataResponse', 'recipient');
  }

  @override
  GetSurveyResponseDataResponse rebuild(
          void Function(GetSurveyResponseDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetSurveyResponseDataResponseBuilder toBuilder() =>
      new GetSurveyResponseDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetSurveyResponseDataResponse &&
        id == other.id &&
        createdAt == other.createdAt &&
        recipientId == other.recipientId &&
        answeredBy == other.answeredBy &&
        responseStatus == other.responseStatus &&
        surveyResponseAnswer == other.surveyResponseAnswer &&
        survey == other.survey &&
        recipient == other.recipient &&
        canAppliedReview == other.canAppliedReview;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, recipientId.hashCode);
    _$hash = $jc(_$hash, answeredBy.hashCode);
    _$hash = $jc(_$hash, responseStatus.hashCode);
    _$hash = $jc(_$hash, surveyResponseAnswer.hashCode);
    _$hash = $jc(_$hash, survey.hashCode);
    _$hash = $jc(_$hash, recipient.hashCode);
    _$hash = $jc(_$hash, canAppliedReview.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetSurveyResponseDataResponse')
          ..add('id', id)
          ..add('createdAt', createdAt)
          ..add('recipientId', recipientId)
          ..add('answeredBy', answeredBy)
          ..add('responseStatus', responseStatus)
          ..add('surveyResponseAnswer', surveyResponseAnswer)
          ..add('survey', survey)
          ..add('recipient', recipient)
          ..add('canAppliedReview', canAppliedReview))
        .toString();
  }
}

class GetSurveyResponseDataResponseBuilder
    implements
        Builder<GetSurveyResponseDataResponse,
            GetSurveyResponseDataResponseBuilder> {
  _$GetSurveyResponseDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  num? _recipientId;
  num? get recipientId => _$this._recipientId;
  set recipientId(num? recipientId) => _$this._recipientId = recipientId;

  GetUserDataResponseBuilder? _answeredBy;
  GetUserDataResponseBuilder get answeredBy =>
      _$this._answeredBy ??= new GetUserDataResponseBuilder();
  set answeredBy(GetUserDataResponseBuilder? answeredBy) =>
      _$this._answeredBy = answeredBy;

  StatusResponseBuilder? _responseStatus;
  StatusResponseBuilder get responseStatus =>
      _$this._responseStatus ??= new StatusResponseBuilder();
  set responseStatus(StatusResponseBuilder? responseStatus) =>
      _$this._responseStatus = responseStatus;

  ListBuilder<SurveyResponseAnswerDataResponse>? _surveyResponseAnswer;
  ListBuilder<SurveyResponseAnswerDataResponse> get surveyResponseAnswer =>
      _$this._surveyResponseAnswer ??=
          new ListBuilder<SurveyResponseAnswerDataResponse>();
  set surveyResponseAnswer(
          ListBuilder<SurveyResponseAnswerDataResponse>?
              surveyResponseAnswer) =>
      _$this._surveyResponseAnswer = surveyResponseAnswer;

  GetSurveyDataResponseBuilder? _survey;
  GetSurveyDataResponseBuilder get survey =>
      _$this._survey ??= new GetSurveyDataResponseBuilder();
  set survey(GetSurveyDataResponseBuilder? survey) => _$this._survey = survey;

  GetSurveyRecipientViewDataResponseBuilder? _recipient;
  GetSurveyRecipientViewDataResponseBuilder get recipient =>
      _$this._recipient ??= new GetSurveyRecipientViewDataResponseBuilder();
  set recipient(GetSurveyRecipientViewDataResponseBuilder? recipient) =>
      _$this._recipient = recipient;

  bool? _canAppliedReview;
  bool? get canAppliedReview => _$this._canAppliedReview;
  set canAppliedReview(bool? canAppliedReview) =>
      _$this._canAppliedReview = canAppliedReview;

  GetSurveyResponseDataResponseBuilder() {
    GetSurveyResponseDataResponse._defaults(this);
  }

  GetSurveyResponseDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _createdAt = $v.createdAt;
      _recipientId = $v.recipientId;
      _answeredBy = $v.answeredBy.toBuilder();
      _responseStatus = $v.responseStatus.toBuilder();
      _surveyResponseAnswer = $v.surveyResponseAnswer.toBuilder();
      _survey = $v.survey.toBuilder();
      _recipient = $v.recipient.toBuilder();
      _canAppliedReview = $v.canAppliedReview;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetSurveyResponseDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetSurveyResponseDataResponse;
  }

  @override
  void update(void Function(GetSurveyResponseDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetSurveyResponseDataResponse build() => _build();

  _$GetSurveyResponseDataResponse _build() {
    _$GetSurveyResponseDataResponse _$result;
    try {
      _$result = _$v ??
          new _$GetSurveyResponseDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'GetSurveyResponseDataResponse', 'id'),
              createdAt: BuiltValueNullFieldError.checkNotNull(
                  createdAt, r'GetSurveyResponseDataResponse', 'createdAt'),
              recipientId: BuiltValueNullFieldError.checkNotNull(
                  recipientId, r'GetSurveyResponseDataResponse', 'recipientId'),
              answeredBy: answeredBy.build(),
              responseStatus: responseStatus.build(),
              surveyResponseAnswer: surveyResponseAnswer.build(),
              survey: survey.build(),
              recipient: recipient.build(),
              canAppliedReview: canAppliedReview);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'answeredBy';
        answeredBy.build();
        _$failedField = 'responseStatus';
        responseStatus.build();
        _$failedField = 'surveyResponseAnswer';
        surveyResponseAnswer.build();
        _$failedField = 'survey';
        survey.build();
        _$failedField = 'recipient';
        recipient.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'GetSurveyResponseDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
