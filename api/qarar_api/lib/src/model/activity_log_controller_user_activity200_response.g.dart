// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activity_log_controller_user_activity200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ActivityLogControllerUserActivity200Response
    extends ActivityLogControllerUserActivity200Response {
  @override
  final bool success;
  @override
  final BuiltList<JsonObject> data;
  @override
  final PaginatedMetaDocumented meta;
  @override
  final PaginatedLinksDocumented links;

  factory _$ActivityLogControllerUserActivity200Response(
          [void Function(ActivityLogControllerUserActivity200ResponseBuilder)?
              updates]) =>
      (new ActivityLogControllerUserActivity200ResponseBuilder()
            ..update(updates))
          ._build();

  _$ActivityLogControllerUserActivity200Response._(
      {required this.success,
      required this.data,
      required this.meta,
      required this.links})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'ActivityLogControllerUserActivity200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'ActivityLogControllerUserActivity200Response', 'data');
    BuiltValueNullFieldError.checkNotNull(
        meta, r'ActivityLogControllerUserActivity200Response', 'meta');
    BuiltValueNullFieldError.checkNotNull(
        links, r'ActivityLogControllerUserActivity200Response', 'links');
  }

  @override
  ActivityLogControllerUserActivity200Response rebuild(
          void Function(ActivityLogControllerUserActivity200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ActivityLogControllerUserActivity200ResponseBuilder toBuilder() =>
      new ActivityLogControllerUserActivity200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ActivityLogControllerUserActivity200Response &&
        success == other.success &&
        data == other.data &&
        meta == other.meta &&
        links == other.links;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jc(_$hash, meta.hashCode);
    _$hash = $jc(_$hash, links.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'ActivityLogControllerUserActivity200Response')
          ..add('success', success)
          ..add('data', data)
          ..add('meta', meta)
          ..add('links', links))
        .toString();
  }
}

class ActivityLogControllerUserActivity200ResponseBuilder
    implements
        Builder<ActivityLogControllerUserActivity200Response,
            ActivityLogControllerUserActivity200ResponseBuilder>,
        PaginatedDocumentedBuilder {
  _$ActivityLogControllerUserActivity200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  ListBuilder<JsonObject>? _data;
  ListBuilder<JsonObject> get data =>
      _$this._data ??= new ListBuilder<JsonObject>();
  set data(covariant ListBuilder<JsonObject>? data) => _$this._data = data;

  PaginatedMetaDocumentedBuilder? _meta;
  PaginatedMetaDocumentedBuilder get meta =>
      _$this._meta ??= new PaginatedMetaDocumentedBuilder();
  set meta(covariant PaginatedMetaDocumentedBuilder? meta) =>
      _$this._meta = meta;

  PaginatedLinksDocumentedBuilder? _links;
  PaginatedLinksDocumentedBuilder get links =>
      _$this._links ??= new PaginatedLinksDocumentedBuilder();
  set links(covariant PaginatedLinksDocumentedBuilder? links) =>
      _$this._links = links;

  ActivityLogControllerUserActivity200ResponseBuilder() {
    ActivityLogControllerUserActivity200Response._defaults(this);
  }

  ActivityLogControllerUserActivity200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data.toBuilder();
      _meta = $v.meta.toBuilder();
      _links = $v.links.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant ActivityLogControllerUserActivity200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ActivityLogControllerUserActivity200Response;
  }

  @override
  void update(
      void Function(ActivityLogControllerUserActivity200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  ActivityLogControllerUserActivity200Response build() => _build();

  _$ActivityLogControllerUserActivity200Response _build() {
    _$ActivityLogControllerUserActivity200Response _$result;
    try {
      _$result = _$v ??
          new _$ActivityLogControllerUserActivity200Response._(
              success: BuiltValueNullFieldError.checkNotNull(success,
                  r'ActivityLogControllerUserActivity200Response', 'success'),
              data: data.build(),
              meta: meta.build(),
              links: links.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'data';
        data.build();
        _$failedField = 'meta';
        meta.build();
        _$failedField = 'links';
        links.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ActivityLogControllerUserActivity200Response',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
