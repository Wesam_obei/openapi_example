// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_user_dashboard_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ListUserDashboardDataResponse extends ListUserDashboardDataResponse {
  @override
  final String id;
  @override
  final String name;
  @override
  final bool isDefault;
  @override
  final BuiltList<WidgetConfig>? settings;

  factory _$ListUserDashboardDataResponse(
          [void Function(ListUserDashboardDataResponseBuilder)? updates]) =>
      (new ListUserDashboardDataResponseBuilder()..update(updates))._build();

  _$ListUserDashboardDataResponse._(
      {required this.id,
      required this.name,
      required this.isDefault,
      this.settings})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'ListUserDashboardDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, r'ListUserDashboardDataResponse', 'name');
    BuiltValueNullFieldError.checkNotNull(
        isDefault, r'ListUserDashboardDataResponse', 'isDefault');
  }

  @override
  ListUserDashboardDataResponse rebuild(
          void Function(ListUserDashboardDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ListUserDashboardDataResponseBuilder toBuilder() =>
      new ListUserDashboardDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ListUserDashboardDataResponse &&
        id == other.id &&
        name == other.name &&
        isDefault == other.isDefault &&
        settings == other.settings;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, isDefault.hashCode);
    _$hash = $jc(_$hash, settings.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ListUserDashboardDataResponse')
          ..add('id', id)
          ..add('name', name)
          ..add('isDefault', isDefault)
          ..add('settings', settings))
        .toString();
  }
}

class ListUserDashboardDataResponseBuilder
    implements
        Builder<ListUserDashboardDataResponse,
            ListUserDashboardDataResponseBuilder> {
  _$ListUserDashboardDataResponse? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  bool? _isDefault;
  bool? get isDefault => _$this._isDefault;
  set isDefault(bool? isDefault) => _$this._isDefault = isDefault;

  ListBuilder<WidgetConfig>? _settings;
  ListBuilder<WidgetConfig> get settings =>
      _$this._settings ??= new ListBuilder<WidgetConfig>();
  set settings(ListBuilder<WidgetConfig>? settings) =>
      _$this._settings = settings;

  ListUserDashboardDataResponseBuilder() {
    ListUserDashboardDataResponse._defaults(this);
  }

  ListUserDashboardDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _isDefault = $v.isDefault;
      _settings = $v.settings?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ListUserDashboardDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ListUserDashboardDataResponse;
  }

  @override
  void update(void Function(ListUserDashboardDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ListUserDashboardDataResponse build() => _build();

  _$ListUserDashboardDataResponse _build() {
    _$ListUserDashboardDataResponse _$result;
    try {
      _$result = _$v ??
          new _$ListUserDashboardDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'ListUserDashboardDataResponse', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'ListUserDashboardDataResponse', 'name'),
              isDefault: BuiltValueNullFieldError.checkNotNull(
                  isDefault, r'ListUserDashboardDataResponse', 'isDefault'),
              settings: _settings?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'settings';
        _settings?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ListUserDashboardDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
