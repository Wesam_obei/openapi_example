// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'role_update_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$RoleUpdateRequest extends RoleUpdateRequest {
  @override
  final String name;
  @override
  final String description;
  @override
  final BuiltList<String> permissions;

  factory _$RoleUpdateRequest(
          [void Function(RoleUpdateRequestBuilder)? updates]) =>
      (new RoleUpdateRequestBuilder()..update(updates))._build();

  _$RoleUpdateRequest._(
      {required this.name,
      required this.description,
      required this.permissions})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(name, r'RoleUpdateRequest', 'name');
    BuiltValueNullFieldError.checkNotNull(
        description, r'RoleUpdateRequest', 'description');
    BuiltValueNullFieldError.checkNotNull(
        permissions, r'RoleUpdateRequest', 'permissions');
  }

  @override
  RoleUpdateRequest rebuild(void Function(RoleUpdateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RoleUpdateRequestBuilder toBuilder() =>
      new RoleUpdateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RoleUpdateRequest &&
        name == other.name &&
        description == other.description &&
        permissions == other.permissions;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, permissions.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'RoleUpdateRequest')
          ..add('name', name)
          ..add('description', description)
          ..add('permissions', permissions))
        .toString();
  }
}

class RoleUpdateRequestBuilder
    implements Builder<RoleUpdateRequest, RoleUpdateRequestBuilder> {
  _$RoleUpdateRequest? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  ListBuilder<String>? _permissions;
  ListBuilder<String> get permissions =>
      _$this._permissions ??= new ListBuilder<String>();
  set permissions(ListBuilder<String>? permissions) =>
      _$this._permissions = permissions;

  RoleUpdateRequestBuilder() {
    RoleUpdateRequest._defaults(this);
  }

  RoleUpdateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _description = $v.description;
      _permissions = $v.permissions.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RoleUpdateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$RoleUpdateRequest;
  }

  @override
  void update(void Function(RoleUpdateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  RoleUpdateRequest build() => _build();

  _$RoleUpdateRequest _build() {
    _$RoleUpdateRequest _$result;
    try {
      _$result = _$v ??
          new _$RoleUpdateRequest._(
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'RoleUpdateRequest', 'name'),
              description: BuiltValueNullFieldError.checkNotNull(
                  description, r'RoleUpdateRequest', 'description'),
              permissions: permissions.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'permissions';
        permissions.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'RoleUpdateRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
