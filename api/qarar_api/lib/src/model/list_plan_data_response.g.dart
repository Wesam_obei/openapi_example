// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_plan_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ListPlanDataResponse extends ListPlanDataResponse {
  @override
  final num id;
  @override
  final String name;
  @override
  final String description;
  @override
  final BuiltList<GetPlanVersionDataResponse> versions;

  factory _$ListPlanDataResponse(
          [void Function(ListPlanDataResponseBuilder)? updates]) =>
      (new ListPlanDataResponseBuilder()..update(updates))._build();

  _$ListPlanDataResponse._(
      {required this.id,
      required this.name,
      required this.description,
      required this.versions})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, r'ListPlanDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, r'ListPlanDataResponse', 'name');
    BuiltValueNullFieldError.checkNotNull(
        description, r'ListPlanDataResponse', 'description');
    BuiltValueNullFieldError.checkNotNull(
        versions, r'ListPlanDataResponse', 'versions');
  }

  @override
  ListPlanDataResponse rebuild(
          void Function(ListPlanDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ListPlanDataResponseBuilder toBuilder() =>
      new ListPlanDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ListPlanDataResponse &&
        id == other.id &&
        name == other.name &&
        description == other.description &&
        versions == other.versions;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, versions.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ListPlanDataResponse')
          ..add('id', id)
          ..add('name', name)
          ..add('description', description)
          ..add('versions', versions))
        .toString();
  }
}

class ListPlanDataResponseBuilder
    implements Builder<ListPlanDataResponse, ListPlanDataResponseBuilder> {
  _$ListPlanDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  ListBuilder<GetPlanVersionDataResponse>? _versions;
  ListBuilder<GetPlanVersionDataResponse> get versions =>
      _$this._versions ??= new ListBuilder<GetPlanVersionDataResponse>();
  set versions(ListBuilder<GetPlanVersionDataResponse>? versions) =>
      _$this._versions = versions;

  ListPlanDataResponseBuilder() {
    ListPlanDataResponse._defaults(this);
  }

  ListPlanDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _description = $v.description;
      _versions = $v.versions.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ListPlanDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ListPlanDataResponse;
  }

  @override
  void update(void Function(ListPlanDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ListPlanDataResponse build() => _build();

  _$ListPlanDataResponse _build() {
    _$ListPlanDataResponse _$result;
    try {
      _$result = _$v ??
          new _$ListPlanDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'ListPlanDataResponse', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'ListPlanDataResponse', 'name'),
              description: BuiltValueNullFieldError.checkNotNull(
                  description, r'ListPlanDataResponse', 'description'),
              versions: versions.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'versions';
        versions.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ListPlanDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
