//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/map_layers_controller_get_incident_reports_map200_response_all_of_data_features_inner_properties_id.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_urban_area_boundaries_map200_response_all_of_data_features_inner_properties.g.dart';

/// MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
///
/// Properties:
/// * [id]
/// * [provinceName]
/// * [urbanBoundaryType]
/// * [municipalityName]
/// * [remarks]
@BuiltValue()
abstract class MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
    implements
        Built<
            MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties,
            MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder> {
  @BuiltValueField(wireName: r'id')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get id;

  @BuiltValueField(wireName: r'provinceName')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get provinceName;

  @BuiltValueField(wireName: r'urbanBoundaryType')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get urbanBoundaryType;

  @BuiltValueField(wireName: r'municipalityName')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get municipalityName;

  @BuiltValueField(wireName: r'remarks')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get remarks;

  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties._();

  factory MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties(
          [void updates(
              MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
                  b)]) =
      _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
              b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties>
      get serializer =>
          _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesSerializer();
}

class _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties,
    _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
        object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.id != null) {
      yield r'id';
      yield serializers.serialize(
        object.id,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.provinceName != null) {
      yield r'provinceName';
      yield serializers.serialize(
        object.provinceName,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.urbanBoundaryType != null) {
      yield r'urbanBoundaryType';
      yield serializers.serialize(
        object.urbanBoundaryType,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.municipalityName != null) {
      yield r'municipalityName';
      yield serializers.serialize(
        object.municipalityName,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.remarks != null) {
      yield r'remarks';
      yield serializers.serialize(
        object.remarks,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
        object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.id.replace(valueDes);
          break;
        case r'provinceName':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.provinceName.replace(valueDes);
          break;
        case r'urbanBoundaryType':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.urbanBoundaryType.replace(valueDes);
          break;
        case r'municipalityName':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.municipalityName.replace(valueDes);
          break;
        case r'remarks':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.remarks.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
      deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
