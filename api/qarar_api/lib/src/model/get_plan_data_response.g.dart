// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_plan_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetPlanDataResponse extends GetPlanDataResponse {
  @override
  final num id;
  @override
  final String name;
  @override
  final String description;
  @override
  final BuiltList<GetPlanVersionDataResponse> versions;

  factory _$GetPlanDataResponse(
          [void Function(GetPlanDataResponseBuilder)? updates]) =>
      (new GetPlanDataResponseBuilder()..update(updates))._build();

  _$GetPlanDataResponse._(
      {required this.id,
      required this.name,
      required this.description,
      required this.versions})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, r'GetPlanDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(name, r'GetPlanDataResponse', 'name');
    BuiltValueNullFieldError.checkNotNull(
        description, r'GetPlanDataResponse', 'description');
    BuiltValueNullFieldError.checkNotNull(
        versions, r'GetPlanDataResponse', 'versions');
  }

  @override
  GetPlanDataResponse rebuild(
          void Function(GetPlanDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetPlanDataResponseBuilder toBuilder() =>
      new GetPlanDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetPlanDataResponse &&
        id == other.id &&
        name == other.name &&
        description == other.description &&
        versions == other.versions;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, versions.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetPlanDataResponse')
          ..add('id', id)
          ..add('name', name)
          ..add('description', description)
          ..add('versions', versions))
        .toString();
  }
}

class GetPlanDataResponseBuilder
    implements Builder<GetPlanDataResponse, GetPlanDataResponseBuilder> {
  _$GetPlanDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  ListBuilder<GetPlanVersionDataResponse>? _versions;
  ListBuilder<GetPlanVersionDataResponse> get versions =>
      _$this._versions ??= new ListBuilder<GetPlanVersionDataResponse>();
  set versions(ListBuilder<GetPlanVersionDataResponse>? versions) =>
      _$this._versions = versions;

  GetPlanDataResponseBuilder() {
    GetPlanDataResponse._defaults(this);
  }

  GetPlanDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _description = $v.description;
      _versions = $v.versions.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetPlanDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetPlanDataResponse;
  }

  @override
  void update(void Function(GetPlanDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetPlanDataResponse build() => _build();

  _$GetPlanDataResponse _build() {
    _$GetPlanDataResponse _$result;
    try {
      _$result = _$v ??
          new _$GetPlanDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'GetPlanDataResponse', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'GetPlanDataResponse', 'name'),
              description: BuiltValueNullFieldError.checkNotNull(
                  description, r'GetPlanDataResponse', 'description'),
              versions: versions.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'versions';
        versions.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'GetPlanDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
