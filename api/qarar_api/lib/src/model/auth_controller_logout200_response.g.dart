// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_controller_logout200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$AuthControllerLogout200Response
    extends AuthControllerLogout200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$AuthControllerLogout200Response(
          [void Function(AuthControllerLogout200ResponseBuilder)? updates]) =>
      (new AuthControllerLogout200ResponseBuilder()..update(updates))._build();

  _$AuthControllerLogout200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'AuthControllerLogout200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'AuthControllerLogout200Response', 'data');
  }

  @override
  AuthControllerLogout200Response rebuild(
          void Function(AuthControllerLogout200ResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AuthControllerLogout200ResponseBuilder toBuilder() =>
      new AuthControllerLogout200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AuthControllerLogout200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'AuthControllerLogout200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class AuthControllerLogout200ResponseBuilder
    implements
        Builder<AuthControllerLogout200Response,
            AuthControllerLogout200ResponseBuilder>,
        SuccessResponseBuilder {
  _$AuthControllerLogout200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  AuthControllerLogout200ResponseBuilder() {
    AuthControllerLogout200Response._defaults(this);
  }

  AuthControllerLogout200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant AuthControllerLogout200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$AuthControllerLogout200Response;
  }

  @override
  void update(void Function(AuthControllerLogout200ResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  AuthControllerLogout200Response build() => _build();

  _$AuthControllerLogout200Response _build() {
    final _$result = _$v ??
        new _$AuthControllerLogout200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success, r'AuthControllerLogout200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'AuthControllerLogout200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
