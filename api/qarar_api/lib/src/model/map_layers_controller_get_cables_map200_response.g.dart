// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_cables_map200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetCablesMap200Response
    extends MapLayersControllerGetCablesMap200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$MapLayersControllerGetCablesMap200Response(
          [void Function(MapLayersControllerGetCablesMap200ResponseBuilder)?
              updates]) =>
      (new MapLayersControllerGetCablesMap200ResponseBuilder()..update(updates))
          ._build();

  _$MapLayersControllerGetCablesMap200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'MapLayersControllerGetCablesMap200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'MapLayersControllerGetCablesMap200Response', 'data');
  }

  @override
  MapLayersControllerGetCablesMap200Response rebuild(
          void Function(MapLayersControllerGetCablesMap200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetCablesMap200ResponseBuilder toBuilder() =>
      new MapLayersControllerGetCablesMap200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapLayersControllerGetCablesMap200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetCablesMap200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class MapLayersControllerGetCablesMap200ResponseBuilder
    implements
        Builder<MapLayersControllerGetCablesMap200Response,
            MapLayersControllerGetCablesMap200ResponseBuilder>,
        SuccessResponseBuilder {
  _$MapLayersControllerGetCablesMap200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  MapLayersControllerGetCablesMap200ResponseBuilder() {
    MapLayersControllerGetCablesMap200Response._defaults(this);
  }

  MapLayersControllerGetCablesMap200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant MapLayersControllerGetCablesMap200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapLayersControllerGetCablesMap200Response;
  }

  @override
  void update(
      void Function(MapLayersControllerGetCablesMap200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetCablesMap200Response build() => _build();

  _$MapLayersControllerGetCablesMap200Response _build() {
    final _$result = _$v ??
        new _$MapLayersControllerGetCablesMap200Response._(
            success: BuiltValueNullFieldError.checkNotNull(success,
                r'MapLayersControllerGetCablesMap200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'MapLayersControllerGetCablesMap200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
