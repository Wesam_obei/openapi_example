//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'list_employee_user_data_response.g.dart';

/// ListEmployeeUserDataResponse
///
/// Properties:
/// * [id]
/// * [email]
/// * [fullName]
/// * [jobTitle]
/// * [employeeId]
@BuiltValue()
abstract class ListEmployeeUserDataResponse
    implements
        Built<ListEmployeeUserDataResponse,
            ListEmployeeUserDataResponseBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'email')
  String get email;

  @BuiltValueField(wireName: r'full_name')
  String get fullName;

  @BuiltValueField(wireName: r'job_title')
  String get jobTitle;

  @BuiltValueField(wireName: r'employee_id')
  num get employeeId;

  ListEmployeeUserDataResponse._();

  factory ListEmployeeUserDataResponse(
          [void updates(ListEmployeeUserDataResponseBuilder b)]) =
      _$ListEmployeeUserDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ListEmployeeUserDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ListEmployeeUserDataResponse> get serializer =>
      _$ListEmployeeUserDataResponseSerializer();
}

class _$ListEmployeeUserDataResponseSerializer
    implements PrimitiveSerializer<ListEmployeeUserDataResponse> {
  @override
  final Iterable<Type> types = const [
    ListEmployeeUserDataResponse,
    _$ListEmployeeUserDataResponse
  ];

  @override
  final String wireName = r'ListEmployeeUserDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ListEmployeeUserDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'email';
    yield serializers.serialize(
      object.email,
      specifiedType: const FullType(String),
    );
    yield r'full_name';
    yield serializers.serialize(
      object.fullName,
      specifiedType: const FullType(String),
    );
    yield r'job_title';
    yield serializers.serialize(
      object.jobTitle,
      specifiedType: const FullType(String),
    );
    yield r'employee_id';
    yield serializers.serialize(
      object.employeeId,
      specifiedType: const FullType(num),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ListEmployeeUserDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ListEmployeeUserDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'email':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.email = valueDes;
          break;
        case r'full_name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.fullName = valueDes;
          break;
        case r'job_title':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.jobTitle = valueDes;
          break;
        case r'employee_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.employeeId = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ListEmployeeUserDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ListEmployeeUserDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
