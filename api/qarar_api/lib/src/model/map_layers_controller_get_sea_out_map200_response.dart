//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/success_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_sea_out_map200_response_all_of_data.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_sea_out_map200_response.g.dart';

/// My custom description
///
/// Properties:
/// * [success]
/// * [data]
@BuiltValue()
abstract class MapLayersControllerGetSeaOutMap200Response
    implements
        SuccessResponse,
        Built<MapLayersControllerGetSeaOutMap200Response,
            MapLayersControllerGetSeaOutMap200ResponseBuilder> {
  MapLayersControllerGetSeaOutMap200Response._();

  factory MapLayersControllerGetSeaOutMap200Response(
          [void updates(MapLayersControllerGetSeaOutMap200ResponseBuilder b)]) =
      _$MapLayersControllerGetSeaOutMap200Response;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(MapLayersControllerGetSeaOutMap200ResponseBuilder b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<MapLayersControllerGetSeaOutMap200Response>
      get serializer =>
          _$MapLayersControllerGetSeaOutMap200ResponseSerializer();
}

class _$MapLayersControllerGetSeaOutMap200ResponseSerializer
    implements PrimitiveSerializer<MapLayersControllerGetSeaOutMap200Response> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetSeaOutMap200Response,
    _$MapLayersControllerGetSeaOutMap200Response
  ];

  @override
  final String wireName = r'MapLayersControllerGetSeaOutMap200Response';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetSeaOutMap200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'data';
    yield serializers.serialize(
      object.data,
      specifiedType: const FullType(JsonObject),
    );
    yield r'success';
    yield serializers.serialize(
      object.success,
      specifiedType: const FullType(bool),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetSeaOutMap200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetSeaOutMap200ResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'data':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.data = valueDes;
          break;
        case r'success':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.success = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetSeaOutMap200Response deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = MapLayersControllerGetSeaOutMap200ResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
