// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_urban_area_boundaries_map200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetUrbanAreaBoundariesMap200Response
    extends MapLayersControllerGetUrbanAreaBoundariesMap200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$MapLayersControllerGetUrbanAreaBoundariesMap200Response(
          [void Function(
                  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseBuilder)?
              updates]) =>
      (new MapLayersControllerGetUrbanAreaBoundariesMap200ResponseBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetUrbanAreaBoundariesMap200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(success,
        r'MapLayersControllerGetUrbanAreaBoundariesMap200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(data,
        r'MapLayersControllerGetUrbanAreaBoundariesMap200Response', 'data');
  }

  @override
  MapLayersControllerGetUrbanAreaBoundariesMap200Response rebuild(
          void Function(
                  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseBuilder toBuilder() =>
      new MapLayersControllerGetUrbanAreaBoundariesMap200ResponseBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapLayersControllerGetUrbanAreaBoundariesMap200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetUrbanAreaBoundariesMap200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class MapLayersControllerGetUrbanAreaBoundariesMap200ResponseBuilder
    implements
        Builder<MapLayersControllerGetUrbanAreaBoundariesMap200Response,
            MapLayersControllerGetUrbanAreaBoundariesMap200ResponseBuilder>,
        SuccessResponseBuilder {
  _$MapLayersControllerGetUrbanAreaBoundariesMap200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseBuilder() {
    MapLayersControllerGetUrbanAreaBoundariesMap200Response._defaults(this);
  }

  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      covariant MapLayersControllerGetUrbanAreaBoundariesMap200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapLayersControllerGetUrbanAreaBoundariesMap200Response;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetUrbanAreaBoundariesMap200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetUrbanAreaBoundariesMap200Response build() => _build();

  _$MapLayersControllerGetUrbanAreaBoundariesMap200Response _build() {
    final _$result = _$v ??
        new _$MapLayersControllerGetUrbanAreaBoundariesMap200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success,
                r'MapLayersControllerGetUrbanAreaBoundariesMap200Response',
                'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data,
                r'MapLayersControllerGetUrbanAreaBoundariesMap200Response',
                'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
