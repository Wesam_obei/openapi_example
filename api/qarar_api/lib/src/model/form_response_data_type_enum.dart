//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'form_response_data_type_enum.g.dart';

class FormResponseDataTypeEnum extends EnumClass {
  @BuiltValueEnumConst(wireName: r'null')
  static const FormResponseDataTypeEnum null_ = _$null_;
  @BuiltValueEnumConst(wireName: r'string')
  static const FormResponseDataTypeEnum string = _$string;
  @BuiltValueEnumConst(wireName: r'integer')
  static const FormResponseDataTypeEnum integer = _$integer;
  @BuiltValueEnumConst(wireName: r'number')
  static const FormResponseDataTypeEnum number = _$number;
  @BuiltValueEnumConst(wireName: r'date')
  static const FormResponseDataTypeEnum date = _$date;
  @BuiltValueEnumConst(wireName: r'datetime')
  static const FormResponseDataTypeEnum datetime = _$datetime;
  @BuiltValueEnumConst(wireName: r'time')
  static const FormResponseDataTypeEnum time = _$time;
  @BuiltValueEnumConst(wireName: r'file')
  static const FormResponseDataTypeEnum file = _$file;
  @BuiltValueEnumConst(wireName: r'range-date')
  static const FormResponseDataTypeEnum rangeDate = _$rangeDate;
  @BuiltValueEnumConst(wireName: r'range-datetime')
  static const FormResponseDataTypeEnum rangeDatetime = _$rangeDatetime;
  @BuiltValueEnumConst(wireName: r'range-time')
  static const FormResponseDataTypeEnum rangeTime = _$rangeTime;
  @BuiltValueEnumConst(wireName: r'coordinate', fallback: true)
  static const FormResponseDataTypeEnum coordinate = _$coordinate;

  static Serializer<FormResponseDataTypeEnum> get serializer =>
      _$formResponseDataTypeEnumSerializer;

  const FormResponseDataTypeEnum._(String name) : super(name);

  static BuiltSet<FormResponseDataTypeEnum> get values => _$values;
  static FormResponseDataTypeEnum valueOf(String name) => _$valueOf(name);
}

/// Optionally, enum_class can generate a mixin to go with your enum for use
/// with Angular. It exposes your enum constants as getters. So, if you mix it
/// in to your Dart component class, the values become available to the
/// corresponding Angular template.
///
/// Trigger mixin generation by writing a line like this one next to your enum.
abstract class FormResponseDataTypeEnumMixin = Object
    with _$FormResponseDataTypeEnumMixin;
