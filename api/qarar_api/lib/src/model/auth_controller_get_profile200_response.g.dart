// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_controller_get_profile200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$AuthControllerGetProfile200Response
    extends AuthControllerGetProfile200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$AuthControllerGetProfile200Response(
          [void Function(AuthControllerGetProfile200ResponseBuilder)?
              updates]) =>
      (new AuthControllerGetProfile200ResponseBuilder()..update(updates))
          ._build();

  _$AuthControllerGetProfile200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'AuthControllerGetProfile200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'AuthControllerGetProfile200Response', 'data');
  }

  @override
  AuthControllerGetProfile200Response rebuild(
          void Function(AuthControllerGetProfile200ResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AuthControllerGetProfile200ResponseBuilder toBuilder() =>
      new AuthControllerGetProfile200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AuthControllerGetProfile200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'AuthControllerGetProfile200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class AuthControllerGetProfile200ResponseBuilder
    implements
        Builder<AuthControllerGetProfile200Response,
            AuthControllerGetProfile200ResponseBuilder>,
        SuccessResponseBuilder {
  _$AuthControllerGetProfile200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  AuthControllerGetProfile200ResponseBuilder() {
    AuthControllerGetProfile200Response._defaults(this);
  }

  AuthControllerGetProfile200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant AuthControllerGetProfile200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$AuthControllerGetProfile200Response;
  }

  @override
  void update(
      void Function(AuthControllerGetProfile200ResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  AuthControllerGetProfile200Response build() => _build();

  _$AuthControllerGetProfile200Response _build() {
    final _$result = _$v ??
        new _$AuthControllerGetProfile200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success, r'AuthControllerGetProfile200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'AuthControllerGetProfile200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
