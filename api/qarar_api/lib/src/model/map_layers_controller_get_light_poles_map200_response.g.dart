// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_light_poles_map200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetLightPolesMap200Response
    extends MapLayersControllerGetLightPolesMap200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$MapLayersControllerGetLightPolesMap200Response(
          [void Function(MapLayersControllerGetLightPolesMap200ResponseBuilder)?
              updates]) =>
      (new MapLayersControllerGetLightPolesMap200ResponseBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetLightPolesMap200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'MapLayersControllerGetLightPolesMap200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'MapLayersControllerGetLightPolesMap200Response', 'data');
  }

  @override
  MapLayersControllerGetLightPolesMap200Response rebuild(
          void Function(MapLayersControllerGetLightPolesMap200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetLightPolesMap200ResponseBuilder toBuilder() =>
      new MapLayersControllerGetLightPolesMap200ResponseBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapLayersControllerGetLightPolesMap200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetLightPolesMap200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class MapLayersControllerGetLightPolesMap200ResponseBuilder
    implements
        Builder<MapLayersControllerGetLightPolesMap200Response,
            MapLayersControllerGetLightPolesMap200ResponseBuilder>,
        SuccessResponseBuilder {
  _$MapLayersControllerGetLightPolesMap200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  MapLayersControllerGetLightPolesMap200ResponseBuilder() {
    MapLayersControllerGetLightPolesMap200Response._defaults(this);
  }

  MapLayersControllerGetLightPolesMap200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant MapLayersControllerGetLightPolesMap200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapLayersControllerGetLightPolesMap200Response;
  }

  @override
  void update(
      void Function(MapLayersControllerGetLightPolesMap200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetLightPolesMap200Response build() => _build();

  _$MapLayersControllerGetLightPolesMap200Response _build() {
    final _$result = _$v ??
        new _$MapLayersControllerGetLightPolesMap200Response._(
            success: BuiltValueNullFieldError.checkNotNull(success,
                r'MapLayersControllerGetLightPolesMap200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(data,
                r'MapLayersControllerGetLightPolesMap200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
