//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/refresh_token_response.dart';
import 'package:qarar_api/src/model/success_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'auth_controller_refresh_token200_response.g.dart';

/// My custom description
///
/// Properties:
/// * [success]
/// * [data]
@BuiltValue()
abstract class AuthControllerRefreshToken200Response
    implements
        SuccessResponse,
        Built<AuthControllerRefreshToken200Response,
            AuthControllerRefreshToken200ResponseBuilder> {
  AuthControllerRefreshToken200Response._();

  factory AuthControllerRefreshToken200Response(
          [void updates(AuthControllerRefreshToken200ResponseBuilder b)]) =
      _$AuthControllerRefreshToken200Response;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(AuthControllerRefreshToken200ResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<AuthControllerRefreshToken200Response> get serializer =>
      _$AuthControllerRefreshToken200ResponseSerializer();
}

class _$AuthControllerRefreshToken200ResponseSerializer
    implements PrimitiveSerializer<AuthControllerRefreshToken200Response> {
  @override
  final Iterable<Type> types = const [
    AuthControllerRefreshToken200Response,
    _$AuthControllerRefreshToken200Response
  ];

  @override
  final String wireName = r'AuthControllerRefreshToken200Response';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    AuthControllerRefreshToken200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'data';
    yield serializers.serialize(
      object.data,
      specifiedType: const FullType(JsonObject),
    );
    yield r'success';
    yield serializers.serialize(
      object.success,
      specifiedType: const FullType(bool),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    AuthControllerRefreshToken200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required AuthControllerRefreshToken200ResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'data':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.data = valueDes;
          break;
        case r'success':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.success = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  AuthControllerRefreshToken200Response deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = AuthControllerRefreshToken200ResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
