// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'web_push_payload_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$WebPushPayloadData extends WebPushPayloadData {
  @override
  final Action action;

  factory _$WebPushPayloadData(
          [void Function(WebPushPayloadDataBuilder)? updates]) =>
      (new WebPushPayloadDataBuilder()..update(updates))._build();

  _$WebPushPayloadData._({required this.action}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        action, r'WebPushPayloadData', 'action');
  }

  @override
  WebPushPayloadData rebuild(
          void Function(WebPushPayloadDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  WebPushPayloadDataBuilder toBuilder() =>
      new WebPushPayloadDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is WebPushPayloadData && action == other.action;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, action.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'WebPushPayloadData')
          ..add('action', action))
        .toString();
  }
}

class WebPushPayloadDataBuilder
    implements Builder<WebPushPayloadData, WebPushPayloadDataBuilder> {
  _$WebPushPayloadData? _$v;

  ActionBuilder? _action;
  ActionBuilder get action => _$this._action ??= new ActionBuilder();
  set action(ActionBuilder? action) => _$this._action = action;

  WebPushPayloadDataBuilder() {
    WebPushPayloadData._defaults(this);
  }

  WebPushPayloadDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _action = $v.action.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(WebPushPayloadData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$WebPushPayloadData;
  }

  @override
  void update(void Function(WebPushPayloadDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  WebPushPayloadData build() => _build();

  _$WebPushPayloadData _build() {
    _$WebPushPayloadData _$result;
    try {
      _$result = _$v ?? new _$WebPushPayloadData._(action: action.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'action';
        action.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'WebPushPayloadData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
