// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$LoginResponse extends LoginResponse {
  @override
  final String accessToken;
  @override
  final DateTime accessTokenExpiredDate;
  @override
  final String refreshToken;
  @override
  final DateTime refreshTokenExpiredDate;
  @override
  final String sessionId;

  factory _$LoginResponse([void Function(LoginResponseBuilder)? updates]) =>
      (new LoginResponseBuilder()..update(updates))._build();

  _$LoginResponse._(
      {required this.accessToken,
      required this.accessTokenExpiredDate,
      required this.refreshToken,
      required this.refreshTokenExpiredDate,
      required this.sessionId})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        accessToken, r'LoginResponse', 'accessToken');
    BuiltValueNullFieldError.checkNotNull(
        accessTokenExpiredDate, r'LoginResponse', 'accessTokenExpiredDate');
    BuiltValueNullFieldError.checkNotNull(
        refreshToken, r'LoginResponse', 'refreshToken');
    BuiltValueNullFieldError.checkNotNull(
        refreshTokenExpiredDate, r'LoginResponse', 'refreshTokenExpiredDate');
    BuiltValueNullFieldError.checkNotNull(
        sessionId, r'LoginResponse', 'sessionId');
  }

  @override
  LoginResponse rebuild(void Function(LoginResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LoginResponseBuilder toBuilder() => new LoginResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LoginResponse &&
        accessToken == other.accessToken &&
        accessTokenExpiredDate == other.accessTokenExpiredDate &&
        refreshToken == other.refreshToken &&
        refreshTokenExpiredDate == other.refreshTokenExpiredDate &&
        sessionId == other.sessionId;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, accessToken.hashCode);
    _$hash = $jc(_$hash, accessTokenExpiredDate.hashCode);
    _$hash = $jc(_$hash, refreshToken.hashCode);
    _$hash = $jc(_$hash, refreshTokenExpiredDate.hashCode);
    _$hash = $jc(_$hash, sessionId.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'LoginResponse')
          ..add('accessToken', accessToken)
          ..add('accessTokenExpiredDate', accessTokenExpiredDate)
          ..add('refreshToken', refreshToken)
          ..add('refreshTokenExpiredDate', refreshTokenExpiredDate)
          ..add('sessionId', sessionId))
        .toString();
  }
}

class LoginResponseBuilder
    implements Builder<LoginResponse, LoginResponseBuilder> {
  _$LoginResponse? _$v;

  String? _accessToken;
  String? get accessToken => _$this._accessToken;
  set accessToken(String? accessToken) => _$this._accessToken = accessToken;

  DateTime? _accessTokenExpiredDate;
  DateTime? get accessTokenExpiredDate => _$this._accessTokenExpiredDate;
  set accessTokenExpiredDate(DateTime? accessTokenExpiredDate) =>
      _$this._accessTokenExpiredDate = accessTokenExpiredDate;

  String? _refreshToken;
  String? get refreshToken => _$this._refreshToken;
  set refreshToken(String? refreshToken) => _$this._refreshToken = refreshToken;

  DateTime? _refreshTokenExpiredDate;
  DateTime? get refreshTokenExpiredDate => _$this._refreshTokenExpiredDate;
  set refreshTokenExpiredDate(DateTime? refreshTokenExpiredDate) =>
      _$this._refreshTokenExpiredDate = refreshTokenExpiredDate;

  String? _sessionId;
  String? get sessionId => _$this._sessionId;
  set sessionId(String? sessionId) => _$this._sessionId = sessionId;

  LoginResponseBuilder() {
    LoginResponse._defaults(this);
  }

  LoginResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _accessToken = $v.accessToken;
      _accessTokenExpiredDate = $v.accessTokenExpiredDate;
      _refreshToken = $v.refreshToken;
      _refreshTokenExpiredDate = $v.refreshTokenExpiredDate;
      _sessionId = $v.sessionId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LoginResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$LoginResponse;
  }

  @override
  void update(void Function(LoginResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  LoginResponse build() => _build();

  _$LoginResponse _build() {
    final _$result = _$v ??
        new _$LoginResponse._(
            accessToken: BuiltValueNullFieldError.checkNotNull(
                accessToken, r'LoginResponse', 'accessToken'),
            accessTokenExpiredDate: BuiltValueNullFieldError.checkNotNull(
                accessTokenExpiredDate,
                r'LoginResponse',
                'accessTokenExpiredDate'),
            refreshToken: BuiltValueNullFieldError.checkNotNull(
                refreshToken, r'LoginResponse', 'refreshToken'),
            refreshTokenExpiredDate: BuiltValueNullFieldError.checkNotNull(
                refreshTokenExpiredDate,
                r'LoginResponse',
                'refreshTokenExpiredDate'),
            sessionId: BuiltValueNullFieldError.checkNotNull(
                sessionId, r'LoginResponse', 'sessionId'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
