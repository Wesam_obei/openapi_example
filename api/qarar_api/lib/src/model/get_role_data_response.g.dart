// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_role_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetRoleDataResponse extends GetRoleDataResponse {
  @override
  final num id;
  @override
  final String name;
  @override
  final String description;
  @override
  final DateTime createdAt;
  @override
  final BuiltList<PermissionDataResponse> permissions;

  factory _$GetRoleDataResponse(
          [void Function(GetRoleDataResponseBuilder)? updates]) =>
      (new GetRoleDataResponseBuilder()..update(updates))._build();

  _$GetRoleDataResponse._(
      {required this.id,
      required this.name,
      required this.description,
      required this.createdAt,
      required this.permissions})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, r'GetRoleDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(name, r'GetRoleDataResponse', 'name');
    BuiltValueNullFieldError.checkNotNull(
        description, r'GetRoleDataResponse', 'description');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'GetRoleDataResponse', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        permissions, r'GetRoleDataResponse', 'permissions');
  }

  @override
  GetRoleDataResponse rebuild(
          void Function(GetRoleDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetRoleDataResponseBuilder toBuilder() =>
      new GetRoleDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetRoleDataResponse &&
        id == other.id &&
        name == other.name &&
        description == other.description &&
        createdAt == other.createdAt &&
        permissions == other.permissions;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, permissions.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetRoleDataResponse')
          ..add('id', id)
          ..add('name', name)
          ..add('description', description)
          ..add('createdAt', createdAt)
          ..add('permissions', permissions))
        .toString();
  }
}

class GetRoleDataResponseBuilder
    implements Builder<GetRoleDataResponse, GetRoleDataResponseBuilder> {
  _$GetRoleDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  ListBuilder<PermissionDataResponse>? _permissions;
  ListBuilder<PermissionDataResponse> get permissions =>
      _$this._permissions ??= new ListBuilder<PermissionDataResponse>();
  set permissions(ListBuilder<PermissionDataResponse>? permissions) =>
      _$this._permissions = permissions;

  GetRoleDataResponseBuilder() {
    GetRoleDataResponse._defaults(this);
  }

  GetRoleDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _description = $v.description;
      _createdAt = $v.createdAt;
      _permissions = $v.permissions.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetRoleDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetRoleDataResponse;
  }

  @override
  void update(void Function(GetRoleDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetRoleDataResponse build() => _build();

  _$GetRoleDataResponse _build() {
    _$GetRoleDataResponse _$result;
    try {
      _$result = _$v ??
          new _$GetRoleDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'GetRoleDataResponse', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'GetRoleDataResponse', 'name'),
              description: BuiltValueNullFieldError.checkNotNull(
                  description, r'GetRoleDataResponse', 'description'),
              createdAt: BuiltValueNullFieldError.checkNotNull(
                  createdAt, r'GetRoleDataResponse', 'createdAt'),
              permissions: permissions.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'permissions';
        permissions.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'GetRoleDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
