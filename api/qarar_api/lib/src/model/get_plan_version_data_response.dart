//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/get_file_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'get_plan_version_data_response.g.dart';

/// GetPlanVersionDataResponse
///
/// Properties:
/// * [id]
/// * [revisionNumber]
/// * [revisionYear]
/// * [issuedAt]
/// * [file]
@BuiltValue()
abstract class GetPlanVersionDataResponse
    implements
        Built<GetPlanVersionDataResponse, GetPlanVersionDataResponseBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'revision_number')
  num get revisionNumber;

  @BuiltValueField(wireName: r'revision_year')
  num get revisionYear;

  @BuiltValueField(wireName: r'issued_at')
  String get issuedAt;

  @BuiltValueField(wireName: r'file')
  GetFileDataResponse get file;

  GetPlanVersionDataResponse._();

  factory GetPlanVersionDataResponse(
          [void updates(GetPlanVersionDataResponseBuilder b)]) =
      _$GetPlanVersionDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(GetPlanVersionDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<GetPlanVersionDataResponse> get serializer =>
      _$GetPlanVersionDataResponseSerializer();
}

class _$GetPlanVersionDataResponseSerializer
    implements PrimitiveSerializer<GetPlanVersionDataResponse> {
  @override
  final Iterable<Type> types = const [
    GetPlanVersionDataResponse,
    _$GetPlanVersionDataResponse
  ];

  @override
  final String wireName = r'GetPlanVersionDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    GetPlanVersionDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'revision_number';
    yield serializers.serialize(
      object.revisionNumber,
      specifiedType: const FullType(num),
    );
    yield r'revision_year';
    yield serializers.serialize(
      object.revisionYear,
      specifiedType: const FullType(num),
    );
    yield r'issued_at';
    yield serializers.serialize(
      object.issuedAt,
      specifiedType: const FullType(String),
    );
    yield r'file';
    yield serializers.serialize(
      object.file,
      specifiedType: const FullType(GetFileDataResponse),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    GetPlanVersionDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required GetPlanVersionDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'revision_number':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.revisionNumber = valueDes;
          break;
        case r'revision_year':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.revisionYear = valueDes;
          break;
        case r'issued_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.issuedAt = valueDes;
          break;
        case r'file':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetFileDataResponse),
          ) as GetFileDataResponse;
          result.file.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  GetPlanVersionDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = GetPlanVersionDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
