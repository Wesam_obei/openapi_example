// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_incident_reports_map200_response_all_of_data_features_inner_properties_id.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId
    extends MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId {
  @override
  final AnyOf anyOf;

  factory _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId(
          [void Function(
                  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder)?
              updates]) =>
      (new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId._(
      {required this.anyOf})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        anyOf,
        r'MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId',
        'anyOf');
  }

  @override
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId
      rebuild(
              void Function(
                      MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder)
                  updates) =>
          (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      toBuilder() =>
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId &&
        anyOf == other.anyOf;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, anyOf.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId')
          ..add('anyOf', anyOf))
        .toString();
  }
}

class MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
    implements
        Builder<
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId,
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder> {
  _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      _$v;

  AnyOf? _anyOf;
  AnyOf? get anyOf => _$this._anyOf;
  set anyOf(AnyOf? anyOf) => _$this._anyOf = anyOf;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder() {
    MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId
        ._defaults(this);
  }

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _anyOf = $v.anyOf;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId
      build() => _build();

  _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId
      _build() {
    final _$result = _$v ??
        new _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId
            ._(
            anyOf: BuiltValueNullFieldError.checkNotNull(
                anyOf,
                r'MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId',
                'anyOf'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
