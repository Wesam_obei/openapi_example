//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/form_components_request.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'form_update_request.g.dart';

/// FormUpdateRequest
///
/// Properties:
/// * [name]
/// * [description]
/// * [isActive]
/// * [fileToken]
/// * [formComponents]
/// * [deleteFile]
/// * [deletedComponents]
@BuiltValue()
abstract class FormUpdateRequest
    implements Built<FormUpdateRequest, FormUpdateRequestBuilder> {
  @BuiltValueField(wireName: r'name')
  String? get name;

  @BuiltValueField(wireName: r'description')
  String? get description;

  @BuiltValueField(wireName: r'is_active')
  bool? get isActive;

  @BuiltValueField(wireName: r'file_token')
  String? get fileToken;

  @BuiltValueField(wireName: r'formComponents')
  BuiltList<FormComponentsRequest>? get formComponents;

  @BuiltValueField(wireName: r'delete_file')
  bool? get deleteFile;

  @BuiltValueField(wireName: r'deleted_components')
  BuiltList<num>? get deletedComponents;

  FormUpdateRequest._();

  factory FormUpdateRequest([void updates(FormUpdateRequestBuilder b)]) =
      _$FormUpdateRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(FormUpdateRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<FormUpdateRequest> get serializer =>
      _$FormUpdateRequestSerializer();
}

class _$FormUpdateRequestSerializer
    implements PrimitiveSerializer<FormUpdateRequest> {
  @override
  final Iterable<Type> types = const [FormUpdateRequest, _$FormUpdateRequest];

  @override
  final String wireName = r'FormUpdateRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    FormUpdateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.name != null) {
      yield r'name';
      yield serializers.serialize(
        object.name,
        specifiedType: const FullType(String),
      );
    }
    if (object.description != null) {
      yield r'description';
      yield serializers.serialize(
        object.description,
        specifiedType: const FullType(String),
      );
    }
    if (object.isActive != null) {
      yield r'is_active';
      yield serializers.serialize(
        object.isActive,
        specifiedType: const FullType(bool),
      );
    }
    if (object.fileToken != null) {
      yield r'file_token';
      yield serializers.serialize(
        object.fileToken,
        specifiedType: const FullType(String),
      );
    }
    if (object.formComponents != null) {
      yield r'formComponents';
      yield serializers.serialize(
        object.formComponents,
        specifiedType:
            const FullType(BuiltList, [FullType(FormComponentsRequest)]),
      );
    }
    if (object.deleteFile != null) {
      yield r'delete_file';
      yield serializers.serialize(
        object.deleteFile,
        specifiedType: const FullType(bool),
      );
    }
    if (object.deletedComponents != null) {
      yield r'deleted_components';
      yield serializers.serialize(
        object.deletedComponents,
        specifiedType: const FullType(BuiltList, [FullType(num)]),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    FormUpdateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required FormUpdateRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'description':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.description = valueDes;
          break;
        case r'is_active':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.isActive = valueDes;
          break;
        case r'file_token':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.fileToken = valueDes;
          break;
        case r'formComponents':
          final valueDes = serializers.deserialize(
            value,
            specifiedType:
                const FullType(BuiltList, [FullType(FormComponentsRequest)]),
          ) as BuiltList<FormComponentsRequest>;
          result.formComponents.replace(valueDes);
          break;
        case r'delete_file':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.deleteFile = valueDes;
          break;
        case r'deleted_components':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(num)]),
          ) as BuiltList<num>;
          result.deletedComponents.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  FormUpdateRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = FormUpdateRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
