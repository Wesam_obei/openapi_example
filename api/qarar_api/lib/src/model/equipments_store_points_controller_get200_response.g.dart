// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'equipments_store_points_controller_get200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$EquipmentsStorePointsControllerGet200Response
    extends EquipmentsStorePointsControllerGet200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$EquipmentsStorePointsControllerGet200Response(
          [void Function(EquipmentsStorePointsControllerGet200ResponseBuilder)?
              updates]) =>
      (new EquipmentsStorePointsControllerGet200ResponseBuilder()
            ..update(updates))
          ._build();

  _$EquipmentsStorePointsControllerGet200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'EquipmentsStorePointsControllerGet200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'EquipmentsStorePointsControllerGet200Response', 'data');
  }

  @override
  EquipmentsStorePointsControllerGet200Response rebuild(
          void Function(EquipmentsStorePointsControllerGet200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  EquipmentsStorePointsControllerGet200ResponseBuilder toBuilder() =>
      new EquipmentsStorePointsControllerGet200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EquipmentsStorePointsControllerGet200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'EquipmentsStorePointsControllerGet200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class EquipmentsStorePointsControllerGet200ResponseBuilder
    implements
        Builder<EquipmentsStorePointsControllerGet200Response,
            EquipmentsStorePointsControllerGet200ResponseBuilder>,
        SuccessResponseBuilder {
  _$EquipmentsStorePointsControllerGet200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  EquipmentsStorePointsControllerGet200ResponseBuilder() {
    EquipmentsStorePointsControllerGet200Response._defaults(this);
  }

  EquipmentsStorePointsControllerGet200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant EquipmentsStorePointsControllerGet200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$EquipmentsStorePointsControllerGet200Response;
  }

  @override
  void update(
      void Function(EquipmentsStorePointsControllerGet200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  EquipmentsStorePointsControllerGet200Response build() => _build();

  _$EquipmentsStorePointsControllerGet200Response _build() {
    final _$result = _$v ??
        new _$EquipmentsStorePointsControllerGet200Response._(
            success: BuiltValueNullFieldError.checkNotNull(success,
                r'EquipmentsStorePointsControllerGet200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(data,
                r'EquipmentsStorePointsControllerGet200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
