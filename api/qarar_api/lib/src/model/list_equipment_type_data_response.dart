//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/get_file_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'list_equipment_type_data_response.g.dart';

/// ListEquipmentTypeDataResponse
///
/// Properties:
/// * [id]
/// * [name]
/// * [createdAt]
/// * [file]
@BuiltValue()
abstract class ListEquipmentTypeDataResponse
    implements
        Built<ListEquipmentTypeDataResponse,
            ListEquipmentTypeDataResponseBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'created_at')
  DateTime get createdAt;

  @BuiltValueField(wireName: r'file')
  GetFileDataResponse? get file;

  ListEquipmentTypeDataResponse._();

  factory ListEquipmentTypeDataResponse(
          [void updates(ListEquipmentTypeDataResponseBuilder b)]) =
      _$ListEquipmentTypeDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ListEquipmentTypeDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ListEquipmentTypeDataResponse> get serializer =>
      _$ListEquipmentTypeDataResponseSerializer();
}

class _$ListEquipmentTypeDataResponseSerializer
    implements PrimitiveSerializer<ListEquipmentTypeDataResponse> {
  @override
  final Iterable<Type> types = const [
    ListEquipmentTypeDataResponse,
    _$ListEquipmentTypeDataResponse
  ];

  @override
  final String wireName = r'ListEquipmentTypeDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ListEquipmentTypeDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    yield r'created_at';
    yield serializers.serialize(
      object.createdAt,
      specifiedType: const FullType(DateTime),
    );
    if (object.file != null) {
      yield r'file';
      yield serializers.serialize(
        object.file,
        specifiedType: const FullType(GetFileDataResponse),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ListEquipmentTypeDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ListEquipmentTypeDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'created_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.createdAt = valueDes;
          break;
        case r'file':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetFileDataResponse),
          ) as GetFileDataResponse;
          result.file.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ListEquipmentTypeDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ListEquipmentTypeDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
