// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_audit_log_event_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetAuditLogEventDataResponse extends GetAuditLogEventDataResponse {
  @override
  final String id;
  @override
  final String name;
  @override
  final String description;

  factory _$GetAuditLogEventDataResponse(
          [void Function(GetAuditLogEventDataResponseBuilder)? updates]) =>
      (new GetAuditLogEventDataResponseBuilder()..update(updates))._build();

  _$GetAuditLogEventDataResponse._(
      {required this.id, required this.name, required this.description})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'GetAuditLogEventDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, r'GetAuditLogEventDataResponse', 'name');
    BuiltValueNullFieldError.checkNotNull(
        description, r'GetAuditLogEventDataResponse', 'description');
  }

  @override
  GetAuditLogEventDataResponse rebuild(
          void Function(GetAuditLogEventDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetAuditLogEventDataResponseBuilder toBuilder() =>
      new GetAuditLogEventDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetAuditLogEventDataResponse &&
        id == other.id &&
        name == other.name &&
        description == other.description;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetAuditLogEventDataResponse')
          ..add('id', id)
          ..add('name', name)
          ..add('description', description))
        .toString();
  }
}

class GetAuditLogEventDataResponseBuilder
    implements
        Builder<GetAuditLogEventDataResponse,
            GetAuditLogEventDataResponseBuilder> {
  _$GetAuditLogEventDataResponse? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  GetAuditLogEventDataResponseBuilder() {
    GetAuditLogEventDataResponse._defaults(this);
  }

  GetAuditLogEventDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _description = $v.description;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetAuditLogEventDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetAuditLogEventDataResponse;
  }

  @override
  void update(void Function(GetAuditLogEventDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetAuditLogEventDataResponse build() => _build();

  _$GetAuditLogEventDataResponse _build() {
    final _$result = _$v ??
        new _$GetAuditLogEventDataResponse._(
            id: BuiltValueNullFieldError.checkNotNull(
                id, r'GetAuditLogEventDataResponse', 'id'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, r'GetAuditLogEventDataResponse', 'name'),
            description: BuiltValueNullFieldError.checkNotNull(
                description, r'GetAuditLogEventDataResponse', 'description'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
