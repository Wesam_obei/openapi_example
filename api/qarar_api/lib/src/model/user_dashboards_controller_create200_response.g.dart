// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_dashboards_controller_create200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$UserDashboardsControllerCreate200Response
    extends UserDashboardsControllerCreate200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$UserDashboardsControllerCreate200Response(
          [void Function(UserDashboardsControllerCreate200ResponseBuilder)?
              updates]) =>
      (new UserDashboardsControllerCreate200ResponseBuilder()..update(updates))
          ._build();

  _$UserDashboardsControllerCreate200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'UserDashboardsControllerCreate200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'UserDashboardsControllerCreate200Response', 'data');
  }

  @override
  UserDashboardsControllerCreate200Response rebuild(
          void Function(UserDashboardsControllerCreate200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserDashboardsControllerCreate200ResponseBuilder toBuilder() =>
      new UserDashboardsControllerCreate200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserDashboardsControllerCreate200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'UserDashboardsControllerCreate200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class UserDashboardsControllerCreate200ResponseBuilder
    implements
        Builder<UserDashboardsControllerCreate200Response,
            UserDashboardsControllerCreate200ResponseBuilder>,
        SuccessResponseBuilder {
  _$UserDashboardsControllerCreate200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  UserDashboardsControllerCreate200ResponseBuilder() {
    UserDashboardsControllerCreate200Response._defaults(this);
  }

  UserDashboardsControllerCreate200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant UserDashboardsControllerCreate200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$UserDashboardsControllerCreate200Response;
  }

  @override
  void update(
      void Function(UserDashboardsControllerCreate200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  UserDashboardsControllerCreate200Response build() => _build();

  _$UserDashboardsControllerCreate200Response _build() {
    final _$result = _$v ??
        new _$UserDashboardsControllerCreate200Response._(
            success: BuiltValueNullFieldError.checkNotNull(success,
                r'UserDashboardsControllerCreate200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'UserDashboardsControllerCreate200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
