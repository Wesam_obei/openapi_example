// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'health_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$HealthDataResponse extends HealthDataResponse {
  @override
  final String status;
  @override
  final JsonObject info;
  @override
  final JsonObject error;
  @override
  final JsonObject details;

  factory _$HealthDataResponse(
          [void Function(HealthDataResponseBuilder)? updates]) =>
      (new HealthDataResponseBuilder()..update(updates))._build();

  _$HealthDataResponse._(
      {required this.status,
      required this.info,
      required this.error,
      required this.details})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        status, r'HealthDataResponse', 'status');
    BuiltValueNullFieldError.checkNotNull(info, r'HealthDataResponse', 'info');
    BuiltValueNullFieldError.checkNotNull(
        error, r'HealthDataResponse', 'error');
    BuiltValueNullFieldError.checkNotNull(
        details, r'HealthDataResponse', 'details');
  }

  @override
  HealthDataResponse rebuild(
          void Function(HealthDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  HealthDataResponseBuilder toBuilder() =>
      new HealthDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is HealthDataResponse &&
        status == other.status &&
        info == other.info &&
        error == other.error &&
        details == other.details;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jc(_$hash, info.hashCode);
    _$hash = $jc(_$hash, error.hashCode);
    _$hash = $jc(_$hash, details.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'HealthDataResponse')
          ..add('status', status)
          ..add('info', info)
          ..add('error', error)
          ..add('details', details))
        .toString();
  }
}

class HealthDataResponseBuilder
    implements Builder<HealthDataResponse, HealthDataResponseBuilder> {
  _$HealthDataResponse? _$v;

  String? _status;
  String? get status => _$this._status;
  set status(String? status) => _$this._status = status;

  JsonObject? _info;
  JsonObject? get info => _$this._info;
  set info(JsonObject? info) => _$this._info = info;

  JsonObject? _error;
  JsonObject? get error => _$this._error;
  set error(JsonObject? error) => _$this._error = error;

  JsonObject? _details;
  JsonObject? get details => _$this._details;
  set details(JsonObject? details) => _$this._details = details;

  HealthDataResponseBuilder() {
    HealthDataResponse._defaults(this);
  }

  HealthDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _status = $v.status;
      _info = $v.info;
      _error = $v.error;
      _details = $v.details;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(HealthDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$HealthDataResponse;
  }

  @override
  void update(void Function(HealthDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  HealthDataResponse build() => _build();

  _$HealthDataResponse _build() {
    final _$result = _$v ??
        new _$HealthDataResponse._(
            status: BuiltValueNullFieldError.checkNotNull(
                status, r'HealthDataResponse', 'status'),
            info: BuiltValueNullFieldError.checkNotNull(
                info, r'HealthDataResponse', 'info'),
            error: BuiltValueNullFieldError.checkNotNull(
                error, r'HealthDataResponse', 'error'),
            details: BuiltValueNullFieldError.checkNotNull(
                details, r'HealthDataResponse', 'details'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
