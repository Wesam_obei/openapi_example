// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_entities_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ListEntitiesDataResponse extends ListEntitiesDataResponse {
  @override
  final num id;
  @override
  final String name;
  @override
  final DateTime createdAt;
  @override
  final BuiltList<String> users;
  @override
  final GetFileDataResponse? file;

  factory _$ListEntitiesDataResponse(
          [void Function(ListEntitiesDataResponseBuilder)? updates]) =>
      (new ListEntitiesDataResponseBuilder()..update(updates))._build();

  _$ListEntitiesDataResponse._(
      {required this.id,
      required this.name,
      required this.createdAt,
      required this.users,
      this.file})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'ListEntitiesDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, r'ListEntitiesDataResponse', 'name');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'ListEntitiesDataResponse', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        users, r'ListEntitiesDataResponse', 'users');
  }

  @override
  ListEntitiesDataResponse rebuild(
          void Function(ListEntitiesDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ListEntitiesDataResponseBuilder toBuilder() =>
      new ListEntitiesDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ListEntitiesDataResponse &&
        id == other.id &&
        name == other.name &&
        createdAt == other.createdAt &&
        users == other.users &&
        file == other.file;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, users.hashCode);
    _$hash = $jc(_$hash, file.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ListEntitiesDataResponse')
          ..add('id', id)
          ..add('name', name)
          ..add('createdAt', createdAt)
          ..add('users', users)
          ..add('file', file))
        .toString();
  }
}

class ListEntitiesDataResponseBuilder
    implements
        Builder<ListEntitiesDataResponse, ListEntitiesDataResponseBuilder> {
  _$ListEntitiesDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  ListBuilder<String>? _users;
  ListBuilder<String> get users => _$this._users ??= new ListBuilder<String>();
  set users(ListBuilder<String>? users) => _$this._users = users;

  GetFileDataResponseBuilder? _file;
  GetFileDataResponseBuilder get file =>
      _$this._file ??= new GetFileDataResponseBuilder();
  set file(GetFileDataResponseBuilder? file) => _$this._file = file;

  ListEntitiesDataResponseBuilder() {
    ListEntitiesDataResponse._defaults(this);
  }

  ListEntitiesDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _createdAt = $v.createdAt;
      _users = $v.users.toBuilder();
      _file = $v.file?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ListEntitiesDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ListEntitiesDataResponse;
  }

  @override
  void update(void Function(ListEntitiesDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ListEntitiesDataResponse build() => _build();

  _$ListEntitiesDataResponse _build() {
    _$ListEntitiesDataResponse _$result;
    try {
      _$result = _$v ??
          new _$ListEntitiesDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'ListEntitiesDataResponse', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'ListEntitiesDataResponse', 'name'),
              createdAt: BuiltValueNullFieldError.checkNotNull(
                  createdAt, r'ListEntitiesDataResponse', 'createdAt'),
              users: users.build(),
              file: _file?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'users';
        users.build();
        _$failedField = 'file';
        _file?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ListEntitiesDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
