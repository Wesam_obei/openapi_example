//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'paginated_links_documented.g.dart';

/// PaginatedLinksDocumented
///
/// Properties:
/// * [first]
/// * [previous]
/// * [current]
/// * [next]
/// * [last]
@BuiltValue()
abstract class PaginatedLinksDocumented
    implements
        Built<PaginatedLinksDocumented, PaginatedLinksDocumentedBuilder> {
  @BuiltValueField(wireName: r'first')
  String? get first;

  @BuiltValueField(wireName: r'previous')
  String? get previous;

  @BuiltValueField(wireName: r'current')
  String? get current;

  @BuiltValueField(wireName: r'next')
  String? get next;

  @BuiltValueField(wireName: r'last')
  String? get last;

  PaginatedLinksDocumented._();

  factory PaginatedLinksDocumented(
          [void updates(PaginatedLinksDocumentedBuilder b)]) =
      _$PaginatedLinksDocumented;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(PaginatedLinksDocumentedBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<PaginatedLinksDocumented> get serializer =>
      _$PaginatedLinksDocumentedSerializer();
}

class _$PaginatedLinksDocumentedSerializer
    implements PrimitiveSerializer<PaginatedLinksDocumented> {
  @override
  final Iterable<Type> types = const [
    PaginatedLinksDocumented,
    _$PaginatedLinksDocumented
  ];

  @override
  final String wireName = r'PaginatedLinksDocumented';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    PaginatedLinksDocumented object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.first != null) {
      yield r'first';
      yield serializers.serialize(
        object.first,
        specifiedType: const FullType(String),
      );
    }
    if (object.previous != null) {
      yield r'previous';
      yield serializers.serialize(
        object.previous,
        specifiedType: const FullType(String),
      );
    }
    if (object.current != null) {
      yield r'current';
      yield serializers.serialize(
        object.current,
        specifiedType: const FullType(String),
      );
    }
    if (object.next != null) {
      yield r'next';
      yield serializers.serialize(
        object.next,
        specifiedType: const FullType(String),
      );
    }
    if (object.last != null) {
      yield r'last';
      yield serializers.serialize(
        object.last,
        specifiedType: const FullType(String),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    PaginatedLinksDocumented object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required PaginatedLinksDocumentedBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'first':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.first = valueDes;
          break;
        case r'previous':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.previous = valueDes;
          break;
        case r'current':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.current = valueDes;
          break;
        case r'next':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.next = valueDes;
          break;
        case r'last':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.last = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  PaginatedLinksDocumented deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = PaginatedLinksDocumentedBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
