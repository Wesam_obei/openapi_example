//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/success_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'notifications_controller_count_unread200_response.g.dart';

/// My custom description
///
/// Properties:
/// * [success]
/// * [data] - count unread notifications
@BuiltValue()
abstract class NotificationsControllerCountUnread200Response
    implements
        SuccessResponse,
        Built<NotificationsControllerCountUnread200Response,
            NotificationsControllerCountUnread200ResponseBuilder> {
  NotificationsControllerCountUnread200Response._();

  factory NotificationsControllerCountUnread200Response(
          [void updates(
              NotificationsControllerCountUnread200ResponseBuilder b)]) =
      _$NotificationsControllerCountUnread200Response;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          NotificationsControllerCountUnread200ResponseBuilder b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<NotificationsControllerCountUnread200Response>
      get serializer =>
          _$NotificationsControllerCountUnread200ResponseSerializer();
}

class _$NotificationsControllerCountUnread200ResponseSerializer
    implements
        PrimitiveSerializer<NotificationsControllerCountUnread200Response> {
  @override
  final Iterable<Type> types = const [
    NotificationsControllerCountUnread200Response,
    _$NotificationsControllerCountUnread200Response
  ];

  @override
  final String wireName = r'NotificationsControllerCountUnread200Response';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    NotificationsControllerCountUnread200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'data';
    yield serializers.serialize(
      object.data,
      specifiedType: const FullType(JsonObject),
    );
    yield r'success';
    yield serializers.serialize(
      object.success,
      specifiedType: const FullType(bool),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    NotificationsControllerCountUnread200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required NotificationsControllerCountUnread200ResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'data':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.data = valueDes;
          break;
        case r'success':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.success = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  NotificationsControllerCountUnread200Response deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = NotificationsControllerCountUnread200ResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
