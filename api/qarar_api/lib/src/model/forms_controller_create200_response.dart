//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/success_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'forms_controller_create200_response.g.dart';

/// My custom description
///
/// Properties:
/// * [success]
/// * [data] - Form has been created successfully.
@BuiltValue()
abstract class FormsControllerCreate200Response
    implements
        SuccessResponse,
        Built<FormsControllerCreate200Response,
            FormsControllerCreate200ResponseBuilder> {
  FormsControllerCreate200Response._();

  factory FormsControllerCreate200Response(
          [void updates(FormsControllerCreate200ResponseBuilder b)]) =
      _$FormsControllerCreate200Response;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(FormsControllerCreate200ResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<FormsControllerCreate200Response> get serializer =>
      _$FormsControllerCreate200ResponseSerializer();
}

class _$FormsControllerCreate200ResponseSerializer
    implements PrimitiveSerializer<FormsControllerCreate200Response> {
  @override
  final Iterable<Type> types = const [
    FormsControllerCreate200Response,
    _$FormsControllerCreate200Response
  ];

  @override
  final String wireName = r'FormsControllerCreate200Response';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    FormsControllerCreate200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'data';
    yield serializers.serialize(
      object.data,
      specifiedType: const FullType(JsonObject),
    );
    yield r'success';
    yield serializers.serialize(
      object.success,
      specifiedType: const FullType(bool),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    FormsControllerCreate200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required FormsControllerCreate200ResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'data':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.data = valueDes;
          break;
        case r'success':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.success = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  FormsControllerCreate200Response deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = FormsControllerCreate200ResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
