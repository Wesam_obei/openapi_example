// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'answer_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$AnswerRequest extends AnswerRequest {
  @override
  final num order;
  @override
  final num formComponentId;
  @override
  final JsonObject value;
  @override
  final num? id;

  factory _$AnswerRequest([void Function(AnswerRequestBuilder)? updates]) =>
      (new AnswerRequestBuilder()..update(updates))._build();

  _$AnswerRequest._(
      {required this.order,
      required this.formComponentId,
      required this.value,
      this.id})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(order, r'AnswerRequest', 'order');
    BuiltValueNullFieldError.checkNotNull(
        formComponentId, r'AnswerRequest', 'formComponentId');
    BuiltValueNullFieldError.checkNotNull(value, r'AnswerRequest', 'value');
  }

  @override
  AnswerRequest rebuild(void Function(AnswerRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AnswerRequestBuilder toBuilder() => new AnswerRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AnswerRequest &&
        order == other.order &&
        formComponentId == other.formComponentId &&
        value == other.value &&
        id == other.id;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, order.hashCode);
    _$hash = $jc(_$hash, formComponentId.hashCode);
    _$hash = $jc(_$hash, value.hashCode);
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'AnswerRequest')
          ..add('order', order)
          ..add('formComponentId', formComponentId)
          ..add('value', value)
          ..add('id', id))
        .toString();
  }
}

class AnswerRequestBuilder
    implements Builder<AnswerRequest, AnswerRequestBuilder> {
  _$AnswerRequest? _$v;

  num? _order;
  num? get order => _$this._order;
  set order(num? order) => _$this._order = order;

  num? _formComponentId;
  num? get formComponentId => _$this._formComponentId;
  set formComponentId(num? formComponentId) =>
      _$this._formComponentId = formComponentId;

  JsonObject? _value;
  JsonObject? get value => _$this._value;
  set value(JsonObject? value) => _$this._value = value;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  AnswerRequestBuilder() {
    AnswerRequest._defaults(this);
  }

  AnswerRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _order = $v.order;
      _formComponentId = $v.formComponentId;
      _value = $v.value;
      _id = $v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AnswerRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$AnswerRequest;
  }

  @override
  void update(void Function(AnswerRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  AnswerRequest build() => _build();

  _$AnswerRequest _build() {
    final _$result = _$v ??
        new _$AnswerRequest._(
            order: BuiltValueNullFieldError.checkNotNull(
                order, r'AnswerRequest', 'order'),
            formComponentId: BuiltValueNullFieldError.checkNotNull(
                formComponentId, r'AnswerRequest', 'formComponentId'),
            value: BuiltValueNullFieldError.checkNotNull(
                value, r'AnswerRequest', 'value'),
            id: id);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
