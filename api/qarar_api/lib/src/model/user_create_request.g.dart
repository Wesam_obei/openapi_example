// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_create_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$UserCreateRequest extends UserCreateRequest {
  @override
  final num employeeId;
  @override
  final bool isActive;
  @override
  final BuiltList<num> roles;
  @override
  final BuiltList<String> permissions;
  @override
  final BuiltList<num> entities;

  factory _$UserCreateRequest(
          [void Function(UserCreateRequestBuilder)? updates]) =>
      (new UserCreateRequestBuilder()..update(updates))._build();

  _$UserCreateRequest._(
      {required this.employeeId,
      required this.isActive,
      required this.roles,
      required this.permissions,
      required this.entities})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        employeeId, r'UserCreateRequest', 'employeeId');
    BuiltValueNullFieldError.checkNotNull(
        isActive, r'UserCreateRequest', 'isActive');
    BuiltValueNullFieldError.checkNotNull(roles, r'UserCreateRequest', 'roles');
    BuiltValueNullFieldError.checkNotNull(
        permissions, r'UserCreateRequest', 'permissions');
    BuiltValueNullFieldError.checkNotNull(
        entities, r'UserCreateRequest', 'entities');
  }

  @override
  UserCreateRequest rebuild(void Function(UserCreateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserCreateRequestBuilder toBuilder() =>
      new UserCreateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserCreateRequest &&
        employeeId == other.employeeId &&
        isActive == other.isActive &&
        roles == other.roles &&
        permissions == other.permissions &&
        entities == other.entities;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, employeeId.hashCode);
    _$hash = $jc(_$hash, isActive.hashCode);
    _$hash = $jc(_$hash, roles.hashCode);
    _$hash = $jc(_$hash, permissions.hashCode);
    _$hash = $jc(_$hash, entities.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'UserCreateRequest')
          ..add('employeeId', employeeId)
          ..add('isActive', isActive)
          ..add('roles', roles)
          ..add('permissions', permissions)
          ..add('entities', entities))
        .toString();
  }
}

class UserCreateRequestBuilder
    implements Builder<UserCreateRequest, UserCreateRequestBuilder> {
  _$UserCreateRequest? _$v;

  num? _employeeId;
  num? get employeeId => _$this._employeeId;
  set employeeId(num? employeeId) => _$this._employeeId = employeeId;

  bool? _isActive;
  bool? get isActive => _$this._isActive;
  set isActive(bool? isActive) => _$this._isActive = isActive;

  ListBuilder<num>? _roles;
  ListBuilder<num> get roles => _$this._roles ??= new ListBuilder<num>();
  set roles(ListBuilder<num>? roles) => _$this._roles = roles;

  ListBuilder<String>? _permissions;
  ListBuilder<String> get permissions =>
      _$this._permissions ??= new ListBuilder<String>();
  set permissions(ListBuilder<String>? permissions) =>
      _$this._permissions = permissions;

  ListBuilder<num>? _entities;
  ListBuilder<num> get entities => _$this._entities ??= new ListBuilder<num>();
  set entities(ListBuilder<num>? entities) => _$this._entities = entities;

  UserCreateRequestBuilder() {
    UserCreateRequest._defaults(this);
  }

  UserCreateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _employeeId = $v.employeeId;
      _isActive = $v.isActive;
      _roles = $v.roles.toBuilder();
      _permissions = $v.permissions.toBuilder();
      _entities = $v.entities.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserCreateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$UserCreateRequest;
  }

  @override
  void update(void Function(UserCreateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  UserCreateRequest build() => _build();

  _$UserCreateRequest _build() {
    _$UserCreateRequest _$result;
    try {
      _$result = _$v ??
          new _$UserCreateRequest._(
              employeeId: BuiltValueNullFieldError.checkNotNull(
                  employeeId, r'UserCreateRequest', 'employeeId'),
              isActive: BuiltValueNullFieldError.checkNotNull(
                  isActive, r'UserCreateRequest', 'isActive'),
              roles: roles.build(),
              permissions: permissions.build(),
              entities: entities.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'roles';
        roles.build();
        _$failedField = 'permissions';
        permissions.build();
        _$failedField = 'entities';
        entities.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'UserCreateRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
