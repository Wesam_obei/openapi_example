//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/success_response.dart';
import 'package:qarar_api/src/model/survey_request_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'surveys_controller_requests_details200_response.g.dart';

/// My custom description
///
/// Properties:
/// * [success]
/// * [data]
@BuiltValue()
abstract class SurveysControllerRequestsDetails200Response
    implements
        SuccessResponse,
        Built<SurveysControllerRequestsDetails200Response,
            SurveysControllerRequestsDetails200ResponseBuilder> {
  SurveysControllerRequestsDetails200Response._();

  factory SurveysControllerRequestsDetails200Response(
          [void updates(
              SurveysControllerRequestsDetails200ResponseBuilder b)]) =
      _$SurveysControllerRequestsDetails200Response;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(SurveysControllerRequestsDetails200ResponseBuilder b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<SurveysControllerRequestsDetails200Response>
      get serializer =>
          _$SurveysControllerRequestsDetails200ResponseSerializer();
}

class _$SurveysControllerRequestsDetails200ResponseSerializer
    implements
        PrimitiveSerializer<SurveysControllerRequestsDetails200Response> {
  @override
  final Iterable<Type> types = const [
    SurveysControllerRequestsDetails200Response,
    _$SurveysControllerRequestsDetails200Response
  ];

  @override
  final String wireName = r'SurveysControllerRequestsDetails200Response';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    SurveysControllerRequestsDetails200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'data';
    yield serializers.serialize(
      object.data,
      specifiedType: const FullType(JsonObject),
    );
    yield r'success';
    yield serializers.serialize(
      object.success,
      specifiedType: const FullType(bool),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    SurveysControllerRequestsDetails200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required SurveysControllerRequestsDetails200ResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'data':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.data = valueDes;
          break;
        case r'success':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.success = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  SurveysControllerRequestsDetails200Response deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = SurveysControllerRequestsDetails200ResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
