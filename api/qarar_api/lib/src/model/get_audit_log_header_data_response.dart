//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'get_audit_log_header_data_response.g.dart';

/// GetAuditLogHeaderDataResponse
///
/// Properties:
/// * [userAgent]
/// * [referer]
/// * [xLanguage]
@BuiltValue()
abstract class GetAuditLogHeaderDataResponse
    implements
        Built<GetAuditLogHeaderDataResponse,
            GetAuditLogHeaderDataResponseBuilder> {
  @BuiltValueField(wireName: r'user-agent')
  String get userAgent;

  @BuiltValueField(wireName: r'referer')
  String get referer;

  @BuiltValueField(wireName: r'x-language')
  String get xLanguage;

  GetAuditLogHeaderDataResponse._();

  factory GetAuditLogHeaderDataResponse(
          [void updates(GetAuditLogHeaderDataResponseBuilder b)]) =
      _$GetAuditLogHeaderDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(GetAuditLogHeaderDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<GetAuditLogHeaderDataResponse> get serializer =>
      _$GetAuditLogHeaderDataResponseSerializer();
}

class _$GetAuditLogHeaderDataResponseSerializer
    implements PrimitiveSerializer<GetAuditLogHeaderDataResponse> {
  @override
  final Iterable<Type> types = const [
    GetAuditLogHeaderDataResponse,
    _$GetAuditLogHeaderDataResponse
  ];

  @override
  final String wireName = r'GetAuditLogHeaderDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    GetAuditLogHeaderDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'user-agent';
    yield serializers.serialize(
      object.userAgent,
      specifiedType: const FullType(String),
    );
    yield r'referer';
    yield serializers.serialize(
      object.referer,
      specifiedType: const FullType(String),
    );
    yield r'x-language';
    yield serializers.serialize(
      object.xLanguage,
      specifiedType: const FullType(String),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    GetAuditLogHeaderDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required GetAuditLogHeaderDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'user-agent':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.userAgent = valueDes;
          break;
        case r'referer':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.referer = valueDes;
          break;
        case r'x-language':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.xLanguage = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  GetAuditLogHeaderDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = GetAuditLogHeaderDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
