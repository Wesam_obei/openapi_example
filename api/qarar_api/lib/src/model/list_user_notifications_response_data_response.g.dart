// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_user_notifications_response_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ListUserNotificationsResponseDataResponse
    extends ListUserNotificationsResponseDataResponse {
  @override
  final num id;
  @override
  final String title;
  @override
  final String body;
  @override
  final String icon;
  @override
  final WebPushPayloadData data;
  @override
  final num recipientId;
  @override
  final JsonObject readAt;
  @override
  final JsonObject trashedAt;
  @override
  final DateTime createdAt;
  @override
  final GetNotificationResponseData notification;

  factory _$ListUserNotificationsResponseDataResponse(
          [void Function(ListUserNotificationsResponseDataResponseBuilder)?
              updates]) =>
      (new ListUserNotificationsResponseDataResponseBuilder()..update(updates))
          ._build();

  _$ListUserNotificationsResponseDataResponse._(
      {required this.id,
      required this.title,
      required this.body,
      required this.icon,
      required this.data,
      required this.recipientId,
      required this.readAt,
      required this.trashedAt,
      required this.createdAt,
      required this.notification})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'ListUserNotificationsResponseDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        title, r'ListUserNotificationsResponseDataResponse', 'title');
    BuiltValueNullFieldError.checkNotNull(
        body, r'ListUserNotificationsResponseDataResponse', 'body');
    BuiltValueNullFieldError.checkNotNull(
        icon, r'ListUserNotificationsResponseDataResponse', 'icon');
    BuiltValueNullFieldError.checkNotNull(
        data, r'ListUserNotificationsResponseDataResponse', 'data');
    BuiltValueNullFieldError.checkNotNull(recipientId,
        r'ListUserNotificationsResponseDataResponse', 'recipientId');
    BuiltValueNullFieldError.checkNotNull(
        readAt, r'ListUserNotificationsResponseDataResponse', 'readAt');
    BuiltValueNullFieldError.checkNotNull(
        trashedAt, r'ListUserNotificationsResponseDataResponse', 'trashedAt');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'ListUserNotificationsResponseDataResponse', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(notification,
        r'ListUserNotificationsResponseDataResponse', 'notification');
  }

  @override
  ListUserNotificationsResponseDataResponse rebuild(
          void Function(ListUserNotificationsResponseDataResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ListUserNotificationsResponseDataResponseBuilder toBuilder() =>
      new ListUserNotificationsResponseDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ListUserNotificationsResponseDataResponse &&
        id == other.id &&
        title == other.title &&
        body == other.body &&
        icon == other.icon &&
        data == other.data &&
        recipientId == other.recipientId &&
        readAt == other.readAt &&
        trashedAt == other.trashedAt &&
        createdAt == other.createdAt &&
        notification == other.notification;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, title.hashCode);
    _$hash = $jc(_$hash, body.hashCode);
    _$hash = $jc(_$hash, icon.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jc(_$hash, recipientId.hashCode);
    _$hash = $jc(_$hash, readAt.hashCode);
    _$hash = $jc(_$hash, trashedAt.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, notification.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'ListUserNotificationsResponseDataResponse')
          ..add('id', id)
          ..add('title', title)
          ..add('body', body)
          ..add('icon', icon)
          ..add('data', data)
          ..add('recipientId', recipientId)
          ..add('readAt', readAt)
          ..add('trashedAt', trashedAt)
          ..add('createdAt', createdAt)
          ..add('notification', notification))
        .toString();
  }
}

class ListUserNotificationsResponseDataResponseBuilder
    implements
        Builder<ListUserNotificationsResponseDataResponse,
            ListUserNotificationsResponseDataResponseBuilder> {
  _$ListUserNotificationsResponseDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  String? _body;
  String? get body => _$this._body;
  set body(String? body) => _$this._body = body;

  String? _icon;
  String? get icon => _$this._icon;
  set icon(String? icon) => _$this._icon = icon;

  WebPushPayloadDataBuilder? _data;
  WebPushPayloadDataBuilder get data =>
      _$this._data ??= new WebPushPayloadDataBuilder();
  set data(WebPushPayloadDataBuilder? data) => _$this._data = data;

  num? _recipientId;
  num? get recipientId => _$this._recipientId;
  set recipientId(num? recipientId) => _$this._recipientId = recipientId;

  JsonObject? _readAt;
  JsonObject? get readAt => _$this._readAt;
  set readAt(JsonObject? readAt) => _$this._readAt = readAt;

  JsonObject? _trashedAt;
  JsonObject? get trashedAt => _$this._trashedAt;
  set trashedAt(JsonObject? trashedAt) => _$this._trashedAt = trashedAt;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  GetNotificationResponseDataBuilder? _notification;
  GetNotificationResponseDataBuilder get notification =>
      _$this._notification ??= new GetNotificationResponseDataBuilder();
  set notification(GetNotificationResponseDataBuilder? notification) =>
      _$this._notification = notification;

  ListUserNotificationsResponseDataResponseBuilder() {
    ListUserNotificationsResponseDataResponse._defaults(this);
  }

  ListUserNotificationsResponseDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _title = $v.title;
      _body = $v.body;
      _icon = $v.icon;
      _data = $v.data.toBuilder();
      _recipientId = $v.recipientId;
      _readAt = $v.readAt;
      _trashedAt = $v.trashedAt;
      _createdAt = $v.createdAt;
      _notification = $v.notification.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ListUserNotificationsResponseDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ListUserNotificationsResponseDataResponse;
  }

  @override
  void update(
      void Function(ListUserNotificationsResponseDataResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  ListUserNotificationsResponseDataResponse build() => _build();

  _$ListUserNotificationsResponseDataResponse _build() {
    _$ListUserNotificationsResponseDataResponse _$result;
    try {
      _$result = _$v ??
          new _$ListUserNotificationsResponseDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'ListUserNotificationsResponseDataResponse', 'id'),
              title: BuiltValueNullFieldError.checkNotNull(
                  title, r'ListUserNotificationsResponseDataResponse', 'title'),
              body: BuiltValueNullFieldError.checkNotNull(
                  body, r'ListUserNotificationsResponseDataResponse', 'body'),
              icon: BuiltValueNullFieldError.checkNotNull(
                  icon, r'ListUserNotificationsResponseDataResponse', 'icon'),
              data: data.build(),
              recipientId: BuiltValueNullFieldError.checkNotNull(recipientId,
                  r'ListUserNotificationsResponseDataResponse', 'recipientId'),
              readAt: BuiltValueNullFieldError.checkNotNull(
                  readAt, r'ListUserNotificationsResponseDataResponse', 'readAt'),
              trashedAt: BuiltValueNullFieldError.checkNotNull(
                  trashedAt, r'ListUserNotificationsResponseDataResponse', 'trashedAt'),
              createdAt: BuiltValueNullFieldError.checkNotNull(createdAt, r'ListUserNotificationsResponseDataResponse', 'createdAt'),
              notification: notification.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'data';
        data.build();

        _$failedField = 'notification';
        notification.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ListUserNotificationsResponseDataResponse',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
