//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/success_response.dart';
import 'package:qarar_api/src/model/upload_file_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'core_controller_upload_file200_response.g.dart';

/// My custom description
///
/// Properties:
/// * [success]
/// * [data]
@BuiltValue()
abstract class CoreControllerUploadFile200Response
    implements
        SuccessResponse,
        Built<CoreControllerUploadFile200Response,
            CoreControllerUploadFile200ResponseBuilder> {
  CoreControllerUploadFile200Response._();

  factory CoreControllerUploadFile200Response(
          [void updates(CoreControllerUploadFile200ResponseBuilder b)]) =
      _$CoreControllerUploadFile200Response;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(CoreControllerUploadFile200ResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<CoreControllerUploadFile200Response> get serializer =>
      _$CoreControllerUploadFile200ResponseSerializer();
}

class _$CoreControllerUploadFile200ResponseSerializer
    implements PrimitiveSerializer<CoreControllerUploadFile200Response> {
  @override
  final Iterable<Type> types = const [
    CoreControllerUploadFile200Response,
    _$CoreControllerUploadFile200Response
  ];

  @override
  final String wireName = r'CoreControllerUploadFile200Response';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    CoreControllerUploadFile200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'data';
    yield serializers.serialize(
      object.data,
      specifiedType: const FullType(JsonObject),
    );
    yield r'success';
    yield serializers.serialize(
      object.success,
      specifiedType: const FullType(bool),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    CoreControllerUploadFile200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required CoreControllerUploadFile200ResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'data':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.data = valueDes;
          break;
        case r'success':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.success = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  CoreControllerUploadFile200Response deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = CoreControllerUploadFile200ResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
