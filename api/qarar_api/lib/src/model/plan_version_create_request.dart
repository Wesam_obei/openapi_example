//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'plan_version_create_request.g.dart';

/// PlanVersionCreateRequest
///
/// Properties:
/// * [revisionNumber]
/// * [revisionYear]
/// * [issuedAt]
/// * [fileToken]
@BuiltValue()
abstract class PlanVersionCreateRequest
    implements
        Built<PlanVersionCreateRequest, PlanVersionCreateRequestBuilder> {
  @BuiltValueField(wireName: r'revision_number')
  num get revisionNumber;

  @BuiltValueField(wireName: r'revision_year')
  num get revisionYear;

  @BuiltValueField(wireName: r'issued_at')
  String get issuedAt;

  @BuiltValueField(wireName: r'file_token')
  String get fileToken;

  PlanVersionCreateRequest._();

  factory PlanVersionCreateRequest(
          [void updates(PlanVersionCreateRequestBuilder b)]) =
      _$PlanVersionCreateRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(PlanVersionCreateRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<PlanVersionCreateRequest> get serializer =>
      _$PlanVersionCreateRequestSerializer();
}

class _$PlanVersionCreateRequestSerializer
    implements PrimitiveSerializer<PlanVersionCreateRequest> {
  @override
  final Iterable<Type> types = const [
    PlanVersionCreateRequest,
    _$PlanVersionCreateRequest
  ];

  @override
  final String wireName = r'PlanVersionCreateRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    PlanVersionCreateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'revision_number';
    yield serializers.serialize(
      object.revisionNumber,
      specifiedType: const FullType(num),
    );
    yield r'revision_year';
    yield serializers.serialize(
      object.revisionYear,
      specifiedType: const FullType(num),
    );
    yield r'issued_at';
    yield serializers.serialize(
      object.issuedAt,
      specifiedType: const FullType(String),
    );
    yield r'file_token';
    yield serializers.serialize(
      object.fileToken,
      specifiedType: const FullType(String),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    PlanVersionCreateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required PlanVersionCreateRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'revision_number':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.revisionNumber = valueDes;
          break;
        case r'revision_year':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.revisionYear = valueDes;
          break;
        case r'issued_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.issuedAt = valueDes;
          break;
        case r'file_token':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.fileToken = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  PlanVersionCreateRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = PlanVersionCreateRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
