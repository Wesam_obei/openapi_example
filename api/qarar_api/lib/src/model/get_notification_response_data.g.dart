// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_notification_response_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetNotificationResponseData extends GetNotificationResponseData {
  @override
  final num id;
  @override
  final GetNotificationTypeResponseData notificationType;

  factory _$GetNotificationResponseData(
          [void Function(GetNotificationResponseDataBuilder)? updates]) =>
      (new GetNotificationResponseDataBuilder()..update(updates))._build();

  _$GetNotificationResponseData._(
      {required this.id, required this.notificationType})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'GetNotificationResponseData', 'id');
    BuiltValueNullFieldError.checkNotNull(
        notificationType, r'GetNotificationResponseData', 'notificationType');
  }

  @override
  GetNotificationResponseData rebuild(
          void Function(GetNotificationResponseDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetNotificationResponseDataBuilder toBuilder() =>
      new GetNotificationResponseDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetNotificationResponseData &&
        id == other.id &&
        notificationType == other.notificationType;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, notificationType.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetNotificationResponseData')
          ..add('id', id)
          ..add('notificationType', notificationType))
        .toString();
  }
}

class GetNotificationResponseDataBuilder
    implements
        Builder<GetNotificationResponseData,
            GetNotificationResponseDataBuilder> {
  _$GetNotificationResponseData? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  GetNotificationTypeResponseDataBuilder? _notificationType;
  GetNotificationTypeResponseDataBuilder get notificationType =>
      _$this._notificationType ??= new GetNotificationTypeResponseDataBuilder();
  set notificationType(
          GetNotificationTypeResponseDataBuilder? notificationType) =>
      _$this._notificationType = notificationType;

  GetNotificationResponseDataBuilder() {
    GetNotificationResponseData._defaults(this);
  }

  GetNotificationResponseDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _notificationType = $v.notificationType.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetNotificationResponseData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetNotificationResponseData;
  }

  @override
  void update(void Function(GetNotificationResponseDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetNotificationResponseData build() => _build();

  _$GetNotificationResponseData _build() {
    _$GetNotificationResponseData _$result;
    try {
      _$result = _$v ??
          new _$GetNotificationResponseData._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'GetNotificationResponseData', 'id'),
              notificationType: notificationType.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'notificationType';
        notificationType.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'GetNotificationResponseData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
