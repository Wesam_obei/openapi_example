//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/permission_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'list_permission_category_data_response.g.dart';

/// ListPermissionCategoryDataResponse
///
/// Properties:
/// * [id]
/// * [name]
/// * [label]
/// * [description]
/// * [order]
/// * [createdAt]
/// * [permissions]
@BuiltValue()
abstract class ListPermissionCategoryDataResponse
    implements
        Built<ListPermissionCategoryDataResponse,
            ListPermissionCategoryDataResponseBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'label')
  String get label;

  @BuiltValueField(wireName: r'description')
  String get description;

  @BuiltValueField(wireName: r'order')
  num get order;

  @BuiltValueField(wireName: r'created_at')
  DateTime get createdAt;

  @BuiltValueField(wireName: r'permissions')
  BuiltList<PermissionDataResponse> get permissions;

  ListPermissionCategoryDataResponse._();

  factory ListPermissionCategoryDataResponse(
          [void updates(ListPermissionCategoryDataResponseBuilder b)]) =
      _$ListPermissionCategoryDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ListPermissionCategoryDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ListPermissionCategoryDataResponse> get serializer =>
      _$ListPermissionCategoryDataResponseSerializer();
}

class _$ListPermissionCategoryDataResponseSerializer
    implements PrimitiveSerializer<ListPermissionCategoryDataResponse> {
  @override
  final Iterable<Type> types = const [
    ListPermissionCategoryDataResponse,
    _$ListPermissionCategoryDataResponse
  ];

  @override
  final String wireName = r'ListPermissionCategoryDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ListPermissionCategoryDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    yield r'label';
    yield serializers.serialize(
      object.label,
      specifiedType: const FullType(String),
    );
    yield r'description';
    yield serializers.serialize(
      object.description,
      specifiedType: const FullType(String),
    );
    yield r'order';
    yield serializers.serialize(
      object.order,
      specifiedType: const FullType(num),
    );
    yield r'created_at';
    yield serializers.serialize(
      object.createdAt,
      specifiedType: const FullType(DateTime),
    );
    yield r'permissions';
    yield serializers.serialize(
      object.permissions,
      specifiedType:
          const FullType(BuiltList, [FullType(PermissionDataResponse)]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ListPermissionCategoryDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ListPermissionCategoryDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'label':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.label = valueDes;
          break;
        case r'description':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.description = valueDes;
          break;
        case r'order':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.order = valueDes;
          break;
        case r'created_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.createdAt = valueDes;
          break;
        case r'permissions':
          final valueDes = serializers.deserialize(
            value,
            specifiedType:
                const FullType(BuiltList, [FullType(PermissionDataResponse)]),
          ) as BuiltList<PermissionDataResponse>;
          result.permissions.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ListPermissionCategoryDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ListPermissionCategoryDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
