//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/permissions_enum.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'permission_data_response.g.dart';

/// PermissionDataResponse
///
/// Properties:
/// * [id]
/// * [name]
/// * [label]
/// * [description]
/// * [order]
/// * [createdAt]
@BuiltValue()
abstract class PermissionDataResponse
    implements Built<PermissionDataResponse, PermissionDataResponseBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'name')
  PermissionsEnum get name;
  // enum nameEnum {  create_user,  view_user,  update_user,  delete_user,  create_role,  view_role,  update_role,  delete_role,  edit_user_dashboard,  view_user_dashboard,  delete_user_dashboard,  view_maater_layer,  view_incidents940_layer,  view_construction_license_layer,  view_light_poles_layer,  view_cables_layer,  view_electrical_facilities_layer,  view_bridges_tunnels_sites_layer,  view_store_points_layer,  view_response_layer,  view_critical_points_layer,  view_transport_track_layer,  view_street_naming_layer,  view_municipality_boundary_layer,  view_land_base_parcel_layer,  view_district_boundary_layer,  view_urban_area_boundary_layer,  view_plan_data_layer,  view_manhole_layer,  view_sea_out_layer,  create_plan,  view_plan,  update_plan,  delete_plan,  create_equipment_store_point,  view_equipment_store_point,  update_equipment_store_point,  delete_equipment_store_point,  create_equipment_type,  view_equipment_type,  update_equipment_type,  delete_equipment_type,  create_equipment,  view_equipment,  update_equipment,  delete_equipment,  create_form,  update_form,  delete_form,  view_form,  create_survey,  update_survey,  delete_survey,  view_survey,  respond_survey_for_entity,  survey_verified_responses,  create_entity,  update_entity,  delete_entity,  view_entity,  };

  @BuiltValueField(wireName: r'label')
  String get label;

  @BuiltValueField(wireName: r'description')
  String get description;

  @BuiltValueField(wireName: r'order')
  num get order;

  @BuiltValueField(wireName: r'created_at')
  DateTime get createdAt;

  PermissionDataResponse._();

  factory PermissionDataResponse(
          [void updates(PermissionDataResponseBuilder b)]) =
      _$PermissionDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(PermissionDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<PermissionDataResponse> get serializer =>
      _$PermissionDataResponseSerializer();
}

class _$PermissionDataResponseSerializer
    implements PrimitiveSerializer<PermissionDataResponse> {
  @override
  final Iterable<Type> types = const [
    PermissionDataResponse,
    _$PermissionDataResponse
  ];

  @override
  final String wireName = r'PermissionDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    PermissionDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(PermissionsEnum),
    );
    yield r'label';
    yield serializers.serialize(
      object.label,
      specifiedType: const FullType(String),
    );
    yield r'description';
    yield serializers.serialize(
      object.description,
      specifiedType: const FullType(String),
    );
    yield r'order';
    yield serializers.serialize(
      object.order,
      specifiedType: const FullType(num),
    );
    yield r'created_at';
    yield serializers.serialize(
      object.createdAt,
      specifiedType: const FullType(DateTime),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    PermissionDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required PermissionDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(PermissionsEnum),
          ) as PermissionsEnum;
          result.name = valueDes;
          break;
        case r'label':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.label = valueDes;
          break;
        case r'description':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.description = valueDes;
          break;
        case r'order':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.order = valueDes;
          break;
        case r'created_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.createdAt = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  PermissionDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = PermissionDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
