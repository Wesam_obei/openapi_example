// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_form_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ListFormDataResponse extends ListFormDataResponse {
  @override
  final num id;
  @override
  final String name;
  @override
  final String description;
  @override
  final bool isActive;
  @override
  final bool editable;
  @override
  final DateTime createdAt;
  @override
  final GetFileDataResponse file;
  @override
  final GetUserDataResponse creator;
  @override
  final BuiltList<GetFormComponentDataResponse> children;

  factory _$ListFormDataResponse(
          [void Function(ListFormDataResponseBuilder)? updates]) =>
      (new ListFormDataResponseBuilder()..update(updates))._build();

  _$ListFormDataResponse._(
      {required this.id,
      required this.name,
      required this.description,
      required this.isActive,
      required this.editable,
      required this.createdAt,
      required this.file,
      required this.creator,
      required this.children})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, r'ListFormDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, r'ListFormDataResponse', 'name');
    BuiltValueNullFieldError.checkNotNull(
        description, r'ListFormDataResponse', 'description');
    BuiltValueNullFieldError.checkNotNull(
        isActive, r'ListFormDataResponse', 'isActive');
    BuiltValueNullFieldError.checkNotNull(
        editable, r'ListFormDataResponse', 'editable');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'ListFormDataResponse', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        file, r'ListFormDataResponse', 'file');
    BuiltValueNullFieldError.checkNotNull(
        creator, r'ListFormDataResponse', 'creator');
    BuiltValueNullFieldError.checkNotNull(
        children, r'ListFormDataResponse', 'children');
  }

  @override
  ListFormDataResponse rebuild(
          void Function(ListFormDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ListFormDataResponseBuilder toBuilder() =>
      new ListFormDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ListFormDataResponse &&
        id == other.id &&
        name == other.name &&
        description == other.description &&
        isActive == other.isActive &&
        editable == other.editable &&
        createdAt == other.createdAt &&
        file == other.file &&
        creator == other.creator &&
        children == other.children;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, isActive.hashCode);
    _$hash = $jc(_$hash, editable.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, file.hashCode);
    _$hash = $jc(_$hash, creator.hashCode);
    _$hash = $jc(_$hash, children.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ListFormDataResponse')
          ..add('id', id)
          ..add('name', name)
          ..add('description', description)
          ..add('isActive', isActive)
          ..add('editable', editable)
          ..add('createdAt', createdAt)
          ..add('file', file)
          ..add('creator', creator)
          ..add('children', children))
        .toString();
  }
}

class ListFormDataResponseBuilder
    implements Builder<ListFormDataResponse, ListFormDataResponseBuilder> {
  _$ListFormDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  bool? _isActive;
  bool? get isActive => _$this._isActive;
  set isActive(bool? isActive) => _$this._isActive = isActive;

  bool? _editable;
  bool? get editable => _$this._editable;
  set editable(bool? editable) => _$this._editable = editable;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  GetFileDataResponseBuilder? _file;
  GetFileDataResponseBuilder get file =>
      _$this._file ??= new GetFileDataResponseBuilder();
  set file(GetFileDataResponseBuilder? file) => _$this._file = file;

  GetUserDataResponseBuilder? _creator;
  GetUserDataResponseBuilder get creator =>
      _$this._creator ??= new GetUserDataResponseBuilder();
  set creator(GetUserDataResponseBuilder? creator) => _$this._creator = creator;

  ListBuilder<GetFormComponentDataResponse>? _children;
  ListBuilder<GetFormComponentDataResponse> get children =>
      _$this._children ??= new ListBuilder<GetFormComponentDataResponse>();
  set children(ListBuilder<GetFormComponentDataResponse>? children) =>
      _$this._children = children;

  ListFormDataResponseBuilder() {
    ListFormDataResponse._defaults(this);
  }

  ListFormDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _description = $v.description;
      _isActive = $v.isActive;
      _editable = $v.editable;
      _createdAt = $v.createdAt;
      _file = $v.file.toBuilder();
      _creator = $v.creator.toBuilder();
      _children = $v.children.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ListFormDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ListFormDataResponse;
  }

  @override
  void update(void Function(ListFormDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ListFormDataResponse build() => _build();

  _$ListFormDataResponse _build() {
    _$ListFormDataResponse _$result;
    try {
      _$result = _$v ??
          new _$ListFormDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'ListFormDataResponse', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'ListFormDataResponse', 'name'),
              description: BuiltValueNullFieldError.checkNotNull(
                  description, r'ListFormDataResponse', 'description'),
              isActive: BuiltValueNullFieldError.checkNotNull(
                  isActive, r'ListFormDataResponse', 'isActive'),
              editable: BuiltValueNullFieldError.checkNotNull(
                  editable, r'ListFormDataResponse', 'editable'),
              createdAt: BuiltValueNullFieldError.checkNotNull(
                  createdAt, r'ListFormDataResponse', 'createdAt'),
              file: file.build(),
              creator: creator.build(),
              children: children.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'file';
        file.build();
        _$failedField = 'creator';
        creator.build();
        _$failedField = 'children';
        children.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ListFormDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
