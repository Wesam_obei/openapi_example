//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'dart:core';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:one_of/any_of.dart';

part 'map_layers_controller_get_incident_reports_map200_response_all_of_data_features_inner_properties_id.g.dart';

/// MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId
@BuiltValue()
abstract class MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId
    implements
        Built<
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId,
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder> {
  /// Any Of [String], [bool], [num]
  AnyOf get anyOf;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId._();

  factory MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId(
          [void updates(
              MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
                  b)]) =
      _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
              b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId>
      get serializer =>
          _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdSerializer();
}

class _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId,
    _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId
        object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {}

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId
        object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final anyOf = object.anyOf;
    return serializers.serialize(anyOf,
        specifiedType: FullType(
            AnyOf, anyOf.valueTypes.map((type) => FullType(type)).toList()))!;
  }

  @override
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId
      deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
    Object? anyOfDataSrc;
    final targetType = const FullType(AnyOf, [
      FullType(String),
      FullType(num),
      FullType(bool),
    ]);
    anyOfDataSrc = serialized;
    result.anyOf = serializers.deserialize(anyOfDataSrc,
        specifiedType: targetType) as AnyOf;
    return result.build();
  }
}
