// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_employee_user_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ListEmployeeUserDataResponse extends ListEmployeeUserDataResponse {
  @override
  final num id;
  @override
  final String email;
  @override
  final String fullName;
  @override
  final String jobTitle;
  @override
  final num employeeId;

  factory _$ListEmployeeUserDataResponse(
          [void Function(ListEmployeeUserDataResponseBuilder)? updates]) =>
      (new ListEmployeeUserDataResponseBuilder()..update(updates))._build();

  _$ListEmployeeUserDataResponse._(
      {required this.id,
      required this.email,
      required this.fullName,
      required this.jobTitle,
      required this.employeeId})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'ListEmployeeUserDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        email, r'ListEmployeeUserDataResponse', 'email');
    BuiltValueNullFieldError.checkNotNull(
        fullName, r'ListEmployeeUserDataResponse', 'fullName');
    BuiltValueNullFieldError.checkNotNull(
        jobTitle, r'ListEmployeeUserDataResponse', 'jobTitle');
    BuiltValueNullFieldError.checkNotNull(
        employeeId, r'ListEmployeeUserDataResponse', 'employeeId');
  }

  @override
  ListEmployeeUserDataResponse rebuild(
          void Function(ListEmployeeUserDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ListEmployeeUserDataResponseBuilder toBuilder() =>
      new ListEmployeeUserDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ListEmployeeUserDataResponse &&
        id == other.id &&
        email == other.email &&
        fullName == other.fullName &&
        jobTitle == other.jobTitle &&
        employeeId == other.employeeId;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, email.hashCode);
    _$hash = $jc(_$hash, fullName.hashCode);
    _$hash = $jc(_$hash, jobTitle.hashCode);
    _$hash = $jc(_$hash, employeeId.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ListEmployeeUserDataResponse')
          ..add('id', id)
          ..add('email', email)
          ..add('fullName', fullName)
          ..add('jobTitle', jobTitle)
          ..add('employeeId', employeeId))
        .toString();
  }
}

class ListEmployeeUserDataResponseBuilder
    implements
        Builder<ListEmployeeUserDataResponse,
            ListEmployeeUserDataResponseBuilder> {
  _$ListEmployeeUserDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _email;
  String? get email => _$this._email;
  set email(String? email) => _$this._email = email;

  String? _fullName;
  String? get fullName => _$this._fullName;
  set fullName(String? fullName) => _$this._fullName = fullName;

  String? _jobTitle;
  String? get jobTitle => _$this._jobTitle;
  set jobTitle(String? jobTitle) => _$this._jobTitle = jobTitle;

  num? _employeeId;
  num? get employeeId => _$this._employeeId;
  set employeeId(num? employeeId) => _$this._employeeId = employeeId;

  ListEmployeeUserDataResponseBuilder() {
    ListEmployeeUserDataResponse._defaults(this);
  }

  ListEmployeeUserDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _email = $v.email;
      _fullName = $v.fullName;
      _jobTitle = $v.jobTitle;
      _employeeId = $v.employeeId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ListEmployeeUserDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ListEmployeeUserDataResponse;
  }

  @override
  void update(void Function(ListEmployeeUserDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ListEmployeeUserDataResponse build() => _build();

  _$ListEmployeeUserDataResponse _build() {
    final _$result = _$v ??
        new _$ListEmployeeUserDataResponse._(
            id: BuiltValueNullFieldError.checkNotNull(
                id, r'ListEmployeeUserDataResponse', 'id'),
            email: BuiltValueNullFieldError.checkNotNull(
                email, r'ListEmployeeUserDataResponse', 'email'),
            fullName: BuiltValueNullFieldError.checkNotNull(
                fullName, r'ListEmployeeUserDataResponse', 'fullName'),
            jobTitle: BuiltValueNullFieldError.checkNotNull(
                jobTitle, r'ListEmployeeUserDataResponse', 'jobTitle'),
            employeeId: BuiltValueNullFieldError.checkNotNull(
                employeeId, r'ListEmployeeUserDataResponse', 'employeeId'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
