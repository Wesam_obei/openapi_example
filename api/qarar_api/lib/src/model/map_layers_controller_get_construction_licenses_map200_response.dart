//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/success_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_construction_licenses_map200_response_all_of_data.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_construction_licenses_map200_response.g.dart';

/// My custom description
///
/// Properties:
/// * [success]
/// * [data]
@BuiltValue()
abstract class MapLayersControllerGetConstructionLicensesMap200Response
    implements
        SuccessResponse,
        Built<MapLayersControllerGetConstructionLicensesMap200Response,
            MapLayersControllerGetConstructionLicensesMap200ResponseBuilder> {
  MapLayersControllerGetConstructionLicensesMap200Response._();

  factory MapLayersControllerGetConstructionLicensesMap200Response(
      [void updates(
          MapLayersControllerGetConstructionLicensesMap200ResponseBuilder
              b)]) = _$MapLayersControllerGetConstructionLicensesMap200Response;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetConstructionLicensesMap200ResponseBuilder b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<MapLayersControllerGetConstructionLicensesMap200Response>
      get serializer =>
          _$MapLayersControllerGetConstructionLicensesMap200ResponseSerializer();
}

class _$MapLayersControllerGetConstructionLicensesMap200ResponseSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetConstructionLicensesMap200Response> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetConstructionLicensesMap200Response,
    _$MapLayersControllerGetConstructionLicensesMap200Response
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetConstructionLicensesMap200Response';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetConstructionLicensesMap200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'data';
    yield serializers.serialize(
      object.data,
      specifiedType: const FullType(JsonObject),
    );
    yield r'success';
    yield serializers.serialize(
      object.success,
      specifiedType: const FullType(bool),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetConstructionLicensesMap200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetConstructionLicensesMap200ResponseBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'data':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.data = valueDes;
          break;
        case r'success':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.success = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetConstructionLicensesMap200Response deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetConstructionLicensesMap200ResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
