//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/get_equipment_type_data_response.dart';
import 'package:qarar_api/src/model/get_entity_data_response.dart';
import 'package:qarar_api/src/model/get_equipment_store_point_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'get_equipment_data_response.g.dart';

/// GetEquipmentDataResponse
///
/// Properties:
/// * [id]
/// * [brand]
/// * [model]
/// * [plateNumber]
/// * [vinNumber]
/// * [localNumber]
/// * [serialNumber]
/// * [manufacturingYear]
/// * [registrationExpiryDate]
/// * [ownershipDate]
/// * [createdAt]
/// * [entity]
/// * [equipmentsType]
/// * [equipmentsStorePoint]
@BuiltValue()
abstract class GetEquipmentDataResponse
    implements
        Built<GetEquipmentDataResponse, GetEquipmentDataResponseBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'brand')
  String get brand;

  @BuiltValueField(wireName: r'model')
  String get model;

  @BuiltValueField(wireName: r'plate_number')
  String get plateNumber;

  @BuiltValueField(wireName: r'vin_number')
  String get vinNumber;

  @BuiltValueField(wireName: r'local_number')
  num get localNumber;

  @BuiltValueField(wireName: r'serial_number')
  num get serialNumber;

  @BuiltValueField(wireName: r'manufacturing_year')
  num get manufacturingYear;

  @BuiltValueField(wireName: r'registration_expiry_date')
  DateTime get registrationExpiryDate;

  @BuiltValueField(wireName: r'ownership_date')
  DateTime get ownershipDate;

  @BuiltValueField(wireName: r'created_at')
  DateTime get createdAt;

  @BuiltValueField(wireName: r'entity')
  GetEntityDataResponse get entity;

  @BuiltValueField(wireName: r'equipmentsType')
  GetEquipmentTypeDataResponse get equipmentsType;

  @BuiltValueField(wireName: r'equipmentsStorePoint')
  GetEquipmentStorePointDataResponse get equipmentsStorePoint;

  GetEquipmentDataResponse._();

  factory GetEquipmentDataResponse(
          [void updates(GetEquipmentDataResponseBuilder b)]) =
      _$GetEquipmentDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(GetEquipmentDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<GetEquipmentDataResponse> get serializer =>
      _$GetEquipmentDataResponseSerializer();
}

class _$GetEquipmentDataResponseSerializer
    implements PrimitiveSerializer<GetEquipmentDataResponse> {
  @override
  final Iterable<Type> types = const [
    GetEquipmentDataResponse,
    _$GetEquipmentDataResponse
  ];

  @override
  final String wireName = r'GetEquipmentDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    GetEquipmentDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'brand';
    yield serializers.serialize(
      object.brand,
      specifiedType: const FullType(String),
    );
    yield r'model';
    yield serializers.serialize(
      object.model,
      specifiedType: const FullType(String),
    );
    yield r'plate_number';
    yield serializers.serialize(
      object.plateNumber,
      specifiedType: const FullType(String),
    );
    yield r'vin_number';
    yield serializers.serialize(
      object.vinNumber,
      specifiedType: const FullType(String),
    );
    yield r'local_number';
    yield serializers.serialize(
      object.localNumber,
      specifiedType: const FullType(num),
    );
    yield r'serial_number';
    yield serializers.serialize(
      object.serialNumber,
      specifiedType: const FullType(num),
    );
    yield r'manufacturing_year';
    yield serializers.serialize(
      object.manufacturingYear,
      specifiedType: const FullType(num),
    );
    yield r'registration_expiry_date';
    yield serializers.serialize(
      object.registrationExpiryDate,
      specifiedType: const FullType(DateTime),
    );
    yield r'ownership_date';
    yield serializers.serialize(
      object.ownershipDate,
      specifiedType: const FullType(DateTime),
    );
    yield r'created_at';
    yield serializers.serialize(
      object.createdAt,
      specifiedType: const FullType(DateTime),
    );
    yield r'entity';
    yield serializers.serialize(
      object.entity,
      specifiedType: const FullType(GetEntityDataResponse),
    );
    yield r'equipmentsType';
    yield serializers.serialize(
      object.equipmentsType,
      specifiedType: const FullType(GetEquipmentTypeDataResponse),
    );
    yield r'equipmentsStorePoint';
    yield serializers.serialize(
      object.equipmentsStorePoint,
      specifiedType: const FullType(GetEquipmentStorePointDataResponse),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    GetEquipmentDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required GetEquipmentDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'brand':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.brand = valueDes;
          break;
        case r'model':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.model = valueDes;
          break;
        case r'plate_number':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.plateNumber = valueDes;
          break;
        case r'vin_number':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.vinNumber = valueDes;
          break;
        case r'local_number':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.localNumber = valueDes;
          break;
        case r'serial_number':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.serialNumber = valueDes;
          break;
        case r'manufacturing_year':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.manufacturingYear = valueDes;
          break;
        case r'registration_expiry_date':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.registrationExpiryDate = valueDes;
          break;
        case r'ownership_date':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.ownershipDate = valueDes;
          break;
        case r'created_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.createdAt = valueDes;
          break;
        case r'entity':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetEntityDataResponse),
          ) as GetEntityDataResponse;
          result.entity.replace(valueDes);
          break;
        case r'equipmentsType':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetEquipmentTypeDataResponse),
          ) as GetEquipmentTypeDataResponse;
          result.equipmentsType.replace(valueDes);
          break;
        case r'equipmentsStorePoint':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetEquipmentStorePointDataResponse),
          ) as GetEquipmentStorePointDataResponse;
          result.equipmentsStorePoint.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  GetEquipmentDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = GetEquipmentDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
