// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_storage_locations_map200_response_all_of_data_features_inner.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum
    _$mapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature =
    const MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum
        ._('feature');

MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum
    _$mapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'feature':
      return _$mapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature;
    default:
      return _$mapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature;
  }
}

final BuiltSet<
        MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum>
    _$mapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum>(const <MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum>[
  _$mapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature,
]);

Serializer<
        MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum>
    _$mapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer =
    new _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer();

class _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'feature': 'Feature',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'Feature': 'feature',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum';

  @override
  Object serialize(
          Serializers serializers,
          MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum
              object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum
      deserialize(Serializers serializers, Object serialized,
              {FullType specifiedType = FullType.unspecified}) =>
          MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .valueOf(_fromWire[serialized] ??
                  (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner
    extends MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner {
  @override
  final MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum
      type;
  @override
  final int id;
  @override
  final JsonObject? geometry;
  @override
  final JsonObject properties;

  factory _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner(
          [void Function(
                  MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerBuilder)?
              updates]) =>
      (new MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner._(
      {required this.type,
      required this.id,
      this.geometry,
      required this.properties})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        type,
        r'MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner',
        'type');
    BuiltValueNullFieldError.checkNotNull(
        id,
        r'MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner',
        'id');
    BuiltValueNullFieldError.checkNotNull(
        properties,
        r'MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner',
        'properties');
  }

  @override
  MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner
      rebuild(
              void Function(
                      MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerBuilder)
                  updates) =>
          (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerBuilder
      toBuilder() =>
          new MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner &&
        type == other.type &&
        id == other.id &&
        geometry == other.geometry &&
        properties == other.properties;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, geometry.hashCode);
    _$hash = $jc(_$hash, properties.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner')
          ..add('type', type)
          ..add('id', id)
          ..add('geometry', geometry)
          ..add('properties', properties))
        .toString();
  }
}

class MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerBuilder
    implements
        Builder<
            MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner,
            MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerBuilder> {
  _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner?
      _$v;

  MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum?
      _type;
  MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum?
      get type => _$this._type;
  set type(
          MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerTypeEnum?
              type) =>
      _$this._type = type;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  JsonObject? _geometry;
  JsonObject? get geometry => _$this._geometry;
  set geometry(JsonObject? geometry) => _$this._geometry = geometry;

  JsonObject? _properties;
  JsonObject? get properties => _$this._properties;
  set properties(JsonObject? properties) => _$this._properties = properties;

  MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerBuilder() {
    MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner
        ._defaults(this);
  }

  MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _id = $v.id;
      _geometry = $v.geometry;
      _properties = $v.properties;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner
      build() => _build();

  _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner
      _build() {
    final _$result = _$v ??
        new _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner._(
            type: BuiltValueNullFieldError.checkNotNull(
                type,
                r'MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner',
                'type'),
            id: BuiltValueNullFieldError.checkNotNull(
                id,
                r'MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner',
                'id'),
            geometry: geometry,
            properties: BuiltValueNullFieldError.checkNotNull(
                properties,
                r'MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner',
                'properties'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
