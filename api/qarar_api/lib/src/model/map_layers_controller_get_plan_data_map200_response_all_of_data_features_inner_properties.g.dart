// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_plan_data_map200_response_all_of_data_features_inner_properties.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties
    extends MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties {
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      id;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      municipalityName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      subMunicipalityName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      planNo;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      planName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      planPkNo;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      districtName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      ownerId;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      approvalStatus;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      planHasServices;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      planApprovedBy;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      planLatCoord;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      planLongCoord;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      oldPlanNo;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      mapObjectId;

  factory _$MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties(
          [void Function(
                  MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
              updates]) =>
      (new MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties._(
      {this.id,
      this.municipalityName,
      this.subMunicipalityName,
      this.planNo,
      this.planName,
      this.planPkNo,
      this.districtName,
      this.ownerId,
      this.approvalStatus,
      this.planHasServices,
      this.planApprovedBy,
      this.planLatCoord,
      this.planLongCoord,
      this.oldPlanNo,
      this.mapObjectId})
      : super._();

  @override
  MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties
      rebuild(
              void Function(
                      MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)
                  updates) =>
          (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      toBuilder() =>
          new MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties &&
        id == other.id &&
        municipalityName == other.municipalityName &&
        subMunicipalityName == other.subMunicipalityName &&
        planNo == other.planNo &&
        planName == other.planName &&
        planPkNo == other.planPkNo &&
        districtName == other.districtName &&
        ownerId == other.ownerId &&
        approvalStatus == other.approvalStatus &&
        planHasServices == other.planHasServices &&
        planApprovedBy == other.planApprovedBy &&
        planLatCoord == other.planLatCoord &&
        planLongCoord == other.planLongCoord &&
        oldPlanNo == other.oldPlanNo &&
        mapObjectId == other.mapObjectId;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, municipalityName.hashCode);
    _$hash = $jc(_$hash, subMunicipalityName.hashCode);
    _$hash = $jc(_$hash, planNo.hashCode);
    _$hash = $jc(_$hash, planName.hashCode);
    _$hash = $jc(_$hash, planPkNo.hashCode);
    _$hash = $jc(_$hash, districtName.hashCode);
    _$hash = $jc(_$hash, ownerId.hashCode);
    _$hash = $jc(_$hash, approvalStatus.hashCode);
    _$hash = $jc(_$hash, planHasServices.hashCode);
    _$hash = $jc(_$hash, planApprovedBy.hashCode);
    _$hash = $jc(_$hash, planLatCoord.hashCode);
    _$hash = $jc(_$hash, planLongCoord.hashCode);
    _$hash = $jc(_$hash, oldPlanNo.hashCode);
    _$hash = $jc(_$hash, mapObjectId.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties')
          ..add('id', id)
          ..add('municipalityName', municipalityName)
          ..add('subMunicipalityName', subMunicipalityName)
          ..add('planNo', planNo)
          ..add('planName', planName)
          ..add('planPkNo', planPkNo)
          ..add('districtName', districtName)
          ..add('ownerId', ownerId)
          ..add('approvalStatus', approvalStatus)
          ..add('planHasServices', planHasServices)
          ..add('planApprovedBy', planApprovedBy)
          ..add('planLatCoord', planLatCoord)
          ..add('planLongCoord', planLongCoord)
          ..add('oldPlanNo', oldPlanNo)
          ..add('mapObjectId', mapObjectId))
        .toString();
  }
}

class MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
    implements
        Builder<
            MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties,
            MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder> {
  _$MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties?
      _$v;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _id;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get id => _$this._id ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set id(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              id) =>
      _$this._id = id;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _municipalityName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get municipalityName => _$this._municipalityName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set municipalityName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              municipalityName) =>
      _$this._municipalityName = municipalityName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _subMunicipalityName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get subMunicipalityName => _$this._subMunicipalityName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set subMunicipalityName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              subMunicipalityName) =>
      _$this._subMunicipalityName = subMunicipalityName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _planNo;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get planNo => _$this._planNo ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set planNo(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              planNo) =>
      _$this._planNo = planNo;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _planName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get planName => _$this._planName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set planName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              planName) =>
      _$this._planName = planName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _planPkNo;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get planPkNo => _$this._planPkNo ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set planPkNo(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              planPkNo) =>
      _$this._planPkNo = planPkNo;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _districtName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get districtName => _$this._districtName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set districtName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              districtName) =>
      _$this._districtName = districtName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _ownerId;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get ownerId => _$this._ownerId ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set ownerId(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              ownerId) =>
      _$this._ownerId = ownerId;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _approvalStatus;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get approvalStatus => _$this._approvalStatus ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set approvalStatus(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              approvalStatus) =>
      _$this._approvalStatus = approvalStatus;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _planHasServices;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get planHasServices => _$this._planHasServices ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set planHasServices(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              planHasServices) =>
      _$this._planHasServices = planHasServices;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _planApprovedBy;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get planApprovedBy => _$this._planApprovedBy ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set planApprovedBy(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              planApprovedBy) =>
      _$this._planApprovedBy = planApprovedBy;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _planLatCoord;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get planLatCoord => _$this._planLatCoord ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set planLatCoord(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              planLatCoord) =>
      _$this._planLatCoord = planLatCoord;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _planLongCoord;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get planLongCoord => _$this._planLongCoord ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set planLongCoord(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              planLongCoord) =>
      _$this._planLongCoord = planLongCoord;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _oldPlanNo;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get oldPlanNo => _$this._oldPlanNo ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set oldPlanNo(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              oldPlanNo) =>
      _$this._oldPlanNo = oldPlanNo;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _mapObjectId;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get mapObjectId => _$this._mapObjectId ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set mapObjectId(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              mapObjectId) =>
      _$this._mapObjectId = mapObjectId;

  MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder() {
    MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties
        ._defaults(this);
  }

  MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id?.toBuilder();
      _municipalityName = $v.municipalityName?.toBuilder();
      _subMunicipalityName = $v.subMunicipalityName?.toBuilder();
      _planNo = $v.planNo?.toBuilder();
      _planName = $v.planName?.toBuilder();
      _planPkNo = $v.planPkNo?.toBuilder();
      _districtName = $v.districtName?.toBuilder();
      _ownerId = $v.ownerId?.toBuilder();
      _approvalStatus = $v.approvalStatus?.toBuilder();
      _planHasServices = $v.planHasServices?.toBuilder();
      _planApprovedBy = $v.planApprovedBy?.toBuilder();
      _planLatCoord = $v.planLatCoord?.toBuilder();
      _planLongCoord = $v.planLongCoord?.toBuilder();
      _oldPlanNo = $v.oldPlanNo?.toBuilder();
      _mapObjectId = $v.mapObjectId?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties
      build() => _build();

  _$MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties
      _build() {
    _$MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties
        _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties
              ._(
              id: _id?.build(),
              municipalityName: _municipalityName?.build(),
              subMunicipalityName: _subMunicipalityName?.build(),
              planNo: _planNo?.build(),
              planName: _planName?.build(),
              planPkNo: _planPkNo?.build(),
              districtName: _districtName?.build(),
              ownerId: _ownerId?.build(),
              approvalStatus: _approvalStatus?.build(),
              planHasServices: _planHasServices?.build(),
              planApprovedBy: _planApprovedBy?.build(),
              planLatCoord: _planLatCoord?.build(),
              planLongCoord: _planLongCoord?.build(),
              oldPlanNo: _oldPlanNo?.build(),
              mapObjectId: _mapObjectId?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'id';
        _id?.build();
        _$failedField = 'municipalityName';
        _municipalityName?.build();
        _$failedField = 'subMunicipalityName';
        _subMunicipalityName?.build();
        _$failedField = 'planNo';
        _planNo?.build();
        _$failedField = 'planName';
        _planName?.build();
        _$failedField = 'planPkNo';
        _planPkNo?.build();
        _$failedField = 'districtName';
        _districtName?.build();
        _$failedField = 'ownerId';
        _ownerId?.build();
        _$failedField = 'approvalStatus';
        _approvalStatus?.build();
        _$failedField = 'planHasServices';
        _planHasServices?.build();
        _$failedField = 'planApprovedBy';
        _planApprovedBy?.build();
        _$failedField = 'planLatCoord';
        _planLatCoord?.build();
        _$failedField = 'planLongCoord';
        _planLongCoord?.build();
        _$failedField = 'oldPlanNo';
        _oldPlanNo?.build();
        _$failedField = 'mapObjectId';
        _mapObjectId?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
