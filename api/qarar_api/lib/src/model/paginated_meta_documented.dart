//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/sorting_by_columns_inner_inner.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'paginated_meta_documented.g.dart';

/// PaginatedMetaDocumented
///
/// Properties:
/// * [itemsPerPage]
/// * [totalItems]
/// * [currentPage]
/// * [totalPages]
/// * [sortBy]
/// * [searchBy]
/// * [search]
/// * [select]
/// * [filter]
/// * [includes]
@BuiltValue()
abstract class PaginatedMetaDocumented
    implements Built<PaginatedMetaDocumented, PaginatedMetaDocumentedBuilder> {
  @BuiltValueField(wireName: r'itemsPerPage')
  num get itemsPerPage;

  @BuiltValueField(wireName: r'totalItems')
  num get totalItems;

  @BuiltValueField(wireName: r'currentPage')
  num get currentPage;

  @BuiltValueField(wireName: r'totalPages')
  num get totalPages;

  @BuiltValueField(wireName: r'sortBy')
  BuiltList<BuiltList<SortingByColumnsInnerInner>>? get sortBy;

  @BuiltValueField(wireName: r'searchBy')
  BuiltList<String>? get searchBy;

  @BuiltValueField(wireName: r'search')
  String? get search;

  @BuiltValueField(wireName: r'select')
  BuiltList<String>? get select;

  @BuiltValueField(wireName: r'filter')
  JsonObject? get filter;

  @BuiltValueField(wireName: r'includes')
  BuiltList<String>? get includes;

  PaginatedMetaDocumented._();

  factory PaginatedMetaDocumented(
          [void updates(PaginatedMetaDocumentedBuilder b)]) =
      _$PaginatedMetaDocumented;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(PaginatedMetaDocumentedBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<PaginatedMetaDocumented> get serializer =>
      _$PaginatedMetaDocumentedSerializer();
}

class _$PaginatedMetaDocumentedSerializer
    implements PrimitiveSerializer<PaginatedMetaDocumented> {
  @override
  final Iterable<Type> types = const [
    PaginatedMetaDocumented,
    _$PaginatedMetaDocumented
  ];

  @override
  final String wireName = r'PaginatedMetaDocumented';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    PaginatedMetaDocumented object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'itemsPerPage';
    yield serializers.serialize(
      object.itemsPerPage,
      specifiedType: const FullType(num),
    );
    yield r'totalItems';
    yield serializers.serialize(
      object.totalItems,
      specifiedType: const FullType(num),
    );
    yield r'currentPage';
    yield serializers.serialize(
      object.currentPage,
      specifiedType: const FullType(num),
    );
    yield r'totalPages';
    yield serializers.serialize(
      object.totalPages,
      specifiedType: const FullType(num),
    );
    if (object.sortBy != null) {
      yield r'sortBy';
      yield serializers.serialize(
        object.sortBy,
        specifiedType: const FullType(BuiltList, [
          FullType(BuiltList, [FullType(SortingByColumnsInnerInner)])
        ]),
      );
    }
    if (object.searchBy != null) {
      yield r'searchBy';
      yield serializers.serialize(
        object.searchBy,
        specifiedType: const FullType(BuiltList, [FullType(String)]),
      );
    }
    if (object.search != null) {
      yield r'search';
      yield serializers.serialize(
        object.search,
        specifiedType: const FullType(String),
      );
    }
    if (object.select != null) {
      yield r'select';
      yield serializers.serialize(
        object.select,
        specifiedType: const FullType(BuiltList, [FullType(String)]),
      );
    }
    if (object.filter != null) {
      yield r'filter';
      yield serializers.serialize(
        object.filter,
        specifiedType: const FullType(JsonObject),
      );
    }
    if (object.includes != null) {
      yield r'includes';
      yield serializers.serialize(
        object.includes,
        specifiedType: const FullType(BuiltList, [FullType(String)]),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    PaginatedMetaDocumented object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required PaginatedMetaDocumentedBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'itemsPerPage':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.itemsPerPage = valueDes;
          break;
        case r'totalItems':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.totalItems = valueDes;
          break;
        case r'currentPage':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.currentPage = valueDes;
          break;
        case r'totalPages':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.totalPages = valueDes;
          break;
        case r'sortBy':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [
              FullType(BuiltList, [FullType(SortingByColumnsInnerInner)])
            ]),
          ) as BuiltList<BuiltList<SortingByColumnsInnerInner>>;
          result.sortBy.replace(valueDes);
          break;
        case r'searchBy':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(String)]),
          ) as BuiltList<String>;
          result.searchBy.replace(valueDes);
          break;
        case r'search':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.search = valueDes;
          break;
        case r'select':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(String)]),
          ) as BuiltList<String>;
          result.select.replace(valueDes);
          break;
        case r'filter':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.filter = valueDes;
          break;
        case r'includes':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(String)]),
          ) as BuiltList<String>;
          result.includes.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  PaginatedMetaDocumented deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = PaginatedMetaDocumentedBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
