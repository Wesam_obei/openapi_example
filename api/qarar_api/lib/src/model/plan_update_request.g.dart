// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'plan_update_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PlanUpdateRequest extends PlanUpdateRequest {
  @override
  final String name;
  @override
  final String description;

  factory _$PlanUpdateRequest(
          [void Function(PlanUpdateRequestBuilder)? updates]) =>
      (new PlanUpdateRequestBuilder()..update(updates))._build();

  _$PlanUpdateRequest._({required this.name, required this.description})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(name, r'PlanUpdateRequest', 'name');
    BuiltValueNullFieldError.checkNotNull(
        description, r'PlanUpdateRequest', 'description');
  }

  @override
  PlanUpdateRequest rebuild(void Function(PlanUpdateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PlanUpdateRequestBuilder toBuilder() =>
      new PlanUpdateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PlanUpdateRequest &&
        name == other.name &&
        description == other.description;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'PlanUpdateRequest')
          ..add('name', name)
          ..add('description', description))
        .toString();
  }
}

class PlanUpdateRequestBuilder
    implements Builder<PlanUpdateRequest, PlanUpdateRequestBuilder> {
  _$PlanUpdateRequest? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  PlanUpdateRequestBuilder() {
    PlanUpdateRequest._defaults(this);
  }

  PlanUpdateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _description = $v.description;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PlanUpdateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$PlanUpdateRequest;
  }

  @override
  void update(void Function(PlanUpdateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  PlanUpdateRequest build() => _build();

  _$PlanUpdateRequest _build() {
    final _$result = _$v ??
        new _$PlanUpdateRequest._(
            name: BuiltValueNullFieldError.checkNotNull(
                name, r'PlanUpdateRequest', 'name'),
            description: BuiltValueNullFieldError.checkNotNull(
                description, r'PlanUpdateRequest', 'description'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
