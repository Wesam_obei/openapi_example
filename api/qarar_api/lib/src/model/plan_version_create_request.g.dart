// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'plan_version_create_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PlanVersionCreateRequest extends PlanVersionCreateRequest {
  @override
  final num revisionNumber;
  @override
  final num revisionYear;
  @override
  final String issuedAt;
  @override
  final String fileToken;

  factory _$PlanVersionCreateRequest(
          [void Function(PlanVersionCreateRequestBuilder)? updates]) =>
      (new PlanVersionCreateRequestBuilder()..update(updates))._build();

  _$PlanVersionCreateRequest._(
      {required this.revisionNumber,
      required this.revisionYear,
      required this.issuedAt,
      required this.fileToken})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        revisionNumber, r'PlanVersionCreateRequest', 'revisionNumber');
    BuiltValueNullFieldError.checkNotNull(
        revisionYear, r'PlanVersionCreateRequest', 'revisionYear');
    BuiltValueNullFieldError.checkNotNull(
        issuedAt, r'PlanVersionCreateRequest', 'issuedAt');
    BuiltValueNullFieldError.checkNotNull(
        fileToken, r'PlanVersionCreateRequest', 'fileToken');
  }

  @override
  PlanVersionCreateRequest rebuild(
          void Function(PlanVersionCreateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PlanVersionCreateRequestBuilder toBuilder() =>
      new PlanVersionCreateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PlanVersionCreateRequest &&
        revisionNumber == other.revisionNumber &&
        revisionYear == other.revisionYear &&
        issuedAt == other.issuedAt &&
        fileToken == other.fileToken;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, revisionNumber.hashCode);
    _$hash = $jc(_$hash, revisionYear.hashCode);
    _$hash = $jc(_$hash, issuedAt.hashCode);
    _$hash = $jc(_$hash, fileToken.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'PlanVersionCreateRequest')
          ..add('revisionNumber', revisionNumber)
          ..add('revisionYear', revisionYear)
          ..add('issuedAt', issuedAt)
          ..add('fileToken', fileToken))
        .toString();
  }
}

class PlanVersionCreateRequestBuilder
    implements
        Builder<PlanVersionCreateRequest, PlanVersionCreateRequestBuilder> {
  _$PlanVersionCreateRequest? _$v;

  num? _revisionNumber;
  num? get revisionNumber => _$this._revisionNumber;
  set revisionNumber(num? revisionNumber) =>
      _$this._revisionNumber = revisionNumber;

  num? _revisionYear;
  num? get revisionYear => _$this._revisionYear;
  set revisionYear(num? revisionYear) => _$this._revisionYear = revisionYear;

  String? _issuedAt;
  String? get issuedAt => _$this._issuedAt;
  set issuedAt(String? issuedAt) => _$this._issuedAt = issuedAt;

  String? _fileToken;
  String? get fileToken => _$this._fileToken;
  set fileToken(String? fileToken) => _$this._fileToken = fileToken;

  PlanVersionCreateRequestBuilder() {
    PlanVersionCreateRequest._defaults(this);
  }

  PlanVersionCreateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _revisionNumber = $v.revisionNumber;
      _revisionYear = $v.revisionYear;
      _issuedAt = $v.issuedAt;
      _fileToken = $v.fileToken;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PlanVersionCreateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$PlanVersionCreateRequest;
  }

  @override
  void update(void Function(PlanVersionCreateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  PlanVersionCreateRequest build() => _build();

  _$PlanVersionCreateRequest _build() {
    final _$result = _$v ??
        new _$PlanVersionCreateRequest._(
            revisionNumber: BuiltValueNullFieldError.checkNotNull(
                revisionNumber, r'PlanVersionCreateRequest', 'revisionNumber'),
            revisionYear: BuiltValueNullFieldError.checkNotNull(
                revisionYear, r'PlanVersionCreateRequest', 'revisionYear'),
            issuedAt: BuiltValueNullFieldError.checkNotNull(
                issuedAt, r'PlanVersionCreateRequest', 'issuedAt'),
            fileToken: BuiltValueNullFieldError.checkNotNull(
                fileToken, r'PlanVersionCreateRequest', 'fileToken'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
