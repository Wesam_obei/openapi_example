//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_land_base_parcel_map200_response_all_of_data_features_inner.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_land_base_parcel_map200_response_all_of_data.g.dart';

/// MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData
///
/// Properties:
/// * [type]
/// * [features]
@BuiltValue()
abstract class MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData
    implements
        Built<MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData,
            MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataBuilder> {
  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum get type;
  // enum typeEnum {  FeatureCollection,  };

  @BuiltValueField(wireName: r'features')
  BuiltList<
          MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner>
      get features;

  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData._();

  factory MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData(
          [void updates(
              MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataBuilder
                  b)]) =
      _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataBuilder
              b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData>
      get serializer =>
          _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataSerializer();
}

class _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData,
    _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(
          MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum),
    );
    yield r'features';
    yield serializers.serialize(
      object.features,
      specifiedType: const FullType(BuiltList, [
        FullType(
            MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner)
      ]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum),
          ) as MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum;
          result.type = valueDes;
          break;
        case r'features':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [
              FullType(
                  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner)
            ]),
          ) as BuiltList<
              MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInner>;
          result.features.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfData deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum
    extends EnumClass {
  @BuiltValueEnumConst(wireName: r'FeatureCollection', fallback: true)
  static const MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum
      featureCollection =
      _$mapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum_featureCollection;

  static Serializer<
          MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum>
      get serializer =>
          _$mapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnumSerializer;

  const MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum._(
      String name)
      : super(name);

  static BuiltSet<
          MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum>
      get values =>
          _$mapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnumValues;
  static MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnum
      valueOf(String name) =>
          _$mapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataTypeEnumValueOf(
              name);
}
