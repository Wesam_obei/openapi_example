// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_survey_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetSurveyDataResponse extends GetSurveyDataResponse {
  @override
  final num id;
  @override
  final String name;
  @override
  final String description;
  @override
  final bool isActive;
  @override
  final DateTime startAt;
  @override
  final DateTime endAt;
  @override
  final DateTime createdAt;
  @override
  final num numberOfResponses;
  @override
  final num numberOfRecipients;
  @override
  final GetUserDataResponse creator;
  @override
  final GetFormDataResponse form;
  @override
  final BuiltList<GetSurveyRecipientViewDataResponse> recipients;

  factory _$GetSurveyDataResponse(
          [void Function(GetSurveyDataResponseBuilder)? updates]) =>
      (new GetSurveyDataResponseBuilder()..update(updates))._build();

  _$GetSurveyDataResponse._(
      {required this.id,
      required this.name,
      required this.description,
      required this.isActive,
      required this.startAt,
      required this.endAt,
      required this.createdAt,
      required this.numberOfResponses,
      required this.numberOfRecipients,
      required this.creator,
      required this.form,
      required this.recipients})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, r'GetSurveyDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, r'GetSurveyDataResponse', 'name');
    BuiltValueNullFieldError.checkNotNull(
        description, r'GetSurveyDataResponse', 'description');
    BuiltValueNullFieldError.checkNotNull(
        isActive, r'GetSurveyDataResponse', 'isActive');
    BuiltValueNullFieldError.checkNotNull(
        startAt, r'GetSurveyDataResponse', 'startAt');
    BuiltValueNullFieldError.checkNotNull(
        endAt, r'GetSurveyDataResponse', 'endAt');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'GetSurveyDataResponse', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        numberOfResponses, r'GetSurveyDataResponse', 'numberOfResponses');
    BuiltValueNullFieldError.checkNotNull(
        numberOfRecipients, r'GetSurveyDataResponse', 'numberOfRecipients');
    BuiltValueNullFieldError.checkNotNull(
        creator, r'GetSurveyDataResponse', 'creator');
    BuiltValueNullFieldError.checkNotNull(
        form, r'GetSurveyDataResponse', 'form');
    BuiltValueNullFieldError.checkNotNull(
        recipients, r'GetSurveyDataResponse', 'recipients');
  }

  @override
  GetSurveyDataResponse rebuild(
          void Function(GetSurveyDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetSurveyDataResponseBuilder toBuilder() =>
      new GetSurveyDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetSurveyDataResponse &&
        id == other.id &&
        name == other.name &&
        description == other.description &&
        isActive == other.isActive &&
        startAt == other.startAt &&
        endAt == other.endAt &&
        createdAt == other.createdAt &&
        numberOfResponses == other.numberOfResponses &&
        numberOfRecipients == other.numberOfRecipients &&
        creator == other.creator &&
        form == other.form &&
        recipients == other.recipients;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, isActive.hashCode);
    _$hash = $jc(_$hash, startAt.hashCode);
    _$hash = $jc(_$hash, endAt.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, numberOfResponses.hashCode);
    _$hash = $jc(_$hash, numberOfRecipients.hashCode);
    _$hash = $jc(_$hash, creator.hashCode);
    _$hash = $jc(_$hash, form.hashCode);
    _$hash = $jc(_$hash, recipients.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetSurveyDataResponse')
          ..add('id', id)
          ..add('name', name)
          ..add('description', description)
          ..add('isActive', isActive)
          ..add('startAt', startAt)
          ..add('endAt', endAt)
          ..add('createdAt', createdAt)
          ..add('numberOfResponses', numberOfResponses)
          ..add('numberOfRecipients', numberOfRecipients)
          ..add('creator', creator)
          ..add('form', form)
          ..add('recipients', recipients))
        .toString();
  }
}

class GetSurveyDataResponseBuilder
    implements Builder<GetSurveyDataResponse, GetSurveyDataResponseBuilder> {
  _$GetSurveyDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  bool? _isActive;
  bool? get isActive => _$this._isActive;
  set isActive(bool? isActive) => _$this._isActive = isActive;

  DateTime? _startAt;
  DateTime? get startAt => _$this._startAt;
  set startAt(DateTime? startAt) => _$this._startAt = startAt;

  DateTime? _endAt;
  DateTime? get endAt => _$this._endAt;
  set endAt(DateTime? endAt) => _$this._endAt = endAt;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  num? _numberOfResponses;
  num? get numberOfResponses => _$this._numberOfResponses;
  set numberOfResponses(num? numberOfResponses) =>
      _$this._numberOfResponses = numberOfResponses;

  num? _numberOfRecipients;
  num? get numberOfRecipients => _$this._numberOfRecipients;
  set numberOfRecipients(num? numberOfRecipients) =>
      _$this._numberOfRecipients = numberOfRecipients;

  GetUserDataResponseBuilder? _creator;
  GetUserDataResponseBuilder get creator =>
      _$this._creator ??= new GetUserDataResponseBuilder();
  set creator(GetUserDataResponseBuilder? creator) => _$this._creator = creator;

  GetFormDataResponseBuilder? _form;
  GetFormDataResponseBuilder get form =>
      _$this._form ??= new GetFormDataResponseBuilder();
  set form(GetFormDataResponseBuilder? form) => _$this._form = form;

  ListBuilder<GetSurveyRecipientViewDataResponse>? _recipients;
  ListBuilder<GetSurveyRecipientViewDataResponse> get recipients =>
      _$this._recipients ??=
          new ListBuilder<GetSurveyRecipientViewDataResponse>();
  set recipients(ListBuilder<GetSurveyRecipientViewDataResponse>? recipients) =>
      _$this._recipients = recipients;

  GetSurveyDataResponseBuilder() {
    GetSurveyDataResponse._defaults(this);
  }

  GetSurveyDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _description = $v.description;
      _isActive = $v.isActive;
      _startAt = $v.startAt;
      _endAt = $v.endAt;
      _createdAt = $v.createdAt;
      _numberOfResponses = $v.numberOfResponses;
      _numberOfRecipients = $v.numberOfRecipients;
      _creator = $v.creator.toBuilder();
      _form = $v.form.toBuilder();
      _recipients = $v.recipients.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetSurveyDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetSurveyDataResponse;
  }

  @override
  void update(void Function(GetSurveyDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetSurveyDataResponse build() => _build();

  _$GetSurveyDataResponse _build() {
    _$GetSurveyDataResponse _$result;
    try {
      _$result = _$v ??
          new _$GetSurveyDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'GetSurveyDataResponse', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'GetSurveyDataResponse', 'name'),
              description: BuiltValueNullFieldError.checkNotNull(
                  description, r'GetSurveyDataResponse', 'description'),
              isActive: BuiltValueNullFieldError.checkNotNull(
                  isActive, r'GetSurveyDataResponse', 'isActive'),
              startAt: BuiltValueNullFieldError.checkNotNull(
                  startAt, r'GetSurveyDataResponse', 'startAt'),
              endAt: BuiltValueNullFieldError.checkNotNull(
                  endAt, r'GetSurveyDataResponse', 'endAt'),
              createdAt: BuiltValueNullFieldError.checkNotNull(
                  createdAt, r'GetSurveyDataResponse', 'createdAt'),
              numberOfResponses: BuiltValueNullFieldError.checkNotNull(
                  numberOfResponses, r'GetSurveyDataResponse', 'numberOfResponses'),
              numberOfRecipients:
                  BuiltValueNullFieldError.checkNotNull(numberOfRecipients, r'GetSurveyDataResponse', 'numberOfRecipients'),
              creator: creator.build(),
              form: form.build(),
              recipients: recipients.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'creator';
        creator.build();
        _$failedField = 'form';
        form.build();
        _$failedField = 'recipients';
        recipients.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'GetSurveyDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
