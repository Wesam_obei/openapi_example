// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'survey_request_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SurveyRequestDataResponse extends SurveyRequestDataResponse {
  @override
  final num id;
  @override
  final String name;
  @override
  final String description;
  @override
  final bool isActive;
  @override
  final DateTime startAt;
  @override
  final DateTime endAt;
  @override
  final DateTime createdAt;
  @override
  final GetSurveyResponseDataResponse response;
  @override
  final bool isDelayed;
  @override
  final num recipientId;
  @override
  final String recipientName;
  @override
  final RecipientType recipientType;
  @override
  final GetSurveyDataResponse survey;

  factory _$SurveyRequestDataResponse(
          [void Function(SurveyRequestDataResponseBuilder)? updates]) =>
      (new SurveyRequestDataResponseBuilder()..update(updates))._build();

  _$SurveyRequestDataResponse._(
      {required this.id,
      required this.name,
      required this.description,
      required this.isActive,
      required this.startAt,
      required this.endAt,
      required this.createdAt,
      required this.response,
      required this.isDelayed,
      required this.recipientId,
      required this.recipientName,
      required this.recipientType,
      required this.survey})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'SurveyRequestDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, r'SurveyRequestDataResponse', 'name');
    BuiltValueNullFieldError.checkNotNull(
        description, r'SurveyRequestDataResponse', 'description');
    BuiltValueNullFieldError.checkNotNull(
        isActive, r'SurveyRequestDataResponse', 'isActive');
    BuiltValueNullFieldError.checkNotNull(
        startAt, r'SurveyRequestDataResponse', 'startAt');
    BuiltValueNullFieldError.checkNotNull(
        endAt, r'SurveyRequestDataResponse', 'endAt');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'SurveyRequestDataResponse', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        response, r'SurveyRequestDataResponse', 'response');
    BuiltValueNullFieldError.checkNotNull(
        isDelayed, r'SurveyRequestDataResponse', 'isDelayed');
    BuiltValueNullFieldError.checkNotNull(
        recipientId, r'SurveyRequestDataResponse', 'recipientId');
    BuiltValueNullFieldError.checkNotNull(
        recipientName, r'SurveyRequestDataResponse', 'recipientName');
    BuiltValueNullFieldError.checkNotNull(
        recipientType, r'SurveyRequestDataResponse', 'recipientType');
    BuiltValueNullFieldError.checkNotNull(
        survey, r'SurveyRequestDataResponse', 'survey');
  }

  @override
  SurveyRequestDataResponse rebuild(
          void Function(SurveyRequestDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SurveyRequestDataResponseBuilder toBuilder() =>
      new SurveyRequestDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SurveyRequestDataResponse &&
        id == other.id &&
        name == other.name &&
        description == other.description &&
        isActive == other.isActive &&
        startAt == other.startAt &&
        endAt == other.endAt &&
        createdAt == other.createdAt &&
        response == other.response &&
        isDelayed == other.isDelayed &&
        recipientId == other.recipientId &&
        recipientName == other.recipientName &&
        recipientType == other.recipientType &&
        survey == other.survey;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, isActive.hashCode);
    _$hash = $jc(_$hash, startAt.hashCode);
    _$hash = $jc(_$hash, endAt.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, response.hashCode);
    _$hash = $jc(_$hash, isDelayed.hashCode);
    _$hash = $jc(_$hash, recipientId.hashCode);
    _$hash = $jc(_$hash, recipientName.hashCode);
    _$hash = $jc(_$hash, recipientType.hashCode);
    _$hash = $jc(_$hash, survey.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SurveyRequestDataResponse')
          ..add('id', id)
          ..add('name', name)
          ..add('description', description)
          ..add('isActive', isActive)
          ..add('startAt', startAt)
          ..add('endAt', endAt)
          ..add('createdAt', createdAt)
          ..add('response', response)
          ..add('isDelayed', isDelayed)
          ..add('recipientId', recipientId)
          ..add('recipientName', recipientName)
          ..add('recipientType', recipientType)
          ..add('survey', survey))
        .toString();
  }
}

class SurveyRequestDataResponseBuilder
    implements
        Builder<SurveyRequestDataResponse, SurveyRequestDataResponseBuilder> {
  _$SurveyRequestDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  bool? _isActive;
  bool? get isActive => _$this._isActive;
  set isActive(bool? isActive) => _$this._isActive = isActive;

  DateTime? _startAt;
  DateTime? get startAt => _$this._startAt;
  set startAt(DateTime? startAt) => _$this._startAt = startAt;

  DateTime? _endAt;
  DateTime? get endAt => _$this._endAt;
  set endAt(DateTime? endAt) => _$this._endAt = endAt;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  GetSurveyResponseDataResponseBuilder? _response;
  GetSurveyResponseDataResponseBuilder get response =>
      _$this._response ??= new GetSurveyResponseDataResponseBuilder();
  set response(GetSurveyResponseDataResponseBuilder? response) =>
      _$this._response = response;

  bool? _isDelayed;
  bool? get isDelayed => _$this._isDelayed;
  set isDelayed(bool? isDelayed) => _$this._isDelayed = isDelayed;

  num? _recipientId;
  num? get recipientId => _$this._recipientId;
  set recipientId(num? recipientId) => _$this._recipientId = recipientId;

  String? _recipientName;
  String? get recipientName => _$this._recipientName;
  set recipientName(String? recipientName) =>
      _$this._recipientName = recipientName;

  RecipientType? _recipientType;
  RecipientType? get recipientType => _$this._recipientType;
  set recipientType(RecipientType? recipientType) =>
      _$this._recipientType = recipientType;

  GetSurveyDataResponseBuilder? _survey;
  GetSurveyDataResponseBuilder get survey =>
      _$this._survey ??= new GetSurveyDataResponseBuilder();
  set survey(GetSurveyDataResponseBuilder? survey) => _$this._survey = survey;

  SurveyRequestDataResponseBuilder() {
    SurveyRequestDataResponse._defaults(this);
  }

  SurveyRequestDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _description = $v.description;
      _isActive = $v.isActive;
      _startAt = $v.startAt;
      _endAt = $v.endAt;
      _createdAt = $v.createdAt;
      _response = $v.response.toBuilder();
      _isDelayed = $v.isDelayed;
      _recipientId = $v.recipientId;
      _recipientName = $v.recipientName;
      _recipientType = $v.recipientType;
      _survey = $v.survey.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SurveyRequestDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SurveyRequestDataResponse;
  }

  @override
  void update(void Function(SurveyRequestDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SurveyRequestDataResponse build() => _build();

  _$SurveyRequestDataResponse _build() {
    _$SurveyRequestDataResponse _$result;
    try {
      _$result = _$v ??
          new _$SurveyRequestDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'SurveyRequestDataResponse', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'SurveyRequestDataResponse', 'name'),
              description: BuiltValueNullFieldError.checkNotNull(
                  description, r'SurveyRequestDataResponse', 'description'),
              isActive: BuiltValueNullFieldError.checkNotNull(
                  isActive, r'SurveyRequestDataResponse', 'isActive'),
              startAt: BuiltValueNullFieldError.checkNotNull(
                  startAt, r'SurveyRequestDataResponse', 'startAt'),
              endAt: BuiltValueNullFieldError.checkNotNull(
                  endAt, r'SurveyRequestDataResponse', 'endAt'),
              createdAt: BuiltValueNullFieldError.checkNotNull(
                  createdAt, r'SurveyRequestDataResponse', 'createdAt'),
              response: response.build(),
              isDelayed: BuiltValueNullFieldError.checkNotNull(
                  isDelayed, r'SurveyRequestDataResponse', 'isDelayed'),
              recipientId: BuiltValueNullFieldError.checkNotNull(recipientId, r'SurveyRequestDataResponse', 'recipientId'),
              recipientName: BuiltValueNullFieldError.checkNotNull(recipientName, r'SurveyRequestDataResponse', 'recipientName'),
              recipientType: BuiltValueNullFieldError.checkNotNull(recipientType, r'SurveyRequestDataResponse', 'recipientType'),
              survey: survey.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'response';
        response.build();

        _$failedField = 'survey';
        survey.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'SurveyRequestDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
