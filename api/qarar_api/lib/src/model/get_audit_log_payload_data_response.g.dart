// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_audit_log_payload_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetAuditLogPayloadDataResponse extends GetAuditLogPayloadDataResponse {
  @override
  final GetAuditLogRequestDataResponse request;

  factory _$GetAuditLogPayloadDataResponse(
          [void Function(GetAuditLogPayloadDataResponseBuilder)? updates]) =>
      (new GetAuditLogPayloadDataResponseBuilder()..update(updates))._build();

  _$GetAuditLogPayloadDataResponse._({required this.request}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        request, r'GetAuditLogPayloadDataResponse', 'request');
  }

  @override
  GetAuditLogPayloadDataResponse rebuild(
          void Function(GetAuditLogPayloadDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetAuditLogPayloadDataResponseBuilder toBuilder() =>
      new GetAuditLogPayloadDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetAuditLogPayloadDataResponse && request == other.request;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, request.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetAuditLogPayloadDataResponse')
          ..add('request', request))
        .toString();
  }
}

class GetAuditLogPayloadDataResponseBuilder
    implements
        Builder<GetAuditLogPayloadDataResponse,
            GetAuditLogPayloadDataResponseBuilder> {
  _$GetAuditLogPayloadDataResponse? _$v;

  GetAuditLogRequestDataResponseBuilder? _request;
  GetAuditLogRequestDataResponseBuilder get request =>
      _$this._request ??= new GetAuditLogRequestDataResponseBuilder();
  set request(GetAuditLogRequestDataResponseBuilder? request) =>
      _$this._request = request;

  GetAuditLogPayloadDataResponseBuilder() {
    GetAuditLogPayloadDataResponse._defaults(this);
  }

  GetAuditLogPayloadDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _request = $v.request.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetAuditLogPayloadDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetAuditLogPayloadDataResponse;
  }

  @override
  void update(void Function(GetAuditLogPayloadDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetAuditLogPayloadDataResponse build() => _build();

  _$GetAuditLogPayloadDataResponse _build() {
    _$GetAuditLogPayloadDataResponse _$result;
    try {
      _$result = _$v ??
          new _$GetAuditLogPayloadDataResponse._(request: request.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'request';
        request.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'GetAuditLogPayloadDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
