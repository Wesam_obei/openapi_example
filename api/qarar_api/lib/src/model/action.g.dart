// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'action.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$Action extends Action {
  @override
  final String type;
  @override
  final String link;
  @override
  final num objectId;
  @override
  final String module;

  factory _$Action([void Function(ActionBuilder)? updates]) =>
      (new ActionBuilder()..update(updates))._build();

  _$Action._(
      {required this.type,
      required this.link,
      required this.objectId,
      required this.module})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(type, r'Action', 'type');
    BuiltValueNullFieldError.checkNotNull(link, r'Action', 'link');
    BuiltValueNullFieldError.checkNotNull(objectId, r'Action', 'objectId');
    BuiltValueNullFieldError.checkNotNull(module, r'Action', 'module');
  }

  @override
  Action rebuild(void Function(ActionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ActionBuilder toBuilder() => new ActionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Action &&
        type == other.type &&
        link == other.link &&
        objectId == other.objectId &&
        module == other.module;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, link.hashCode);
    _$hash = $jc(_$hash, objectId.hashCode);
    _$hash = $jc(_$hash, module.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Action')
          ..add('type', type)
          ..add('link', link)
          ..add('objectId', objectId)
          ..add('module', module))
        .toString();
  }
}

class ActionBuilder implements Builder<Action, ActionBuilder> {
  _$Action? _$v;

  String? _type;
  String? get type => _$this._type;
  set type(String? type) => _$this._type = type;

  String? _link;
  String? get link => _$this._link;
  set link(String? link) => _$this._link = link;

  num? _objectId;
  num? get objectId => _$this._objectId;
  set objectId(num? objectId) => _$this._objectId = objectId;

  String? _module;
  String? get module => _$this._module;
  set module(String? module) => _$this._module = module;

  ActionBuilder() {
    Action._defaults(this);
  }

  ActionBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _link = $v.link;
      _objectId = $v.objectId;
      _module = $v.module;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Action other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Action;
  }

  @override
  void update(void Function(ActionBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Action build() => _build();

  _$Action _build() {
    final _$result = _$v ??
        new _$Action._(
            type:
                BuiltValueNullFieldError.checkNotNull(type, r'Action', 'type'),
            link:
                BuiltValueNullFieldError.checkNotNull(link, r'Action', 'link'),
            objectId: BuiltValueNullFieldError.checkNotNull(
                objectId, r'Action', 'objectId'),
            module: BuiltValueNullFieldError.checkNotNull(
                module, r'Action', 'module'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
