// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_urban_area_boundaries_map200_response_all_of_data_features_inner.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum
    _$mapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature =
    const MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum
        ._('feature');

MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum
    _$mapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'feature':
      return _$mapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature;
    default:
      return _$mapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature;
  }
}

final BuiltSet<
        MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum>
    _$mapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum>(const <MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum>[
  _$mapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature,
]);

Serializer<
        MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum>
    _$mapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer =
    new _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer();

class _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'feature': 'Feature',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'Feature': 'feature',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum';

  @override
  Object serialize(
          Serializers serializers,
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum
              object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum
      deserialize(Serializers serializers, Object serialized,
              {FullType specifiedType = FullType.unspecified}) =>
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .valueOf(_fromWire[serialized] ??
                  (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner
    extends MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner {
  @override
  final MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum
      type;
  @override
  final int id;
  @override
  final JsonObject? geometry;
  @override
  final MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
      properties;

  factory _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner(
          [void Function(
                  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerBuilder)?
              updates]) =>
      (new MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner._(
      {required this.type,
      required this.id,
      this.geometry,
      required this.properties})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        type,
        r'MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner',
        'type');
    BuiltValueNullFieldError.checkNotNull(
        id,
        r'MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner',
        'id');
    BuiltValueNullFieldError.checkNotNull(
        properties,
        r'MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner',
        'properties');
  }

  @override
  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner
      rebuild(
              void Function(
                      MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerBuilder)
                  updates) =>
          (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerBuilder
      toBuilder() =>
          new MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner &&
        type == other.type &&
        id == other.id &&
        geometry == other.geometry &&
        properties == other.properties;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, geometry.hashCode);
    _$hash = $jc(_$hash, properties.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner')
          ..add('type', type)
          ..add('id', id)
          ..add('geometry', geometry)
          ..add('properties', properties))
        .toString();
  }
}

class MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerBuilder
    implements
        Builder<
            MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner,
            MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerBuilder> {
  _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner?
      _$v;

  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum?
      _type;
  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum?
      get type => _$this._type;
  set type(
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum?
              type) =>
      _$this._type = type;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  JsonObject? _geometry;
  JsonObject? get geometry => _$this._geometry;
  set geometry(JsonObject? geometry) => _$this._geometry = geometry;

  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder?
      _properties;
  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      get properties => _$this._properties ??=
          new MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder();
  set properties(
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder?
              properties) =>
      _$this._properties = properties;

  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerBuilder() {
    MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner
        ._defaults(this);
  }

  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _id = $v.id;
      _geometry = $v.geometry;
      _properties = $v.properties.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner
      build() => _build();

  _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner
      _build() {
    _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner
        _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner',
                  'type'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id,
                  r'MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner',
                  'id'),
              geometry: geometry,
              properties: properties.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'properties';
        properties.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
