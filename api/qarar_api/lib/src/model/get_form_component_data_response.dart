//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/get_user_data_response.dart';
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/get_form_component_response_data_type.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'get_form_component_data_response.g.dart';

/// GetFormComponentDataResponse
///
/// Properties:
/// * [id]
/// * [label]
/// * [placeholder]
/// * [description]
/// * [isRequired]
/// * [isActive]
/// * [responseDataTypeId]
/// * [parentComponentId]
/// * [isArray]
/// * [isGroup]
/// * [allowMultipleResponse]
/// * [order]
/// * [config]
/// * [createdAt]
/// * [creator]
/// * [responseDataType]
/// * [children]
@BuiltValue()
abstract class GetFormComponentDataResponse
    implements
        Built<GetFormComponentDataResponse,
            GetFormComponentDataResponseBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'label')
  String get label;

  @BuiltValueField(wireName: r'placeholder')
  String? get placeholder;

  @BuiltValueField(wireName: r'description')
  String get description;

  @BuiltValueField(wireName: r'is_required')
  bool get isRequired;

  @BuiltValueField(wireName: r'is_active')
  bool get isActive;

  @BuiltValueField(wireName: r'response_data_type_id')
  num get responseDataTypeId;

  @BuiltValueField(wireName: r'parent_component_id')
  num get parentComponentId;

  @BuiltValueField(wireName: r'is_array')
  bool get isArray;

  @BuiltValueField(wireName: r'is_group')
  bool get isGroup;

  @BuiltValueField(wireName: r'allow_multiple_response')
  bool get allowMultipleResponse;

  @BuiltValueField(wireName: r'order')
  num get order;

  @BuiltValueField(wireName: r'config')
  JsonObject get config;

  @BuiltValueField(wireName: r'created_at')
  DateTime get createdAt;

  @BuiltValueField(wireName: r'creator')
  GetUserDataResponse get creator;

  @BuiltValueField(wireName: r'responseDataType')
  GetFormComponentResponseDataType get responseDataType;

  @BuiltValueField(wireName: r'children')
  BuiltList<GetFormComponentDataResponse> get children;

  GetFormComponentDataResponse._();

  factory GetFormComponentDataResponse(
          [void updates(GetFormComponentDataResponseBuilder b)]) =
      _$GetFormComponentDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(GetFormComponentDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<GetFormComponentDataResponse> get serializer =>
      _$GetFormComponentDataResponseSerializer();
}

class _$GetFormComponentDataResponseSerializer
    implements PrimitiveSerializer<GetFormComponentDataResponse> {
  @override
  final Iterable<Type> types = const [
    GetFormComponentDataResponse,
    _$GetFormComponentDataResponse
  ];

  @override
  final String wireName = r'GetFormComponentDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    GetFormComponentDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'label';
    yield serializers.serialize(
      object.label,
      specifiedType: const FullType(String),
    );
    yield r'placeholder';
    yield object.placeholder == null
        ? null
        : serializers.serialize(
            object.placeholder,
            specifiedType: const FullType.nullable(String),
          );
    yield r'description';
    yield serializers.serialize(
      object.description,
      specifiedType: const FullType(String),
    );
    yield r'is_required';
    yield serializers.serialize(
      object.isRequired,
      specifiedType: const FullType(bool),
    );
    yield r'is_active';
    yield serializers.serialize(
      object.isActive,
      specifiedType: const FullType(bool),
    );
    yield r'response_data_type_id';
    yield serializers.serialize(
      object.responseDataTypeId,
      specifiedType: const FullType(num),
    );
    yield r'parent_component_id';
    yield serializers.serialize(
      object.parentComponentId,
      specifiedType: const FullType(num),
    );
    yield r'is_array';
    yield serializers.serialize(
      object.isArray,
      specifiedType: const FullType(bool),
    );
    yield r'is_group';
    yield serializers.serialize(
      object.isGroup,
      specifiedType: const FullType(bool),
    );
    yield r'allow_multiple_response';
    yield serializers.serialize(
      object.allowMultipleResponse,
      specifiedType: const FullType(bool),
    );
    yield r'order';
    yield serializers.serialize(
      object.order,
      specifiedType: const FullType(num),
    );
    yield r'config';
    yield serializers.serialize(
      object.config,
      specifiedType: const FullType(JsonObject),
    );
    yield r'created_at';
    yield serializers.serialize(
      object.createdAt,
      specifiedType: const FullType(DateTime),
    );
    yield r'creator';
    yield serializers.serialize(
      object.creator,
      specifiedType: const FullType(GetUserDataResponse),
    );
    yield r'responseDataType';
    yield serializers.serialize(
      object.responseDataType,
      specifiedType: const FullType(GetFormComponentResponseDataType),
    );
    yield r'children';
    yield serializers.serialize(
      object.children,
      specifiedType:
          const FullType(BuiltList, [FullType(GetFormComponentDataResponse)]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    GetFormComponentDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required GetFormComponentDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'label':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.label = valueDes;
          break;
        case r'placeholder':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(String),
          ) as String?;
          if (valueDes == null) continue;
          result.placeholder = valueDes;
          break;
        case r'description':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.description = valueDes;
          break;
        case r'is_required':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.isRequired = valueDes;
          break;
        case r'is_active':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.isActive = valueDes;
          break;
        case r'response_data_type_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.responseDataTypeId = valueDes;
          break;
        case r'parent_component_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.parentComponentId = valueDes;
          break;
        case r'is_array':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.isArray = valueDes;
          break;
        case r'is_group':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.isGroup = valueDes;
          break;
        case r'allow_multiple_response':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.allowMultipleResponse = valueDes;
          break;
        case r'order':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.order = valueDes;
          break;
        case r'config':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.config = valueDes;
          break;
        case r'created_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.createdAt = valueDes;
          break;
        case r'creator':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetUserDataResponse),
          ) as GetUserDataResponse;
          result.creator.replace(valueDes);
          break;
        case r'responseDataType':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetFormComponentResponseDataType),
          ) as GetFormComponentResponseDataType;
          result.responseDataType.replace(valueDes);
          break;
        case r'children':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                BuiltList, [FullType(GetFormComponentDataResponse)]),
          ) as BuiltList<GetFormComponentDataResponse>;
          result.children.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  GetFormComponentDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = GetFormComponentDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
