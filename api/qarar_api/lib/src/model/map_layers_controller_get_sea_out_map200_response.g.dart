// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_sea_out_map200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetSeaOutMap200Response
    extends MapLayersControllerGetSeaOutMap200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$MapLayersControllerGetSeaOutMap200Response(
          [void Function(MapLayersControllerGetSeaOutMap200ResponseBuilder)?
              updates]) =>
      (new MapLayersControllerGetSeaOutMap200ResponseBuilder()..update(updates))
          ._build();

  _$MapLayersControllerGetSeaOutMap200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'MapLayersControllerGetSeaOutMap200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'MapLayersControllerGetSeaOutMap200Response', 'data');
  }

  @override
  MapLayersControllerGetSeaOutMap200Response rebuild(
          void Function(MapLayersControllerGetSeaOutMap200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetSeaOutMap200ResponseBuilder toBuilder() =>
      new MapLayersControllerGetSeaOutMap200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapLayersControllerGetSeaOutMap200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetSeaOutMap200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class MapLayersControllerGetSeaOutMap200ResponseBuilder
    implements
        Builder<MapLayersControllerGetSeaOutMap200Response,
            MapLayersControllerGetSeaOutMap200ResponseBuilder>,
        SuccessResponseBuilder {
  _$MapLayersControllerGetSeaOutMap200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  MapLayersControllerGetSeaOutMap200ResponseBuilder() {
    MapLayersControllerGetSeaOutMap200Response._defaults(this);
  }

  MapLayersControllerGetSeaOutMap200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant MapLayersControllerGetSeaOutMap200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapLayersControllerGetSeaOutMap200Response;
  }

  @override
  void update(
      void Function(MapLayersControllerGetSeaOutMap200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetSeaOutMap200Response build() => _build();

  _$MapLayersControllerGetSeaOutMap200Response _build() {
    final _$result = _$v ??
        new _$MapLayersControllerGetSeaOutMap200Response._(
            success: BuiltValueNullFieldError.checkNotNull(success,
                r'MapLayersControllerGetSeaOutMap200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'MapLayersControllerGetSeaOutMap200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
