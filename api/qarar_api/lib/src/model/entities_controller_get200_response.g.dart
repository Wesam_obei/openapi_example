// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'entities_controller_get200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$EntitiesControllerGet200Response
    extends EntitiesControllerGet200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$EntitiesControllerGet200Response(
          [void Function(EntitiesControllerGet200ResponseBuilder)? updates]) =>
      (new EntitiesControllerGet200ResponseBuilder()..update(updates))._build();

  _$EntitiesControllerGet200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'EntitiesControllerGet200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'EntitiesControllerGet200Response', 'data');
  }

  @override
  EntitiesControllerGet200Response rebuild(
          void Function(EntitiesControllerGet200ResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  EntitiesControllerGet200ResponseBuilder toBuilder() =>
      new EntitiesControllerGet200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EntitiesControllerGet200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'EntitiesControllerGet200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class EntitiesControllerGet200ResponseBuilder
    implements
        Builder<EntitiesControllerGet200Response,
            EntitiesControllerGet200ResponseBuilder>,
        SuccessResponseBuilder {
  _$EntitiesControllerGet200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  EntitiesControllerGet200ResponseBuilder() {
    EntitiesControllerGet200Response._defaults(this);
  }

  EntitiesControllerGet200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant EntitiesControllerGet200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$EntitiesControllerGet200Response;
  }

  @override
  void update(void Function(EntitiesControllerGet200ResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  EntitiesControllerGet200Response build() => _build();

  _$EntitiesControllerGet200Response _build() {
    final _$result = _$v ??
        new _$EntitiesControllerGet200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success, r'EntitiesControllerGet200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'EntitiesControllerGet200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
