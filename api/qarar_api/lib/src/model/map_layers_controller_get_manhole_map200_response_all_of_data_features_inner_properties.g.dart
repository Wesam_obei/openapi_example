// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_manhole_map200_response_all_of_data_features_inner_properties.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties
    extends MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties {
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      id;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      municipalityName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      city;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      district;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      street;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      manholeNo;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      manholeId;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      manholeDepth;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      manholeLaderMaterial;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      manholeCoverMaterial;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      manholeMaterial;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      manholeDiameter;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      manholeInvertElevation;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      ancillaryRole;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      manholeGroundElevation;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      manholeTotalFlow;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      manholeIncomingLineId1;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      manholeIncomingLineInvert1;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      manholeIncomingLineId2;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      manholeIncomingLineInvert2;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      manholeIncomingLineId3;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      manholeIncomingLineInvert3;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      manholeOutgoingLineId;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      manholeOutgoingLineInvert;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      manholeType;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      enabled;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      upstreamStructureId;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      downstreamStructureId;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      groundElevation;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      description;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      designDate;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      designCompany;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      constructionDate;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      constructionCompany;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      inServiceDate;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      lifeCycleStatus;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      remarks;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      gl;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      il;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      diameter;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      lastEditedUser;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      lastEditedDate;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      createdUser;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      createdDate;

  factory _$MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties(
          [void Function(
                  MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
              updates]) =>
      (new MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties._(
      {this.id,
      this.municipalityName,
      this.city,
      this.district,
      this.street,
      this.manholeNo,
      this.manholeId,
      this.manholeDepth,
      this.manholeLaderMaterial,
      this.manholeCoverMaterial,
      this.manholeMaterial,
      this.manholeDiameter,
      this.manholeInvertElevation,
      this.ancillaryRole,
      this.manholeGroundElevation,
      this.manholeTotalFlow,
      this.manholeIncomingLineId1,
      this.manholeIncomingLineInvert1,
      this.manholeIncomingLineId2,
      this.manholeIncomingLineInvert2,
      this.manholeIncomingLineId3,
      this.manholeIncomingLineInvert3,
      this.manholeOutgoingLineId,
      this.manholeOutgoingLineInvert,
      this.manholeType,
      this.enabled,
      this.upstreamStructureId,
      this.downstreamStructureId,
      this.groundElevation,
      this.description,
      this.designDate,
      this.designCompany,
      this.constructionDate,
      this.constructionCompany,
      this.inServiceDate,
      this.lifeCycleStatus,
      this.remarks,
      this.gl,
      this.il,
      this.diameter,
      this.lastEditedUser,
      this.lastEditedDate,
      this.createdUser,
      this.createdDate})
      : super._();

  @override
  MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties
      rebuild(
              void Function(
                      MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)
                  updates) =>
          (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      toBuilder() =>
          new MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties &&
        id == other.id &&
        municipalityName == other.municipalityName &&
        city == other.city &&
        district == other.district &&
        street == other.street &&
        manholeNo == other.manholeNo &&
        manholeId == other.manholeId &&
        manholeDepth == other.manholeDepth &&
        manholeLaderMaterial == other.manholeLaderMaterial &&
        manholeCoverMaterial == other.manholeCoverMaterial &&
        manholeMaterial == other.manholeMaterial &&
        manholeDiameter == other.manholeDiameter &&
        manholeInvertElevation == other.manholeInvertElevation &&
        ancillaryRole == other.ancillaryRole &&
        manholeGroundElevation == other.manholeGroundElevation &&
        manholeTotalFlow == other.manholeTotalFlow &&
        manholeIncomingLineId1 == other.manholeIncomingLineId1 &&
        manholeIncomingLineInvert1 == other.manholeIncomingLineInvert1 &&
        manholeIncomingLineId2 == other.manholeIncomingLineId2 &&
        manholeIncomingLineInvert2 == other.manholeIncomingLineInvert2 &&
        manholeIncomingLineId3 == other.manholeIncomingLineId3 &&
        manholeIncomingLineInvert3 == other.manholeIncomingLineInvert3 &&
        manholeOutgoingLineId == other.manholeOutgoingLineId &&
        manholeOutgoingLineInvert == other.manholeOutgoingLineInvert &&
        manholeType == other.manholeType &&
        enabled == other.enabled &&
        upstreamStructureId == other.upstreamStructureId &&
        downstreamStructureId == other.downstreamStructureId &&
        groundElevation == other.groundElevation &&
        description == other.description &&
        designDate == other.designDate &&
        designCompany == other.designCompany &&
        constructionDate == other.constructionDate &&
        constructionCompany == other.constructionCompany &&
        inServiceDate == other.inServiceDate &&
        lifeCycleStatus == other.lifeCycleStatus &&
        remarks == other.remarks &&
        gl == other.gl &&
        il == other.il &&
        diameter == other.diameter &&
        lastEditedUser == other.lastEditedUser &&
        lastEditedDate == other.lastEditedDate &&
        createdUser == other.createdUser &&
        createdDate == other.createdDate;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, municipalityName.hashCode);
    _$hash = $jc(_$hash, city.hashCode);
    _$hash = $jc(_$hash, district.hashCode);
    _$hash = $jc(_$hash, street.hashCode);
    _$hash = $jc(_$hash, manholeNo.hashCode);
    _$hash = $jc(_$hash, manholeId.hashCode);
    _$hash = $jc(_$hash, manholeDepth.hashCode);
    _$hash = $jc(_$hash, manholeLaderMaterial.hashCode);
    _$hash = $jc(_$hash, manholeCoverMaterial.hashCode);
    _$hash = $jc(_$hash, manholeMaterial.hashCode);
    _$hash = $jc(_$hash, manholeDiameter.hashCode);
    _$hash = $jc(_$hash, manholeInvertElevation.hashCode);
    _$hash = $jc(_$hash, ancillaryRole.hashCode);
    _$hash = $jc(_$hash, manholeGroundElevation.hashCode);
    _$hash = $jc(_$hash, manholeTotalFlow.hashCode);
    _$hash = $jc(_$hash, manholeIncomingLineId1.hashCode);
    _$hash = $jc(_$hash, manholeIncomingLineInvert1.hashCode);
    _$hash = $jc(_$hash, manholeIncomingLineId2.hashCode);
    _$hash = $jc(_$hash, manholeIncomingLineInvert2.hashCode);
    _$hash = $jc(_$hash, manholeIncomingLineId3.hashCode);
    _$hash = $jc(_$hash, manholeIncomingLineInvert3.hashCode);
    _$hash = $jc(_$hash, manholeOutgoingLineId.hashCode);
    _$hash = $jc(_$hash, manholeOutgoingLineInvert.hashCode);
    _$hash = $jc(_$hash, manholeType.hashCode);
    _$hash = $jc(_$hash, enabled.hashCode);
    _$hash = $jc(_$hash, upstreamStructureId.hashCode);
    _$hash = $jc(_$hash, downstreamStructureId.hashCode);
    _$hash = $jc(_$hash, groundElevation.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, designDate.hashCode);
    _$hash = $jc(_$hash, designCompany.hashCode);
    _$hash = $jc(_$hash, constructionDate.hashCode);
    _$hash = $jc(_$hash, constructionCompany.hashCode);
    _$hash = $jc(_$hash, inServiceDate.hashCode);
    _$hash = $jc(_$hash, lifeCycleStatus.hashCode);
    _$hash = $jc(_$hash, remarks.hashCode);
    _$hash = $jc(_$hash, gl.hashCode);
    _$hash = $jc(_$hash, il.hashCode);
    _$hash = $jc(_$hash, diameter.hashCode);
    _$hash = $jc(_$hash, lastEditedUser.hashCode);
    _$hash = $jc(_$hash, lastEditedDate.hashCode);
    _$hash = $jc(_$hash, createdUser.hashCode);
    _$hash = $jc(_$hash, createdDate.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties')
          ..add('id', id)
          ..add('municipalityName', municipalityName)
          ..add('city', city)
          ..add('district', district)
          ..add('street', street)
          ..add('manholeNo', manholeNo)
          ..add('manholeId', manholeId)
          ..add('manholeDepth', manholeDepth)
          ..add('manholeLaderMaterial', manholeLaderMaterial)
          ..add('manholeCoverMaterial', manholeCoverMaterial)
          ..add('manholeMaterial', manholeMaterial)
          ..add('manholeDiameter', manholeDiameter)
          ..add('manholeInvertElevation', manholeInvertElevation)
          ..add('ancillaryRole', ancillaryRole)
          ..add('manholeGroundElevation', manholeGroundElevation)
          ..add('manholeTotalFlow', manholeTotalFlow)
          ..add('manholeIncomingLineId1', manholeIncomingLineId1)
          ..add('manholeIncomingLineInvert1', manholeIncomingLineInvert1)
          ..add('manholeIncomingLineId2', manholeIncomingLineId2)
          ..add('manholeIncomingLineInvert2', manholeIncomingLineInvert2)
          ..add('manholeIncomingLineId3', manholeIncomingLineId3)
          ..add('manholeIncomingLineInvert3', manholeIncomingLineInvert3)
          ..add('manholeOutgoingLineId', manholeOutgoingLineId)
          ..add('manholeOutgoingLineInvert', manholeOutgoingLineInvert)
          ..add('manholeType', manholeType)
          ..add('enabled', enabled)
          ..add('upstreamStructureId', upstreamStructureId)
          ..add('downstreamStructureId', downstreamStructureId)
          ..add('groundElevation', groundElevation)
          ..add('description', description)
          ..add('designDate', designDate)
          ..add('designCompany', designCompany)
          ..add('constructionDate', constructionDate)
          ..add('constructionCompany', constructionCompany)
          ..add('inServiceDate', inServiceDate)
          ..add('lifeCycleStatus', lifeCycleStatus)
          ..add('remarks', remarks)
          ..add('gl', gl)
          ..add('il', il)
          ..add('diameter', diameter)
          ..add('lastEditedUser', lastEditedUser)
          ..add('lastEditedDate', lastEditedDate)
          ..add('createdUser', createdUser)
          ..add('createdDate', createdDate))
        .toString();
  }
}

class MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
    implements
        Builder<
            MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties,
            MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder> {
  _$MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties?
      _$v;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _id;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get id => _$this._id ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set id(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              id) =>
      _$this._id = id;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _municipalityName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get municipalityName => _$this._municipalityName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set municipalityName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              municipalityName) =>
      _$this._municipalityName = municipalityName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _city;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get city => _$this._city ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set city(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              city) =>
      _$this._city = city;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _district;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get district => _$this._district ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set district(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              district) =>
      _$this._district = district;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _street;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get street => _$this._street ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set street(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              street) =>
      _$this._street = street;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _manholeNo;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get manholeNo => _$this._manholeNo ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set manholeNo(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              manholeNo) =>
      _$this._manholeNo = manholeNo;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _manholeId;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get manholeId => _$this._manholeId ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set manholeId(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              manholeId) =>
      _$this._manholeId = manholeId;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _manholeDepth;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get manholeDepth => _$this._manholeDepth ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set manholeDepth(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              manholeDepth) =>
      _$this._manholeDepth = manholeDepth;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _manholeLaderMaterial;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get manholeLaderMaterial => _$this._manholeLaderMaterial ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set manholeLaderMaterial(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              manholeLaderMaterial) =>
      _$this._manholeLaderMaterial = manholeLaderMaterial;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _manholeCoverMaterial;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get manholeCoverMaterial => _$this._manholeCoverMaterial ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set manholeCoverMaterial(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              manholeCoverMaterial) =>
      _$this._manholeCoverMaterial = manholeCoverMaterial;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _manholeMaterial;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get manholeMaterial => _$this._manholeMaterial ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set manholeMaterial(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              manholeMaterial) =>
      _$this._manholeMaterial = manholeMaterial;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _manholeDiameter;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get manholeDiameter => _$this._manholeDiameter ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set manholeDiameter(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              manholeDiameter) =>
      _$this._manholeDiameter = manholeDiameter;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _manholeInvertElevation;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get manholeInvertElevation => _$this._manholeInvertElevation ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set manholeInvertElevation(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              manholeInvertElevation) =>
      _$this._manholeInvertElevation = manholeInvertElevation;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _ancillaryRole;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get ancillaryRole => _$this._ancillaryRole ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set ancillaryRole(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              ancillaryRole) =>
      _$this._ancillaryRole = ancillaryRole;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _manholeGroundElevation;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get manholeGroundElevation => _$this._manholeGroundElevation ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set manholeGroundElevation(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              manholeGroundElevation) =>
      _$this._manholeGroundElevation = manholeGroundElevation;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _manholeTotalFlow;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get manholeTotalFlow => _$this._manholeTotalFlow ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set manholeTotalFlow(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              manholeTotalFlow) =>
      _$this._manholeTotalFlow = manholeTotalFlow;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _manholeIncomingLineId1;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get manholeIncomingLineId1 => _$this._manholeIncomingLineId1 ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set manholeIncomingLineId1(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              manholeIncomingLineId1) =>
      _$this._manholeIncomingLineId1 = manholeIncomingLineId1;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _manholeIncomingLineInvert1;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get manholeIncomingLineInvert1 => _$this._manholeIncomingLineInvert1 ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set manholeIncomingLineInvert1(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              manholeIncomingLineInvert1) =>
      _$this._manholeIncomingLineInvert1 = manholeIncomingLineInvert1;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _manholeIncomingLineId2;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get manholeIncomingLineId2 => _$this._manholeIncomingLineId2 ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set manholeIncomingLineId2(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              manholeIncomingLineId2) =>
      _$this._manholeIncomingLineId2 = manholeIncomingLineId2;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _manholeIncomingLineInvert2;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get manholeIncomingLineInvert2 => _$this._manholeIncomingLineInvert2 ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set manholeIncomingLineInvert2(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              manholeIncomingLineInvert2) =>
      _$this._manholeIncomingLineInvert2 = manholeIncomingLineInvert2;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _manholeIncomingLineId3;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get manholeIncomingLineId3 => _$this._manholeIncomingLineId3 ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set manholeIncomingLineId3(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              manholeIncomingLineId3) =>
      _$this._manholeIncomingLineId3 = manholeIncomingLineId3;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _manholeIncomingLineInvert3;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get manholeIncomingLineInvert3 => _$this._manholeIncomingLineInvert3 ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set manholeIncomingLineInvert3(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              manholeIncomingLineInvert3) =>
      _$this._manholeIncomingLineInvert3 = manholeIncomingLineInvert3;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _manholeOutgoingLineId;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get manholeOutgoingLineId => _$this._manholeOutgoingLineId ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set manholeOutgoingLineId(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              manholeOutgoingLineId) =>
      _$this._manholeOutgoingLineId = manholeOutgoingLineId;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _manholeOutgoingLineInvert;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get manholeOutgoingLineInvert => _$this._manholeOutgoingLineInvert ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set manholeOutgoingLineInvert(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              manholeOutgoingLineInvert) =>
      _$this._manholeOutgoingLineInvert = manholeOutgoingLineInvert;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _manholeType;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get manholeType => _$this._manholeType ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set manholeType(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              manholeType) =>
      _$this._manholeType = manholeType;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _enabled;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get enabled => _$this._enabled ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set enabled(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              enabled) =>
      _$this._enabled = enabled;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _upstreamStructureId;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get upstreamStructureId => _$this._upstreamStructureId ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set upstreamStructureId(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              upstreamStructureId) =>
      _$this._upstreamStructureId = upstreamStructureId;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _downstreamStructureId;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get downstreamStructureId => _$this._downstreamStructureId ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set downstreamStructureId(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              downstreamStructureId) =>
      _$this._downstreamStructureId = downstreamStructureId;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _groundElevation;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get groundElevation => _$this._groundElevation ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set groundElevation(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              groundElevation) =>
      _$this._groundElevation = groundElevation;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _description;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get description => _$this._description ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set description(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              description) =>
      _$this._description = description;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _designDate;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get designDate => _$this._designDate ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set designDate(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              designDate) =>
      _$this._designDate = designDate;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _designCompany;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get designCompany => _$this._designCompany ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set designCompany(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              designCompany) =>
      _$this._designCompany = designCompany;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _constructionDate;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get constructionDate => _$this._constructionDate ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set constructionDate(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              constructionDate) =>
      _$this._constructionDate = constructionDate;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _constructionCompany;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get constructionCompany => _$this._constructionCompany ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set constructionCompany(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              constructionCompany) =>
      _$this._constructionCompany = constructionCompany;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _inServiceDate;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get inServiceDate => _$this._inServiceDate ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set inServiceDate(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              inServiceDate) =>
      _$this._inServiceDate = inServiceDate;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _lifeCycleStatus;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get lifeCycleStatus => _$this._lifeCycleStatus ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set lifeCycleStatus(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              lifeCycleStatus) =>
      _$this._lifeCycleStatus = lifeCycleStatus;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _remarks;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get remarks => _$this._remarks ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set remarks(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              remarks) =>
      _$this._remarks = remarks;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _gl;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get gl => _$this._gl ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set gl(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              gl) =>
      _$this._gl = gl;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _il;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get il => _$this._il ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set il(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              il) =>
      _$this._il = il;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _diameter;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get diameter => _$this._diameter ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set diameter(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              diameter) =>
      _$this._diameter = diameter;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _lastEditedUser;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get lastEditedUser => _$this._lastEditedUser ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set lastEditedUser(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              lastEditedUser) =>
      _$this._lastEditedUser = lastEditedUser;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _lastEditedDate;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get lastEditedDate => _$this._lastEditedDate ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set lastEditedDate(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              lastEditedDate) =>
      _$this._lastEditedDate = lastEditedDate;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _createdUser;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get createdUser => _$this._createdUser ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set createdUser(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              createdUser) =>
      _$this._createdUser = createdUser;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _createdDate;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get createdDate => _$this._createdDate ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set createdDate(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              createdDate) =>
      _$this._createdDate = createdDate;

  MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder() {
    MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties
        ._defaults(this);
  }

  MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id?.toBuilder();
      _municipalityName = $v.municipalityName?.toBuilder();
      _city = $v.city?.toBuilder();
      _district = $v.district?.toBuilder();
      _street = $v.street?.toBuilder();
      _manholeNo = $v.manholeNo?.toBuilder();
      _manholeId = $v.manholeId?.toBuilder();
      _manholeDepth = $v.manholeDepth?.toBuilder();
      _manholeLaderMaterial = $v.manholeLaderMaterial?.toBuilder();
      _manholeCoverMaterial = $v.manholeCoverMaterial?.toBuilder();
      _manholeMaterial = $v.manholeMaterial?.toBuilder();
      _manholeDiameter = $v.manholeDiameter?.toBuilder();
      _manholeInvertElevation = $v.manholeInvertElevation?.toBuilder();
      _ancillaryRole = $v.ancillaryRole?.toBuilder();
      _manholeGroundElevation = $v.manholeGroundElevation?.toBuilder();
      _manholeTotalFlow = $v.manholeTotalFlow?.toBuilder();
      _manholeIncomingLineId1 = $v.manholeIncomingLineId1?.toBuilder();
      _manholeIncomingLineInvert1 = $v.manholeIncomingLineInvert1?.toBuilder();
      _manholeIncomingLineId2 = $v.manholeIncomingLineId2?.toBuilder();
      _manholeIncomingLineInvert2 = $v.manholeIncomingLineInvert2?.toBuilder();
      _manholeIncomingLineId3 = $v.manholeIncomingLineId3?.toBuilder();
      _manholeIncomingLineInvert3 = $v.manholeIncomingLineInvert3?.toBuilder();
      _manholeOutgoingLineId = $v.manholeOutgoingLineId?.toBuilder();
      _manholeOutgoingLineInvert = $v.manholeOutgoingLineInvert?.toBuilder();
      _manholeType = $v.manholeType?.toBuilder();
      _enabled = $v.enabled?.toBuilder();
      _upstreamStructureId = $v.upstreamStructureId?.toBuilder();
      _downstreamStructureId = $v.downstreamStructureId?.toBuilder();
      _groundElevation = $v.groundElevation?.toBuilder();
      _description = $v.description?.toBuilder();
      _designDate = $v.designDate?.toBuilder();
      _designCompany = $v.designCompany?.toBuilder();
      _constructionDate = $v.constructionDate?.toBuilder();
      _constructionCompany = $v.constructionCompany?.toBuilder();
      _inServiceDate = $v.inServiceDate?.toBuilder();
      _lifeCycleStatus = $v.lifeCycleStatus?.toBuilder();
      _remarks = $v.remarks?.toBuilder();
      _gl = $v.gl?.toBuilder();
      _il = $v.il?.toBuilder();
      _diameter = $v.diameter?.toBuilder();
      _lastEditedUser = $v.lastEditedUser?.toBuilder();
      _lastEditedDate = $v.lastEditedDate?.toBuilder();
      _createdUser = $v.createdUser?.toBuilder();
      _createdDate = $v.createdDate?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties
      build() => _build();

  _$MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties
      _build() {
    _$MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties
        _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties
              ._(
              id: _id?.build(),
              municipalityName: _municipalityName?.build(),
              city: _city?.build(),
              district: _district?.build(),
              street: _street?.build(),
              manholeNo: _manholeNo?.build(),
              manholeId: _manholeId?.build(),
              manholeDepth: _manholeDepth?.build(),
              manholeLaderMaterial: _manholeLaderMaterial?.build(),
              manholeCoverMaterial: _manholeCoverMaterial?.build(),
              manholeMaterial: _manholeMaterial?.build(),
              manholeDiameter: _manholeDiameter?.build(),
              manholeInvertElevation: _manholeInvertElevation?.build(),
              ancillaryRole: _ancillaryRole?.build(),
              manholeGroundElevation: _manholeGroundElevation?.build(),
              manholeTotalFlow: _manholeTotalFlow?.build(),
              manholeIncomingLineId1: _manholeIncomingLineId1?.build(),
              manholeIncomingLineInvert1: _manholeIncomingLineInvert1?.build(),
              manholeIncomingLineId2: _manholeIncomingLineId2?.build(),
              manholeIncomingLineInvert2: _manholeIncomingLineInvert2?.build(),
              manholeIncomingLineId3: _manholeIncomingLineId3?.build(),
              manholeIncomingLineInvert3: _manholeIncomingLineInvert3?.build(),
              manholeOutgoingLineId: _manholeOutgoingLineId?.build(),
              manholeOutgoingLineInvert: _manholeOutgoingLineInvert?.build(),
              manholeType: _manholeType?.build(),
              enabled: _enabled?.build(),
              upstreamStructureId: _upstreamStructureId?.build(),
              downstreamStructureId: _downstreamStructureId?.build(),
              groundElevation: _groundElevation?.build(),
              description: _description?.build(),
              designDate: _designDate?.build(),
              designCompany: _designCompany?.build(),
              constructionDate: _constructionDate?.build(),
              constructionCompany: _constructionCompany?.build(),
              inServiceDate: _inServiceDate?.build(),
              lifeCycleStatus: _lifeCycleStatus?.build(),
              remarks: _remarks?.build(),
              gl: _gl?.build(),
              il: _il?.build(),
              diameter: _diameter?.build(),
              lastEditedUser: _lastEditedUser?.build(),
              lastEditedDate: _lastEditedDate?.build(),
              createdUser: _createdUser?.build(),
              createdDate: _createdDate?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'id';
        _id?.build();
        _$failedField = 'municipalityName';
        _municipalityName?.build();
        _$failedField = 'city';
        _city?.build();
        _$failedField = 'district';
        _district?.build();
        _$failedField = 'street';
        _street?.build();
        _$failedField = 'manholeNo';
        _manholeNo?.build();
        _$failedField = 'manholeId';
        _manholeId?.build();
        _$failedField = 'manholeDepth';
        _manholeDepth?.build();
        _$failedField = 'manholeLaderMaterial';
        _manholeLaderMaterial?.build();
        _$failedField = 'manholeCoverMaterial';
        _manholeCoverMaterial?.build();
        _$failedField = 'manholeMaterial';
        _manholeMaterial?.build();
        _$failedField = 'manholeDiameter';
        _manholeDiameter?.build();
        _$failedField = 'manholeInvertElevation';
        _manholeInvertElevation?.build();
        _$failedField = 'ancillaryRole';
        _ancillaryRole?.build();
        _$failedField = 'manholeGroundElevation';
        _manholeGroundElevation?.build();
        _$failedField = 'manholeTotalFlow';
        _manholeTotalFlow?.build();
        _$failedField = 'manholeIncomingLineId1';
        _manholeIncomingLineId1?.build();
        _$failedField = 'manholeIncomingLineInvert1';
        _manholeIncomingLineInvert1?.build();
        _$failedField = 'manholeIncomingLineId2';
        _manholeIncomingLineId2?.build();
        _$failedField = 'manholeIncomingLineInvert2';
        _manholeIncomingLineInvert2?.build();
        _$failedField = 'manholeIncomingLineId3';
        _manholeIncomingLineId3?.build();
        _$failedField = 'manholeIncomingLineInvert3';
        _manholeIncomingLineInvert3?.build();
        _$failedField = 'manholeOutgoingLineId';
        _manholeOutgoingLineId?.build();
        _$failedField = 'manholeOutgoingLineInvert';
        _manholeOutgoingLineInvert?.build();
        _$failedField = 'manholeType';
        _manholeType?.build();
        _$failedField = 'enabled';
        _enabled?.build();
        _$failedField = 'upstreamStructureId';
        _upstreamStructureId?.build();
        _$failedField = 'downstreamStructureId';
        _downstreamStructureId?.build();
        _$failedField = 'groundElevation';
        _groundElevation?.build();
        _$failedField = 'description';
        _description?.build();
        _$failedField = 'designDate';
        _designDate?.build();
        _$failedField = 'designCompany';
        _designCompany?.build();
        _$failedField = 'constructionDate';
        _constructionDate?.build();
        _$failedField = 'constructionCompany';
        _constructionCompany?.build();
        _$failedField = 'inServiceDate';
        _inServiceDate?.build();
        _$failedField = 'lifeCycleStatus';
        _lifeCycleStatus?.build();
        _$failedField = 'remarks';
        _remarks?.build();
        _$failedField = 'gl';
        _gl?.build();
        _$failedField = 'il';
        _il?.build();
        _$failedField = 'diameter';
        _diameter?.build();
        _$failedField = 'lastEditedUser';
        _lastEditedUser?.build();
        _$failedField = 'lastEditedDate';
        _lastEditedDate?.build();
        _$failedField = 'createdUser';
        _createdUser?.build();
        _$failedField = 'createdDate';
        _createdDate?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
