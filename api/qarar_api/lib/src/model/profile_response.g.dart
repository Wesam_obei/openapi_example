// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ProfileResponse extends ProfileResponse {
  @override
  final num id;
  @override
  final String email;
  @override
  final String fullName;
  @override
  final DateTime createdAt;
  @override
  final bool isActive;
  @override
  final BuiltList<GetRoleDataResponse> roles;
  @override
  final BuiltList<PermissionDataResponse> permissions;
  @override
  final BuiltList<GetEntityDataResponse> entities;
  @override
  final GetRaqmyInfo raqmyInfo;

  factory _$ProfileResponse([void Function(ProfileResponseBuilder)? updates]) =>
      (new ProfileResponseBuilder()..update(updates))._build();

  _$ProfileResponse._(
      {required this.id,
      required this.email,
      required this.fullName,
      required this.createdAt,
      required this.isActive,
      required this.roles,
      required this.permissions,
      required this.entities,
      required this.raqmyInfo})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, r'ProfileResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(email, r'ProfileResponse', 'email');
    BuiltValueNullFieldError.checkNotNull(
        fullName, r'ProfileResponse', 'fullName');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'ProfileResponse', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        isActive, r'ProfileResponse', 'isActive');
    BuiltValueNullFieldError.checkNotNull(roles, r'ProfileResponse', 'roles');
    BuiltValueNullFieldError.checkNotNull(
        permissions, r'ProfileResponse', 'permissions');
    BuiltValueNullFieldError.checkNotNull(
        entities, r'ProfileResponse', 'entities');
    BuiltValueNullFieldError.checkNotNull(
        raqmyInfo, r'ProfileResponse', 'raqmyInfo');
  }

  @override
  ProfileResponse rebuild(void Function(ProfileResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProfileResponseBuilder toBuilder() =>
      new ProfileResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProfileResponse &&
        id == other.id &&
        email == other.email &&
        fullName == other.fullName &&
        createdAt == other.createdAt &&
        isActive == other.isActive &&
        roles == other.roles &&
        permissions == other.permissions &&
        entities == other.entities &&
        raqmyInfo == other.raqmyInfo;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, email.hashCode);
    _$hash = $jc(_$hash, fullName.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, isActive.hashCode);
    _$hash = $jc(_$hash, roles.hashCode);
    _$hash = $jc(_$hash, permissions.hashCode);
    _$hash = $jc(_$hash, entities.hashCode);
    _$hash = $jc(_$hash, raqmyInfo.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ProfileResponse')
          ..add('id', id)
          ..add('email', email)
          ..add('fullName', fullName)
          ..add('createdAt', createdAt)
          ..add('isActive', isActive)
          ..add('roles', roles)
          ..add('permissions', permissions)
          ..add('entities', entities)
          ..add('raqmyInfo', raqmyInfo))
        .toString();
  }
}

class ProfileResponseBuilder
    implements Builder<ProfileResponse, ProfileResponseBuilder> {
  _$ProfileResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _email;
  String? get email => _$this._email;
  set email(String? email) => _$this._email = email;

  String? _fullName;
  String? get fullName => _$this._fullName;
  set fullName(String? fullName) => _$this._fullName = fullName;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  bool? _isActive;
  bool? get isActive => _$this._isActive;
  set isActive(bool? isActive) => _$this._isActive = isActive;

  ListBuilder<GetRoleDataResponse>? _roles;
  ListBuilder<GetRoleDataResponse> get roles =>
      _$this._roles ??= new ListBuilder<GetRoleDataResponse>();
  set roles(ListBuilder<GetRoleDataResponse>? roles) => _$this._roles = roles;

  ListBuilder<PermissionDataResponse>? _permissions;
  ListBuilder<PermissionDataResponse> get permissions =>
      _$this._permissions ??= new ListBuilder<PermissionDataResponse>();
  set permissions(ListBuilder<PermissionDataResponse>? permissions) =>
      _$this._permissions = permissions;

  ListBuilder<GetEntityDataResponse>? _entities;
  ListBuilder<GetEntityDataResponse> get entities =>
      _$this._entities ??= new ListBuilder<GetEntityDataResponse>();
  set entities(ListBuilder<GetEntityDataResponse>? entities) =>
      _$this._entities = entities;

  GetRaqmyInfoBuilder? _raqmyInfo;
  GetRaqmyInfoBuilder get raqmyInfo =>
      _$this._raqmyInfo ??= new GetRaqmyInfoBuilder();
  set raqmyInfo(GetRaqmyInfoBuilder? raqmyInfo) =>
      _$this._raqmyInfo = raqmyInfo;

  ProfileResponseBuilder() {
    ProfileResponse._defaults(this);
  }

  ProfileResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _email = $v.email;
      _fullName = $v.fullName;
      _createdAt = $v.createdAt;
      _isActive = $v.isActive;
      _roles = $v.roles.toBuilder();
      _permissions = $v.permissions.toBuilder();
      _entities = $v.entities.toBuilder();
      _raqmyInfo = $v.raqmyInfo.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProfileResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ProfileResponse;
  }

  @override
  void update(void Function(ProfileResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ProfileResponse build() => _build();

  _$ProfileResponse _build() {
    _$ProfileResponse _$result;
    try {
      _$result = _$v ??
          new _$ProfileResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'ProfileResponse', 'id'),
              email: BuiltValueNullFieldError.checkNotNull(
                  email, r'ProfileResponse', 'email'),
              fullName: BuiltValueNullFieldError.checkNotNull(
                  fullName, r'ProfileResponse', 'fullName'),
              createdAt: BuiltValueNullFieldError.checkNotNull(
                  createdAt, r'ProfileResponse', 'createdAt'),
              isActive: BuiltValueNullFieldError.checkNotNull(
                  isActive, r'ProfileResponse', 'isActive'),
              roles: roles.build(),
              permissions: permissions.build(),
              entities: entities.build(),
              raqmyInfo: raqmyInfo.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'roles';
        roles.build();
        _$failedField = 'permissions';
        permissions.build();
        _$failedField = 'entities';
        entities.build();
        _$failedField = 'raqmyInfo';
        raqmyInfo.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ProfileResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
