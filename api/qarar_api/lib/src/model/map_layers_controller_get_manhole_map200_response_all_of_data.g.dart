// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_manhole_map200_response_all_of_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum_featureCollection =
    const MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum._(
        'featureCollection');

MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'featureCollection':
      return _$mapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum_featureCollection;
    default:
      return _$mapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum_featureCollection;
  }
}

final BuiltSet<MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum>(const <MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum>[
  _$mapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum_featureCollection,
]);

Serializer<MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnumSerializer =
    new _$MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnumSerializer();

class _$MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'featureCollection': 'FeatureCollection',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'FeatureCollection': 'featureCollection',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum';

  @override
  Object serialize(Serializers serializers,
          MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum deserialize(
          Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetManholeMap200ResponseAllOfData
    extends MapLayersControllerGetManholeMap200ResponseAllOfData {
  @override
  final MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum type;
  @override
  final BuiltList<
          MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner>
      features;

  factory _$MapLayersControllerGetManholeMap200ResponseAllOfData(
          [void Function(
                  MapLayersControllerGetManholeMap200ResponseAllOfDataBuilder)?
              updates]) =>
      (new MapLayersControllerGetManholeMap200ResponseAllOfDataBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetManholeMap200ResponseAllOfData._(
      {required this.type, required this.features})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        type, r'MapLayersControllerGetManholeMap200ResponseAllOfData', 'type');
    BuiltValueNullFieldError.checkNotNull(features,
        r'MapLayersControllerGetManholeMap200ResponseAllOfData', 'features');
  }

  @override
  MapLayersControllerGetManholeMap200ResponseAllOfData rebuild(
          void Function(
                  MapLayersControllerGetManholeMap200ResponseAllOfDataBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetManholeMap200ResponseAllOfDataBuilder toBuilder() =>
      new MapLayersControllerGetManholeMap200ResponseAllOfDataBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapLayersControllerGetManholeMap200ResponseAllOfData &&
        type == other.type &&
        features == other.features;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, features.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetManholeMap200ResponseAllOfData')
          ..add('type', type)
          ..add('features', features))
        .toString();
  }
}

class MapLayersControllerGetManholeMap200ResponseAllOfDataBuilder
    implements
        Builder<MapLayersControllerGetManholeMap200ResponseAllOfData,
            MapLayersControllerGetManholeMap200ResponseAllOfDataBuilder> {
  _$MapLayersControllerGetManholeMap200ResponseAllOfData? _$v;

  MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum? _type;
  MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum? get type =>
      _$this._type;
  set type(
          MapLayersControllerGetManholeMap200ResponseAllOfDataTypeEnum? type) =>
      _$this._type = type;

  ListBuilder<
          MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner>?
      _features;
  ListBuilder<MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner>
      get features => _$this._features ??= new ListBuilder<
          MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner>();
  set features(
          ListBuilder<
                  MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner>?
              features) =>
      _$this._features = features;

  MapLayersControllerGetManholeMap200ResponseAllOfDataBuilder() {
    MapLayersControllerGetManholeMap200ResponseAllOfData._defaults(this);
  }

  MapLayersControllerGetManholeMap200ResponseAllOfDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _features = $v.features.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MapLayersControllerGetManholeMap200ResponseAllOfData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapLayersControllerGetManholeMap200ResponseAllOfData;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetManholeMap200ResponseAllOfDataBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetManholeMap200ResponseAllOfData build() => _build();

  _$MapLayersControllerGetManholeMap200ResponseAllOfData _build() {
    _$MapLayersControllerGetManholeMap200ResponseAllOfData _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetManholeMap200ResponseAllOfData._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetManholeMap200ResponseAllOfData',
                  'type'),
              features: features.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'features';
        features.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetManholeMap200ResponseAllOfData',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
