// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_dashboard_update_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$UserDashboardUpdateRequest extends UserDashboardUpdateRequest {
  @override
  final String name;
  @override
  final BuiltList<WidgetConfig>? settings;

  factory _$UserDashboardUpdateRequest(
          [void Function(UserDashboardUpdateRequestBuilder)? updates]) =>
      (new UserDashboardUpdateRequestBuilder()..update(updates))._build();

  _$UserDashboardUpdateRequest._({required this.name, this.settings})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        name, r'UserDashboardUpdateRequest', 'name');
  }

  @override
  UserDashboardUpdateRequest rebuild(
          void Function(UserDashboardUpdateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserDashboardUpdateRequestBuilder toBuilder() =>
      new UserDashboardUpdateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserDashboardUpdateRequest &&
        name == other.name &&
        settings == other.settings;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, settings.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'UserDashboardUpdateRequest')
          ..add('name', name)
          ..add('settings', settings))
        .toString();
  }
}

class UserDashboardUpdateRequestBuilder
    implements
        Builder<UserDashboardUpdateRequest, UserDashboardUpdateRequestBuilder> {
  _$UserDashboardUpdateRequest? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  ListBuilder<WidgetConfig>? _settings;
  ListBuilder<WidgetConfig> get settings =>
      _$this._settings ??= new ListBuilder<WidgetConfig>();
  set settings(ListBuilder<WidgetConfig>? settings) =>
      _$this._settings = settings;

  UserDashboardUpdateRequestBuilder() {
    UserDashboardUpdateRequest._defaults(this);
  }

  UserDashboardUpdateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _settings = $v.settings?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserDashboardUpdateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$UserDashboardUpdateRequest;
  }

  @override
  void update(void Function(UserDashboardUpdateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  UserDashboardUpdateRequest build() => _build();

  _$UserDashboardUpdateRequest _build() {
    _$UserDashboardUpdateRequest _$result;
    try {
      _$result = _$v ??
          new _$UserDashboardUpdateRequest._(
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'UserDashboardUpdateRequest', 'name'),
              settings: _settings?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'settings';
        _settings?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'UserDashboardUpdateRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
