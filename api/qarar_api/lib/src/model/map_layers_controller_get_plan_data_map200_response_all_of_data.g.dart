// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_plan_data_map200_response_all_of_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum_featureCollection =
    const MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum._(
        'featureCollection');

MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'featureCollection':
      return _$mapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum_featureCollection;
    default:
      return _$mapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum_featureCollection;
  }
}

final BuiltSet<MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum>(const <MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum>[
  _$mapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum_featureCollection,
]);

Serializer<MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnumSerializer =
    new _$MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnumSerializer();

class _$MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'featureCollection': 'FeatureCollection',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'FeatureCollection': 'featureCollection',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum';

  @override
  Object serialize(Serializers serializers,
          MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum deserialize(
          Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetPlanDataMap200ResponseAllOfData
    extends MapLayersControllerGetPlanDataMap200ResponseAllOfData {
  @override
  final MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum type;
  @override
  final BuiltList<
          MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner>
      features;

  factory _$MapLayersControllerGetPlanDataMap200ResponseAllOfData(
          [void Function(
                  MapLayersControllerGetPlanDataMap200ResponseAllOfDataBuilder)?
              updates]) =>
      (new MapLayersControllerGetPlanDataMap200ResponseAllOfDataBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetPlanDataMap200ResponseAllOfData._(
      {required this.type, required this.features})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        type, r'MapLayersControllerGetPlanDataMap200ResponseAllOfData', 'type');
    BuiltValueNullFieldError.checkNotNull(features,
        r'MapLayersControllerGetPlanDataMap200ResponseAllOfData', 'features');
  }

  @override
  MapLayersControllerGetPlanDataMap200ResponseAllOfData rebuild(
          void Function(
                  MapLayersControllerGetPlanDataMap200ResponseAllOfDataBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetPlanDataMap200ResponseAllOfDataBuilder toBuilder() =>
      new MapLayersControllerGetPlanDataMap200ResponseAllOfDataBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapLayersControllerGetPlanDataMap200ResponseAllOfData &&
        type == other.type &&
        features == other.features;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, features.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetPlanDataMap200ResponseAllOfData')
          ..add('type', type)
          ..add('features', features))
        .toString();
  }
}

class MapLayersControllerGetPlanDataMap200ResponseAllOfDataBuilder
    implements
        Builder<MapLayersControllerGetPlanDataMap200ResponseAllOfData,
            MapLayersControllerGetPlanDataMap200ResponseAllOfDataBuilder> {
  _$MapLayersControllerGetPlanDataMap200ResponseAllOfData? _$v;

  MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum? _type;
  MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum? get type =>
      _$this._type;
  set type(
          MapLayersControllerGetPlanDataMap200ResponseAllOfDataTypeEnum?
              type) =>
      _$this._type = type;

  ListBuilder<
          MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner>?
      _features;
  ListBuilder<
          MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner>
      get features => _$this._features ??= new ListBuilder<
          MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner>();
  set features(
          ListBuilder<
                  MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner>?
              features) =>
      _$this._features = features;

  MapLayersControllerGetPlanDataMap200ResponseAllOfDataBuilder() {
    MapLayersControllerGetPlanDataMap200ResponseAllOfData._defaults(this);
  }

  MapLayersControllerGetPlanDataMap200ResponseAllOfDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _features = $v.features.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MapLayersControllerGetPlanDataMap200ResponseAllOfData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapLayersControllerGetPlanDataMap200ResponseAllOfData;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetPlanDataMap200ResponseAllOfDataBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetPlanDataMap200ResponseAllOfData build() => _build();

  _$MapLayersControllerGetPlanDataMap200ResponseAllOfData _build() {
    _$MapLayersControllerGetPlanDataMap200ResponseAllOfData _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetPlanDataMap200ResponseAllOfData._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetPlanDataMap200ResponseAllOfData',
                  'type'),
              features: features.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'features';
        features.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetPlanDataMap200ResponseAllOfData',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
