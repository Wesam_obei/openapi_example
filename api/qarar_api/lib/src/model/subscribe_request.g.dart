// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'subscribe_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SubscribeRequest extends SubscribeRequest {
  @override
  final String sessionId;
  @override
  final String endpoint;
  @override
  final JsonObject keys;

  factory _$SubscribeRequest(
          [void Function(SubscribeRequestBuilder)? updates]) =>
      (new SubscribeRequestBuilder()..update(updates))._build();

  _$SubscribeRequest._(
      {required this.sessionId, required this.endpoint, required this.keys})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        sessionId, r'SubscribeRequest', 'sessionId');
    BuiltValueNullFieldError.checkNotNull(
        endpoint, r'SubscribeRequest', 'endpoint');
    BuiltValueNullFieldError.checkNotNull(keys, r'SubscribeRequest', 'keys');
  }

  @override
  SubscribeRequest rebuild(void Function(SubscribeRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SubscribeRequestBuilder toBuilder() =>
      new SubscribeRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SubscribeRequest &&
        sessionId == other.sessionId &&
        endpoint == other.endpoint &&
        keys == other.keys;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, sessionId.hashCode);
    _$hash = $jc(_$hash, endpoint.hashCode);
    _$hash = $jc(_$hash, keys.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SubscribeRequest')
          ..add('sessionId', sessionId)
          ..add('endpoint', endpoint)
          ..add('keys', keys))
        .toString();
  }
}

class SubscribeRequestBuilder
    implements Builder<SubscribeRequest, SubscribeRequestBuilder> {
  _$SubscribeRequest? _$v;

  String? _sessionId;
  String? get sessionId => _$this._sessionId;
  set sessionId(String? sessionId) => _$this._sessionId = sessionId;

  String? _endpoint;
  String? get endpoint => _$this._endpoint;
  set endpoint(String? endpoint) => _$this._endpoint = endpoint;

  JsonObject? _keys;
  JsonObject? get keys => _$this._keys;
  set keys(JsonObject? keys) => _$this._keys = keys;

  SubscribeRequestBuilder() {
    SubscribeRequest._defaults(this);
  }

  SubscribeRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _sessionId = $v.sessionId;
      _endpoint = $v.endpoint;
      _keys = $v.keys;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SubscribeRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SubscribeRequest;
  }

  @override
  void update(void Function(SubscribeRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SubscribeRequest build() => _build();

  _$SubscribeRequest _build() {
    final _$result = _$v ??
        new _$SubscribeRequest._(
            sessionId: BuiltValueNullFieldError.checkNotNull(
                sessionId, r'SubscribeRequest', 'sessionId'),
            endpoint: BuiltValueNullFieldError.checkNotNull(
                endpoint, r'SubscribeRequest', 'endpoint'),
            keys: BuiltValueNullFieldError.checkNotNull(
                keys, r'SubscribeRequest', 'keys'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
