// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_electrical_facilities_map200_response_all_of_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum_featureCollection =
    const MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum
        ._('featureCollection');

MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'featureCollection':
      return _$mapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum_featureCollection;
    default:
      return _$mapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum_featureCollection;
  }
}

final BuiltSet<
        MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum>(const <MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum>[
  _$mapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum_featureCollection,
]);

Serializer<
        MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnumSerializer =
    new _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnumSerializer();

class _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'featureCollection': 'FeatureCollection',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'FeatureCollection': 'featureCollection',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum';

  @override
  Object serialize(
          Serializers serializers,
          MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum
              object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum
      deserialize(Serializers serializers, Object serialized,
              {FullType specifiedType = FullType.unspecified}) =>
          MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum
              .valueOf(_fromWire[serialized] ??
                  (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData
    extends MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData {
  @override
  final MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum
      type;
  @override
  final BuiltList<
          MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInner>
      features;

  factory _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData(
          [void Function(
                  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataBuilder)?
              updates]) =>
      (new MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData._(
      {required this.type, required this.features})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        type,
        r'MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData',
        'type');
    BuiltValueNullFieldError.checkNotNull(
        features,
        r'MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData',
        'features');
  }

  @override
  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData rebuild(
          void Function(
                  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataBuilder
      toBuilder() =>
          new MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData &&
        type == other.type &&
        features == other.features;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, features.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData')
          ..add('type', type)
          ..add('features', features))
        .toString();
  }
}

class MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataBuilder
    implements
        Builder<
            MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData,
            MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataBuilder> {
  _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData? _$v;

  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum?
      _type;
  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum?
      get type => _$this._type;
  set type(
          MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum?
              type) =>
      _$this._type = type;

  ListBuilder<
          MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInner>?
      _features;
  ListBuilder<
          MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInner>
      get features => _$this._features ??= new ListBuilder<
          MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInner>();
  set features(
          ListBuilder<
                  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInner>?
              features) =>
      _$this._features = features;

  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataBuilder() {
    MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData._defaults(
        this);
  }

  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _features = $v.features.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData build() =>
      _build();

  _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData _build() {
    _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData
        _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData
              ._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData',
                  'type'),
              features: features.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'features';
        features.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
