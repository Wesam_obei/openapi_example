// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_plan_data_map200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetPlanDataMap200Response
    extends MapLayersControllerGetPlanDataMap200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$MapLayersControllerGetPlanDataMap200Response(
          [void Function(MapLayersControllerGetPlanDataMap200ResponseBuilder)?
              updates]) =>
      (new MapLayersControllerGetPlanDataMap200ResponseBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetPlanDataMap200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'MapLayersControllerGetPlanDataMap200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'MapLayersControllerGetPlanDataMap200Response', 'data');
  }

  @override
  MapLayersControllerGetPlanDataMap200Response rebuild(
          void Function(MapLayersControllerGetPlanDataMap200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetPlanDataMap200ResponseBuilder toBuilder() =>
      new MapLayersControllerGetPlanDataMap200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapLayersControllerGetPlanDataMap200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetPlanDataMap200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class MapLayersControllerGetPlanDataMap200ResponseBuilder
    implements
        Builder<MapLayersControllerGetPlanDataMap200Response,
            MapLayersControllerGetPlanDataMap200ResponseBuilder>,
        SuccessResponseBuilder {
  _$MapLayersControllerGetPlanDataMap200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  MapLayersControllerGetPlanDataMap200ResponseBuilder() {
    MapLayersControllerGetPlanDataMap200Response._defaults(this);
  }

  MapLayersControllerGetPlanDataMap200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant MapLayersControllerGetPlanDataMap200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapLayersControllerGetPlanDataMap200Response;
  }

  @override
  void update(
      void Function(MapLayersControllerGetPlanDataMap200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetPlanDataMap200Response build() => _build();

  _$MapLayersControllerGetPlanDataMap200Response _build() {
    final _$result = _$v ??
        new _$MapLayersControllerGetPlanDataMap200Response._(
            success: BuiltValueNullFieldError.checkNotNull(success,
                r'MapLayersControllerGetPlanDataMap200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'MapLayersControllerGetPlanDataMap200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
