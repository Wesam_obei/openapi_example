//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_sea_out_map200_response_all_of_data_features_inner.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_sea_out_map200_response_all_of_data.g.dart';

/// MapLayersControllerGetSeaOutMap200ResponseAllOfData
///
/// Properties:
/// * [type]
/// * [features]
@BuiltValue()
abstract class MapLayersControllerGetSeaOutMap200ResponseAllOfData
    implements
        Built<MapLayersControllerGetSeaOutMap200ResponseAllOfData,
            MapLayersControllerGetSeaOutMap200ResponseAllOfDataBuilder> {
  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum get type;
  // enum typeEnum {  FeatureCollection,  };

  @BuiltValueField(wireName: r'features')
  BuiltList<MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInner>
      get features;

  MapLayersControllerGetSeaOutMap200ResponseAllOfData._();

  factory MapLayersControllerGetSeaOutMap200ResponseAllOfData(
          [void updates(
              MapLayersControllerGetSeaOutMap200ResponseAllOfDataBuilder b)]) =
      _$MapLayersControllerGetSeaOutMap200ResponseAllOfData;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetSeaOutMap200ResponseAllOfDataBuilder b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<MapLayersControllerGetSeaOutMap200ResponseAllOfData>
      get serializer =>
          _$MapLayersControllerGetSeaOutMap200ResponseAllOfDataSerializer();
}

class _$MapLayersControllerGetSeaOutMap200ResponseAllOfDataSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetSeaOutMap200ResponseAllOfData> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetSeaOutMap200ResponseAllOfData,
    _$MapLayersControllerGetSeaOutMap200ResponseAllOfData
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetSeaOutMap200ResponseAllOfData';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetSeaOutMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(
          MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum),
    );
    yield r'features';
    yield serializers.serialize(
      object.features,
      specifiedType: const FullType(BuiltList, [
        FullType(
            MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInner)
      ]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetSeaOutMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetSeaOutMap200ResponseAllOfDataBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum),
          ) as MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum;
          result.type = valueDes;
          break;
        case r'features':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [
              FullType(
                  MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInner)
            ]),
          ) as BuiltList<
              MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInner>;
          result.features.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetSeaOutMap200ResponseAllOfData deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = MapLayersControllerGetSeaOutMap200ResponseAllOfDataBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum
    extends EnumClass {
  @BuiltValueEnumConst(wireName: r'FeatureCollection', fallback: true)
  static const MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum
      featureCollection =
      _$mapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum_featureCollection;

  static Serializer<MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum>
      get serializer =>
          _$mapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnumSerializer;

  const MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum._(
      String name)
      : super(name);

  static BuiltSet<MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum>
      get values =>
          _$mapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnumValues;
  static MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum valueOf(
          String name) =>
      _$mapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnumValueOf(
          name);
}
