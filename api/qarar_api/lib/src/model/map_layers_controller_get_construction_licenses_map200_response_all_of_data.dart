//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_construction_licenses_map200_response_all_of_data_features_inner.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_construction_licenses_map200_response_all_of_data.g.dart';

/// MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData
///
/// Properties:
/// * [type]
/// * [features]
@BuiltValue()
abstract class MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData
    implements
        Built<MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData,
            MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataBuilder> {
  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum
      get type;
  // enum typeEnum {  FeatureCollection,  };

  @BuiltValueField(wireName: r'features')
  BuiltList<
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner>
      get features;

  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData._();

  factory MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData(
          [void updates(
              MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataBuilder
                  b)]) =
      _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataBuilder
              b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData>
      get serializer =>
          _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataSerializer();
}

class _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData,
    _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum),
    );
    yield r'features';
    yield serializers.serialize(
      object.features,
      specifiedType: const FullType(BuiltList, [
        FullType(
            MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner)
      ]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum),
          ) as MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum;
          result.type = valueDes;
          break;
        case r'features':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [
              FullType(
                  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner)
            ]),
          ) as BuiltList<
              MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner>;
          result.features.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum
    extends EnumClass {
  @BuiltValueEnumConst(wireName: r'FeatureCollection', fallback: true)
  static const MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum
      featureCollection =
      _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum_featureCollection;

  static Serializer<
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum>
      get serializer =>
          _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnumSerializer;

  const MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum._(
      String name)
      : super(name);

  static BuiltSet<
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum>
      get values =>
          _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnumValues;
  static MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnum
      valueOf(String name) =>
          _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataTypeEnumValueOf(
              name);
}
