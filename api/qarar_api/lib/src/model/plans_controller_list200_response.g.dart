// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'plans_controller_list200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PlansControllerList200Response extends PlansControllerList200Response {
  @override
  final bool success;
  @override
  final BuiltList<JsonObject> data;
  @override
  final PaginatedMetaDocumented meta;
  @override
  final PaginatedLinksDocumented links;

  factory _$PlansControllerList200Response(
          [void Function(PlansControllerList200ResponseBuilder)? updates]) =>
      (new PlansControllerList200ResponseBuilder()..update(updates))._build();

  _$PlansControllerList200Response._(
      {required this.success,
      required this.data,
      required this.meta,
      required this.links})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'PlansControllerList200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'PlansControllerList200Response', 'data');
    BuiltValueNullFieldError.checkNotNull(
        meta, r'PlansControllerList200Response', 'meta');
    BuiltValueNullFieldError.checkNotNull(
        links, r'PlansControllerList200Response', 'links');
  }

  @override
  PlansControllerList200Response rebuild(
          void Function(PlansControllerList200ResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PlansControllerList200ResponseBuilder toBuilder() =>
      new PlansControllerList200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PlansControllerList200Response &&
        success == other.success &&
        data == other.data &&
        meta == other.meta &&
        links == other.links;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jc(_$hash, meta.hashCode);
    _$hash = $jc(_$hash, links.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'PlansControllerList200Response')
          ..add('success', success)
          ..add('data', data)
          ..add('meta', meta)
          ..add('links', links))
        .toString();
  }
}

class PlansControllerList200ResponseBuilder
    implements
        Builder<PlansControllerList200Response,
            PlansControllerList200ResponseBuilder>,
        PaginatedDocumentedBuilder {
  _$PlansControllerList200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  ListBuilder<JsonObject>? _data;
  ListBuilder<JsonObject> get data =>
      _$this._data ??= new ListBuilder<JsonObject>();
  set data(covariant ListBuilder<JsonObject>? data) => _$this._data = data;

  PaginatedMetaDocumentedBuilder? _meta;
  PaginatedMetaDocumentedBuilder get meta =>
      _$this._meta ??= new PaginatedMetaDocumentedBuilder();
  set meta(covariant PaginatedMetaDocumentedBuilder? meta) =>
      _$this._meta = meta;

  PaginatedLinksDocumentedBuilder? _links;
  PaginatedLinksDocumentedBuilder get links =>
      _$this._links ??= new PaginatedLinksDocumentedBuilder();
  set links(covariant PaginatedLinksDocumentedBuilder? links) =>
      _$this._links = links;

  PlansControllerList200ResponseBuilder() {
    PlansControllerList200Response._defaults(this);
  }

  PlansControllerList200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data.toBuilder();
      _meta = $v.meta.toBuilder();
      _links = $v.links.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant PlansControllerList200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$PlansControllerList200Response;
  }

  @override
  void update(void Function(PlansControllerList200ResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  PlansControllerList200Response build() => _build();

  _$PlansControllerList200Response _build() {
    _$PlansControllerList200Response _$result;
    try {
      _$result = _$v ??
          new _$PlansControllerList200Response._(
              success: BuiltValueNullFieldError.checkNotNull(
                  success, r'PlansControllerList200Response', 'success'),
              data: data.build(),
              meta: meta.build(),
              links: links.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'data';
        data.build();
        _$failedField = 'meta';
        meta.build();
        _$failedField = 'links';
        links.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'PlansControllerList200Response', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
