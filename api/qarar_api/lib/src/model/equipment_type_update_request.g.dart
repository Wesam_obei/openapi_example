// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'equipment_type_update_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$EquipmentTypeUpdateRequest extends EquipmentTypeUpdateRequest {
  @override
  final String name;
  @override
  final String? fileToken;
  @override
  final bool? deleteFile;

  factory _$EquipmentTypeUpdateRequest(
          [void Function(EquipmentTypeUpdateRequestBuilder)? updates]) =>
      (new EquipmentTypeUpdateRequestBuilder()..update(updates))._build();

  _$EquipmentTypeUpdateRequest._(
      {required this.name, this.fileToken, this.deleteFile})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        name, r'EquipmentTypeUpdateRequest', 'name');
  }

  @override
  EquipmentTypeUpdateRequest rebuild(
          void Function(EquipmentTypeUpdateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  EquipmentTypeUpdateRequestBuilder toBuilder() =>
      new EquipmentTypeUpdateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EquipmentTypeUpdateRequest &&
        name == other.name &&
        fileToken == other.fileToken &&
        deleteFile == other.deleteFile;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, fileToken.hashCode);
    _$hash = $jc(_$hash, deleteFile.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'EquipmentTypeUpdateRequest')
          ..add('name', name)
          ..add('fileToken', fileToken)
          ..add('deleteFile', deleteFile))
        .toString();
  }
}

class EquipmentTypeUpdateRequestBuilder
    implements
        Builder<EquipmentTypeUpdateRequest, EquipmentTypeUpdateRequestBuilder> {
  _$EquipmentTypeUpdateRequest? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _fileToken;
  String? get fileToken => _$this._fileToken;
  set fileToken(String? fileToken) => _$this._fileToken = fileToken;

  bool? _deleteFile;
  bool? get deleteFile => _$this._deleteFile;
  set deleteFile(bool? deleteFile) => _$this._deleteFile = deleteFile;

  EquipmentTypeUpdateRequestBuilder() {
    EquipmentTypeUpdateRequest._defaults(this);
  }

  EquipmentTypeUpdateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _fileToken = $v.fileToken;
      _deleteFile = $v.deleteFile;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(EquipmentTypeUpdateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$EquipmentTypeUpdateRequest;
  }

  @override
  void update(void Function(EquipmentTypeUpdateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  EquipmentTypeUpdateRequest build() => _build();

  _$EquipmentTypeUpdateRequest _build() {
    final _$result = _$v ??
        new _$EquipmentTypeUpdateRequest._(
            name: BuiltValueNullFieldError.checkNotNull(
                name, r'EquipmentTypeUpdateRequest', 'name'),
            fileToken: fileToken,
            deleteFile: deleteFile);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
