//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'plan_create_request.g.dart';

/// PlanCreateRequest
///
/// Properties:
/// * [name]
/// * [description]
@BuiltValue()
abstract class PlanCreateRequest
    implements Built<PlanCreateRequest, PlanCreateRequestBuilder> {
  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'description')
  String get description;

  PlanCreateRequest._();

  factory PlanCreateRequest([void updates(PlanCreateRequestBuilder b)]) =
      _$PlanCreateRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(PlanCreateRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<PlanCreateRequest> get serializer =>
      _$PlanCreateRequestSerializer();
}

class _$PlanCreateRequestSerializer
    implements PrimitiveSerializer<PlanCreateRequest> {
  @override
  final Iterable<Type> types = const [PlanCreateRequest, _$PlanCreateRequest];

  @override
  final String wireName = r'PlanCreateRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    PlanCreateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    yield r'description';
    yield serializers.serialize(
      object.description,
      specifiedType: const FullType(String),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    PlanCreateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required PlanCreateRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'description':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.description = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  PlanCreateRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = PlanCreateRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
