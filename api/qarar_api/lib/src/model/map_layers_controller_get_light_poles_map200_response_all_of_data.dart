//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_light_poles_map200_response_all_of_data_features_inner.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_light_poles_map200_response_all_of_data.g.dart';

/// MapLayersControllerGetLightPolesMap200ResponseAllOfData
///
/// Properties:
/// * [type]
/// * [features]
@BuiltValue()
abstract class MapLayersControllerGetLightPolesMap200ResponseAllOfData
    implements
        Built<MapLayersControllerGetLightPolesMap200ResponseAllOfData,
            MapLayersControllerGetLightPolesMap200ResponseAllOfDataBuilder> {
  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum get type;
  // enum typeEnum {  FeatureCollection,  };

  @BuiltValueField(wireName: r'features')
  BuiltList<
          MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner>
      get features;

  MapLayersControllerGetLightPolesMap200ResponseAllOfData._();

  factory MapLayersControllerGetLightPolesMap200ResponseAllOfData(
      [void updates(
          MapLayersControllerGetLightPolesMap200ResponseAllOfDataBuilder
              b)]) = _$MapLayersControllerGetLightPolesMap200ResponseAllOfData;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetLightPolesMap200ResponseAllOfDataBuilder b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<MapLayersControllerGetLightPolesMap200ResponseAllOfData>
      get serializer =>
          _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataSerializer();
}

class _$MapLayersControllerGetLightPolesMap200ResponseAllOfDataSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetLightPolesMap200ResponseAllOfData> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetLightPolesMap200ResponseAllOfData,
    _$MapLayersControllerGetLightPolesMap200ResponseAllOfData
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetLightPolesMap200ResponseAllOfData';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetLightPolesMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(
          MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum),
    );
    yield r'features';
    yield serializers.serialize(
      object.features,
      specifiedType: const FullType(BuiltList, [
        FullType(
            MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner)
      ]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetLightPolesMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetLightPolesMap200ResponseAllOfDataBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum),
          ) as MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum;
          result.type = valueDes;
          break;
        case r'features':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [
              FullType(
                  MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner)
            ]),
          ) as BuiltList<
              MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner>;
          result.features.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetLightPolesMap200ResponseAllOfData deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetLightPolesMap200ResponseAllOfDataBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum
    extends EnumClass {
  @BuiltValueEnumConst(wireName: r'FeatureCollection', fallback: true)
  static const MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum
      featureCollection =
      _$mapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum_featureCollection;

  static Serializer<
          MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum>
      get serializer =>
          _$mapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnumSerializer;

  const MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum._(
      String name)
      : super(name);

  static BuiltSet<
          MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum>
      get values =>
          _$mapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnumValues;
  static MapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnum valueOf(
          String name) =>
      _$mapLayersControllerGetLightPolesMap200ResponseAllOfDataTypeEnumValueOf(
          name);
}
