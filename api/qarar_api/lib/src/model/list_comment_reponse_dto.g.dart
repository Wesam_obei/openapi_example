// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_comment_reponse_dto.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ListCommentReponseDto extends ListCommentReponseDto {
  @override
  final num id;
  @override
  final SurveyResponseAnswerDataResponse surveyResponseAnswer;
  @override
  final String comment;
  @override
  final GetUserDataResponse commentBy;
  @override
  final DateTime createdAt;

  factory _$ListCommentReponseDto(
          [void Function(ListCommentReponseDtoBuilder)? updates]) =>
      (new ListCommentReponseDtoBuilder()..update(updates))._build();

  _$ListCommentReponseDto._(
      {required this.id,
      required this.surveyResponseAnswer,
      required this.comment,
      required this.commentBy,
      required this.createdAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, r'ListCommentReponseDto', 'id');
    BuiltValueNullFieldError.checkNotNull(
        surveyResponseAnswer, r'ListCommentReponseDto', 'surveyResponseAnswer');
    BuiltValueNullFieldError.checkNotNull(
        comment, r'ListCommentReponseDto', 'comment');
    BuiltValueNullFieldError.checkNotNull(
        commentBy, r'ListCommentReponseDto', 'commentBy');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'ListCommentReponseDto', 'createdAt');
  }

  @override
  ListCommentReponseDto rebuild(
          void Function(ListCommentReponseDtoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ListCommentReponseDtoBuilder toBuilder() =>
      new ListCommentReponseDtoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ListCommentReponseDto &&
        id == other.id &&
        surveyResponseAnswer == other.surveyResponseAnswer &&
        comment == other.comment &&
        commentBy == other.commentBy &&
        createdAt == other.createdAt;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, surveyResponseAnswer.hashCode);
    _$hash = $jc(_$hash, comment.hashCode);
    _$hash = $jc(_$hash, commentBy.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ListCommentReponseDto')
          ..add('id', id)
          ..add('surveyResponseAnswer', surveyResponseAnswer)
          ..add('comment', comment)
          ..add('commentBy', commentBy)
          ..add('createdAt', createdAt))
        .toString();
  }
}

class ListCommentReponseDtoBuilder
    implements Builder<ListCommentReponseDto, ListCommentReponseDtoBuilder> {
  _$ListCommentReponseDto? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  SurveyResponseAnswerDataResponseBuilder? _surveyResponseAnswer;
  SurveyResponseAnswerDataResponseBuilder get surveyResponseAnswer =>
      _$this._surveyResponseAnswer ??=
          new SurveyResponseAnswerDataResponseBuilder();
  set surveyResponseAnswer(
          SurveyResponseAnswerDataResponseBuilder? surveyResponseAnswer) =>
      _$this._surveyResponseAnswer = surveyResponseAnswer;

  String? _comment;
  String? get comment => _$this._comment;
  set comment(String? comment) => _$this._comment = comment;

  GetUserDataResponseBuilder? _commentBy;
  GetUserDataResponseBuilder get commentBy =>
      _$this._commentBy ??= new GetUserDataResponseBuilder();
  set commentBy(GetUserDataResponseBuilder? commentBy) =>
      _$this._commentBy = commentBy;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  ListCommentReponseDtoBuilder() {
    ListCommentReponseDto._defaults(this);
  }

  ListCommentReponseDtoBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _surveyResponseAnswer = $v.surveyResponseAnswer.toBuilder();
      _comment = $v.comment;
      _commentBy = $v.commentBy.toBuilder();
      _createdAt = $v.createdAt;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ListCommentReponseDto other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ListCommentReponseDto;
  }

  @override
  void update(void Function(ListCommentReponseDtoBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ListCommentReponseDto build() => _build();

  _$ListCommentReponseDto _build() {
    _$ListCommentReponseDto _$result;
    try {
      _$result = _$v ??
          new _$ListCommentReponseDto._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'ListCommentReponseDto', 'id'),
              surveyResponseAnswer: surveyResponseAnswer.build(),
              comment: BuiltValueNullFieldError.checkNotNull(
                  comment, r'ListCommentReponseDto', 'comment'),
              commentBy: commentBy.build(),
              createdAt: BuiltValueNullFieldError.checkNotNull(
                  createdAt, r'ListCommentReponseDto', 'createdAt'));
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'surveyResponseAnswer';
        surveyResponseAnswer.build();

        _$failedField = 'commentBy';
        commentBy.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ListCommentReponseDto', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
