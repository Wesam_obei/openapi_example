// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notifications_controller_count_unread200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$NotificationsControllerCountUnread200Response
    extends NotificationsControllerCountUnread200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$NotificationsControllerCountUnread200Response(
          [void Function(NotificationsControllerCountUnread200ResponseBuilder)?
              updates]) =>
      (new NotificationsControllerCountUnread200ResponseBuilder()
            ..update(updates))
          ._build();

  _$NotificationsControllerCountUnread200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'NotificationsControllerCountUnread200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'NotificationsControllerCountUnread200Response', 'data');
  }

  @override
  NotificationsControllerCountUnread200Response rebuild(
          void Function(NotificationsControllerCountUnread200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  NotificationsControllerCountUnread200ResponseBuilder toBuilder() =>
      new NotificationsControllerCountUnread200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is NotificationsControllerCountUnread200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'NotificationsControllerCountUnread200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class NotificationsControllerCountUnread200ResponseBuilder
    implements
        Builder<NotificationsControllerCountUnread200Response,
            NotificationsControllerCountUnread200ResponseBuilder>,
        SuccessResponseBuilder {
  _$NotificationsControllerCountUnread200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  NotificationsControllerCountUnread200ResponseBuilder() {
    NotificationsControllerCountUnread200Response._defaults(this);
  }

  NotificationsControllerCountUnread200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant NotificationsControllerCountUnread200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$NotificationsControllerCountUnread200Response;
  }

  @override
  void update(
      void Function(NotificationsControllerCountUnread200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  NotificationsControllerCountUnread200Response build() => _build();

  _$NotificationsControllerCountUnread200Response _build() {
    final _$result = _$v ??
        new _$NotificationsControllerCountUnread200Response._(
            success: BuiltValueNullFieldError.checkNotNull(success,
                r'NotificationsControllerCountUnread200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(data,
                r'NotificationsControllerCountUnread200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
