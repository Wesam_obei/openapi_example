// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'equipments_types_controller_get200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$EquipmentsTypesControllerGet200Response
    extends EquipmentsTypesControllerGet200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$EquipmentsTypesControllerGet200Response(
          [void Function(EquipmentsTypesControllerGet200ResponseBuilder)?
              updates]) =>
      (new EquipmentsTypesControllerGet200ResponseBuilder()..update(updates))
          ._build();

  _$EquipmentsTypesControllerGet200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'EquipmentsTypesControllerGet200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'EquipmentsTypesControllerGet200Response', 'data');
  }

  @override
  EquipmentsTypesControllerGet200Response rebuild(
          void Function(EquipmentsTypesControllerGet200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  EquipmentsTypesControllerGet200ResponseBuilder toBuilder() =>
      new EquipmentsTypesControllerGet200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EquipmentsTypesControllerGet200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'EquipmentsTypesControllerGet200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class EquipmentsTypesControllerGet200ResponseBuilder
    implements
        Builder<EquipmentsTypesControllerGet200Response,
            EquipmentsTypesControllerGet200ResponseBuilder>,
        SuccessResponseBuilder {
  _$EquipmentsTypesControllerGet200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  EquipmentsTypesControllerGet200ResponseBuilder() {
    EquipmentsTypesControllerGet200Response._defaults(this);
  }

  EquipmentsTypesControllerGet200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant EquipmentsTypesControllerGet200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$EquipmentsTypesControllerGet200Response;
  }

  @override
  void update(
      void Function(EquipmentsTypesControllerGet200ResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  EquipmentsTypesControllerGet200Response build() => _build();

  _$EquipmentsTypesControllerGet200Response _build() {
    final _$result = _$v ??
        new _$EquipmentsTypesControllerGet200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success, r'EquipmentsTypesControllerGet200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'EquipmentsTypesControllerGet200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
