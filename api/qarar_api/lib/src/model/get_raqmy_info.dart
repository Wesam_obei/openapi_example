//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'get_raqmy_info.g.dart';

/// GetRaqmyInfo
///
/// Properties:
/// * [employeeId]
@BuiltValue()
abstract class GetRaqmyInfo
    implements Built<GetRaqmyInfo, GetRaqmyInfoBuilder> {
  @BuiltValueField(wireName: r'employee_id')
  num get employeeId;

  GetRaqmyInfo._();

  factory GetRaqmyInfo([void updates(GetRaqmyInfoBuilder b)]) = _$GetRaqmyInfo;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(GetRaqmyInfoBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<GetRaqmyInfo> get serializer => _$GetRaqmyInfoSerializer();
}

class _$GetRaqmyInfoSerializer implements PrimitiveSerializer<GetRaqmyInfo> {
  @override
  final Iterable<Type> types = const [GetRaqmyInfo, _$GetRaqmyInfo];

  @override
  final String wireName = r'GetRaqmyInfo';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    GetRaqmyInfo object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'employee_id';
    yield serializers.serialize(
      object.employeeId,
      specifiedType: const FullType(num),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    GetRaqmyInfo object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required GetRaqmyInfoBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'employee_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.employeeId = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  GetRaqmyInfo deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = GetRaqmyInfoBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
