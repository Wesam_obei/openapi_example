//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/json_object.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'survey_response_answer_data_response.g.dart';

/// SurveyResponseAnswerDataResponse
///
/// Properties:
/// * [id]
/// * [parentAnswerId]
/// * [order]
/// * [value]
/// * [formComponentId]
@BuiltValue()
abstract class SurveyResponseAnswerDataResponse
    implements
        Built<SurveyResponseAnswerDataResponse,
            SurveyResponseAnswerDataResponseBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'parent_answer_id')
  num get parentAnswerId;

  @BuiltValueField(wireName: r'order')
  num get order;

  @BuiltValueField(wireName: r'value')
  JsonObject get value;

  @BuiltValueField(wireName: r'form_component_id')
  num get formComponentId;

  SurveyResponseAnswerDataResponse._();

  factory SurveyResponseAnswerDataResponse(
          [void updates(SurveyResponseAnswerDataResponseBuilder b)]) =
      _$SurveyResponseAnswerDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(SurveyResponseAnswerDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<SurveyResponseAnswerDataResponse> get serializer =>
      _$SurveyResponseAnswerDataResponseSerializer();
}

class _$SurveyResponseAnswerDataResponseSerializer
    implements PrimitiveSerializer<SurveyResponseAnswerDataResponse> {
  @override
  final Iterable<Type> types = const [
    SurveyResponseAnswerDataResponse,
    _$SurveyResponseAnswerDataResponse
  ];

  @override
  final String wireName = r'SurveyResponseAnswerDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    SurveyResponseAnswerDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'parent_answer_id';
    yield serializers.serialize(
      object.parentAnswerId,
      specifiedType: const FullType(num),
    );
    yield r'order';
    yield serializers.serialize(
      object.order,
      specifiedType: const FullType(num),
    );
    yield r'value';
    yield serializers.serialize(
      object.value,
      specifiedType: const FullType(JsonObject),
    );
    yield r'form_component_id';
    yield serializers.serialize(
      object.formComponentId,
      specifiedType: const FullType(num),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    SurveyResponseAnswerDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required SurveyResponseAnswerDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'parent_answer_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.parentAnswerId = valueDes;
          break;
        case r'order':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.order = valueDes;
          break;
        case r'value':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.value = valueDes;
          break;
        case r'form_component_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.formComponentId = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  SurveyResponseAnswerDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = SurveyResponseAnswerDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
