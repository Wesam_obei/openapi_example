//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/get_survey_response_data_response.dart';
import 'package:qarar_api/src/model/recipient_type.dart';
import 'package:qarar_api/src/model/get_survey_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'survey_request_data_response.g.dart';

/// SurveyRequestDataResponse
///
/// Properties:
/// * [id]
/// * [name]
/// * [description]
/// * [isActive]
/// * [startAt]
/// * [endAt]
/// * [createdAt]
/// * [response]
/// * [isDelayed]
/// * [recipientId]
/// * [recipientName]
/// * [recipientType]
/// * [survey]
@BuiltValue()
abstract class SurveyRequestDataResponse
    implements
        Built<SurveyRequestDataResponse, SurveyRequestDataResponseBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'description')
  String get description;

  @BuiltValueField(wireName: r'is_active')
  bool get isActive;

  @BuiltValueField(wireName: r'start_at')
  DateTime get startAt;

  @BuiltValueField(wireName: r'end_at')
  DateTime get endAt;

  @BuiltValueField(wireName: r'created_at')
  DateTime get createdAt;

  @BuiltValueField(wireName: r'response')
  GetSurveyResponseDataResponse get response;

  @BuiltValueField(wireName: r'is_delayed')
  bool get isDelayed;

  @BuiltValueField(wireName: r'recipient_id')
  num get recipientId;

  @BuiltValueField(wireName: r'recipient_name')
  String get recipientName;

  @BuiltValueField(wireName: r'recipient_type')
  RecipientType get recipientType;
  // enum recipientTypeEnum {  users,  entities,  };

  @BuiltValueField(wireName: r'survey')
  GetSurveyDataResponse get survey;

  SurveyRequestDataResponse._();

  factory SurveyRequestDataResponse(
          [void updates(SurveyRequestDataResponseBuilder b)]) =
      _$SurveyRequestDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(SurveyRequestDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<SurveyRequestDataResponse> get serializer =>
      _$SurveyRequestDataResponseSerializer();
}

class _$SurveyRequestDataResponseSerializer
    implements PrimitiveSerializer<SurveyRequestDataResponse> {
  @override
  final Iterable<Type> types = const [
    SurveyRequestDataResponse,
    _$SurveyRequestDataResponse
  ];

  @override
  final String wireName = r'SurveyRequestDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    SurveyRequestDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    yield r'description';
    yield serializers.serialize(
      object.description,
      specifiedType: const FullType(String),
    );
    yield r'is_active';
    yield serializers.serialize(
      object.isActive,
      specifiedType: const FullType(bool),
    );
    yield r'start_at';
    yield serializers.serialize(
      object.startAt,
      specifiedType: const FullType(DateTime),
    );
    yield r'end_at';
    yield serializers.serialize(
      object.endAt,
      specifiedType: const FullType(DateTime),
    );
    yield r'created_at';
    yield serializers.serialize(
      object.createdAt,
      specifiedType: const FullType(DateTime),
    );
    yield r'response';
    yield serializers.serialize(
      object.response,
      specifiedType: const FullType(GetSurveyResponseDataResponse),
    );
    yield r'is_delayed';
    yield serializers.serialize(
      object.isDelayed,
      specifiedType: const FullType(bool),
    );
    yield r'recipient_id';
    yield serializers.serialize(
      object.recipientId,
      specifiedType: const FullType(num),
    );
    yield r'recipient_name';
    yield serializers.serialize(
      object.recipientName,
      specifiedType: const FullType(String),
    );
    yield r'recipient_type';
    yield serializers.serialize(
      object.recipientType,
      specifiedType: const FullType(RecipientType),
    );
    yield r'survey';
    yield serializers.serialize(
      object.survey,
      specifiedType: const FullType(GetSurveyDataResponse),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    SurveyRequestDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required SurveyRequestDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'description':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.description = valueDes;
          break;
        case r'is_active':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.isActive = valueDes;
          break;
        case r'start_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.startAt = valueDes;
          break;
        case r'end_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.endAt = valueDes;
          break;
        case r'created_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.createdAt = valueDes;
          break;
        case r'response':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetSurveyResponseDataResponse),
          ) as GetSurveyResponseDataResponse;
          result.response.replace(valueDes);
          break;
        case r'is_delayed':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.isDelayed = valueDes;
          break;
        case r'recipient_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.recipientId = valueDes;
          break;
        case r'recipient_name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.recipientName = valueDes;
          break;
        case r'recipient_type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(RecipientType),
          ) as RecipientType;
          result.recipientType = valueDes;
          break;
        case r'survey':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetSurveyDataResponse),
          ) as GetSurveyDataResponse;
          result.survey.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  SurveyRequestDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = SurveyRequestDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
