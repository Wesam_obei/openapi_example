// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'base_data_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$BaseDataRequest extends BaseDataRequest {
  @override
  final Envelope envelope;
  @override
  final BuiltList<String> fields;

  factory _$BaseDataRequest([void Function(BaseDataRequestBuilder)? updates]) =>
      (new BaseDataRequestBuilder()..update(updates))._build();

  _$BaseDataRequest._({required this.envelope, required this.fields})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        envelope, r'BaseDataRequest', 'envelope');
    BuiltValueNullFieldError.checkNotNull(fields, r'BaseDataRequest', 'fields');
  }

  @override
  BaseDataRequest rebuild(void Function(BaseDataRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BaseDataRequestBuilder toBuilder() =>
      new BaseDataRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BaseDataRequest &&
        envelope == other.envelope &&
        fields == other.fields;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, envelope.hashCode);
    _$hash = $jc(_$hash, fields.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'BaseDataRequest')
          ..add('envelope', envelope)
          ..add('fields', fields))
        .toString();
  }
}

class BaseDataRequestBuilder
    implements Builder<BaseDataRequest, BaseDataRequestBuilder> {
  _$BaseDataRequest? _$v;

  EnvelopeBuilder? _envelope;
  EnvelopeBuilder get envelope => _$this._envelope ??= new EnvelopeBuilder();
  set envelope(EnvelopeBuilder? envelope) => _$this._envelope = envelope;

  ListBuilder<String>? _fields;
  ListBuilder<String> get fields =>
      _$this._fields ??= new ListBuilder<String>();
  set fields(ListBuilder<String>? fields) => _$this._fields = fields;

  BaseDataRequestBuilder() {
    BaseDataRequest._defaults(this);
  }

  BaseDataRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _envelope = $v.envelope.toBuilder();
      _fields = $v.fields.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BaseDataRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BaseDataRequest;
  }

  @override
  void update(void Function(BaseDataRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  BaseDataRequest build() => _build();

  _$BaseDataRequest _build() {
    _$BaseDataRequest _$result;
    try {
      _$result = _$v ??
          new _$BaseDataRequest._(
              envelope: envelope.build(), fields: fields.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'envelope';
        envelope.build();
        _$failedField = 'fields';
        fields.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'BaseDataRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
