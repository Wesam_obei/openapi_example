//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/map_layers_controller_get_municipalities_map200_response_all_of_data_features_inner_properties.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_municipalities_map200_response_all_of_data_features_inner.g.dart';

/// GeoJson Feature
///
/// Properties:
/// * [type]
/// * [id]
/// * [geometry]
/// * [properties]
@BuiltValue()
abstract class MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner
    implements
        Built<
            MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner,
            MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerBuilder> {
  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerTypeEnum
      get type;
  // enum typeEnum {  Feature,  };

  @BuiltValueField(wireName: r'id')
  int get id;

  @BuiltValueField(wireName: r'geometry')
  JsonObject? get geometry;

  @BuiltValueField(wireName: r'properties')
  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerProperties
      get properties;

  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner._();

  factory MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner(
          [void updates(
              MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerBuilder
                  b)]) =
      _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerBuilder
              b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner>
      get serializer =>
          _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerSerializer();
}

class _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner,
    _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner
        object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerTypeEnum),
    );
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(int),
    );
    yield r'geometry';
    yield object.geometry == null
        ? null
        : serializers.serialize(
            object.geometry,
            specifiedType: const FullType.nullable(JsonObject),
          );
    yield r'properties';
    yield serializers.serialize(
      object.properties,
      specifiedType: const FullType(
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerProperties),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner
        object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerTypeEnum),
          ) as MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerTypeEnum;
          result.type = valueDes;
          break;
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(int),
          ) as int;
          result.id = valueDes;
          break;
        case r'geometry':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(JsonObject),
          ) as JsonObject?;
          if (valueDes == null) continue;
          result.geometry = valueDes;
          break;
        case r'properties':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerProperties),
          ) as MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerProperties;
          result.properties.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInner
      deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerTypeEnum
    extends EnumClass {
  @BuiltValueEnumConst(wireName: r'Feature', fallback: true)
  static const MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerTypeEnum
      feature =
      _$mapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature;

  static Serializer<
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerTypeEnum>
      get serializer =>
          _$mapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer;

  const MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerTypeEnum._(
      String name)
      : super(name);

  static BuiltSet<
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerTypeEnum>
      get values =>
          _$mapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerTypeEnumValues;
  static MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerTypeEnum
      valueOf(String name) =>
          _$mapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerTypeEnumValueOf(
              name);
}
