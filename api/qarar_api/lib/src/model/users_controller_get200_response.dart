//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/success_response.dart';
import 'package:qarar_api/src/model/get_user_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'users_controller_get200_response.g.dart';

/// My custom description
///
/// Properties:
/// * [success]
/// * [data]
@BuiltValue()
abstract class UsersControllerGet200Response
    implements
        SuccessResponse,
        Built<UsersControllerGet200Response,
            UsersControllerGet200ResponseBuilder> {
  UsersControllerGet200Response._();

  factory UsersControllerGet200Response(
          [void updates(UsersControllerGet200ResponseBuilder b)]) =
      _$UsersControllerGet200Response;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(UsersControllerGet200ResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<UsersControllerGet200Response> get serializer =>
      _$UsersControllerGet200ResponseSerializer();
}

class _$UsersControllerGet200ResponseSerializer
    implements PrimitiveSerializer<UsersControllerGet200Response> {
  @override
  final Iterable<Type> types = const [
    UsersControllerGet200Response,
    _$UsersControllerGet200Response
  ];

  @override
  final String wireName = r'UsersControllerGet200Response';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    UsersControllerGet200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'data';
    yield serializers.serialize(
      object.data,
      specifiedType: const FullType(JsonObject),
    );
    yield r'success';
    yield serializers.serialize(
      object.success,
      specifiedType: const FullType(bool),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    UsersControllerGet200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required UsersControllerGet200ResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'data':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.data = valueDes;
          break;
        case r'success':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.success = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  UsersControllerGet200Response deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = UsersControllerGet200ResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
