// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'users_controller_employees_list200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$UsersControllerEmployeesList200Response
    extends UsersControllerEmployeesList200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$UsersControllerEmployeesList200Response(
          [void Function(UsersControllerEmployeesList200ResponseBuilder)?
              updates]) =>
      (new UsersControllerEmployeesList200ResponseBuilder()..update(updates))
          ._build();

  _$UsersControllerEmployeesList200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'UsersControllerEmployeesList200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'UsersControllerEmployeesList200Response', 'data');
  }

  @override
  UsersControllerEmployeesList200Response rebuild(
          void Function(UsersControllerEmployeesList200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UsersControllerEmployeesList200ResponseBuilder toBuilder() =>
      new UsersControllerEmployeesList200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UsersControllerEmployeesList200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'UsersControllerEmployeesList200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class UsersControllerEmployeesList200ResponseBuilder
    implements
        Builder<UsersControllerEmployeesList200Response,
            UsersControllerEmployeesList200ResponseBuilder>,
        SuccessResponseBuilder {
  _$UsersControllerEmployeesList200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  UsersControllerEmployeesList200ResponseBuilder() {
    UsersControllerEmployeesList200Response._defaults(this);
  }

  UsersControllerEmployeesList200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant UsersControllerEmployeesList200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$UsersControllerEmployeesList200Response;
  }

  @override
  void update(
      void Function(UsersControllerEmployeesList200ResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  UsersControllerEmployeesList200Response build() => _build();

  _$UsersControllerEmployeesList200Response _build() {
    final _$result = _$v ??
        new _$UsersControllerEmployeesList200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success, r'UsersControllerEmployeesList200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'UsersControllerEmployeesList200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
