// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_bridges_tunnels_sites_map200_response_all_of_data_features_inner.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum
    _$mapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature =
    const MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum
        ._('feature');

MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum
    _$mapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'feature':
      return _$mapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature;
    default:
      return _$mapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature;
  }
}

final BuiltSet<
        MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum>
    _$mapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum>(const <MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum>[
  _$mapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature,
]);

Serializer<
        MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum>
    _$mapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer =
    new _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer();

class _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'feature': 'Feature',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'Feature': 'feature',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum';

  @override
  Object serialize(
          Serializers serializers,
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum
              object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum
      deserialize(Serializers serializers, Object serialized,
              {FullType specifiedType = FullType.unspecified}) =>
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .valueOf(_fromWire[serialized] ??
                  (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner
    extends MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner {
  @override
  final MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum
      type;
  @override
  final int id;
  @override
  final JsonObject? geometry;
  @override
  final MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties
      properties;

  factory _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner(
          [void Function(
                  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerBuilder)?
              updates]) =>
      (new MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner._(
      {required this.type,
      required this.id,
      this.geometry,
      required this.properties})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        type,
        r'MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner',
        'type');
    BuiltValueNullFieldError.checkNotNull(
        id,
        r'MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner',
        'id');
    BuiltValueNullFieldError.checkNotNull(
        properties,
        r'MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner',
        'properties');
  }

  @override
  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner
      rebuild(
              void Function(
                      MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerBuilder)
                  updates) =>
          (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerBuilder
      toBuilder() =>
          new MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner &&
        type == other.type &&
        id == other.id &&
        geometry == other.geometry &&
        properties == other.properties;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, geometry.hashCode);
    _$hash = $jc(_$hash, properties.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner')
          ..add('type', type)
          ..add('id', id)
          ..add('geometry', geometry)
          ..add('properties', properties))
        .toString();
  }
}

class MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerBuilder
    implements
        Builder<
            MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner,
            MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerBuilder> {
  _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner?
      _$v;

  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum?
      _type;
  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum?
      get type => _$this._type;
  set type(
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerTypeEnum?
              type) =>
      _$this._type = type;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  JsonObject? _geometry;
  JsonObject? get geometry => _$this._geometry;
  set geometry(JsonObject? geometry) => _$this._geometry = geometry;

  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder?
      _properties;
  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      get properties => _$this._properties ??=
          new MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder();
  set properties(
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder?
              properties) =>
      _$this._properties = properties;

  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerBuilder() {
    MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner
        ._defaults(this);
  }

  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _id = $v.id;
      _geometry = $v.geometry;
      _properties = $v.properties.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner
      build() => _build();

  _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner
      _build() {
    _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner
        _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner',
                  'type'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id,
                  r'MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner',
                  'id'),
              geometry: geometry,
              properties: properties.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'properties';
        properties.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
