// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_survey_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ListSurveyDataResponse extends ListSurveyDataResponse {
  @override
  final num id;
  @override
  final String name;
  @override
  final String description;
  @override
  final bool isActive;
  @override
  final DateTime startAt;
  @override
  final DateTime endAt;
  @override
  final DateTime createdAt;
  @override
  final num numberOfResponses;
  @override
  final num numberOfRecipients;
  @override
  final GetUserDataResponse creator;
  @override
  final GetFormDataResponse form;
  @override
  final BuiltList<GetSurveyRecipientViewDataResponse> recipients;

  factory _$ListSurveyDataResponse(
          [void Function(ListSurveyDataResponseBuilder)? updates]) =>
      (new ListSurveyDataResponseBuilder()..update(updates))._build();

  _$ListSurveyDataResponse._(
      {required this.id,
      required this.name,
      required this.description,
      required this.isActive,
      required this.startAt,
      required this.endAt,
      required this.createdAt,
      required this.numberOfResponses,
      required this.numberOfRecipients,
      required this.creator,
      required this.form,
      required this.recipients})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, r'ListSurveyDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, r'ListSurveyDataResponse', 'name');
    BuiltValueNullFieldError.checkNotNull(
        description, r'ListSurveyDataResponse', 'description');
    BuiltValueNullFieldError.checkNotNull(
        isActive, r'ListSurveyDataResponse', 'isActive');
    BuiltValueNullFieldError.checkNotNull(
        startAt, r'ListSurveyDataResponse', 'startAt');
    BuiltValueNullFieldError.checkNotNull(
        endAt, r'ListSurveyDataResponse', 'endAt');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'ListSurveyDataResponse', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        numberOfResponses, r'ListSurveyDataResponse', 'numberOfResponses');
    BuiltValueNullFieldError.checkNotNull(
        numberOfRecipients, r'ListSurveyDataResponse', 'numberOfRecipients');
    BuiltValueNullFieldError.checkNotNull(
        creator, r'ListSurveyDataResponse', 'creator');
    BuiltValueNullFieldError.checkNotNull(
        form, r'ListSurveyDataResponse', 'form');
    BuiltValueNullFieldError.checkNotNull(
        recipients, r'ListSurveyDataResponse', 'recipients');
  }

  @override
  ListSurveyDataResponse rebuild(
          void Function(ListSurveyDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ListSurveyDataResponseBuilder toBuilder() =>
      new ListSurveyDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ListSurveyDataResponse &&
        id == other.id &&
        name == other.name &&
        description == other.description &&
        isActive == other.isActive &&
        startAt == other.startAt &&
        endAt == other.endAt &&
        createdAt == other.createdAt &&
        numberOfResponses == other.numberOfResponses &&
        numberOfRecipients == other.numberOfRecipients &&
        creator == other.creator &&
        form == other.form &&
        recipients == other.recipients;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, isActive.hashCode);
    _$hash = $jc(_$hash, startAt.hashCode);
    _$hash = $jc(_$hash, endAt.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, numberOfResponses.hashCode);
    _$hash = $jc(_$hash, numberOfRecipients.hashCode);
    _$hash = $jc(_$hash, creator.hashCode);
    _$hash = $jc(_$hash, form.hashCode);
    _$hash = $jc(_$hash, recipients.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ListSurveyDataResponse')
          ..add('id', id)
          ..add('name', name)
          ..add('description', description)
          ..add('isActive', isActive)
          ..add('startAt', startAt)
          ..add('endAt', endAt)
          ..add('createdAt', createdAt)
          ..add('numberOfResponses', numberOfResponses)
          ..add('numberOfRecipients', numberOfRecipients)
          ..add('creator', creator)
          ..add('form', form)
          ..add('recipients', recipients))
        .toString();
  }
}

class ListSurveyDataResponseBuilder
    implements Builder<ListSurveyDataResponse, ListSurveyDataResponseBuilder> {
  _$ListSurveyDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  bool? _isActive;
  bool? get isActive => _$this._isActive;
  set isActive(bool? isActive) => _$this._isActive = isActive;

  DateTime? _startAt;
  DateTime? get startAt => _$this._startAt;
  set startAt(DateTime? startAt) => _$this._startAt = startAt;

  DateTime? _endAt;
  DateTime? get endAt => _$this._endAt;
  set endAt(DateTime? endAt) => _$this._endAt = endAt;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  num? _numberOfResponses;
  num? get numberOfResponses => _$this._numberOfResponses;
  set numberOfResponses(num? numberOfResponses) =>
      _$this._numberOfResponses = numberOfResponses;

  num? _numberOfRecipients;
  num? get numberOfRecipients => _$this._numberOfRecipients;
  set numberOfRecipients(num? numberOfRecipients) =>
      _$this._numberOfRecipients = numberOfRecipients;

  GetUserDataResponseBuilder? _creator;
  GetUserDataResponseBuilder get creator =>
      _$this._creator ??= new GetUserDataResponseBuilder();
  set creator(GetUserDataResponseBuilder? creator) => _$this._creator = creator;

  GetFormDataResponseBuilder? _form;
  GetFormDataResponseBuilder get form =>
      _$this._form ??= new GetFormDataResponseBuilder();
  set form(GetFormDataResponseBuilder? form) => _$this._form = form;

  ListBuilder<GetSurveyRecipientViewDataResponse>? _recipients;
  ListBuilder<GetSurveyRecipientViewDataResponse> get recipients =>
      _$this._recipients ??=
          new ListBuilder<GetSurveyRecipientViewDataResponse>();
  set recipients(ListBuilder<GetSurveyRecipientViewDataResponse>? recipients) =>
      _$this._recipients = recipients;

  ListSurveyDataResponseBuilder() {
    ListSurveyDataResponse._defaults(this);
  }

  ListSurveyDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _description = $v.description;
      _isActive = $v.isActive;
      _startAt = $v.startAt;
      _endAt = $v.endAt;
      _createdAt = $v.createdAt;
      _numberOfResponses = $v.numberOfResponses;
      _numberOfRecipients = $v.numberOfRecipients;
      _creator = $v.creator.toBuilder();
      _form = $v.form.toBuilder();
      _recipients = $v.recipients.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ListSurveyDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ListSurveyDataResponse;
  }

  @override
  void update(void Function(ListSurveyDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ListSurveyDataResponse build() => _build();

  _$ListSurveyDataResponse _build() {
    _$ListSurveyDataResponse _$result;
    try {
      _$result = _$v ??
          new _$ListSurveyDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'ListSurveyDataResponse', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'ListSurveyDataResponse', 'name'),
              description: BuiltValueNullFieldError.checkNotNull(
                  description, r'ListSurveyDataResponse', 'description'),
              isActive: BuiltValueNullFieldError.checkNotNull(
                  isActive, r'ListSurveyDataResponse', 'isActive'),
              startAt: BuiltValueNullFieldError.checkNotNull(
                  startAt, r'ListSurveyDataResponse', 'startAt'),
              endAt: BuiltValueNullFieldError.checkNotNull(
                  endAt, r'ListSurveyDataResponse', 'endAt'),
              createdAt: BuiltValueNullFieldError.checkNotNull(
                  createdAt, r'ListSurveyDataResponse', 'createdAt'),
              numberOfResponses: BuiltValueNullFieldError.checkNotNull(
                  numberOfResponses, r'ListSurveyDataResponse', 'numberOfResponses'),
              numberOfRecipients:
                  BuiltValueNullFieldError.checkNotNull(numberOfRecipients, r'ListSurveyDataResponse', 'numberOfRecipients'),
              creator: creator.build(),
              form: form.build(),
              recipients: recipients.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'creator';
        creator.build();
        _$failedField = 'form';
        form.build();
        _$failedField = 'recipients';
        recipients.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ListSurveyDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
