// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_bridges_tunnels_sites_map200_response_all_of_data_features_inner_properties.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties
    extends MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties {
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      id;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      projectName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      devSiteDate;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      devsiteDate;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      compRat;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      primDate;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      finalDate;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      projState;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      stages;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      devWebsiteDate;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      primDateNew;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      finalDateNew;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      primDateMun;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      primDateExc;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      finalDateMun;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      finalDateExc;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      devWebsiteDateMun;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      devWebsiteDateExc;

  factory _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties(
          [void Function(
                  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
              updates]) =>
      (new MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties._(
      {this.id,
      this.projectName,
      this.devSiteDate,
      this.devsiteDate,
      this.compRat,
      this.primDate,
      this.finalDate,
      this.projState,
      this.stages,
      this.devWebsiteDate,
      this.primDateNew,
      this.finalDateNew,
      this.primDateMun,
      this.primDateExc,
      this.finalDateMun,
      this.finalDateExc,
      this.devWebsiteDateMun,
      this.devWebsiteDateExc})
      : super._();

  @override
  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties
      rebuild(
              void Function(
                      MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)
                  updates) =>
          (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      toBuilder() =>
          new MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties &&
        id == other.id &&
        projectName == other.projectName &&
        devSiteDate == other.devSiteDate &&
        devsiteDate == other.devsiteDate &&
        compRat == other.compRat &&
        primDate == other.primDate &&
        finalDate == other.finalDate &&
        projState == other.projState &&
        stages == other.stages &&
        devWebsiteDate == other.devWebsiteDate &&
        primDateNew == other.primDateNew &&
        finalDateNew == other.finalDateNew &&
        primDateMun == other.primDateMun &&
        primDateExc == other.primDateExc &&
        finalDateMun == other.finalDateMun &&
        finalDateExc == other.finalDateExc &&
        devWebsiteDateMun == other.devWebsiteDateMun &&
        devWebsiteDateExc == other.devWebsiteDateExc;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, projectName.hashCode);
    _$hash = $jc(_$hash, devSiteDate.hashCode);
    _$hash = $jc(_$hash, devsiteDate.hashCode);
    _$hash = $jc(_$hash, compRat.hashCode);
    _$hash = $jc(_$hash, primDate.hashCode);
    _$hash = $jc(_$hash, finalDate.hashCode);
    _$hash = $jc(_$hash, projState.hashCode);
    _$hash = $jc(_$hash, stages.hashCode);
    _$hash = $jc(_$hash, devWebsiteDate.hashCode);
    _$hash = $jc(_$hash, primDateNew.hashCode);
    _$hash = $jc(_$hash, finalDateNew.hashCode);
    _$hash = $jc(_$hash, primDateMun.hashCode);
    _$hash = $jc(_$hash, primDateExc.hashCode);
    _$hash = $jc(_$hash, finalDateMun.hashCode);
    _$hash = $jc(_$hash, finalDateExc.hashCode);
    _$hash = $jc(_$hash, devWebsiteDateMun.hashCode);
    _$hash = $jc(_$hash, devWebsiteDateExc.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties')
          ..add('id', id)
          ..add('projectName', projectName)
          ..add('devSiteDate', devSiteDate)
          ..add('devsiteDate', devsiteDate)
          ..add('compRat', compRat)
          ..add('primDate', primDate)
          ..add('finalDate', finalDate)
          ..add('projState', projState)
          ..add('stages', stages)
          ..add('devWebsiteDate', devWebsiteDate)
          ..add('primDateNew', primDateNew)
          ..add('finalDateNew', finalDateNew)
          ..add('primDateMun', primDateMun)
          ..add('primDateExc', primDateExc)
          ..add('finalDateMun', finalDateMun)
          ..add('finalDateExc', finalDateExc)
          ..add('devWebsiteDateMun', devWebsiteDateMun)
          ..add('devWebsiteDateExc', devWebsiteDateExc))
        .toString();
  }
}

class MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
    implements
        Builder<
            MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties,
            MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder> {
  _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties?
      _$v;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _id;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get id => _$this._id ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set id(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              id) =>
      _$this._id = id;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _projectName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get projectName => _$this._projectName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set projectName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              projectName) =>
      _$this._projectName = projectName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _devSiteDate;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get devSiteDate => _$this._devSiteDate ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set devSiteDate(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              devSiteDate) =>
      _$this._devSiteDate = devSiteDate;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _devsiteDate;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get devsiteDate => _$this._devsiteDate ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set devsiteDate(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              devsiteDate) =>
      _$this._devsiteDate = devsiteDate;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _compRat;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get compRat => _$this._compRat ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set compRat(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              compRat) =>
      _$this._compRat = compRat;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _primDate;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get primDate => _$this._primDate ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set primDate(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              primDate) =>
      _$this._primDate = primDate;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _finalDate;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get finalDate => _$this._finalDate ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set finalDate(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              finalDate) =>
      _$this._finalDate = finalDate;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _projState;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get projState => _$this._projState ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set projState(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              projState) =>
      _$this._projState = projState;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _stages;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get stages => _$this._stages ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set stages(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              stages) =>
      _$this._stages = stages;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _devWebsiteDate;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get devWebsiteDate => _$this._devWebsiteDate ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set devWebsiteDate(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              devWebsiteDate) =>
      _$this._devWebsiteDate = devWebsiteDate;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _primDateNew;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get primDateNew => _$this._primDateNew ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set primDateNew(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              primDateNew) =>
      _$this._primDateNew = primDateNew;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _finalDateNew;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get finalDateNew => _$this._finalDateNew ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set finalDateNew(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              finalDateNew) =>
      _$this._finalDateNew = finalDateNew;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _primDateMun;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get primDateMun => _$this._primDateMun ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set primDateMun(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              primDateMun) =>
      _$this._primDateMun = primDateMun;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _primDateExc;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get primDateExc => _$this._primDateExc ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set primDateExc(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              primDateExc) =>
      _$this._primDateExc = primDateExc;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _finalDateMun;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get finalDateMun => _$this._finalDateMun ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set finalDateMun(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              finalDateMun) =>
      _$this._finalDateMun = finalDateMun;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _finalDateExc;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get finalDateExc => _$this._finalDateExc ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set finalDateExc(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              finalDateExc) =>
      _$this._finalDateExc = finalDateExc;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _devWebsiteDateMun;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get devWebsiteDateMun => _$this._devWebsiteDateMun ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set devWebsiteDateMun(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              devWebsiteDateMun) =>
      _$this._devWebsiteDateMun = devWebsiteDateMun;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _devWebsiteDateExc;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get devWebsiteDateExc => _$this._devWebsiteDateExc ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set devWebsiteDateExc(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              devWebsiteDateExc) =>
      _$this._devWebsiteDateExc = devWebsiteDateExc;

  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder() {
    MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties
        ._defaults(this);
  }

  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id?.toBuilder();
      _projectName = $v.projectName?.toBuilder();
      _devSiteDate = $v.devSiteDate?.toBuilder();
      _devsiteDate = $v.devsiteDate?.toBuilder();
      _compRat = $v.compRat?.toBuilder();
      _primDate = $v.primDate?.toBuilder();
      _finalDate = $v.finalDate?.toBuilder();
      _projState = $v.projState?.toBuilder();
      _stages = $v.stages?.toBuilder();
      _devWebsiteDate = $v.devWebsiteDate?.toBuilder();
      _primDateNew = $v.primDateNew?.toBuilder();
      _finalDateNew = $v.finalDateNew?.toBuilder();
      _primDateMun = $v.primDateMun?.toBuilder();
      _primDateExc = $v.primDateExc?.toBuilder();
      _finalDateMun = $v.finalDateMun?.toBuilder();
      _finalDateExc = $v.finalDateExc?.toBuilder();
      _devWebsiteDateMun = $v.devWebsiteDateMun?.toBuilder();
      _devWebsiteDateExc = $v.devWebsiteDateExc?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties
      build() => _build();

  _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties
      _build() {
    _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties
        _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties
              ._(
              id: _id?.build(),
              projectName: _projectName?.build(),
              devSiteDate: _devSiteDate?.build(),
              devsiteDate: _devsiteDate?.build(),
              compRat: _compRat?.build(),
              primDate: _primDate?.build(),
              finalDate: _finalDate?.build(),
              projState: _projState?.build(),
              stages: _stages?.build(),
              devWebsiteDate: _devWebsiteDate?.build(),
              primDateNew: _primDateNew?.build(),
              finalDateNew: _finalDateNew?.build(),
              primDateMun: _primDateMun?.build(),
              primDateExc: _primDateExc?.build(),
              finalDateMun: _finalDateMun?.build(),
              finalDateExc: _finalDateExc?.build(),
              devWebsiteDateMun: _devWebsiteDateMun?.build(),
              devWebsiteDateExc: _devWebsiteDateExc?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'id';
        _id?.build();
        _$failedField = 'projectName';
        _projectName?.build();
        _$failedField = 'devSiteDate';
        _devSiteDate?.build();
        _$failedField = 'devsiteDate';
        _devsiteDate?.build();
        _$failedField = 'compRat';
        _compRat?.build();
        _$failedField = 'primDate';
        _primDate?.build();
        _$failedField = 'finalDate';
        _finalDate?.build();
        _$failedField = 'projState';
        _projState?.build();
        _$failedField = 'stages';
        _stages?.build();
        _$failedField = 'devWebsiteDate';
        _devWebsiteDate?.build();
        _$failedField = 'primDateNew';
        _primDateNew?.build();
        _$failedField = 'finalDateNew';
        _finalDateNew?.build();
        _$failedField = 'primDateMun';
        _primDateMun?.build();
        _$failedField = 'primDateExc';
        _primDateExc?.build();
        _$failedField = 'finalDateMun';
        _finalDateMun?.build();
        _$failedField = 'finalDateExc';
        _finalDateExc?.build();
        _$failedField = 'devWebsiteDateMun';
        _devWebsiteDateMun?.build();
        _$failedField = 'devWebsiteDateExc';
        _devWebsiteDateExc?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
