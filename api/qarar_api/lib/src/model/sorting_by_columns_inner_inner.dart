//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'dart:core';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:one_of/one_of.dart';

part 'sorting_by_columns_inner_inner.g.dart';

/// SortingByColumnsInnerInner
@BuiltValue()
abstract class SortingByColumnsInnerInner
    implements
        Built<SortingByColumnsInnerInner, SortingByColumnsInnerInnerBuilder> {
  /// One Of [String]
  OneOf get oneOf;

  SortingByColumnsInnerInner._();

  factory SortingByColumnsInnerInner(
          [void updates(SortingByColumnsInnerInnerBuilder b)]) =
      _$SortingByColumnsInnerInner;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(SortingByColumnsInnerInnerBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<SortingByColumnsInnerInner> get serializer =>
      _$SortingByColumnsInnerInnerSerializer();
}

class _$SortingByColumnsInnerInnerSerializer
    implements PrimitiveSerializer<SortingByColumnsInnerInner> {
  @override
  final Iterable<Type> types = const [
    SortingByColumnsInnerInner,
    _$SortingByColumnsInnerInner
  ];

  @override
  final String wireName = r'SortingByColumnsInnerInner';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    SortingByColumnsInnerInner object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {}

  @override
  Object serialize(
    Serializers serializers,
    SortingByColumnsInnerInner object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final oneOf = object.oneOf;
    return serializers.serialize(oneOf.value,
        specifiedType: FullType(oneOf.valueType))!;
  }

  @override
  SortingByColumnsInnerInner deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = SortingByColumnsInnerInnerBuilder();
    Object? oneOfDataSrc;
    final targetType = const FullType(OneOf, [
      FullType(String),
      FullType(OneOf1Enum),
    ]);
    oneOfDataSrc = serialized;
    result.oneOf = serializers.deserialize(oneOfDataSrc,
        specifiedType: targetType) as OneOf;
    return result.build();
  }
}
