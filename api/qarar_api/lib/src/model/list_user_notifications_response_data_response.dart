//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/web_push_payload_data.dart';
import 'package:qarar_api/src/model/get_notification_response_data.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'list_user_notifications_response_data_response.g.dart';

/// ListUserNotificationsResponseDataResponse
///
/// Properties:
/// * [id]
/// * [title]
/// * [body]
/// * [icon]
/// * [data]
/// * [recipientId]
/// * [readAt]
/// * [trashedAt]
/// * [createdAt]
/// * [notification]
@BuiltValue()
abstract class ListUserNotificationsResponseDataResponse
    implements
        Built<ListUserNotificationsResponseDataResponse,
            ListUserNotificationsResponseDataResponseBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'title')
  String get title;

  @BuiltValueField(wireName: r'body')
  String get body;

  @BuiltValueField(wireName: r'icon')
  String get icon;

  @BuiltValueField(wireName: r'data')
  WebPushPayloadData get data;

  @BuiltValueField(wireName: r'recipient_id')
  num get recipientId;

  @BuiltValueField(wireName: r'read_at')
  JsonObject get readAt;

  @BuiltValueField(wireName: r'trashed_at')
  JsonObject get trashedAt;

  @BuiltValueField(wireName: r'created_at')
  DateTime get createdAt;

  @BuiltValueField(wireName: r'notification')
  GetNotificationResponseData get notification;

  ListUserNotificationsResponseDataResponse._();

  factory ListUserNotificationsResponseDataResponse(
          [void updates(ListUserNotificationsResponseDataResponseBuilder b)]) =
      _$ListUserNotificationsResponseDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ListUserNotificationsResponseDataResponseBuilder b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ListUserNotificationsResponseDataResponse> get serializer =>
      _$ListUserNotificationsResponseDataResponseSerializer();
}

class _$ListUserNotificationsResponseDataResponseSerializer
    implements PrimitiveSerializer<ListUserNotificationsResponseDataResponse> {
  @override
  final Iterable<Type> types = const [
    ListUserNotificationsResponseDataResponse,
    _$ListUserNotificationsResponseDataResponse
  ];

  @override
  final String wireName = r'ListUserNotificationsResponseDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ListUserNotificationsResponseDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'title';
    yield serializers.serialize(
      object.title,
      specifiedType: const FullType(String),
    );
    yield r'body';
    yield serializers.serialize(
      object.body,
      specifiedType: const FullType(String),
    );
    yield r'icon';
    yield serializers.serialize(
      object.icon,
      specifiedType: const FullType(String),
    );
    yield r'data';
    yield serializers.serialize(
      object.data,
      specifiedType: const FullType(WebPushPayloadData),
    );
    yield r'recipient_id';
    yield serializers.serialize(
      object.recipientId,
      specifiedType: const FullType(num),
    );
    yield r'read_at';
    yield serializers.serialize(
      object.readAt,
      specifiedType: const FullType(JsonObject),
    );
    yield r'trashed_at';
    yield serializers.serialize(
      object.trashedAt,
      specifiedType: const FullType(JsonObject),
    );
    yield r'created_at';
    yield serializers.serialize(
      object.createdAt,
      specifiedType: const FullType(DateTime),
    );
    yield r'notification';
    yield serializers.serialize(
      object.notification,
      specifiedType: const FullType(GetNotificationResponseData),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ListUserNotificationsResponseDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ListUserNotificationsResponseDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'title':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.title = valueDes;
          break;
        case r'body':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.body = valueDes;
          break;
        case r'icon':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.icon = valueDes;
          break;
        case r'data':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(WebPushPayloadData),
          ) as WebPushPayloadData;
          result.data.replace(valueDes);
          break;
        case r'recipient_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.recipientId = valueDes;
          break;
        case r'read_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.readAt = valueDes;
          break;
        case r'trashed_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.trashedAt = valueDes;
          break;
        case r'created_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.createdAt = valueDes;
          break;
        case r'notification':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetNotificationResponseData),
          ) as GetNotificationResponseData;
          result.notification.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ListUserNotificationsResponseDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ListUserNotificationsResponseDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
