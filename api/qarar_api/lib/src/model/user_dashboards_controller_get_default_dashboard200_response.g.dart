// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_dashboards_controller_get_default_dashboard200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$UserDashboardsControllerGetDefaultDashboard200Response
    extends UserDashboardsControllerGetDefaultDashboard200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$UserDashboardsControllerGetDefaultDashboard200Response(
          [void Function(
                  UserDashboardsControllerGetDefaultDashboard200ResponseBuilder)?
              updates]) =>
      (new UserDashboardsControllerGetDefaultDashboard200ResponseBuilder()
            ..update(updates))
          ._build();

  _$UserDashboardsControllerGetDefaultDashboard200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(success,
        r'UserDashboardsControllerGetDefaultDashboard200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(data,
        r'UserDashboardsControllerGetDefaultDashboard200Response', 'data');
  }

  @override
  UserDashboardsControllerGetDefaultDashboard200Response rebuild(
          void Function(
                  UserDashboardsControllerGetDefaultDashboard200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserDashboardsControllerGetDefaultDashboard200ResponseBuilder toBuilder() =>
      new UserDashboardsControllerGetDefaultDashboard200ResponseBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserDashboardsControllerGetDefaultDashboard200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'UserDashboardsControllerGetDefaultDashboard200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class UserDashboardsControllerGetDefaultDashboard200ResponseBuilder
    implements
        Builder<UserDashboardsControllerGetDefaultDashboard200Response,
            UserDashboardsControllerGetDefaultDashboard200ResponseBuilder>,
        SuccessResponseBuilder {
  _$UserDashboardsControllerGetDefaultDashboard200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  UserDashboardsControllerGetDefaultDashboard200ResponseBuilder() {
    UserDashboardsControllerGetDefaultDashboard200Response._defaults(this);
  }

  UserDashboardsControllerGetDefaultDashboard200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      covariant UserDashboardsControllerGetDefaultDashboard200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$UserDashboardsControllerGetDefaultDashboard200Response;
  }

  @override
  void update(
      void Function(
              UserDashboardsControllerGetDefaultDashboard200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  UserDashboardsControllerGetDefaultDashboard200Response build() => _build();

  _$UserDashboardsControllerGetDefaultDashboard200Response _build() {
    final _$result = _$v ??
        new _$UserDashboardsControllerGetDefaultDashboard200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success,
                r'UserDashboardsControllerGetDefaultDashboard200Response',
                'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data,
                r'UserDashboardsControllerGetDefaultDashboard200Response',
                'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
