//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/answer_request.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'recipient_response_request.g.dart';

/// RecipientResponseRequest
///
/// Properties:
/// * [recipientId]
/// * [status]
/// * [answers]
/// * [deletedMultiAnswerIds]
@BuiltValue()
abstract class RecipientResponseRequest
    implements
        Built<RecipientResponseRequest, RecipientResponseRequestBuilder> {
  @BuiltValueField(wireName: r'recipient_id')
  num get recipientId;

  @BuiltValueField(wireName: r'status')
  RecipientResponseRequestStatusEnum get status;
  // enum statusEnum {  draft,  submitted,  };

  @BuiltValueField(wireName: r'answers')
  BuiltList<AnswerRequest> get answers;

  @BuiltValueField(wireName: r'deletedMultiAnswerIds')
  BuiltList<num> get deletedMultiAnswerIds;

  RecipientResponseRequest._();

  factory RecipientResponseRequest(
          [void updates(RecipientResponseRequestBuilder b)]) =
      _$RecipientResponseRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(RecipientResponseRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<RecipientResponseRequest> get serializer =>
      _$RecipientResponseRequestSerializer();
}

class _$RecipientResponseRequestSerializer
    implements PrimitiveSerializer<RecipientResponseRequest> {
  @override
  final Iterable<Type> types = const [
    RecipientResponseRequest,
    _$RecipientResponseRequest
  ];

  @override
  final String wireName = r'RecipientResponseRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    RecipientResponseRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'recipient_id';
    yield serializers.serialize(
      object.recipientId,
      specifiedType: const FullType(num),
    );
    yield r'status';
    yield serializers.serialize(
      object.status,
      specifiedType: const FullType(RecipientResponseRequestStatusEnum),
    );
    yield r'answers';
    yield serializers.serialize(
      object.answers,
      specifiedType: const FullType(BuiltList, [FullType(AnswerRequest)]),
    );
    yield r'deletedMultiAnswerIds';
    yield serializers.serialize(
      object.deletedMultiAnswerIds,
      specifiedType: const FullType(BuiltList, [FullType(num)]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    RecipientResponseRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required RecipientResponseRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'recipient_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.recipientId = valueDes;
          break;
        case r'status':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(RecipientResponseRequestStatusEnum),
          ) as RecipientResponseRequestStatusEnum;
          result.status = valueDes;
          break;
        case r'answers':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(AnswerRequest)]),
          ) as BuiltList<AnswerRequest>;
          result.answers.replace(valueDes);
          break;
        case r'deletedMultiAnswerIds':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(num)]),
          ) as BuiltList<num>;
          result.deletedMultiAnswerIds.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  RecipientResponseRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = RecipientResponseRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class RecipientResponseRequestStatusEnum extends EnumClass {
  @BuiltValueEnumConst(wireName: r'draft')
  static const RecipientResponseRequestStatusEnum draft =
      _$recipientResponseRequestStatusEnum_draft;
  @BuiltValueEnumConst(wireName: r'submitted', fallback: true)
  static const RecipientResponseRequestStatusEnum submitted =
      _$recipientResponseRequestStatusEnum_submitted;

  static Serializer<RecipientResponseRequestStatusEnum> get serializer =>
      _$recipientResponseRequestStatusEnumSerializer;

  const RecipientResponseRequestStatusEnum._(String name) : super(name);

  static BuiltSet<RecipientResponseRequestStatusEnum> get values =>
      _$recipientResponseRequestStatusEnumValues;
  static RecipientResponseRequestStatusEnum valueOf(String name) =>
      _$recipientResponseRequestStatusEnumValueOf(name);
}
