//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/success_response.dart';
import 'package:qarar_api/src/model/get_equipment_store_point_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'equipments_store_points_controller_get200_response.g.dart';

/// My custom description
///
/// Properties:
/// * [success]
/// * [data]
@BuiltValue()
abstract class EquipmentsStorePointsControllerGet200Response
    implements
        SuccessResponse,
        Built<EquipmentsStorePointsControllerGet200Response,
            EquipmentsStorePointsControllerGet200ResponseBuilder> {
  EquipmentsStorePointsControllerGet200Response._();

  factory EquipmentsStorePointsControllerGet200Response(
          [void updates(
              EquipmentsStorePointsControllerGet200ResponseBuilder b)]) =
      _$EquipmentsStorePointsControllerGet200Response;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          EquipmentsStorePointsControllerGet200ResponseBuilder b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<EquipmentsStorePointsControllerGet200Response>
      get serializer =>
          _$EquipmentsStorePointsControllerGet200ResponseSerializer();
}

class _$EquipmentsStorePointsControllerGet200ResponseSerializer
    implements
        PrimitiveSerializer<EquipmentsStorePointsControllerGet200Response> {
  @override
  final Iterable<Type> types = const [
    EquipmentsStorePointsControllerGet200Response,
    _$EquipmentsStorePointsControllerGet200Response
  ];

  @override
  final String wireName = r'EquipmentsStorePointsControllerGet200Response';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    EquipmentsStorePointsControllerGet200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'data';
    yield serializers.serialize(
      object.data,
      specifiedType: const FullType(JsonObject),
    );
    yield r'success';
    yield serializers.serialize(
      object.success,
      specifiedType: const FullType(bool),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    EquipmentsStorePointsControllerGet200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required EquipmentsStorePointsControllerGet200ResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'data':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.data = valueDes;
          break;
        case r'success':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.success = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  EquipmentsStorePointsControllerGet200Response deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = EquipmentsStorePointsControllerGet200ResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
