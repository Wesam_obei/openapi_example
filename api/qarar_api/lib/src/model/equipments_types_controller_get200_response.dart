//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/success_response.dart';
import 'package:qarar_api/src/model/list_equipment_type_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'equipments_types_controller_get200_response.g.dart';

/// My custom description
///
/// Properties:
/// * [success]
/// * [data]
@BuiltValue()
abstract class EquipmentsTypesControllerGet200Response
    implements
        SuccessResponse,
        Built<EquipmentsTypesControllerGet200Response,
            EquipmentsTypesControllerGet200ResponseBuilder> {
  EquipmentsTypesControllerGet200Response._();

  factory EquipmentsTypesControllerGet200Response(
          [void updates(EquipmentsTypesControllerGet200ResponseBuilder b)]) =
      _$EquipmentsTypesControllerGet200Response;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(EquipmentsTypesControllerGet200ResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<EquipmentsTypesControllerGet200Response> get serializer =>
      _$EquipmentsTypesControllerGet200ResponseSerializer();
}

class _$EquipmentsTypesControllerGet200ResponseSerializer
    implements PrimitiveSerializer<EquipmentsTypesControllerGet200Response> {
  @override
  final Iterable<Type> types = const [
    EquipmentsTypesControllerGet200Response,
    _$EquipmentsTypesControllerGet200Response
  ];

  @override
  final String wireName = r'EquipmentsTypesControllerGet200Response';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    EquipmentsTypesControllerGet200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'data';
    yield serializers.serialize(
      object.data,
      specifiedType: const FullType(JsonObject),
    );
    yield r'success';
    yield serializers.serialize(
      object.success,
      specifiedType: const FullType(bool),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    EquipmentsTypesControllerGet200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required EquipmentsTypesControllerGet200ResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'data':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.data = valueDes;
          break;
        case r'success':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.success = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  EquipmentsTypesControllerGet200Response deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = EquipmentsTypesControllerGet200ResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
