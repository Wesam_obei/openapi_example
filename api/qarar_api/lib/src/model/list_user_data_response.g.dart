// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_user_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ListUserDataResponse extends ListUserDataResponse {
  @override
  final num id;
  @override
  final String email;
  @override
  final String fullName;
  @override
  final String jobTitle;
  @override
  final bool isActive;
  @override
  final DateTime createdAt;
  @override
  final BuiltList<GetRoleDataResponse> roles;
  @override
  final BuiltList<PermissionDataResponse> permissions;
  @override
  final BuiltList<GetEntityDataResponse> entities;
  @override
  final GetRaqmyInfo raqmyInfo;

  factory _$ListUserDataResponse(
          [void Function(ListUserDataResponseBuilder)? updates]) =>
      (new ListUserDataResponseBuilder()..update(updates))._build();

  _$ListUserDataResponse._(
      {required this.id,
      required this.email,
      required this.fullName,
      required this.jobTitle,
      required this.isActive,
      required this.createdAt,
      required this.roles,
      required this.permissions,
      required this.entities,
      required this.raqmyInfo})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, r'ListUserDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        email, r'ListUserDataResponse', 'email');
    BuiltValueNullFieldError.checkNotNull(
        fullName, r'ListUserDataResponse', 'fullName');
    BuiltValueNullFieldError.checkNotNull(
        jobTitle, r'ListUserDataResponse', 'jobTitle');
    BuiltValueNullFieldError.checkNotNull(
        isActive, r'ListUserDataResponse', 'isActive');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'ListUserDataResponse', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        roles, r'ListUserDataResponse', 'roles');
    BuiltValueNullFieldError.checkNotNull(
        permissions, r'ListUserDataResponse', 'permissions');
    BuiltValueNullFieldError.checkNotNull(
        entities, r'ListUserDataResponse', 'entities');
    BuiltValueNullFieldError.checkNotNull(
        raqmyInfo, r'ListUserDataResponse', 'raqmyInfo');
  }

  @override
  ListUserDataResponse rebuild(
          void Function(ListUserDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ListUserDataResponseBuilder toBuilder() =>
      new ListUserDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ListUserDataResponse &&
        id == other.id &&
        email == other.email &&
        fullName == other.fullName &&
        jobTitle == other.jobTitle &&
        isActive == other.isActive &&
        createdAt == other.createdAt &&
        roles == other.roles &&
        permissions == other.permissions &&
        entities == other.entities &&
        raqmyInfo == other.raqmyInfo;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, email.hashCode);
    _$hash = $jc(_$hash, fullName.hashCode);
    _$hash = $jc(_$hash, jobTitle.hashCode);
    _$hash = $jc(_$hash, isActive.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, roles.hashCode);
    _$hash = $jc(_$hash, permissions.hashCode);
    _$hash = $jc(_$hash, entities.hashCode);
    _$hash = $jc(_$hash, raqmyInfo.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ListUserDataResponse')
          ..add('id', id)
          ..add('email', email)
          ..add('fullName', fullName)
          ..add('jobTitle', jobTitle)
          ..add('isActive', isActive)
          ..add('createdAt', createdAt)
          ..add('roles', roles)
          ..add('permissions', permissions)
          ..add('entities', entities)
          ..add('raqmyInfo', raqmyInfo))
        .toString();
  }
}

class ListUserDataResponseBuilder
    implements Builder<ListUserDataResponse, ListUserDataResponseBuilder> {
  _$ListUserDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _email;
  String? get email => _$this._email;
  set email(String? email) => _$this._email = email;

  String? _fullName;
  String? get fullName => _$this._fullName;
  set fullName(String? fullName) => _$this._fullName = fullName;

  String? _jobTitle;
  String? get jobTitle => _$this._jobTitle;
  set jobTitle(String? jobTitle) => _$this._jobTitle = jobTitle;

  bool? _isActive;
  bool? get isActive => _$this._isActive;
  set isActive(bool? isActive) => _$this._isActive = isActive;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  ListBuilder<GetRoleDataResponse>? _roles;
  ListBuilder<GetRoleDataResponse> get roles =>
      _$this._roles ??= new ListBuilder<GetRoleDataResponse>();
  set roles(ListBuilder<GetRoleDataResponse>? roles) => _$this._roles = roles;

  ListBuilder<PermissionDataResponse>? _permissions;
  ListBuilder<PermissionDataResponse> get permissions =>
      _$this._permissions ??= new ListBuilder<PermissionDataResponse>();
  set permissions(ListBuilder<PermissionDataResponse>? permissions) =>
      _$this._permissions = permissions;

  ListBuilder<GetEntityDataResponse>? _entities;
  ListBuilder<GetEntityDataResponse> get entities =>
      _$this._entities ??= new ListBuilder<GetEntityDataResponse>();
  set entities(ListBuilder<GetEntityDataResponse>? entities) =>
      _$this._entities = entities;

  GetRaqmyInfoBuilder? _raqmyInfo;
  GetRaqmyInfoBuilder get raqmyInfo =>
      _$this._raqmyInfo ??= new GetRaqmyInfoBuilder();
  set raqmyInfo(GetRaqmyInfoBuilder? raqmyInfo) =>
      _$this._raqmyInfo = raqmyInfo;

  ListUserDataResponseBuilder() {
    ListUserDataResponse._defaults(this);
  }

  ListUserDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _email = $v.email;
      _fullName = $v.fullName;
      _jobTitle = $v.jobTitle;
      _isActive = $v.isActive;
      _createdAt = $v.createdAt;
      _roles = $v.roles.toBuilder();
      _permissions = $v.permissions.toBuilder();
      _entities = $v.entities.toBuilder();
      _raqmyInfo = $v.raqmyInfo.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ListUserDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ListUserDataResponse;
  }

  @override
  void update(void Function(ListUserDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ListUserDataResponse build() => _build();

  _$ListUserDataResponse _build() {
    _$ListUserDataResponse _$result;
    try {
      _$result = _$v ??
          new _$ListUserDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'ListUserDataResponse', 'id'),
              email: BuiltValueNullFieldError.checkNotNull(
                  email, r'ListUserDataResponse', 'email'),
              fullName: BuiltValueNullFieldError.checkNotNull(
                  fullName, r'ListUserDataResponse', 'fullName'),
              jobTitle: BuiltValueNullFieldError.checkNotNull(
                  jobTitle, r'ListUserDataResponse', 'jobTitle'),
              isActive: BuiltValueNullFieldError.checkNotNull(
                  isActive, r'ListUserDataResponse', 'isActive'),
              createdAt: BuiltValueNullFieldError.checkNotNull(
                  createdAt, r'ListUserDataResponse', 'createdAt'),
              roles: roles.build(),
              permissions: permissions.build(),
              entities: entities.build(),
              raqmyInfo: raqmyInfo.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'roles';
        roles.build();
        _$failedField = 'permissions';
        permissions.build();
        _$failedField = 'entities';
        entities.build();
        _$failedField = 'raqmyInfo';
        raqmyInfo.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ListUserDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
