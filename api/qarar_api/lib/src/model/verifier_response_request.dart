//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'verifier_response_request.g.dart';

/// VerifierResponseRequest
///
/// Properties:
/// * [recipientId]
/// * [status]
@BuiltValue()
abstract class VerifierResponseRequest
    implements Built<VerifierResponseRequest, VerifierResponseRequestBuilder> {
  @BuiltValueField(wireName: r'recipient_id')
  num get recipientId;

  @BuiltValueField(wireName: r'status')
  VerifierResponseRequestStatusEnum get status;
  // enum statusEnum {  approved,  need-change,  };

  VerifierResponseRequest._();

  factory VerifierResponseRequest(
          [void updates(VerifierResponseRequestBuilder b)]) =
      _$VerifierResponseRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(VerifierResponseRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<VerifierResponseRequest> get serializer =>
      _$VerifierResponseRequestSerializer();
}

class _$VerifierResponseRequestSerializer
    implements PrimitiveSerializer<VerifierResponseRequest> {
  @override
  final Iterable<Type> types = const [
    VerifierResponseRequest,
    _$VerifierResponseRequest
  ];

  @override
  final String wireName = r'VerifierResponseRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    VerifierResponseRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'recipient_id';
    yield serializers.serialize(
      object.recipientId,
      specifiedType: const FullType(num),
    );
    yield r'status';
    yield serializers.serialize(
      object.status,
      specifiedType: const FullType(VerifierResponseRequestStatusEnum),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    VerifierResponseRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required VerifierResponseRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'recipient_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.recipientId = valueDes;
          break;
        case r'status':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(VerifierResponseRequestStatusEnum),
          ) as VerifierResponseRequestStatusEnum;
          result.status = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  VerifierResponseRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = VerifierResponseRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class VerifierResponseRequestStatusEnum extends EnumClass {
  @BuiltValueEnumConst(wireName: r'approved')
  static const VerifierResponseRequestStatusEnum approved =
      _$verifierResponseRequestStatusEnum_approved;
  @BuiltValueEnumConst(wireName: r'need-change', fallback: true)
  static const VerifierResponseRequestStatusEnum needChange =
      _$verifierResponseRequestStatusEnum_needChange;

  static Serializer<VerifierResponseRequestStatusEnum> get serializer =>
      _$verifierResponseRequestStatusEnumSerializer;

  const VerifierResponseRequestStatusEnum._(String name) : super(name);

  static BuiltSet<VerifierResponseRequestStatusEnum> get values =>
      _$verifierResponseRequestStatusEnumValues;
  static VerifierResponseRequestStatusEnum valueOf(String name) =>
      _$verifierResponseRequestStatusEnumValueOf(name);
}
