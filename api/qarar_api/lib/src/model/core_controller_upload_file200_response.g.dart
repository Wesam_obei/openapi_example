// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'core_controller_upload_file200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$CoreControllerUploadFile200Response
    extends CoreControllerUploadFile200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$CoreControllerUploadFile200Response(
          [void Function(CoreControllerUploadFile200ResponseBuilder)?
              updates]) =>
      (new CoreControllerUploadFile200ResponseBuilder()..update(updates))
          ._build();

  _$CoreControllerUploadFile200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'CoreControllerUploadFile200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'CoreControllerUploadFile200Response', 'data');
  }

  @override
  CoreControllerUploadFile200Response rebuild(
          void Function(CoreControllerUploadFile200ResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CoreControllerUploadFile200ResponseBuilder toBuilder() =>
      new CoreControllerUploadFile200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CoreControllerUploadFile200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'CoreControllerUploadFile200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class CoreControllerUploadFile200ResponseBuilder
    implements
        Builder<CoreControllerUploadFile200Response,
            CoreControllerUploadFile200ResponseBuilder>,
        SuccessResponseBuilder {
  _$CoreControllerUploadFile200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  CoreControllerUploadFile200ResponseBuilder() {
    CoreControllerUploadFile200Response._defaults(this);
  }

  CoreControllerUploadFile200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant CoreControllerUploadFile200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$CoreControllerUploadFile200Response;
  }

  @override
  void update(
      void Function(CoreControllerUploadFile200ResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  CoreControllerUploadFile200Response build() => _build();

  _$CoreControllerUploadFile200Response _build() {
    final _$result = _$v ??
        new _$CoreControllerUploadFile200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success, r'CoreControllerUploadFile200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'CoreControllerUploadFile200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
