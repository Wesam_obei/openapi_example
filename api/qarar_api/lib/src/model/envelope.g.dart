// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'envelope.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$Envelope extends Envelope {
  @override
  final num xmin;
  @override
  final num ymin;
  @override
  final num xmax;
  @override
  final num ymax;

  factory _$Envelope([void Function(EnvelopeBuilder)? updates]) =>
      (new EnvelopeBuilder()..update(updates))._build();

  _$Envelope._(
      {required this.xmin,
      required this.ymin,
      required this.xmax,
      required this.ymax})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(xmin, r'Envelope', 'xmin');
    BuiltValueNullFieldError.checkNotNull(ymin, r'Envelope', 'ymin');
    BuiltValueNullFieldError.checkNotNull(xmax, r'Envelope', 'xmax');
    BuiltValueNullFieldError.checkNotNull(ymax, r'Envelope', 'ymax');
  }

  @override
  Envelope rebuild(void Function(EnvelopeBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  EnvelopeBuilder toBuilder() => new EnvelopeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Envelope &&
        xmin == other.xmin &&
        ymin == other.ymin &&
        xmax == other.xmax &&
        ymax == other.ymax;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, xmin.hashCode);
    _$hash = $jc(_$hash, ymin.hashCode);
    _$hash = $jc(_$hash, xmax.hashCode);
    _$hash = $jc(_$hash, ymax.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Envelope')
          ..add('xmin', xmin)
          ..add('ymin', ymin)
          ..add('xmax', xmax)
          ..add('ymax', ymax))
        .toString();
  }
}

class EnvelopeBuilder implements Builder<Envelope, EnvelopeBuilder> {
  _$Envelope? _$v;

  num? _xmin;
  num? get xmin => _$this._xmin;
  set xmin(num? xmin) => _$this._xmin = xmin;

  num? _ymin;
  num? get ymin => _$this._ymin;
  set ymin(num? ymin) => _$this._ymin = ymin;

  num? _xmax;
  num? get xmax => _$this._xmax;
  set xmax(num? xmax) => _$this._xmax = xmax;

  num? _ymax;
  num? get ymax => _$this._ymax;
  set ymax(num? ymax) => _$this._ymax = ymax;

  EnvelopeBuilder() {
    Envelope._defaults(this);
  }

  EnvelopeBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _xmin = $v.xmin;
      _ymin = $v.ymin;
      _xmax = $v.xmax;
      _ymax = $v.ymax;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Envelope other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Envelope;
  }

  @override
  void update(void Function(EnvelopeBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Envelope build() => _build();

  _$Envelope _build() {
    final _$result = _$v ??
        new _$Envelope._(
            xmin: BuiltValueNullFieldError.checkNotNull(
                xmin, r'Envelope', 'xmin'),
            ymin: BuiltValueNullFieldError.checkNotNull(
                ymin, r'Envelope', 'ymin'),
            xmax: BuiltValueNullFieldError.checkNotNull(
                xmax, r'Envelope', 'xmax'),
            ymax: BuiltValueNullFieldError.checkNotNull(
                ymax, r'Envelope', 'ymax'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
