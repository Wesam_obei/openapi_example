// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'survey_response_answer_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SurveyResponseAnswerDataResponse
    extends SurveyResponseAnswerDataResponse {
  @override
  final num id;
  @override
  final num parentAnswerId;
  @override
  final num order;
  @override
  final JsonObject value;
  @override
  final num formComponentId;

  factory _$SurveyResponseAnswerDataResponse(
          [void Function(SurveyResponseAnswerDataResponseBuilder)? updates]) =>
      (new SurveyResponseAnswerDataResponseBuilder()..update(updates))._build();

  _$SurveyResponseAnswerDataResponse._(
      {required this.id,
      required this.parentAnswerId,
      required this.order,
      required this.value,
      required this.formComponentId})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'SurveyResponseAnswerDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        parentAnswerId, r'SurveyResponseAnswerDataResponse', 'parentAnswerId');
    BuiltValueNullFieldError.checkNotNull(
        order, r'SurveyResponseAnswerDataResponse', 'order');
    BuiltValueNullFieldError.checkNotNull(
        value, r'SurveyResponseAnswerDataResponse', 'value');
    BuiltValueNullFieldError.checkNotNull(formComponentId,
        r'SurveyResponseAnswerDataResponse', 'formComponentId');
  }

  @override
  SurveyResponseAnswerDataResponse rebuild(
          void Function(SurveyResponseAnswerDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SurveyResponseAnswerDataResponseBuilder toBuilder() =>
      new SurveyResponseAnswerDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SurveyResponseAnswerDataResponse &&
        id == other.id &&
        parentAnswerId == other.parentAnswerId &&
        order == other.order &&
        value == other.value &&
        formComponentId == other.formComponentId;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, parentAnswerId.hashCode);
    _$hash = $jc(_$hash, order.hashCode);
    _$hash = $jc(_$hash, value.hashCode);
    _$hash = $jc(_$hash, formComponentId.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SurveyResponseAnswerDataResponse')
          ..add('id', id)
          ..add('parentAnswerId', parentAnswerId)
          ..add('order', order)
          ..add('value', value)
          ..add('formComponentId', formComponentId))
        .toString();
  }
}

class SurveyResponseAnswerDataResponseBuilder
    implements
        Builder<SurveyResponseAnswerDataResponse,
            SurveyResponseAnswerDataResponseBuilder> {
  _$SurveyResponseAnswerDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  num? _parentAnswerId;
  num? get parentAnswerId => _$this._parentAnswerId;
  set parentAnswerId(num? parentAnswerId) =>
      _$this._parentAnswerId = parentAnswerId;

  num? _order;
  num? get order => _$this._order;
  set order(num? order) => _$this._order = order;

  JsonObject? _value;
  JsonObject? get value => _$this._value;
  set value(JsonObject? value) => _$this._value = value;

  num? _formComponentId;
  num? get formComponentId => _$this._formComponentId;
  set formComponentId(num? formComponentId) =>
      _$this._formComponentId = formComponentId;

  SurveyResponseAnswerDataResponseBuilder() {
    SurveyResponseAnswerDataResponse._defaults(this);
  }

  SurveyResponseAnswerDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _parentAnswerId = $v.parentAnswerId;
      _order = $v.order;
      _value = $v.value;
      _formComponentId = $v.formComponentId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SurveyResponseAnswerDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SurveyResponseAnswerDataResponse;
  }

  @override
  void update(void Function(SurveyResponseAnswerDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SurveyResponseAnswerDataResponse build() => _build();

  _$SurveyResponseAnswerDataResponse _build() {
    final _$result = _$v ??
        new _$SurveyResponseAnswerDataResponse._(
            id: BuiltValueNullFieldError.checkNotNull(
                id, r'SurveyResponseAnswerDataResponse', 'id'),
            parentAnswerId: BuiltValueNullFieldError.checkNotNull(
                parentAnswerId,
                r'SurveyResponseAnswerDataResponse',
                'parentAnswerId'),
            order: BuiltValueNullFieldError.checkNotNull(
                order, r'SurveyResponseAnswerDataResponse', 'order'),
            value: BuiltValueNullFieldError.checkNotNull(
                value, r'SurveyResponseAnswerDataResponse', 'value'),
            formComponentId: BuiltValueNullFieldError.checkNotNull(
                formComponentId,
                r'SurveyResponseAnswerDataResponse',
                'formComponentId'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
