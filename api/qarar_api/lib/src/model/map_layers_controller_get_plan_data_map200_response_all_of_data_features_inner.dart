//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_plan_data_map200_response_all_of_data_features_inner_properties.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_plan_data_map200_response_all_of_data_features_inner.g.dart';

/// GeoJson Feature
///
/// Properties:
/// * [type]
/// * [id]
/// * [geometry]
/// * [properties]
@BuiltValue()
abstract class MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner
    implements
        Built<
            MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner,
            MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerBuilder> {
  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerTypeEnum
      get type;
  // enum typeEnum {  Feature,  };

  @BuiltValueField(wireName: r'id')
  int get id;

  @BuiltValueField(wireName: r'geometry')
  JsonObject? get geometry;

  @BuiltValueField(wireName: r'properties')
  MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties
      get properties;

  MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner._();

  factory MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner(
          [void updates(
              MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerBuilder
                  b)]) =
      _$MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerBuilder
              b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<
          MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner>
      get serializer =>
          _$MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerSerializer();
}

class _$MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner,
    _$MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(
          MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerTypeEnum),
    );
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(int),
    );
    yield r'geometry';
    yield object.geometry == null
        ? null
        : serializers.serialize(
            object.geometry,
            specifiedType: const FullType.nullable(JsonObject),
          );
    yield r'properties';
    yield serializers.serialize(
      object.properties,
      specifiedType: const FullType(
          MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerTypeEnum),
          ) as MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerTypeEnum;
          result.type = valueDes;
          break;
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(int),
          ) as int;
          result.id = valueDes;
          break;
        case r'geometry':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(JsonObject),
          ) as JsonObject?;
          if (valueDes == null) continue;
          result.geometry = valueDes;
          break;
        case r'properties':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties),
          ) as MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties;
          result.properties.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInner
      deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerTypeEnum
    extends EnumClass {
  @BuiltValueEnumConst(wireName: r'Feature', fallback: true)
  static const MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerTypeEnum
      feature =
      _$mapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature;

  static Serializer<
          MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerTypeEnum>
      get serializer =>
          _$mapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer;

  const MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerTypeEnum._(
      String name)
      : super(name);

  static BuiltSet<
          MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerTypeEnum>
      get values =>
          _$mapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerTypeEnumValues;
  static MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerTypeEnum
      valueOf(String name) =>
          _$mapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerTypeEnumValueOf(
              name);
}
