// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'form_create_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$FormCreateRequest extends FormCreateRequest {
  @override
  final String name;
  @override
  final bool isActive;
  @override
  final BuiltList<FormComponentsRequest> formComponents;
  @override
  final String? description;
  @override
  final String? fileToken;

  factory _$FormCreateRequest(
          [void Function(FormCreateRequestBuilder)? updates]) =>
      (new FormCreateRequestBuilder()..update(updates))._build();

  _$FormCreateRequest._(
      {required this.name,
      required this.isActive,
      required this.formComponents,
      this.description,
      this.fileToken})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(name, r'FormCreateRequest', 'name');
    BuiltValueNullFieldError.checkNotNull(
        isActive, r'FormCreateRequest', 'isActive');
    BuiltValueNullFieldError.checkNotNull(
        formComponents, r'FormCreateRequest', 'formComponents');
  }

  @override
  FormCreateRequest rebuild(void Function(FormCreateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  FormCreateRequestBuilder toBuilder() =>
      new FormCreateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is FormCreateRequest &&
        name == other.name &&
        isActive == other.isActive &&
        formComponents == other.formComponents &&
        description == other.description &&
        fileToken == other.fileToken;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, isActive.hashCode);
    _$hash = $jc(_$hash, formComponents.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, fileToken.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'FormCreateRequest')
          ..add('name', name)
          ..add('isActive', isActive)
          ..add('formComponents', formComponents)
          ..add('description', description)
          ..add('fileToken', fileToken))
        .toString();
  }
}

class FormCreateRequestBuilder
    implements Builder<FormCreateRequest, FormCreateRequestBuilder> {
  _$FormCreateRequest? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  bool? _isActive;
  bool? get isActive => _$this._isActive;
  set isActive(bool? isActive) => _$this._isActive = isActive;

  ListBuilder<FormComponentsRequest>? _formComponents;
  ListBuilder<FormComponentsRequest> get formComponents =>
      _$this._formComponents ??= new ListBuilder<FormComponentsRequest>();
  set formComponents(ListBuilder<FormComponentsRequest>? formComponents) =>
      _$this._formComponents = formComponents;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  String? _fileToken;
  String? get fileToken => _$this._fileToken;
  set fileToken(String? fileToken) => _$this._fileToken = fileToken;

  FormCreateRequestBuilder() {
    FormCreateRequest._defaults(this);
  }

  FormCreateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _isActive = $v.isActive;
      _formComponents = $v.formComponents.toBuilder();
      _description = $v.description;
      _fileToken = $v.fileToken;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(FormCreateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$FormCreateRequest;
  }

  @override
  void update(void Function(FormCreateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  FormCreateRequest build() => _build();

  _$FormCreateRequest _build() {
    _$FormCreateRequest _$result;
    try {
      _$result = _$v ??
          new _$FormCreateRequest._(
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'FormCreateRequest', 'name'),
              isActive: BuiltValueNullFieldError.checkNotNull(
                  isActive, r'FormCreateRequest', 'isActive'),
              formComponents: formComponents.build(),
              description: description,
              fileToken: fileToken);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'formComponents';
        formComponents.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'FormCreateRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
