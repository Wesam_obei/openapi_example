//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_electrical_facilities_map200_response_all_of_data_features_inner.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_electrical_facilities_map200_response_all_of_data.g.dart';

/// MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData
///
/// Properties:
/// * [type]
/// * [features]
@BuiltValue()
abstract class MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData
    implements
        Built<MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData,
            MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataBuilder> {
  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum
      get type;
  // enum typeEnum {  FeatureCollection,  };

  @BuiltValueField(wireName: r'features')
  BuiltList<
          MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInner>
      get features;

  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData._();

  factory MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData(
          [void updates(
              MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataBuilder
                  b)]) =
      _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataBuilder
              b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<
          MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData>
      get serializer =>
          _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataSerializer();
}

class _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData,
    _$MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(
          MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum),
    );
    yield r'features';
    yield serializers.serialize(
      object.features,
      specifiedType: const FullType(BuiltList, [
        FullType(
            MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInner)
      ]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum),
          ) as MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum;
          result.type = valueDes;
          break;
        case r'features':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [
              FullType(
                  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInner)
            ]),
          ) as BuiltList<
              MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataFeaturesInner>;
          result.features.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfData deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum
    extends EnumClass {
  @BuiltValueEnumConst(wireName: r'FeatureCollection', fallback: true)
  static const MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum
      featureCollection =
      _$mapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum_featureCollection;

  static Serializer<
          MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum>
      get serializer =>
          _$mapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnumSerializer;

  const MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum._(
      String name)
      : super(name);

  static BuiltSet<
          MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum>
      get values =>
          _$mapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnumValues;
  static MapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnum
      valueOf(String name) =>
          _$mapLayersControllerGetElectricalFacilitiesMap200ResponseAllOfDataTypeEnumValueOf(
              name);
}
