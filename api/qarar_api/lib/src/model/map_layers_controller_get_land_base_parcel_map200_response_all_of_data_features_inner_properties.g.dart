// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_land_base_parcel_map200_response_all_of_data_features_inner_properties.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties
    extends MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties {
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      id;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      parcelPlanNo;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      parcelMainLuse;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      parcelSubLuse;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      oldParcelPlanNO;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      USING_SYMBOL;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      UNITS_NUMBER;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      PLAN_NO;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      BLDG_CONDITIONS;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      municipality;

  factory _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties(
          [void Function(
                  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
              updates]) =>
      (new MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties._(
      {this.id,
      this.parcelPlanNo,
      this.parcelMainLuse,
      this.parcelSubLuse,
      this.oldParcelPlanNO,
      this.USING_SYMBOL,
      this.UNITS_NUMBER,
      this.PLAN_NO,
      this.BLDG_CONDITIONS,
      this.municipality})
      : super._();

  @override
  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties
      rebuild(
              void Function(
                      MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)
                  updates) =>
          (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      toBuilder() =>
          new MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties &&
        id == other.id &&
        parcelPlanNo == other.parcelPlanNo &&
        parcelMainLuse == other.parcelMainLuse &&
        parcelSubLuse == other.parcelSubLuse &&
        oldParcelPlanNO == other.oldParcelPlanNO &&
        USING_SYMBOL == other.USING_SYMBOL &&
        UNITS_NUMBER == other.UNITS_NUMBER &&
        PLAN_NO == other.PLAN_NO &&
        BLDG_CONDITIONS == other.BLDG_CONDITIONS &&
        municipality == other.municipality;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, parcelPlanNo.hashCode);
    _$hash = $jc(_$hash, parcelMainLuse.hashCode);
    _$hash = $jc(_$hash, parcelSubLuse.hashCode);
    _$hash = $jc(_$hash, oldParcelPlanNO.hashCode);
    _$hash = $jc(_$hash, USING_SYMBOL.hashCode);
    _$hash = $jc(_$hash, UNITS_NUMBER.hashCode);
    _$hash = $jc(_$hash, PLAN_NO.hashCode);
    _$hash = $jc(_$hash, BLDG_CONDITIONS.hashCode);
    _$hash = $jc(_$hash, municipality.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties')
          ..add('id', id)
          ..add('parcelPlanNo', parcelPlanNo)
          ..add('parcelMainLuse', parcelMainLuse)
          ..add('parcelSubLuse', parcelSubLuse)
          ..add('oldParcelPlanNO', oldParcelPlanNO)
          ..add('USING_SYMBOL', USING_SYMBOL)
          ..add('UNITS_NUMBER', UNITS_NUMBER)
          ..add('PLAN_NO', PLAN_NO)
          ..add('BLDG_CONDITIONS', BLDG_CONDITIONS)
          ..add('municipality', municipality))
        .toString();
  }
}

class MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
    implements
        Builder<
            MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties,
            MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder> {
  _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties?
      _$v;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _id;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get id => _$this._id ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set id(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              id) =>
      _$this._id = id;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _parcelPlanNo;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get parcelPlanNo => _$this._parcelPlanNo ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set parcelPlanNo(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              parcelPlanNo) =>
      _$this._parcelPlanNo = parcelPlanNo;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _parcelMainLuse;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get parcelMainLuse => _$this._parcelMainLuse ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set parcelMainLuse(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              parcelMainLuse) =>
      _$this._parcelMainLuse = parcelMainLuse;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _parcelSubLuse;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get parcelSubLuse => _$this._parcelSubLuse ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set parcelSubLuse(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              parcelSubLuse) =>
      _$this._parcelSubLuse = parcelSubLuse;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _oldParcelPlanNO;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get oldParcelPlanNO => _$this._oldParcelPlanNO ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set oldParcelPlanNO(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              oldParcelPlanNO) =>
      _$this._oldParcelPlanNO = oldParcelPlanNO;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _USING_SYMBOL;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get USING_SYMBOL => _$this._USING_SYMBOL ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set USING_SYMBOL(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              USING_SYMBOL) =>
      _$this._USING_SYMBOL = USING_SYMBOL;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _UNITS_NUMBER;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get UNITS_NUMBER => _$this._UNITS_NUMBER ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set UNITS_NUMBER(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              UNITS_NUMBER) =>
      _$this._UNITS_NUMBER = UNITS_NUMBER;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _PLAN_NO;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get PLAN_NO => _$this._PLAN_NO ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set PLAN_NO(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              PLAN_NO) =>
      _$this._PLAN_NO = PLAN_NO;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _BLDG_CONDITIONS;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get BLDG_CONDITIONS => _$this._BLDG_CONDITIONS ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set BLDG_CONDITIONS(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              BLDG_CONDITIONS) =>
      _$this._BLDG_CONDITIONS = BLDG_CONDITIONS;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _municipality;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get municipality => _$this._municipality ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set municipality(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              municipality) =>
      _$this._municipality = municipality;

  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder() {
    MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties
        ._defaults(this);
  }

  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id?.toBuilder();
      _parcelPlanNo = $v.parcelPlanNo?.toBuilder();
      _parcelMainLuse = $v.parcelMainLuse?.toBuilder();
      _parcelSubLuse = $v.parcelSubLuse?.toBuilder();
      _oldParcelPlanNO = $v.oldParcelPlanNO?.toBuilder();
      _USING_SYMBOL = $v.USING_SYMBOL?.toBuilder();
      _UNITS_NUMBER = $v.UNITS_NUMBER?.toBuilder();
      _PLAN_NO = $v.PLAN_NO?.toBuilder();
      _BLDG_CONDITIONS = $v.BLDG_CONDITIONS?.toBuilder();
      _municipality = $v.municipality?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties
      build() => _build();

  _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties
      _build() {
    _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties
        _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties
              ._(
              id: _id?.build(),
              parcelPlanNo: _parcelPlanNo?.build(),
              parcelMainLuse: _parcelMainLuse?.build(),
              parcelSubLuse: _parcelSubLuse?.build(),
              oldParcelPlanNO: _oldParcelPlanNO?.build(),
              USING_SYMBOL: _USING_SYMBOL?.build(),
              UNITS_NUMBER: _UNITS_NUMBER?.build(),
              PLAN_NO: _PLAN_NO?.build(),
              BLDG_CONDITIONS: _BLDG_CONDITIONS?.build(),
              municipality: _municipality?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'id';
        _id?.build();
        _$failedField = 'parcelPlanNo';
        _parcelPlanNo?.build();
        _$failedField = 'parcelMainLuse';
        _parcelMainLuse?.build();
        _$failedField = 'parcelSubLuse';
        _parcelSubLuse?.build();
        _$failedField = 'oldParcelPlanNO';
        _oldParcelPlanNO?.build();
        _$failedField = 'USING_SYMBOL';
        _USING_SYMBOL?.build();
        _$failedField = 'UNITS_NUMBER';
        _UNITS_NUMBER?.build();
        _$failedField = 'PLAN_NO';
        _PLAN_NO?.build();
        _$failedField = 'BLDG_CONDITIONS';
        _BLDG_CONDITIONS?.build();
        _$failedField = 'municipality';
        _municipality?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
