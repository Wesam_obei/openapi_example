// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recipient_type.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const RecipientType _$users = const RecipientType._('users');
const RecipientType _$entities = const RecipientType._('entities');

RecipientType _$valueOf(String name) {
  switch (name) {
    case 'users':
      return _$users;
    case 'entities':
      return _$entities;
    default:
      return _$entities;
  }
}

final BuiltSet<RecipientType> _$values =
    new BuiltSet<RecipientType>(const <RecipientType>[
  _$users,
  _$entities,
]);

class _$RecipientTypeMeta {
  const _$RecipientTypeMeta();
  RecipientType get users => _$users;
  RecipientType get entities => _$entities;
  RecipientType valueOf(String name) => _$valueOf(name);
  BuiltSet<RecipientType> get values => _$values;
}

abstract class _$RecipientTypeMixin {
  // ignore: non_constant_identifier_names
  _$RecipientTypeMeta get RecipientType => const _$RecipientTypeMeta();
}

Serializer<RecipientType> _$recipientTypeSerializer =
    new _$RecipientTypeSerializer();

class _$RecipientTypeSerializer implements PrimitiveSerializer<RecipientType> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'users': 'users',
    'entities': 'entities',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'users': 'users',
    'entities': 'entities',
  };

  @override
  final Iterable<Type> types = const <Type>[RecipientType];
  @override
  final String wireName = 'RecipientType';

  @override
  Object serialize(Serializers serializers, RecipientType object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  RecipientType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      RecipientType.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
