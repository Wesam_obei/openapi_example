//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'envelope.g.dart';

/// Envelope
///
/// Properties:
/// * [xmin]
/// * [ymin]
/// * [xmax]
/// * [ymax]
@BuiltValue()
abstract class Envelope implements Built<Envelope, EnvelopeBuilder> {
  @BuiltValueField(wireName: r'xmin')
  num get xmin;

  @BuiltValueField(wireName: r'ymin')
  num get ymin;

  @BuiltValueField(wireName: r'xmax')
  num get xmax;

  @BuiltValueField(wireName: r'ymax')
  num get ymax;

  Envelope._();

  factory Envelope([void updates(EnvelopeBuilder b)]) = _$Envelope;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(EnvelopeBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<Envelope> get serializer => _$EnvelopeSerializer();
}

class _$EnvelopeSerializer implements PrimitiveSerializer<Envelope> {
  @override
  final Iterable<Type> types = const [Envelope, _$Envelope];

  @override
  final String wireName = r'Envelope';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    Envelope object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'xmin';
    yield serializers.serialize(
      object.xmin,
      specifiedType: const FullType(num),
    );
    yield r'ymin';
    yield serializers.serialize(
      object.ymin,
      specifiedType: const FullType(num),
    );
    yield r'xmax';
    yield serializers.serialize(
      object.xmax,
      specifiedType: const FullType(num),
    );
    yield r'ymax';
    yield serializers.serialize(
      object.ymax,
      specifiedType: const FullType(num),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    Envelope object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required EnvelopeBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'xmin':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.xmin = valueDes;
          break;
        case r'ymin':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.ymin = valueDes;
          break;
        case r'xmax':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.xmax = valueDes;
          break;
        case r'ymax':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.ymax = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  Envelope deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = EnvelopeBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
