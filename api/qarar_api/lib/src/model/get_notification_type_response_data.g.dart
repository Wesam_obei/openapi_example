// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_notification_type_response_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetNotificationTypeResponseData
    extends GetNotificationTypeResponseData {
  @override
  final num id;
  @override
  final String name;

  factory _$GetNotificationTypeResponseData(
          [void Function(GetNotificationTypeResponseDataBuilder)? updates]) =>
      (new GetNotificationTypeResponseDataBuilder()..update(updates))._build();

  _$GetNotificationTypeResponseData._({required this.id, required this.name})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'GetNotificationTypeResponseData', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, r'GetNotificationTypeResponseData', 'name');
  }

  @override
  GetNotificationTypeResponseData rebuild(
          void Function(GetNotificationTypeResponseDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetNotificationTypeResponseDataBuilder toBuilder() =>
      new GetNotificationTypeResponseDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetNotificationTypeResponseData &&
        id == other.id &&
        name == other.name;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetNotificationTypeResponseData')
          ..add('id', id)
          ..add('name', name))
        .toString();
  }
}

class GetNotificationTypeResponseDataBuilder
    implements
        Builder<GetNotificationTypeResponseData,
            GetNotificationTypeResponseDataBuilder> {
  _$GetNotificationTypeResponseData? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  GetNotificationTypeResponseDataBuilder() {
    GetNotificationTypeResponseData._defaults(this);
  }

  GetNotificationTypeResponseDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetNotificationTypeResponseData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetNotificationTypeResponseData;
  }

  @override
  void update(void Function(GetNotificationTypeResponseDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetNotificationTypeResponseData build() => _build();

  _$GetNotificationTypeResponseData _build() {
    final _$result = _$v ??
        new _$GetNotificationTypeResponseData._(
            id: BuiltValueNullFieldError.checkNotNull(
                id, r'GetNotificationTypeResponseData', 'id'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, r'GetNotificationTypeResponseData', 'name'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
