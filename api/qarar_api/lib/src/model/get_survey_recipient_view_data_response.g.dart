// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_survey_recipient_view_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetSurveyRecipientViewDataResponse
    extends GetSurveyRecipientViewDataResponse {
  @override
  final num id;
  @override
  final num originalId;
  @override
  final String name;
  @override
  final GetSurveyResponseDataResponse response;
  @override
  final String type;

  factory _$GetSurveyRecipientViewDataResponse(
          [void Function(GetSurveyRecipientViewDataResponseBuilder)?
              updates]) =>
      (new GetSurveyRecipientViewDataResponseBuilder()..update(updates))
          ._build();

  _$GetSurveyRecipientViewDataResponse._(
      {required this.id,
      required this.originalId,
      required this.name,
      required this.response,
      required this.type})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'GetSurveyRecipientViewDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        originalId, r'GetSurveyRecipientViewDataResponse', 'originalId');
    BuiltValueNullFieldError.checkNotNull(
        name, r'GetSurveyRecipientViewDataResponse', 'name');
    BuiltValueNullFieldError.checkNotNull(
        response, r'GetSurveyRecipientViewDataResponse', 'response');
    BuiltValueNullFieldError.checkNotNull(
        type, r'GetSurveyRecipientViewDataResponse', 'type');
  }

  @override
  GetSurveyRecipientViewDataResponse rebuild(
          void Function(GetSurveyRecipientViewDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetSurveyRecipientViewDataResponseBuilder toBuilder() =>
      new GetSurveyRecipientViewDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetSurveyRecipientViewDataResponse &&
        id == other.id &&
        originalId == other.originalId &&
        name == other.name &&
        response == other.response &&
        type == other.type;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, originalId.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, response.hashCode);
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetSurveyRecipientViewDataResponse')
          ..add('id', id)
          ..add('originalId', originalId)
          ..add('name', name)
          ..add('response', response)
          ..add('type', type))
        .toString();
  }
}

class GetSurveyRecipientViewDataResponseBuilder
    implements
        Builder<GetSurveyRecipientViewDataResponse,
            GetSurveyRecipientViewDataResponseBuilder> {
  _$GetSurveyRecipientViewDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  num? _originalId;
  num? get originalId => _$this._originalId;
  set originalId(num? originalId) => _$this._originalId = originalId;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  GetSurveyResponseDataResponseBuilder? _response;
  GetSurveyResponseDataResponseBuilder get response =>
      _$this._response ??= new GetSurveyResponseDataResponseBuilder();
  set response(GetSurveyResponseDataResponseBuilder? response) =>
      _$this._response = response;

  String? _type;
  String? get type => _$this._type;
  set type(String? type) => _$this._type = type;

  GetSurveyRecipientViewDataResponseBuilder() {
    GetSurveyRecipientViewDataResponse._defaults(this);
  }

  GetSurveyRecipientViewDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _originalId = $v.originalId;
      _name = $v.name;
      _response = $v.response.toBuilder();
      _type = $v.type;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetSurveyRecipientViewDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetSurveyRecipientViewDataResponse;
  }

  @override
  void update(
      void Function(GetSurveyRecipientViewDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetSurveyRecipientViewDataResponse build() => _build();

  _$GetSurveyRecipientViewDataResponse _build() {
    _$GetSurveyRecipientViewDataResponse _$result;
    try {
      _$result = _$v ??
          new _$GetSurveyRecipientViewDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'GetSurveyRecipientViewDataResponse', 'id'),
              originalId: BuiltValueNullFieldError.checkNotNull(originalId,
                  r'GetSurveyRecipientViewDataResponse', 'originalId'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'GetSurveyRecipientViewDataResponse', 'name'),
              response: response.build(),
              type: BuiltValueNullFieldError.checkNotNull(
                  type, r'GetSurveyRecipientViewDataResponse', 'type'));
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'response';
        response.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'GetSurveyRecipientViewDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
