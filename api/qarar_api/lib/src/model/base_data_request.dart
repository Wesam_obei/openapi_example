//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/envelope.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'base_data_request.g.dart';

/// BaseDataRequest
///
/// Properties:
/// * [envelope]
/// * [fields]
@BuiltValue()
abstract class BaseDataRequest
    implements Built<BaseDataRequest, BaseDataRequestBuilder> {
  @BuiltValueField(wireName: r'envelope')
  Envelope get envelope;

  @BuiltValueField(wireName: r'fields')
  BuiltList<String> get fields;

  BaseDataRequest._();

  factory BaseDataRequest([void updates(BaseDataRequestBuilder b)]) =
      _$BaseDataRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(BaseDataRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<BaseDataRequest> get serializer =>
      _$BaseDataRequestSerializer();
}

class _$BaseDataRequestSerializer
    implements PrimitiveSerializer<BaseDataRequest> {
  @override
  final Iterable<Type> types = const [BaseDataRequest, _$BaseDataRequest];

  @override
  final String wireName = r'BaseDataRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    BaseDataRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'envelope';
    yield serializers.serialize(
      object.envelope,
      specifiedType: const FullType(Envelope),
    );
    yield r'fields';
    yield serializers.serialize(
      object.fields,
      specifiedType: const FullType(BuiltList, [FullType(String)]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    BaseDataRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required BaseDataRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'envelope':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(Envelope),
          ) as Envelope;
          result.envelope.replace(valueDes);
          break;
        case r'fields':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(String)]),
          ) as BuiltList<String>;
          result.fields.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  BaseDataRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = BaseDataRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
