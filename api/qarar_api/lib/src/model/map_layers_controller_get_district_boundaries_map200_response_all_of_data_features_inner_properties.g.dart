// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_district_boundaries_map200_response_all_of_data_features_inner_properties.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
    extends MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties {
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      id;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      municipalityName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      municipalitySubName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      districtName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      districtEngName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      remarks;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      areaName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      cityName;

  factory _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties(
          [void Function(
                  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
              updates]) =>
      (new MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties._(
      {this.id,
      this.municipalityName,
      this.municipalitySubName,
      this.districtName,
      this.districtEngName,
      this.remarks,
      this.areaName,
      this.cityName})
      : super._();

  @override
  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
      rebuild(
              void Function(
                      MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)
                  updates) =>
          (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      toBuilder() =>
          new MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties &&
        id == other.id &&
        municipalityName == other.municipalityName &&
        municipalitySubName == other.municipalitySubName &&
        districtName == other.districtName &&
        districtEngName == other.districtEngName &&
        remarks == other.remarks &&
        areaName == other.areaName &&
        cityName == other.cityName;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, municipalityName.hashCode);
    _$hash = $jc(_$hash, municipalitySubName.hashCode);
    _$hash = $jc(_$hash, districtName.hashCode);
    _$hash = $jc(_$hash, districtEngName.hashCode);
    _$hash = $jc(_$hash, remarks.hashCode);
    _$hash = $jc(_$hash, areaName.hashCode);
    _$hash = $jc(_$hash, cityName.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties')
          ..add('id', id)
          ..add('municipalityName', municipalityName)
          ..add('municipalitySubName', municipalitySubName)
          ..add('districtName', districtName)
          ..add('districtEngName', districtEngName)
          ..add('remarks', remarks)
          ..add('areaName', areaName)
          ..add('cityName', cityName))
        .toString();
  }
}

class MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
    implements
        Builder<
            MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties,
            MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder> {
  _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties?
      _$v;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _id;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get id => _$this._id ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set id(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              id) =>
      _$this._id = id;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _municipalityName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get municipalityName => _$this._municipalityName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set municipalityName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              municipalityName) =>
      _$this._municipalityName = municipalityName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _municipalitySubName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get municipalitySubName => _$this._municipalitySubName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set municipalitySubName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              municipalitySubName) =>
      _$this._municipalitySubName = municipalitySubName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _districtName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get districtName => _$this._districtName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set districtName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              districtName) =>
      _$this._districtName = districtName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _districtEngName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get districtEngName => _$this._districtEngName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set districtEngName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              districtEngName) =>
      _$this._districtEngName = districtEngName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _remarks;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get remarks => _$this._remarks ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set remarks(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              remarks) =>
      _$this._remarks = remarks;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _areaName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get areaName => _$this._areaName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set areaName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              areaName) =>
      _$this._areaName = areaName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _cityName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get cityName => _$this._cityName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set cityName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              cityName) =>
      _$this._cityName = cityName;

  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder() {
    MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
        ._defaults(this);
  }

  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id?.toBuilder();
      _municipalityName = $v.municipalityName?.toBuilder();
      _municipalitySubName = $v.municipalitySubName?.toBuilder();
      _districtName = $v.districtName?.toBuilder();
      _districtEngName = $v.districtEngName?.toBuilder();
      _remarks = $v.remarks?.toBuilder();
      _areaName = $v.areaName?.toBuilder();
      _cityName = $v.cityName?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
      build() => _build();

  _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
      _build() {
    _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
        _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
              ._(
              id: _id?.build(),
              municipalityName: _municipalityName?.build(),
              municipalitySubName: _municipalitySubName?.build(),
              districtName: _districtName?.build(),
              districtEngName: _districtEngName?.build(),
              remarks: _remarks?.build(),
              areaName: _areaName?.build(),
              cityName: _cityName?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'id';
        _id?.build();
        _$failedField = 'municipalityName';
        _municipalityName?.build();
        _$failedField = 'municipalitySubName';
        _municipalitySubName?.build();
        _$failedField = 'districtName';
        _districtName?.build();
        _$failedField = 'districtEngName';
        _districtEngName?.build();
        _$failedField = 'remarks';
        _remarks?.build();
        _$failedField = 'areaName';
        _areaName?.build();
        _$failedField = 'cityName';
        _cityName?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
