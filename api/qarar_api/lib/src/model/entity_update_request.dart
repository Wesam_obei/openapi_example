//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'entity_update_request.g.dart';

/// EntityUpdateRequest
///
/// Properties:
/// * [name]
/// * [users]
/// * [fileToken]
/// * [deleteFile]
@BuiltValue()
abstract class EntityUpdateRequest
    implements Built<EntityUpdateRequest, EntityUpdateRequestBuilder> {
  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'users')
  BuiltList<num> get users;

  @BuiltValueField(wireName: r'file_token')
  String? get fileToken;

  @BuiltValueField(wireName: r'delete_file')
  bool? get deleteFile;

  EntityUpdateRequest._();

  factory EntityUpdateRequest([void updates(EntityUpdateRequestBuilder b)]) =
      _$EntityUpdateRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(EntityUpdateRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<EntityUpdateRequest> get serializer =>
      _$EntityUpdateRequestSerializer();
}

class _$EntityUpdateRequestSerializer
    implements PrimitiveSerializer<EntityUpdateRequest> {
  @override
  final Iterable<Type> types = const [
    EntityUpdateRequest,
    _$EntityUpdateRequest
  ];

  @override
  final String wireName = r'EntityUpdateRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    EntityUpdateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    yield r'users';
    yield serializers.serialize(
      object.users,
      specifiedType: const FullType(BuiltList, [FullType(num)]),
    );
    if (object.fileToken != null) {
      yield r'file_token';
      yield serializers.serialize(
        object.fileToken,
        specifiedType: const FullType(String),
      );
    }
    if (object.deleteFile != null) {
      yield r'delete_file';
      yield serializers.serialize(
        object.deleteFile,
        specifiedType: const FullType(bool),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    EntityUpdateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required EntityUpdateRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'users':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(num)]),
          ) as BuiltList<num>;
          result.users.replace(valueDes);
          break;
        case r'file_token':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.fileToken = valueDes;
          break;
        case r'delete_file':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.deleteFile = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  EntityUpdateRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = EntityUpdateRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
