// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_cables_map200_response_all_of_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum_featureCollection =
    const MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum._(
        'featureCollection');

MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'featureCollection':
      return _$mapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum_featureCollection;
    default:
      return _$mapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum_featureCollection;
  }
}

final BuiltSet<MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum>(const <MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum>[
  _$mapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum_featureCollection,
]);

Serializer<MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnumSerializer =
    new _$MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnumSerializer();

class _$MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'featureCollection': 'FeatureCollection',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'FeatureCollection': 'featureCollection',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum';

  @override
  Object serialize(Serializers serializers,
          MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum deserialize(
          Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetCablesMap200ResponseAllOfData
    extends MapLayersControllerGetCablesMap200ResponseAllOfData {
  @override
  final MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum type;
  @override
  final BuiltList<
          MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner>
      features;

  factory _$MapLayersControllerGetCablesMap200ResponseAllOfData(
          [void Function(
                  MapLayersControllerGetCablesMap200ResponseAllOfDataBuilder)?
              updates]) =>
      (new MapLayersControllerGetCablesMap200ResponseAllOfDataBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetCablesMap200ResponseAllOfData._(
      {required this.type, required this.features})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        type, r'MapLayersControllerGetCablesMap200ResponseAllOfData', 'type');
    BuiltValueNullFieldError.checkNotNull(features,
        r'MapLayersControllerGetCablesMap200ResponseAllOfData', 'features');
  }

  @override
  MapLayersControllerGetCablesMap200ResponseAllOfData rebuild(
          void Function(
                  MapLayersControllerGetCablesMap200ResponseAllOfDataBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetCablesMap200ResponseAllOfDataBuilder toBuilder() =>
      new MapLayersControllerGetCablesMap200ResponseAllOfDataBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapLayersControllerGetCablesMap200ResponseAllOfData &&
        type == other.type &&
        features == other.features;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, features.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetCablesMap200ResponseAllOfData')
          ..add('type', type)
          ..add('features', features))
        .toString();
  }
}

class MapLayersControllerGetCablesMap200ResponseAllOfDataBuilder
    implements
        Builder<MapLayersControllerGetCablesMap200ResponseAllOfData,
            MapLayersControllerGetCablesMap200ResponseAllOfDataBuilder> {
  _$MapLayersControllerGetCablesMap200ResponseAllOfData? _$v;

  MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum? _type;
  MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum? get type =>
      _$this._type;
  set type(MapLayersControllerGetCablesMap200ResponseAllOfDataTypeEnum? type) =>
      _$this._type = type;

  ListBuilder<MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner>?
      _features;
  ListBuilder<MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner>
      get features => _$this._features ??= new ListBuilder<
          MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner>();
  set features(
          ListBuilder<
                  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner>?
              features) =>
      _$this._features = features;

  MapLayersControllerGetCablesMap200ResponseAllOfDataBuilder() {
    MapLayersControllerGetCablesMap200ResponseAllOfData._defaults(this);
  }

  MapLayersControllerGetCablesMap200ResponseAllOfDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _features = $v.features.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MapLayersControllerGetCablesMap200ResponseAllOfData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapLayersControllerGetCablesMap200ResponseAllOfData;
  }

  @override
  void update(
      void Function(MapLayersControllerGetCablesMap200ResponseAllOfDataBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetCablesMap200ResponseAllOfData build() => _build();

  _$MapLayersControllerGetCablesMap200ResponseAllOfData _build() {
    _$MapLayersControllerGetCablesMap200ResponseAllOfData _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetCablesMap200ResponseAllOfData._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetCablesMap200ResponseAllOfData',
                  'type'),
              features: features.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'features';
        features.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetCablesMap200ResponseAllOfData',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
