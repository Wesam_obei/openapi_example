//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/map_layers_controller_get_street_names_map200_response_all_of_data_features_inner.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_street_names_map200_response_all_of_data.g.dart';

/// MapLayersControllerGetStreetNamesMap200ResponseAllOfData
///
/// Properties:
/// * [type]
/// * [features]
@BuiltValue()
abstract class MapLayersControllerGetStreetNamesMap200ResponseAllOfData
    implements
        Built<MapLayersControllerGetStreetNamesMap200ResponseAllOfData,
            MapLayersControllerGetStreetNamesMap200ResponseAllOfDataBuilder> {
  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum get type;
  // enum typeEnum {  FeatureCollection,  };

  @BuiltValueField(wireName: r'features')
  BuiltList<
          MapLayersControllerGetStreetNamesMap200ResponseAllOfDataFeaturesInner>
      get features;

  MapLayersControllerGetStreetNamesMap200ResponseAllOfData._();

  factory MapLayersControllerGetStreetNamesMap200ResponseAllOfData(
      [void updates(
          MapLayersControllerGetStreetNamesMap200ResponseAllOfDataBuilder
              b)]) = _$MapLayersControllerGetStreetNamesMap200ResponseAllOfData;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetStreetNamesMap200ResponseAllOfDataBuilder b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<MapLayersControllerGetStreetNamesMap200ResponseAllOfData>
      get serializer =>
          _$MapLayersControllerGetStreetNamesMap200ResponseAllOfDataSerializer();
}

class _$MapLayersControllerGetStreetNamesMap200ResponseAllOfDataSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetStreetNamesMap200ResponseAllOfData> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetStreetNamesMap200ResponseAllOfData,
    _$MapLayersControllerGetStreetNamesMap200ResponseAllOfData
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetStreetNamesMap200ResponseAllOfData';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetStreetNamesMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(
          MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum),
    );
    yield r'features';
    yield serializers.serialize(
      object.features,
      specifiedType: const FullType(BuiltList, [
        FullType(
            MapLayersControllerGetStreetNamesMap200ResponseAllOfDataFeaturesInner)
      ]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetStreetNamesMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetStreetNamesMap200ResponseAllOfDataBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum),
          ) as MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum;
          result.type = valueDes;
          break;
        case r'features':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [
              FullType(
                  MapLayersControllerGetStreetNamesMap200ResponseAllOfDataFeaturesInner)
            ]),
          ) as BuiltList<
              MapLayersControllerGetStreetNamesMap200ResponseAllOfDataFeaturesInner>;
          result.features.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetStreetNamesMap200ResponseAllOfData deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetStreetNamesMap200ResponseAllOfDataBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum
    extends EnumClass {
  @BuiltValueEnumConst(wireName: r'FeatureCollection', fallback: true)
  static const MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum
      featureCollection =
      _$mapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum_featureCollection;

  static Serializer<
          MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum>
      get serializer =>
          _$mapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnumSerializer;

  const MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum._(
      String name)
      : super(name);

  static BuiltSet<
          MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum>
      get values =>
          _$mapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnumValues;
  static MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum valueOf(
          String name) =>
      _$mapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnumValueOf(
          name);
}
