//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/success_response.dart';
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/list_permission_category_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'permissions_categories_controller_list200_response.g.dart';

/// My custom description
///
/// Properties:
/// * [success]
/// * [data]
@BuiltValue()
abstract class PermissionsCategoriesControllerList200Response
    implements
        SuccessResponse,
        Built<PermissionsCategoriesControllerList200Response,
            PermissionsCategoriesControllerList200ResponseBuilder> {
  PermissionsCategoriesControllerList200Response._();

  factory PermissionsCategoriesControllerList200Response(
          [void updates(
              PermissionsCategoriesControllerList200ResponseBuilder b)]) =
      _$PermissionsCategoriesControllerList200Response;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          PermissionsCategoriesControllerList200ResponseBuilder b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<PermissionsCategoriesControllerList200Response>
      get serializer =>
          _$PermissionsCategoriesControllerList200ResponseSerializer();
}

class _$PermissionsCategoriesControllerList200ResponseSerializer
    implements
        PrimitiveSerializer<PermissionsCategoriesControllerList200Response> {
  @override
  final Iterable<Type> types = const [
    PermissionsCategoriesControllerList200Response,
    _$PermissionsCategoriesControllerList200Response
  ];

  @override
  final String wireName = r'PermissionsCategoriesControllerList200Response';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    PermissionsCategoriesControllerList200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'data';
    yield serializers.serialize(
      object.data,
      specifiedType: const FullType(JsonObject),
    );
    yield r'success';
    yield serializers.serialize(
      object.success,
      specifiedType: const FullType(bool),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    PermissionsCategoriesControllerList200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required PermissionsCategoriesControllerList200ResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'data':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.data = valueDes;
          break;
        case r'success':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.success = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  PermissionsCategoriesControllerList200Response deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = PermissionsCategoriesControllerList200ResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
