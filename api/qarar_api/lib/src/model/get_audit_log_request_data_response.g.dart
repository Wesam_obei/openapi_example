// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_audit_log_request_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetAuditLogRequestDataResponse extends GetAuditLogRequestDataResponse {
  @override
  final GetAuditLogHeaderDataResponse headers;

  factory _$GetAuditLogRequestDataResponse(
          [void Function(GetAuditLogRequestDataResponseBuilder)? updates]) =>
      (new GetAuditLogRequestDataResponseBuilder()..update(updates))._build();

  _$GetAuditLogRequestDataResponse._({required this.headers}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        headers, r'GetAuditLogRequestDataResponse', 'headers');
  }

  @override
  GetAuditLogRequestDataResponse rebuild(
          void Function(GetAuditLogRequestDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetAuditLogRequestDataResponseBuilder toBuilder() =>
      new GetAuditLogRequestDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetAuditLogRequestDataResponse && headers == other.headers;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, headers.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetAuditLogRequestDataResponse')
          ..add('headers', headers))
        .toString();
  }
}

class GetAuditLogRequestDataResponseBuilder
    implements
        Builder<GetAuditLogRequestDataResponse,
            GetAuditLogRequestDataResponseBuilder> {
  _$GetAuditLogRequestDataResponse? _$v;

  GetAuditLogHeaderDataResponseBuilder? _headers;
  GetAuditLogHeaderDataResponseBuilder get headers =>
      _$this._headers ??= new GetAuditLogHeaderDataResponseBuilder();
  set headers(GetAuditLogHeaderDataResponseBuilder? headers) =>
      _$this._headers = headers;

  GetAuditLogRequestDataResponseBuilder() {
    GetAuditLogRequestDataResponse._defaults(this);
  }

  GetAuditLogRequestDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _headers = $v.headers.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetAuditLogRequestDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetAuditLogRequestDataResponse;
  }

  @override
  void update(void Function(GetAuditLogRequestDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetAuditLogRequestDataResponse build() => _build();

  _$GetAuditLogRequestDataResponse _build() {
    _$GetAuditLogRequestDataResponse _$result;
    try {
      _$result = _$v ??
          new _$GetAuditLogRequestDataResponse._(headers: headers.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'headers';
        headers.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'GetAuditLogRequestDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
