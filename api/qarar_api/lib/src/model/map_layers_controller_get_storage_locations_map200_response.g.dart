// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_storage_locations_map200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetStorageLocationsMap200Response
    extends MapLayersControllerGetStorageLocationsMap200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$MapLayersControllerGetStorageLocationsMap200Response(
          [void Function(
                  MapLayersControllerGetStorageLocationsMap200ResponseBuilder)?
              updates]) =>
      (new MapLayersControllerGetStorageLocationsMap200ResponseBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetStorageLocationsMap200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(success,
        r'MapLayersControllerGetStorageLocationsMap200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'MapLayersControllerGetStorageLocationsMap200Response', 'data');
  }

  @override
  MapLayersControllerGetStorageLocationsMap200Response rebuild(
          void Function(
                  MapLayersControllerGetStorageLocationsMap200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetStorageLocationsMap200ResponseBuilder toBuilder() =>
      new MapLayersControllerGetStorageLocationsMap200ResponseBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapLayersControllerGetStorageLocationsMap200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetStorageLocationsMap200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class MapLayersControllerGetStorageLocationsMap200ResponseBuilder
    implements
        Builder<MapLayersControllerGetStorageLocationsMap200Response,
            MapLayersControllerGetStorageLocationsMap200ResponseBuilder>,
        SuccessResponseBuilder {
  _$MapLayersControllerGetStorageLocationsMap200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  MapLayersControllerGetStorageLocationsMap200ResponseBuilder() {
    MapLayersControllerGetStorageLocationsMap200Response._defaults(this);
  }

  MapLayersControllerGetStorageLocationsMap200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      covariant MapLayersControllerGetStorageLocationsMap200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapLayersControllerGetStorageLocationsMap200Response;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetStorageLocationsMap200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetStorageLocationsMap200Response build() => _build();

  _$MapLayersControllerGetStorageLocationsMap200Response _build() {
    final _$result = _$v ??
        new _$MapLayersControllerGetStorageLocationsMap200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success,
                r'MapLayersControllerGetStorageLocationsMap200Response',
                'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data,
                r'MapLayersControllerGetStorageLocationsMap200Response',
                'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
