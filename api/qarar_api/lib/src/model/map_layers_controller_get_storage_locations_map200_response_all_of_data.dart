//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_storage_locations_map200_response_all_of_data_features_inner.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_storage_locations_map200_response_all_of_data.g.dart';

/// MapLayersControllerGetStorageLocationsMap200ResponseAllOfData
///
/// Properties:
/// * [type]
/// * [features]
@BuiltValue()
abstract class MapLayersControllerGetStorageLocationsMap200ResponseAllOfData
    implements
        Built<MapLayersControllerGetStorageLocationsMap200ResponseAllOfData,
            MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataBuilder> {
  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum
      get type;
  // enum typeEnum {  FeatureCollection,  };

  @BuiltValueField(wireName: r'features')
  BuiltList<
          MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner>
      get features;

  MapLayersControllerGetStorageLocationsMap200ResponseAllOfData._();

  factory MapLayersControllerGetStorageLocationsMap200ResponseAllOfData(
          [void updates(
              MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataBuilder
                  b)]) =
      _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfData;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataBuilder
              b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<
          MapLayersControllerGetStorageLocationsMap200ResponseAllOfData>
      get serializer =>
          _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataSerializer();
}

class _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetStorageLocationsMap200ResponseAllOfData> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetStorageLocationsMap200ResponseAllOfData,
    _$MapLayersControllerGetStorageLocationsMap200ResponseAllOfData
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetStorageLocationsMap200ResponseAllOfData';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetStorageLocationsMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(
          MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum),
    );
    yield r'features';
    yield serializers.serialize(
      object.features,
      specifiedType: const FullType(BuiltList, [
        FullType(
            MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner)
      ]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetStorageLocationsMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum),
          ) as MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum;
          result.type = valueDes;
          break;
        case r'features':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [
              FullType(
                  MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner)
            ]),
          ) as BuiltList<
              MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner>;
          result.features.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetStorageLocationsMap200ResponseAllOfData deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum
    extends EnumClass {
  @BuiltValueEnumConst(wireName: r'FeatureCollection', fallback: true)
  static const MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum
      featureCollection =
      _$mapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum_featureCollection;

  static Serializer<
          MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum>
      get serializer =>
          _$mapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnumSerializer;

  const MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum._(
      String name)
      : super(name);

  static BuiltSet<
          MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum>
      get values =>
          _$mapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnumValues;
  static MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnum
      valueOf(String name) =>
          _$mapLayersControllerGetStorageLocationsMap200ResponseAllOfDataTypeEnumValueOf(
              name);
}
