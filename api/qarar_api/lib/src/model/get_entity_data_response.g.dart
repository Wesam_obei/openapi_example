// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_entity_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetEntityDataResponse extends GetEntityDataResponse {
  @override
  final num id;
  @override
  final String name;
  @override
  final DateTime createdAt;
  @override
  final BuiltList<String> users;
  @override
  final GetFileDataResponse? file;

  factory _$GetEntityDataResponse(
          [void Function(GetEntityDataResponseBuilder)? updates]) =>
      (new GetEntityDataResponseBuilder()..update(updates))._build();

  _$GetEntityDataResponse._(
      {required this.id,
      required this.name,
      required this.createdAt,
      required this.users,
      this.file})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, r'GetEntityDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, r'GetEntityDataResponse', 'name');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'GetEntityDataResponse', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        users, r'GetEntityDataResponse', 'users');
  }

  @override
  GetEntityDataResponse rebuild(
          void Function(GetEntityDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetEntityDataResponseBuilder toBuilder() =>
      new GetEntityDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetEntityDataResponse &&
        id == other.id &&
        name == other.name &&
        createdAt == other.createdAt &&
        users == other.users &&
        file == other.file;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, users.hashCode);
    _$hash = $jc(_$hash, file.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetEntityDataResponse')
          ..add('id', id)
          ..add('name', name)
          ..add('createdAt', createdAt)
          ..add('users', users)
          ..add('file', file))
        .toString();
  }
}

class GetEntityDataResponseBuilder
    implements Builder<GetEntityDataResponse, GetEntityDataResponseBuilder> {
  _$GetEntityDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  ListBuilder<String>? _users;
  ListBuilder<String> get users => _$this._users ??= new ListBuilder<String>();
  set users(ListBuilder<String>? users) => _$this._users = users;

  GetFileDataResponseBuilder? _file;
  GetFileDataResponseBuilder get file =>
      _$this._file ??= new GetFileDataResponseBuilder();
  set file(GetFileDataResponseBuilder? file) => _$this._file = file;

  GetEntityDataResponseBuilder() {
    GetEntityDataResponse._defaults(this);
  }

  GetEntityDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _createdAt = $v.createdAt;
      _users = $v.users.toBuilder();
      _file = $v.file?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetEntityDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetEntityDataResponse;
  }

  @override
  void update(void Function(GetEntityDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetEntityDataResponse build() => _build();

  _$GetEntityDataResponse _build() {
    _$GetEntityDataResponse _$result;
    try {
      _$result = _$v ??
          new _$GetEntityDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'GetEntityDataResponse', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'GetEntityDataResponse', 'name'),
              createdAt: BuiltValueNullFieldError.checkNotNull(
                  createdAt, r'GetEntityDataResponse', 'createdAt'),
              users: users.build(),
              file: _file?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'users';
        users.build();
        _$failedField = 'file';
        _file?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'GetEntityDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
