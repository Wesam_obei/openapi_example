//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/get_user_data_response.dart';
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/get_form_data_response.dart';
import 'package:qarar_api/src/model/get_survey_recipient_view_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'list_survey_data_response.g.dart';

/// ListSurveyDataResponse
///
/// Properties:
/// * [id]
/// * [name]
/// * [description]
/// * [isActive]
/// * [startAt]
/// * [endAt]
/// * [createdAt]
/// * [numberOfResponses]
/// * [numberOfRecipients]
/// * [creator]
/// * [form]
/// * [recipients]
@BuiltValue()
abstract class ListSurveyDataResponse
    implements Built<ListSurveyDataResponse, ListSurveyDataResponseBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'description')
  String get description;

  @BuiltValueField(wireName: r'is_active')
  bool get isActive;

  @BuiltValueField(wireName: r'start_at')
  DateTime get startAt;

  @BuiltValueField(wireName: r'end_at')
  DateTime get endAt;

  @BuiltValueField(wireName: r'created_at')
  DateTime get createdAt;

  @BuiltValueField(wireName: r'number_of_responses')
  num get numberOfResponses;

  @BuiltValueField(wireName: r'number_of_recipients')
  num get numberOfRecipients;

  @BuiltValueField(wireName: r'creator')
  GetUserDataResponse get creator;

  @BuiltValueField(wireName: r'form')
  GetFormDataResponse get form;

  @BuiltValueField(wireName: r'recipients')
  BuiltList<GetSurveyRecipientViewDataResponse> get recipients;

  ListSurveyDataResponse._();

  factory ListSurveyDataResponse(
          [void updates(ListSurveyDataResponseBuilder b)]) =
      _$ListSurveyDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ListSurveyDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ListSurveyDataResponse> get serializer =>
      _$ListSurveyDataResponseSerializer();
}

class _$ListSurveyDataResponseSerializer
    implements PrimitiveSerializer<ListSurveyDataResponse> {
  @override
  final Iterable<Type> types = const [
    ListSurveyDataResponse,
    _$ListSurveyDataResponse
  ];

  @override
  final String wireName = r'ListSurveyDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ListSurveyDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    yield r'description';
    yield serializers.serialize(
      object.description,
      specifiedType: const FullType(String),
    );
    yield r'is_active';
    yield serializers.serialize(
      object.isActive,
      specifiedType: const FullType(bool),
    );
    yield r'start_at';
    yield serializers.serialize(
      object.startAt,
      specifiedType: const FullType(DateTime),
    );
    yield r'end_at';
    yield serializers.serialize(
      object.endAt,
      specifiedType: const FullType(DateTime),
    );
    yield r'created_at';
    yield serializers.serialize(
      object.createdAt,
      specifiedType: const FullType(DateTime),
    );
    yield r'number_of_responses';
    yield serializers.serialize(
      object.numberOfResponses,
      specifiedType: const FullType(num),
    );
    yield r'number_of_recipients';
    yield serializers.serialize(
      object.numberOfRecipients,
      specifiedType: const FullType(num),
    );
    yield r'creator';
    yield serializers.serialize(
      object.creator,
      specifiedType: const FullType(GetUserDataResponse),
    );
    yield r'form';
    yield serializers.serialize(
      object.form,
      specifiedType: const FullType(GetFormDataResponse),
    );
    yield r'recipients';
    yield serializers.serialize(
      object.recipients,
      specifiedType: const FullType(
          BuiltList, [FullType(GetSurveyRecipientViewDataResponse)]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ListSurveyDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ListSurveyDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'description':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.description = valueDes;
          break;
        case r'is_active':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.isActive = valueDes;
          break;
        case r'start_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.startAt = valueDes;
          break;
        case r'end_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.endAt = valueDes;
          break;
        case r'created_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.createdAt = valueDes;
          break;
        case r'number_of_responses':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.numberOfResponses = valueDes;
          break;
        case r'number_of_recipients':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.numberOfRecipients = valueDes;
          break;
        case r'creator':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetUserDataResponse),
          ) as GetUserDataResponse;
          result.creator.replace(valueDes);
          break;
        case r'form':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetFormDataResponse),
          ) as GetFormDataResponse;
          result.form.replace(valueDes);
          break;
        case r'recipients':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                BuiltList, [FullType(GetSurveyRecipientViewDataResponse)]),
          ) as BuiltList<GetSurveyRecipientViewDataResponse>;
          result.recipients.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ListSurveyDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ListSurveyDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
