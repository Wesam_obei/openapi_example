//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/form_components_request.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'form_create_request.g.dart';

/// FormCreateRequest
///
/// Properties:
/// * [name]
/// * [isActive]
/// * [formComponents]
/// * [description]
/// * [fileToken]
@BuiltValue()
abstract class FormCreateRequest
    implements Built<FormCreateRequest, FormCreateRequestBuilder> {
  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'is_active')
  bool get isActive;

  @BuiltValueField(wireName: r'formComponents')
  BuiltList<FormComponentsRequest> get formComponents;

  @BuiltValueField(wireName: r'description')
  String? get description;

  @BuiltValueField(wireName: r'file_token')
  String? get fileToken;

  FormCreateRequest._();

  factory FormCreateRequest([void updates(FormCreateRequestBuilder b)]) =
      _$FormCreateRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(FormCreateRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<FormCreateRequest> get serializer =>
      _$FormCreateRequestSerializer();
}

class _$FormCreateRequestSerializer
    implements PrimitiveSerializer<FormCreateRequest> {
  @override
  final Iterable<Type> types = const [FormCreateRequest, _$FormCreateRequest];

  @override
  final String wireName = r'FormCreateRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    FormCreateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    yield r'is_active';
    yield serializers.serialize(
      object.isActive,
      specifiedType: const FullType(bool),
    );
    yield r'formComponents';
    yield serializers.serialize(
      object.formComponents,
      specifiedType:
          const FullType(BuiltList, [FullType(FormComponentsRequest)]),
    );
    if (object.description != null) {
      yield r'description';
      yield serializers.serialize(
        object.description,
        specifiedType: const FullType(String),
      );
    }
    if (object.fileToken != null) {
      yield r'file_token';
      yield serializers.serialize(
        object.fileToken,
        specifiedType: const FullType(String),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    FormCreateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required FormCreateRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'is_active':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.isActive = valueDes;
          break;
        case r'formComponents':
          final valueDes = serializers.deserialize(
            value,
            specifiedType:
                const FullType(BuiltList, [FullType(FormComponentsRequest)]),
          ) as BuiltList<FormComponentsRequest>;
          result.formComponents.replace(valueDes);
          break;
        case r'description':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.description = valueDes;
          break;
        case r'file_token':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.fileToken = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  FormCreateRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = FormCreateRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
