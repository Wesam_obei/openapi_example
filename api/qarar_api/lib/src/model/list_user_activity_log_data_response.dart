//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/get_audit_log_payload_data_response.dart';
import 'package:qarar_api/src/model/get_user_data_response.dart';
import 'package:qarar_api/src/model/get_audit_log_event_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'list_user_activity_log_data_response.g.dart';

/// ListUserActivityLogDataResponse
///
/// Properties:
/// * [id]
/// * [userId]
/// * [eventId]
/// * [event]
/// * [user]
/// * [payload]
/// * [createdAt]
@BuiltValue()
abstract class ListUserActivityLogDataResponse
    implements
        Built<ListUserActivityLogDataResponse,
            ListUserActivityLogDataResponseBuilder> {
  @BuiltValueField(wireName: r'id')
  String get id;

  @BuiltValueField(wireName: r'user_id')
  String get userId;

  @BuiltValueField(wireName: r'event_id')
  String get eventId;

  @BuiltValueField(wireName: r'event')
  GetAuditLogEventDataResponse get event;

  @BuiltValueField(wireName: r'user')
  GetUserDataResponse get user;

  @BuiltValueField(wireName: r'payload')
  GetAuditLogPayloadDataResponse get payload;

  @BuiltValueField(wireName: r'created_at')
  DateTime get createdAt;

  ListUserActivityLogDataResponse._();

  factory ListUserActivityLogDataResponse(
          [void updates(ListUserActivityLogDataResponseBuilder b)]) =
      _$ListUserActivityLogDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ListUserActivityLogDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ListUserActivityLogDataResponse> get serializer =>
      _$ListUserActivityLogDataResponseSerializer();
}

class _$ListUserActivityLogDataResponseSerializer
    implements PrimitiveSerializer<ListUserActivityLogDataResponse> {
  @override
  final Iterable<Type> types = const [
    ListUserActivityLogDataResponse,
    _$ListUserActivityLogDataResponse
  ];

  @override
  final String wireName = r'ListUserActivityLogDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ListUserActivityLogDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(String),
    );
    yield r'user_id';
    yield serializers.serialize(
      object.userId,
      specifiedType: const FullType(String),
    );
    yield r'event_id';
    yield serializers.serialize(
      object.eventId,
      specifiedType: const FullType(String),
    );
    yield r'event';
    yield serializers.serialize(
      object.event,
      specifiedType: const FullType(GetAuditLogEventDataResponse),
    );
    yield r'user';
    yield serializers.serialize(
      object.user,
      specifiedType: const FullType(GetUserDataResponse),
    );
    yield r'payload';
    yield serializers.serialize(
      object.payload,
      specifiedType: const FullType(GetAuditLogPayloadDataResponse),
    );
    yield r'created_at';
    yield serializers.serialize(
      object.createdAt,
      specifiedType: const FullType(DateTime),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ListUserActivityLogDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ListUserActivityLogDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.id = valueDes;
          break;
        case r'user_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.userId = valueDes;
          break;
        case r'event_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.eventId = valueDes;
          break;
        case r'event':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetAuditLogEventDataResponse),
          ) as GetAuditLogEventDataResponse;
          result.event.replace(valueDes);
          break;
        case r'user':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetUserDataResponse),
          ) as GetUserDataResponse;
          result.user.replace(valueDes);
          break;
        case r'payload':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetAuditLogPayloadDataResponse),
          ) as GetAuditLogPayloadDataResponse;
          result.payload.replace(valueDes);
          break;
        case r'created_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.createdAt = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ListUserActivityLogDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ListUserActivityLogDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
