//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/map_layers_controller_get_incident_reports_map200_response_all_of_data_features_inner_properties_id.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_municipalities_map200_response_all_of_data_features_inner_properties.g.dart';

/// MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerProperties
///
/// Properties:
/// * [id]
/// * [remarks]
/// * [cityName]
/// * [class_]
/// * [name]
/// * [type]
/// * [engname]
/// * [provinceName]
@BuiltValue()
abstract class MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerProperties
    implements
        Built<
            MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerProperties,
            MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder> {
  @BuiltValueField(wireName: r'id')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get id;

  @BuiltValueField(wireName: r'remarks')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get remarks;

  @BuiltValueField(wireName: r'cityName')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get cityName;

  @BuiltValueField(wireName: r'class')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get class_;

  @BuiltValueField(wireName: r'name')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get name;

  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get type;

  @BuiltValueField(wireName: r'engname')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get engname;

  @BuiltValueField(wireName: r'provinceName')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get provinceName;

  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerProperties._();

  factory MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerProperties(
          [void updates(
              MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
                  b)]) =
      _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerProperties;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
              b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<
          MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerProperties>
      get serializer =>
          _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerPropertiesSerializer();
}

class _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerPropertiesSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerProperties> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerProperties,
    _$MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerProperties
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerProperties';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerProperties
        object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.id != null) {
      yield r'id';
      yield serializers.serialize(
        object.id,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.remarks != null) {
      yield r'remarks';
      yield serializers.serialize(
        object.remarks,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.cityName != null) {
      yield r'cityName';
      yield serializers.serialize(
        object.cityName,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.class_ != null) {
      yield r'class';
      yield serializers.serialize(
        object.class_,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.name != null) {
      yield r'name';
      yield serializers.serialize(
        object.name,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.type != null) {
      yield r'type';
      yield serializers.serialize(
        object.type,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.engname != null) {
      yield r'engname';
      yield serializers.serialize(
        object.engname,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.provinceName != null) {
      yield r'provinceName';
      yield serializers.serialize(
        object.provinceName,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerProperties
        object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.id.replace(valueDes);
          break;
        case r'remarks':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.remarks.replace(valueDes);
          break;
        case r'cityName':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.cityName.replace(valueDes);
          break;
        case r'class':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.class_.replace(valueDes);
          break;
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.name.replace(valueDes);
          break;
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.type.replace(valueDes);
          break;
        case r'engname':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.engname.replace(valueDes);
          break;
        case r'provinceName':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.provinceName.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerProperties
      deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetMunicipalitiesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
