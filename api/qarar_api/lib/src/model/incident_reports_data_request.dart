//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/envelope.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'incident_reports_data_request.g.dart';

/// IncidentReportsDataRequest
///
/// Properties:
/// * [envelope]
/// * [fields]
/// * [statuses]
/// * [startTime]
/// * [endTime]
@BuiltValue()
abstract class IncidentReportsDataRequest
    implements
        Built<IncidentReportsDataRequest, IncidentReportsDataRequestBuilder> {
  @BuiltValueField(wireName: r'envelope')
  Envelope get envelope;

  @BuiltValueField(wireName: r'fields')
  BuiltList<String> get fields;

  @BuiltValueField(wireName: r'statuses')
  BuiltList<String> get statuses;

  @BuiltValueField(wireName: r'startTime')
  DateTime get startTime;

  @BuiltValueField(wireName: r'endTime')
  DateTime get endTime;

  IncidentReportsDataRequest._();

  factory IncidentReportsDataRequest(
          [void updates(IncidentReportsDataRequestBuilder b)]) =
      _$IncidentReportsDataRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(IncidentReportsDataRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<IncidentReportsDataRequest> get serializer =>
      _$IncidentReportsDataRequestSerializer();
}

class _$IncidentReportsDataRequestSerializer
    implements PrimitiveSerializer<IncidentReportsDataRequest> {
  @override
  final Iterable<Type> types = const [
    IncidentReportsDataRequest,
    _$IncidentReportsDataRequest
  ];

  @override
  final String wireName = r'IncidentReportsDataRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    IncidentReportsDataRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'envelope';
    yield serializers.serialize(
      object.envelope,
      specifiedType: const FullType(Envelope),
    );
    yield r'fields';
    yield serializers.serialize(
      object.fields,
      specifiedType: const FullType(BuiltList, [FullType(String)]),
    );
    yield r'statuses';
    yield serializers.serialize(
      object.statuses,
      specifiedType: const FullType(BuiltList, [FullType(String)]),
    );
    yield r'startTime';
    yield serializers.serialize(
      object.startTime,
      specifiedType: const FullType(DateTime),
    );
    yield r'endTime';
    yield serializers.serialize(
      object.endTime,
      specifiedType: const FullType(DateTime),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    IncidentReportsDataRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required IncidentReportsDataRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'envelope':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(Envelope),
          ) as Envelope;
          result.envelope.replace(valueDes);
          break;
        case r'fields':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(String)]),
          ) as BuiltList<String>;
          result.fields.replace(valueDes);
          break;
        case r'statuses':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(String)]),
          ) as BuiltList<String>;
          result.statuses.replace(valueDes);
          break;
        case r'startTime':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.startTime = valueDes;
          break;
        case r'endTime':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.endTime = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  IncidentReportsDataRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = IncidentReportsDataRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
