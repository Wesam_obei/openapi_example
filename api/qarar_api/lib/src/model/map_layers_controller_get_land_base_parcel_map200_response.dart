//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/success_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_land_base_parcel_map200_response_all_of_data.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_land_base_parcel_map200_response.g.dart';

/// My custom description
///
/// Properties:
/// * [success]
/// * [data]
@BuiltValue()
abstract class MapLayersControllerGetLandBaseParcelMap200Response
    implements
        SuccessResponse,
        Built<MapLayersControllerGetLandBaseParcelMap200Response,
            MapLayersControllerGetLandBaseParcelMap200ResponseBuilder> {
  MapLayersControllerGetLandBaseParcelMap200Response._();

  factory MapLayersControllerGetLandBaseParcelMap200Response(
          [void updates(
              MapLayersControllerGetLandBaseParcelMap200ResponseBuilder b)]) =
      _$MapLayersControllerGetLandBaseParcelMap200Response;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetLandBaseParcelMap200ResponseBuilder b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<MapLayersControllerGetLandBaseParcelMap200Response>
      get serializer =>
          _$MapLayersControllerGetLandBaseParcelMap200ResponseSerializer();
}

class _$MapLayersControllerGetLandBaseParcelMap200ResponseSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetLandBaseParcelMap200Response> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetLandBaseParcelMap200Response,
    _$MapLayersControllerGetLandBaseParcelMap200Response
  ];

  @override
  final String wireName = r'MapLayersControllerGetLandBaseParcelMap200Response';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetLandBaseParcelMap200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'data';
    yield serializers.serialize(
      object.data,
      specifiedType: const FullType(JsonObject),
    );
    yield r'success';
    yield serializers.serialize(
      object.success,
      specifiedType: const FullType(bool),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetLandBaseParcelMap200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetLandBaseParcelMap200ResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'data':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(JsonObject),
          ) as JsonObject;
          result.data = valueDes;
          break;
        case r'success':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.success = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetLandBaseParcelMap200Response deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = MapLayersControllerGetLandBaseParcelMap200ResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
