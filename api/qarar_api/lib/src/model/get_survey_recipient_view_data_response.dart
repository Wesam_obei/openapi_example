//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/get_survey_response_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'get_survey_recipient_view_data_response.g.dart';

/// GetSurveyRecipientViewDataResponse
///
/// Properties:
/// * [id]
/// * [originalId]
/// * [name]
/// * [response]
/// * [type]
@BuiltValue()
abstract class GetSurveyRecipientViewDataResponse
    implements
        Built<GetSurveyRecipientViewDataResponse,
            GetSurveyRecipientViewDataResponseBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'original_id')
  num get originalId;

  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'response')
  GetSurveyResponseDataResponse get response;

  @BuiltValueField(wireName: r'type')
  String get type;

  GetSurveyRecipientViewDataResponse._();

  factory GetSurveyRecipientViewDataResponse(
          [void updates(GetSurveyRecipientViewDataResponseBuilder b)]) =
      _$GetSurveyRecipientViewDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(GetSurveyRecipientViewDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<GetSurveyRecipientViewDataResponse> get serializer =>
      _$GetSurveyRecipientViewDataResponseSerializer();
}

class _$GetSurveyRecipientViewDataResponseSerializer
    implements PrimitiveSerializer<GetSurveyRecipientViewDataResponse> {
  @override
  final Iterable<Type> types = const [
    GetSurveyRecipientViewDataResponse,
    _$GetSurveyRecipientViewDataResponse
  ];

  @override
  final String wireName = r'GetSurveyRecipientViewDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    GetSurveyRecipientViewDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'original_id';
    yield serializers.serialize(
      object.originalId,
      specifiedType: const FullType(num),
    );
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    yield r'response';
    yield serializers.serialize(
      object.response,
      specifiedType: const FullType(GetSurveyResponseDataResponse),
    );
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(String),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    GetSurveyRecipientViewDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required GetSurveyRecipientViewDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'original_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.originalId = valueDes;
          break;
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'response':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetSurveyResponseDataResponse),
          ) as GetSurveyResponseDataResponse;
          result.response.replace(valueDes);
          break;
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.type = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  GetSurveyRecipientViewDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = GetSurveyRecipientViewDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
