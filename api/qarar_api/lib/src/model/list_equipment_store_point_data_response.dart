//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/get_entity_data_response.dart';
import 'package:qarar_api/src/model/location_data.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'list_equipment_store_point_data_response.g.dart';

/// ListEquipmentStorePointDataResponse
///
/// Properties:
/// * [id]
/// * [name]
/// * [description]
/// * [location]
/// * [createdAt]
/// * [entity]
@BuiltValue()
abstract class ListEquipmentStorePointDataResponse
    implements
        Built<ListEquipmentStorePointDataResponse,
            ListEquipmentStorePointDataResponseBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'description')
  String get description;

  @BuiltValueField(wireName: r'location')
  LocationData get location;

  @BuiltValueField(wireName: r'created_at')
  DateTime get createdAt;

  @BuiltValueField(wireName: r'entity')
  GetEntityDataResponse get entity;

  ListEquipmentStorePointDataResponse._();

  factory ListEquipmentStorePointDataResponse(
          [void updates(ListEquipmentStorePointDataResponseBuilder b)]) =
      _$ListEquipmentStorePointDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ListEquipmentStorePointDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ListEquipmentStorePointDataResponse> get serializer =>
      _$ListEquipmentStorePointDataResponseSerializer();
}

class _$ListEquipmentStorePointDataResponseSerializer
    implements PrimitiveSerializer<ListEquipmentStorePointDataResponse> {
  @override
  final Iterable<Type> types = const [
    ListEquipmentStorePointDataResponse,
    _$ListEquipmentStorePointDataResponse
  ];

  @override
  final String wireName = r'ListEquipmentStorePointDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ListEquipmentStorePointDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    yield r'description';
    yield serializers.serialize(
      object.description,
      specifiedType: const FullType(String),
    );
    yield r'location';
    yield serializers.serialize(
      object.location,
      specifiedType: const FullType(LocationData),
    );
    yield r'created_at';
    yield serializers.serialize(
      object.createdAt,
      specifiedType: const FullType(DateTime),
    );
    yield r'entity';
    yield serializers.serialize(
      object.entity,
      specifiedType: const FullType(GetEntityDataResponse),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ListEquipmentStorePointDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ListEquipmentStorePointDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'description':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.description = valueDes;
          break;
        case r'location':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(LocationData),
          ) as LocationData;
          result.location.replace(valueDes);
          break;
        case r'created_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.createdAt = valueDes;
          break;
        case r'entity':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetEntityDataResponse),
          ) as GetEntityDataResponse;
          result.entity.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ListEquipmentStorePointDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ListEquipmentStorePointDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
