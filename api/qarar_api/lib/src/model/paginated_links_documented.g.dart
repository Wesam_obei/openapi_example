// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'paginated_links_documented.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PaginatedLinksDocumented extends PaginatedLinksDocumented {
  @override
  final String? first;
  @override
  final String? previous;
  @override
  final String? current;
  @override
  final String? next;
  @override
  final String? last;

  factory _$PaginatedLinksDocumented(
          [void Function(PaginatedLinksDocumentedBuilder)? updates]) =>
      (new PaginatedLinksDocumentedBuilder()..update(updates))._build();

  _$PaginatedLinksDocumented._(
      {this.first, this.previous, this.current, this.next, this.last})
      : super._();

  @override
  PaginatedLinksDocumented rebuild(
          void Function(PaginatedLinksDocumentedBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PaginatedLinksDocumentedBuilder toBuilder() =>
      new PaginatedLinksDocumentedBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PaginatedLinksDocumented &&
        first == other.first &&
        previous == other.previous &&
        current == other.current &&
        next == other.next &&
        last == other.last;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, first.hashCode);
    _$hash = $jc(_$hash, previous.hashCode);
    _$hash = $jc(_$hash, current.hashCode);
    _$hash = $jc(_$hash, next.hashCode);
    _$hash = $jc(_$hash, last.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'PaginatedLinksDocumented')
          ..add('first', first)
          ..add('previous', previous)
          ..add('current', current)
          ..add('next', next)
          ..add('last', last))
        .toString();
  }
}

class PaginatedLinksDocumentedBuilder
    implements
        Builder<PaginatedLinksDocumented, PaginatedLinksDocumentedBuilder> {
  _$PaginatedLinksDocumented? _$v;

  String? _first;
  String? get first => _$this._first;
  set first(String? first) => _$this._first = first;

  String? _previous;
  String? get previous => _$this._previous;
  set previous(String? previous) => _$this._previous = previous;

  String? _current;
  String? get current => _$this._current;
  set current(String? current) => _$this._current = current;

  String? _next;
  String? get next => _$this._next;
  set next(String? next) => _$this._next = next;

  String? _last;
  String? get last => _$this._last;
  set last(String? last) => _$this._last = last;

  PaginatedLinksDocumentedBuilder() {
    PaginatedLinksDocumented._defaults(this);
  }

  PaginatedLinksDocumentedBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _first = $v.first;
      _previous = $v.previous;
      _current = $v.current;
      _next = $v.next;
      _last = $v.last;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PaginatedLinksDocumented other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$PaginatedLinksDocumented;
  }

  @override
  void update(void Function(PaginatedLinksDocumentedBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  PaginatedLinksDocumented build() => _build();

  _$PaginatedLinksDocumented _build() {
    final _$result = _$v ??
        new _$PaginatedLinksDocumented._(
            first: first,
            previous: previous,
            current: current,
            next: next,
            last: last);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
