//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/get_plan_version_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'get_plan_data_response.g.dart';

/// GetPlanDataResponse
///
/// Properties:
/// * [id]
/// * [name]
/// * [description]
/// * [versions]
@BuiltValue()
abstract class GetPlanDataResponse
    implements Built<GetPlanDataResponse, GetPlanDataResponseBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'description')
  String get description;

  @BuiltValueField(wireName: r'versions')
  BuiltList<GetPlanVersionDataResponse> get versions;

  GetPlanDataResponse._();

  factory GetPlanDataResponse([void updates(GetPlanDataResponseBuilder b)]) =
      _$GetPlanDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(GetPlanDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<GetPlanDataResponse> get serializer =>
      _$GetPlanDataResponseSerializer();
}

class _$GetPlanDataResponseSerializer
    implements PrimitiveSerializer<GetPlanDataResponse> {
  @override
  final Iterable<Type> types = const [
    GetPlanDataResponse,
    _$GetPlanDataResponse
  ];

  @override
  final String wireName = r'GetPlanDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    GetPlanDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    yield r'description';
    yield serializers.serialize(
      object.description,
      specifiedType: const FullType(String),
    );
    yield r'versions';
    yield serializers.serialize(
      object.versions,
      specifiedType:
          const FullType(BuiltList, [FullType(GetPlanVersionDataResponse)]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    GetPlanDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required GetPlanDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'description':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.description = valueDes;
          break;
        case r'versions':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                BuiltList, [FullType(GetPlanVersionDataResponse)]),
          ) as BuiltList<GetPlanVersionDataResponse>;
          result.versions.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  GetPlanDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = GetPlanDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
