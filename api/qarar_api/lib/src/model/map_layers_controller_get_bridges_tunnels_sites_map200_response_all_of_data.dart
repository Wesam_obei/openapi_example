//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_bridges_tunnels_sites_map200_response_all_of_data_features_inner.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_bridges_tunnels_sites_map200_response_all_of_data.g.dart';

/// MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData
///
/// Properties:
/// * [type]
/// * [features]
@BuiltValue()
abstract class MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData
    implements
        Built<MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData,
            MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataBuilder> {
  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum
      get type;
  // enum typeEnum {  FeatureCollection,  };

  @BuiltValueField(wireName: r'features')
  BuiltList<
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner>
      get features;

  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData._();

  factory MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData(
          [void updates(
              MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataBuilder
                  b)]) =
      _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataBuilder
              b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData>
      get serializer =>
          _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataSerializer();
}

class _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData,
    _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum),
    );
    yield r'features';
    yield serializers.serialize(
      object.features,
      specifiedType: const FullType(BuiltList, [
        FullType(
            MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner)
      ]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum),
          ) as MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum;
          result.type = valueDes;
          break;
        case r'features':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [
              FullType(
                  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner)
            ]),
          ) as BuiltList<
              MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInner>;
          result.features.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfData deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum
    extends EnumClass {
  @BuiltValueEnumConst(wireName: r'FeatureCollection', fallback: true)
  static const MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum
      featureCollection =
      _$mapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum_featureCollection;

  static Serializer<
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum>
      get serializer =>
          _$mapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnumSerializer;

  const MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum._(
      String name)
      : super(name);

  static BuiltSet<
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum>
      get values =>
          _$mapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnumValues;
  static MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnum
      valueOf(String name) =>
          _$mapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataTypeEnumValueOf(
              name);
}
