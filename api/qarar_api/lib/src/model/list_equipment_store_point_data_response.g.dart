// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_equipment_store_point_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ListEquipmentStorePointDataResponse
    extends ListEquipmentStorePointDataResponse {
  @override
  final num id;
  @override
  final String name;
  @override
  final String description;
  @override
  final LocationData location;
  @override
  final DateTime createdAt;
  @override
  final GetEntityDataResponse entity;

  factory _$ListEquipmentStorePointDataResponse(
          [void Function(ListEquipmentStorePointDataResponseBuilder)?
              updates]) =>
      (new ListEquipmentStorePointDataResponseBuilder()..update(updates))
          ._build();

  _$ListEquipmentStorePointDataResponse._(
      {required this.id,
      required this.name,
      required this.description,
      required this.location,
      required this.createdAt,
      required this.entity})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'ListEquipmentStorePointDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, r'ListEquipmentStorePointDataResponse', 'name');
    BuiltValueNullFieldError.checkNotNull(
        description, r'ListEquipmentStorePointDataResponse', 'description');
    BuiltValueNullFieldError.checkNotNull(
        location, r'ListEquipmentStorePointDataResponse', 'location');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'ListEquipmentStorePointDataResponse', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        entity, r'ListEquipmentStorePointDataResponse', 'entity');
  }

  @override
  ListEquipmentStorePointDataResponse rebuild(
          void Function(ListEquipmentStorePointDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ListEquipmentStorePointDataResponseBuilder toBuilder() =>
      new ListEquipmentStorePointDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ListEquipmentStorePointDataResponse &&
        id == other.id &&
        name == other.name &&
        description == other.description &&
        location == other.location &&
        createdAt == other.createdAt &&
        entity == other.entity;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, location.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, entity.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ListEquipmentStorePointDataResponse')
          ..add('id', id)
          ..add('name', name)
          ..add('description', description)
          ..add('location', location)
          ..add('createdAt', createdAt)
          ..add('entity', entity))
        .toString();
  }
}

class ListEquipmentStorePointDataResponseBuilder
    implements
        Builder<ListEquipmentStorePointDataResponse,
            ListEquipmentStorePointDataResponseBuilder> {
  _$ListEquipmentStorePointDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  LocationDataBuilder? _location;
  LocationDataBuilder get location =>
      _$this._location ??= new LocationDataBuilder();
  set location(LocationDataBuilder? location) => _$this._location = location;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  GetEntityDataResponseBuilder? _entity;
  GetEntityDataResponseBuilder get entity =>
      _$this._entity ??= new GetEntityDataResponseBuilder();
  set entity(GetEntityDataResponseBuilder? entity) => _$this._entity = entity;

  ListEquipmentStorePointDataResponseBuilder() {
    ListEquipmentStorePointDataResponse._defaults(this);
  }

  ListEquipmentStorePointDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _description = $v.description;
      _location = $v.location.toBuilder();
      _createdAt = $v.createdAt;
      _entity = $v.entity.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ListEquipmentStorePointDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ListEquipmentStorePointDataResponse;
  }

  @override
  void update(
      void Function(ListEquipmentStorePointDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ListEquipmentStorePointDataResponse build() => _build();

  _$ListEquipmentStorePointDataResponse _build() {
    _$ListEquipmentStorePointDataResponse _$result;
    try {
      _$result = _$v ??
          new _$ListEquipmentStorePointDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'ListEquipmentStorePointDataResponse', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'ListEquipmentStorePointDataResponse', 'name'),
              description: BuiltValueNullFieldError.checkNotNull(description,
                  r'ListEquipmentStorePointDataResponse', 'description'),
              location: location.build(),
              createdAt: BuiltValueNullFieldError.checkNotNull(createdAt,
                  r'ListEquipmentStorePointDataResponse', 'createdAt'),
              entity: entity.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'location';
        location.build();

        _$failedField = 'entity';
        entity.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ListEquipmentStorePointDataResponse',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
