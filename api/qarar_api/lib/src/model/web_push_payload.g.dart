// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'web_push_payload.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$WebPushPayload extends WebPushPayload {
  @override
  final num id;
  @override
  final String title;
  @override
  final String body;
  @override
  final String icon;
  @override
  final WebPushPayloadData data;

  factory _$WebPushPayload([void Function(WebPushPayloadBuilder)? updates]) =>
      (new WebPushPayloadBuilder()..update(updates))._build();

  _$WebPushPayload._(
      {required this.id,
      required this.title,
      required this.body,
      required this.icon,
      required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, r'WebPushPayload', 'id');
    BuiltValueNullFieldError.checkNotNull(title, r'WebPushPayload', 'title');
    BuiltValueNullFieldError.checkNotNull(body, r'WebPushPayload', 'body');
    BuiltValueNullFieldError.checkNotNull(icon, r'WebPushPayload', 'icon');
    BuiltValueNullFieldError.checkNotNull(data, r'WebPushPayload', 'data');
  }

  @override
  WebPushPayload rebuild(void Function(WebPushPayloadBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  WebPushPayloadBuilder toBuilder() =>
      new WebPushPayloadBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is WebPushPayload &&
        id == other.id &&
        title == other.title &&
        body == other.body &&
        icon == other.icon &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, title.hashCode);
    _$hash = $jc(_$hash, body.hashCode);
    _$hash = $jc(_$hash, icon.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'WebPushPayload')
          ..add('id', id)
          ..add('title', title)
          ..add('body', body)
          ..add('icon', icon)
          ..add('data', data))
        .toString();
  }
}

class WebPushPayloadBuilder
    implements Builder<WebPushPayload, WebPushPayloadBuilder> {
  _$WebPushPayload? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  String? _body;
  String? get body => _$this._body;
  set body(String? body) => _$this._body = body;

  String? _icon;
  String? get icon => _$this._icon;
  set icon(String? icon) => _$this._icon = icon;

  WebPushPayloadDataBuilder? _data;
  WebPushPayloadDataBuilder get data =>
      _$this._data ??= new WebPushPayloadDataBuilder();
  set data(WebPushPayloadDataBuilder? data) => _$this._data = data;

  WebPushPayloadBuilder() {
    WebPushPayload._defaults(this);
  }

  WebPushPayloadBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _title = $v.title;
      _body = $v.body;
      _icon = $v.icon;
      _data = $v.data.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(WebPushPayload other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$WebPushPayload;
  }

  @override
  void update(void Function(WebPushPayloadBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  WebPushPayload build() => _build();

  _$WebPushPayload _build() {
    _$WebPushPayload _$result;
    try {
      _$result = _$v ??
          new _$WebPushPayload._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'WebPushPayload', 'id'),
              title: BuiltValueNullFieldError.checkNotNull(
                  title, r'WebPushPayload', 'title'),
              body: BuiltValueNullFieldError.checkNotNull(
                  body, r'WebPushPayload', 'body'),
              icon: BuiltValueNullFieldError.checkNotNull(
                  icon, r'WebPushPayload', 'icon'),
              data: data.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'data';
        data.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'WebPushPayload', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
