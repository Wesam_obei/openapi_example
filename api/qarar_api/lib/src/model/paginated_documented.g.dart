// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'paginated_documented.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

abstract class PaginatedDocumentedBuilder {
  void replace(PaginatedDocumented other);
  void update(void Function(PaginatedDocumentedBuilder) updates);
  bool? get success;
  set success(bool? success);

  ListBuilder<JsonObject> get data;
  set data(ListBuilder<JsonObject>? data);

  PaginatedMetaDocumentedBuilder get meta;
  set meta(PaginatedMetaDocumentedBuilder? meta);

  PaginatedLinksDocumentedBuilder get links;
  set links(PaginatedLinksDocumentedBuilder? links);
}

class _$$PaginatedDocumented extends $PaginatedDocumented {
  @override
  final bool success;
  @override
  final BuiltList<JsonObject> data;
  @override
  final PaginatedMetaDocumented meta;
  @override
  final PaginatedLinksDocumented links;

  factory _$$PaginatedDocumented(
          [void Function($PaginatedDocumentedBuilder)? updates]) =>
      (new $PaginatedDocumentedBuilder()..update(updates))._build();

  _$$PaginatedDocumented._(
      {required this.success,
      required this.data,
      required this.meta,
      required this.links})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'$PaginatedDocumented', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'$PaginatedDocumented', 'data');
    BuiltValueNullFieldError.checkNotNull(
        meta, r'$PaginatedDocumented', 'meta');
    BuiltValueNullFieldError.checkNotNull(
        links, r'$PaginatedDocumented', 'links');
  }

  @override
  $PaginatedDocumented rebuild(
          void Function($PaginatedDocumentedBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  $PaginatedDocumentedBuilder toBuilder() =>
      new $PaginatedDocumentedBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is $PaginatedDocumented &&
        success == other.success &&
        data == other.data &&
        meta == other.meta &&
        links == other.links;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jc(_$hash, meta.hashCode);
    _$hash = $jc(_$hash, links.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'$PaginatedDocumented')
          ..add('success', success)
          ..add('data', data)
          ..add('meta', meta)
          ..add('links', links))
        .toString();
  }
}

class $PaginatedDocumentedBuilder
    implements
        Builder<$PaginatedDocumented, $PaginatedDocumentedBuilder>,
        PaginatedDocumentedBuilder {
  _$$PaginatedDocumented? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  ListBuilder<JsonObject>? _data;
  ListBuilder<JsonObject> get data =>
      _$this._data ??= new ListBuilder<JsonObject>();
  set data(covariant ListBuilder<JsonObject>? data) => _$this._data = data;

  PaginatedMetaDocumentedBuilder? _meta;
  PaginatedMetaDocumentedBuilder get meta =>
      _$this._meta ??= new PaginatedMetaDocumentedBuilder();
  set meta(covariant PaginatedMetaDocumentedBuilder? meta) =>
      _$this._meta = meta;

  PaginatedLinksDocumentedBuilder? _links;
  PaginatedLinksDocumentedBuilder get links =>
      _$this._links ??= new PaginatedLinksDocumentedBuilder();
  set links(covariant PaginatedLinksDocumentedBuilder? links) =>
      _$this._links = links;

  $PaginatedDocumentedBuilder() {
    $PaginatedDocumented._defaults(this);
  }

  $PaginatedDocumentedBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data.toBuilder();
      _meta = $v.meta.toBuilder();
      _links = $v.links.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant $PaginatedDocumented other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$$PaginatedDocumented;
  }

  @override
  void update(void Function($PaginatedDocumentedBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  $PaginatedDocumented build() => _build();

  _$$PaginatedDocumented _build() {
    _$$PaginatedDocumented _$result;
    try {
      _$result = _$v ??
          new _$$PaginatedDocumented._(
              success: BuiltValueNullFieldError.checkNotNull(
                  success, r'$PaginatedDocumented', 'success'),
              data: data.build(),
              meta: meta.build(),
              links: links.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'data';
        data.build();
        _$failedField = 'meta';
        meta.build();
        _$failedField = 'links';
        links.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'$PaginatedDocumented', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
