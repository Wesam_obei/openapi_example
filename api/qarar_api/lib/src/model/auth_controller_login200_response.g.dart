// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_controller_login200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$AuthControllerLogin200Response extends AuthControllerLogin200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$AuthControllerLogin200Response(
          [void Function(AuthControllerLogin200ResponseBuilder)? updates]) =>
      (new AuthControllerLogin200ResponseBuilder()..update(updates))._build();

  _$AuthControllerLogin200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'AuthControllerLogin200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'AuthControllerLogin200Response', 'data');
  }

  @override
  AuthControllerLogin200Response rebuild(
          void Function(AuthControllerLogin200ResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AuthControllerLogin200ResponseBuilder toBuilder() =>
      new AuthControllerLogin200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AuthControllerLogin200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'AuthControllerLogin200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class AuthControllerLogin200ResponseBuilder
    implements
        Builder<AuthControllerLogin200Response,
            AuthControllerLogin200ResponseBuilder>,
        SuccessResponseBuilder {
  _$AuthControllerLogin200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  AuthControllerLogin200ResponseBuilder() {
    AuthControllerLogin200Response._defaults(this);
  }

  AuthControllerLogin200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant AuthControllerLogin200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$AuthControllerLogin200Response;
  }

  @override
  void update(void Function(AuthControllerLogin200ResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  AuthControllerLogin200Response build() => _build();

  _$AuthControllerLogin200Response _build() {
    final _$result = _$v ??
        new _$AuthControllerLogin200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success, r'AuthControllerLogin200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'AuthControllerLogin200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
