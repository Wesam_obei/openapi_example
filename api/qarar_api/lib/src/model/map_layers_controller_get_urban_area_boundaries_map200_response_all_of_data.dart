//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_urban_area_boundaries_map200_response_all_of_data_features_inner.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_urban_area_boundaries_map200_response_all_of_data.g.dart';

/// MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData
///
/// Properties:
/// * [type]
/// * [features]
@BuiltValue()
abstract class MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData
    implements
        Built<MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData,
            MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataBuilder> {
  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum
      get type;
  // enum typeEnum {  FeatureCollection,  };

  @BuiltValueField(wireName: r'features')
  BuiltList<
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner>
      get features;

  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData._();

  factory MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData(
          [void updates(
              MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataBuilder
                  b)]) =
      _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataBuilder
              b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData>
      get serializer =>
          _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataSerializer();
}

class _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData,
    _$MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum),
    );
    yield r'features';
    yield serializers.serialize(
      object.features,
      specifiedType: const FullType(BuiltList, [
        FullType(
            MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner)
      ]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum),
          ) as MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum;
          result.type = valueDes;
          break;
        case r'features':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [
              FullType(
                  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner)
            ]),
          ) as BuiltList<
              MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInner>;
          result.features.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfData deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum
    extends EnumClass {
  @BuiltValueEnumConst(wireName: r'FeatureCollection', fallback: true)
  static const MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum
      featureCollection =
      _$mapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum_featureCollection;

  static Serializer<
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum>
      get serializer =>
          _$mapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnumSerializer;

  const MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum._(
      String name)
      : super(name);

  static BuiltSet<
          MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum>
      get values =>
          _$mapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnumValues;
  static MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnum
      valueOf(String name) =>
          _$mapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataTypeEnumValueOf(
              name);
}
