//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/widget_config.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'user_dashboard_update_request.g.dart';

/// UserDashboardUpdateRequest
///
/// Properties:
/// * [name]
/// * [settings]
@BuiltValue()
abstract class UserDashboardUpdateRequest
    implements
        Built<UserDashboardUpdateRequest, UserDashboardUpdateRequestBuilder> {
  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'settings')
  BuiltList<WidgetConfig>? get settings;

  UserDashboardUpdateRequest._();

  factory UserDashboardUpdateRequest(
          [void updates(UserDashboardUpdateRequestBuilder b)]) =
      _$UserDashboardUpdateRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(UserDashboardUpdateRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<UserDashboardUpdateRequest> get serializer =>
      _$UserDashboardUpdateRequestSerializer();
}

class _$UserDashboardUpdateRequestSerializer
    implements PrimitiveSerializer<UserDashboardUpdateRequest> {
  @override
  final Iterable<Type> types = const [
    UserDashboardUpdateRequest,
    _$UserDashboardUpdateRequest
  ];

  @override
  final String wireName = r'UserDashboardUpdateRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    UserDashboardUpdateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    yield r'settings';
    yield object.settings == null
        ? null
        : serializers.serialize(
            object.settings,
            specifiedType:
                const FullType.nullable(BuiltList, [FullType(WidgetConfig)]),
          );
  }

  @override
  Object serialize(
    Serializers serializers,
    UserDashboardUpdateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required UserDashboardUpdateRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'settings':
          final valueDes = serializers.deserialize(
            value,
            specifiedType:
                const FullType.nullable(BuiltList, [FullType(WidgetConfig)]),
          ) as BuiltList<WidgetConfig>?;
          if (valueDes == null) continue;
          result.settings.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  UserDashboardUpdateRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = UserDashboardUpdateRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
