// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_raqmy_info.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetRaqmyInfo extends GetRaqmyInfo {
  @override
  final num employeeId;

  factory _$GetRaqmyInfo([void Function(GetRaqmyInfoBuilder)? updates]) =>
      (new GetRaqmyInfoBuilder()..update(updates))._build();

  _$GetRaqmyInfo._({required this.employeeId}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        employeeId, r'GetRaqmyInfo', 'employeeId');
  }

  @override
  GetRaqmyInfo rebuild(void Function(GetRaqmyInfoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetRaqmyInfoBuilder toBuilder() => new GetRaqmyInfoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetRaqmyInfo && employeeId == other.employeeId;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, employeeId.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetRaqmyInfo')
          ..add('employeeId', employeeId))
        .toString();
  }
}

class GetRaqmyInfoBuilder
    implements Builder<GetRaqmyInfo, GetRaqmyInfoBuilder> {
  _$GetRaqmyInfo? _$v;

  num? _employeeId;
  num? get employeeId => _$this._employeeId;
  set employeeId(num? employeeId) => _$this._employeeId = employeeId;

  GetRaqmyInfoBuilder() {
    GetRaqmyInfo._defaults(this);
  }

  GetRaqmyInfoBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _employeeId = $v.employeeId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetRaqmyInfo other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetRaqmyInfo;
  }

  @override
  void update(void Function(GetRaqmyInfoBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetRaqmyInfo build() => _build();

  _$GetRaqmyInfo _build() {
    final _$result = _$v ??
        new _$GetRaqmyInfo._(
            employeeId: BuiltValueNullFieldError.checkNotNull(
                employeeId, r'GetRaqmyInfo', 'employeeId'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
