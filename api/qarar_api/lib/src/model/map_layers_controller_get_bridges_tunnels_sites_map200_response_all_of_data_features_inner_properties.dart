//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/map_layers_controller_get_incident_reports_map200_response_all_of_data_features_inner_properties_id.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_bridges_tunnels_sites_map200_response_all_of_data_features_inner_properties.g.dart';

/// MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties
///
/// Properties:
/// * [id]
/// * [projectName]
/// * [devSiteDate]
/// * [devsiteDate]
/// * [compRat]
/// * [primDate]
/// * [finalDate]
/// * [projState]
/// * [stages]
/// * [devWebsiteDate]
/// * [primDateNew]
/// * [finalDateNew]
/// * [primDateMun]
/// * [primDateExc]
/// * [finalDateMun]
/// * [finalDateExc]
/// * [devWebsiteDateMun]
/// * [devWebsiteDateExc]
@BuiltValue()
abstract class MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties
    implements
        Built<
            MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties,
            MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder> {
  @BuiltValueField(wireName: r'id')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get id;

  @BuiltValueField(wireName: r'projectName')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get projectName;

  @BuiltValueField(wireName: r'devSiteDate')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get devSiteDate;

  @BuiltValueField(wireName: r'devsiteDate')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get devsiteDate;

  @BuiltValueField(wireName: r'compRat')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get compRat;

  @BuiltValueField(wireName: r'primDate')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get primDate;

  @BuiltValueField(wireName: r'finalDate')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get finalDate;

  @BuiltValueField(wireName: r'projState')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get projState;

  @BuiltValueField(wireName: r'stages')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get stages;

  @BuiltValueField(wireName: r'devWebsiteDate')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get devWebsiteDate;

  @BuiltValueField(wireName: r'primDateNew')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get primDateNew;

  @BuiltValueField(wireName: r'finalDateNew')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get finalDateNew;

  @BuiltValueField(wireName: r'primDateMun')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get primDateMun;

  @BuiltValueField(wireName: r'primDateExc')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get primDateExc;

  @BuiltValueField(wireName: r'finalDateMun')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get finalDateMun;

  @BuiltValueField(wireName: r'finalDateExc')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get finalDateExc;

  @BuiltValueField(wireName: r'devWebsiteDateMun')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get devWebsiteDateMun;

  @BuiltValueField(wireName: r'devWebsiteDateExc')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      get devWebsiteDateExc;

  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties._();

  factory MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties(
          [void updates(
              MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
                  b)]) =
      _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
              b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<
          MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties>
      get serializer =>
          _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesSerializer();
}

class _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties,
    _$MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties
        object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.id != null) {
      yield r'id';
      yield serializers.serialize(
        object.id,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.projectName != null) {
      yield r'projectName';
      yield serializers.serialize(
        object.projectName,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.devSiteDate != null) {
      yield r'devSiteDate';
      yield serializers.serialize(
        object.devSiteDate,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.devsiteDate != null) {
      yield r'devsiteDate';
      yield serializers.serialize(
        object.devsiteDate,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.compRat != null) {
      yield r'compRat';
      yield serializers.serialize(
        object.compRat,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.primDate != null) {
      yield r'primDate';
      yield serializers.serialize(
        object.primDate,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.finalDate != null) {
      yield r'finalDate';
      yield serializers.serialize(
        object.finalDate,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.projState != null) {
      yield r'projState';
      yield serializers.serialize(
        object.projState,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.stages != null) {
      yield r'stages';
      yield serializers.serialize(
        object.stages,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.devWebsiteDate != null) {
      yield r'devWebsiteDate';
      yield serializers.serialize(
        object.devWebsiteDate,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.primDateNew != null) {
      yield r'primDateNew';
      yield serializers.serialize(
        object.primDateNew,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.finalDateNew != null) {
      yield r'finalDateNew';
      yield serializers.serialize(
        object.finalDateNew,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.primDateMun != null) {
      yield r'primDateMun';
      yield serializers.serialize(
        object.primDateMun,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.primDateExc != null) {
      yield r'primDateExc';
      yield serializers.serialize(
        object.primDateExc,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.finalDateMun != null) {
      yield r'finalDateMun';
      yield serializers.serialize(
        object.finalDateMun,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.finalDateExc != null) {
      yield r'finalDateExc';
      yield serializers.serialize(
        object.finalDateExc,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.devWebsiteDateMun != null) {
      yield r'devWebsiteDateMun';
      yield serializers.serialize(
        object.devWebsiteDateMun,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
    if (object.devWebsiteDateExc != null) {
      yield r'devWebsiteDateExc';
      yield serializers.serialize(
        object.devWebsiteDateExc,
        specifiedType: const FullType.nullable(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties
        object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.id.replace(valueDes);
          break;
        case r'projectName':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.projectName.replace(valueDes);
          break;
        case r'devSiteDate':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.devSiteDate.replace(valueDes);
          break;
        case r'devsiteDate':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.devsiteDate.replace(valueDes);
          break;
        case r'compRat':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.compRat.replace(valueDes);
          break;
        case r'primDate':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.primDate.replace(valueDes);
          break;
        case r'finalDate':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.finalDate.replace(valueDes);
          break;
        case r'projState':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.projState.replace(valueDes);
          break;
        case r'stages':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.stages.replace(valueDes);
          break;
        case r'devWebsiteDate':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.devWebsiteDate.replace(valueDes);
          break;
        case r'primDateNew':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.primDateNew.replace(valueDes);
          break;
        case r'finalDateNew':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.finalDateNew.replace(valueDes);
          break;
        case r'primDateMun':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.primDateMun.replace(valueDes);
          break;
        case r'primDateExc':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.primDateExc.replace(valueDes);
          break;
        case r'finalDateMun':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.finalDateMun.replace(valueDes);
          break;
        case r'finalDateExc':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.finalDateExc.replace(valueDes);
          break;
        case r'devWebsiteDateMun':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.devWebsiteDateMun.replace(valueDes);
          break;
        case r'devWebsiteDateExc':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?;
          if (valueDes == null) continue;
          result.devWebsiteDateExc.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties
      deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
