// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'entity_create_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$EntityCreateRequest extends EntityCreateRequest {
  @override
  final String name;
  @override
  final BuiltList<num> users;
  @override
  final String? fileToken;

  factory _$EntityCreateRequest(
          [void Function(EntityCreateRequestBuilder)? updates]) =>
      (new EntityCreateRequestBuilder()..update(updates))._build();

  _$EntityCreateRequest._(
      {required this.name, required this.users, this.fileToken})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(name, r'EntityCreateRequest', 'name');
    BuiltValueNullFieldError.checkNotNull(
        users, r'EntityCreateRequest', 'users');
  }

  @override
  EntityCreateRequest rebuild(
          void Function(EntityCreateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  EntityCreateRequestBuilder toBuilder() =>
      new EntityCreateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EntityCreateRequest &&
        name == other.name &&
        users == other.users &&
        fileToken == other.fileToken;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, users.hashCode);
    _$hash = $jc(_$hash, fileToken.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'EntityCreateRequest')
          ..add('name', name)
          ..add('users', users)
          ..add('fileToken', fileToken))
        .toString();
  }
}

class EntityCreateRequestBuilder
    implements Builder<EntityCreateRequest, EntityCreateRequestBuilder> {
  _$EntityCreateRequest? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  ListBuilder<num>? _users;
  ListBuilder<num> get users => _$this._users ??= new ListBuilder<num>();
  set users(ListBuilder<num>? users) => _$this._users = users;

  String? _fileToken;
  String? get fileToken => _$this._fileToken;
  set fileToken(String? fileToken) => _$this._fileToken = fileToken;

  EntityCreateRequestBuilder() {
    EntityCreateRequest._defaults(this);
  }

  EntityCreateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _users = $v.users.toBuilder();
      _fileToken = $v.fileToken;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(EntityCreateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$EntityCreateRequest;
  }

  @override
  void update(void Function(EntityCreateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  EntityCreateRequest build() => _build();

  _$EntityCreateRequest _build() {
    _$EntityCreateRequest _$result;
    try {
      _$result = _$v ??
          new _$EntityCreateRequest._(
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'EntityCreateRequest', 'name'),
              users: users.build(),
              fileToken: fileToken);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'users';
        users.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'EntityCreateRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
