//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/get_notification_type_response_data.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'get_notification_response_data.g.dart';

/// GetNotificationResponseData
///
/// Properties:
/// * [id]
/// * [notificationType]
@BuiltValue()
abstract class GetNotificationResponseData
    implements
        Built<GetNotificationResponseData, GetNotificationResponseDataBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'notificationType')
  GetNotificationTypeResponseData get notificationType;

  GetNotificationResponseData._();

  factory GetNotificationResponseData(
          [void updates(GetNotificationResponseDataBuilder b)]) =
      _$GetNotificationResponseData;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(GetNotificationResponseDataBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<GetNotificationResponseData> get serializer =>
      _$GetNotificationResponseDataSerializer();
}

class _$GetNotificationResponseDataSerializer
    implements PrimitiveSerializer<GetNotificationResponseData> {
  @override
  final Iterable<Type> types = const [
    GetNotificationResponseData,
    _$GetNotificationResponseData
  ];

  @override
  final String wireName = r'GetNotificationResponseData';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    GetNotificationResponseData object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'notificationType';
    yield serializers.serialize(
      object.notificationType,
      specifiedType: const FullType(GetNotificationTypeResponseData),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    GetNotificationResponseData object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required GetNotificationResponseDataBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'notificationType':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetNotificationTypeResponseData),
          ) as GetNotificationTypeResponseData;
          result.notificationType.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  GetNotificationResponseData deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = GetNotificationResponseDataBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
