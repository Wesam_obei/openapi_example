// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'incident_reports_data_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$IncidentReportsDataRequest extends IncidentReportsDataRequest {
  @override
  final Envelope envelope;
  @override
  final BuiltList<String> fields;
  @override
  final BuiltList<String> statuses;
  @override
  final DateTime startTime;
  @override
  final DateTime endTime;

  factory _$IncidentReportsDataRequest(
          [void Function(IncidentReportsDataRequestBuilder)? updates]) =>
      (new IncidentReportsDataRequestBuilder()..update(updates))._build();

  _$IncidentReportsDataRequest._(
      {required this.envelope,
      required this.fields,
      required this.statuses,
      required this.startTime,
      required this.endTime})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        envelope, r'IncidentReportsDataRequest', 'envelope');
    BuiltValueNullFieldError.checkNotNull(
        fields, r'IncidentReportsDataRequest', 'fields');
    BuiltValueNullFieldError.checkNotNull(
        statuses, r'IncidentReportsDataRequest', 'statuses');
    BuiltValueNullFieldError.checkNotNull(
        startTime, r'IncidentReportsDataRequest', 'startTime');
    BuiltValueNullFieldError.checkNotNull(
        endTime, r'IncidentReportsDataRequest', 'endTime');
  }

  @override
  IncidentReportsDataRequest rebuild(
          void Function(IncidentReportsDataRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  IncidentReportsDataRequestBuilder toBuilder() =>
      new IncidentReportsDataRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is IncidentReportsDataRequest &&
        envelope == other.envelope &&
        fields == other.fields &&
        statuses == other.statuses &&
        startTime == other.startTime &&
        endTime == other.endTime;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, envelope.hashCode);
    _$hash = $jc(_$hash, fields.hashCode);
    _$hash = $jc(_$hash, statuses.hashCode);
    _$hash = $jc(_$hash, startTime.hashCode);
    _$hash = $jc(_$hash, endTime.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'IncidentReportsDataRequest')
          ..add('envelope', envelope)
          ..add('fields', fields)
          ..add('statuses', statuses)
          ..add('startTime', startTime)
          ..add('endTime', endTime))
        .toString();
  }
}

class IncidentReportsDataRequestBuilder
    implements
        Builder<IncidentReportsDataRequest, IncidentReportsDataRequestBuilder> {
  _$IncidentReportsDataRequest? _$v;

  EnvelopeBuilder? _envelope;
  EnvelopeBuilder get envelope => _$this._envelope ??= new EnvelopeBuilder();
  set envelope(EnvelopeBuilder? envelope) => _$this._envelope = envelope;

  ListBuilder<String>? _fields;
  ListBuilder<String> get fields =>
      _$this._fields ??= new ListBuilder<String>();
  set fields(ListBuilder<String>? fields) => _$this._fields = fields;

  ListBuilder<String>? _statuses;
  ListBuilder<String> get statuses =>
      _$this._statuses ??= new ListBuilder<String>();
  set statuses(ListBuilder<String>? statuses) => _$this._statuses = statuses;

  DateTime? _startTime;
  DateTime? get startTime => _$this._startTime;
  set startTime(DateTime? startTime) => _$this._startTime = startTime;

  DateTime? _endTime;
  DateTime? get endTime => _$this._endTime;
  set endTime(DateTime? endTime) => _$this._endTime = endTime;

  IncidentReportsDataRequestBuilder() {
    IncidentReportsDataRequest._defaults(this);
  }

  IncidentReportsDataRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _envelope = $v.envelope.toBuilder();
      _fields = $v.fields.toBuilder();
      _statuses = $v.statuses.toBuilder();
      _startTime = $v.startTime;
      _endTime = $v.endTime;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(IncidentReportsDataRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$IncidentReportsDataRequest;
  }

  @override
  void update(void Function(IncidentReportsDataRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  IncidentReportsDataRequest build() => _build();

  _$IncidentReportsDataRequest _build() {
    _$IncidentReportsDataRequest _$result;
    try {
      _$result = _$v ??
          new _$IncidentReportsDataRequest._(
              envelope: envelope.build(),
              fields: fields.build(),
              statuses: statuses.build(),
              startTime: BuiltValueNullFieldError.checkNotNull(
                  startTime, r'IncidentReportsDataRequest', 'startTime'),
              endTime: BuiltValueNullFieldError.checkNotNull(
                  endTime, r'IncidentReportsDataRequest', 'endTime'));
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'envelope';
        envelope.build();
        _$failedField = 'fields';
        fields.build();
        _$failedField = 'statuses';
        statuses.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'IncidentReportsDataRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
