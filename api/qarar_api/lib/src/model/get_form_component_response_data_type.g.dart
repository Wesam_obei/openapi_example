// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_form_component_response_data_type.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetFormComponentResponseDataType
    extends GetFormComponentResponseDataType {
  @override
  final num id;
  @override
  final FormResponseDataTypeEnum name;

  factory _$GetFormComponentResponseDataType(
          [void Function(GetFormComponentResponseDataTypeBuilder)? updates]) =>
      (new GetFormComponentResponseDataTypeBuilder()..update(updates))._build();

  _$GetFormComponentResponseDataType._({required this.id, required this.name})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'GetFormComponentResponseDataType', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, r'GetFormComponentResponseDataType', 'name');
  }

  @override
  GetFormComponentResponseDataType rebuild(
          void Function(GetFormComponentResponseDataTypeBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetFormComponentResponseDataTypeBuilder toBuilder() =>
      new GetFormComponentResponseDataTypeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetFormComponentResponseDataType &&
        id == other.id &&
        name == other.name;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetFormComponentResponseDataType')
          ..add('id', id)
          ..add('name', name))
        .toString();
  }
}

class GetFormComponentResponseDataTypeBuilder
    implements
        Builder<GetFormComponentResponseDataType,
            GetFormComponentResponseDataTypeBuilder> {
  _$GetFormComponentResponseDataType? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  FormResponseDataTypeEnum? _name;
  FormResponseDataTypeEnum? get name => _$this._name;
  set name(FormResponseDataTypeEnum? name) => _$this._name = name;

  GetFormComponentResponseDataTypeBuilder() {
    GetFormComponentResponseDataType._defaults(this);
  }

  GetFormComponentResponseDataTypeBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetFormComponentResponseDataType other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetFormComponentResponseDataType;
  }

  @override
  void update(void Function(GetFormComponentResponseDataTypeBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetFormComponentResponseDataType build() => _build();

  _$GetFormComponentResponseDataType _build() {
    final _$result = _$v ??
        new _$GetFormComponentResponseDataType._(
            id: BuiltValueNullFieldError.checkNotNull(
                id, r'GetFormComponentResponseDataType', 'id'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, r'GetFormComponentResponseDataType', 'name'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
