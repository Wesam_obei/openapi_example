// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_incident_reports_map200_response_all_of_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum_featureCollection =
    const MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum
        ._('featureCollection');

MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'featureCollection':
      return _$mapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum_featureCollection;
    default:
      return _$mapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum_featureCollection;
  }
}

final BuiltSet<
        MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum>(const <MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum>[
  _$mapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum_featureCollection,
]);

Serializer<MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnumSerializer =
    new _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnumSerializer();

class _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'featureCollection': 'FeatureCollection',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'FeatureCollection': 'featureCollection',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum';

  @override
  Object serialize(
          Serializers serializers,
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum
              object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum
      deserialize(Serializers serializers, Object serialized,
              {FullType specifiedType = FullType.unspecified}) =>
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum
              .valueOf(_fromWire[serialized] ??
                  (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfData
    extends MapLayersControllerGetIncidentReportsMap200ResponseAllOfData {
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum
      type;
  @override
  final BuiltList<
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInner>
      features;

  factory _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfData(
          [void Function(
                  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataBuilder)?
              updates]) =>
      (new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfData._(
      {required this.type, required this.features})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        type,
        r'MapLayersControllerGetIncidentReportsMap200ResponseAllOfData',
        'type');
    BuiltValueNullFieldError.checkNotNull(
        features,
        r'MapLayersControllerGetIncidentReportsMap200ResponseAllOfData',
        'features');
  }

  @override
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfData rebuild(
          void Function(
                  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataBuilder
      toBuilder() =>
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetIncidentReportsMap200ResponseAllOfData &&
        type == other.type &&
        features == other.features;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, features.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetIncidentReportsMap200ResponseAllOfData')
          ..add('type', type)
          ..add('features', features))
        .toString();
  }
}

class MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataBuilder
    implements
        Builder<MapLayersControllerGetIncidentReportsMap200ResponseAllOfData,
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataBuilder> {
  _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfData? _$v;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum? _type;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum?
      get type => _$this._type;
  set type(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum?
              type) =>
      _$this._type = type;

  ListBuilder<
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInner>?
      _features;
  ListBuilder<
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInner>
      get features => _$this._features ??= new ListBuilder<
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInner>();
  set features(
          ListBuilder<
                  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInner>?
              features) =>
      _$this._features = features;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataBuilder() {
    MapLayersControllerGetIncidentReportsMap200ResponseAllOfData._defaults(
        this);
  }

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _features = $v.features.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetIncidentReportsMap200ResponseAllOfData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v =
        other as _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfData;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfData build() =>
      _build();

  _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfData _build() {
    _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfData _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfData._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetIncidentReportsMap200ResponseAllOfData',
                  'type'),
              features: features.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'features';
        features.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetIncidentReportsMap200ResponseAllOfData',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
