// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'comment_create_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$CommentCreateRequest extends CommentCreateRequest {
  @override
  final num surveyResponseAnswerId;
  @override
  final String comment;

  factory _$CommentCreateRequest(
          [void Function(CommentCreateRequestBuilder)? updates]) =>
      (new CommentCreateRequestBuilder()..update(updates))._build();

  _$CommentCreateRequest._(
      {required this.surveyResponseAnswerId, required this.comment})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(surveyResponseAnswerId,
        r'CommentCreateRequest', 'surveyResponseAnswerId');
    BuiltValueNullFieldError.checkNotNull(
        comment, r'CommentCreateRequest', 'comment');
  }

  @override
  CommentCreateRequest rebuild(
          void Function(CommentCreateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CommentCreateRequestBuilder toBuilder() =>
      new CommentCreateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CommentCreateRequest &&
        surveyResponseAnswerId == other.surveyResponseAnswerId &&
        comment == other.comment;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, surveyResponseAnswerId.hashCode);
    _$hash = $jc(_$hash, comment.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'CommentCreateRequest')
          ..add('surveyResponseAnswerId', surveyResponseAnswerId)
          ..add('comment', comment))
        .toString();
  }
}

class CommentCreateRequestBuilder
    implements Builder<CommentCreateRequest, CommentCreateRequestBuilder> {
  _$CommentCreateRequest? _$v;

  num? _surveyResponseAnswerId;
  num? get surveyResponseAnswerId => _$this._surveyResponseAnswerId;
  set surveyResponseAnswerId(num? surveyResponseAnswerId) =>
      _$this._surveyResponseAnswerId = surveyResponseAnswerId;

  String? _comment;
  String? get comment => _$this._comment;
  set comment(String? comment) => _$this._comment = comment;

  CommentCreateRequestBuilder() {
    CommentCreateRequest._defaults(this);
  }

  CommentCreateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _surveyResponseAnswerId = $v.surveyResponseAnswerId;
      _comment = $v.comment;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CommentCreateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$CommentCreateRequest;
  }

  @override
  void update(void Function(CommentCreateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  CommentCreateRequest build() => _build();

  _$CommentCreateRequest _build() {
    final _$result = _$v ??
        new _$CommentCreateRequest._(
            surveyResponseAnswerId: BuiltValueNullFieldError.checkNotNull(
                surveyResponseAnswerId,
                r'CommentCreateRequest',
                'surveyResponseAnswerId'),
            comment: BuiltValueNullFieldError.checkNotNull(
                comment, r'CommentCreateRequest', 'comment'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
