// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_construction_licenses_map200_response_all_of_data_features_inner_properties.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties
    extends MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties {
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      id;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      licenseNo;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      licenseIssueDateH;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      licenseExpiryDate2;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      licenseStatus;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      licenseStatusName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      licenseTypeName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      ammanaId;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      ammanaName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      buildingDesc;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      buildingArea;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      fenceTypeWallType;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      constructionLicenseType;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      districtCode;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      districtName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      supervisorOfficeId;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      supervisorOfficeName;

  factory _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties(
          [void Function(
                  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
              updates]) =>
      (new MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties._(
      {this.id,
      this.licenseNo,
      this.licenseIssueDateH,
      this.licenseExpiryDate2,
      this.licenseStatus,
      this.licenseStatusName,
      this.licenseTypeName,
      this.ammanaId,
      this.ammanaName,
      this.buildingDesc,
      this.buildingArea,
      this.fenceTypeWallType,
      this.constructionLicenseType,
      this.districtCode,
      this.districtName,
      this.supervisorOfficeId,
      this.supervisorOfficeName})
      : super._();

  @override
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties
      rebuild(
              void Function(
                      MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)
                  updates) =>
          (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      toBuilder() =>
          new MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties &&
        id == other.id &&
        licenseNo == other.licenseNo &&
        licenseIssueDateH == other.licenseIssueDateH &&
        licenseExpiryDate2 == other.licenseExpiryDate2 &&
        licenseStatus == other.licenseStatus &&
        licenseStatusName == other.licenseStatusName &&
        licenseTypeName == other.licenseTypeName &&
        ammanaId == other.ammanaId &&
        ammanaName == other.ammanaName &&
        buildingDesc == other.buildingDesc &&
        buildingArea == other.buildingArea &&
        fenceTypeWallType == other.fenceTypeWallType &&
        constructionLicenseType == other.constructionLicenseType &&
        districtCode == other.districtCode &&
        districtName == other.districtName &&
        supervisorOfficeId == other.supervisorOfficeId &&
        supervisorOfficeName == other.supervisorOfficeName;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, licenseNo.hashCode);
    _$hash = $jc(_$hash, licenseIssueDateH.hashCode);
    _$hash = $jc(_$hash, licenseExpiryDate2.hashCode);
    _$hash = $jc(_$hash, licenseStatus.hashCode);
    _$hash = $jc(_$hash, licenseStatusName.hashCode);
    _$hash = $jc(_$hash, licenseTypeName.hashCode);
    _$hash = $jc(_$hash, ammanaId.hashCode);
    _$hash = $jc(_$hash, ammanaName.hashCode);
    _$hash = $jc(_$hash, buildingDesc.hashCode);
    _$hash = $jc(_$hash, buildingArea.hashCode);
    _$hash = $jc(_$hash, fenceTypeWallType.hashCode);
    _$hash = $jc(_$hash, constructionLicenseType.hashCode);
    _$hash = $jc(_$hash, districtCode.hashCode);
    _$hash = $jc(_$hash, districtName.hashCode);
    _$hash = $jc(_$hash, supervisorOfficeId.hashCode);
    _$hash = $jc(_$hash, supervisorOfficeName.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties')
          ..add('id', id)
          ..add('licenseNo', licenseNo)
          ..add('licenseIssueDateH', licenseIssueDateH)
          ..add('licenseExpiryDate2', licenseExpiryDate2)
          ..add('licenseStatus', licenseStatus)
          ..add('licenseStatusName', licenseStatusName)
          ..add('licenseTypeName', licenseTypeName)
          ..add('ammanaId', ammanaId)
          ..add('ammanaName', ammanaName)
          ..add('buildingDesc', buildingDesc)
          ..add('buildingArea', buildingArea)
          ..add('fenceTypeWallType', fenceTypeWallType)
          ..add('constructionLicenseType', constructionLicenseType)
          ..add('districtCode', districtCode)
          ..add('districtName', districtName)
          ..add('supervisorOfficeId', supervisorOfficeId)
          ..add('supervisorOfficeName', supervisorOfficeName))
        .toString();
  }
}

class MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
    implements
        Builder<
            MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties,
            MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder> {
  _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties?
      _$v;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _id;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get id => _$this._id ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set id(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              id) =>
      _$this._id = id;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _licenseNo;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get licenseNo => _$this._licenseNo ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set licenseNo(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              licenseNo) =>
      _$this._licenseNo = licenseNo;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _licenseIssueDateH;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get licenseIssueDateH => _$this._licenseIssueDateH ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set licenseIssueDateH(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              licenseIssueDateH) =>
      _$this._licenseIssueDateH = licenseIssueDateH;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _licenseExpiryDate2;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get licenseExpiryDate2 => _$this._licenseExpiryDate2 ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set licenseExpiryDate2(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              licenseExpiryDate2) =>
      _$this._licenseExpiryDate2 = licenseExpiryDate2;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _licenseStatus;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get licenseStatus => _$this._licenseStatus ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set licenseStatus(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              licenseStatus) =>
      _$this._licenseStatus = licenseStatus;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _licenseStatusName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get licenseStatusName => _$this._licenseStatusName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set licenseStatusName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              licenseStatusName) =>
      _$this._licenseStatusName = licenseStatusName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _licenseTypeName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get licenseTypeName => _$this._licenseTypeName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set licenseTypeName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              licenseTypeName) =>
      _$this._licenseTypeName = licenseTypeName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _ammanaId;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get ammanaId => _$this._ammanaId ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set ammanaId(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              ammanaId) =>
      _$this._ammanaId = ammanaId;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _ammanaName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get ammanaName => _$this._ammanaName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set ammanaName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              ammanaName) =>
      _$this._ammanaName = ammanaName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _buildingDesc;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get buildingDesc => _$this._buildingDesc ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set buildingDesc(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              buildingDesc) =>
      _$this._buildingDesc = buildingDesc;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _buildingArea;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get buildingArea => _$this._buildingArea ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set buildingArea(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              buildingArea) =>
      _$this._buildingArea = buildingArea;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _fenceTypeWallType;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get fenceTypeWallType => _$this._fenceTypeWallType ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set fenceTypeWallType(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              fenceTypeWallType) =>
      _$this._fenceTypeWallType = fenceTypeWallType;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _constructionLicenseType;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get constructionLicenseType => _$this._constructionLicenseType ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set constructionLicenseType(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              constructionLicenseType) =>
      _$this._constructionLicenseType = constructionLicenseType;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _districtCode;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get districtCode => _$this._districtCode ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set districtCode(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              districtCode) =>
      _$this._districtCode = districtCode;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _districtName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get districtName => _$this._districtName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set districtName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              districtName) =>
      _$this._districtName = districtName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _supervisorOfficeId;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get supervisorOfficeId => _$this._supervisorOfficeId ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set supervisorOfficeId(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              supervisorOfficeId) =>
      _$this._supervisorOfficeId = supervisorOfficeId;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _supervisorOfficeName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get supervisorOfficeName => _$this._supervisorOfficeName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set supervisorOfficeName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              supervisorOfficeName) =>
      _$this._supervisorOfficeName = supervisorOfficeName;

  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder() {
    MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties
        ._defaults(this);
  }

  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id?.toBuilder();
      _licenseNo = $v.licenseNo?.toBuilder();
      _licenseIssueDateH = $v.licenseIssueDateH?.toBuilder();
      _licenseExpiryDate2 = $v.licenseExpiryDate2?.toBuilder();
      _licenseStatus = $v.licenseStatus?.toBuilder();
      _licenseStatusName = $v.licenseStatusName?.toBuilder();
      _licenseTypeName = $v.licenseTypeName?.toBuilder();
      _ammanaId = $v.ammanaId?.toBuilder();
      _ammanaName = $v.ammanaName?.toBuilder();
      _buildingDesc = $v.buildingDesc?.toBuilder();
      _buildingArea = $v.buildingArea?.toBuilder();
      _fenceTypeWallType = $v.fenceTypeWallType?.toBuilder();
      _constructionLicenseType = $v.constructionLicenseType?.toBuilder();
      _districtCode = $v.districtCode?.toBuilder();
      _districtName = $v.districtName?.toBuilder();
      _supervisorOfficeId = $v.supervisorOfficeId?.toBuilder();
      _supervisorOfficeName = $v.supervisorOfficeName?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties
      build() => _build();

  _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties
      _build() {
    _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties
        _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties
              ._(
              id: _id?.build(),
              licenseNo: _licenseNo?.build(),
              licenseIssueDateH: _licenseIssueDateH?.build(),
              licenseExpiryDate2: _licenseExpiryDate2?.build(),
              licenseStatus: _licenseStatus?.build(),
              licenseStatusName: _licenseStatusName?.build(),
              licenseTypeName: _licenseTypeName?.build(),
              ammanaId: _ammanaId?.build(),
              ammanaName: _ammanaName?.build(),
              buildingDesc: _buildingDesc?.build(),
              buildingArea: _buildingArea?.build(),
              fenceTypeWallType: _fenceTypeWallType?.build(),
              constructionLicenseType: _constructionLicenseType?.build(),
              districtCode: _districtCode?.build(),
              districtName: _districtName?.build(),
              supervisorOfficeId: _supervisorOfficeId?.build(),
              supervisorOfficeName: _supervisorOfficeName?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'id';
        _id?.build();
        _$failedField = 'licenseNo';
        _licenseNo?.build();
        _$failedField = 'licenseIssueDateH';
        _licenseIssueDateH?.build();
        _$failedField = 'licenseExpiryDate2';
        _licenseExpiryDate2?.build();
        _$failedField = 'licenseStatus';
        _licenseStatus?.build();
        _$failedField = 'licenseStatusName';
        _licenseStatusName?.build();
        _$failedField = 'licenseTypeName';
        _licenseTypeName?.build();
        _$failedField = 'ammanaId';
        _ammanaId?.build();
        _$failedField = 'ammanaName';
        _ammanaName?.build();
        _$failedField = 'buildingDesc';
        _buildingDesc?.build();
        _$failedField = 'buildingArea';
        _buildingArea?.build();
        _$failedField = 'fenceTypeWallType';
        _fenceTypeWallType?.build();
        _$failedField = 'constructionLicenseType';
        _constructionLicenseType?.build();
        _$failedField = 'districtCode';
        _districtCode?.build();
        _$failedField = 'districtName';
        _districtName?.build();
        _$failedField = 'supervisorOfficeId';
        _supervisorOfficeId?.build();
        _$failedField = 'supervisorOfficeName';
        _supervisorOfficeName?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
