// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_sea_out_map200_response_all_of_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum_featureCollection =
    const MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum._(
        'featureCollection');

MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'featureCollection':
      return _$mapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum_featureCollection;
    default:
      return _$mapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum_featureCollection;
  }
}

final BuiltSet<MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum>(const <MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum>[
  _$mapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum_featureCollection,
]);

Serializer<MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnumSerializer =
    new _$MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnumSerializer();

class _$MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'featureCollection': 'FeatureCollection',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'FeatureCollection': 'featureCollection',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum';

  @override
  Object serialize(Serializers serializers,
          MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum deserialize(
          Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetSeaOutMap200ResponseAllOfData
    extends MapLayersControllerGetSeaOutMap200ResponseAllOfData {
  @override
  final MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum type;
  @override
  final BuiltList<
          MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInner>
      features;

  factory _$MapLayersControllerGetSeaOutMap200ResponseAllOfData(
          [void Function(
                  MapLayersControllerGetSeaOutMap200ResponseAllOfDataBuilder)?
              updates]) =>
      (new MapLayersControllerGetSeaOutMap200ResponseAllOfDataBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetSeaOutMap200ResponseAllOfData._(
      {required this.type, required this.features})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        type, r'MapLayersControllerGetSeaOutMap200ResponseAllOfData', 'type');
    BuiltValueNullFieldError.checkNotNull(features,
        r'MapLayersControllerGetSeaOutMap200ResponseAllOfData', 'features');
  }

  @override
  MapLayersControllerGetSeaOutMap200ResponseAllOfData rebuild(
          void Function(
                  MapLayersControllerGetSeaOutMap200ResponseAllOfDataBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetSeaOutMap200ResponseAllOfDataBuilder toBuilder() =>
      new MapLayersControllerGetSeaOutMap200ResponseAllOfDataBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapLayersControllerGetSeaOutMap200ResponseAllOfData &&
        type == other.type &&
        features == other.features;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, features.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetSeaOutMap200ResponseAllOfData')
          ..add('type', type)
          ..add('features', features))
        .toString();
  }
}

class MapLayersControllerGetSeaOutMap200ResponseAllOfDataBuilder
    implements
        Builder<MapLayersControllerGetSeaOutMap200ResponseAllOfData,
            MapLayersControllerGetSeaOutMap200ResponseAllOfDataBuilder> {
  _$MapLayersControllerGetSeaOutMap200ResponseAllOfData? _$v;

  MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum? _type;
  MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum? get type =>
      _$this._type;
  set type(MapLayersControllerGetSeaOutMap200ResponseAllOfDataTypeEnum? type) =>
      _$this._type = type;

  ListBuilder<MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInner>?
      _features;
  ListBuilder<MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInner>
      get features => _$this._features ??= new ListBuilder<
          MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInner>();
  set features(
          ListBuilder<
                  MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInner>?
              features) =>
      _$this._features = features;

  MapLayersControllerGetSeaOutMap200ResponseAllOfDataBuilder() {
    MapLayersControllerGetSeaOutMap200ResponseAllOfData._defaults(this);
  }

  MapLayersControllerGetSeaOutMap200ResponseAllOfDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _features = $v.features.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MapLayersControllerGetSeaOutMap200ResponseAllOfData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapLayersControllerGetSeaOutMap200ResponseAllOfData;
  }

  @override
  void update(
      void Function(MapLayersControllerGetSeaOutMap200ResponseAllOfDataBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetSeaOutMap200ResponseAllOfData build() => _build();

  _$MapLayersControllerGetSeaOutMap200ResponseAllOfData _build() {
    _$MapLayersControllerGetSeaOutMap200ResponseAllOfData _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetSeaOutMap200ResponseAllOfData._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetSeaOutMap200ResponseAllOfData',
                  'type'),
              features: features.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'features';
        features.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetSeaOutMap200ResponseAllOfData',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
