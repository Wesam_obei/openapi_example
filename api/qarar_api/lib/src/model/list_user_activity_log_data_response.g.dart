// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_user_activity_log_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ListUserActivityLogDataResponse
    extends ListUserActivityLogDataResponse {
  @override
  final String id;
  @override
  final String userId;
  @override
  final String eventId;
  @override
  final GetAuditLogEventDataResponse event;
  @override
  final GetUserDataResponse user;
  @override
  final GetAuditLogPayloadDataResponse payload;
  @override
  final DateTime createdAt;

  factory _$ListUserActivityLogDataResponse(
          [void Function(ListUserActivityLogDataResponseBuilder)? updates]) =>
      (new ListUserActivityLogDataResponseBuilder()..update(updates))._build();

  _$ListUserActivityLogDataResponse._(
      {required this.id,
      required this.userId,
      required this.eventId,
      required this.event,
      required this.user,
      required this.payload,
      required this.createdAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'ListUserActivityLogDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        userId, r'ListUserActivityLogDataResponse', 'userId');
    BuiltValueNullFieldError.checkNotNull(
        eventId, r'ListUserActivityLogDataResponse', 'eventId');
    BuiltValueNullFieldError.checkNotNull(
        event, r'ListUserActivityLogDataResponse', 'event');
    BuiltValueNullFieldError.checkNotNull(
        user, r'ListUserActivityLogDataResponse', 'user');
    BuiltValueNullFieldError.checkNotNull(
        payload, r'ListUserActivityLogDataResponse', 'payload');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'ListUserActivityLogDataResponse', 'createdAt');
  }

  @override
  ListUserActivityLogDataResponse rebuild(
          void Function(ListUserActivityLogDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ListUserActivityLogDataResponseBuilder toBuilder() =>
      new ListUserActivityLogDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ListUserActivityLogDataResponse &&
        id == other.id &&
        userId == other.userId &&
        eventId == other.eventId &&
        event == other.event &&
        user == other.user &&
        payload == other.payload &&
        createdAt == other.createdAt;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, userId.hashCode);
    _$hash = $jc(_$hash, eventId.hashCode);
    _$hash = $jc(_$hash, event.hashCode);
    _$hash = $jc(_$hash, user.hashCode);
    _$hash = $jc(_$hash, payload.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ListUserActivityLogDataResponse')
          ..add('id', id)
          ..add('userId', userId)
          ..add('eventId', eventId)
          ..add('event', event)
          ..add('user', user)
          ..add('payload', payload)
          ..add('createdAt', createdAt))
        .toString();
  }
}

class ListUserActivityLogDataResponseBuilder
    implements
        Builder<ListUserActivityLogDataResponse,
            ListUserActivityLogDataResponseBuilder> {
  _$ListUserActivityLogDataResponse? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _userId;
  String? get userId => _$this._userId;
  set userId(String? userId) => _$this._userId = userId;

  String? _eventId;
  String? get eventId => _$this._eventId;
  set eventId(String? eventId) => _$this._eventId = eventId;

  GetAuditLogEventDataResponseBuilder? _event;
  GetAuditLogEventDataResponseBuilder get event =>
      _$this._event ??= new GetAuditLogEventDataResponseBuilder();
  set event(GetAuditLogEventDataResponseBuilder? event) =>
      _$this._event = event;

  GetUserDataResponseBuilder? _user;
  GetUserDataResponseBuilder get user =>
      _$this._user ??= new GetUserDataResponseBuilder();
  set user(GetUserDataResponseBuilder? user) => _$this._user = user;

  GetAuditLogPayloadDataResponseBuilder? _payload;
  GetAuditLogPayloadDataResponseBuilder get payload =>
      _$this._payload ??= new GetAuditLogPayloadDataResponseBuilder();
  set payload(GetAuditLogPayloadDataResponseBuilder? payload) =>
      _$this._payload = payload;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  ListUserActivityLogDataResponseBuilder() {
    ListUserActivityLogDataResponse._defaults(this);
  }

  ListUserActivityLogDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _userId = $v.userId;
      _eventId = $v.eventId;
      _event = $v.event.toBuilder();
      _user = $v.user.toBuilder();
      _payload = $v.payload.toBuilder();
      _createdAt = $v.createdAt;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ListUserActivityLogDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ListUserActivityLogDataResponse;
  }

  @override
  void update(void Function(ListUserActivityLogDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ListUserActivityLogDataResponse build() => _build();

  _$ListUserActivityLogDataResponse _build() {
    _$ListUserActivityLogDataResponse _$result;
    try {
      _$result = _$v ??
          new _$ListUserActivityLogDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'ListUserActivityLogDataResponse', 'id'),
              userId: BuiltValueNullFieldError.checkNotNull(
                  userId, r'ListUserActivityLogDataResponse', 'userId'),
              eventId: BuiltValueNullFieldError.checkNotNull(
                  eventId, r'ListUserActivityLogDataResponse', 'eventId'),
              event: event.build(),
              user: user.build(),
              payload: payload.build(),
              createdAt: BuiltValueNullFieldError.checkNotNull(
                  createdAt, r'ListUserActivityLogDataResponse', 'createdAt'));
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'event';
        event.build();
        _$failedField = 'user';
        user.build();
        _$failedField = 'payload';
        payload.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ListUserActivityLogDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
