// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_land_base_parcel_map200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetLandBaseParcelMap200Response
    extends MapLayersControllerGetLandBaseParcelMap200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$MapLayersControllerGetLandBaseParcelMap200Response(
          [void Function(
                  MapLayersControllerGetLandBaseParcelMap200ResponseBuilder)?
              updates]) =>
      (new MapLayersControllerGetLandBaseParcelMap200ResponseBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetLandBaseParcelMap200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(success,
        r'MapLayersControllerGetLandBaseParcelMap200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'MapLayersControllerGetLandBaseParcelMap200Response', 'data');
  }

  @override
  MapLayersControllerGetLandBaseParcelMap200Response rebuild(
          void Function(
                  MapLayersControllerGetLandBaseParcelMap200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetLandBaseParcelMap200ResponseBuilder toBuilder() =>
      new MapLayersControllerGetLandBaseParcelMap200ResponseBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapLayersControllerGetLandBaseParcelMap200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetLandBaseParcelMap200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class MapLayersControllerGetLandBaseParcelMap200ResponseBuilder
    implements
        Builder<MapLayersControllerGetLandBaseParcelMap200Response,
            MapLayersControllerGetLandBaseParcelMap200ResponseBuilder>,
        SuccessResponseBuilder {
  _$MapLayersControllerGetLandBaseParcelMap200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  MapLayersControllerGetLandBaseParcelMap200ResponseBuilder() {
    MapLayersControllerGetLandBaseParcelMap200Response._defaults(this);
  }

  MapLayersControllerGetLandBaseParcelMap200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      covariant MapLayersControllerGetLandBaseParcelMap200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapLayersControllerGetLandBaseParcelMap200Response;
  }

  @override
  void update(
      void Function(MapLayersControllerGetLandBaseParcelMap200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetLandBaseParcelMap200Response build() => _build();

  _$MapLayersControllerGetLandBaseParcelMap200Response _build() {
    final _$result = _$v ??
        new _$MapLayersControllerGetLandBaseParcelMap200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success,
                r'MapLayersControllerGetLandBaseParcelMap200Response',
                'success'),
            data: BuiltValueNullFieldError.checkNotNull(data,
                r'MapLayersControllerGetLandBaseParcelMap200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
