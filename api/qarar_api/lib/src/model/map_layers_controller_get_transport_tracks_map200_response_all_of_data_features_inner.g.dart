// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_transport_tracks_map200_response_all_of_data_features_inner.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum
    _$mapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature =
    const MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum
        ._('feature');

MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum
    _$mapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'feature':
      return _$mapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature;
    default:
      return _$mapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature;
  }
}

final BuiltSet<
        MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum>
    _$mapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum>(const <MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum>[
  _$mapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature,
]);

Serializer<
        MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum>
    _$mapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer =
    new _$MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer();

class _$MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'feature': 'Feature',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'Feature': 'feature',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum';

  @override
  Object serialize(
          Serializers serializers,
          MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum
              object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum
      deserialize(Serializers serializers, Object serialized,
              {FullType specifiedType = FullType.unspecified}) =>
          MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum
              .valueOf(_fromWire[serialized] ??
                  (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner
    extends MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner {
  @override
  final MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum
      type;
  @override
  final int id;
  @override
  final JsonObject? geometry;
  @override
  final MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerProperties
      properties;

  factory _$MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner(
          [void Function(
                  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerBuilder)?
              updates]) =>
      (new MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner._(
      {required this.type,
      required this.id,
      this.geometry,
      required this.properties})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        type,
        r'MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner',
        'type');
    BuiltValueNullFieldError.checkNotNull(
        id,
        r'MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner',
        'id');
    BuiltValueNullFieldError.checkNotNull(
        properties,
        r'MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner',
        'properties');
  }

  @override
  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner rebuild(
          void Function(
                  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerBuilder
      toBuilder() =>
          new MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner &&
        type == other.type &&
        id == other.id &&
        geometry == other.geometry &&
        properties == other.properties;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, geometry.hashCode);
    _$hash = $jc(_$hash, properties.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner')
          ..add('type', type)
          ..add('id', id)
          ..add('geometry', geometry)
          ..add('properties', properties))
        .toString();
  }
}

class MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerBuilder
    implements
        Builder<
            MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner,
            MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerBuilder> {
  _$MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner?
      _$v;

  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum?
      _type;
  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum?
      get type => _$this._type;
  set type(
          MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerTypeEnum?
              type) =>
      _$this._type = type;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  JsonObject? _geometry;
  JsonObject? get geometry => _$this._geometry;
  set geometry(JsonObject? geometry) => _$this._geometry = geometry;

  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder?
      _properties;
  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      get properties => _$this._properties ??=
          new MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder();
  set properties(
          MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder?
              properties) =>
      _$this._properties = properties;

  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerBuilder() {
    MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner
        ._defaults(this);
  }

  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _id = $v.id;
      _geometry = $v.geometry;
      _properties = $v.properties.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInnerBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner
      build() => _build();

  _$MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner
      _build() {
    _$MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner
        _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner',
                  'type'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id,
                  r'MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner',
                  'id'),
              geometry: geometry,
              properties: properties.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'properties';
        properties.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
