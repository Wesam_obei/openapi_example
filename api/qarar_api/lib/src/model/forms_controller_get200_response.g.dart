// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'forms_controller_get200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$FormsControllerGet200Response extends FormsControllerGet200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$FormsControllerGet200Response(
          [void Function(FormsControllerGet200ResponseBuilder)? updates]) =>
      (new FormsControllerGet200ResponseBuilder()..update(updates))._build();

  _$FormsControllerGet200Response._({required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        success, r'FormsControllerGet200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'FormsControllerGet200Response', 'data');
  }

  @override
  FormsControllerGet200Response rebuild(
          void Function(FormsControllerGet200ResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  FormsControllerGet200ResponseBuilder toBuilder() =>
      new FormsControllerGet200ResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is FormsControllerGet200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'FormsControllerGet200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class FormsControllerGet200ResponseBuilder
    implements
        Builder<FormsControllerGet200Response,
            FormsControllerGet200ResponseBuilder>,
        SuccessResponseBuilder {
  _$FormsControllerGet200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  FormsControllerGet200ResponseBuilder() {
    FormsControllerGet200Response._defaults(this);
  }

  FormsControllerGet200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant FormsControllerGet200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$FormsControllerGet200Response;
  }

  @override
  void update(void Function(FormsControllerGet200ResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  FormsControllerGet200Response build() => _build();

  _$FormsControllerGet200Response _build() {
    final _$result = _$v ??
        new _$FormsControllerGet200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success, r'FormsControllerGet200Response', 'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'FormsControllerGet200Response', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
