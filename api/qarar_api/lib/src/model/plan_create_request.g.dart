// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'plan_create_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PlanCreateRequest extends PlanCreateRequest {
  @override
  final String name;
  @override
  final String description;

  factory _$PlanCreateRequest(
          [void Function(PlanCreateRequestBuilder)? updates]) =>
      (new PlanCreateRequestBuilder()..update(updates))._build();

  _$PlanCreateRequest._({required this.name, required this.description})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(name, r'PlanCreateRequest', 'name');
    BuiltValueNullFieldError.checkNotNull(
        description, r'PlanCreateRequest', 'description');
  }

  @override
  PlanCreateRequest rebuild(void Function(PlanCreateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PlanCreateRequestBuilder toBuilder() =>
      new PlanCreateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PlanCreateRequest &&
        name == other.name &&
        description == other.description;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'PlanCreateRequest')
          ..add('name', name)
          ..add('description', description))
        .toString();
  }
}

class PlanCreateRequestBuilder
    implements Builder<PlanCreateRequest, PlanCreateRequestBuilder> {
  _$PlanCreateRequest? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  PlanCreateRequestBuilder() {
    PlanCreateRequest._defaults(this);
  }

  PlanCreateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _description = $v.description;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PlanCreateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$PlanCreateRequest;
  }

  @override
  void update(void Function(PlanCreateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  PlanCreateRequest build() => _build();

  _$PlanCreateRequest _build() {
    final _$result = _$v ??
        new _$PlanCreateRequest._(
            name: BuiltValueNullFieldError.checkNotNull(
                name, r'PlanCreateRequest', 'name'),
            description: BuiltValueNullFieldError.checkNotNull(
                description, r'PlanCreateRequest', 'description'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
