//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_manhole_map200_response_all_of_data_features_inner_properties.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_manhole_map200_response_all_of_data_features_inner.g.dart';

/// GeoJson Feature
///
/// Properties:
/// * [type]
/// * [id]
/// * [geometry]
/// * [properties]
@BuiltValue()
abstract class MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner
    implements
        Built<MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner,
            MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerBuilder> {
  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerTypeEnum
      get type;
  // enum typeEnum {  Feature,  };

  @BuiltValueField(wireName: r'id')
  int get id;

  @BuiltValueField(wireName: r'geometry')
  JsonObject? get geometry;

  @BuiltValueField(wireName: r'properties')
  MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties
      get properties;

  MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner._();

  factory MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner(
          [void updates(
              MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerBuilder
                  b)]) =
      _$MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerBuilder
              b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<
          MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner>
      get serializer =>
          _$MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerSerializer();
}

class _$MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner,
    _$MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(
          MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerTypeEnum),
    );
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(int),
    );
    yield r'geometry';
    yield object.geometry == null
        ? null
        : serializers.serialize(
            object.geometry,
            specifiedType: const FullType.nullable(JsonObject),
          );
    yield r'properties';
    yield serializers.serialize(
      object.properties,
      specifiedType: const FullType(
          MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerTypeEnum),
          ) as MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerTypeEnum;
          result.type = valueDes;
          break;
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(int),
          ) as int;
          result.id = valueDes;
          break;
        case r'geometry':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(JsonObject),
          ) as JsonObject?;
          if (valueDes == null) continue;
          result.geometry = valueDes;
          break;
        case r'properties':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties),
          ) as MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerProperties;
          result.properties.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInner deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerTypeEnum
    extends EnumClass {
  @BuiltValueEnumConst(wireName: r'Feature', fallback: true)
  static const MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerTypeEnum
      feature =
      _$mapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature;

  static Serializer<
          MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerTypeEnum>
      get serializer =>
          _$mapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer;

  const MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerTypeEnum._(
      String name)
      : super(name);

  static BuiltSet<
          MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerTypeEnum>
      get values =>
          _$mapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerTypeEnumValues;
  static MapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerTypeEnum
      valueOf(String name) =>
          _$mapLayersControllerGetManholeMap200ResponseAllOfDataFeaturesInnerTypeEnumValueOf(
              name);
}
