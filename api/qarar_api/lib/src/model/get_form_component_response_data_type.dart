//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/form_response_data_type_enum.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'get_form_component_response_data_type.g.dart';

/// GetFormComponentResponseDataType
///
/// Properties:
/// * [id]
/// * [name]
@BuiltValue()
abstract class GetFormComponentResponseDataType
    implements
        Built<GetFormComponentResponseDataType,
            GetFormComponentResponseDataTypeBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'name')
  FormResponseDataTypeEnum get name;
  // enum nameEnum {  null,  string,  integer,  number,  date,  datetime,  time,  file,  range-date,  range-datetime,  range-time,  coordinate,  };

  GetFormComponentResponseDataType._();

  factory GetFormComponentResponseDataType(
          [void updates(GetFormComponentResponseDataTypeBuilder b)]) =
      _$GetFormComponentResponseDataType;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(GetFormComponentResponseDataTypeBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<GetFormComponentResponseDataType> get serializer =>
      _$GetFormComponentResponseDataTypeSerializer();
}

class _$GetFormComponentResponseDataTypeSerializer
    implements PrimitiveSerializer<GetFormComponentResponseDataType> {
  @override
  final Iterable<Type> types = const [
    GetFormComponentResponseDataType,
    _$GetFormComponentResponseDataType
  ];

  @override
  final String wireName = r'GetFormComponentResponseDataType';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    GetFormComponentResponseDataType object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(FormResponseDataTypeEnum),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    GetFormComponentResponseDataType object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required GetFormComponentResponseDataTypeBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(FormResponseDataTypeEnum),
          ) as FormResponseDataTypeEnum;
          result.name = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  GetFormComponentResponseDataType deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = GetFormComponentResponseDataTypeBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
