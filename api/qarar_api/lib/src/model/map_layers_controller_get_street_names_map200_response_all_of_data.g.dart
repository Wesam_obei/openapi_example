// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_street_names_map200_response_all_of_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum_featureCollection =
    const MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum._(
        'featureCollection');

MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'featureCollection':
      return _$mapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum_featureCollection;
    default:
      return _$mapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum_featureCollection;
  }
}

final BuiltSet<MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum>(const <MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum>[
  _$mapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum_featureCollection,
]);

Serializer<MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnumSerializer =
    new _$MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnumSerializer();

class _$MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'featureCollection': 'FeatureCollection',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'FeatureCollection': 'featureCollection',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum';

  @override
  Object serialize(
          Serializers serializers,
          MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum
              object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum deserialize(
          Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetStreetNamesMap200ResponseAllOfData
    extends MapLayersControllerGetStreetNamesMap200ResponseAllOfData {
  @override
  final MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum type;
  @override
  final BuiltList<
          MapLayersControllerGetStreetNamesMap200ResponseAllOfDataFeaturesInner>
      features;

  factory _$MapLayersControllerGetStreetNamesMap200ResponseAllOfData(
          [void Function(
                  MapLayersControllerGetStreetNamesMap200ResponseAllOfDataBuilder)?
              updates]) =>
      (new MapLayersControllerGetStreetNamesMap200ResponseAllOfDataBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetStreetNamesMap200ResponseAllOfData._(
      {required this.type, required this.features})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(type,
        r'MapLayersControllerGetStreetNamesMap200ResponseAllOfData', 'type');
    BuiltValueNullFieldError.checkNotNull(
        features,
        r'MapLayersControllerGetStreetNamesMap200ResponseAllOfData',
        'features');
  }

  @override
  MapLayersControllerGetStreetNamesMap200ResponseAllOfData rebuild(
          void Function(
                  MapLayersControllerGetStreetNamesMap200ResponseAllOfDataBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetStreetNamesMap200ResponseAllOfDataBuilder toBuilder() =>
      new MapLayersControllerGetStreetNamesMap200ResponseAllOfDataBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapLayersControllerGetStreetNamesMap200ResponseAllOfData &&
        type == other.type &&
        features == other.features;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, features.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetStreetNamesMap200ResponseAllOfData')
          ..add('type', type)
          ..add('features', features))
        .toString();
  }
}

class MapLayersControllerGetStreetNamesMap200ResponseAllOfDataBuilder
    implements
        Builder<MapLayersControllerGetStreetNamesMap200ResponseAllOfData,
            MapLayersControllerGetStreetNamesMap200ResponseAllOfDataBuilder> {
  _$MapLayersControllerGetStreetNamesMap200ResponseAllOfData? _$v;

  MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum? _type;
  MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum? get type =>
      _$this._type;
  set type(
          MapLayersControllerGetStreetNamesMap200ResponseAllOfDataTypeEnum?
              type) =>
      _$this._type = type;

  ListBuilder<
          MapLayersControllerGetStreetNamesMap200ResponseAllOfDataFeaturesInner>?
      _features;
  ListBuilder<
          MapLayersControllerGetStreetNamesMap200ResponseAllOfDataFeaturesInner>
      get features => _$this._features ??= new ListBuilder<
          MapLayersControllerGetStreetNamesMap200ResponseAllOfDataFeaturesInner>();
  set features(
          ListBuilder<
                  MapLayersControllerGetStreetNamesMap200ResponseAllOfDataFeaturesInner>?
              features) =>
      _$this._features = features;

  MapLayersControllerGetStreetNamesMap200ResponseAllOfDataBuilder() {
    MapLayersControllerGetStreetNamesMap200ResponseAllOfData._defaults(this);
  }

  MapLayersControllerGetStreetNamesMap200ResponseAllOfDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _features = $v.features.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MapLayersControllerGetStreetNamesMap200ResponseAllOfData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapLayersControllerGetStreetNamesMap200ResponseAllOfData;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetStreetNamesMap200ResponseAllOfDataBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetStreetNamesMap200ResponseAllOfData build() => _build();

  _$MapLayersControllerGetStreetNamesMap200ResponseAllOfData _build() {
    _$MapLayersControllerGetStreetNamesMap200ResponseAllOfData _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetStreetNamesMap200ResponseAllOfData._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetStreetNamesMap200ResponseAllOfData',
                  'type'),
              features: features.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'features';
        features.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetStreetNamesMap200ResponseAllOfData',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
