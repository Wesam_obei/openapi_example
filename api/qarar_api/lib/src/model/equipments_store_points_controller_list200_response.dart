//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/list_equipment_store_point_data_response.dart';
import 'package:qarar_api/src/model/paginated_links_documented.dart';
import 'package:qarar_api/src/model/paginated_meta_documented.dart';
import 'package:qarar_api/src/model/paginated_documented.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'equipments_store_points_controller_list200_response.g.dart';

/// EquipmentsStorePointsControllerList200Response
///
/// Properties:
/// * [success]
/// * [data]
/// * [meta]
/// * [links]
@BuiltValue()
abstract class EquipmentsStorePointsControllerList200Response
    implements
        PaginatedDocumented,
        Built<EquipmentsStorePointsControllerList200Response,
            EquipmentsStorePointsControllerList200ResponseBuilder> {
  EquipmentsStorePointsControllerList200Response._();

  factory EquipmentsStorePointsControllerList200Response(
          [void updates(
              EquipmentsStorePointsControllerList200ResponseBuilder b)]) =
      _$EquipmentsStorePointsControllerList200Response;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          EquipmentsStorePointsControllerList200ResponseBuilder b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<EquipmentsStorePointsControllerList200Response>
      get serializer =>
          _$EquipmentsStorePointsControllerList200ResponseSerializer();
}

class _$EquipmentsStorePointsControllerList200ResponseSerializer
    implements
        PrimitiveSerializer<EquipmentsStorePointsControllerList200Response> {
  @override
  final Iterable<Type> types = const [
    EquipmentsStorePointsControllerList200Response,
    _$EquipmentsStorePointsControllerList200Response
  ];

  @override
  final String wireName = r'EquipmentsStorePointsControllerList200Response';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    EquipmentsStorePointsControllerList200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'links';
    yield serializers.serialize(
      object.links,
      specifiedType: const FullType(PaginatedLinksDocumented),
    );
    yield r'data';
    yield serializers.serialize(
      object.data,
      specifiedType: const FullType(BuiltList, [FullType(JsonObject)]),
    );
    yield r'success';
    yield serializers.serialize(
      object.success,
      specifiedType: const FullType(bool),
    );
    yield r'meta';
    yield serializers.serialize(
      object.meta,
      specifiedType: const FullType(PaginatedMetaDocumented),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    EquipmentsStorePointsControllerList200Response object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required EquipmentsStorePointsControllerList200ResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'links':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(PaginatedLinksDocumented),
          ) as PaginatedLinksDocumented;
          result.links.replace(valueDes);
          break;
        case r'data':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(JsonObject)]),
          ) as BuiltList<JsonObject>;
          result.data.replace(valueDes);
          break;
        case r'success':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.success = valueDes;
          break;
        case r'meta':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(PaginatedMetaDocumented),
          ) as PaginatedMetaDocumented;
          result.meta.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  EquipmentsStorePointsControllerList200Response deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = EquipmentsStorePointsControllerList200ResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
