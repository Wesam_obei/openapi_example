// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_equipment_type_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetEquipmentTypeDataResponse extends GetEquipmentTypeDataResponse {
  @override
  final num id;
  @override
  final String name;
  @override
  final DateTime createdAt;
  @override
  final GetFileDataResponse? file;

  factory _$GetEquipmentTypeDataResponse(
          [void Function(GetEquipmentTypeDataResponseBuilder)? updates]) =>
      (new GetEquipmentTypeDataResponseBuilder()..update(updates))._build();

  _$GetEquipmentTypeDataResponse._(
      {required this.id,
      required this.name,
      required this.createdAt,
      this.file})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'GetEquipmentTypeDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, r'GetEquipmentTypeDataResponse', 'name');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'GetEquipmentTypeDataResponse', 'createdAt');
  }

  @override
  GetEquipmentTypeDataResponse rebuild(
          void Function(GetEquipmentTypeDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetEquipmentTypeDataResponseBuilder toBuilder() =>
      new GetEquipmentTypeDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetEquipmentTypeDataResponse &&
        id == other.id &&
        name == other.name &&
        createdAt == other.createdAt &&
        file == other.file;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, file.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetEquipmentTypeDataResponse')
          ..add('id', id)
          ..add('name', name)
          ..add('createdAt', createdAt)
          ..add('file', file))
        .toString();
  }
}

class GetEquipmentTypeDataResponseBuilder
    implements
        Builder<GetEquipmentTypeDataResponse,
            GetEquipmentTypeDataResponseBuilder> {
  _$GetEquipmentTypeDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  GetFileDataResponseBuilder? _file;
  GetFileDataResponseBuilder get file =>
      _$this._file ??= new GetFileDataResponseBuilder();
  set file(GetFileDataResponseBuilder? file) => _$this._file = file;

  GetEquipmentTypeDataResponseBuilder() {
    GetEquipmentTypeDataResponse._defaults(this);
  }

  GetEquipmentTypeDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _createdAt = $v.createdAt;
      _file = $v.file?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetEquipmentTypeDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetEquipmentTypeDataResponse;
  }

  @override
  void update(void Function(GetEquipmentTypeDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetEquipmentTypeDataResponse build() => _build();

  _$GetEquipmentTypeDataResponse _build() {
    _$GetEquipmentTypeDataResponse _$result;
    try {
      _$result = _$v ??
          new _$GetEquipmentTypeDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'GetEquipmentTypeDataResponse', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'GetEquipmentTypeDataResponse', 'name'),
              createdAt: BuiltValueNullFieldError.checkNotNull(
                  createdAt, r'GetEquipmentTypeDataResponse', 'createdAt'),
              file: _file?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'file';
        _file?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'GetEquipmentTypeDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
