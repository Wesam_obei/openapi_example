//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/map_layers_controller_get_transport_tracks_map200_response_all_of_data_features_inner.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_transport_tracks_map200_response_all_of_data.g.dart';

/// MapLayersControllerGetTransportTracksMap200ResponseAllOfData
///
/// Properties:
/// * [type]
/// * [features]
@BuiltValue()
abstract class MapLayersControllerGetTransportTracksMap200ResponseAllOfData
    implements
        Built<MapLayersControllerGetTransportTracksMap200ResponseAllOfData,
            MapLayersControllerGetTransportTracksMap200ResponseAllOfDataBuilder> {
  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum get type;
  // enum typeEnum {  FeatureCollection,  };

  @BuiltValueField(wireName: r'features')
  BuiltList<
          MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner>
      get features;

  MapLayersControllerGetTransportTracksMap200ResponseAllOfData._();

  factory MapLayersControllerGetTransportTracksMap200ResponseAllOfData(
          [void updates(
              MapLayersControllerGetTransportTracksMap200ResponseAllOfDataBuilder
                  b)]) =
      _$MapLayersControllerGetTransportTracksMap200ResponseAllOfData;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetTransportTracksMap200ResponseAllOfDataBuilder
              b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<
          MapLayersControllerGetTransportTracksMap200ResponseAllOfData>
      get serializer =>
          _$MapLayersControllerGetTransportTracksMap200ResponseAllOfDataSerializer();
}

class _$MapLayersControllerGetTransportTracksMap200ResponseAllOfDataSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetTransportTracksMap200ResponseAllOfData> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetTransportTracksMap200ResponseAllOfData,
    _$MapLayersControllerGetTransportTracksMap200ResponseAllOfData
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetTransportTracksMap200ResponseAllOfData';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetTransportTracksMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(
          MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum),
    );
    yield r'features';
    yield serializers.serialize(
      object.features,
      specifiedType: const FullType(BuiltList, [
        FullType(
            MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner)
      ]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetTransportTracksMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetTransportTracksMap200ResponseAllOfDataBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum),
          ) as MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum;
          result.type = valueDes;
          break;
        case r'features':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [
              FullType(
                  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner)
            ]),
          ) as BuiltList<
              MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner>;
          result.features.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetTransportTracksMap200ResponseAllOfData deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetTransportTracksMap200ResponseAllOfDataBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum
    extends EnumClass {
  @BuiltValueEnumConst(wireName: r'FeatureCollection', fallback: true)
  static const MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum
      featureCollection =
      _$mapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum_featureCollection;

  static Serializer<
          MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum>
      get serializer =>
          _$mapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnumSerializer;

  const MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum._(
      String name)
      : super(name);

  static BuiltSet<
          MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum>
      get values =>
          _$mapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnumValues;
  static MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum
      valueOf(String name) =>
          _$mapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnumValueOf(
              name);
}
