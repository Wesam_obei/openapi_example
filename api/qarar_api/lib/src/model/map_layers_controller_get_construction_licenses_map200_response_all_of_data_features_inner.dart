//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_construction_licenses_map200_response_all_of_data_features_inner_properties.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_construction_licenses_map200_response_all_of_data_features_inner.g.dart';

/// GeoJson Feature
///
/// Properties:
/// * [type]
/// * [id]
/// * [geometry]
/// * [properties]
@BuiltValue()
abstract class MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner
    implements
        Built<
            MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner,
            MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerBuilder> {
  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum
      get type;
  // enum typeEnum {  Feature,  };

  @BuiltValueField(wireName: r'id')
  int get id;

  @BuiltValueField(wireName: r'geometry')
  JsonObject? get geometry;

  @BuiltValueField(wireName: r'properties')
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties
      get properties;

  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner._();

  factory MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner(
          [void updates(
              MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerBuilder
                  b)]) =
      _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerBuilder
              b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner>
      get serializer =>
          _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerSerializer();
}

class _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner,
    _$MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner
        object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum),
    );
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(int),
    );
    yield r'geometry';
    yield object.geometry == null
        ? null
        : serializers.serialize(
            object.geometry,
            specifiedType: const FullType.nullable(JsonObject),
          );
    yield r'properties';
    yield serializers.serialize(
      object.properties,
      specifiedType: const FullType(
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner
        object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum),
          ) as MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum;
          result.type = valueDes;
          break;
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(int),
          ) as int;
          result.id = valueDes;
          break;
        case r'geometry':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(JsonObject),
          ) as JsonObject?;
          if (valueDes == null) continue;
          result.geometry = valueDes;
          break;
        case r'properties':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties),
          ) as MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties;
          result.properties.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner
      deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum
    extends EnumClass {
  @BuiltValueEnumConst(wireName: r'Feature', fallback: true)
  static const MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum
      feature =
      _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature;

  static Serializer<
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum>
      get serializer =>
          _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer;

  const MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum._(
      String name)
      : super(name);

  static BuiltSet<
          MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum>
      get values =>
          _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnumValues;
  static MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnum
      valueOf(String name) =>
          _$mapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerTypeEnumValueOf(
              name);
}
