//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_incident_reports_map200_response_all_of_data_features_inner.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_incident_reports_map200_response_all_of_data.g.dart';

/// MapLayersControllerGetIncidentReportsMap200ResponseAllOfData
///
/// Properties:
/// * [type]
/// * [features]
@BuiltValue()
abstract class MapLayersControllerGetIncidentReportsMap200ResponseAllOfData
    implements
        Built<MapLayersControllerGetIncidentReportsMap200ResponseAllOfData,
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataBuilder> {
  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum get type;
  // enum typeEnum {  FeatureCollection,  };

  @BuiltValueField(wireName: r'features')
  BuiltList<
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInner>
      get features;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfData._();

  factory MapLayersControllerGetIncidentReportsMap200ResponseAllOfData(
          [void updates(
              MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataBuilder
                  b)]) =
      _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfData;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataBuilder
              b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfData>
      get serializer =>
          _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataSerializer();
}

class _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfData> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetIncidentReportsMap200ResponseAllOfData,
    _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfData
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetIncidentReportsMap200ResponseAllOfData';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetIncidentReportsMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum),
    );
    yield r'features';
    yield serializers.serialize(
      object.features,
      specifiedType: const FullType(BuiltList, [
        FullType(
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInner)
      ]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetIncidentReportsMap200ResponseAllOfData object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum),
          ) as MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum;
          result.type = valueDes;
          break;
        case r'features':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [
              FullType(
                  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInner)
            ]),
          ) as BuiltList<
              MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInner>;
          result.features.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfData deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum
    extends EnumClass {
  @BuiltValueEnumConst(wireName: r'FeatureCollection', fallback: true)
  static const MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum
      featureCollection =
      _$mapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum_featureCollection;

  static Serializer<
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum>
      get serializer =>
          _$mapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnumSerializer;

  const MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum._(
      String name)
      : super(name);

  static BuiltSet<
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum>
      get values =>
          _$mapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnumValues;
  static MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnum
      valueOf(String name) =>
          _$mapLayersControllerGetIncidentReportsMap200ResponseAllOfDataTypeEnumValueOf(
              name);
}
