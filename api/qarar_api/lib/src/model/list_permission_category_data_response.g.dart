// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_permission_category_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ListPermissionCategoryDataResponse
    extends ListPermissionCategoryDataResponse {
  @override
  final num id;
  @override
  final String name;
  @override
  final String label;
  @override
  final String description;
  @override
  final num order;
  @override
  final DateTime createdAt;
  @override
  final BuiltList<PermissionDataResponse> permissions;

  factory _$ListPermissionCategoryDataResponse(
          [void Function(ListPermissionCategoryDataResponseBuilder)?
              updates]) =>
      (new ListPermissionCategoryDataResponseBuilder()..update(updates))
          ._build();

  _$ListPermissionCategoryDataResponse._(
      {required this.id,
      required this.name,
      required this.label,
      required this.description,
      required this.order,
      required this.createdAt,
      required this.permissions})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'ListPermissionCategoryDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, r'ListPermissionCategoryDataResponse', 'name');
    BuiltValueNullFieldError.checkNotNull(
        label, r'ListPermissionCategoryDataResponse', 'label');
    BuiltValueNullFieldError.checkNotNull(
        description, r'ListPermissionCategoryDataResponse', 'description');
    BuiltValueNullFieldError.checkNotNull(
        order, r'ListPermissionCategoryDataResponse', 'order');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'ListPermissionCategoryDataResponse', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        permissions, r'ListPermissionCategoryDataResponse', 'permissions');
  }

  @override
  ListPermissionCategoryDataResponse rebuild(
          void Function(ListPermissionCategoryDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ListPermissionCategoryDataResponseBuilder toBuilder() =>
      new ListPermissionCategoryDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ListPermissionCategoryDataResponse &&
        id == other.id &&
        name == other.name &&
        label == other.label &&
        description == other.description &&
        order == other.order &&
        createdAt == other.createdAt &&
        permissions == other.permissions;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, label.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, order.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, permissions.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ListPermissionCategoryDataResponse')
          ..add('id', id)
          ..add('name', name)
          ..add('label', label)
          ..add('description', description)
          ..add('order', order)
          ..add('createdAt', createdAt)
          ..add('permissions', permissions))
        .toString();
  }
}

class ListPermissionCategoryDataResponseBuilder
    implements
        Builder<ListPermissionCategoryDataResponse,
            ListPermissionCategoryDataResponseBuilder> {
  _$ListPermissionCategoryDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _label;
  String? get label => _$this._label;
  set label(String? label) => _$this._label = label;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  num? _order;
  num? get order => _$this._order;
  set order(num? order) => _$this._order = order;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  ListBuilder<PermissionDataResponse>? _permissions;
  ListBuilder<PermissionDataResponse> get permissions =>
      _$this._permissions ??= new ListBuilder<PermissionDataResponse>();
  set permissions(ListBuilder<PermissionDataResponse>? permissions) =>
      _$this._permissions = permissions;

  ListPermissionCategoryDataResponseBuilder() {
    ListPermissionCategoryDataResponse._defaults(this);
  }

  ListPermissionCategoryDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _label = $v.label;
      _description = $v.description;
      _order = $v.order;
      _createdAt = $v.createdAt;
      _permissions = $v.permissions.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ListPermissionCategoryDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ListPermissionCategoryDataResponse;
  }

  @override
  void update(
      void Function(ListPermissionCategoryDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ListPermissionCategoryDataResponse build() => _build();

  _$ListPermissionCategoryDataResponse _build() {
    _$ListPermissionCategoryDataResponse _$result;
    try {
      _$result = _$v ??
          new _$ListPermissionCategoryDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'ListPermissionCategoryDataResponse', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'ListPermissionCategoryDataResponse', 'name'),
              label: BuiltValueNullFieldError.checkNotNull(
                  label, r'ListPermissionCategoryDataResponse', 'label'),
              description: BuiltValueNullFieldError.checkNotNull(description,
                  r'ListPermissionCategoryDataResponse', 'description'),
              order: BuiltValueNullFieldError.checkNotNull(
                  order, r'ListPermissionCategoryDataResponse', 'order'),
              createdAt: BuiltValueNullFieldError.checkNotNull(createdAt,
                  r'ListPermissionCategoryDataResponse', 'createdAt'),
              permissions: permissions.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'permissions';
        permissions.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ListPermissionCategoryDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
