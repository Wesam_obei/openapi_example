//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_district_boundaries_map200_response_all_of_data_features_inner_properties.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_layers_controller_get_district_boundaries_map200_response_all_of_data_features_inner.g.dart';

/// GeoJson Feature
///
/// Properties:
/// * [type]
/// * [id]
/// * [geometry]
/// * [properties]
@BuiltValue()
abstract class MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner
    implements
        Built<
            MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner,
            MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerBuilder> {
  @BuiltValueField(wireName: r'type')
  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum
      get type;
  // enum typeEnum {  Feature,  };

  @BuiltValueField(wireName: r'id')
  int get id;

  @BuiltValueField(wireName: r'geometry')
  JsonObject? get geometry;

  @BuiltValueField(wireName: r'properties')
  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
      get properties;

  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner._();

  factory MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner(
          [void updates(
              MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerBuilder
                  b)]) =
      _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerBuilder
              b) =>
      b;

  @BuiltValueSerializer(custom: true)
  static Serializer<
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner>
      get serializer =>
          _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerSerializer();
}

class _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner> {
  @override
  final Iterable<Type> types = const [
    MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner,
    _$MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner
  ];

  @override
  final String wireName =
      r'MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner
        object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum),
    );
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(int),
    );
    yield r'geometry';
    yield object.geometry == null
        ? null
        : serializers.serialize(
            object.geometry,
            specifiedType: const FullType.nullable(JsonObject),
          );
    yield r'properties';
    yield serializers.serialize(
      object.properties,
      specifiedType: const FullType(
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner
        object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerBuilder
        result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum),
          ) as MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum;
          result.type = valueDes;
          break;
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(int),
          ) as int;
          result.id = valueDes;
          break;
        case r'geometry':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType.nullable(JsonObject),
          ) as JsonObject?;
          if (valueDes == null) continue;
          result.geometry = valueDes;
          break;
        case r'properties':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(
                MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties),
          ) as MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties;
          result.properties.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner
      deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result =
        MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum
    extends EnumClass {
  @BuiltValueEnumConst(wireName: r'Feature', fallback: true)
  static const MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum
      feature =
      _$mapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum_feature;

  static Serializer<
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum>
      get serializer =>
          _$mapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnumSerializer;

  const MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum._(
      String name)
      : super(name);

  static BuiltSet<
          MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum>
      get values =>
          _$mapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnumValues;
  static MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnum
      valueOf(String name) =>
          _$mapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerTypeEnumValueOf(
              name);
}
