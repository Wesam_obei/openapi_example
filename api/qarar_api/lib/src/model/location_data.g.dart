// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'location_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$LocationData extends LocationData {
  @override
  final num latitude;
  @override
  final num longitude;

  factory _$LocationData([void Function(LocationDataBuilder)? updates]) =>
      (new LocationDataBuilder()..update(updates))._build();

  _$LocationData._({required this.latitude, required this.longitude})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        latitude, r'LocationData', 'latitude');
    BuiltValueNullFieldError.checkNotNull(
        longitude, r'LocationData', 'longitude');
  }

  @override
  LocationData rebuild(void Function(LocationDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LocationDataBuilder toBuilder() => new LocationDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LocationData &&
        latitude == other.latitude &&
        longitude == other.longitude;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, latitude.hashCode);
    _$hash = $jc(_$hash, longitude.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'LocationData')
          ..add('latitude', latitude)
          ..add('longitude', longitude))
        .toString();
  }
}

class LocationDataBuilder
    implements Builder<LocationData, LocationDataBuilder> {
  _$LocationData? _$v;

  num? _latitude;
  num? get latitude => _$this._latitude;
  set latitude(num? latitude) => _$this._latitude = latitude;

  num? _longitude;
  num? get longitude => _$this._longitude;
  set longitude(num? longitude) => _$this._longitude = longitude;

  LocationDataBuilder() {
    LocationData._defaults(this);
  }

  LocationDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _latitude = $v.latitude;
      _longitude = $v.longitude;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LocationData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$LocationData;
  }

  @override
  void update(void Function(LocationDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  LocationData build() => _build();

  _$LocationData _build() {
    final _$result = _$v ??
        new _$LocationData._(
            latitude: BuiltValueNullFieldError.checkNotNull(
                latitude, r'LocationData', 'latitude'),
            longitude: BuiltValueNullFieldError.checkNotNull(
                longitude, r'LocationData', 'longitude'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
