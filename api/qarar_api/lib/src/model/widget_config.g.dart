// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'widget_config.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$WidgetConfig extends WidgetConfig {
  @override
  final String id;
  @override
  final String view;
  @override
  final JsonObject style;
  @override
  final JsonObject config;

  factory _$WidgetConfig([void Function(WidgetConfigBuilder)? updates]) =>
      (new WidgetConfigBuilder()..update(updates))._build();

  _$WidgetConfig._(
      {required this.id,
      required this.view,
      required this.style,
      required this.config})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, r'WidgetConfig', 'id');
    BuiltValueNullFieldError.checkNotNull(view, r'WidgetConfig', 'view');
    BuiltValueNullFieldError.checkNotNull(style, r'WidgetConfig', 'style');
    BuiltValueNullFieldError.checkNotNull(config, r'WidgetConfig', 'config');
  }

  @override
  WidgetConfig rebuild(void Function(WidgetConfigBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  WidgetConfigBuilder toBuilder() => new WidgetConfigBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is WidgetConfig &&
        id == other.id &&
        view == other.view &&
        style == other.style &&
        config == other.config;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, view.hashCode);
    _$hash = $jc(_$hash, style.hashCode);
    _$hash = $jc(_$hash, config.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'WidgetConfig')
          ..add('id', id)
          ..add('view', view)
          ..add('style', style)
          ..add('config', config))
        .toString();
  }
}

class WidgetConfigBuilder
    implements Builder<WidgetConfig, WidgetConfigBuilder> {
  _$WidgetConfig? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _view;
  String? get view => _$this._view;
  set view(String? view) => _$this._view = view;

  JsonObject? _style;
  JsonObject? get style => _$this._style;
  set style(JsonObject? style) => _$this._style = style;

  JsonObject? _config;
  JsonObject? get config => _$this._config;
  set config(JsonObject? config) => _$this._config = config;

  WidgetConfigBuilder() {
    WidgetConfig._defaults(this);
  }

  WidgetConfigBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _view = $v.view;
      _style = $v.style;
      _config = $v.config;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(WidgetConfig other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$WidgetConfig;
  }

  @override
  void update(void Function(WidgetConfigBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  WidgetConfig build() => _build();

  _$WidgetConfig _build() {
    final _$result = _$v ??
        new _$WidgetConfig._(
            id: BuiltValueNullFieldError.checkNotNull(
                id, r'WidgetConfig', 'id'),
            view: BuiltValueNullFieldError.checkNotNull(
                view, r'WidgetConfig', 'view'),
            style: BuiltValueNullFieldError.checkNotNull(
                style, r'WidgetConfig', 'style'),
            config: BuiltValueNullFieldError.checkNotNull(
                config, r'WidgetConfig', 'config'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
