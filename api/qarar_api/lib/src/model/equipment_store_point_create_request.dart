//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/location_data.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'equipment_store_point_create_request.g.dart';

/// EquipmentStorePointCreateRequest
///
/// Properties:
/// * [name]
/// * [location]
/// * [entityId]
/// * [description]
/// * [streetName]
@BuiltValue()
abstract class EquipmentStorePointCreateRequest
    implements
        Built<EquipmentStorePointCreateRequest,
            EquipmentStorePointCreateRequestBuilder> {
  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'location')
  LocationData get location;

  @BuiltValueField(wireName: r'entity_id')
  num get entityId;

  @BuiltValueField(wireName: r'description')
  String? get description;

  @BuiltValueField(wireName: r'street_name')
  String? get streetName;

  EquipmentStorePointCreateRequest._();

  factory EquipmentStorePointCreateRequest(
          [void updates(EquipmentStorePointCreateRequestBuilder b)]) =
      _$EquipmentStorePointCreateRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(EquipmentStorePointCreateRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<EquipmentStorePointCreateRequest> get serializer =>
      _$EquipmentStorePointCreateRequestSerializer();
}

class _$EquipmentStorePointCreateRequestSerializer
    implements PrimitiveSerializer<EquipmentStorePointCreateRequest> {
  @override
  final Iterable<Type> types = const [
    EquipmentStorePointCreateRequest,
    _$EquipmentStorePointCreateRequest
  ];

  @override
  final String wireName = r'EquipmentStorePointCreateRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    EquipmentStorePointCreateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    yield r'location';
    yield serializers.serialize(
      object.location,
      specifiedType: const FullType(LocationData),
    );
    yield r'entity_id';
    yield serializers.serialize(
      object.entityId,
      specifiedType: const FullType(num),
    );
    if (object.description != null) {
      yield r'description';
      yield serializers.serialize(
        object.description,
        specifiedType: const FullType(String),
      );
    }
    if (object.streetName != null) {
      yield r'street_name';
      yield serializers.serialize(
        object.streetName,
        specifiedType: const FullType(String),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    EquipmentStorePointCreateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required EquipmentStorePointCreateRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'location':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(LocationData),
          ) as LocationData;
          result.location.replace(valueDes);
          break;
        case r'entity_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.entityId = valueDes;
          break;
        case r'description':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.description = valueDes;
          break;
        case r'street_name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.streetName = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  EquipmentStorePointCreateRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = EquipmentStorePointCreateRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
