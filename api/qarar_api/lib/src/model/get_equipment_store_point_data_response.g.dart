// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_equipment_store_point_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetEquipmentStorePointDataResponse
    extends GetEquipmentStorePointDataResponse {
  @override
  final num id;
  @override
  final String name;
  @override
  final String description;
  @override
  final LocationData location;
  @override
  final DateTime createdAt;
  @override
  final GetEntityDataResponse entity;

  factory _$GetEquipmentStorePointDataResponse(
          [void Function(GetEquipmentStorePointDataResponseBuilder)?
              updates]) =>
      (new GetEquipmentStorePointDataResponseBuilder()..update(updates))
          ._build();

  _$GetEquipmentStorePointDataResponse._(
      {required this.id,
      required this.name,
      required this.description,
      required this.location,
      required this.createdAt,
      required this.entity})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'GetEquipmentStorePointDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, r'GetEquipmentStorePointDataResponse', 'name');
    BuiltValueNullFieldError.checkNotNull(
        description, r'GetEquipmentStorePointDataResponse', 'description');
    BuiltValueNullFieldError.checkNotNull(
        location, r'GetEquipmentStorePointDataResponse', 'location');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, r'GetEquipmentStorePointDataResponse', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        entity, r'GetEquipmentStorePointDataResponse', 'entity');
  }

  @override
  GetEquipmentStorePointDataResponse rebuild(
          void Function(GetEquipmentStorePointDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetEquipmentStorePointDataResponseBuilder toBuilder() =>
      new GetEquipmentStorePointDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetEquipmentStorePointDataResponse &&
        id == other.id &&
        name == other.name &&
        description == other.description &&
        location == other.location &&
        createdAt == other.createdAt &&
        entity == other.entity;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, location.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, entity.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetEquipmentStorePointDataResponse')
          ..add('id', id)
          ..add('name', name)
          ..add('description', description)
          ..add('location', location)
          ..add('createdAt', createdAt)
          ..add('entity', entity))
        .toString();
  }
}

class GetEquipmentStorePointDataResponseBuilder
    implements
        Builder<GetEquipmentStorePointDataResponse,
            GetEquipmentStorePointDataResponseBuilder> {
  _$GetEquipmentStorePointDataResponse? _$v;

  num? _id;
  num? get id => _$this._id;
  set id(num? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  LocationDataBuilder? _location;
  LocationDataBuilder get location =>
      _$this._location ??= new LocationDataBuilder();
  set location(LocationDataBuilder? location) => _$this._location = location;

  DateTime? _createdAt;
  DateTime? get createdAt => _$this._createdAt;
  set createdAt(DateTime? createdAt) => _$this._createdAt = createdAt;

  GetEntityDataResponseBuilder? _entity;
  GetEntityDataResponseBuilder get entity =>
      _$this._entity ??= new GetEntityDataResponseBuilder();
  set entity(GetEntityDataResponseBuilder? entity) => _$this._entity = entity;

  GetEquipmentStorePointDataResponseBuilder() {
    GetEquipmentStorePointDataResponse._defaults(this);
  }

  GetEquipmentStorePointDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _description = $v.description;
      _location = $v.location.toBuilder();
      _createdAt = $v.createdAt;
      _entity = $v.entity.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetEquipmentStorePointDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetEquipmentStorePointDataResponse;
  }

  @override
  void update(
      void Function(GetEquipmentStorePointDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetEquipmentStorePointDataResponse build() => _build();

  _$GetEquipmentStorePointDataResponse _build() {
    _$GetEquipmentStorePointDataResponse _$result;
    try {
      _$result = _$v ??
          new _$GetEquipmentStorePointDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'GetEquipmentStorePointDataResponse', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'GetEquipmentStorePointDataResponse', 'name'),
              description: BuiltValueNullFieldError.checkNotNull(description,
                  r'GetEquipmentStorePointDataResponse', 'description'),
              location: location.build(),
              createdAt: BuiltValueNullFieldError.checkNotNull(createdAt,
                  r'GetEquipmentStorePointDataResponse', 'createdAt'),
              entity: entity.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'location';
        location.build();

        _$failedField = 'entity';
        entity.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'GetEquipmentStorePointDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
