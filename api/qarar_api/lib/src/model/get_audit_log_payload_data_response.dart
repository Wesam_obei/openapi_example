//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:qarar_api/src/model/get_audit_log_request_data_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'get_audit_log_payload_data_response.g.dart';

/// GetAuditLogPayloadDataResponse
///
/// Properties:
/// * [request]
@BuiltValue()
abstract class GetAuditLogPayloadDataResponse
    implements
        Built<GetAuditLogPayloadDataResponse,
            GetAuditLogPayloadDataResponseBuilder> {
  @BuiltValueField(wireName: r'request')
  GetAuditLogRequestDataResponse get request;

  GetAuditLogPayloadDataResponse._();

  factory GetAuditLogPayloadDataResponse(
          [void updates(GetAuditLogPayloadDataResponseBuilder b)]) =
      _$GetAuditLogPayloadDataResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(GetAuditLogPayloadDataResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<GetAuditLogPayloadDataResponse> get serializer =>
      _$GetAuditLogPayloadDataResponseSerializer();
}

class _$GetAuditLogPayloadDataResponseSerializer
    implements PrimitiveSerializer<GetAuditLogPayloadDataResponse> {
  @override
  final Iterable<Type> types = const [
    GetAuditLogPayloadDataResponse,
    _$GetAuditLogPayloadDataResponse
  ];

  @override
  final String wireName = r'GetAuditLogPayloadDataResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    GetAuditLogPayloadDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'request';
    yield serializers.serialize(
      object.request,
      specifiedType: const FullType(GetAuditLogRequestDataResponse),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    GetAuditLogPayloadDataResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required GetAuditLogPayloadDataResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'request':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(GetAuditLogRequestDataResponse),
          ) as GetAuditLogRequestDataResponse;
          result.request.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  GetAuditLogPayloadDataResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = GetAuditLogPayloadDataResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
