// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'form_update_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$FormUpdateRequest extends FormUpdateRequest {
  @override
  final String? name;
  @override
  final String? description;
  @override
  final bool? isActive;
  @override
  final String? fileToken;
  @override
  final BuiltList<FormComponentsRequest>? formComponents;
  @override
  final bool? deleteFile;
  @override
  final BuiltList<num>? deletedComponents;

  factory _$FormUpdateRequest(
          [void Function(FormUpdateRequestBuilder)? updates]) =>
      (new FormUpdateRequestBuilder()..update(updates))._build();

  _$FormUpdateRequest._(
      {this.name,
      this.description,
      this.isActive,
      this.fileToken,
      this.formComponents,
      this.deleteFile,
      this.deletedComponents})
      : super._();

  @override
  FormUpdateRequest rebuild(void Function(FormUpdateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  FormUpdateRequestBuilder toBuilder() =>
      new FormUpdateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is FormUpdateRequest &&
        name == other.name &&
        description == other.description &&
        isActive == other.isActive &&
        fileToken == other.fileToken &&
        formComponents == other.formComponents &&
        deleteFile == other.deleteFile &&
        deletedComponents == other.deletedComponents;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, isActive.hashCode);
    _$hash = $jc(_$hash, fileToken.hashCode);
    _$hash = $jc(_$hash, formComponents.hashCode);
    _$hash = $jc(_$hash, deleteFile.hashCode);
    _$hash = $jc(_$hash, deletedComponents.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'FormUpdateRequest')
          ..add('name', name)
          ..add('description', description)
          ..add('isActive', isActive)
          ..add('fileToken', fileToken)
          ..add('formComponents', formComponents)
          ..add('deleteFile', deleteFile)
          ..add('deletedComponents', deletedComponents))
        .toString();
  }
}

class FormUpdateRequestBuilder
    implements Builder<FormUpdateRequest, FormUpdateRequestBuilder> {
  _$FormUpdateRequest? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  bool? _isActive;
  bool? get isActive => _$this._isActive;
  set isActive(bool? isActive) => _$this._isActive = isActive;

  String? _fileToken;
  String? get fileToken => _$this._fileToken;
  set fileToken(String? fileToken) => _$this._fileToken = fileToken;

  ListBuilder<FormComponentsRequest>? _formComponents;
  ListBuilder<FormComponentsRequest> get formComponents =>
      _$this._formComponents ??= new ListBuilder<FormComponentsRequest>();
  set formComponents(ListBuilder<FormComponentsRequest>? formComponents) =>
      _$this._formComponents = formComponents;

  bool? _deleteFile;
  bool? get deleteFile => _$this._deleteFile;
  set deleteFile(bool? deleteFile) => _$this._deleteFile = deleteFile;

  ListBuilder<num>? _deletedComponents;
  ListBuilder<num> get deletedComponents =>
      _$this._deletedComponents ??= new ListBuilder<num>();
  set deletedComponents(ListBuilder<num>? deletedComponents) =>
      _$this._deletedComponents = deletedComponents;

  FormUpdateRequestBuilder() {
    FormUpdateRequest._defaults(this);
  }

  FormUpdateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _description = $v.description;
      _isActive = $v.isActive;
      _fileToken = $v.fileToken;
      _formComponents = $v.formComponents?.toBuilder();
      _deleteFile = $v.deleteFile;
      _deletedComponents = $v.deletedComponents?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(FormUpdateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$FormUpdateRequest;
  }

  @override
  void update(void Function(FormUpdateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  FormUpdateRequest build() => _build();

  _$FormUpdateRequest _build() {
    _$FormUpdateRequest _$result;
    try {
      _$result = _$v ??
          new _$FormUpdateRequest._(
              name: name,
              description: description,
              isActive: isActive,
              fileToken: fileToken,
              formComponents: _formComponents?.build(),
              deleteFile: deleteFile,
              deletedComponents: _deletedComponents?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'formComponents';
        _formComponents?.build();

        _$failedField = 'deletedComponents';
        _deletedComponents?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'FormUpdateRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
