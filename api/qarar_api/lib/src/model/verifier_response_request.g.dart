// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'verifier_response_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const VerifierResponseRequestStatusEnum
    _$verifierResponseRequestStatusEnum_approved =
    const VerifierResponseRequestStatusEnum._('approved');
const VerifierResponseRequestStatusEnum
    _$verifierResponseRequestStatusEnum_needChange =
    const VerifierResponseRequestStatusEnum._('needChange');

VerifierResponseRequestStatusEnum _$verifierResponseRequestStatusEnumValueOf(
    String name) {
  switch (name) {
    case 'approved':
      return _$verifierResponseRequestStatusEnum_approved;
    case 'needChange':
      return _$verifierResponseRequestStatusEnum_needChange;
    default:
      return _$verifierResponseRequestStatusEnum_needChange;
  }
}

final BuiltSet<VerifierResponseRequestStatusEnum>
    _$verifierResponseRequestStatusEnumValues = new BuiltSet<
        VerifierResponseRequestStatusEnum>(const <VerifierResponseRequestStatusEnum>[
  _$verifierResponseRequestStatusEnum_approved,
  _$verifierResponseRequestStatusEnum_needChange,
]);

Serializer<VerifierResponseRequestStatusEnum>
    _$verifierResponseRequestStatusEnumSerializer =
    new _$VerifierResponseRequestStatusEnumSerializer();

class _$VerifierResponseRequestStatusEnumSerializer
    implements PrimitiveSerializer<VerifierResponseRequestStatusEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'approved': 'approved',
    'needChange': 'need-change',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'approved': 'approved',
    'need-change': 'needChange',
  };

  @override
  final Iterable<Type> types = const <Type>[VerifierResponseRequestStatusEnum];
  @override
  final String wireName = 'VerifierResponseRequestStatusEnum';

  @override
  Object serialize(
          Serializers serializers, VerifierResponseRequestStatusEnum object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  VerifierResponseRequestStatusEnum deserialize(
          Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      VerifierResponseRequestStatusEnum.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

class _$VerifierResponseRequest extends VerifierResponseRequest {
  @override
  final num recipientId;
  @override
  final VerifierResponseRequestStatusEnum status;

  factory _$VerifierResponseRequest(
          [void Function(VerifierResponseRequestBuilder)? updates]) =>
      (new VerifierResponseRequestBuilder()..update(updates))._build();

  _$VerifierResponseRequest._({required this.recipientId, required this.status})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        recipientId, r'VerifierResponseRequest', 'recipientId');
    BuiltValueNullFieldError.checkNotNull(
        status, r'VerifierResponseRequest', 'status');
  }

  @override
  VerifierResponseRequest rebuild(
          void Function(VerifierResponseRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  VerifierResponseRequestBuilder toBuilder() =>
      new VerifierResponseRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is VerifierResponseRequest &&
        recipientId == other.recipientId &&
        status == other.status;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, recipientId.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'VerifierResponseRequest')
          ..add('recipientId', recipientId)
          ..add('status', status))
        .toString();
  }
}

class VerifierResponseRequestBuilder
    implements
        Builder<VerifierResponseRequest, VerifierResponseRequestBuilder> {
  _$VerifierResponseRequest? _$v;

  num? _recipientId;
  num? get recipientId => _$this._recipientId;
  set recipientId(num? recipientId) => _$this._recipientId = recipientId;

  VerifierResponseRequestStatusEnum? _status;
  VerifierResponseRequestStatusEnum? get status => _$this._status;
  set status(VerifierResponseRequestStatusEnum? status) =>
      _$this._status = status;

  VerifierResponseRequestBuilder() {
    VerifierResponseRequest._defaults(this);
  }

  VerifierResponseRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _recipientId = $v.recipientId;
      _status = $v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(VerifierResponseRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$VerifierResponseRequest;
  }

  @override
  void update(void Function(VerifierResponseRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  VerifierResponseRequest build() => _build();

  _$VerifierResponseRequest _build() {
    final _$result = _$v ??
        new _$VerifierResponseRequest._(
            recipientId: BuiltValueNullFieldError.checkNotNull(
                recipientId, r'VerifierResponseRequest', 'recipientId'),
            status: BuiltValueNullFieldError.checkNotNull(
                status, r'VerifierResponseRequest', 'status'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
