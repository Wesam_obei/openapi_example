// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'error_details.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ErrorDetails extends ErrorDetails {
  @override
  final String code;
  @override
  final String message;
  @override
  final JsonObject data;

  factory _$ErrorDetails([void Function(ErrorDetailsBuilder)? updates]) =>
      (new ErrorDetailsBuilder()..update(updates))._build();

  _$ErrorDetails._(
      {required this.code, required this.message, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(code, r'ErrorDetails', 'code');
    BuiltValueNullFieldError.checkNotNull(message, r'ErrorDetails', 'message');
    BuiltValueNullFieldError.checkNotNull(data, r'ErrorDetails', 'data');
  }

  @override
  ErrorDetails rebuild(void Function(ErrorDetailsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ErrorDetailsBuilder toBuilder() => new ErrorDetailsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ErrorDetails &&
        code == other.code &&
        message == other.message &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, code.hashCode);
    _$hash = $jc(_$hash, message.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ErrorDetails')
          ..add('code', code)
          ..add('message', message)
          ..add('data', data))
        .toString();
  }
}

class ErrorDetailsBuilder
    implements Builder<ErrorDetails, ErrorDetailsBuilder> {
  _$ErrorDetails? _$v;

  String? _code;
  String? get code => _$this._code;
  set code(String? code) => _$this._code = code;

  String? _message;
  String? get message => _$this._message;
  set message(String? message) => _$this._message = message;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(JsonObject? data) => _$this._data = data;

  ErrorDetailsBuilder() {
    ErrorDetails._defaults(this);
  }

  ErrorDetailsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _code = $v.code;
      _message = $v.message;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ErrorDetails other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ErrorDetails;
  }

  @override
  void update(void Function(ErrorDetailsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ErrorDetails build() => _build();

  _$ErrorDetails _build() {
    final _$result = _$v ??
        new _$ErrorDetails._(
            code: BuiltValueNullFieldError.checkNotNull(
                code, r'ErrorDetails', 'code'),
            message: BuiltValueNullFieldError.checkNotNull(
                message, r'ErrorDetails', 'message'),
            data: BuiltValueNullFieldError.checkNotNull(
                data, r'ErrorDetails', 'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
