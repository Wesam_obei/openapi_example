// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_sea_out_map200_response_all_of_data_features_inner_properties.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerProperties
    extends MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerProperties {
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      id;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      districtName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      municipalityName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      cityName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      streetName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      seaotNo;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      seaotId;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      seaotHeight;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      seaotRetainingWallHeight;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      seaotWidth;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      seaotLength;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      seaotInvertLevel;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      seaotGroundElevation;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      seaotIncomingLineId;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      seaotType;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      seaotIncomingLineInvert;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      seaotNoInspectionAccess;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      upstreamStructureId;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      downstreamStructureId;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      groundElevation;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      description;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      designDate;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      designCompany;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      constructionDate;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      constructionCompany;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      inserviceDate;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      lifeCycleStatus;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      ancillaryRole;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      enabled;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      remarks;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      lastEditedUser;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      lastEditedDate;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      createdUser;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      createdDate;

  factory _$MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerProperties(
          [void Function(
                  MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
              updates]) =>
      (new MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerProperties._(
      {this.id,
      this.districtName,
      this.municipalityName,
      this.cityName,
      this.streetName,
      this.seaotNo,
      this.seaotId,
      this.seaotHeight,
      this.seaotRetainingWallHeight,
      this.seaotWidth,
      this.seaotLength,
      this.seaotInvertLevel,
      this.seaotGroundElevation,
      this.seaotIncomingLineId,
      this.seaotType,
      this.seaotIncomingLineInvert,
      this.seaotNoInspectionAccess,
      this.upstreamStructureId,
      this.downstreamStructureId,
      this.groundElevation,
      this.description,
      this.designDate,
      this.designCompany,
      this.constructionDate,
      this.constructionCompany,
      this.inserviceDate,
      this.lifeCycleStatus,
      this.ancillaryRole,
      this.enabled,
      this.remarks,
      this.lastEditedUser,
      this.lastEditedDate,
      this.createdUser,
      this.createdDate})
      : super._();

  @override
  MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerProperties
      rebuild(
              void Function(
                      MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)
                  updates) =>
          (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      toBuilder() =>
          new MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerProperties &&
        id == other.id &&
        districtName == other.districtName &&
        municipalityName == other.municipalityName &&
        cityName == other.cityName &&
        streetName == other.streetName &&
        seaotNo == other.seaotNo &&
        seaotId == other.seaotId &&
        seaotHeight == other.seaotHeight &&
        seaotRetainingWallHeight == other.seaotRetainingWallHeight &&
        seaotWidth == other.seaotWidth &&
        seaotLength == other.seaotLength &&
        seaotInvertLevel == other.seaotInvertLevel &&
        seaotGroundElevation == other.seaotGroundElevation &&
        seaotIncomingLineId == other.seaotIncomingLineId &&
        seaotType == other.seaotType &&
        seaotIncomingLineInvert == other.seaotIncomingLineInvert &&
        seaotNoInspectionAccess == other.seaotNoInspectionAccess &&
        upstreamStructureId == other.upstreamStructureId &&
        downstreamStructureId == other.downstreamStructureId &&
        groundElevation == other.groundElevation &&
        description == other.description &&
        designDate == other.designDate &&
        designCompany == other.designCompany &&
        constructionDate == other.constructionDate &&
        constructionCompany == other.constructionCompany &&
        inserviceDate == other.inserviceDate &&
        lifeCycleStatus == other.lifeCycleStatus &&
        ancillaryRole == other.ancillaryRole &&
        enabled == other.enabled &&
        remarks == other.remarks &&
        lastEditedUser == other.lastEditedUser &&
        lastEditedDate == other.lastEditedDate &&
        createdUser == other.createdUser &&
        createdDate == other.createdDate;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, districtName.hashCode);
    _$hash = $jc(_$hash, municipalityName.hashCode);
    _$hash = $jc(_$hash, cityName.hashCode);
    _$hash = $jc(_$hash, streetName.hashCode);
    _$hash = $jc(_$hash, seaotNo.hashCode);
    _$hash = $jc(_$hash, seaotId.hashCode);
    _$hash = $jc(_$hash, seaotHeight.hashCode);
    _$hash = $jc(_$hash, seaotRetainingWallHeight.hashCode);
    _$hash = $jc(_$hash, seaotWidth.hashCode);
    _$hash = $jc(_$hash, seaotLength.hashCode);
    _$hash = $jc(_$hash, seaotInvertLevel.hashCode);
    _$hash = $jc(_$hash, seaotGroundElevation.hashCode);
    _$hash = $jc(_$hash, seaotIncomingLineId.hashCode);
    _$hash = $jc(_$hash, seaotType.hashCode);
    _$hash = $jc(_$hash, seaotIncomingLineInvert.hashCode);
    _$hash = $jc(_$hash, seaotNoInspectionAccess.hashCode);
    _$hash = $jc(_$hash, upstreamStructureId.hashCode);
    _$hash = $jc(_$hash, downstreamStructureId.hashCode);
    _$hash = $jc(_$hash, groundElevation.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, designDate.hashCode);
    _$hash = $jc(_$hash, designCompany.hashCode);
    _$hash = $jc(_$hash, constructionDate.hashCode);
    _$hash = $jc(_$hash, constructionCompany.hashCode);
    _$hash = $jc(_$hash, inserviceDate.hashCode);
    _$hash = $jc(_$hash, lifeCycleStatus.hashCode);
    _$hash = $jc(_$hash, ancillaryRole.hashCode);
    _$hash = $jc(_$hash, enabled.hashCode);
    _$hash = $jc(_$hash, remarks.hashCode);
    _$hash = $jc(_$hash, lastEditedUser.hashCode);
    _$hash = $jc(_$hash, lastEditedDate.hashCode);
    _$hash = $jc(_$hash, createdUser.hashCode);
    _$hash = $jc(_$hash, createdDate.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerProperties')
          ..add('id', id)
          ..add('districtName', districtName)
          ..add('municipalityName', municipalityName)
          ..add('cityName', cityName)
          ..add('streetName', streetName)
          ..add('seaotNo', seaotNo)
          ..add('seaotId', seaotId)
          ..add('seaotHeight', seaotHeight)
          ..add('seaotRetainingWallHeight', seaotRetainingWallHeight)
          ..add('seaotWidth', seaotWidth)
          ..add('seaotLength', seaotLength)
          ..add('seaotInvertLevel', seaotInvertLevel)
          ..add('seaotGroundElevation', seaotGroundElevation)
          ..add('seaotIncomingLineId', seaotIncomingLineId)
          ..add('seaotType', seaotType)
          ..add('seaotIncomingLineInvert', seaotIncomingLineInvert)
          ..add('seaotNoInspectionAccess', seaotNoInspectionAccess)
          ..add('upstreamStructureId', upstreamStructureId)
          ..add('downstreamStructureId', downstreamStructureId)
          ..add('groundElevation', groundElevation)
          ..add('description', description)
          ..add('designDate', designDate)
          ..add('designCompany', designCompany)
          ..add('constructionDate', constructionDate)
          ..add('constructionCompany', constructionCompany)
          ..add('inserviceDate', inserviceDate)
          ..add('lifeCycleStatus', lifeCycleStatus)
          ..add('ancillaryRole', ancillaryRole)
          ..add('enabled', enabled)
          ..add('remarks', remarks)
          ..add('lastEditedUser', lastEditedUser)
          ..add('lastEditedDate', lastEditedDate)
          ..add('createdUser', createdUser)
          ..add('createdDate', createdDate))
        .toString();
  }
}

class MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
    implements
        Builder<
            MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerProperties,
            MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder> {
  _$MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerProperties?
      _$v;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _id;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get id => _$this._id ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set id(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              id) =>
      _$this._id = id;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _districtName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get districtName => _$this._districtName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set districtName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              districtName) =>
      _$this._districtName = districtName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _municipalityName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get municipalityName => _$this._municipalityName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set municipalityName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              municipalityName) =>
      _$this._municipalityName = municipalityName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _cityName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get cityName => _$this._cityName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set cityName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              cityName) =>
      _$this._cityName = cityName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _streetName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get streetName => _$this._streetName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set streetName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              streetName) =>
      _$this._streetName = streetName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _seaotNo;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get seaotNo => _$this._seaotNo ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set seaotNo(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              seaotNo) =>
      _$this._seaotNo = seaotNo;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _seaotId;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get seaotId => _$this._seaotId ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set seaotId(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              seaotId) =>
      _$this._seaotId = seaotId;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _seaotHeight;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get seaotHeight => _$this._seaotHeight ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set seaotHeight(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              seaotHeight) =>
      _$this._seaotHeight = seaotHeight;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _seaotRetainingWallHeight;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get seaotRetainingWallHeight => _$this._seaotRetainingWallHeight ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set seaotRetainingWallHeight(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              seaotRetainingWallHeight) =>
      _$this._seaotRetainingWallHeight = seaotRetainingWallHeight;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _seaotWidth;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get seaotWidth => _$this._seaotWidth ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set seaotWidth(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              seaotWidth) =>
      _$this._seaotWidth = seaotWidth;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _seaotLength;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get seaotLength => _$this._seaotLength ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set seaotLength(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              seaotLength) =>
      _$this._seaotLength = seaotLength;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _seaotInvertLevel;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get seaotInvertLevel => _$this._seaotInvertLevel ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set seaotInvertLevel(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              seaotInvertLevel) =>
      _$this._seaotInvertLevel = seaotInvertLevel;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _seaotGroundElevation;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get seaotGroundElevation => _$this._seaotGroundElevation ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set seaotGroundElevation(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              seaotGroundElevation) =>
      _$this._seaotGroundElevation = seaotGroundElevation;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _seaotIncomingLineId;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get seaotIncomingLineId => _$this._seaotIncomingLineId ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set seaotIncomingLineId(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              seaotIncomingLineId) =>
      _$this._seaotIncomingLineId = seaotIncomingLineId;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _seaotType;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get seaotType => _$this._seaotType ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set seaotType(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              seaotType) =>
      _$this._seaotType = seaotType;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _seaotIncomingLineInvert;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get seaotIncomingLineInvert => _$this._seaotIncomingLineInvert ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set seaotIncomingLineInvert(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              seaotIncomingLineInvert) =>
      _$this._seaotIncomingLineInvert = seaotIncomingLineInvert;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _seaotNoInspectionAccess;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get seaotNoInspectionAccess => _$this._seaotNoInspectionAccess ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set seaotNoInspectionAccess(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              seaotNoInspectionAccess) =>
      _$this._seaotNoInspectionAccess = seaotNoInspectionAccess;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _upstreamStructureId;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get upstreamStructureId => _$this._upstreamStructureId ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set upstreamStructureId(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              upstreamStructureId) =>
      _$this._upstreamStructureId = upstreamStructureId;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _downstreamStructureId;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get downstreamStructureId => _$this._downstreamStructureId ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set downstreamStructureId(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              downstreamStructureId) =>
      _$this._downstreamStructureId = downstreamStructureId;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _groundElevation;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get groundElevation => _$this._groundElevation ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set groundElevation(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              groundElevation) =>
      _$this._groundElevation = groundElevation;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _description;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get description => _$this._description ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set description(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              description) =>
      _$this._description = description;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _designDate;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get designDate => _$this._designDate ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set designDate(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              designDate) =>
      _$this._designDate = designDate;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _designCompany;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get designCompany => _$this._designCompany ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set designCompany(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              designCompany) =>
      _$this._designCompany = designCompany;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _constructionDate;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get constructionDate => _$this._constructionDate ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set constructionDate(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              constructionDate) =>
      _$this._constructionDate = constructionDate;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _constructionCompany;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get constructionCompany => _$this._constructionCompany ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set constructionCompany(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              constructionCompany) =>
      _$this._constructionCompany = constructionCompany;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _inserviceDate;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get inserviceDate => _$this._inserviceDate ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set inserviceDate(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              inserviceDate) =>
      _$this._inserviceDate = inserviceDate;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _lifeCycleStatus;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get lifeCycleStatus => _$this._lifeCycleStatus ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set lifeCycleStatus(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              lifeCycleStatus) =>
      _$this._lifeCycleStatus = lifeCycleStatus;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _ancillaryRole;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get ancillaryRole => _$this._ancillaryRole ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set ancillaryRole(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              ancillaryRole) =>
      _$this._ancillaryRole = ancillaryRole;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _enabled;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get enabled => _$this._enabled ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set enabled(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              enabled) =>
      _$this._enabled = enabled;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _remarks;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get remarks => _$this._remarks ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set remarks(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              remarks) =>
      _$this._remarks = remarks;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _lastEditedUser;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get lastEditedUser => _$this._lastEditedUser ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set lastEditedUser(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              lastEditedUser) =>
      _$this._lastEditedUser = lastEditedUser;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _lastEditedDate;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get lastEditedDate => _$this._lastEditedDate ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set lastEditedDate(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              lastEditedDate) =>
      _$this._lastEditedDate = lastEditedDate;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _createdUser;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get createdUser => _$this._createdUser ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set createdUser(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              createdUser) =>
      _$this._createdUser = createdUser;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _createdDate;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get createdDate => _$this._createdDate ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set createdDate(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              createdDate) =>
      _$this._createdDate = createdDate;

  MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder() {
    MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerProperties
        ._defaults(this);
  }

  MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id?.toBuilder();
      _districtName = $v.districtName?.toBuilder();
      _municipalityName = $v.municipalityName?.toBuilder();
      _cityName = $v.cityName?.toBuilder();
      _streetName = $v.streetName?.toBuilder();
      _seaotNo = $v.seaotNo?.toBuilder();
      _seaotId = $v.seaotId?.toBuilder();
      _seaotHeight = $v.seaotHeight?.toBuilder();
      _seaotRetainingWallHeight = $v.seaotRetainingWallHeight?.toBuilder();
      _seaotWidth = $v.seaotWidth?.toBuilder();
      _seaotLength = $v.seaotLength?.toBuilder();
      _seaotInvertLevel = $v.seaotInvertLevel?.toBuilder();
      _seaotGroundElevation = $v.seaotGroundElevation?.toBuilder();
      _seaotIncomingLineId = $v.seaotIncomingLineId?.toBuilder();
      _seaotType = $v.seaotType?.toBuilder();
      _seaotIncomingLineInvert = $v.seaotIncomingLineInvert?.toBuilder();
      _seaotNoInspectionAccess = $v.seaotNoInspectionAccess?.toBuilder();
      _upstreamStructureId = $v.upstreamStructureId?.toBuilder();
      _downstreamStructureId = $v.downstreamStructureId?.toBuilder();
      _groundElevation = $v.groundElevation?.toBuilder();
      _description = $v.description?.toBuilder();
      _designDate = $v.designDate?.toBuilder();
      _designCompany = $v.designCompany?.toBuilder();
      _constructionDate = $v.constructionDate?.toBuilder();
      _constructionCompany = $v.constructionCompany?.toBuilder();
      _inserviceDate = $v.inserviceDate?.toBuilder();
      _lifeCycleStatus = $v.lifeCycleStatus?.toBuilder();
      _ancillaryRole = $v.ancillaryRole?.toBuilder();
      _enabled = $v.enabled?.toBuilder();
      _remarks = $v.remarks?.toBuilder();
      _lastEditedUser = $v.lastEditedUser?.toBuilder();
      _lastEditedDate = $v.lastEditedDate?.toBuilder();
      _createdUser = $v.createdUser?.toBuilder();
      _createdDate = $v.createdDate?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerProperties
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerProperties;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerProperties
      build() => _build();

  _$MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerProperties
      _build() {
    _$MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerProperties
        _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerProperties
              ._(
              id: _id?.build(),
              districtName: _districtName?.build(),
              municipalityName: _municipalityName?.build(),
              cityName: _cityName?.build(),
              streetName: _streetName?.build(),
              seaotNo: _seaotNo?.build(),
              seaotId: _seaotId?.build(),
              seaotHeight: _seaotHeight?.build(),
              seaotRetainingWallHeight: _seaotRetainingWallHeight?.build(),
              seaotWidth: _seaotWidth?.build(),
              seaotLength: _seaotLength?.build(),
              seaotInvertLevel: _seaotInvertLevel?.build(),
              seaotGroundElevation: _seaotGroundElevation?.build(),
              seaotIncomingLineId: _seaotIncomingLineId?.build(),
              seaotType: _seaotType?.build(),
              seaotIncomingLineInvert: _seaotIncomingLineInvert?.build(),
              seaotNoInspectionAccess: _seaotNoInspectionAccess?.build(),
              upstreamStructureId: _upstreamStructureId?.build(),
              downstreamStructureId: _downstreamStructureId?.build(),
              groundElevation: _groundElevation?.build(),
              description: _description?.build(),
              designDate: _designDate?.build(),
              designCompany: _designCompany?.build(),
              constructionDate: _constructionDate?.build(),
              constructionCompany: _constructionCompany?.build(),
              inserviceDate: _inserviceDate?.build(),
              lifeCycleStatus: _lifeCycleStatus?.build(),
              ancillaryRole: _ancillaryRole?.build(),
              enabled: _enabled?.build(),
              remarks: _remarks?.build(),
              lastEditedUser: _lastEditedUser?.build(),
              lastEditedDate: _lastEditedDate?.build(),
              createdUser: _createdUser?.build(),
              createdDate: _createdDate?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'id';
        _id?.build();
        _$failedField = 'districtName';
        _districtName?.build();
        _$failedField = 'municipalityName';
        _municipalityName?.build();
        _$failedField = 'cityName';
        _cityName?.build();
        _$failedField = 'streetName';
        _streetName?.build();
        _$failedField = 'seaotNo';
        _seaotNo?.build();
        _$failedField = 'seaotId';
        _seaotId?.build();
        _$failedField = 'seaotHeight';
        _seaotHeight?.build();
        _$failedField = 'seaotRetainingWallHeight';
        _seaotRetainingWallHeight?.build();
        _$failedField = 'seaotWidth';
        _seaotWidth?.build();
        _$failedField = 'seaotLength';
        _seaotLength?.build();
        _$failedField = 'seaotInvertLevel';
        _seaotInvertLevel?.build();
        _$failedField = 'seaotGroundElevation';
        _seaotGroundElevation?.build();
        _$failedField = 'seaotIncomingLineId';
        _seaotIncomingLineId?.build();
        _$failedField = 'seaotType';
        _seaotType?.build();
        _$failedField = 'seaotIncomingLineInvert';
        _seaotIncomingLineInvert?.build();
        _$failedField = 'seaotNoInspectionAccess';
        _seaotNoInspectionAccess?.build();
        _$failedField = 'upstreamStructureId';
        _upstreamStructureId?.build();
        _$failedField = 'downstreamStructureId';
        _downstreamStructureId?.build();
        _$failedField = 'groundElevation';
        _groundElevation?.build();
        _$failedField = 'description';
        _description?.build();
        _$failedField = 'designDate';
        _designDate?.build();
        _$failedField = 'designCompany';
        _designCompany?.build();
        _$failedField = 'constructionDate';
        _constructionDate?.build();
        _$failedField = 'constructionCompany';
        _constructionCompany?.build();
        _$failedField = 'inserviceDate';
        _inserviceDate?.build();
        _$failedField = 'lifeCycleStatus';
        _lifeCycleStatus?.build();
        _$failedField = 'ancillaryRole';
        _ancillaryRole?.build();
        _$failedField = 'enabled';
        _enabled?.build();
        _$failedField = 'remarks';
        _remarks?.build();
        _$failedField = 'lastEditedUser';
        _lastEditedUser?.build();
        _$failedField = 'lastEditedDate';
        _lastEditedDate?.build();
        _$failedField = 'createdUser';
        _createdUser?.build();
        _$failedField = 'createdDate';
        _createdDate?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerProperties',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
