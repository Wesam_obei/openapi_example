// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_dashboard_create_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$UserDashboardCreateRequest extends UserDashboardCreateRequest {
  @override
  final String name;

  factory _$UserDashboardCreateRequest(
          [void Function(UserDashboardCreateRequestBuilder)? updates]) =>
      (new UserDashboardCreateRequestBuilder()..update(updates))._build();

  _$UserDashboardCreateRequest._({required this.name}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        name, r'UserDashboardCreateRequest', 'name');
  }

  @override
  UserDashboardCreateRequest rebuild(
          void Function(UserDashboardCreateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserDashboardCreateRequestBuilder toBuilder() =>
      new UserDashboardCreateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserDashboardCreateRequest && name == other.name;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'UserDashboardCreateRequest')
          ..add('name', name))
        .toString();
  }
}

class UserDashboardCreateRequestBuilder
    implements
        Builder<UserDashboardCreateRequest, UserDashboardCreateRequestBuilder> {
  _$UserDashboardCreateRequest? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  UserDashboardCreateRequestBuilder() {
    UserDashboardCreateRequest._defaults(this);
  }

  UserDashboardCreateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserDashboardCreateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$UserDashboardCreateRequest;
  }

  @override
  void update(void Function(UserDashboardCreateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  UserDashboardCreateRequest build() => _build();

  _$UserDashboardCreateRequest _build() {
    final _$result = _$v ??
        new _$UserDashboardCreateRequest._(
            name: BuiltValueNullFieldError.checkNotNull(
                name, r'UserDashboardCreateRequest', 'name'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
