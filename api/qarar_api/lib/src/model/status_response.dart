//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'status_response.g.dart';

/// StatusResponse
///
/// Properties:
/// * [id]
/// * [name]
@BuiltValue()
abstract class StatusResponse
    implements Built<StatusResponse, StatusResponseBuilder> {
  @BuiltValueField(wireName: r'id')
  num get id;

  @BuiltValueField(wireName: r'name')
  StatusResponseNameEnum get name;
  // enum nameEnum {  draft,  submitted,  approved,  need-change,  };

  StatusResponse._();

  factory StatusResponse([void updates(StatusResponseBuilder b)]) =
      _$StatusResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(StatusResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<StatusResponse> get serializer =>
      _$StatusResponseSerializer();
}

class _$StatusResponseSerializer
    implements PrimitiveSerializer<StatusResponse> {
  @override
  final Iterable<Type> types = const [StatusResponse, _$StatusResponse];

  @override
  final String wireName = r'StatusResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    StatusResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'id';
    yield serializers.serialize(
      object.id,
      specifiedType: const FullType(num),
    );
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(StatusResponseNameEnum),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    StatusResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required StatusResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.id = valueDes;
          break;
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(StatusResponseNameEnum),
          ) as StatusResponseNameEnum;
          result.name = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  StatusResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = StatusResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class StatusResponseNameEnum extends EnumClass {
  @BuiltValueEnumConst(wireName: r'draft')
  static const StatusResponseNameEnum draft = _$statusResponseNameEnum_draft;
  @BuiltValueEnumConst(wireName: r'submitted')
  static const StatusResponseNameEnum submitted =
      _$statusResponseNameEnum_submitted;
  @BuiltValueEnumConst(wireName: r'approved')
  static const StatusResponseNameEnum approved =
      _$statusResponseNameEnum_approved;
  @BuiltValueEnumConst(wireName: r'need-change', fallback: true)
  static const StatusResponseNameEnum needChange =
      _$statusResponseNameEnum_needChange;

  static Serializer<StatusResponseNameEnum> get serializer =>
      _$statusResponseNameEnumSerializer;

  const StatusResponseNameEnum._(String name) : super(name);

  static BuiltSet<StatusResponseNameEnum> get values =>
      _$statusResponseNameEnumValues;
  static StatusResponseNameEnum valueOf(String name) =>
      _$statusResponseNameEnumValueOf(name);
}
