// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_transport_tracks_map200_response_all_of_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum_featureCollection =
    const MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum
        ._('featureCollection');

MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum
    _$mapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnumValueOf(
        String name) {
  switch (name) {
    case 'featureCollection':
      return _$mapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum_featureCollection;
    default:
      return _$mapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum_featureCollection;
  }
}

final BuiltSet<
        MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnumValues =
    new BuiltSet<
        MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum>(const <MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum>[
  _$mapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum_featureCollection,
]);

Serializer<MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum>
    _$mapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnumSerializer =
    new _$MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnumSerializer();

class _$MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnumSerializer
    implements
        PrimitiveSerializer<
            MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'featureCollection': 'FeatureCollection',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'FeatureCollection': 'featureCollection',
  };

  @override
  final Iterable<Type> types = const <Type>[
    MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum
  ];
  @override
  final String wireName =
      'MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum';

  @override
  Object serialize(
          Serializers serializers,
          MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum
              object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum
      deserialize(Serializers serializers, Object serialized,
              {FullType specifiedType = FullType.unspecified}) =>
          MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum
              .valueOf(_fromWire[serialized] ??
                  (serialized is String ? serialized : ''));
}

class _$MapLayersControllerGetTransportTracksMap200ResponseAllOfData
    extends MapLayersControllerGetTransportTracksMap200ResponseAllOfData {
  @override
  final MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum
      type;
  @override
  final BuiltList<
          MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner>
      features;

  factory _$MapLayersControllerGetTransportTracksMap200ResponseAllOfData(
          [void Function(
                  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataBuilder)?
              updates]) =>
      (new MapLayersControllerGetTransportTracksMap200ResponseAllOfDataBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetTransportTracksMap200ResponseAllOfData._(
      {required this.type, required this.features})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        type,
        r'MapLayersControllerGetTransportTracksMap200ResponseAllOfData',
        'type');
    BuiltValueNullFieldError.checkNotNull(
        features,
        r'MapLayersControllerGetTransportTracksMap200ResponseAllOfData',
        'features');
  }

  @override
  MapLayersControllerGetTransportTracksMap200ResponseAllOfData rebuild(
          void Function(
                  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataBuilder
      toBuilder() =>
          new MapLayersControllerGetTransportTracksMap200ResponseAllOfDataBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetTransportTracksMap200ResponseAllOfData &&
        type == other.type &&
        features == other.features;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, features.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetTransportTracksMap200ResponseAllOfData')
          ..add('type', type)
          ..add('features', features))
        .toString();
  }
}

class MapLayersControllerGetTransportTracksMap200ResponseAllOfDataBuilder
    implements
        Builder<MapLayersControllerGetTransportTracksMap200ResponseAllOfData,
            MapLayersControllerGetTransportTracksMap200ResponseAllOfDataBuilder> {
  _$MapLayersControllerGetTransportTracksMap200ResponseAllOfData? _$v;

  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum? _type;
  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum?
      get type => _$this._type;
  set type(
          MapLayersControllerGetTransportTracksMap200ResponseAllOfDataTypeEnum?
              type) =>
      _$this._type = type;

  ListBuilder<
          MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner>?
      _features;
  ListBuilder<
          MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner>
      get features => _$this._features ??= new ListBuilder<
          MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner>();
  set features(
          ListBuilder<
                  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataFeaturesInner>?
              features) =>
      _$this._features = features;

  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataBuilder() {
    MapLayersControllerGetTransportTracksMap200ResponseAllOfData._defaults(
        this);
  }

  MapLayersControllerGetTransportTracksMap200ResponseAllOfDataBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _features = $v.features.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetTransportTracksMap200ResponseAllOfData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v =
        other as _$MapLayersControllerGetTransportTracksMap200ResponseAllOfData;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetTransportTracksMap200ResponseAllOfDataBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetTransportTracksMap200ResponseAllOfData build() =>
      _build();

  _$MapLayersControllerGetTransportTracksMap200ResponseAllOfData _build() {
    _$MapLayersControllerGetTransportTracksMap200ResponseAllOfData _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetTransportTracksMap200ResponseAllOfData._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type,
                  r'MapLayersControllerGetTransportTracksMap200ResponseAllOfData',
                  'type'),
              features: features.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'features';
        features.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetTransportTracksMap200ResponseAllOfData',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
