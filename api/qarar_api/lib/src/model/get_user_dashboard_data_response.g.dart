// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_user_dashboard_data_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetUserDashboardDataResponse extends GetUserDashboardDataResponse {
  @override
  final String id;
  @override
  final String name;
  @override
  final bool isDefault;
  @override
  final BuiltList<WidgetConfig>? settings;

  factory _$GetUserDashboardDataResponse(
          [void Function(GetUserDashboardDataResponseBuilder)? updates]) =>
      (new GetUserDashboardDataResponseBuilder()..update(updates))._build();

  _$GetUserDashboardDataResponse._(
      {required this.id,
      required this.name,
      required this.isDefault,
      this.settings})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'GetUserDashboardDataResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, r'GetUserDashboardDataResponse', 'name');
    BuiltValueNullFieldError.checkNotNull(
        isDefault, r'GetUserDashboardDataResponse', 'isDefault');
  }

  @override
  GetUserDashboardDataResponse rebuild(
          void Function(GetUserDashboardDataResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetUserDashboardDataResponseBuilder toBuilder() =>
      new GetUserDashboardDataResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetUserDashboardDataResponse &&
        id == other.id &&
        name == other.name &&
        isDefault == other.isDefault &&
        settings == other.settings;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, isDefault.hashCode);
    _$hash = $jc(_$hash, settings.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetUserDashboardDataResponse')
          ..add('id', id)
          ..add('name', name)
          ..add('isDefault', isDefault)
          ..add('settings', settings))
        .toString();
  }
}

class GetUserDashboardDataResponseBuilder
    implements
        Builder<GetUserDashboardDataResponse,
            GetUserDashboardDataResponseBuilder> {
  _$GetUserDashboardDataResponse? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  bool? _isDefault;
  bool? get isDefault => _$this._isDefault;
  set isDefault(bool? isDefault) => _$this._isDefault = isDefault;

  ListBuilder<WidgetConfig>? _settings;
  ListBuilder<WidgetConfig> get settings =>
      _$this._settings ??= new ListBuilder<WidgetConfig>();
  set settings(ListBuilder<WidgetConfig>? settings) =>
      _$this._settings = settings;

  GetUserDashboardDataResponseBuilder() {
    GetUserDashboardDataResponse._defaults(this);
  }

  GetUserDashboardDataResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _isDefault = $v.isDefault;
      _settings = $v.settings?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetUserDashboardDataResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetUserDashboardDataResponse;
  }

  @override
  void update(void Function(GetUserDashboardDataResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetUserDashboardDataResponse build() => _build();

  _$GetUserDashboardDataResponse _build() {
    _$GetUserDashboardDataResponse _$result;
    try {
      _$result = _$v ??
          new _$GetUserDashboardDataResponse._(
              id: BuiltValueNullFieldError.checkNotNull(
                  id, r'GetUserDashboardDataResponse', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'GetUserDashboardDataResponse', 'name'),
              isDefault: BuiltValueNullFieldError.checkNotNull(
                  isDefault, r'GetUserDashboardDataResponse', 'isDefault'),
              settings: _settings?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'settings';
        _settings?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'GetUserDashboardDataResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
