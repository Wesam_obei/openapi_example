//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'entity_create_request.g.dart';

/// EntityCreateRequest
///
/// Properties:
/// * [name]
/// * [users]
/// * [fileToken]
@BuiltValue()
abstract class EntityCreateRequest
    implements Built<EntityCreateRequest, EntityCreateRequestBuilder> {
  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'users')
  BuiltList<num> get users;

  @BuiltValueField(wireName: r'file_token')
  String? get fileToken;

  EntityCreateRequest._();

  factory EntityCreateRequest([void updates(EntityCreateRequestBuilder b)]) =
      _$EntityCreateRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(EntityCreateRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<EntityCreateRequest> get serializer =>
      _$EntityCreateRequestSerializer();
}

class _$EntityCreateRequestSerializer
    implements PrimitiveSerializer<EntityCreateRequest> {
  @override
  final Iterable<Type> types = const [
    EntityCreateRequest,
    _$EntityCreateRequest
  ];

  @override
  final String wireName = r'EntityCreateRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    EntityCreateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    yield r'users';
    yield serializers.serialize(
      object.users,
      specifiedType: const FullType(BuiltList, [FullType(num)]),
    );
    if (object.fileToken != null) {
      yield r'file_token';
      yield serializers.serialize(
        object.fileToken,
        specifiedType: const FullType(String),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    EntityCreateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required EntityCreateRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'users':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(num)]),
          ) as BuiltList<num>;
          result.users.replace(valueDes);
          break;
        case r'file_token':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.fileToken = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  EntityCreateRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = EntityCreateRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
