// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_incident_reports_map200_response_all_of_data_features_inner_properties.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties
    extends MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties {
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      id;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      status;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      locationId;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      locationName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      streetFullName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      ticketNumber;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      createdDate;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      closedDate;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      district;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      municipality;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      submunicipalityName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      mainClassificationId;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      mainClassificationName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      subClassificationId;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      subClassificationName;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      subSubClassificationId;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      subSubClassificationName;

  factory _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties(
          [void Function(
                  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
              updates]) =>
      (new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties._(
      {this.id,
      this.status,
      this.locationId,
      this.locationName,
      this.streetFullName,
      this.ticketNumber,
      this.createdDate,
      this.closedDate,
      this.district,
      this.municipality,
      this.submunicipalityName,
      this.mainClassificationId,
      this.mainClassificationName,
      this.subClassificationId,
      this.subClassificationName,
      this.subSubClassificationId,
      this.subSubClassificationName})
      : super._();

  @override
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties
      rebuild(
              void Function(
                      MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)
                  updates) =>
          (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      toBuilder() =>
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties &&
        id == other.id &&
        status == other.status &&
        locationId == other.locationId &&
        locationName == other.locationName &&
        streetFullName == other.streetFullName &&
        ticketNumber == other.ticketNumber &&
        createdDate == other.createdDate &&
        closedDate == other.closedDate &&
        district == other.district &&
        municipality == other.municipality &&
        submunicipalityName == other.submunicipalityName &&
        mainClassificationId == other.mainClassificationId &&
        mainClassificationName == other.mainClassificationName &&
        subClassificationId == other.subClassificationId &&
        subClassificationName == other.subClassificationName &&
        subSubClassificationId == other.subSubClassificationId &&
        subSubClassificationName == other.subSubClassificationName;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jc(_$hash, locationId.hashCode);
    _$hash = $jc(_$hash, locationName.hashCode);
    _$hash = $jc(_$hash, streetFullName.hashCode);
    _$hash = $jc(_$hash, ticketNumber.hashCode);
    _$hash = $jc(_$hash, createdDate.hashCode);
    _$hash = $jc(_$hash, closedDate.hashCode);
    _$hash = $jc(_$hash, district.hashCode);
    _$hash = $jc(_$hash, municipality.hashCode);
    _$hash = $jc(_$hash, submunicipalityName.hashCode);
    _$hash = $jc(_$hash, mainClassificationId.hashCode);
    _$hash = $jc(_$hash, mainClassificationName.hashCode);
    _$hash = $jc(_$hash, subClassificationId.hashCode);
    _$hash = $jc(_$hash, subClassificationName.hashCode);
    _$hash = $jc(_$hash, subSubClassificationId.hashCode);
    _$hash = $jc(_$hash, subSubClassificationName.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties')
          ..add('id', id)
          ..add('status', status)
          ..add('locationId', locationId)
          ..add('locationName', locationName)
          ..add('streetFullName', streetFullName)
          ..add('ticketNumber', ticketNumber)
          ..add('createdDate', createdDate)
          ..add('closedDate', closedDate)
          ..add('district', district)
          ..add('municipality', municipality)
          ..add('submunicipalityName', submunicipalityName)
          ..add('mainClassificationId', mainClassificationId)
          ..add('mainClassificationName', mainClassificationName)
          ..add('subClassificationId', subClassificationId)
          ..add('subClassificationName', subClassificationName)
          ..add('subSubClassificationId', subSubClassificationId)
          ..add('subSubClassificationName', subSubClassificationName))
        .toString();
  }
}

class MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
    implements
        Builder<
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties,
            MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder> {
  _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties?
      _$v;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _id;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get id => _$this._id ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set id(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              id) =>
      _$this._id = id;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _status;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get status => _$this._status ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set status(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              status) =>
      _$this._status = status;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _locationId;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get locationId => _$this._locationId ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set locationId(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              locationId) =>
      _$this._locationId = locationId;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _locationName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get locationName => _$this._locationName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set locationName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              locationName) =>
      _$this._locationName = locationName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _streetFullName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get streetFullName => _$this._streetFullName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set streetFullName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              streetFullName) =>
      _$this._streetFullName = streetFullName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _ticketNumber;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get ticketNumber => _$this._ticketNumber ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set ticketNumber(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              ticketNumber) =>
      _$this._ticketNumber = ticketNumber;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _createdDate;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get createdDate => _$this._createdDate ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set createdDate(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              createdDate) =>
      _$this._createdDate = createdDate;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _closedDate;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get closedDate => _$this._closedDate ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set closedDate(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              closedDate) =>
      _$this._closedDate = closedDate;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _district;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get district => _$this._district ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set district(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              district) =>
      _$this._district = district;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _municipality;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get municipality => _$this._municipality ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set municipality(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              municipality) =>
      _$this._municipality = municipality;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _submunicipalityName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get submunicipalityName => _$this._submunicipalityName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set submunicipalityName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              submunicipalityName) =>
      _$this._submunicipalityName = submunicipalityName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _mainClassificationId;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get mainClassificationId => _$this._mainClassificationId ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set mainClassificationId(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              mainClassificationId) =>
      _$this._mainClassificationId = mainClassificationId;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _mainClassificationName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get mainClassificationName => _$this._mainClassificationName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set mainClassificationName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              mainClassificationName) =>
      _$this._mainClassificationName = mainClassificationName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _subClassificationId;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get subClassificationId => _$this._subClassificationId ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set subClassificationId(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              subClassificationId) =>
      _$this._subClassificationId = subClassificationId;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _subClassificationName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get subClassificationName => _$this._subClassificationName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set subClassificationName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              subClassificationName) =>
      _$this._subClassificationName = subClassificationName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _subSubClassificationId;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get subSubClassificationId => _$this._subSubClassificationId ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set subSubClassificationId(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              subSubClassificationId) =>
      _$this._subSubClassificationId = subSubClassificationId;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _subSubClassificationName;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get subSubClassificationName => _$this._subSubClassificationName ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set subSubClassificationName(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              subSubClassificationName) =>
      _$this._subSubClassificationName = subSubClassificationName;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder() {
    MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties
        ._defaults(this);
  }

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id?.toBuilder();
      _status = $v.status?.toBuilder();
      _locationId = $v.locationId?.toBuilder();
      _locationName = $v.locationName?.toBuilder();
      _streetFullName = $v.streetFullName?.toBuilder();
      _ticketNumber = $v.ticketNumber?.toBuilder();
      _createdDate = $v.createdDate?.toBuilder();
      _closedDate = $v.closedDate?.toBuilder();
      _district = $v.district?.toBuilder();
      _municipality = $v.municipality?.toBuilder();
      _submunicipalityName = $v.submunicipalityName?.toBuilder();
      _mainClassificationId = $v.mainClassificationId?.toBuilder();
      _mainClassificationName = $v.mainClassificationName?.toBuilder();
      _subClassificationId = $v.subClassificationId?.toBuilder();
      _subClassificationName = $v.subClassificationName?.toBuilder();
      _subSubClassificationId = $v.subSubClassificationId?.toBuilder();
      _subSubClassificationName = $v.subSubClassificationName?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties
      build() => _build();

  _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties
      _build() {
    _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties
        _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties
              ._(
              id: _id?.build(),
              status: _status?.build(),
              locationId: _locationId?.build(),
              locationName: _locationName?.build(),
              streetFullName: _streetFullName?.build(),
              ticketNumber: _ticketNumber?.build(),
              createdDate: _createdDate?.build(),
              closedDate: _closedDate?.build(),
              district: _district?.build(),
              municipality: _municipality?.build(),
              submunicipalityName: _submunicipalityName?.build(),
              mainClassificationId: _mainClassificationId?.build(),
              mainClassificationName: _mainClassificationName?.build(),
              subClassificationId: _subClassificationId?.build(),
              subClassificationName: _subClassificationName?.build(),
              subSubClassificationId: _subSubClassificationId?.build(),
              subSubClassificationName: _subSubClassificationName?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'id';
        _id?.build();
        _$failedField = 'status';
        _status?.build();
        _$failedField = 'locationId';
        _locationId?.build();
        _$failedField = 'locationName';
        _locationName?.build();
        _$failedField = 'streetFullName';
        _streetFullName?.build();
        _$failedField = 'ticketNumber';
        _ticketNumber?.build();
        _$failedField = 'createdDate';
        _createdDate?.build();
        _$failedField = 'closedDate';
        _closedDate?.build();
        _$failedField = 'district';
        _district?.build();
        _$failedField = 'municipality';
        _municipality?.build();
        _$failedField = 'submunicipalityName';
        _submunicipalityName?.build();
        _$failedField = 'mainClassificationId';
        _mainClassificationId?.build();
        _$failedField = 'mainClassificationName';
        _mainClassificationName?.build();
        _$failedField = 'subClassificationId';
        _subClassificationId?.build();
        _$failedField = 'subClassificationName';
        _subClassificationName?.build();
        _$failedField = 'subSubClassificationId';
        _subSubClassificationId?.build();
        _$failedField = 'subSubClassificationName';
        _subSubClassificationName?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
