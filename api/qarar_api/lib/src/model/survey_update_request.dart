//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'survey_update_request.g.dart';

/// SurveyUpdateRequest
///
/// Properties:
/// * [name]
/// * [formId]
/// * [isActive]
/// * [startAt]
/// * [endAt]
/// * [usersIds]
/// * [entitiesIds]
/// * [description]
/// * [deletedUsersIds]
/// * [deletedEntitiesIds]
@BuiltValue()
abstract class SurveyUpdateRequest
    implements Built<SurveyUpdateRequest, SurveyUpdateRequestBuilder> {
  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'form_id')
  num get formId;

  @BuiltValueField(wireName: r'is_active')
  bool get isActive;

  @BuiltValueField(wireName: r'start_at')
  DateTime get startAt;

  @BuiltValueField(wireName: r'end_at')
  DateTime get endAt;

  @BuiltValueField(wireName: r'usersIds')
  BuiltList<num> get usersIds;

  @BuiltValueField(wireName: r'entitiesIds')
  BuiltList<num> get entitiesIds;

  @BuiltValueField(wireName: r'description')
  String? get description;

  @BuiltValueField(wireName: r'deletedUsersIds')
  BuiltList<num>? get deletedUsersIds;

  @BuiltValueField(wireName: r'deletedEntitiesIds')
  BuiltList<num>? get deletedEntitiesIds;

  SurveyUpdateRequest._();

  factory SurveyUpdateRequest([void updates(SurveyUpdateRequestBuilder b)]) =
      _$SurveyUpdateRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(SurveyUpdateRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<SurveyUpdateRequest> get serializer =>
      _$SurveyUpdateRequestSerializer();
}

class _$SurveyUpdateRequestSerializer
    implements PrimitiveSerializer<SurveyUpdateRequest> {
  @override
  final Iterable<Type> types = const [
    SurveyUpdateRequest,
    _$SurveyUpdateRequest
  ];

  @override
  final String wireName = r'SurveyUpdateRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    SurveyUpdateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    yield r'form_id';
    yield serializers.serialize(
      object.formId,
      specifiedType: const FullType(num),
    );
    yield r'is_active';
    yield serializers.serialize(
      object.isActive,
      specifiedType: const FullType(bool),
    );
    yield r'start_at';
    yield serializers.serialize(
      object.startAt,
      specifiedType: const FullType(DateTime),
    );
    yield r'end_at';
    yield serializers.serialize(
      object.endAt,
      specifiedType: const FullType(DateTime),
    );
    yield r'usersIds';
    yield serializers.serialize(
      object.usersIds,
      specifiedType: const FullType(BuiltList, [FullType(num)]),
    );
    yield r'entitiesIds';
    yield serializers.serialize(
      object.entitiesIds,
      specifiedType: const FullType(BuiltList, [FullType(num)]),
    );
    if (object.description != null) {
      yield r'description';
      yield serializers.serialize(
        object.description,
        specifiedType: const FullType(String),
      );
    }
    if (object.deletedUsersIds != null) {
      yield r'deletedUsersIds';
      yield serializers.serialize(
        object.deletedUsersIds,
        specifiedType: const FullType(BuiltList, [FullType(num)]),
      );
    }
    if (object.deletedEntitiesIds != null) {
      yield r'deletedEntitiesIds';
      yield serializers.serialize(
        object.deletedEntitiesIds,
        specifiedType: const FullType(BuiltList, [FullType(num)]),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    SurveyUpdateRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required SurveyUpdateRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'form_id':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.formId = valueDes;
          break;
        case r'is_active':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.isActive = valueDes;
          break;
        case r'start_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.startAt = valueDes;
          break;
        case r'end_at':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.endAt = valueDes;
          break;
        case r'usersIds':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(num)]),
          ) as BuiltList<num>;
          result.usersIds.replace(valueDes);
          break;
        case r'entitiesIds':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(num)]),
          ) as BuiltList<num>;
          result.entitiesIds.replace(valueDes);
          break;
        case r'description':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.description = valueDes;
          break;
        case r'deletedUsersIds':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(num)]),
          ) as BuiltList<num>;
          result.deletedUsersIds.replace(valueDes);
          break;
        case r'deletedEntitiesIds':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(num)]),
          ) as BuiltList<num>;
          result.deletedEntitiesIds.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  SurveyUpdateRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = SurveyUpdateRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
