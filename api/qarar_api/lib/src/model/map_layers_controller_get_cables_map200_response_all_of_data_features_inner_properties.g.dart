// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_layers_controller_get_cables_map200_response_all_of_data_features_inner_properties.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerProperties
    extends MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerProperties {
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      id;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      cableId;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      cableSize;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      cableEnabled;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      cableMaterial;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      cableInstallationDate;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      cableElevation;
  @override
  final MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId?
      substationNO;

  factory _$MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerProperties(
          [void Function(
                  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
              updates]) =>
      (new MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..update(updates))
          ._build();

  _$MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerProperties._(
      {this.id,
      this.cableId,
      this.cableSize,
      this.cableEnabled,
      this.cableMaterial,
      this.cableInstallationDate,
      this.cableElevation,
      this.substationNO})
      : super._();

  @override
  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerProperties
      rebuild(
              void Function(
                      MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)
                  updates) =>
          (toBuilder()..update(updates)).build();

  @override
  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      toBuilder() =>
          new MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerProperties &&
        id == other.id &&
        cableId == other.cableId &&
        cableSize == other.cableSize &&
        cableEnabled == other.cableEnabled &&
        cableMaterial == other.cableMaterial &&
        cableInstallationDate == other.cableInstallationDate &&
        cableElevation == other.cableElevation &&
        substationNO == other.substationNO;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, cableId.hashCode);
    _$hash = $jc(_$hash, cableSize.hashCode);
    _$hash = $jc(_$hash, cableEnabled.hashCode);
    _$hash = $jc(_$hash, cableMaterial.hashCode);
    _$hash = $jc(_$hash, cableInstallationDate.hashCode);
    _$hash = $jc(_$hash, cableElevation.hashCode);
    _$hash = $jc(_$hash, substationNO.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerProperties')
          ..add('id', id)
          ..add('cableId', cableId)
          ..add('cableSize', cableSize)
          ..add('cableEnabled', cableEnabled)
          ..add('cableMaterial', cableMaterial)
          ..add('cableInstallationDate', cableInstallationDate)
          ..add('cableElevation', cableElevation)
          ..add('substationNO', substationNO))
        .toString();
  }
}

class MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
    implements
        Builder<
            MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerProperties,
            MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder> {
  _$MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerProperties?
      _$v;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _id;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get id => _$this._id ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set id(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              id) =>
      _$this._id = id;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _cableId;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get cableId => _$this._cableId ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set cableId(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              cableId) =>
      _$this._cableId = cableId;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _cableSize;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get cableSize => _$this._cableSize ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set cableSize(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              cableSize) =>
      _$this._cableSize = cableSize;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _cableEnabled;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get cableEnabled => _$this._cableEnabled ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set cableEnabled(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              cableEnabled) =>
      _$this._cableEnabled = cableEnabled;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _cableMaterial;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get cableMaterial => _$this._cableMaterial ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set cableMaterial(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              cableMaterial) =>
      _$this._cableMaterial = cableMaterial;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _cableInstallationDate;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get cableInstallationDate => _$this._cableInstallationDate ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set cableInstallationDate(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              cableInstallationDate) =>
      _$this._cableInstallationDate = cableInstallationDate;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _cableElevation;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get cableElevation => _$this._cableElevation ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set cableElevation(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              cableElevation) =>
      _$this._cableElevation = cableElevation;

  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
      _substationNO;
  MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder
      get substationNO => _$this._substationNO ??=
          new MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder();
  set substationNO(
          MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesIdBuilder?
              substationNO) =>
      _$this._substationNO = substationNO;

  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder() {
    MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerProperties
        ._defaults(this);
  }

  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id?.toBuilder();
      _cableId = $v.cableId?.toBuilder();
      _cableSize = $v.cableSize?.toBuilder();
      _cableEnabled = $v.cableEnabled?.toBuilder();
      _cableMaterial = $v.cableMaterial?.toBuilder();
      _cableInstallationDate = $v.cableInstallationDate?.toBuilder();
      _cableElevation = $v.cableElevation?.toBuilder();
      _substationNO = $v.substationNO?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerProperties
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerProperties;
  }

  @override
  void update(
      void Function(
              MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerProperties
      build() => _build();

  _$MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerProperties
      _build() {
    _$MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerProperties
        _$result;
    try {
      _$result = _$v ??
          new _$MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerProperties
              ._(
              id: _id?.build(),
              cableId: _cableId?.build(),
              cableSize: _cableSize?.build(),
              cableEnabled: _cableEnabled?.build(),
              cableMaterial: _cableMaterial?.build(),
              cableInstallationDate: _cableInstallationDate?.build(),
              cableElevation: _cableElevation?.build(),
              substationNO: _substationNO?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'id';
        _id?.build();
        _$failedField = 'cableId';
        _cableId?.build();
        _$failedField = 'cableSize';
        _cableSize?.build();
        _$failedField = 'cableEnabled';
        _cableEnabled?.build();
        _$failedField = 'cableMaterial';
        _cableMaterial?.build();
        _$failedField = 'cableInstallationDate';
        _cableInstallationDate?.build();
        _$failedField = 'cableElevation';
        _cableElevation?.build();
        _$failedField = 'substationNO';
        _substationNO?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerProperties',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
