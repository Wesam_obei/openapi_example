//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/model/paginated_links_documented.dart';
import 'package:qarar_api/src/model/paginated_meta_documented.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'paginated_documented.g.dart';

/// PaginatedDocumented
///
/// Properties:
/// * [success]
/// * [data]
/// * [meta]
/// * [links]
@BuiltValue(instantiable: false)
abstract class PaginatedDocumented {
  @BuiltValueField(wireName: r'success')
  bool get success;

  @BuiltValueField(wireName: r'data')
  BuiltList<JsonObject> get data;

  @BuiltValueField(wireName: r'meta')
  PaginatedMetaDocumented get meta;

  @BuiltValueField(wireName: r'links')
  PaginatedLinksDocumented get links;

  @BuiltValueSerializer(custom: true)
  static Serializer<PaginatedDocumented> get serializer =>
      _$PaginatedDocumentedSerializer();
}

class _$PaginatedDocumentedSerializer
    implements PrimitiveSerializer<PaginatedDocumented> {
  @override
  final Iterable<Type> types = const [PaginatedDocumented];

  @override
  final String wireName = r'PaginatedDocumented';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    PaginatedDocumented object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'success';
    yield serializers.serialize(
      object.success,
      specifiedType: const FullType(bool),
    );
    yield r'data';
    yield serializers.serialize(
      object.data,
      specifiedType: const FullType(BuiltList, [FullType(JsonObject)]),
    );
    yield r'meta';
    yield serializers.serialize(
      object.meta,
      specifiedType: const FullType(PaginatedMetaDocumented),
    );
    yield r'links';
    yield serializers.serialize(
      object.links,
      specifiedType: const FullType(PaginatedLinksDocumented),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    PaginatedDocumented object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object,
            specifiedType: specifiedType)
        .toList();
  }

  @override
  PaginatedDocumented deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return serializers.deserialize(serialized,
        specifiedType: FullType($PaginatedDocumented)) as $PaginatedDocumented;
  }
}

/// a concrete implementation of [PaginatedDocumented], since [PaginatedDocumented] is not instantiable
@BuiltValue(instantiable: true)
abstract class $PaginatedDocumented
    implements
        PaginatedDocumented,
        Built<$PaginatedDocumented, $PaginatedDocumentedBuilder> {
  $PaginatedDocumented._();

  factory $PaginatedDocumented(
          [void Function($PaginatedDocumentedBuilder)? updates]) =
      _$$PaginatedDocumented;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults($PaginatedDocumentedBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<$PaginatedDocumented> get serializer =>
      _$$PaginatedDocumentedSerializer();
}

class _$$PaginatedDocumentedSerializer
    implements PrimitiveSerializer<$PaginatedDocumented> {
  @override
  final Iterable<Type> types = const [
    $PaginatedDocumented,
    _$$PaginatedDocumented
  ];

  @override
  final String wireName = r'$PaginatedDocumented';

  @override
  Object serialize(
    Serializers serializers,
    $PaginatedDocumented object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return serializers.serialize(object,
        specifiedType: FullType(PaginatedDocumented))!;
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required PaginatedDocumentedBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'success':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.success = valueDes;
          break;
        case r'data':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(JsonObject)]),
          ) as BuiltList<JsonObject>;
          result.data.replace(valueDes);
          break;
        case r'meta':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(PaginatedMetaDocumented),
          ) as PaginatedMetaDocumented;
          result.meta.replace(valueDes);
          break;
        case r'links':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(PaginatedLinksDocumented),
          ) as PaginatedLinksDocumented;
          result.links.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  $PaginatedDocumented deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = $PaginatedDocumentedBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}
