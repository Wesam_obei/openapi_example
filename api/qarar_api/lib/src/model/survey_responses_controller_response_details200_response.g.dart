// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'survey_responses_controller_response_details200_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SurveyResponsesControllerResponseDetails200Response
    extends SurveyResponsesControllerResponseDetails200Response {
  @override
  final bool success;
  @override
  final JsonObject data;

  factory _$SurveyResponsesControllerResponseDetails200Response(
          [void Function(
                  SurveyResponsesControllerResponseDetails200ResponseBuilder)?
              updates]) =>
      (new SurveyResponsesControllerResponseDetails200ResponseBuilder()
            ..update(updates))
          ._build();

  _$SurveyResponsesControllerResponseDetails200Response._(
      {required this.success, required this.data})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(success,
        r'SurveyResponsesControllerResponseDetails200Response', 'success');
    BuiltValueNullFieldError.checkNotNull(
        data, r'SurveyResponsesControllerResponseDetails200Response', 'data');
  }

  @override
  SurveyResponsesControllerResponseDetails200Response rebuild(
          void Function(
                  SurveyResponsesControllerResponseDetails200ResponseBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SurveyResponsesControllerResponseDetails200ResponseBuilder toBuilder() =>
      new SurveyResponsesControllerResponseDetails200ResponseBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SurveyResponsesControllerResponseDetails200Response &&
        success == other.success &&
        data == other.data;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, data.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'SurveyResponsesControllerResponseDetails200Response')
          ..add('success', success)
          ..add('data', data))
        .toString();
  }
}

class SurveyResponsesControllerResponseDetails200ResponseBuilder
    implements
        Builder<SurveyResponsesControllerResponseDetails200Response,
            SurveyResponsesControllerResponseDetails200ResponseBuilder>,
        SuccessResponseBuilder {
  _$SurveyResponsesControllerResponseDetails200Response? _$v;

  bool? _success;
  bool? get success => _$this._success;
  set success(covariant bool? success) => _$this._success = success;

  JsonObject? _data;
  JsonObject? get data => _$this._data;
  set data(covariant JsonObject? data) => _$this._data = data;

  SurveyResponsesControllerResponseDetails200ResponseBuilder() {
    SurveyResponsesControllerResponseDetails200Response._defaults(this);
  }

  SurveyResponsesControllerResponseDetails200ResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _success = $v.success;
      _data = $v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      covariant SurveyResponsesControllerResponseDetails200Response other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SurveyResponsesControllerResponseDetails200Response;
  }

  @override
  void update(
      void Function(SurveyResponsesControllerResponseDetails200ResponseBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  SurveyResponsesControllerResponseDetails200Response build() => _build();

  _$SurveyResponsesControllerResponseDetails200Response _build() {
    final _$result = _$v ??
        new _$SurveyResponsesControllerResponseDetails200Response._(
            success: BuiltValueNullFieldError.checkNotNull(
                success,
                r'SurveyResponsesControllerResponseDetails200Response',
                'success'),
            data: BuiltValueNullFieldError.checkNotNull(
                data,
                r'SurveyResponsesControllerResponseDetails200Response',
                'data'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
