//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'dart:async';

import 'package:built_value/serializer.dart';
import 'package:dio/dio.dart';

import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/api_util.dart';
import 'package:qarar_api/src/model/error_response.dart';
import 'package:qarar_api/src/model/plan_create_request.dart';
import 'package:qarar_api/src/model/plan_update_request.dart';
import 'package:qarar_api/src/model/plans_controller_get200_response.dart';
import 'package:qarar_api/src/model/plans_controller_list200_response.dart';

class PlansApi {
  final Dio _dio;

  final Serializers _serializers;

  const PlansApi(this._dio, this._serializers);

  /// plansControllerCreate
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [planCreateRequest]
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future]
  /// Throws [DioException] if API call or serialization fails
  Future<Response<void>> plansControllerCreate({
    String authorization = '{{token}}',
    required PlanCreateRequest planCreateRequest,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/plans';
    final _options = Options(
      method: r'POST',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      contentType: 'application/json',
      validateStatus: validateStatus,
    );

    dynamic _bodyData;

    try {
      const _type = FullType(PlanCreateRequest);
      _bodyData =
          _serializers.serialize(planCreateRequest, specifiedType: _type);
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _options.compose(
          _dio.options,
          _path,
        ),
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    final _response = await _dio.request<Object>(
      _path,
      data: _bodyData,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    return _response;
  }

  /// plansControllerDelete
  ///
  ///
  /// Parameters:
  /// * [id]
  /// * [authorization] - Bearer {token}
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future]
  /// Throws [DioException] if API call or serialization fails
  Future<Response<void>> plansControllerDelete({
    required String id,
    String authorization = '{{token}}',
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/plans/{id}'.replaceAll(
        '{' r'id' '}',
        encodeQueryParameter(_serializers, id, const FullType(String))
            .toString());
    final _options = Options(
      method: r'DELETE',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    return _response;
  }

  /// plansControllerGet
  ///
  ///
  /// Parameters:
  /// * [id]
  /// * [authorization] - Bearer {token}
  /// * [includes] - List of relations to select.     <p>              <b>Example: </b> versions,versions.file           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [PlansControllerGet200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<PlansControllerGet200Response>> plansControllerGet({
    required String id,
    String authorization = '{{token}}',
    String? includes,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/plans/{id}'.replaceAll(
        '{' r'id' '}',
        encodeQueryParameter(_serializers, id, const FullType(String))
            .toString());
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _queryParameters = <String, dynamic>{
      if (includes != null)
        r'includes': encodeQueryParameter(
            _serializers, includes, const FullType(String)),
    };

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      queryParameters: _queryParameters,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    PlansControllerGet200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(PlansControllerGet200Response),
            ) as PlansControllerGet200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<PlansControllerGet200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// plansControllerList
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [page] - Page number to retrieve.If you provide invalid value the default page number will applied         <p>              <b>Example: </b> 1           </p>         <p>              <b>Default Value: </b> 1           </p>
  /// * [limit] - Number of records per page.       <p>              <b>Example: </b> 10           </p>       <p>              <b>Default Value: </b> 20           </p>       <p>              <b>Max Value: </b> 100           </p>               If provided value is greater than max value, max value will be applied.
  /// * [filterPeriodId] - Filter by id query param.           <p>              <b>Format: </b> filter.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.id=$not:$like:John Doe&filter.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul>
  /// * [filterPeriodName] - Filter by name query param.           <p>              <b>Format: </b> filter.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.name=$not:$like:John Doe&filter.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul>
  /// * [filterPeriodDescription] - Filter by description query param.           <p>              <b>Format: </b> filter.description={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.description=$not:$like:John Doe&filter.description=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul>
  /// * [filterPeriodVersionPeriodName] - Filter by version.name query param.           <p>              <b>Format: </b> filter.version.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.version.name=$not:$like:John Doe&filter.version.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul>
  /// * [sortBy] - Parameter to sort by.       <p>To sort by multiple fields, just provide query param multiple types. The order in url defines an order of sorting</p>       <p>              <b>Format: </b> fieldName:DIRECTION           </p>       <p>              <b>Example: </b> sortBy=id:DESC&sortBy=createdAt:ASC           </p>       <p>              <b>Default Value: </b> id:DESC           </p>       <h4>Available Fields</h4><ul><li>id</li> <li>name</li> <li>versions.created_at</li></ul>
  /// * [search] - Search term to filter result values         <p>              <b>Example: </b> John           </p>         <p>              <b>Default Value: </b> No default value           </p>
  /// * [searchBy] - List of fields to search by term to filter result values         <p>              <b>Example: </b> id,name           </p>         <p>              <b>Default Value: </b> By default all fields mentioned below will be used to search by term           </p>         <h4>Available Fields</h4><ul><li>id</li> <li>name</li></ul>
  /// * [includes] - List of relations to select.     <p>              <b>Example: </b> versions,versions.file           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [PlansControllerList200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<PlansControllerList200Response>> plansControllerList({
    String authorization = '{{token}}',
    num? page,
    num? limit,
    BuiltList<String>? filterPeriodId,
    BuiltList<String>? filterPeriodName,
    BuiltList<String>? filterPeriodDescription,
    BuiltList<String>? filterPeriodVersionPeriodName,
    BuiltList<String>? sortBy,
    String? search,
    String? searchBy,
    String? includes,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/plans';
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _queryParameters = <String, dynamic>{
      if (page != null)
        r'page': encodeQueryParameter(_serializers, page, const FullType(num)),
      if (limit != null)
        r'limit':
            encodeQueryParameter(_serializers, limit, const FullType(num)),
      if (filterPeriodId != null)
        r'filter.id': encodeCollectionQueryParameter<String>(
          _serializers,
          filterPeriodId,
          const FullType(BuiltList, [FullType(String)]),
          format: ListFormat.multi,
        ),
      if (filterPeriodName != null)
        r'filter.name': encodeCollectionQueryParameter<String>(
          _serializers,
          filterPeriodName,
          const FullType(BuiltList, [FullType(String)]),
          format: ListFormat.multi,
        ),
      if (filterPeriodDescription != null)
        r'filter.description': encodeCollectionQueryParameter<String>(
          _serializers,
          filterPeriodDescription,
          const FullType(BuiltList, [FullType(String)]),
          format: ListFormat.multi,
        ),
      if (filterPeriodVersionPeriodName != null)
        r'filter.version.name': encodeCollectionQueryParameter<String>(
          _serializers,
          filterPeriodVersionPeriodName,
          const FullType(BuiltList, [FullType(String)]),
          format: ListFormat.multi,
        ),
      if (sortBy != null)
        r'sortBy': encodeCollectionQueryParameter<String>(
          _serializers,
          sortBy,
          const FullType(BuiltList, [FullType(String)]),
          format: ListFormat.multi,
        ),
      if (search != null)
        r'search':
            encodeQueryParameter(_serializers, search, const FullType(String)),
      if (searchBy != null)
        r'searchBy': encodeQueryParameter(
            _serializers, searchBy, const FullType(String)),
      if (includes != null)
        r'includes': encodeQueryParameter(
            _serializers, includes, const FullType(String)),
    };

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      queryParameters: _queryParameters,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    PlansControllerList200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(PlansControllerList200Response),
            ) as PlansControllerList200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<PlansControllerList200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// plansControllerUpdate
  ///
  ///
  /// Parameters:
  /// * [id]
  /// * [authorization] - Bearer {token}
  /// * [planUpdateRequest]
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future]
  /// Throws [DioException] if API call or serialization fails
  Future<Response<void>> plansControllerUpdate({
    required String id,
    String authorization = '{{token}}',
    required PlanUpdateRequest planUpdateRequest,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/plans/{id}'.replaceAll(
        '{' r'id' '}',
        encodeQueryParameter(_serializers, id, const FullType(String))
            .toString());
    final _options = Options(
      method: r'PATCH',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      contentType: 'application/json',
      validateStatus: validateStatus,
    );

    dynamic _bodyData;

    try {
      const _type = FullType(PlanUpdateRequest);
      _bodyData =
          _serializers.serialize(planUpdateRequest, specifiedType: _type);
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _options.compose(
          _dio.options,
          _path,
        ),
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    final _response = await _dio.request<Object>(
      _path,
      data: _bodyData,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    return _response;
  }
}
