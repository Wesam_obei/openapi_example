//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'dart:async';

import 'package:built_value/serializer.dart';
import 'package:dio/dio.dart';

import 'package:built_collection/built_collection.dart';
import 'package:qarar_api/src/api_util.dart';
import 'package:qarar_api/src/model/activity_log_controller_last_login200_response.dart';
import 'package:qarar_api/src/model/activity_log_controller_user_activity200_response.dart';
import 'package:qarar_api/src/model/error_response.dart';

class ActivityLogApi {
  final Dio _dio;

  final Serializers _serializers;

  const ActivityLogApi(this._dio, this._serializers);

  /// activityLogControllerLastLogin
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [ActivityLogControllerLastLogin200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<ActivityLogControllerLastLogin200Response>>
      activityLogControllerLastLogin({
    String authorization = '{{token}}',
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/activity-log/last-login';
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    ActivityLogControllerLastLogin200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType:
                  const FullType(ActivityLogControllerLastLogin200Response),
            ) as ActivityLogControllerLastLogin200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<ActivityLogControllerLastLogin200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// activityLogControllerUserActivity
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [page] - Page number to retrieve.If you provide invalid value the default page number will applied         <p>              <b>Example: </b> 1           </p>         <p>              <b>Default Value: </b> 1           </p>
  /// * [limit] - Number of records per page.       <p>              <b>Example: </b> 10           </p>       <p>              <b>Default Value: </b> 20           </p>       <p>              <b>Max Value: </b> 100           </p>               If provided value is greater than max value, max value will be applied.
  /// * [filterPeriodUserPeriodId] - Filter by user.id query param.           <p>              <b>Format: </b> filter.user.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.user.id=$not:$like:John Doe&filter.user.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$in</li> <li>$not</li></ul>
  /// * [filterPeriodUserPeriodFullName] - Filter by user.full_name query param.           <p>              <b>Format: </b> filter.user.full_name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.user.full_name=$not:$like:John Doe&filter.user.full_name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul>
  /// * [filterPeriodUserPeriodEmail] - Filter by user.email query param.           <p>              <b>Format: </b> filter.user.email={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.user.email=$not:$like:John Doe&filter.user.email=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul>
  /// * [filterPeriodEventPeriodName] - Filter by event.name query param.           <p>              <b>Format: </b> filter.event.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.event.name=$not:$like:John Doe&filter.event.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul>
  /// * [sortBy] - Parameter to sort by.       <p>To sort by multiple fields, just provide query param multiple types. The order in url defines an order of sorting</p>       <p>              <b>Format: </b> fieldName:DIRECTION           </p>       <p>              <b>Example: </b> sortBy=id:DESC&sortBy=createdAt:ASC           </p>       <p>              <b>Default Value: </b> id:DESC           </p>       <h4>Available Fields</h4><ul><li>id</li> <li>event_id</li></ul>
  /// * [search] - Search term to filter result values         <p>              <b>Example: </b> John           </p>         <p>              <b>Default Value: </b> No default value           </p>
  /// * [searchBy] - List of fields to search by term to filter result values         <p>              <b>Example: </b> id,event_id           </p>         <p>              <b>Default Value: </b> By default all fields mentioned below will be used to search by term           </p>         <h4>Available Fields</h4><ul><li>id</li> <li>event_id</li></ul>
  /// * [includes] - List of relations to select.     <p>              <b>Example: </b> user,event           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [ActivityLogControllerUserActivity200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<ActivityLogControllerUserActivity200Response>>
      activityLogControllerUserActivity({
    String authorization = '{{token}}',
    num? page,
    num? limit,
    BuiltList<String>? filterPeriodUserPeriodId,
    BuiltList<String>? filterPeriodUserPeriodFullName,
    BuiltList<String>? filterPeriodUserPeriodEmail,
    BuiltList<String>? filterPeriodEventPeriodName,
    BuiltList<String>? sortBy,
    String? search,
    String? searchBy,
    String? includes,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/activity-log/user-activity';
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _queryParameters = <String, dynamic>{
      if (page != null)
        r'page': encodeQueryParameter(_serializers, page, const FullType(num)),
      if (limit != null)
        r'limit':
            encodeQueryParameter(_serializers, limit, const FullType(num)),
      if (filterPeriodUserPeriodId != null)
        r'filter.user.id': encodeCollectionQueryParameter<String>(
          _serializers,
          filterPeriodUserPeriodId,
          const FullType(BuiltList, [FullType(String)]),
          format: ListFormat.multi,
        ),
      if (filterPeriodUserPeriodFullName != null)
        r'filter.user.full_name': encodeCollectionQueryParameter<String>(
          _serializers,
          filterPeriodUserPeriodFullName,
          const FullType(BuiltList, [FullType(String)]),
          format: ListFormat.multi,
        ),
      if (filterPeriodUserPeriodEmail != null)
        r'filter.user.email': encodeCollectionQueryParameter<String>(
          _serializers,
          filterPeriodUserPeriodEmail,
          const FullType(BuiltList, [FullType(String)]),
          format: ListFormat.multi,
        ),
      if (filterPeriodEventPeriodName != null)
        r'filter.event.name': encodeCollectionQueryParameter<String>(
          _serializers,
          filterPeriodEventPeriodName,
          const FullType(BuiltList, [FullType(String)]),
          format: ListFormat.multi,
        ),
      if (sortBy != null)
        r'sortBy': encodeCollectionQueryParameter<String>(
          _serializers,
          sortBy,
          const FullType(BuiltList, [FullType(String)]),
          format: ListFormat.multi,
        ),
      if (search != null)
        r'search':
            encodeQueryParameter(_serializers, search, const FullType(String)),
      if (searchBy != null)
        r'searchBy': encodeQueryParameter(
            _serializers, searchBy, const FullType(String)),
      if (includes != null)
        r'includes': encodeQueryParameter(
            _serializers, includes, const FullType(String)),
    };

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      queryParameters: _queryParameters,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    ActivityLogControllerUserActivity200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType:
                  const FullType(ActivityLogControllerUserActivity200Response),
            ) as ActivityLogControllerUserActivity200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<ActivityLogControllerUserActivity200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// activityLogControllerUsersActivity
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [page] - Page number to retrieve.If you provide invalid value the default page number will applied         <p>              <b>Example: </b> 1           </p>         <p>              <b>Default Value: </b> 1           </p>
  /// * [limit] - Number of records per page.       <p>              <b>Example: </b> 10           </p>       <p>              <b>Default Value: </b> 20           </p>       <p>              <b>Max Value: </b> 100           </p>               If provided value is greater than max value, max value will be applied.
  /// * [filterPeriodUserPeriodId] - Filter by user.id query param.           <p>              <b>Format: </b> filter.user.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.user.id=$not:$like:John Doe&filter.user.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$in</li> <li>$not</li></ul>
  /// * [filterPeriodUserPeriodFullName] - Filter by user.full_name query param.           <p>              <b>Format: </b> filter.user.full_name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.user.full_name=$not:$like:John Doe&filter.user.full_name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul>
  /// * [filterPeriodUserPeriodEmail] - Filter by user.email query param.           <p>              <b>Format: </b> filter.user.email={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.user.email=$not:$like:John Doe&filter.user.email=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul>
  /// * [filterPeriodEventPeriodName] - Filter by event.name query param.           <p>              <b>Format: </b> filter.event.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.event.name=$not:$like:John Doe&filter.event.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul>
  /// * [sortBy] - Parameter to sort by.       <p>To sort by multiple fields, just provide query param multiple types. The order in url defines an order of sorting</p>       <p>              <b>Format: </b> fieldName:DIRECTION           </p>       <p>              <b>Example: </b> sortBy=id:DESC&sortBy=createdAt:ASC           </p>       <p>              <b>Default Value: </b> id:DESC           </p>       <h4>Available Fields</h4><ul><li>id</li> <li>event_id</li></ul>
  /// * [search] - Search term to filter result values         <p>              <b>Example: </b> John           </p>         <p>              <b>Default Value: </b> No default value           </p>
  /// * [searchBy] - List of fields to search by term to filter result values         <p>              <b>Example: </b> id,event_id           </p>         <p>              <b>Default Value: </b> By default all fields mentioned below will be used to search by term           </p>         <h4>Available Fields</h4><ul><li>id</li> <li>event_id</li></ul>
  /// * [includes] - List of relations to select.     <p>              <b>Example: </b> user,event           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [ActivityLogControllerUserActivity200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<ActivityLogControllerUserActivity200Response>>
      activityLogControllerUsersActivity({
    String authorization = '{{token}}',
    num? page,
    num? limit,
    BuiltList<String>? filterPeriodUserPeriodId,
    BuiltList<String>? filterPeriodUserPeriodFullName,
    BuiltList<String>? filterPeriodUserPeriodEmail,
    BuiltList<String>? filterPeriodEventPeriodName,
    BuiltList<String>? sortBy,
    String? search,
    String? searchBy,
    String? includes,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/activity-log/users-activity';
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _queryParameters = <String, dynamic>{
      if (page != null)
        r'page': encodeQueryParameter(_serializers, page, const FullType(num)),
      if (limit != null)
        r'limit':
            encodeQueryParameter(_serializers, limit, const FullType(num)),
      if (filterPeriodUserPeriodId != null)
        r'filter.user.id': encodeCollectionQueryParameter<String>(
          _serializers,
          filterPeriodUserPeriodId,
          const FullType(BuiltList, [FullType(String)]),
          format: ListFormat.multi,
        ),
      if (filterPeriodUserPeriodFullName != null)
        r'filter.user.full_name': encodeCollectionQueryParameter<String>(
          _serializers,
          filterPeriodUserPeriodFullName,
          const FullType(BuiltList, [FullType(String)]),
          format: ListFormat.multi,
        ),
      if (filterPeriodUserPeriodEmail != null)
        r'filter.user.email': encodeCollectionQueryParameter<String>(
          _serializers,
          filterPeriodUserPeriodEmail,
          const FullType(BuiltList, [FullType(String)]),
          format: ListFormat.multi,
        ),
      if (filterPeriodEventPeriodName != null)
        r'filter.event.name': encodeCollectionQueryParameter<String>(
          _serializers,
          filterPeriodEventPeriodName,
          const FullType(BuiltList, [FullType(String)]),
          format: ListFormat.multi,
        ),
      if (sortBy != null)
        r'sortBy': encodeCollectionQueryParameter<String>(
          _serializers,
          sortBy,
          const FullType(BuiltList, [FullType(String)]),
          format: ListFormat.multi,
        ),
      if (search != null)
        r'search':
            encodeQueryParameter(_serializers, search, const FullType(String)),
      if (searchBy != null)
        r'searchBy': encodeQueryParameter(
            _serializers, searchBy, const FullType(String)),
      if (includes != null)
        r'includes': encodeQueryParameter(
            _serializers, includes, const FullType(String)),
    };

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      queryParameters: _queryParameters,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    ActivityLogControllerUserActivity200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType:
                  const FullType(ActivityLogControllerUserActivity200Response),
            ) as ActivityLogControllerUserActivity200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<ActivityLogControllerUserActivity200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }
}
