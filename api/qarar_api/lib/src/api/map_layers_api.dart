//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'dart:async';

import 'package:built_value/serializer.dart';
import 'package:dio/dio.dart';

import 'package:qarar_api/src/api_util.dart';
import 'package:qarar_api/src/model/base_data_request.dart';
import 'package:qarar_api/src/model/error_response.dart';
import 'package:qarar_api/src/model/incident_reports_data_request.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_bridges_tunnels_sites_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_cables_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_construction_licenses_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_district_boundaries_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_electrical_facilities_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_incident_reports_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_land_base_parcel_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_light_poles_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_manhole_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_municipalities_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_plan_data_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_sea_out_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_storage_locations_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_street_names_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_transport_tracks_map200_response.dart';
import 'package:qarar_api/src/model/map_layers_controller_get_urban_area_boundaries_map200_response.dart';

class MapLayersApi {
  final Dio _dio;

  final Serializers _serializers;

  const MapLayersApi(this._dio, this._serializers);

  /// mapLayersControllerGetBridgesTunnelsSitesFeature
  ///
  ///
  /// Parameters:
  /// * [id]
  /// * [authorization] - Bearer {token}
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetBridgesTunnelsSitesMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetBridgesTunnelsSitesMap200Response>>
      mapLayersControllerGetBridgesTunnelsSitesFeature({
    required String id,
    String authorization = '{{token}}',
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/bridges-tunnels-sites/feature/{id}'
        .replaceAll(
            '{' r'id' '}',
            encodeQueryParameter(_serializers, id, const FullType(String))
                .toString());
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetBridgesTunnelsSitesMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetBridgesTunnelsSitesMap200Response),
            ) as MapLayersControllerGetBridgesTunnelsSitesMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetBridgesTunnelsSitesMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetBridgesTunnelsSitesMap
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [baseDataRequest]
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetBridgesTunnelsSitesMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetBridgesTunnelsSitesMap200Response>>
      mapLayersControllerGetBridgesTunnelsSitesMap({
    String authorization = '{{token}}',
    required BaseDataRequest baseDataRequest,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/bridges-tunnels-sites';
    final _options = Options(
      method: r'POST',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      contentType: 'application/json',
      validateStatus: validateStatus,
    );

    dynamic _bodyData;

    try {
      const _type = FullType(BaseDataRequest);
      _bodyData = _serializers.serialize(baseDataRequest, specifiedType: _type);
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _options.compose(
          _dio.options,
          _path,
        ),
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    final _response = await _dio.request<Object>(
      _path,
      data: _bodyData,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetBridgesTunnelsSitesMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetBridgesTunnelsSitesMap200Response),
            ) as MapLayersControllerGetBridgesTunnelsSitesMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetBridgesTunnelsSitesMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetCablesFeature
  ///
  ///
  /// Parameters:
  /// * [id]
  /// * [authorization] - Bearer {token}
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetCablesMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetCablesMap200Response>>
      mapLayersControllerGetCablesFeature({
    required String id,
    String authorization = '{{token}}',
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/cables/feature/{id}'.replaceAll(
        '{' r'id' '}',
        encodeQueryParameter(_serializers, id, const FullType(String))
            .toString());
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetCablesMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType:
                  const FullType(MapLayersControllerGetCablesMap200Response),
            ) as MapLayersControllerGetCablesMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetCablesMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetCablesMap
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [baseDataRequest]
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetCablesMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetCablesMap200Response>>
      mapLayersControllerGetCablesMap({
    String authorization = '{{token}}',
    required BaseDataRequest baseDataRequest,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/cables';
    final _options = Options(
      method: r'POST',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      contentType: 'application/json',
      validateStatus: validateStatus,
    );

    dynamic _bodyData;

    try {
      const _type = FullType(BaseDataRequest);
      _bodyData = _serializers.serialize(baseDataRequest, specifiedType: _type);
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _options.compose(
          _dio.options,
          _path,
        ),
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    final _response = await _dio.request<Object>(
      _path,
      data: _bodyData,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetCablesMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType:
                  const FullType(MapLayersControllerGetCablesMap200Response),
            ) as MapLayersControllerGetCablesMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetCablesMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetConstructionLicensesFeature
  ///
  ///
  /// Parameters:
  /// * [id]
  /// * [authorization] - Bearer {token}
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetConstructionLicensesMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetConstructionLicensesMap200Response>>
      mapLayersControllerGetConstructionLicensesFeature({
    required String id,
    String authorization = '{{token}}',
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/construction-licenses/feature/{id}'
        .replaceAll(
            '{' r'id' '}',
            encodeQueryParameter(_serializers, id, const FullType(String))
                .toString());
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetConstructionLicensesMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetConstructionLicensesMap200Response),
            ) as MapLayersControllerGetConstructionLicensesMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetConstructionLicensesMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetConstructionLicensesMap
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [baseDataRequest]
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetConstructionLicensesMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetConstructionLicensesMap200Response>>
      mapLayersControllerGetConstructionLicensesMap({
    String authorization = '{{token}}',
    required BaseDataRequest baseDataRequest,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/construction-licenses';
    final _options = Options(
      method: r'POST',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      contentType: 'application/json',
      validateStatus: validateStatus,
    );

    dynamic _bodyData;

    try {
      const _type = FullType(BaseDataRequest);
      _bodyData = _serializers.serialize(baseDataRequest, specifiedType: _type);
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _options.compose(
          _dio.options,
          _path,
        ),
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    final _response = await _dio.request<Object>(
      _path,
      data: _bodyData,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetConstructionLicensesMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetConstructionLicensesMap200Response),
            ) as MapLayersControllerGetConstructionLicensesMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetConstructionLicensesMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetCriticalPointsFeature
  ///
  ///
  /// Parameters:
  /// * [id]
  /// * [authorization] - Bearer {token}
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetStorageLocationsMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetStorageLocationsMap200Response>>
      mapLayersControllerGetCriticalPointsFeature({
    required String id,
    String authorization = '{{token}}',
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/critical-points/feature/{id}'.replaceAll(
        '{' r'id' '}',
        encodeQueryParameter(_serializers, id, const FullType(String))
            .toString());
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetStorageLocationsMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetStorageLocationsMap200Response),
            ) as MapLayersControllerGetStorageLocationsMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetStorageLocationsMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetCriticalPointsMap
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [baseDataRequest]
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetStorageLocationsMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetStorageLocationsMap200Response>>
      mapLayersControllerGetCriticalPointsMap({
    String authorization = '{{token}}',
    required BaseDataRequest baseDataRequest,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/critical-points';
    final _options = Options(
      method: r'POST',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      contentType: 'application/json',
      validateStatus: validateStatus,
    );

    dynamic _bodyData;

    try {
      const _type = FullType(BaseDataRequest);
      _bodyData = _serializers.serialize(baseDataRequest, specifiedType: _type);
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _options.compose(
          _dio.options,
          _path,
        ),
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    final _response = await _dio.request<Object>(
      _path,
      data: _bodyData,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetStorageLocationsMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetStorageLocationsMap200Response),
            ) as MapLayersControllerGetStorageLocationsMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetStorageLocationsMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetDistrictBoundariesFeature
  ///
  ///
  /// Parameters:
  /// * [id]
  /// * [authorization] - Bearer {token}
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetDistrictBoundariesMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetDistrictBoundariesMap200Response>>
      mapLayersControllerGetDistrictBoundariesFeature({
    required String id,
    String authorization = '{{token}}',
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/district-boundary/feature/{id}'
        .replaceAll(
            '{' r'id' '}',
            encodeQueryParameter(_serializers, id, const FullType(String))
                .toString());
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetDistrictBoundariesMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetDistrictBoundariesMap200Response),
            ) as MapLayersControllerGetDistrictBoundariesMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetDistrictBoundariesMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetDistrictBoundariesMap
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [baseDataRequest]
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetDistrictBoundariesMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetDistrictBoundariesMap200Response>>
      mapLayersControllerGetDistrictBoundariesMap({
    String authorization = '{{token}}',
    required BaseDataRequest baseDataRequest,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/district-boundary';
    final _options = Options(
      method: r'POST',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      contentType: 'application/json',
      validateStatus: validateStatus,
    );

    dynamic _bodyData;

    try {
      const _type = FullType(BaseDataRequest);
      _bodyData = _serializers.serialize(baseDataRequest, specifiedType: _type);
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _options.compose(
          _dio.options,
          _path,
        ),
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    final _response = await _dio.request<Object>(
      _path,
      data: _bodyData,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetDistrictBoundariesMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetDistrictBoundariesMap200Response),
            ) as MapLayersControllerGetDistrictBoundariesMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetDistrictBoundariesMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetElectricalFacilitiesFeature
  ///
  ///
  /// Parameters:
  /// * [id]
  /// * [authorization] - Bearer {token}
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetElectricalFacilitiesMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetElectricalFacilitiesMap200Response>>
      mapLayersControllerGetElectricalFacilitiesFeature({
    required String id,
    String authorization = '{{token}}',
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/electrical-facilities/feature/{id}'
        .replaceAll(
            '{' r'id' '}',
            encodeQueryParameter(_serializers, id, const FullType(String))
                .toString());
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetElectricalFacilitiesMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetElectricalFacilitiesMap200Response),
            ) as MapLayersControllerGetElectricalFacilitiesMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetElectricalFacilitiesMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetElectricalFacilitiesMap
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [baseDataRequest]
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetElectricalFacilitiesMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetElectricalFacilitiesMap200Response>>
      mapLayersControllerGetElectricalFacilitiesMap({
    String authorization = '{{token}}',
    required BaseDataRequest baseDataRequest,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/electrical-facilities';
    final _options = Options(
      method: r'POST',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      contentType: 'application/json',
      validateStatus: validateStatus,
    );

    dynamic _bodyData;

    try {
      const _type = FullType(BaseDataRequest);
      _bodyData = _serializers.serialize(baseDataRequest, specifiedType: _type);
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _options.compose(
          _dio.options,
          _path,
        ),
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    final _response = await _dio.request<Object>(
      _path,
      data: _bodyData,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetElectricalFacilitiesMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetElectricalFacilitiesMap200Response),
            ) as MapLayersControllerGetElectricalFacilitiesMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetElectricalFacilitiesMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetIncidentReportsMap
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [incidentReportsDataRequest]
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetIncidentReportsMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetIncidentReportsMap200Response>>
      mapLayersControllerGetIncidentReportsMap({
    String authorization = '{{token}}',
    required IncidentReportsDataRequest incidentReportsDataRequest,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/incidents940';
    final _options = Options(
      method: r'POST',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      contentType: 'application/json',
      validateStatus: validateStatus,
    );

    dynamic _bodyData;

    try {
      const _type = FullType(IncidentReportsDataRequest);
      _bodyData = _serializers.serialize(incidentReportsDataRequest,
          specifiedType: _type);
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _options.compose(
          _dio.options,
          _path,
        ),
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    final _response = await _dio.request<Object>(
      _path,
      data: _bodyData,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetIncidentReportsMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetIncidentReportsMap200Response),
            ) as MapLayersControllerGetIncidentReportsMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetIncidentReportsMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetIncidentsFeature
  ///
  ///
  /// Parameters:
  /// * [id]
  /// * [authorization] - Bearer {token}
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetIncidentReportsMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetIncidentReportsMap200Response>>
      mapLayersControllerGetIncidentsFeature({
    required String id,
    String authorization = '{{token}}',
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/incidents940/feature/{id}'.replaceAll(
        '{' r'id' '}',
        encodeQueryParameter(_serializers, id, const FullType(String))
            .toString());
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetIncidentReportsMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetIncidentReportsMap200Response),
            ) as MapLayersControllerGetIncidentReportsMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetIncidentReportsMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetLandBaseParcelFeature
  ///
  ///
  /// Parameters:
  /// * [id]
  /// * [authorization] - Bearer {token}
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetLandBaseParcelMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetLandBaseParcelMap200Response>>
      mapLayersControllerGetLandBaseParcelFeature({
    required String id,
    String authorization = '{{token}}',
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/land-base-parcel/feature/{id}'
        .replaceAll(
            '{' r'id' '}',
            encodeQueryParameter(_serializers, id, const FullType(String))
                .toString());
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetLandBaseParcelMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetLandBaseParcelMap200Response),
            ) as MapLayersControllerGetLandBaseParcelMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetLandBaseParcelMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetLandBaseParcelMap
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [baseDataRequest]
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetLandBaseParcelMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetLandBaseParcelMap200Response>>
      mapLayersControllerGetLandBaseParcelMap({
    String authorization = '{{token}}',
    required BaseDataRequest baseDataRequest,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/land-base-parcel';
    final _options = Options(
      method: r'POST',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      contentType: 'application/json',
      validateStatus: validateStatus,
    );

    dynamic _bodyData;

    try {
      const _type = FullType(BaseDataRequest);
      _bodyData = _serializers.serialize(baseDataRequest, specifiedType: _type);
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _options.compose(
          _dio.options,
          _path,
        ),
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    final _response = await _dio.request<Object>(
      _path,
      data: _bodyData,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetLandBaseParcelMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetLandBaseParcelMap200Response),
            ) as MapLayersControllerGetLandBaseParcelMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetLandBaseParcelMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetLightPolesFeature
  ///
  ///
  /// Parameters:
  /// * [id]
  /// * [authorization] - Bearer {token}
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetLightPolesMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetLightPolesMap200Response>>
      mapLayersControllerGetLightPolesFeature({
    required String id,
    String authorization = '{{token}}',
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/light-poles/feature/{id}'.replaceAll(
        '{' r'id' '}',
        encodeQueryParameter(_serializers, id, const FullType(String))
            .toString());
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetLightPolesMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetLightPolesMap200Response),
            ) as MapLayersControllerGetLightPolesMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetLightPolesMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetLightPolesMap
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [baseDataRequest]
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetLightPolesMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetLightPolesMap200Response>>
      mapLayersControllerGetLightPolesMap({
    String authorization = '{{token}}',
    required BaseDataRequest baseDataRequest,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/light-poles';
    final _options = Options(
      method: r'POST',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      contentType: 'application/json',
      validateStatus: validateStatus,
    );

    dynamic _bodyData;

    try {
      const _type = FullType(BaseDataRequest);
      _bodyData = _serializers.serialize(baseDataRequest, specifiedType: _type);
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _options.compose(
          _dio.options,
          _path,
        ),
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    final _response = await _dio.request<Object>(
      _path,
      data: _bodyData,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetLightPolesMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetLightPolesMap200Response),
            ) as MapLayersControllerGetLightPolesMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetLightPolesMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetManholeFeature
  ///
  ///
  /// Parameters:
  /// * [id]
  /// * [authorization] - Bearer {token}
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetManholeMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetManholeMap200Response>>
      mapLayersControllerGetManholeFeature({
    required String id,
    String authorization = '{{token}}',
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/manhole/feature/{id}'.replaceAll(
        '{' r'id' '}',
        encodeQueryParameter(_serializers, id, const FullType(String))
            .toString());
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetManholeMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType:
                  const FullType(MapLayersControllerGetManholeMap200Response),
            ) as MapLayersControllerGetManholeMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetManholeMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetManholeMap
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [baseDataRequest]
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetManholeMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetManholeMap200Response>>
      mapLayersControllerGetManholeMap({
    String authorization = '{{token}}',
    required BaseDataRequest baseDataRequest,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/manhole';
    final _options = Options(
      method: r'POST',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      contentType: 'application/json',
      validateStatus: validateStatus,
    );

    dynamic _bodyData;

    try {
      const _type = FullType(BaseDataRequest);
      _bodyData = _serializers.serialize(baseDataRequest, specifiedType: _type);
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _options.compose(
          _dio.options,
          _path,
        ),
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    final _response = await _dio.request<Object>(
      _path,
      data: _bodyData,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetManholeMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType:
                  const FullType(MapLayersControllerGetManholeMap200Response),
            ) as MapLayersControllerGetManholeMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetManholeMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetMunicipalitiesFeature
  ///
  ///
  /// Parameters:
  /// * [id]
  /// * [authorization] - Bearer {token}
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetMunicipalitiesMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetMunicipalitiesMap200Response>>
      mapLayersControllerGetMunicipalitiesFeature({
    required String id,
    String authorization = '{{token}}',
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/municipalities/feature/{id}'.replaceAll(
        '{' r'id' '}',
        encodeQueryParameter(_serializers, id, const FullType(String))
            .toString());
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetMunicipalitiesMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetMunicipalitiesMap200Response),
            ) as MapLayersControllerGetMunicipalitiesMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetMunicipalitiesMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetMunicipalitiesMap
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [baseDataRequest]
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetMunicipalitiesMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetMunicipalitiesMap200Response>>
      mapLayersControllerGetMunicipalitiesMap({
    String authorization = '{{token}}',
    required BaseDataRequest baseDataRequest,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/municipalities';
    final _options = Options(
      method: r'POST',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      contentType: 'application/json',
      validateStatus: validateStatus,
    );

    dynamic _bodyData;

    try {
      const _type = FullType(BaseDataRequest);
      _bodyData = _serializers.serialize(baseDataRequest, specifiedType: _type);
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _options.compose(
          _dio.options,
          _path,
        ),
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    final _response = await _dio.request<Object>(
      _path,
      data: _bodyData,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetMunicipalitiesMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetMunicipalitiesMap200Response),
            ) as MapLayersControllerGetMunicipalitiesMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetMunicipalitiesMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetPlanDataFeature
  ///
  ///
  /// Parameters:
  /// * [id]
  /// * [authorization] - Bearer {token}
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetPlanDataMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetPlanDataMap200Response>>
      mapLayersControllerGetPlanDataFeature({
    required String id,
    String authorization = '{{token}}',
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/plan-data/feature/{id}'.replaceAll(
        '{' r'id' '}',
        encodeQueryParameter(_serializers, id, const FullType(String))
            .toString());
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetPlanDataMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType:
                  const FullType(MapLayersControllerGetPlanDataMap200Response),
            ) as MapLayersControllerGetPlanDataMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetPlanDataMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetPlanDataMap
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [baseDataRequest]
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetPlanDataMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetPlanDataMap200Response>>
      mapLayersControllerGetPlanDataMap({
    String authorization = '{{token}}',
    required BaseDataRequest baseDataRequest,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/plan-data';
    final _options = Options(
      method: r'POST',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      contentType: 'application/json',
      validateStatus: validateStatus,
    );

    dynamic _bodyData;

    try {
      const _type = FullType(BaseDataRequest);
      _bodyData = _serializers.serialize(baseDataRequest, specifiedType: _type);
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _options.compose(
          _dio.options,
          _path,
        ),
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    final _response = await _dio.request<Object>(
      _path,
      data: _bodyData,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetPlanDataMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType:
                  const FullType(MapLayersControllerGetPlanDataMap200Response),
            ) as MapLayersControllerGetPlanDataMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetPlanDataMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetResponseFeature
  ///
  ///
  /// Parameters:
  /// * [id]
  /// * [authorization] - Bearer {token}
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetStorageLocationsMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetStorageLocationsMap200Response>>
      mapLayersControllerGetResponseFeature({
    required String id,
    String authorization = '{{token}}',
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/response/feature/{id}'.replaceAll(
        '{' r'id' '}',
        encodeQueryParameter(_serializers, id, const FullType(String))
            .toString());
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetStorageLocationsMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetStorageLocationsMap200Response),
            ) as MapLayersControllerGetStorageLocationsMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetStorageLocationsMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetResponseMap
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [baseDataRequest]
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetStorageLocationsMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetStorageLocationsMap200Response>>
      mapLayersControllerGetResponseMap({
    String authorization = '{{token}}',
    required BaseDataRequest baseDataRequest,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/response';
    final _options = Options(
      method: r'POST',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      contentType: 'application/json',
      validateStatus: validateStatus,
    );

    dynamic _bodyData;

    try {
      const _type = FullType(BaseDataRequest);
      _bodyData = _serializers.serialize(baseDataRequest, specifiedType: _type);
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _options.compose(
          _dio.options,
          _path,
        ),
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    final _response = await _dio.request<Object>(
      _path,
      data: _bodyData,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetStorageLocationsMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetStorageLocationsMap200Response),
            ) as MapLayersControllerGetStorageLocationsMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetStorageLocationsMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetSeaOutFeature
  ///
  ///
  /// Parameters:
  /// * [id]
  /// * [authorization] - Bearer {token}
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetSeaOutMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetSeaOutMap200Response>>
      mapLayersControllerGetSeaOutFeature({
    required String id,
    String authorization = '{{token}}',
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/sea-out/feature/{id}'.replaceAll(
        '{' r'id' '}',
        encodeQueryParameter(_serializers, id, const FullType(String))
            .toString());
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetSeaOutMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType:
                  const FullType(MapLayersControllerGetSeaOutMap200Response),
            ) as MapLayersControllerGetSeaOutMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetSeaOutMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetSeaOutMap
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [baseDataRequest]
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetSeaOutMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetSeaOutMap200Response>>
      mapLayersControllerGetSeaOutMap({
    String authorization = '{{token}}',
    required BaseDataRequest baseDataRequest,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/sea-out';
    final _options = Options(
      method: r'POST',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      contentType: 'application/json',
      validateStatus: validateStatus,
    );

    dynamic _bodyData;

    try {
      const _type = FullType(BaseDataRequest);
      _bodyData = _serializers.serialize(baseDataRequest, specifiedType: _type);
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _options.compose(
          _dio.options,
          _path,
        ),
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    final _response = await _dio.request<Object>(
      _path,
      data: _bodyData,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetSeaOutMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType:
                  const FullType(MapLayersControllerGetSeaOutMap200Response),
            ) as MapLayersControllerGetSeaOutMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetSeaOutMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetStorageLocationsFeature
  ///
  ///
  /// Parameters:
  /// * [id]
  /// * [authorization] - Bearer {token}
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetStorageLocationsMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetStorageLocationsMap200Response>>
      mapLayersControllerGetStorageLocationsFeature({
    required String id,
    String authorization = '{{token}}',
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/storage-locations/feature/{id}'
        .replaceAll(
            '{' r'id' '}',
            encodeQueryParameter(_serializers, id, const FullType(String))
                .toString());
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetStorageLocationsMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetStorageLocationsMap200Response),
            ) as MapLayersControllerGetStorageLocationsMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetStorageLocationsMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetStorageLocationsMap
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [baseDataRequest]
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetStorageLocationsMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetStorageLocationsMap200Response>>
      mapLayersControllerGetStorageLocationsMap({
    String authorization = '{{token}}',
    required BaseDataRequest baseDataRequest,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/storage-locations';
    final _options = Options(
      method: r'POST',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      contentType: 'application/json',
      validateStatus: validateStatus,
    );

    dynamic _bodyData;

    try {
      const _type = FullType(BaseDataRequest);
      _bodyData = _serializers.serialize(baseDataRequest, specifiedType: _type);
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _options.compose(
          _dio.options,
          _path,
        ),
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    final _response = await _dio.request<Object>(
      _path,
      data: _bodyData,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetStorageLocationsMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetStorageLocationsMap200Response),
            ) as MapLayersControllerGetStorageLocationsMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetStorageLocationsMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetStreetNamesFeature
  ///
  ///
  /// Parameters:
  /// * [id]
  /// * [authorization] - Bearer {token}
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetStreetNamesMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetStreetNamesMap200Response>>
      mapLayersControllerGetStreetNamesFeature({
    required String id,
    String authorization = '{{token}}',
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/street-names/feature/{id}'.replaceAll(
        '{' r'id' '}',
        encodeQueryParameter(_serializers, id, const FullType(String))
            .toString());
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetStreetNamesMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetStreetNamesMap200Response),
            ) as MapLayersControllerGetStreetNamesMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetStreetNamesMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetStreetNamesMap
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [baseDataRequest]
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetStreetNamesMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetStreetNamesMap200Response>>
      mapLayersControllerGetStreetNamesMap({
    String authorization = '{{token}}',
    required BaseDataRequest baseDataRequest,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/street-names';
    final _options = Options(
      method: r'POST',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      contentType: 'application/json',
      validateStatus: validateStatus,
    );

    dynamic _bodyData;

    try {
      const _type = FullType(BaseDataRequest);
      _bodyData = _serializers.serialize(baseDataRequest, specifiedType: _type);
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _options.compose(
          _dio.options,
          _path,
        ),
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    final _response = await _dio.request<Object>(
      _path,
      data: _bodyData,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetStreetNamesMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetStreetNamesMap200Response),
            ) as MapLayersControllerGetStreetNamesMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetStreetNamesMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetTransportTracksFeature
  ///
  ///
  /// Parameters:
  /// * [id]
  /// * [authorization] - Bearer {token}
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetTransportTracksMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetTransportTracksMap200Response>>
      mapLayersControllerGetTransportTracksFeature({
    required String id,
    String authorization = '{{token}}',
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/transport-tracks/feature/{id}'
        .replaceAll(
            '{' r'id' '}',
            encodeQueryParameter(_serializers, id, const FullType(String))
                .toString());
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetTransportTracksMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetTransportTracksMap200Response),
            ) as MapLayersControllerGetTransportTracksMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetTransportTracksMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetTransportTracksMap
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [baseDataRequest]
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetTransportTracksMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetTransportTracksMap200Response>>
      mapLayersControllerGetTransportTracksMap({
    String authorization = '{{token}}',
    required BaseDataRequest baseDataRequest,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/transport-tracks';
    final _options = Options(
      method: r'POST',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      contentType: 'application/json',
      validateStatus: validateStatus,
    );

    dynamic _bodyData;

    try {
      const _type = FullType(BaseDataRequest);
      _bodyData = _serializers.serialize(baseDataRequest, specifiedType: _type);
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _options.compose(
          _dio.options,
          _path,
        ),
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    final _response = await _dio.request<Object>(
      _path,
      data: _bodyData,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetTransportTracksMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetTransportTracksMap200Response),
            ) as MapLayersControllerGetTransportTracksMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetTransportTracksMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetUrbanAreaBoundariesFeature
  ///
  ///
  /// Parameters:
  /// * [id]
  /// * [authorization] - Bearer {token}
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetUrbanAreaBoundariesMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetUrbanAreaBoundariesMap200Response>>
      mapLayersControllerGetUrbanAreaBoundariesFeature({
    required String id,
    String authorization = '{{token}}',
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/urban-area-boundary/feature/{id}'
        .replaceAll(
            '{' r'id' '}',
            encodeQueryParameter(_serializers, id, const FullType(String))
                .toString());
    final _options = Options(
      method: r'GET',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      validateStatus: validateStatus,
    );

    final _response = await _dio.request<Object>(
      _path,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetUrbanAreaBoundariesMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetUrbanAreaBoundariesMap200Response),
            ) as MapLayersControllerGetUrbanAreaBoundariesMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetUrbanAreaBoundariesMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }

  /// mapLayersControllerGetUrbanAreaBoundariesMap
  ///
  ///
  /// Parameters:
  /// * [authorization] - Bearer {token}
  /// * [baseDataRequest]
  /// * [xLanguage]
  /// * [contentType]
  /// * [cancelToken] - A [CancelToken] that can be used to cancel the operation
  /// * [headers] - Can be used to add additional headers to the request
  /// * [extras] - Can be used to add flags to the request
  /// * [validateStatus] - A [ValidateStatus] callback that can be used to determine request success based on the HTTP status of the response
  /// * [onSendProgress] - A [ProgressCallback] that can be used to get the send progress
  /// * [onReceiveProgress] - A [ProgressCallback] that can be used to get the receive progress
  ///
  /// Returns a [Future] containing a [Response] with a [MapLayersControllerGetUrbanAreaBoundariesMap200Response] as data
  /// Throws [DioException] if API call or serialization fails
  Future<Response<MapLayersControllerGetUrbanAreaBoundariesMap200Response>>
      mapLayersControllerGetUrbanAreaBoundariesMap({
    String authorization = '{{token}}',
    required BaseDataRequest baseDataRequest,
    JsonObject? xLanguage,
    JsonObject? contentType,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? extra,
    ValidateStatus? validateStatus,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final _path = r'/api/v1/map-layers/urban-area-boundary';
    final _options = Options(
      method: r'POST',
      headers: <String, dynamic>{
        r'Authorization': authorization,
        if (xLanguage != null) r'X-Language': xLanguage,
        if (contentType != null) r'Content-Type': contentType,
        ...?headers,
      },
      extra: <String, dynamic>{
        'secure': <Map<String, String>>[
          {
            'type': 'http',
            'scheme': 'bearer',
            'name': 'bearer',
          },
        ],
        ...?extra,
      },
      contentType: 'application/json',
      validateStatus: validateStatus,
    );

    dynamic _bodyData;

    try {
      const _type = FullType(BaseDataRequest);
      _bodyData = _serializers.serialize(baseDataRequest, specifiedType: _type);
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _options.compose(
          _dio.options,
          _path,
        ),
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    final _response = await _dio.request<Object>(
      _path,
      data: _bodyData,
      options: _options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

    MapLayersControllerGetUrbanAreaBoundariesMap200Response? _responseData;

    try {
      final rawResponse = _response.data;
      _responseData = rawResponse == null
          ? null
          : _serializers.deserialize(
              rawResponse,
              specifiedType: const FullType(
                  MapLayersControllerGetUrbanAreaBoundariesMap200Response),
            ) as MapLayersControllerGetUrbanAreaBoundariesMap200Response;
    } catch (error, stackTrace) {
      throw DioException(
        requestOptions: _response.requestOptions,
        response: _response,
        type: DioExceptionType.unknown,
        error: error,
        stackTrace: stackTrace,
      );
    }

    return Response<MapLayersControllerGetUrbanAreaBoundariesMap200Response>(
      data: _responseData,
      headers: _response.headers,
      isRedirect: _response.isRedirect,
      requestOptions: _response.requestOptions,
      redirects: _response.redirects,
      statusCode: _response.statusCode,
      statusMessage: _response.statusMessage,
      extra: _response.extra,
    );
  }
}
