import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

/// tests for FormsApi
void main() {
  final instance = QararApi().getFormsApi();

  group(FormsApi, () {
    //Future<FormsControllerCreate200Response> formsControllerCreate(String authorization, FormCreateRequest formCreateRequest, { JsonObject xLanguage, JsonObject contentType }) async
    test('test formsControllerCreate', () async {
      // TODO
    });

    //Future formsControllerDelete(String id, String authorization, { JsonObject xLanguage, JsonObject contentType }) async
    test('test formsControllerDelete', () async {
      // TODO
    });

    //Future formsControllerDuplicate(String authorization, FormDuplicateRequest formDuplicateRequest, { JsonObject xLanguage, JsonObject contentType }) async
    test('test formsControllerDuplicate', () async {
      // TODO
    });

    //Future<FormsControllerGet200Response> formsControllerGet(num id, String authorization, { String includes, JsonObject xLanguage, JsonObject contentType }) async
    test('test formsControllerGet', () async {
      // TODO
    });

    //Future<FormsControllerList200Response> formsControllerList(String authorization, { num page, num limit, BuiltList<String> filterPeriodId, BuiltList<String> filterPeriodName, BuiltList<String> filterPeriodDescription, BuiltList<String> filterPeriodPermissionPeriodName, BuiltList<String> sortBy, String search, String searchBy, String includes, JsonObject xLanguage, JsonObject contentType }) async
    test('test formsControllerList', () async {
      // TODO
    });

    //Future<FormsControllerUpdate200Response> formsControllerUpdate(num id, String authorization, FormUpdateRequest formUpdateRequest, { JsonObject xLanguage, JsonObject contentType }) async
    test('test formsControllerUpdate', () async {
      // TODO
    });
  });
}
