import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for SurveyCreateRequest
void main() {
  final instance = SurveyCreateRequestBuilder();
  // TODO add properties to the builder and call build()

  group(SurveyCreateRequest, () {
    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // num formId
    test('to test the property `formId`', () async {
      // TODO
    });

    // bool isActive
    test('to test the property `isActive`', () async {
      // TODO
    });

    // DateTime startAt
    test('to test the property `startAt`', () async {
      // TODO
    });

    // DateTime endAt
    test('to test the property `endAt`', () async {
      // TODO
    });

    // BuiltList<num> usersIds
    test('to test the property `usersIds`', () async {
      // TODO
    });

    // BuiltList<num> entitiesIds
    test('to test the property `entitiesIds`', () async {
      // TODO
    });

    // String description
    test('to test the property `description`', () async {
      // TODO
    });
  });
}
