import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for ListUserNotificationsResponseDataResponse
void main() {
  final instance = ListUserNotificationsResponseDataResponseBuilder();
  // TODO add properties to the builder and call build()

  group(ListUserNotificationsResponseDataResponse, () {
    // num id
    test('to test the property `id`', () async {
      // TODO
    });

    // String title
    test('to test the property `title`', () async {
      // TODO
    });

    // String body
    test('to test the property `body`', () async {
      // TODO
    });

    // String icon
    test('to test the property `icon`', () async {
      // TODO
    });

    // WebPushPayloadData data
    test('to test the property `data`', () async {
      // TODO
    });

    // num recipientId
    test('to test the property `recipientId`', () async {
      // TODO
    });

    // JsonObject readAt
    test('to test the property `readAt`', () async {
      // TODO
    });

    // JsonObject trashedAt
    test('to test the property `trashedAt`', () async {
      // TODO
    });

    // DateTime createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // GetNotificationResponseData notification
    test('to test the property `notification`', () async {
      // TODO
    });
  });
}
