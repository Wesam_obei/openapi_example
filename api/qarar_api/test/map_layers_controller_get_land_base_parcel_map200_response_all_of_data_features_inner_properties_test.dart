import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties
void main() {
  final instance =
      MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder();
  // TODO add properties to the builder and call build()

  group(
      MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties,
      () {
    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId id
    test('to test the property `id`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId parcelPlanNo
    test('to test the property `parcelPlanNo`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId parcelMainLuse
    test('to test the property `parcelMainLuse`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId parcelSubLuse
    test('to test the property `parcelSubLuse`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId oldParcelPlanNO
    test('to test the property `oldParcelPlanNO`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId USING_SYMBOL
    test('to test the property `USING_SYMBOL`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId UNITS_NUMBER
    test('to test the property `UNITS_NUMBER`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId PLAN_NO
    test('to test the property `PLAN_NO`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId BLDG_CONDITIONS
    test('to test the property `BLDG_CONDITIONS`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId municipality
    test('to test the property `municipality`', () async {
      // TODO
    });
  });
}
