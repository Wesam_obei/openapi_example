import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for ListUserDataResponse
void main() {
  final instance = ListUserDataResponseBuilder();
  // TODO add properties to the builder and call build()

  group(ListUserDataResponse, () {
    // num id
    test('to test the property `id`', () async {
      // TODO
    });

    // String email
    test('to test the property `email`', () async {
      // TODO
    });

    // String fullName
    test('to test the property `fullName`', () async {
      // TODO
    });

    // String jobTitle
    test('to test the property `jobTitle`', () async {
      // TODO
    });

    // bool isActive
    test('to test the property `isActive`', () async {
      // TODO
    });

    // DateTime createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // BuiltList<GetRoleDataResponse> roles
    test('to test the property `roles`', () async {
      // TODO
    });

    // BuiltList<PermissionDataResponse> permissions
    test('to test the property `permissions`', () async {
      // TODO
    });

    // BuiltList<GetEntityDataResponse> entities
    test('to test the property `entities`', () async {
      // TODO
    });

    // GetRaqmyInfo raqmyInfo
    test('to test the property `raqmyInfo`', () async {
      // TODO
    });
  });
}
