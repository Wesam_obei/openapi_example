import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for GetSurveyRecipientViewDataResponse
void main() {
  final instance = GetSurveyRecipientViewDataResponseBuilder();
  // TODO add properties to the builder and call build()

  group(GetSurveyRecipientViewDataResponse, () {
    // num id
    test('to test the property `id`', () async {
      // TODO
    });

    // num originalId
    test('to test the property `originalId`', () async {
      // TODO
    });

    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // GetSurveyResponseDataResponse response
    test('to test the property `response`', () async {
      // TODO
    });

    // String type
    test('to test the property `type`', () async {
      // TODO
    });
  });
}
