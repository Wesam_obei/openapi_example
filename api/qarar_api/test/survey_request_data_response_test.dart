import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for SurveyRequestDataResponse
void main() {
  final instance = SurveyRequestDataResponseBuilder();
  // TODO add properties to the builder and call build()

  group(SurveyRequestDataResponse, () {
    // num id
    test('to test the property `id`', () async {
      // TODO
    });

    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // String description
    test('to test the property `description`', () async {
      // TODO
    });

    // bool isActive
    test('to test the property `isActive`', () async {
      // TODO
    });

    // DateTime startAt
    test('to test the property `startAt`', () async {
      // TODO
    });

    // DateTime endAt
    test('to test the property `endAt`', () async {
      // TODO
    });

    // DateTime createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // GetSurveyResponseDataResponse response
    test('to test the property `response`', () async {
      // TODO
    });

    // bool isDelayed
    test('to test the property `isDelayed`', () async {
      // TODO
    });

    // num recipientId
    test('to test the property `recipientId`', () async {
      // TODO
    });

    // String recipientName
    test('to test the property `recipientName`', () async {
      // TODO
    });

    // RecipientType recipientType
    test('to test the property `recipientType`', () async {
      // TODO
    });

    // GetSurveyDataResponse survey
    test('to test the property `survey`', () async {
      // TODO
    });
  });
}
