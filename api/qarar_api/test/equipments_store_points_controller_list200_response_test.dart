import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for EquipmentsStorePointsControllerList200Response
void main() {
  final instance = EquipmentsStorePointsControllerList200ResponseBuilder();
  // TODO add properties to the builder and call build()

  group(EquipmentsStorePointsControllerList200Response, () {
    // bool success
    test('to test the property `success`', () async {
      // TODO
    });

    // BuiltList<ListEquipmentStorePointDataResponse> data
    test('to test the property `data`', () async {
      // TODO
    });

    // PaginatedMetaDocumented meta
    test('to test the property `meta`', () async {
      // TODO
    });

    // PaginatedLinksDocumented links
    test('to test the property `links`', () async {
      // TODO
    });
  });
}
