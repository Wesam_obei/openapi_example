import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for GetUserDashboardDataResponse
void main() {
  final instance = GetUserDashboardDataResponseBuilder();
  // TODO add properties to the builder and call build()

  group(GetUserDashboardDataResponse, () {
    // String id
    test('to test the property `id`', () async {
      // TODO
    });

    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // bool isDefault
    test('to test the property `isDefault`', () async {
      // TODO
    });

    // BuiltList<WidgetConfig> settings
    test('to test the property `settings`', () async {
      // TODO
    });
  });
}
