import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for GetUserActivityLogDataResponse
void main() {
  final instance = GetUserActivityLogDataResponseBuilder();
  // TODO add properties to the builder and call build()

  group(GetUserActivityLogDataResponse, () {
    // String id
    test('to test the property `id`', () async {
      // TODO
    });

    // String userId
    test('to test the property `userId`', () async {
      // TODO
    });

    // String eventId
    test('to test the property `eventId`', () async {
      // TODO
    });

    // GetAuditLogEventDataResponse event
    test('to test the property `event`', () async {
      // TODO
    });

    // GetUserDataResponse user
    test('to test the property `user`', () async {
      // TODO
    });

    // GetAuditLogPayloadDataResponse payload
    test('to test the property `payload`', () async {
      // TODO
    });

    // DateTime createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });
  });
}
