import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for SurveyResponseAnswerDataResponse
void main() {
  final instance = SurveyResponseAnswerDataResponseBuilder();
  // TODO add properties to the builder and call build()

  group(SurveyResponseAnswerDataResponse, () {
    // num id
    test('to test the property `id`', () async {
      // TODO
    });

    // num parentAnswerId
    test('to test the property `parentAnswerId`', () async {
      // TODO
    });

    // num order
    test('to test the property `order`', () async {
      // TODO
    });

    // JsonObject value
    test('to test the property `value`', () async {
      // TODO
    });

    // num formComponentId
    test('to test the property `formComponentId`', () async {
      // TODO
    });
  });
}
