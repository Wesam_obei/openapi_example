import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for GetFileDataResponse
void main() {
  final instance = GetFileDataResponseBuilder();
  // TODO add properties to the builder and call build()

  group(GetFileDataResponse, () {
    // num id
    test('to test the property `id`', () async {
      // TODO
    });

    // String filename
    test('to test the property `filename`', () async {
      // TODO
    });

    // num size
    test('to test the property `size`', () async {
      // TODO
    });

    // String mimeType
    test('to test the property `mimeType`', () async {
      // TODO
    });

    // String path
    test('to test the property `path`', () async {
      // TODO
    });
  });
}
