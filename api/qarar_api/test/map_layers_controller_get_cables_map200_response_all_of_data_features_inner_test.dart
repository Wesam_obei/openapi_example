import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner
void main() {
  final instance =
      MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerBuilder();
  // TODO add properties to the builder and call build()

  group(MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner, () {
    // String type
    test('to test the property `type`', () async {
      // TODO
    });

    // int id
    test('to test the property `id`', () async {
      // TODO
    });

    // JsonObject geometry
    test('to test the property `geometry`', () async {
      // TODO
    });

    // MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInnerProperties properties
    test('to test the property `properties`', () async {
      // TODO
    });
  });
}
