import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for GetPlanVersionDataResponse
void main() {
  final instance = GetPlanVersionDataResponseBuilder();
  // TODO add properties to the builder and call build()

  group(GetPlanVersionDataResponse, () {
    // num id
    test('to test the property `id`', () async {
      // TODO
    });

    // num revisionNumber
    test('to test the property `revisionNumber`', () async {
      // TODO
    });

    // num revisionYear
    test('to test the property `revisionYear`', () async {
      // TODO
    });

    // String issuedAt
    test('to test the property `issuedAt`', () async {
      // TODO
    });

    // GetFileDataResponse file
    test('to test the property `file`', () async {
      // TODO
    });
  });
}
