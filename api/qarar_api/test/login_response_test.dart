import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for LoginResponse
void main() {
  final instance = LoginResponseBuilder();
  // TODO add properties to the builder and call build()

  group(LoginResponse, () {
    // String accessToken
    test('to test the property `accessToken`', () async {
      // TODO
    });

    // DateTime accessTokenExpiredDate
    test('to test the property `accessTokenExpiredDate`', () async {
      // TODO
    });

    // String refreshToken
    test('to test the property `refreshToken`', () async {
      // TODO
    });

    // DateTime refreshTokenExpiredDate
    test('to test the property `refreshTokenExpiredDate`', () async {
      // TODO
    });

    // String sessionId
    test('to test the property `sessionId`', () async {
      // TODO
    });
  });
}
