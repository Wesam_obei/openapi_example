import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner
void main() {
  final instance =
      MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerBuilder();
  // TODO add properties to the builder and call build()

  group(
      MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInner,
      () {
    // String type
    test('to test the property `type`', () async {
      // TODO
    });

    // int id
    test('to test the property `id`', () async {
      // TODO
    });

    // JsonObject geometry
    test('to test the property `geometry`', () async {
      // TODO
    });

    // MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties properties
    test('to test the property `properties`', () async {
      // TODO
    });
  });
}
