import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for EquipmentStorePointUpdateRequest
void main() {
  final instance = EquipmentStorePointUpdateRequestBuilder();
  // TODO add properties to the builder and call build()

  group(EquipmentStorePointUpdateRequest, () {
    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // LocationData location
    test('to test the property `location`', () async {
      // TODO
    });

    // num entityId
    test('to test the property `entityId`', () async {
      // TODO
    });

    // String description
    test('to test the property `description`', () async {
      // TODO
    });

    // String streetName
    test('to test the property `streetName`', () async {
      // TODO
    });
  });
}
