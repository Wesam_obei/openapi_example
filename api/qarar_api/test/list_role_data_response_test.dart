import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for ListRoleDataResponse
void main() {
  final instance = ListRoleDataResponseBuilder();
  // TODO add properties to the builder and call build()

  group(ListRoleDataResponse, () {
    // num id
    test('to test the property `id`', () async {
      // TODO
    });

    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // String description
    test('to test the property `description`', () async {
      // TODO
    });

    // DateTime createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // BuiltList<PermissionDataResponse> permissions
    test('to test the property `permissions`', () async {
      // TODO
    });
  });
}
