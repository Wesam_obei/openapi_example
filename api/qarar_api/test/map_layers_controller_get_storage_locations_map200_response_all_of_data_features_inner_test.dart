import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner
void main() {
  final instance =
      MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInnerBuilder();
  // TODO add properties to the builder and call build()

  group(
      MapLayersControllerGetStorageLocationsMap200ResponseAllOfDataFeaturesInner,
      () {
    // String type
    test('to test the property `type`', () async {
      // TODO
    });

    // int id
    test('to test the property `id`', () async {
      // TODO
    });

    // JsonObject geometry
    test('to test the property `geometry`', () async {
      // TODO
    });

    // JsonObject properties
    test('to test the property `properties`', () async {
      // TODO
    });
  });
}
