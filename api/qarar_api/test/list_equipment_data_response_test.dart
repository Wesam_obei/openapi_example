import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for ListEquipmentDataResponse
void main() {
  final instance = ListEquipmentDataResponseBuilder();
  // TODO add properties to the builder and call build()

  group(ListEquipmentDataResponse, () {
    // num id
    test('to test the property `id`', () async {
      // TODO
    });

    // String brand
    test('to test the property `brand`', () async {
      // TODO
    });

    // String model
    test('to test the property `model`', () async {
      // TODO
    });

    // String plateNumber
    test('to test the property `plateNumber`', () async {
      // TODO
    });

    // String vinNumber
    test('to test the property `vinNumber`', () async {
      // TODO
    });

    // num localNumber
    test('to test the property `localNumber`', () async {
      // TODO
    });

    // num serialNumber
    test('to test the property `serialNumber`', () async {
      // TODO
    });

    // num manufacturingYear
    test('to test the property `manufacturingYear`', () async {
      // TODO
    });

    // DateTime registrationExpiryDate
    test('to test the property `registrationExpiryDate`', () async {
      // TODO
    });

    // DateTime ownershipDate
    test('to test the property `ownershipDate`', () async {
      // TODO
    });

    // DateTime createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // GetEntityDataResponse entity
    test('to test the property `entity`', () async {
      // TODO
    });

    // GetEquipmentTypeDataResponse equipmentsType
    test('to test the property `equipmentsType`', () async {
      // TODO
    });

    // GetEquipmentStorePointDataResponse equipmentsStorePoint
    test('to test the property `equipmentsStorePoint`', () async {
      // TODO
    });
  });
}
