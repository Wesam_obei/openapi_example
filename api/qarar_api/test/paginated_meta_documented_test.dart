import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for PaginatedMetaDocumented
void main() {
  final instance = PaginatedMetaDocumentedBuilder();
  // TODO add properties to the builder and call build()

  group(PaginatedMetaDocumented, () {
    // num itemsPerPage
    test('to test the property `itemsPerPage`', () async {
      // TODO
    });

    // num totalItems
    test('to test the property `totalItems`', () async {
      // TODO
    });

    // num currentPage
    test('to test the property `currentPage`', () async {
      // TODO
    });

    // num totalPages
    test('to test the property `totalPages`', () async {
      // TODO
    });

    // BuiltList<BuiltList<SortingByColumnsInnerInner>> sortBy
    test('to test the property `sortBy`', () async {
      // TODO
    });

    // BuiltList<String> searchBy
    test('to test the property `searchBy`', () async {
      // TODO
    });

    // String search
    test('to test the property `search`', () async {
      // TODO
    });

    // BuiltList<String> select
    test('to test the property `select`', () async {
      // TODO
    });

    // JsonObject filter
    test('to test the property `filter`', () async {
      // TODO
    });

    // BuiltList<String> includes
    test('to test the property `includes`', () async {
      // TODO
    });
  });
}
