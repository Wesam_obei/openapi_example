import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties
void main() {
  final instance =
      MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder();
  // TODO add properties to the builder and call build()

  group(
      MapLayersControllerGetBridgesTunnelsSitesMap200ResponseAllOfDataFeaturesInnerProperties,
      () {
    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId id
    test('to test the property `id`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId projectName
    test('to test the property `projectName`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId devSiteDate
    test('to test the property `devSiteDate`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId devsiteDate
    test('to test the property `devsiteDate`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId compRat
    test('to test the property `compRat`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId primDate
    test('to test the property `primDate`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId finalDate
    test('to test the property `finalDate`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId projState
    test('to test the property `projState`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId stages
    test('to test the property `stages`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId devWebsiteDate
    test('to test the property `devWebsiteDate`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId primDateNew
    test('to test the property `primDateNew`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId finalDateNew
    test('to test the property `finalDateNew`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId primDateMun
    test('to test the property `primDateMun`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId primDateExc
    test('to test the property `primDateExc`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId finalDateMun
    test('to test the property `finalDateMun`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId finalDateExc
    test('to test the property `finalDateExc`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId devWebsiteDateMun
    test('to test the property `devWebsiteDateMun`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId devWebsiteDateExc
    test('to test the property `devWebsiteDateExc`', () async {
      // TODO
    });
  });
}
