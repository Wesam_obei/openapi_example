import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
void main() {
  final instance =
      MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder();
  // TODO add properties to the builder and call build()

  group(
      MapLayersControllerGetUrbanAreaBoundariesMap200ResponseAllOfDataFeaturesInnerProperties,
      () {
    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId id
    test('to test the property `id`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId provinceName
    test('to test the property `provinceName`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId urbanBoundaryType
    test('to test the property `urbanBoundaryType`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId municipalityName
    test('to test the property `municipalityName`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId remarks
    test('to test the property `remarks`', () async {
      // TODO
    });
  });
}
