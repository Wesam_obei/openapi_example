import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for ListPlanDataResponse
void main() {
  final instance = ListPlanDataResponseBuilder();
  // TODO add properties to the builder and call build()

  group(ListPlanDataResponse, () {
    // num id
    test('to test the property `id`', () async {
      // TODO
    });

    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // String description
    test('to test the property `description`', () async {
      // TODO
    });

    // BuiltList<GetPlanVersionDataResponse> versions
    test('to test the property `versions`', () async {
      // TODO
    });
  });
}
