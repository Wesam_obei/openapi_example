import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties
void main() {
  final instance =
      MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder();
  // TODO add properties to the builder and call build()

  group(
      MapLayersControllerGetDistrictBoundariesMap200ResponseAllOfDataFeaturesInnerProperties,
      () {
    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId id
    test('to test the property `id`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId municipalityName
    test('to test the property `municipalityName`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId municipalitySubName
    test('to test the property `municipalitySubName`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId districtName
    test('to test the property `districtName`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId districtEngName
    test('to test the property `districtEngName`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId remarks
    test('to test the property `remarks`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId areaName
    test('to test the property `areaName`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId cityName
    test('to test the property `cityName`', () async {
      // TODO
    });
  });
}
