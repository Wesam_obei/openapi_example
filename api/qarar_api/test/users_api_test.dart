import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

/// tests for UsersApi
void main() {
  final instance = QararApi().getUsersApi();

  group(UsersApi, () {
    //Future usersControllerCreate(String authorization, UserCreateRequest userCreateRequest, { JsonObject xLanguage, JsonObject contentType }) async
    test('test usersControllerCreate', () async {
      // TODO
    });

    //Future usersControllerDelete(String id, String authorization, { JsonObject xLanguage, JsonObject contentType }) async
    test('test usersControllerDelete', () async {
      // TODO
    });

    //Future<UsersControllerEmployeesList200Response> usersControllerEmployeesList(String authorization, { String search, JsonObject xLanguage, JsonObject contentType }) async
    test('test usersControllerEmployeesList', () async {
      // TODO
    });

    //Future<UsersControllerGet200Response> usersControllerGet(String id, String authorization, { String includes, JsonObject xLanguage, JsonObject contentType }) async
    test('test usersControllerGet', () async {
      // TODO
    });

    //Future<UsersControllerList200Response> usersControllerList(String authorization, { num page, num limit, BuiltList<String> filterPeriodId, BuiltList<String> filterPeriodFullName, BuiltList<String> filterPeriodEmail, BuiltList<String> filterPeriodIsActive, BuiltList<String> filterPeriodRolePeriodName, BuiltList<String> filterPeriodRolePeriodPermissionsPeriodName, BuiltList<String> filterPeriodRolePeriodPermissionsPeriodCategoryPeriodName, BuiltList<String> filterPeriodEntitiesPeriodId, BuiltList<String> sortBy, String search, String searchBy, String includes, JsonObject xLanguage, JsonObject contentType }) async
    test('test usersControllerList', () async {
      // TODO
    });

    //Future usersControllerUpdate(String id, String authorization, UserUpdateRequest userUpdateRequest, { JsonObject xLanguage, JsonObject contentType }) async
    test('test usersControllerUpdate', () async {
      // TODO
    });
  });
}
