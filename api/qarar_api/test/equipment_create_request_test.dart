import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for EquipmentCreateRequest
void main() {
  final instance = EquipmentCreateRequestBuilder();
  // TODO add properties to the builder and call build()

  group(EquipmentCreateRequest, () {
    // num equipmentTypeId
    test('to test the property `equipmentTypeId`', () async {
      // TODO
    });

    // num equipmentStorePointId
    test('to test the property `equipmentStorePointId`', () async {
      // TODO
    });

    // num entityId
    test('to test the property `entityId`', () async {
      // TODO
    });

    // String brand
    test('to test the property `brand`', () async {
      // TODO
    });

    // String model
    test('to test the property `model`', () async {
      // TODO
    });

    // String plateNumber
    test('to test the property `plateNumber`', () async {
      // TODO
    });

    // String vinNumber
    test('to test the property `vinNumber`', () async {
      // TODO
    });

    // num localNumber
    test('to test the property `localNumber`', () async {
      // TODO
    });

    // num serialNumber
    test('to test the property `serialNumber`', () async {
      // TODO
    });

    // num manufacturingYear
    test('to test the property `manufacturingYear`', () async {
      // TODO
    });

    // String registrationExpiryDate
    test('to test the property `registrationExpiryDate`', () async {
      // TODO
    });

    // String ownershipDate
    test('to test the property `ownershipDate`', () async {
      // TODO
    });
  });
}
