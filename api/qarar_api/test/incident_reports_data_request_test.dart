import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for IncidentReportsDataRequest
void main() {
  final instance = IncidentReportsDataRequestBuilder();
  // TODO add properties to the builder and call build()

  group(IncidentReportsDataRequest, () {
    // Envelope envelope
    test('to test the property `envelope`', () async {
      // TODO
    });

    // BuiltList<String> fields
    test('to test the property `fields`', () async {
      // TODO
    });

    // BuiltList<String> statuses
    test('to test the property `statuses`', () async {
      // TODO
    });

    // DateTime startTime
    test('to test the property `startTime`', () async {
      // TODO
    });

    // DateTime endTime
    test('to test the property `endTime`', () async {
      // TODO
    });
  });
}
