import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

/// tests for EquipmentsStorePointsApi
void main() {
  final instance = QararApi().getEquipmentsStorePointsApi();

  group(EquipmentsStorePointsApi, () {
    //Future equipmentsStorePointsControllerCreate(String authorization, EquipmentStorePointCreateRequest equipmentStorePointCreateRequest, { JsonObject xLanguage, JsonObject contentType }) async
    test('test equipmentsStorePointsControllerCreate', () async {
      // TODO
    });

    //Future equipmentsStorePointsControllerDelete(String id, String authorization, { JsonObject xLanguage, JsonObject contentType }) async
    test('test equipmentsStorePointsControllerDelete', () async {
      // TODO
    });

    //Future<EquipmentsStorePointsControllerGet200Response> equipmentsStorePointsControllerGet(String id, String authorization, { String includes, JsonObject xLanguage, JsonObject contentType }) async
    test('test equipmentsStorePointsControllerGet', () async {
      // TODO
    });

    //Future<EquipmentsStorePointsControllerList200Response> equipmentsStorePointsControllerList(String authorization, { num page, num limit, BuiltList<String> filterPeriodId, BuiltList<String> filterPeriodName, BuiltList<String> filterPeriodDescription, BuiltList<String> filterPeriodEntityPeriodId, BuiltList<String> filterPeriodCreatorPeriodId, BuiltList<String> filterPeriodEntityPeriodName, BuiltList<String> sortBy, String search, String searchBy, String includes, JsonObject xLanguage, JsonObject contentType }) async
    test('test equipmentsStorePointsControllerList', () async {
      // TODO
    });

    //Future equipmentsStorePointsControllerUpdate(String id, String authorization, EquipmentStorePointUpdateRequest equipmentStorePointUpdateRequest, { JsonObject xLanguage, JsonObject contentType }) async
    test('test equipmentsStorePointsControllerUpdate', () async {
      // TODO
    });
  });
}
