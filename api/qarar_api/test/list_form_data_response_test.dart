import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for ListFormDataResponse
void main() {
  final instance = ListFormDataResponseBuilder();
  // TODO add properties to the builder and call build()

  group(ListFormDataResponse, () {
    // num id
    test('to test the property `id`', () async {
      // TODO
    });

    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // String description
    test('to test the property `description`', () async {
      // TODO
    });

    // bool isActive
    test('to test the property `isActive`', () async {
      // TODO
    });

    // bool editable
    test('to test the property `editable`', () async {
      // TODO
    });

    // DateTime createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // GetFileDataResponse file
    test('to test the property `file`', () async {
      // TODO
    });

    // GetUserDataResponse creator
    test('to test the property `creator`', () async {
      // TODO
    });

    // BuiltList<GetFormComponentDataResponse> children
    test('to test the property `children`', () async {
      // TODO
    });
  });
}
