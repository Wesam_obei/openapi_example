import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for WebPushPayload
void main() {
  final instance = WebPushPayloadBuilder();
  // TODO add properties to the builder and call build()

  group(WebPushPayload, () {
    // num id
    test('to test the property `id`', () async {
      // TODO
    });

    // String title
    test('to test the property `title`', () async {
      // TODO
    });

    // String body
    test('to test the property `body`', () async {
      // TODO
    });

    // String icon
    test('to test the property `icon`', () async {
      // TODO
    });

    // WebPushPayloadData data
    test('to test the property `data`', () async {
      // TODO
    });
  });
}
