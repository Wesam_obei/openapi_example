import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for GetFormComponentDataResponse
void main() {
  final instance = GetFormComponentDataResponseBuilder();
  // TODO add properties to the builder and call build()

  group(GetFormComponentDataResponse, () {
    // num id
    test('to test the property `id`', () async {
      // TODO
    });

    // String label
    test('to test the property `label`', () async {
      // TODO
    });

    // String placeholder
    test('to test the property `placeholder`', () async {
      // TODO
    });

    // String description
    test('to test the property `description`', () async {
      // TODO
    });

    // bool isRequired
    test('to test the property `isRequired`', () async {
      // TODO
    });

    // bool isActive
    test('to test the property `isActive`', () async {
      // TODO
    });

    // num responseDataTypeId
    test('to test the property `responseDataTypeId`', () async {
      // TODO
    });

    // num parentComponentId
    test('to test the property `parentComponentId`', () async {
      // TODO
    });

    // bool isArray
    test('to test the property `isArray`', () async {
      // TODO
    });

    // bool isGroup
    test('to test the property `isGroup`', () async {
      // TODO
    });

    // bool allowMultipleResponse
    test('to test the property `allowMultipleResponse`', () async {
      // TODO
    });

    // num order
    test('to test the property `order`', () async {
      // TODO
    });

    // JsonObject config
    test('to test the property `config`', () async {
      // TODO
    });

    // DateTime createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // GetUserDataResponse creator
    test('to test the property `creator`', () async {
      // TODO
    });

    // GetFormComponentResponseDataType responseDataType
    test('to test the property `responseDataType`', () async {
      // TODO
    });

    // BuiltList<GetFormComponentDataResponse> children
    test('to test the property `children`', () async {
      // TODO
    });
  });
}
