import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

/// tests for SurveysApi
void main() {
  final instance = QararApi().getSurveysApi();

  group(SurveysApi, () {
    //Future surveysControllerCreate(String authorization, SurveyCreateRequest surveyCreateRequest, { JsonObject xLanguage, JsonObject contentType }) async
    test('test surveysControllerCreate', () async {
      // TODO
    });

    //Future surveysControllerDelete(String id, String authorization, { JsonObject xLanguage, JsonObject contentType }) async
    test('test surveysControllerDelete', () async {
      // TODO
    });

    //Future<SurveysControllerGet200Response> surveysControllerGet(String id, String authorization, { String includes, JsonObject xLanguage, JsonObject contentType }) async
    test('test surveysControllerGet', () async {
      // TODO
    });

    //Future<SurveysControllerList200Response> surveysControllerList(String authorization, { num page, num limit, BuiltList<String> filterPeriodId, BuiltList<String> filterPeriodName, BuiltList<String> filterPeriodDescription, BuiltList<String> sortBy, String search, String searchBy, String includes, JsonObject xLanguage, JsonObject contentType }) async
    test('test surveysControllerList', () async {
      // TODO
    });

    //Future<SurveysControllerRequests200Response> surveysControllerRequests(String authorization, { num page, num limit, BuiltList<String> filterPeriodId, BuiltList<String> filterPeriodIsDelayed, BuiltList<String> filterPeriodResponsePeriodResponseStatusPeriodName, BuiltList<String> filterPeriodResponsePeriodResponseStatusId, BuiltList<String> sortBy, String search, String searchBy, JsonObject xLanguage, JsonObject contentType }) async
    test('test surveysControllerRequests', () async {
      // TODO
    });

    //Future<SurveysControllerRequestsDetails200Response> surveysControllerRequestsDetails(String recipientId, String authorization, { String includes, JsonObject xLanguage, JsonObject contentType }) async
    test('test surveysControllerRequestsDetails', () async {
      // TODO
    });

    //Future surveysControllerUpdate(String id, String authorization, SurveyUpdateRequest surveyUpdateRequest, { JsonObject xLanguage, JsonObject contentType }) async
    test('test surveysControllerUpdate', () async {
      // TODO
    });
  });
}
