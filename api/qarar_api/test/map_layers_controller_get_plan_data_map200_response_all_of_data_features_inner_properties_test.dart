import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties
void main() {
  final instance =
      MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerPropertiesBuilder();
  // TODO add properties to the builder and call build()

  group(
      MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties,
      () {
    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId id
    test('to test the property `id`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId municipalityName
    test('to test the property `municipalityName`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId subMunicipalityName
    test('to test the property `subMunicipalityName`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId planNo
    test('to test the property `planNo`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId planName
    test('to test the property `planName`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId planPkNo
    test('to test the property `planPkNo`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId districtName
    test('to test the property `districtName`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId ownerId
    test('to test the property `ownerId`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId approvalStatus
    test('to test the property `approvalStatus`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId planHasServices
    test('to test the property `planHasServices`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId planApprovedBy
    test('to test the property `planApprovedBy`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId planLatCoord
    test('to test the property `planLatCoord`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId planLongCoord
    test('to test the property `planLongCoord`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId oldPlanNo
    test('to test the property `oldPlanNo`', () async {
      // TODO
    });

    // MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId mapObjectId
    test('to test the property `mapObjectId`', () async {
      // TODO
    });
  });
}
