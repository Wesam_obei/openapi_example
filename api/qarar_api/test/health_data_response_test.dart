import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for HealthDataResponse
void main() {
  final instance = HealthDataResponseBuilder();
  // TODO add properties to the builder and call build()

  group(HealthDataResponse, () {
    // String status
    test('to test the property `status`', () async {
      // TODO
    });

    // JsonObject info
    test('to test the property `info`', () async {
      // TODO
    });

    // JsonObject error
    test('to test the property `error`', () async {
      // TODO
    });

    // JsonObject details
    test('to test the property `details`', () async {
      // TODO
    });
  });
}
