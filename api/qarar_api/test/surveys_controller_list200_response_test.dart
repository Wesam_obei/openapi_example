import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for SurveysControllerList200Response
void main() {
  final instance = SurveysControllerList200ResponseBuilder();
  // TODO add properties to the builder and call build()

  group(SurveysControllerList200Response, () {
    // bool success
    test('to test the property `success`', () async {
      // TODO
    });

    // BuiltList<ListSurveyDataResponse> data
    test('to test the property `data`', () async {
      // TODO
    });

    // PaginatedMetaDocumented meta
    test('to test the property `meta`', () async {
      // TODO
    });

    // PaginatedLinksDocumented links
    test('to test the property `links`', () async {
      // TODO
    });
  });
}
