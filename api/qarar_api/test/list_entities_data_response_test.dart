import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for ListEntitiesDataResponse
void main() {
  final instance = ListEntitiesDataResponseBuilder();
  // TODO add properties to the builder and call build()

  group(ListEntitiesDataResponse, () {
    // num id
    test('to test the property `id`', () async {
      // TODO
    });

    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // DateTime createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // BuiltList<String> users
    test('to test the property `users`', () async {
      // TODO
    });

    // GetFileDataResponse file
    test('to test the property `file`', () async {
      // TODO
    });
  });
}
