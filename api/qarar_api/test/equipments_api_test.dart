import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

/// tests for EquipmentsApi
void main() {
  final instance = QararApi().getEquipmentsApi();

  group(EquipmentsApi, () {
    //Future equipmentsControllerCreate(String authorization, EquipmentCreateRequest equipmentCreateRequest, { JsonObject xLanguage, JsonObject contentType }) async
    test('test equipmentsControllerCreate', () async {
      // TODO
    });

    //Future equipmentsControllerDelete(String id, String authorization, { JsonObject xLanguage, JsonObject contentType }) async
    test('test equipmentsControllerDelete', () async {
      // TODO
    });

    //Future<EquipmentsControllerGet200Response> equipmentsControllerGet(String id, String authorization, { String includes, JsonObject xLanguage, JsonObject contentType }) async
    test('test equipmentsControllerGet', () async {
      // TODO
    });

    //Future<EquipmentsControllerList200Response> equipmentsControllerList(String authorization, { num page, num limit, BuiltList<String> filterPeriodId, BuiltList<String> filterPeriodName, BuiltList<String> filterPeriodDescription, BuiltList<String> filterPeriodEntityPeriodId, BuiltList<String> filterPeriodEntityPeriodName, BuiltList<String> filterPeriodEquipmentsTypePeriodId, BuiltList<String> sortBy, String search, String searchBy, String includes, JsonObject xLanguage, JsonObject contentType }) async
    test('test equipmentsControllerList', () async {
      // TODO
    });

    //Future equipmentsControllerUpdate(String id, String authorization, EquipmentUpdateRequest equipmentUpdateRequest, { JsonObject xLanguage, JsonObject contentType }) async
    test('test equipmentsControllerUpdate', () async {
      // TODO
    });
  });
}
