import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for GetSurveyDataResponse
void main() {
  final instance = GetSurveyDataResponseBuilder();
  // TODO add properties to the builder and call build()

  group(GetSurveyDataResponse, () {
    // num id
    test('to test the property `id`', () async {
      // TODO
    });

    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // String description
    test('to test the property `description`', () async {
      // TODO
    });

    // bool isActive
    test('to test the property `isActive`', () async {
      // TODO
    });

    // DateTime startAt
    test('to test the property `startAt`', () async {
      // TODO
    });

    // DateTime endAt
    test('to test the property `endAt`', () async {
      // TODO
    });

    // DateTime createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // num numberOfResponses
    test('to test the property `numberOfResponses`', () async {
      // TODO
    });

    // num numberOfRecipients
    test('to test the property `numberOfRecipients`', () async {
      // TODO
    });

    // GetUserDataResponse creator
    test('to test the property `creator`', () async {
      // TODO
    });

    // GetFormDataResponse form
    test('to test the property `form`', () async {
      // TODO
    });

    // BuiltList<GetSurveyRecipientViewDataResponse> recipients
    test('to test the property `recipients`', () async {
      // TODO
    });
  });
}
