import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for ListSurveyResponseDataResponse
void main() {
  final instance = ListSurveyResponseDataResponseBuilder();
  // TODO add properties to the builder and call build()

  group(ListSurveyResponseDataResponse, () {
    // num id
    test('to test the property `id`', () async {
      // TODO
    });

    // DateTime createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // num recipientId
    test('to test the property `recipientId`', () async {
      // TODO
    });

    // GetUserDataResponse answeredBy
    test('to test the property `answeredBy`', () async {
      // TODO
    });

    // StatusResponse responseStatus
    test('to test the property `responseStatus`', () async {
      // TODO
    });

    // BuiltList<SurveyResponseAnswerDataResponse> surveyResponseAnswer
    test('to test the property `surveyResponseAnswer`', () async {
      // TODO
    });

    // GetSurveyDataResponse survey
    test('to test the property `survey`', () async {
      // TODO
    });

    // GetSurveyRecipientViewDataResponse recipient
    test('to test the property `recipient`', () async {
      // TODO
    });

    // bool canAppliedReview
    test('to test the property `canAppliedReview`', () async {
      // TODO
    });
  });
}
