import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for PermissionDataResponse
void main() {
  final instance = PermissionDataResponseBuilder();
  // TODO add properties to the builder and call build()

  group(PermissionDataResponse, () {
    // num id
    test('to test the property `id`', () async {
      // TODO
    });

    // PermissionsEnum name
    test('to test the property `name`', () async {
      // TODO
    });

    // String label
    test('to test the property `label`', () async {
      // TODO
    });

    // String description
    test('to test the property `description`', () async {
      // TODO
    });

    // num order
    test('to test the property `order`', () async {
      // TODO
    });

    // DateTime createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });
  });
}
