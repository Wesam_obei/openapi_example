import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for ListEquipmentStorePointDataResponse
void main() {
  final instance = ListEquipmentStorePointDataResponseBuilder();
  // TODO add properties to the builder and call build()

  group(ListEquipmentStorePointDataResponse, () {
    // num id
    test('to test the property `id`', () async {
      // TODO
    });

    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // String description
    test('to test the property `description`', () async {
      // TODO
    });

    // LocationData location
    test('to test the property `location`', () async {
      // TODO
    });

    // DateTime createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // GetEntityDataResponse entity
    test('to test the property `entity`', () async {
      // TODO
    });
  });
}
