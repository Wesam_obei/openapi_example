import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for ListEmployeeUserDataResponse
void main() {
  final instance = ListEmployeeUserDataResponseBuilder();
  // TODO add properties to the builder and call build()

  group(ListEmployeeUserDataResponse, () {
    // num id
    test('to test the property `id`', () async {
      // TODO
    });

    // String email
    test('to test the property `email`', () async {
      // TODO
    });

    // String fullName
    test('to test the property `fullName`', () async {
      // TODO
    });

    // String jobTitle
    test('to test the property `jobTitle`', () async {
      // TODO
    });

    // num employeeId
    test('to test the property `employeeId`', () async {
      // TODO
    });
  });
}
