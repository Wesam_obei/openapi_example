import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

/// tests for SurveyResponsesApi
void main() {
  final instance = QararApi().getSurveyResponsesApi();

  group(SurveyResponsesApi, () {
    //Future<SurveyResponsesControllerList200Response> surveyResponsesControllerList(String surveyId, String authorization, { num page, num limit, BuiltList<String> filterPeriodId, BuiltList<String> filterPeriodResponseStatusPeriodName, BuiltList<String> sortBy, String search, String searchBy, String includes, JsonObject xLanguage, JsonObject contentType }) async
    test('test surveyResponsesControllerList', () async {
      // TODO
    });

    //Future surveyResponsesControllerRecipientResponse(String surveyId, String authorization, RecipientResponseRequest recipientResponseRequest, { JsonObject xLanguage, JsonObject contentType }) async
    test('test surveyResponsesControllerRecipientResponse', () async {
      // TODO
    });

    //Future<SurveyResponsesControllerResponseDetails200Response> surveyResponsesControllerResponseDetails(String responseId, String surveyId, String authorization, { String includes, JsonObject xLanguage, JsonObject contentType }) async
    test('test surveyResponsesControllerResponseDetails', () async {
      // TODO
    });

    //Future surveyResponsesControllerVerifierResponse(String surveyId, String authorization, VerifierResponseRequest verifierResponseRequest, { JsonObject xLanguage, JsonObject contentType }) async
    test('test surveyResponsesControllerVerifierResponse', () async {
      // TODO
    });
  });
}
