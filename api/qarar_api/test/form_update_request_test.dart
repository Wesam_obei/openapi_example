import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for FormUpdateRequest
void main() {
  final instance = FormUpdateRequestBuilder();
  // TODO add properties to the builder and call build()

  group(FormUpdateRequest, () {
    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // String description
    test('to test the property `description`', () async {
      // TODO
    });

    // bool isActive
    test('to test the property `isActive`', () async {
      // TODO
    });

    // String fileToken
    test('to test the property `fileToken`', () async {
      // TODO
    });

    // BuiltList<FormComponentsRequest> formComponents
    test('to test the property `formComponents`', () async {
      // TODO
    });

    // bool deleteFile
    test('to test the property `deleteFile`', () async {
      // TODO
    });

    // BuiltList<num> deletedComponents
    test('to test the property `deletedComponents`', () async {
      // TODO
    });
  });
}
