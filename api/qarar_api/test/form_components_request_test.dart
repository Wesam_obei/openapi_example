import 'package:test/test.dart';
import 'package:qarar_api/qarar_api.dart';

// tests for FormComponentsRequest
void main() {
  final instance = FormComponentsRequestBuilder();
  // TODO add properties to the builder and call build()

  group(FormComponentsRequest, () {
    // FormResponseDataTypeEnum responseDataType
    test('to test the property `responseDataType`', () async {
      // TODO
    });

    // bool isArray
    test('to test the property `isArray`', () async {
      // TODO
    });

    // String label
    test('to test the property `label`', () async {
      // TODO
    });

    // String placeholder
    test('to test the property `placeholder`', () async {
      // TODO
    });

    // String description
    test('to test the property `description`', () async {
      // TODO
    });

    // bool isRequired
    test('to test the property `isRequired`', () async {
      // TODO
    });

    // bool allowMultipleResponse
    test('to test the property `allowMultipleResponse`', () async {
      // TODO
    });

    // bool isGroup
    test('to test the property `isGroup`', () async {
      // TODO
    });

    // bool config
    test('to test the property `config`', () async {
      // TODO
    });

    // num order
    test('to test the property `order`', () async {
      // TODO
    });

    // num id
    test('to test the property `id`', () async {
      // TODO
    });

    // BuiltList<FormComponentsRequest> children
    test('to test the property `children`', () async {
      // TODO
    });
  });
}
