# qarar_api.model.ListUserNotificationsResponseDataResponse

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | 
**title** | **String** |  | 
**body** | **String** |  | 
**icon** | **String** |  | 
**data** | [**WebPushPayloadData**](WebPushPayloadData.md) |  | 
**recipientId** | **num** |  | 
**readAt** | [**JsonObject**](.md) |  | 
**trashedAt** | [**JsonObject**](.md) |  | 
**createdAt** | [**DateTime**](DateTime.md) |  | 
**notification** | [**GetNotificationResponseData**](GetNotificationResponseData.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


