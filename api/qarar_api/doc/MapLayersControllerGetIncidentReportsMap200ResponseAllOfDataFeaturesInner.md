# qarar_api.model.MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInner

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | 
**id** | **int** |  | 
**geometry** | [**JsonObject**](.md) |  | 
**properties** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


