# qarar_api.model.GetEquipmentDataResponse

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | 
**brand** | **String** |  | 
**model** | **String** |  | 
**plateNumber** | **String** |  | 
**vinNumber** | **String** |  | 
**localNumber** | **num** |  | 
**serialNumber** | **num** |  | 
**manufacturingYear** | **num** |  | 
**registrationExpiryDate** | [**DateTime**](DateTime.md) |  | 
**ownershipDate** | [**DateTime**](DateTime.md) |  | 
**createdAt** | [**DateTime**](DateTime.md) |  | 
**entity** | [**GetEntityDataResponse**](GetEntityDataResponse.md) |  | 
**equipmentsType** | [**GetEquipmentTypeDataResponse**](GetEquipmentTypeDataResponse.md) |  | 
**equipmentsStorePoint** | [**GetEquipmentStorePointDataResponse**](GetEquipmentStorePointDataResponse.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


