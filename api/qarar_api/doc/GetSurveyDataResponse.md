# qarar_api.model.GetSurveyDataResponse

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | 
**name** | **String** |  | 
**description** | **String** |  | 
**isActive** | **bool** |  | 
**startAt** | [**DateTime**](DateTime.md) |  | 
**endAt** | [**DateTime**](DateTime.md) |  | 
**createdAt** | [**DateTime**](DateTime.md) |  | 
**numberOfResponses** | **num** |  | 
**numberOfRecipients** | **num** |  | 
**creator** | [**GetUserDataResponse**](GetUserDataResponse.md) |  | 
**form** | [**GetFormDataResponse**](GetFormDataResponse.md) |  | 
**recipients** | [**BuiltList&lt;GetSurveyRecipientViewDataResponse&gt;**](GetSurveyRecipientViewDataResponse.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


