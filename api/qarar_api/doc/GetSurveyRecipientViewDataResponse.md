# qarar_api.model.GetSurveyRecipientViewDataResponse

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | 
**originalId** | **num** |  | 
**name** | **String** |  | 
**response** | [**GetSurveyResponseDataResponse**](GetSurveyResponseDataResponse.md) |  | 
**type** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


