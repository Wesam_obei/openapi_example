# qarar_api.api.HealthApi

## Load the API package
```dart
import 'package:qarar_api/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**healthControllerCheck**](HealthApi.md#healthcontrollercheck) | **GET** /api/v1/health | 


# **healthControllerCheck**
> HealthControllerCheck200Response healthControllerCheck(xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getHealthApi();
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.healthControllerCheck(xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling HealthApi->healthControllerCheck: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**HealthControllerCheck200Response**](HealthControllerCheck200Response.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

