# qarar_api.model.IncidentReportsDataRequest

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**envelope** | [**Envelope**](Envelope.md) |  | 
**fields** | **BuiltList&lt;String&gt;** |  | 
**statuses** | **BuiltList&lt;String&gt;** |  | 
**startTime** | [**DateTime**](DateTime.md) |  | 
**endTime** | [**DateTime**](DateTime.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


