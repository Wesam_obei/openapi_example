# qarar_api.model.LoginResponse

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessToken** | **String** |  | 
**accessTokenExpiredDate** | [**DateTime**](DateTime.md) |  | 
**refreshToken** | **String** |  | 
**refreshTokenExpiredDate** | [**DateTime**](DateTime.md) |  | 
**sessionId** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


