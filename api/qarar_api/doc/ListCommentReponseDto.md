# qarar_api.model.ListCommentReponseDto

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | 
**surveyResponseAnswer** | [**SurveyResponseAnswerDataResponse**](SurveyResponseAnswerDataResponse.md) |  | 
**comment** | **String** |  | 
**commentBy** | [**GetUserDataResponse**](GetUserDataResponse.md) |  | 
**createdAt** | [**DateTime**](DateTime.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


