# qarar_api.api.CoreApi

## Load the API package
```dart
import 'package:qarar_api/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**coreControllerUploadFile**](CoreApi.md#corecontrolleruploadfile) | **POST** /api/v1/core/upload-file | 


# **coreControllerUploadFile**
> CoreControllerUploadFile200Response coreControllerUploadFile(authorization, file, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getCoreApi();
final String authorization = authorization_example; // String | Bearer {token}
final MultipartFile file = BINARY_DATA_HERE; // MultipartFile | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.coreControllerUploadFile(authorization, file, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling CoreApi->coreControllerUploadFile: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **file** | **MultipartFile**|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**CoreControllerUploadFile200Response**](CoreControllerUploadFile200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

