# qarar_api.model.ListPermissionCategoryDataResponse

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | 
**name** | **String** |  | 
**label** | **String** |  | 
**description** | **String** |  | 
**order** | **num** |  | 
**createdAt** | [**DateTime**](DateTime.md) |  | 
**permissions** | [**BuiltList&lt;PermissionDataResponse&gt;**](PermissionDataResponse.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


