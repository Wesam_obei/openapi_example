# qarar_api.model.PaginatedLinksDocumented

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first** | **String** |  | [optional] 
**previous** | **String** |  | [optional] 
**current** | **String** |  | [optional] 
**next** | **String** |  | [optional] 
**last** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


