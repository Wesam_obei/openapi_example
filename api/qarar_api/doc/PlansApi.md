# qarar_api.api.PlansApi

## Load the API package
```dart
import 'package:qarar_api/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**plansControllerCreate**](PlansApi.md#planscontrollercreate) | **POST** /api/v1/plans | 
[**plansControllerDelete**](PlansApi.md#planscontrollerdelete) | **DELETE** /api/v1/plans/{id} | 
[**plansControllerGet**](PlansApi.md#planscontrollerget) | **GET** /api/v1/plans/{id} | 
[**plansControllerList**](PlansApi.md#planscontrollerlist) | **GET** /api/v1/plans | 
[**plansControllerUpdate**](PlansApi.md#planscontrollerupdate) | **PATCH** /api/v1/plans/{id} | 


# **plansControllerCreate**
> plansControllerCreate(authorization, planCreateRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getPlansApi();
final String authorization = authorization_example; // String | Bearer {token}
final PlanCreateRequest planCreateRequest = ; // PlanCreateRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    api.plansControllerCreate(authorization, planCreateRequest, xLanguage, contentType);
} catch on DioException (e) {
    print('Exception when calling PlansApi->plansControllerCreate: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **planCreateRequest** | [**PlanCreateRequest**](PlanCreateRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plansControllerDelete**
> plansControllerDelete(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getPlansApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    api.plansControllerDelete(id, authorization, xLanguage, contentType);
} catch on DioException (e) {
    print('Exception when calling PlansApi->plansControllerDelete: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plansControllerGet**
> PlansControllerGet200Response plansControllerGet(id, authorization, includes, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getPlansApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final String includes = includes_example; // String | List of relations to select.     <p>              <b>Example: </b> versions,versions.file           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>     
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.plansControllerGet(id, authorization, includes, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling PlansApi->plansControllerGet: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **includes** | **String**| List of relations to select.     <p>              <b>Example: </b> versions,versions.file           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>      | [optional] 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**PlansControllerGet200Response**](PlansControllerGet200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plansControllerList**
> PlansControllerList200Response plansControllerList(authorization, page, limit, filterPeriodId, filterPeriodName, filterPeriodDescription, filterPeriodVersionPeriodName, sortBy, search, searchBy, includes, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getPlansApi();
final String authorization = authorization_example; // String | Bearer {token}
final num page = 8.14; // num | Page number to retrieve.If you provide invalid value the default page number will applied         <p>              <b>Example: </b> 1           </p>         <p>              <b>Default Value: </b> 1           </p>         
final num limit = 8.14; // num | Number of records per page.       <p>              <b>Example: </b> 10           </p>       <p>              <b>Default Value: </b> 20           </p>       <p>              <b>Max Value: </b> 100           </p>               If provided value is greater than max value, max value will be applied.       
final BuiltList<String> filterPeriodId = ; // BuiltList<String> | Filter by id query param.           <p>              <b>Format: </b> filter.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.id=$not:$like:John Doe&filter.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodName = ; // BuiltList<String> | Filter by name query param.           <p>              <b>Format: </b> filter.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.name=$not:$like:John Doe&filter.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodDescription = ; // BuiltList<String> | Filter by description query param.           <p>              <b>Format: </b> filter.description={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.description=$not:$like:John Doe&filter.description=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodVersionPeriodName = ; // BuiltList<String> | Filter by version.name query param.           <p>              <b>Format: </b> filter.version.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.version.name=$not:$like:John Doe&filter.version.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> sortBy = ; // BuiltList<String> | Parameter to sort by.       <p>To sort by multiple fields, just provide query param multiple types. The order in url defines an order of sorting</p>       <p>              <b>Format: </b> fieldName:DIRECTION           </p>       <p>              <b>Example: </b> sortBy=id:DESC&sortBy=createdAt:ASC           </p>       <p>              <b>Default Value: </b> id:DESC           </p>       <h4>Available Fields</h4><ul><li>id</li> <li>name</li> <li>versions.created_at</li></ul>       
final String search = search_example; // String | Search term to filter result values         <p>              <b>Example: </b> John           </p>         <p>              <b>Default Value: </b> No default value           </p>         
final String searchBy = searchBy_example; // String | List of fields to search by term to filter result values         <p>              <b>Example: </b> id,name           </p>         <p>              <b>Default Value: </b> By default all fields mentioned below will be used to search by term           </p>         <h4>Available Fields</h4><ul><li>id</li> <li>name</li></ul>         
final String includes = includes_example; // String | List of relations to select.     <p>              <b>Example: </b> versions,versions.file           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>     
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.plansControllerList(authorization, page, limit, filterPeriodId, filterPeriodName, filterPeriodDescription, filterPeriodVersionPeriodName, sortBy, search, searchBy, includes, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling PlansApi->plansControllerList: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **page** | **num**| Page number to retrieve.If you provide invalid value the default page number will applied         <p>              <b>Example: </b> 1           </p>         <p>              <b>Default Value: </b> 1           </p>          | [optional] 
 **limit** | **num**| Number of records per page.       <p>              <b>Example: </b> 10           </p>       <p>              <b>Default Value: </b> 20           </p>       <p>              <b>Max Value: </b> 100           </p>               If provided value is greater than max value, max value will be applied.        | [optional] 
 **filterPeriodId** | [**BuiltList&lt;String&gt;**](String.md)| Filter by id query param.           <p>              <b>Format: </b> filter.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.id=$not:$like:John Doe&filter.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodName** | [**BuiltList&lt;String&gt;**](String.md)| Filter by name query param.           <p>              <b>Format: </b> filter.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.name=$not:$like:John Doe&filter.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodDescription** | [**BuiltList&lt;String&gt;**](String.md)| Filter by description query param.           <p>              <b>Format: </b> filter.description={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.description=$not:$like:John Doe&filter.description=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodVersionPeriodName** | [**BuiltList&lt;String&gt;**](String.md)| Filter by version.name query param.           <p>              <b>Format: </b> filter.version.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.version.name=$not:$like:John Doe&filter.version.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **sortBy** | [**BuiltList&lt;String&gt;**](String.md)| Parameter to sort by.       <p>To sort by multiple fields, just provide query param multiple types. The order in url defines an order of sorting</p>       <p>              <b>Format: </b> fieldName:DIRECTION           </p>       <p>              <b>Example: </b> sortBy=id:DESC&sortBy=createdAt:ASC           </p>       <p>              <b>Default Value: </b> id:DESC           </p>       <h4>Available Fields</h4><ul><li>id</li> <li>name</li> <li>versions.created_at</li></ul>        | [optional] 
 **search** | **String**| Search term to filter result values         <p>              <b>Example: </b> John           </p>         <p>              <b>Default Value: </b> No default value           </p>          | [optional] 
 **searchBy** | **String**| List of fields to search by term to filter result values         <p>              <b>Example: </b> id,name           </p>         <p>              <b>Default Value: </b> By default all fields mentioned below will be used to search by term           </p>         <h4>Available Fields</h4><ul><li>id</li> <li>name</li></ul>          | [optional] 
 **includes** | **String**| List of relations to select.     <p>              <b>Example: </b> versions,versions.file           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>      | [optional] 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**PlansControllerList200Response**](PlansControllerList200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plansControllerUpdate**
> plansControllerUpdate(id, authorization, planUpdateRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getPlansApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final PlanUpdateRequest planUpdateRequest = ; // PlanUpdateRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    api.plansControllerUpdate(id, authorization, planUpdateRequest, xLanguage, contentType);
} catch on DioException (e) {
    print('Exception when calling PlansApi->plansControllerUpdate: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **planUpdateRequest** | [**PlanUpdateRequest**](PlanUpdateRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

