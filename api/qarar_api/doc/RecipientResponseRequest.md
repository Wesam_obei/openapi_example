# qarar_api.model.RecipientResponseRequest

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recipientId** | **num** |  | 
**status** | **String** |  | 
**answers** | [**BuiltList&lt;AnswerRequest&gt;**](AnswerRequest.md) |  | 
**deletedMultiAnswerIds** | **BuiltList&lt;num&gt;** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


