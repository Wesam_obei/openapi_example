# qarar_api.model.MapLayersControllerGetConstructionLicensesMap200Response

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | **bool** |  | 
**data** | [**MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData**](MapLayersControllerGetConstructionLicensesMap200ResponseAllOfData.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


