# qarar_api.model.SurveyResponseAnswerDataResponse

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | 
**parentAnswerId** | **num** |  | 
**order** | **num** |  | 
**value** | [**JsonObject**](.md) |  | 
**formComponentId** | **num** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


