# qarar_api.model.ListSurveyResponseDataResponse

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | 
**createdAt** | [**DateTime**](DateTime.md) |  | 
**recipientId** | **num** |  | 
**answeredBy** | [**GetUserDataResponse**](GetUserDataResponse.md) |  | 
**responseStatus** | [**StatusResponse**](StatusResponse.md) |  | 
**surveyResponseAnswer** | [**BuiltList&lt;SurveyResponseAnswerDataResponse&gt;**](SurveyResponseAnswerDataResponse.md) |  | 
**survey** | [**GetSurveyDataResponse**](GetSurveyDataResponse.md) |  | 
**recipient** | [**GetSurveyRecipientViewDataResponse**](GetSurveyRecipientViewDataResponse.md) |  | 
**canAppliedReview** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


