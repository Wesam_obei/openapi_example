# qarar_api.model.SurveysControllerList200Response

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | **bool** |  | 
**data** | [**BuiltList&lt;ListSurveyDataResponse&gt;**](ListSurveyDataResponse.md) |  | 
**meta** | [**PaginatedMetaDocumented**](PaginatedMetaDocumented.md) |  | 
**links** | [**PaginatedLinksDocumented**](PaginatedLinksDocumented.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


