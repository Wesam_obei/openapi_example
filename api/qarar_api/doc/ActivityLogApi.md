# qarar_api.api.ActivityLogApi

## Load the API package
```dart
import 'package:qarar_api/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activityLogControllerLastLogin**](ActivityLogApi.md#activitylogcontrollerlastlogin) | **GET** /api/v1/activity-log/last-login | 
[**activityLogControllerUserActivity**](ActivityLogApi.md#activitylogcontrolleruseractivity) | **GET** /api/v1/activity-log/user-activity | 
[**activityLogControllerUsersActivity**](ActivityLogApi.md#activitylogcontrollerusersactivity) | **GET** /api/v1/activity-log/users-activity | 


# **activityLogControllerLastLogin**
> ActivityLogControllerLastLogin200Response activityLogControllerLastLogin(authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getActivityLogApi();
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.activityLogControllerLastLogin(authorization, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling ActivityLogApi->activityLogControllerLastLogin: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**ActivityLogControllerLastLogin200Response**](ActivityLogControllerLastLogin200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activityLogControllerUserActivity**
> ActivityLogControllerUserActivity200Response activityLogControllerUserActivity(authorization, page, limit, filterPeriodUserPeriodId, filterPeriodUserPeriodFullName, filterPeriodUserPeriodEmail, filterPeriodEventPeriodName, sortBy, search, searchBy, includes, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getActivityLogApi();
final String authorization = authorization_example; // String | Bearer {token}
final num page = 8.14; // num | Page number to retrieve.If you provide invalid value the default page number will applied         <p>              <b>Example: </b> 1           </p>         <p>              <b>Default Value: </b> 1           </p>         
final num limit = 8.14; // num | Number of records per page.       <p>              <b>Example: </b> 10           </p>       <p>              <b>Default Value: </b> 20           </p>       <p>              <b>Max Value: </b> 100           </p>               If provided value is greater than max value, max value will be applied.       
final BuiltList<String> filterPeriodUserPeriodId = ; // BuiltList<String> | Filter by user.id query param.           <p>              <b>Format: </b> filter.user.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.user.id=$not:$like:John Doe&filter.user.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$in</li> <li>$not</li></ul>
final BuiltList<String> filterPeriodUserPeriodFullName = ; // BuiltList<String> | Filter by user.full_name query param.           <p>              <b>Format: </b> filter.user.full_name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.user.full_name=$not:$like:John Doe&filter.user.full_name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodUserPeriodEmail = ; // BuiltList<String> | Filter by user.email query param.           <p>              <b>Format: </b> filter.user.email={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.user.email=$not:$like:John Doe&filter.user.email=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodEventPeriodName = ; // BuiltList<String> | Filter by event.name query param.           <p>              <b>Format: </b> filter.event.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.event.name=$not:$like:John Doe&filter.event.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> sortBy = ; // BuiltList<String> | Parameter to sort by.       <p>To sort by multiple fields, just provide query param multiple types. The order in url defines an order of sorting</p>       <p>              <b>Format: </b> fieldName:DIRECTION           </p>       <p>              <b>Example: </b> sortBy=id:DESC&sortBy=createdAt:ASC           </p>       <p>              <b>Default Value: </b> id:DESC           </p>       <h4>Available Fields</h4><ul><li>id</li> <li>event_id</li></ul>       
final String search = search_example; // String | Search term to filter result values         <p>              <b>Example: </b> John           </p>         <p>              <b>Default Value: </b> No default value           </p>         
final String searchBy = searchBy_example; // String | List of fields to search by term to filter result values         <p>              <b>Example: </b> id,event_id           </p>         <p>              <b>Default Value: </b> By default all fields mentioned below will be used to search by term           </p>         <h4>Available Fields</h4><ul><li>id</li> <li>event_id</li></ul>         
final String includes = includes_example; // String | List of relations to select.     <p>              <b>Example: </b> user,event           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>     
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.activityLogControllerUserActivity(authorization, page, limit, filterPeriodUserPeriodId, filterPeriodUserPeriodFullName, filterPeriodUserPeriodEmail, filterPeriodEventPeriodName, sortBy, search, searchBy, includes, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling ActivityLogApi->activityLogControllerUserActivity: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **page** | **num**| Page number to retrieve.If you provide invalid value the default page number will applied         <p>              <b>Example: </b> 1           </p>         <p>              <b>Default Value: </b> 1           </p>          | [optional] 
 **limit** | **num**| Number of records per page.       <p>              <b>Example: </b> 10           </p>       <p>              <b>Default Value: </b> 20           </p>       <p>              <b>Max Value: </b> 100           </p>               If provided value is greater than max value, max value will be applied.        | [optional] 
 **filterPeriodUserPeriodId** | [**BuiltList&lt;String&gt;**](String.md)| Filter by user.id query param.           <p>              <b>Format: </b> filter.user.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.user.id=$not:$like:John Doe&filter.user.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$in</li> <li>$not</li></ul> | [optional] 
 **filterPeriodUserPeriodFullName** | [**BuiltList&lt;String&gt;**](String.md)| Filter by user.full_name query param.           <p>              <b>Format: </b> filter.user.full_name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.user.full_name=$not:$like:John Doe&filter.user.full_name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodUserPeriodEmail** | [**BuiltList&lt;String&gt;**](String.md)| Filter by user.email query param.           <p>              <b>Format: </b> filter.user.email={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.user.email=$not:$like:John Doe&filter.user.email=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodEventPeriodName** | [**BuiltList&lt;String&gt;**](String.md)| Filter by event.name query param.           <p>              <b>Format: </b> filter.event.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.event.name=$not:$like:John Doe&filter.event.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **sortBy** | [**BuiltList&lt;String&gt;**](String.md)| Parameter to sort by.       <p>To sort by multiple fields, just provide query param multiple types. The order in url defines an order of sorting</p>       <p>              <b>Format: </b> fieldName:DIRECTION           </p>       <p>              <b>Example: </b> sortBy=id:DESC&sortBy=createdAt:ASC           </p>       <p>              <b>Default Value: </b> id:DESC           </p>       <h4>Available Fields</h4><ul><li>id</li> <li>event_id</li></ul>        | [optional] 
 **search** | **String**| Search term to filter result values         <p>              <b>Example: </b> John           </p>         <p>              <b>Default Value: </b> No default value           </p>          | [optional] 
 **searchBy** | **String**| List of fields to search by term to filter result values         <p>              <b>Example: </b> id,event_id           </p>         <p>              <b>Default Value: </b> By default all fields mentioned below will be used to search by term           </p>         <h4>Available Fields</h4><ul><li>id</li> <li>event_id</li></ul>          | [optional] 
 **includes** | **String**| List of relations to select.     <p>              <b>Example: </b> user,event           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>      | [optional] 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**ActivityLogControllerUserActivity200Response**](ActivityLogControllerUserActivity200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activityLogControllerUsersActivity**
> ActivityLogControllerUserActivity200Response activityLogControllerUsersActivity(authorization, page, limit, filterPeriodUserPeriodId, filterPeriodUserPeriodFullName, filterPeriodUserPeriodEmail, filterPeriodEventPeriodName, sortBy, search, searchBy, includes, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getActivityLogApi();
final String authorization = authorization_example; // String | Bearer {token}
final num page = 8.14; // num | Page number to retrieve.If you provide invalid value the default page number will applied         <p>              <b>Example: </b> 1           </p>         <p>              <b>Default Value: </b> 1           </p>         
final num limit = 8.14; // num | Number of records per page.       <p>              <b>Example: </b> 10           </p>       <p>              <b>Default Value: </b> 20           </p>       <p>              <b>Max Value: </b> 100           </p>               If provided value is greater than max value, max value will be applied.       
final BuiltList<String> filterPeriodUserPeriodId = ; // BuiltList<String> | Filter by user.id query param.           <p>              <b>Format: </b> filter.user.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.user.id=$not:$like:John Doe&filter.user.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$in</li> <li>$not</li></ul>
final BuiltList<String> filterPeriodUserPeriodFullName = ; // BuiltList<String> | Filter by user.full_name query param.           <p>              <b>Format: </b> filter.user.full_name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.user.full_name=$not:$like:John Doe&filter.user.full_name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodUserPeriodEmail = ; // BuiltList<String> | Filter by user.email query param.           <p>              <b>Format: </b> filter.user.email={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.user.email=$not:$like:John Doe&filter.user.email=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodEventPeriodName = ; // BuiltList<String> | Filter by event.name query param.           <p>              <b>Format: </b> filter.event.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.event.name=$not:$like:John Doe&filter.event.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> sortBy = ; // BuiltList<String> | Parameter to sort by.       <p>To sort by multiple fields, just provide query param multiple types. The order in url defines an order of sorting</p>       <p>              <b>Format: </b> fieldName:DIRECTION           </p>       <p>              <b>Example: </b> sortBy=id:DESC&sortBy=createdAt:ASC           </p>       <p>              <b>Default Value: </b> id:DESC           </p>       <h4>Available Fields</h4><ul><li>id</li> <li>event_id</li></ul>       
final String search = search_example; // String | Search term to filter result values         <p>              <b>Example: </b> John           </p>         <p>              <b>Default Value: </b> No default value           </p>         
final String searchBy = searchBy_example; // String | List of fields to search by term to filter result values         <p>              <b>Example: </b> id,event_id           </p>         <p>              <b>Default Value: </b> By default all fields mentioned below will be used to search by term           </p>         <h4>Available Fields</h4><ul><li>id</li> <li>event_id</li></ul>         
final String includes = includes_example; // String | List of relations to select.     <p>              <b>Example: </b> user,event           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>     
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.activityLogControllerUsersActivity(authorization, page, limit, filterPeriodUserPeriodId, filterPeriodUserPeriodFullName, filterPeriodUserPeriodEmail, filterPeriodEventPeriodName, sortBy, search, searchBy, includes, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling ActivityLogApi->activityLogControllerUsersActivity: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **page** | **num**| Page number to retrieve.If you provide invalid value the default page number will applied         <p>              <b>Example: </b> 1           </p>         <p>              <b>Default Value: </b> 1           </p>          | [optional] 
 **limit** | **num**| Number of records per page.       <p>              <b>Example: </b> 10           </p>       <p>              <b>Default Value: </b> 20           </p>       <p>              <b>Max Value: </b> 100           </p>               If provided value is greater than max value, max value will be applied.        | [optional] 
 **filterPeriodUserPeriodId** | [**BuiltList&lt;String&gt;**](String.md)| Filter by user.id query param.           <p>              <b>Format: </b> filter.user.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.user.id=$not:$like:John Doe&filter.user.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$in</li> <li>$not</li></ul> | [optional] 
 **filterPeriodUserPeriodFullName** | [**BuiltList&lt;String&gt;**](String.md)| Filter by user.full_name query param.           <p>              <b>Format: </b> filter.user.full_name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.user.full_name=$not:$like:John Doe&filter.user.full_name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodUserPeriodEmail** | [**BuiltList&lt;String&gt;**](String.md)| Filter by user.email query param.           <p>              <b>Format: </b> filter.user.email={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.user.email=$not:$like:John Doe&filter.user.email=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodEventPeriodName** | [**BuiltList&lt;String&gt;**](String.md)| Filter by event.name query param.           <p>              <b>Format: </b> filter.event.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.event.name=$not:$like:John Doe&filter.event.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **sortBy** | [**BuiltList&lt;String&gt;**](String.md)| Parameter to sort by.       <p>To sort by multiple fields, just provide query param multiple types. The order in url defines an order of sorting</p>       <p>              <b>Format: </b> fieldName:DIRECTION           </p>       <p>              <b>Example: </b> sortBy=id:DESC&sortBy=createdAt:ASC           </p>       <p>              <b>Default Value: </b> id:DESC           </p>       <h4>Available Fields</h4><ul><li>id</li> <li>event_id</li></ul>        | [optional] 
 **search** | **String**| Search term to filter result values         <p>              <b>Example: </b> John           </p>         <p>              <b>Default Value: </b> No default value           </p>          | [optional] 
 **searchBy** | **String**| List of fields to search by term to filter result values         <p>              <b>Example: </b> id,event_id           </p>         <p>              <b>Default Value: </b> By default all fields mentioned below will be used to search by term           </p>         <h4>Available Fields</h4><ul><li>id</li> <li>event_id</li></ul>          | [optional] 
 **includes** | **String**| List of relations to select.     <p>              <b>Example: </b> user,event           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>      | [optional] 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**ActivityLogControllerUserActivity200Response**](ActivityLogControllerUserActivity200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

