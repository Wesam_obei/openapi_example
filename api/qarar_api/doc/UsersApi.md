# qarar_api.api.UsersApi

## Load the API package
```dart
import 'package:qarar_api/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**usersControllerCreate**](UsersApi.md#userscontrollercreate) | **POST** /api/v1/users | 
[**usersControllerDelete**](UsersApi.md#userscontrollerdelete) | **DELETE** /api/v1/users/{id} | 
[**usersControllerEmployeesList**](UsersApi.md#userscontrolleremployeeslist) | **GET** /api/v1/users/employees-list | 
[**usersControllerGet**](UsersApi.md#userscontrollerget) | **GET** /api/v1/users/{id} | 
[**usersControllerList**](UsersApi.md#userscontrollerlist) | **GET** /api/v1/users | 
[**usersControllerUpdate**](UsersApi.md#userscontrollerupdate) | **PATCH** /api/v1/users/{id} | 


# **usersControllerCreate**
> usersControllerCreate(authorization, userCreateRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getUsersApi();
final String authorization = authorization_example; // String | Bearer {token}
final UserCreateRequest userCreateRequest = ; // UserCreateRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    api.usersControllerCreate(authorization, userCreateRequest, xLanguage, contentType);
} catch on DioException (e) {
    print('Exception when calling UsersApi->usersControllerCreate: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **userCreateRequest** | [**UserCreateRequest**](UserCreateRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **usersControllerDelete**
> usersControllerDelete(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getUsersApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    api.usersControllerDelete(id, authorization, xLanguage, contentType);
} catch on DioException (e) {
    print('Exception when calling UsersApi->usersControllerDelete: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **usersControllerEmployeesList**
> UsersControllerEmployeesList200Response usersControllerEmployeesList(authorization, search, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getUsersApi();
final String authorization = authorization_example; // String | Bearer {token}
final String search = search_example; // String | Optional search query parameter
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.usersControllerEmployeesList(authorization, search, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling UsersApi->usersControllerEmployeesList: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **search** | **String**| Optional search query parameter | [optional] 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**UsersControllerEmployeesList200Response**](UsersControllerEmployeesList200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **usersControllerGet**
> UsersControllerGet200Response usersControllerGet(id, authorization, includes, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getUsersApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final String includes = includes_example; // String | List of relations to select.     <p>              <b>Example: </b> roles,roles.permissions.category,permissions,permissions.category,entities           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>     
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.usersControllerGet(id, authorization, includes, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling UsersApi->usersControllerGet: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **includes** | **String**| List of relations to select.     <p>              <b>Example: </b> roles,roles.permissions.category,permissions,permissions.category,entities           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>      | [optional] 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**UsersControllerGet200Response**](UsersControllerGet200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **usersControllerList**
> UsersControllerList200Response usersControllerList(authorization, page, limit, filterPeriodId, filterPeriodFullName, filterPeriodEmail, filterPeriodIsActive, filterPeriodRolePeriodName, filterPeriodRolePeriodPermissionsPeriodName, filterPeriodRolePeriodPermissionsPeriodCategoryPeriodName, filterPeriodEntitiesPeriodId, sortBy, search, searchBy, includes, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getUsersApi();
final String authorization = authorization_example; // String | Bearer {token}
final num page = 8.14; // num | Page number to retrieve.If you provide invalid value the default page number will applied         <p>              <b>Example: </b> 1           </p>         <p>              <b>Default Value: </b> 1           </p>         
final num limit = 8.14; // num | Number of records per page.       <p>              <b>Example: </b> 10           </p>       <p>              <b>Default Value: </b> 20           </p>       <p>              <b>Max Value: </b> 100           </p>       <p>              <b>Retrieve All Data: </b> -1           </p>        If provided value is greater than max value, max value will be applied.       
final BuiltList<String> filterPeriodId = ; // BuiltList<String> | Filter by id query param.           <p>              <b>Format: </b> filter.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.id=$not:$like:John Doe&filter.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$in</li> <li>$not</li></ul>
final BuiltList<String> filterPeriodFullName = ; // BuiltList<String> | Filter by full_name query param.           <p>              <b>Format: </b> filter.full_name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.full_name=$not:$like:John Doe&filter.full_name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodEmail = ; // BuiltList<String> | Filter by email query param.           <p>              <b>Format: </b> filter.email={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.email=$not:$like:John Doe&filter.email=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodIsActive = ; // BuiltList<String> | Filter by is_active query param.           <p>              <b>Format: </b> filter.is_active={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.is_active=$not:$like:John Doe&filter.is_active=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodRolePeriodName = ; // BuiltList<String> | Filter by role.name query param.           <p>              <b>Format: </b> filter.role.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.role.name=$not:$like:John Doe&filter.role.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodRolePeriodPermissionsPeriodName = ; // BuiltList<String> | Filter by role.permissions.name query param.           <p>              <b>Format: </b> filter.role.permissions.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.role.permissions.name=$not:$like:John Doe&filter.role.permissions.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodRolePeriodPermissionsPeriodCategoryPeriodName = ; // BuiltList<String> | Filter by role.permissions.category.name query param.           <p>              <b>Format: </b> filter.role.permissions.category.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.role.permissions.category.name=$not:$like:John Doe&filter.role.permissions.category.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodEntitiesPeriodId = ; // BuiltList<String> | Filter by entities.id query param.           <p>              <b>Format: </b> filter.entities.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.entities.id=$not:$like:John Doe&filter.entities.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$in</li> <li>$not</li></ul>
final BuiltList<String> sortBy = ; // BuiltList<String> | Parameter to sort by.       <p>To sort by multiple fields, just provide query param multiple types. The order in url defines an order of sorting</p>       <p>              <b>Format: </b> fieldName:DIRECTION           </p>       <p>              <b>Example: </b> sortBy=id:DESC&sortBy=createdAt:ASC           </p>       <p>              <b>Default Value: </b> id:DESC           </p>       <h4>Available Fields</h4><ul><li>id</li> <li>email</li> <li>full_name</li> <li>is_active</li></ul>       
final String search = search_example; // String | Search term to filter result values         <p>              <b>Example: </b> John           </p>         <p>              <b>Default Value: </b> No default value           </p>         
final String searchBy = searchBy_example; // String | List of fields to search by term to filter result values         <p>              <b>Example: </b> id,email,full_name           </p>         <p>              <b>Default Value: </b> By default all fields mentioned below will be used to search by term           </p>         <h4>Available Fields</h4><ul><li>id</li> <li>email</li> <li>full_name</li></ul>         
final String includes = includes_example; // String | List of relations to select.     <p>              <b>Example: </b> roles,roles.permissions.category,permissions,permissions.category,entities           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>     
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.usersControllerList(authorization, page, limit, filterPeriodId, filterPeriodFullName, filterPeriodEmail, filterPeriodIsActive, filterPeriodRolePeriodName, filterPeriodRolePeriodPermissionsPeriodName, filterPeriodRolePeriodPermissionsPeriodCategoryPeriodName, filterPeriodEntitiesPeriodId, sortBy, search, searchBy, includes, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling UsersApi->usersControllerList: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **page** | **num**| Page number to retrieve.If you provide invalid value the default page number will applied         <p>              <b>Example: </b> 1           </p>         <p>              <b>Default Value: </b> 1           </p>          | [optional] 
 **limit** | **num**| Number of records per page.       <p>              <b>Example: </b> 10           </p>       <p>              <b>Default Value: </b> 20           </p>       <p>              <b>Max Value: </b> 100           </p>       <p>              <b>Retrieve All Data: </b> -1           </p>        If provided value is greater than max value, max value will be applied.        | [optional] 
 **filterPeriodId** | [**BuiltList&lt;String&gt;**](String.md)| Filter by id query param.           <p>              <b>Format: </b> filter.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.id=$not:$like:John Doe&filter.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$in</li> <li>$not</li></ul> | [optional] 
 **filterPeriodFullName** | [**BuiltList&lt;String&gt;**](String.md)| Filter by full_name query param.           <p>              <b>Format: </b> filter.full_name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.full_name=$not:$like:John Doe&filter.full_name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodEmail** | [**BuiltList&lt;String&gt;**](String.md)| Filter by email query param.           <p>              <b>Format: </b> filter.email={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.email=$not:$like:John Doe&filter.email=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodIsActive** | [**BuiltList&lt;String&gt;**](String.md)| Filter by is_active query param.           <p>              <b>Format: </b> filter.is_active={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.is_active=$not:$like:John Doe&filter.is_active=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodRolePeriodName** | [**BuiltList&lt;String&gt;**](String.md)| Filter by role.name query param.           <p>              <b>Format: </b> filter.role.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.role.name=$not:$like:John Doe&filter.role.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodRolePeriodPermissionsPeriodName** | [**BuiltList&lt;String&gt;**](String.md)| Filter by role.permissions.name query param.           <p>              <b>Format: </b> filter.role.permissions.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.role.permissions.name=$not:$like:John Doe&filter.role.permissions.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodRolePeriodPermissionsPeriodCategoryPeriodName** | [**BuiltList&lt;String&gt;**](String.md)| Filter by role.permissions.category.name query param.           <p>              <b>Format: </b> filter.role.permissions.category.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.role.permissions.category.name=$not:$like:John Doe&filter.role.permissions.category.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodEntitiesPeriodId** | [**BuiltList&lt;String&gt;**](String.md)| Filter by entities.id query param.           <p>              <b>Format: </b> filter.entities.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.entities.id=$not:$like:John Doe&filter.entities.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$in</li> <li>$not</li></ul> | [optional] 
 **sortBy** | [**BuiltList&lt;String&gt;**](String.md)| Parameter to sort by.       <p>To sort by multiple fields, just provide query param multiple types. The order in url defines an order of sorting</p>       <p>              <b>Format: </b> fieldName:DIRECTION           </p>       <p>              <b>Example: </b> sortBy=id:DESC&sortBy=createdAt:ASC           </p>       <p>              <b>Default Value: </b> id:DESC           </p>       <h4>Available Fields</h4><ul><li>id</li> <li>email</li> <li>full_name</li> <li>is_active</li></ul>        | [optional] 
 **search** | **String**| Search term to filter result values         <p>              <b>Example: </b> John           </p>         <p>              <b>Default Value: </b> No default value           </p>          | [optional] 
 **searchBy** | **String**| List of fields to search by term to filter result values         <p>              <b>Example: </b> id,email,full_name           </p>         <p>              <b>Default Value: </b> By default all fields mentioned below will be used to search by term           </p>         <h4>Available Fields</h4><ul><li>id</li> <li>email</li> <li>full_name</li></ul>          | [optional] 
 **includes** | **String**| List of relations to select.     <p>              <b>Example: </b> roles,roles.permissions.category,permissions,permissions.category,entities           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>      | [optional] 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**UsersControllerList200Response**](UsersControllerList200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **usersControllerUpdate**
> usersControllerUpdate(id, authorization, userUpdateRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getUsersApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final UserUpdateRequest userUpdateRequest = ; // UserUpdateRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    api.usersControllerUpdate(id, authorization, userUpdateRequest, xLanguage, contentType);
} catch on DioException (e) {
    print('Exception when calling UsersApi->usersControllerUpdate: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **userUpdateRequest** | [**UserUpdateRequest**](UserUpdateRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

