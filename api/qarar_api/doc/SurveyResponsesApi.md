# qarar_api.api.SurveyResponsesApi

## Load the API package
```dart
import 'package:qarar_api/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**surveyResponsesControllerList**](SurveyResponsesApi.md#surveyresponsescontrollerlist) | **GET** /api/v1/survey-responses/{survey_id} | 
[**surveyResponsesControllerRecipientResponse**](SurveyResponsesApi.md#surveyresponsescontrollerrecipientresponse) | **POST** /api/v1/survey-responses/{survey_id}/recipient/responses | 
[**surveyResponsesControllerResponseDetails**](SurveyResponsesApi.md#surveyresponsescontrollerresponsedetails) | **GET** /api/v1/survey-responses/{survey_id}/responses/{response_id} | 
[**surveyResponsesControllerVerifierResponse**](SurveyResponsesApi.md#surveyresponsescontrollerverifierresponse) | **POST** /api/v1/survey-responses/{survey_id}/verifier/responses | 


# **surveyResponsesControllerList**
> SurveyResponsesControllerList200Response surveyResponsesControllerList(surveyId, authorization, page, limit, filterPeriodId, filterPeriodResponseStatusPeriodName, sortBy, search, searchBy, includes, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getSurveyResponsesApi();
final String surveyId = surveyId_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final num page = 8.14; // num | Page number to retrieve.If you provide invalid value the default page number will applied         <p>              <b>Example: </b> 1           </p>         <p>              <b>Default Value: </b> 1           </p>         
final num limit = 8.14; // num | Number of records per page.       <p>              <b>Example: </b> 10           </p>       <p>              <b>Default Value: </b> 20           </p>       <p>              <b>Max Value: </b> 100           </p>       <p>              <b>Retrieve All Data: </b> -1           </p>        If provided value is greater than max value, max value will be applied.       
final BuiltList<String> filterPeriodId = ; // BuiltList<String> | Filter by id query param.           <p>              <b>Format: </b> filter.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.id=$not:$like:John Doe&filter.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$in</li> <li>$not</li></ul>
final BuiltList<String> filterPeriodResponseStatusPeriodName = ; // BuiltList<String> | Filter by responseStatus.name query param.           <p>              <b>Format: </b> filter.responseStatus.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.responseStatus.name=$not:$like:John Doe&filter.responseStatus.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$in</li> <li>$not</li></ul>
final BuiltList<String> sortBy = ; // BuiltList<String> | Parameter to sort by.       <p>To sort by multiple fields, just provide query param multiple types. The order in url defines an order of sorting</p>       <p>              <b>Format: </b> fieldName:DIRECTION           </p>       <p>              <b>Example: </b> sortBy=id:DESC&sortBy=createdAt:ASC           </p>       <p>              <b>Default Value: </b> id:DESC           </p>       <h4>Available Fields</h4><ul><li>id</li></ul>       
final String search = search_example; // String | Search term to filter result values         <p>              <b>Example: </b> John           </p>         <p>              <b>Default Value: </b> No default value           </p>         
final String searchBy = searchBy_example; // String | List of fields to search by term to filter result values         <p>              <b>Example: </b> answered_by,survey_id,response_status_id,recipient_id           </p>         <p>              <b>Default Value: </b> By default all fields mentioned below will be used to search by term           </p>         <h4>Available Fields</h4><ul><li>answered_by</li> <li>survey_id</li> <li>response_status_id</li> <li>recipient_id</li></ul>         
final String includes = includes_example; // String | List of relations to select.     <p>              <b>Example: </b> survey,survey.form,survey.form.formComponents,survey.form.formComponents.responseDataType,surveyResponseAnswer           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>     
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.surveyResponsesControllerList(surveyId, authorization, page, limit, filterPeriodId, filterPeriodResponseStatusPeriodName, sortBy, search, searchBy, includes, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling SurveyResponsesApi->surveyResponsesControllerList: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **surveyId** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **page** | **num**| Page number to retrieve.If you provide invalid value the default page number will applied         <p>              <b>Example: </b> 1           </p>         <p>              <b>Default Value: </b> 1           </p>          | [optional] 
 **limit** | **num**| Number of records per page.       <p>              <b>Example: </b> 10           </p>       <p>              <b>Default Value: </b> 20           </p>       <p>              <b>Max Value: </b> 100           </p>       <p>              <b>Retrieve All Data: </b> -1           </p>        If provided value is greater than max value, max value will be applied.        | [optional] 
 **filterPeriodId** | [**BuiltList&lt;String&gt;**](String.md)| Filter by id query param.           <p>              <b>Format: </b> filter.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.id=$not:$like:John Doe&filter.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$in</li> <li>$not</li></ul> | [optional] 
 **filterPeriodResponseStatusPeriodName** | [**BuiltList&lt;String&gt;**](String.md)| Filter by responseStatus.name query param.           <p>              <b>Format: </b> filter.responseStatus.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.responseStatus.name=$not:$like:John Doe&filter.responseStatus.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$in</li> <li>$not</li></ul> | [optional] 
 **sortBy** | [**BuiltList&lt;String&gt;**](String.md)| Parameter to sort by.       <p>To sort by multiple fields, just provide query param multiple types. The order in url defines an order of sorting</p>       <p>              <b>Format: </b> fieldName:DIRECTION           </p>       <p>              <b>Example: </b> sortBy=id:DESC&sortBy=createdAt:ASC           </p>       <p>              <b>Default Value: </b> id:DESC           </p>       <h4>Available Fields</h4><ul><li>id</li></ul>        | [optional] 
 **search** | **String**| Search term to filter result values         <p>              <b>Example: </b> John           </p>         <p>              <b>Default Value: </b> No default value           </p>          | [optional] 
 **searchBy** | **String**| List of fields to search by term to filter result values         <p>              <b>Example: </b> answered_by,survey_id,response_status_id,recipient_id           </p>         <p>              <b>Default Value: </b> By default all fields mentioned below will be used to search by term           </p>         <h4>Available Fields</h4><ul><li>answered_by</li> <li>survey_id</li> <li>response_status_id</li> <li>recipient_id</li></ul>          | [optional] 
 **includes** | **String**| List of relations to select.     <p>              <b>Example: </b> survey,survey.form,survey.form.formComponents,survey.form.formComponents.responseDataType,surveyResponseAnswer           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>      | [optional] 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**SurveyResponsesControllerList200Response**](SurveyResponsesControllerList200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **surveyResponsesControllerRecipientResponse**
> surveyResponsesControllerRecipientResponse(surveyId, authorization, recipientResponseRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getSurveyResponsesApi();
final String surveyId = surveyId_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final RecipientResponseRequest recipientResponseRequest = ; // RecipientResponseRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    api.surveyResponsesControllerRecipientResponse(surveyId, authorization, recipientResponseRequest, xLanguage, contentType);
} catch on DioException (e) {
    print('Exception when calling SurveyResponsesApi->surveyResponsesControllerRecipientResponse: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **surveyId** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **recipientResponseRequest** | [**RecipientResponseRequest**](RecipientResponseRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **surveyResponsesControllerResponseDetails**
> SurveyResponsesControllerResponseDetails200Response surveyResponsesControllerResponseDetails(responseId, surveyId, authorization, includes, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getSurveyResponsesApi();
final String responseId = responseId_example; // String | 
final String surveyId = surveyId_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final String includes = includes_example; // String | List of relations to select.     <p>              <b>Example: </b> survey,survey.form,survey.form.formComponents,survey.form.formComponents.responseDataType,surveyResponseAnswer           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>     
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.surveyResponsesControllerResponseDetails(responseId, surveyId, authorization, includes, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling SurveyResponsesApi->surveyResponsesControllerResponseDetails: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **responseId** | **String**|  | 
 **surveyId** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **includes** | **String**| List of relations to select.     <p>              <b>Example: </b> survey,survey.form,survey.form.formComponents,survey.form.formComponents.responseDataType,surveyResponseAnswer           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>      | [optional] 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**SurveyResponsesControllerResponseDetails200Response**](SurveyResponsesControllerResponseDetails200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **surveyResponsesControllerVerifierResponse**
> surveyResponsesControllerVerifierResponse(surveyId, authorization, verifierResponseRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getSurveyResponsesApi();
final String surveyId = surveyId_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final VerifierResponseRequest verifierResponseRequest = ; // VerifierResponseRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    api.surveyResponsesControllerVerifierResponse(surveyId, authorization, verifierResponseRequest, xLanguage, contentType);
} catch on DioException (e) {
    print('Exception when calling SurveyResponsesApi->surveyResponsesControllerVerifierResponse: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **surveyId** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **verifierResponseRequest** | [**VerifierResponseRequest**](VerifierResponseRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

