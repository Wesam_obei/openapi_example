# qarar_api.model.ListFormDataResponse

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | 
**name** | **String** |  | 
**description** | **String** |  | 
**isActive** | **bool** |  | 
**editable** | **bool** |  | 
**createdAt** | [**DateTime**](DateTime.md) |  | 
**file** | [**GetFileDataResponse**](GetFileDataResponse.md) |  | 
**creator** | [**GetUserDataResponse**](GetUserDataResponse.md) |  | 
**children** | [**BuiltList&lt;GetFormComponentDataResponse&gt;**](GetFormComponentDataResponse.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


