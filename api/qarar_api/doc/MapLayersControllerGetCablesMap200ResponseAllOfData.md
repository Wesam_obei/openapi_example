# qarar_api.model.MapLayersControllerGetCablesMap200ResponseAllOfData

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | 
**features** | [**BuiltList&lt;MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner&gt;**](MapLayersControllerGetCablesMap200ResponseAllOfDataFeaturesInner.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


