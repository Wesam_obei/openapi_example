# qarar_api.api.MapLayersApi

## Load the API package
```dart
import 'package:qarar_api/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**mapLayersControllerGetBridgesTunnelsSitesFeature**](MapLayersApi.md#maplayerscontrollergetbridgestunnelssitesfeature) | **GET** /api/v1/map-layers/bridges-tunnels-sites/feature/{id} | 
[**mapLayersControllerGetBridgesTunnelsSitesMap**](MapLayersApi.md#maplayerscontrollergetbridgestunnelssitesmap) | **POST** /api/v1/map-layers/bridges-tunnels-sites | 
[**mapLayersControllerGetCablesFeature**](MapLayersApi.md#maplayerscontrollergetcablesfeature) | **GET** /api/v1/map-layers/cables/feature/{id} | 
[**mapLayersControllerGetCablesMap**](MapLayersApi.md#maplayerscontrollergetcablesmap) | **POST** /api/v1/map-layers/cables | 
[**mapLayersControllerGetConstructionLicensesFeature**](MapLayersApi.md#maplayerscontrollergetconstructionlicensesfeature) | **GET** /api/v1/map-layers/construction-licenses/feature/{id} | 
[**mapLayersControllerGetConstructionLicensesMap**](MapLayersApi.md#maplayerscontrollergetconstructionlicensesmap) | **POST** /api/v1/map-layers/construction-licenses | 
[**mapLayersControllerGetCriticalPointsFeature**](MapLayersApi.md#maplayerscontrollergetcriticalpointsfeature) | **GET** /api/v1/map-layers/critical-points/feature/{id} | 
[**mapLayersControllerGetCriticalPointsMap**](MapLayersApi.md#maplayerscontrollergetcriticalpointsmap) | **POST** /api/v1/map-layers/critical-points | 
[**mapLayersControllerGetDistrictBoundariesFeature**](MapLayersApi.md#maplayerscontrollergetdistrictboundariesfeature) | **GET** /api/v1/map-layers/district-boundary/feature/{id} | 
[**mapLayersControllerGetDistrictBoundariesMap**](MapLayersApi.md#maplayerscontrollergetdistrictboundariesmap) | **POST** /api/v1/map-layers/district-boundary | 
[**mapLayersControllerGetElectricalFacilitiesFeature**](MapLayersApi.md#maplayerscontrollergetelectricalfacilitiesfeature) | **GET** /api/v1/map-layers/electrical-facilities/feature/{id} | 
[**mapLayersControllerGetElectricalFacilitiesMap**](MapLayersApi.md#maplayerscontrollergetelectricalfacilitiesmap) | **POST** /api/v1/map-layers/electrical-facilities | 
[**mapLayersControllerGetIncidentReportsMap**](MapLayersApi.md#maplayerscontrollergetincidentreportsmap) | **POST** /api/v1/map-layers/incidents940 | 
[**mapLayersControllerGetIncidentsFeature**](MapLayersApi.md#maplayerscontrollergetincidentsfeature) | **GET** /api/v1/map-layers/incidents940/feature/{id} | 
[**mapLayersControllerGetLandBaseParcelFeature**](MapLayersApi.md#maplayerscontrollergetlandbaseparcelfeature) | **GET** /api/v1/map-layers/land-base-parcel/feature/{id} | 
[**mapLayersControllerGetLandBaseParcelMap**](MapLayersApi.md#maplayerscontrollergetlandbaseparcelmap) | **POST** /api/v1/map-layers/land-base-parcel | 
[**mapLayersControllerGetLightPolesFeature**](MapLayersApi.md#maplayerscontrollergetlightpolesfeature) | **GET** /api/v1/map-layers/light-poles/feature/{id} | 
[**mapLayersControllerGetLightPolesMap**](MapLayersApi.md#maplayerscontrollergetlightpolesmap) | **POST** /api/v1/map-layers/light-poles | 
[**mapLayersControllerGetManholeFeature**](MapLayersApi.md#maplayerscontrollergetmanholefeature) | **GET** /api/v1/map-layers/manhole/feature/{id} | 
[**mapLayersControllerGetManholeMap**](MapLayersApi.md#maplayerscontrollergetmanholemap) | **POST** /api/v1/map-layers/manhole | 
[**mapLayersControllerGetMunicipalitiesFeature**](MapLayersApi.md#maplayerscontrollergetmunicipalitiesfeature) | **GET** /api/v1/map-layers/municipalities/feature/{id} | 
[**mapLayersControllerGetMunicipalitiesMap**](MapLayersApi.md#maplayerscontrollergetmunicipalitiesmap) | **POST** /api/v1/map-layers/municipalities | 
[**mapLayersControllerGetPlanDataFeature**](MapLayersApi.md#maplayerscontrollergetplandatafeature) | **GET** /api/v1/map-layers/plan-data/feature/{id} | 
[**mapLayersControllerGetPlanDataMap**](MapLayersApi.md#maplayerscontrollergetplandatamap) | **POST** /api/v1/map-layers/plan-data | 
[**mapLayersControllerGetResponseFeature**](MapLayersApi.md#maplayerscontrollergetresponsefeature) | **GET** /api/v1/map-layers/response/feature/{id} | 
[**mapLayersControllerGetResponseMap**](MapLayersApi.md#maplayerscontrollergetresponsemap) | **POST** /api/v1/map-layers/response | 
[**mapLayersControllerGetSeaOutFeature**](MapLayersApi.md#maplayerscontrollergetseaoutfeature) | **GET** /api/v1/map-layers/sea-out/feature/{id} | 
[**mapLayersControllerGetSeaOutMap**](MapLayersApi.md#maplayerscontrollergetseaoutmap) | **POST** /api/v1/map-layers/sea-out | 
[**mapLayersControllerGetStorageLocationsFeature**](MapLayersApi.md#maplayerscontrollergetstoragelocationsfeature) | **GET** /api/v1/map-layers/storage-locations/feature/{id} | 
[**mapLayersControllerGetStorageLocationsMap**](MapLayersApi.md#maplayerscontrollergetstoragelocationsmap) | **POST** /api/v1/map-layers/storage-locations | 
[**mapLayersControllerGetStreetNamesFeature**](MapLayersApi.md#maplayerscontrollergetstreetnamesfeature) | **GET** /api/v1/map-layers/street-names/feature/{id} | 
[**mapLayersControllerGetStreetNamesMap**](MapLayersApi.md#maplayerscontrollergetstreetnamesmap) | **POST** /api/v1/map-layers/street-names | 
[**mapLayersControllerGetTransportTracksFeature**](MapLayersApi.md#maplayerscontrollergettransporttracksfeature) | **GET** /api/v1/map-layers/transport-tracks/feature/{id} | 
[**mapLayersControllerGetTransportTracksMap**](MapLayersApi.md#maplayerscontrollergettransporttracksmap) | **POST** /api/v1/map-layers/transport-tracks | 
[**mapLayersControllerGetUrbanAreaBoundariesFeature**](MapLayersApi.md#maplayerscontrollergeturbanareaboundariesfeature) | **GET** /api/v1/map-layers/urban-area-boundary/feature/{id} | 
[**mapLayersControllerGetUrbanAreaBoundariesMap**](MapLayersApi.md#maplayerscontrollergeturbanareaboundariesmap) | **POST** /api/v1/map-layers/urban-area-boundary | 


# **mapLayersControllerGetBridgesTunnelsSitesFeature**
> MapLayersControllerGetBridgesTunnelsSitesMap200Response mapLayersControllerGetBridgesTunnelsSitesFeature(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetBridgesTunnelsSitesFeature(id, authorization, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetBridgesTunnelsSitesFeature: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetBridgesTunnelsSitesMap200Response**](MapLayersControllerGetBridgesTunnelsSitesMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetBridgesTunnelsSitesMap**
> MapLayersControllerGetBridgesTunnelsSitesMap200Response mapLayersControllerGetBridgesTunnelsSitesMap(authorization, baseDataRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String authorization = authorization_example; // String | Bearer {token}
final BaseDataRequest baseDataRequest = ; // BaseDataRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetBridgesTunnelsSitesMap(authorization, baseDataRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetBridgesTunnelsSitesMap: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **baseDataRequest** | [**BaseDataRequest**](BaseDataRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetBridgesTunnelsSitesMap200Response**](MapLayersControllerGetBridgesTunnelsSitesMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetCablesFeature**
> MapLayersControllerGetCablesMap200Response mapLayersControllerGetCablesFeature(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetCablesFeature(id, authorization, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetCablesFeature: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetCablesMap200Response**](MapLayersControllerGetCablesMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetCablesMap**
> MapLayersControllerGetCablesMap200Response mapLayersControllerGetCablesMap(authorization, baseDataRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String authorization = authorization_example; // String | Bearer {token}
final BaseDataRequest baseDataRequest = ; // BaseDataRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetCablesMap(authorization, baseDataRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetCablesMap: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **baseDataRequest** | [**BaseDataRequest**](BaseDataRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetCablesMap200Response**](MapLayersControllerGetCablesMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetConstructionLicensesFeature**
> MapLayersControllerGetConstructionLicensesMap200Response mapLayersControllerGetConstructionLicensesFeature(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetConstructionLicensesFeature(id, authorization, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetConstructionLicensesFeature: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetConstructionLicensesMap200Response**](MapLayersControllerGetConstructionLicensesMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetConstructionLicensesMap**
> MapLayersControllerGetConstructionLicensesMap200Response mapLayersControllerGetConstructionLicensesMap(authorization, baseDataRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String authorization = authorization_example; // String | Bearer {token}
final BaseDataRequest baseDataRequest = ; // BaseDataRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetConstructionLicensesMap(authorization, baseDataRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetConstructionLicensesMap: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **baseDataRequest** | [**BaseDataRequest**](BaseDataRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetConstructionLicensesMap200Response**](MapLayersControllerGetConstructionLicensesMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetCriticalPointsFeature**
> MapLayersControllerGetStorageLocationsMap200Response mapLayersControllerGetCriticalPointsFeature(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetCriticalPointsFeature(id, authorization, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetCriticalPointsFeature: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetStorageLocationsMap200Response**](MapLayersControllerGetStorageLocationsMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetCriticalPointsMap**
> MapLayersControllerGetStorageLocationsMap200Response mapLayersControllerGetCriticalPointsMap(authorization, baseDataRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String authorization = authorization_example; // String | Bearer {token}
final BaseDataRequest baseDataRequest = ; // BaseDataRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetCriticalPointsMap(authorization, baseDataRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetCriticalPointsMap: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **baseDataRequest** | [**BaseDataRequest**](BaseDataRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetStorageLocationsMap200Response**](MapLayersControllerGetStorageLocationsMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetDistrictBoundariesFeature**
> MapLayersControllerGetDistrictBoundariesMap200Response mapLayersControllerGetDistrictBoundariesFeature(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetDistrictBoundariesFeature(id, authorization, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetDistrictBoundariesFeature: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetDistrictBoundariesMap200Response**](MapLayersControllerGetDistrictBoundariesMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetDistrictBoundariesMap**
> MapLayersControllerGetDistrictBoundariesMap200Response mapLayersControllerGetDistrictBoundariesMap(authorization, baseDataRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String authorization = authorization_example; // String | Bearer {token}
final BaseDataRequest baseDataRequest = ; // BaseDataRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetDistrictBoundariesMap(authorization, baseDataRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetDistrictBoundariesMap: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **baseDataRequest** | [**BaseDataRequest**](BaseDataRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetDistrictBoundariesMap200Response**](MapLayersControllerGetDistrictBoundariesMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetElectricalFacilitiesFeature**
> MapLayersControllerGetElectricalFacilitiesMap200Response mapLayersControllerGetElectricalFacilitiesFeature(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetElectricalFacilitiesFeature(id, authorization, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetElectricalFacilitiesFeature: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetElectricalFacilitiesMap200Response**](MapLayersControllerGetElectricalFacilitiesMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetElectricalFacilitiesMap**
> MapLayersControllerGetElectricalFacilitiesMap200Response mapLayersControllerGetElectricalFacilitiesMap(authorization, baseDataRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String authorization = authorization_example; // String | Bearer {token}
final BaseDataRequest baseDataRequest = ; // BaseDataRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetElectricalFacilitiesMap(authorization, baseDataRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetElectricalFacilitiesMap: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **baseDataRequest** | [**BaseDataRequest**](BaseDataRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetElectricalFacilitiesMap200Response**](MapLayersControllerGetElectricalFacilitiesMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetIncidentReportsMap**
> MapLayersControllerGetIncidentReportsMap200Response mapLayersControllerGetIncidentReportsMap(authorization, incidentReportsDataRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String authorization = authorization_example; // String | Bearer {token}
final IncidentReportsDataRequest incidentReportsDataRequest = ; // IncidentReportsDataRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetIncidentReportsMap(authorization, incidentReportsDataRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetIncidentReportsMap: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **incidentReportsDataRequest** | [**IncidentReportsDataRequest**](IncidentReportsDataRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetIncidentReportsMap200Response**](MapLayersControllerGetIncidentReportsMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetIncidentsFeature**
> MapLayersControllerGetIncidentReportsMap200Response mapLayersControllerGetIncidentsFeature(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetIncidentsFeature(id, authorization, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetIncidentsFeature: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetIncidentReportsMap200Response**](MapLayersControllerGetIncidentReportsMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetLandBaseParcelFeature**
> MapLayersControllerGetLandBaseParcelMap200Response mapLayersControllerGetLandBaseParcelFeature(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetLandBaseParcelFeature(id, authorization, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetLandBaseParcelFeature: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetLandBaseParcelMap200Response**](MapLayersControllerGetLandBaseParcelMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetLandBaseParcelMap**
> MapLayersControllerGetLandBaseParcelMap200Response mapLayersControllerGetLandBaseParcelMap(authorization, baseDataRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String authorization = authorization_example; // String | Bearer {token}
final BaseDataRequest baseDataRequest = ; // BaseDataRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetLandBaseParcelMap(authorization, baseDataRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetLandBaseParcelMap: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **baseDataRequest** | [**BaseDataRequest**](BaseDataRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetLandBaseParcelMap200Response**](MapLayersControllerGetLandBaseParcelMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetLightPolesFeature**
> MapLayersControllerGetLightPolesMap200Response mapLayersControllerGetLightPolesFeature(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetLightPolesFeature(id, authorization, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetLightPolesFeature: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetLightPolesMap200Response**](MapLayersControllerGetLightPolesMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetLightPolesMap**
> MapLayersControllerGetLightPolesMap200Response mapLayersControllerGetLightPolesMap(authorization, baseDataRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String authorization = authorization_example; // String | Bearer {token}
final BaseDataRequest baseDataRequest = ; // BaseDataRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetLightPolesMap(authorization, baseDataRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetLightPolesMap: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **baseDataRequest** | [**BaseDataRequest**](BaseDataRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetLightPolesMap200Response**](MapLayersControllerGetLightPolesMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetManholeFeature**
> MapLayersControllerGetManholeMap200Response mapLayersControllerGetManholeFeature(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetManholeFeature(id, authorization, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetManholeFeature: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetManholeMap200Response**](MapLayersControllerGetManholeMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetManholeMap**
> MapLayersControllerGetManholeMap200Response mapLayersControllerGetManholeMap(authorization, baseDataRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String authorization = authorization_example; // String | Bearer {token}
final BaseDataRequest baseDataRequest = ; // BaseDataRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetManholeMap(authorization, baseDataRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetManholeMap: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **baseDataRequest** | [**BaseDataRequest**](BaseDataRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetManholeMap200Response**](MapLayersControllerGetManholeMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetMunicipalitiesFeature**
> MapLayersControllerGetMunicipalitiesMap200Response mapLayersControllerGetMunicipalitiesFeature(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetMunicipalitiesFeature(id, authorization, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetMunicipalitiesFeature: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetMunicipalitiesMap200Response**](MapLayersControllerGetMunicipalitiesMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetMunicipalitiesMap**
> MapLayersControllerGetMunicipalitiesMap200Response mapLayersControllerGetMunicipalitiesMap(authorization, baseDataRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String authorization = authorization_example; // String | Bearer {token}
final BaseDataRequest baseDataRequest = ; // BaseDataRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetMunicipalitiesMap(authorization, baseDataRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetMunicipalitiesMap: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **baseDataRequest** | [**BaseDataRequest**](BaseDataRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetMunicipalitiesMap200Response**](MapLayersControllerGetMunicipalitiesMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetPlanDataFeature**
> MapLayersControllerGetPlanDataMap200Response mapLayersControllerGetPlanDataFeature(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetPlanDataFeature(id, authorization, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetPlanDataFeature: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetPlanDataMap200Response**](MapLayersControllerGetPlanDataMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetPlanDataMap**
> MapLayersControllerGetPlanDataMap200Response mapLayersControllerGetPlanDataMap(authorization, baseDataRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String authorization = authorization_example; // String | Bearer {token}
final BaseDataRequest baseDataRequest = ; // BaseDataRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetPlanDataMap(authorization, baseDataRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetPlanDataMap: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **baseDataRequest** | [**BaseDataRequest**](BaseDataRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetPlanDataMap200Response**](MapLayersControllerGetPlanDataMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetResponseFeature**
> MapLayersControllerGetStorageLocationsMap200Response mapLayersControllerGetResponseFeature(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetResponseFeature(id, authorization, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetResponseFeature: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetStorageLocationsMap200Response**](MapLayersControllerGetStorageLocationsMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetResponseMap**
> MapLayersControllerGetStorageLocationsMap200Response mapLayersControllerGetResponseMap(authorization, baseDataRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String authorization = authorization_example; // String | Bearer {token}
final BaseDataRequest baseDataRequest = ; // BaseDataRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetResponseMap(authorization, baseDataRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetResponseMap: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **baseDataRequest** | [**BaseDataRequest**](BaseDataRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetStorageLocationsMap200Response**](MapLayersControllerGetStorageLocationsMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetSeaOutFeature**
> MapLayersControllerGetSeaOutMap200Response mapLayersControllerGetSeaOutFeature(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetSeaOutFeature(id, authorization, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetSeaOutFeature: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetSeaOutMap200Response**](MapLayersControllerGetSeaOutMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetSeaOutMap**
> MapLayersControllerGetSeaOutMap200Response mapLayersControllerGetSeaOutMap(authorization, baseDataRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String authorization = authorization_example; // String | Bearer {token}
final BaseDataRequest baseDataRequest = ; // BaseDataRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetSeaOutMap(authorization, baseDataRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetSeaOutMap: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **baseDataRequest** | [**BaseDataRequest**](BaseDataRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetSeaOutMap200Response**](MapLayersControllerGetSeaOutMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetStorageLocationsFeature**
> MapLayersControllerGetStorageLocationsMap200Response mapLayersControllerGetStorageLocationsFeature(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetStorageLocationsFeature(id, authorization, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetStorageLocationsFeature: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetStorageLocationsMap200Response**](MapLayersControllerGetStorageLocationsMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetStorageLocationsMap**
> MapLayersControllerGetStorageLocationsMap200Response mapLayersControllerGetStorageLocationsMap(authorization, baseDataRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String authorization = authorization_example; // String | Bearer {token}
final BaseDataRequest baseDataRequest = ; // BaseDataRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetStorageLocationsMap(authorization, baseDataRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetStorageLocationsMap: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **baseDataRequest** | [**BaseDataRequest**](BaseDataRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetStorageLocationsMap200Response**](MapLayersControllerGetStorageLocationsMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetStreetNamesFeature**
> MapLayersControllerGetStreetNamesMap200Response mapLayersControllerGetStreetNamesFeature(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetStreetNamesFeature(id, authorization, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetStreetNamesFeature: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetStreetNamesMap200Response**](MapLayersControllerGetStreetNamesMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetStreetNamesMap**
> MapLayersControllerGetStreetNamesMap200Response mapLayersControllerGetStreetNamesMap(authorization, baseDataRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String authorization = authorization_example; // String | Bearer {token}
final BaseDataRequest baseDataRequest = ; // BaseDataRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetStreetNamesMap(authorization, baseDataRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetStreetNamesMap: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **baseDataRequest** | [**BaseDataRequest**](BaseDataRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetStreetNamesMap200Response**](MapLayersControllerGetStreetNamesMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetTransportTracksFeature**
> MapLayersControllerGetTransportTracksMap200Response mapLayersControllerGetTransportTracksFeature(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetTransportTracksFeature(id, authorization, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetTransportTracksFeature: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetTransportTracksMap200Response**](MapLayersControllerGetTransportTracksMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetTransportTracksMap**
> MapLayersControllerGetTransportTracksMap200Response mapLayersControllerGetTransportTracksMap(authorization, baseDataRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String authorization = authorization_example; // String | Bearer {token}
final BaseDataRequest baseDataRequest = ; // BaseDataRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetTransportTracksMap(authorization, baseDataRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetTransportTracksMap: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **baseDataRequest** | [**BaseDataRequest**](BaseDataRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetTransportTracksMap200Response**](MapLayersControllerGetTransportTracksMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetUrbanAreaBoundariesFeature**
> MapLayersControllerGetUrbanAreaBoundariesMap200Response mapLayersControllerGetUrbanAreaBoundariesFeature(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetUrbanAreaBoundariesFeature(id, authorization, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetUrbanAreaBoundariesFeature: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetUrbanAreaBoundariesMap200Response**](MapLayersControllerGetUrbanAreaBoundariesMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mapLayersControllerGetUrbanAreaBoundariesMap**
> MapLayersControllerGetUrbanAreaBoundariesMap200Response mapLayersControllerGetUrbanAreaBoundariesMap(authorization, baseDataRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getMapLayersApi();
final String authorization = authorization_example; // String | Bearer {token}
final BaseDataRequest baseDataRequest = ; // BaseDataRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.mapLayersControllerGetUrbanAreaBoundariesMap(authorization, baseDataRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling MapLayersApi->mapLayersControllerGetUrbanAreaBoundariesMap: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **baseDataRequest** | [**BaseDataRequest**](BaseDataRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**MapLayersControllerGetUrbanAreaBoundariesMap200Response**](MapLayersControllerGetUrbanAreaBoundariesMap200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

