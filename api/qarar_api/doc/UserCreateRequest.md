# qarar_api.model.UserCreateRequest

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**employeeId** | **num** |  | 
**isActive** | **bool** |  | 
**roles** | **BuiltList&lt;num&gt;** |  | 
**permissions** | **BuiltList&lt;String&gt;** |  | 
**entities** | **BuiltList&lt;num&gt;** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


