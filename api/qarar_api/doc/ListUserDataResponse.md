# qarar_api.model.ListUserDataResponse

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | 
**email** | **String** |  | 
**fullName** | **String** |  | 
**jobTitle** | **String** |  | 
**isActive** | **bool** |  | 
**createdAt** | [**DateTime**](DateTime.md) |  | 
**roles** | [**BuiltList&lt;GetRoleDataResponse&gt;**](GetRoleDataResponse.md) |  | 
**permissions** | [**BuiltList&lt;PermissionDataResponse&gt;**](PermissionDataResponse.md) |  | 
**entities** | [**BuiltList&lt;GetEntityDataResponse&gt;**](GetEntityDataResponse.md) |  | 
**raqmyInfo** | [**GetRaqmyInfo**](GetRaqmyInfo.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


