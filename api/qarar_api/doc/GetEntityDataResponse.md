# qarar_api.model.GetEntityDataResponse

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | 
**name** | **String** |  | 
**createdAt** | [**DateTime**](DateTime.md) |  | 
**users** | **BuiltList&lt;String&gt;** |  | 
**file** | [**GetFileDataResponse**](GetFileDataResponse.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


