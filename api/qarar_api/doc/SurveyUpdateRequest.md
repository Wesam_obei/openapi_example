# qarar_api.model.SurveyUpdateRequest

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**formId** | **num** |  | 
**isActive** | **bool** |  | 
**startAt** | [**DateTime**](DateTime.md) |  | 
**endAt** | [**DateTime**](DateTime.md) |  | 
**usersIds** | **BuiltList&lt;num&gt;** |  | 
**entitiesIds** | **BuiltList&lt;num&gt;** |  | 
**description** | **String** |  | [optional] 
**deletedUsersIds** | **BuiltList&lt;num&gt;** |  | [optional] 
**deletedEntitiesIds** | **BuiltList&lt;num&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


