# qarar_api.model.GetFormComponentDataResponse

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | 
**label** | **String** |  | 
**placeholder** | **String** |  | 
**description** | **String** |  | 
**isRequired** | **bool** |  | 
**isActive** | **bool** |  | 
**responseDataTypeId** | **num** |  | 
**parentComponentId** | **num** |  | 
**isArray** | **bool** |  | 
**isGroup** | **bool** |  | 
**allowMultipleResponse** | **bool** |  | 
**order** | **num** |  | 
**config** | [**JsonObject**](.md) |  | 
**createdAt** | [**DateTime**](DateTime.md) |  | 
**creator** | [**GetUserDataResponse**](GetUserDataResponse.md) |  | 
**responseDataType** | [**GetFormComponentResponseDataType**](GetFormComponentResponseDataType.md) |  | 
**children** | [**BuiltList&lt;GetFormComponentDataResponse&gt;**](GetFormComponentDataResponse.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


