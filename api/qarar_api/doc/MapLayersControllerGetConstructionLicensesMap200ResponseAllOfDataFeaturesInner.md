# qarar_api.model.MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInner

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | 
**id** | **int** |  | 
**geometry** | [**JsonObject**](.md) |  | 
**properties** | [**MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties**](MapLayersControllerGetConstructionLicensesMap200ResponseAllOfDataFeaturesInnerProperties.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


