# qarar_api.model.ListUserActivityLogDataResponse

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**userId** | **String** |  | 
**eventId** | **String** |  | 
**event** | [**GetAuditLogEventDataResponse**](GetAuditLogEventDataResponse.md) |  | 
**user** | [**GetUserDataResponse**](GetUserDataResponse.md) |  | 
**payload** | [**GetAuditLogPayloadDataResponse**](GetAuditLogPayloadDataResponse.md) |  | 
**createdAt** | [**DateTime**](DateTime.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


