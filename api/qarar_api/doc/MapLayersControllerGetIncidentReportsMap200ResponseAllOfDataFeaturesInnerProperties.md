# qarar_api.model.MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerProperties

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**status** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**locationId** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**locationName** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**streetFullName** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**ticketNumber** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**createdDate** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**closedDate** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**district** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**municipality** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**submunicipalityName** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**mainClassificationId** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**mainClassificationName** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**subClassificationId** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**subClassificationName** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**subSubClassificationId** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**subSubClassificationName** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


