# qarar_api.model.MapLayersControllerGetLandBaseParcelMap200ResponseAllOfDataFeaturesInnerProperties

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**parcelPlanNo** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**parcelMainLuse** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**parcelSubLuse** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**oldParcelPlanNO** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**USING_SYMBOL** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**UNITS_NUMBER** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**PLAN_NO** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**BLDG_CONDITIONS** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**municipality** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


