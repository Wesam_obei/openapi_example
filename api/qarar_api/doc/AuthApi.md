# qarar_api.api.AuthApi

## Load the API package
```dart
import 'package:qarar_api/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authControllerGetProfile**](AuthApi.md#authcontrollergetprofile) | **GET** /api/v1/auth/profile | 
[**authControllerLogin**](AuthApi.md#authcontrollerlogin) | **POST** /api/v1/auth/login | 
[**authControllerLogout**](AuthApi.md#authcontrollerlogout) | **POST** /api/v1/auth/logout | 
[**authControllerRefreshToken**](AuthApi.md#authcontrollerrefreshtoken) | **POST** /api/v1/auth/refresh | 


# **authControllerGetProfile**
> AuthControllerGetProfile200Response authControllerGetProfile(authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getAuthApi();
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.authControllerGetProfile(authorization, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling AuthApi->authControllerGetProfile: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**AuthControllerGetProfile200Response**](AuthControllerGetProfile200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authControllerLogin**
> AuthControllerLogin200Response authControllerLogin(loginRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getAuthApi();
final LoginRequest loginRequest = ; // LoginRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.authControllerLogin(loginRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling AuthApi->authControllerLogin: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loginRequest** | [**LoginRequest**](LoginRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**AuthControllerLogin200Response**](AuthControllerLogin200Response.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authControllerLogout**
> AuthControllerLogout200Response authControllerLogout(authorization, logoutRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getAuthApi();
final String authorization = authorization_example; // String | Bearer {token}
final LogoutRequest logoutRequest = ; // LogoutRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.authControllerLogout(authorization, logoutRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling AuthApi->authControllerLogout: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **logoutRequest** | [**LogoutRequest**](LogoutRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**AuthControllerLogout200Response**](AuthControllerLogout200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authControllerRefreshToken**
> AuthControllerRefreshToken200Response authControllerRefreshToken(refreshTokenRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getAuthApi();
final RefreshTokenRequest refreshTokenRequest = ; // RefreshTokenRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.authControllerRefreshToken(refreshTokenRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling AuthApi->authControllerRefreshToken: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **refreshTokenRequest** | [**RefreshTokenRequest**](RefreshTokenRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**AuthControllerRefreshToken200Response**](AuthControllerRefreshToken200Response.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

