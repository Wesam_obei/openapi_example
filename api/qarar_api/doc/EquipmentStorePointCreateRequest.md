# qarar_api.model.EquipmentStorePointCreateRequest

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**location** | [**LocationData**](LocationData.md) |  | 
**entityId** | **num** |  | 
**description** | **String** |  | [optional] 
**streetName** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


