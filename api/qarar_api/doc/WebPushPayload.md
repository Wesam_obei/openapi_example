# qarar_api.model.WebPushPayload

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | 
**title** | **String** |  | 
**body** | **String** |  | 
**icon** | **String** |  | 
**data** | [**WebPushPayloadData**](WebPushPayloadData.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


