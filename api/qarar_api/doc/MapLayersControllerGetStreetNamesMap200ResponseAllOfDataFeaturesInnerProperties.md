# qarar_api.model.MapLayersControllerGetStreetNamesMap200ResponseAllOfDataFeaturesInnerProperties

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**streetFullName** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**width** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**municipality** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


