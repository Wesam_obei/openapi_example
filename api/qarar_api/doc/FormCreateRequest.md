# qarar_api.model.FormCreateRequest

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**isActive** | **bool** |  | 
**formComponents** | [**BuiltList&lt;FormComponentsRequest&gt;**](FormComponentsRequest.md) |  | 
**description** | **String** |  | [optional] 
**fileToken** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


