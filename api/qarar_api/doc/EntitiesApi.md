# qarar_api.api.EntitiesApi

## Load the API package
```dart
import 'package:qarar_api/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**entitiesControllerCreate**](EntitiesApi.md#entitiescontrollercreate) | **POST** /api/v1/entities | 
[**entitiesControllerDelete**](EntitiesApi.md#entitiescontrollerdelete) | **DELETE** /api/v1/entities/{id} | 
[**entitiesControllerGet**](EntitiesApi.md#entitiescontrollerget) | **GET** /api/v1/entities/{id} | 
[**entitiesControllerList**](EntitiesApi.md#entitiescontrollerlist) | **GET** /api/v1/entities | 
[**entitiesControllerUpdate**](EntitiesApi.md#entitiescontrollerupdate) | **PATCH** /api/v1/entities/{id} | 


# **entitiesControllerCreate**
> entitiesControllerCreate(authorization, entityCreateRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getEntitiesApi();
final String authorization = authorization_example; // String | Bearer {token}
final EntityCreateRequest entityCreateRequest = ; // EntityCreateRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    api.entitiesControllerCreate(authorization, entityCreateRequest, xLanguage, contentType);
} catch on DioException (e) {
    print('Exception when calling EntitiesApi->entitiesControllerCreate: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **entityCreateRequest** | [**EntityCreateRequest**](EntityCreateRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **entitiesControllerDelete**
> entitiesControllerDelete(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getEntitiesApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    api.entitiesControllerDelete(id, authorization, xLanguage, contentType);
} catch on DioException (e) {
    print('Exception when calling EntitiesApi->entitiesControllerDelete: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **entitiesControllerGet**
> EntitiesControllerGet200Response entitiesControllerGet(id, authorization, includes, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getEntitiesApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final String includes = includes_example; // String | List of relations to select.     <p>              <b>Example: </b> users,file           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>     
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.entitiesControllerGet(id, authorization, includes, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling EntitiesApi->entitiesControllerGet: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **includes** | **String**| List of relations to select.     <p>              <b>Example: </b> users,file           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>      | [optional] 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**EntitiesControllerGet200Response**](EntitiesControllerGet200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **entitiesControllerList**
> EntitiesControllerList200Response entitiesControllerList(authorization, page, limit, filterPeriodId, filterPeriodName, sortBy, search, searchBy, includes, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getEntitiesApi();
final String authorization = authorization_example; // String | Bearer {token}
final num page = 8.14; // num | Page number to retrieve.If you provide invalid value the default page number will applied         <p>              <b>Example: </b> 1           </p>         <p>              <b>Default Value: </b> 1           </p>         
final num limit = 8.14; // num | Number of records per page.       <p>              <b>Example: </b> 10           </p>       <p>              <b>Default Value: </b> 20           </p>       <p>              <b>Max Value: </b> 100           </p>       <p>              <b>Retrieve All Data: </b> -1           </p>        If provided value is greater than max value, max value will be applied.       
final BuiltList<String> filterPeriodId = ; // BuiltList<String> | Filter by id query param.           <p>              <b>Format: </b> filter.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.id=$not:$like:John Doe&filter.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$in</li> <li>$not</li></ul>
final BuiltList<String> filterPeriodName = ; // BuiltList<String> | Filter by name query param.           <p>              <b>Format: </b> filter.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.name=$not:$like:John Doe&filter.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li></ul>
final BuiltList<String> sortBy = ; // BuiltList<String> | Parameter to sort by.       <p>To sort by multiple fields, just provide query param multiple types. The order in url defines an order of sorting</p>       <p>              <b>Format: </b> fieldName:DIRECTION           </p>       <p>              <b>Example: </b> sortBy=id:DESC&sortBy=createdAt:ASC           </p>       <p>              <b>Default Value: </b> created_at:DESC           </p>       <h4>Available Fields</h4><ul><li>id</li> <li>name</li></ul>       
final String search = search_example; // String | Search term to filter result values         <p>              <b>Example: </b> John           </p>         <p>              <b>Default Value: </b> No default value           </p>         
final String searchBy = searchBy_example; // String | List of fields to search by term to filter result values         <p>              <b>Example: </b> name           </p>         <p>              <b>Default Value: </b> By default all fields mentioned below will be used to search by term           </p>         <h4>Available Fields</h4><ul><li>name</li></ul>         
final String includes = includes_example; // String | List of relations to select.     <p>              <b>Example: </b> users,file           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>     
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.entitiesControllerList(authorization, page, limit, filterPeriodId, filterPeriodName, sortBy, search, searchBy, includes, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling EntitiesApi->entitiesControllerList: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **page** | **num**| Page number to retrieve.If you provide invalid value the default page number will applied         <p>              <b>Example: </b> 1           </p>         <p>              <b>Default Value: </b> 1           </p>          | [optional] 
 **limit** | **num**| Number of records per page.       <p>              <b>Example: </b> 10           </p>       <p>              <b>Default Value: </b> 20           </p>       <p>              <b>Max Value: </b> 100           </p>       <p>              <b>Retrieve All Data: </b> -1           </p>        If provided value is greater than max value, max value will be applied.        | [optional] 
 **filterPeriodId** | [**BuiltList&lt;String&gt;**](String.md)| Filter by id query param.           <p>              <b>Format: </b> filter.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.id=$not:$like:John Doe&filter.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$in</li> <li>$not</li></ul> | [optional] 
 **filterPeriodName** | [**BuiltList&lt;String&gt;**](String.md)| Filter by name query param.           <p>              <b>Format: </b> filter.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.name=$not:$like:John Doe&filter.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li></ul> | [optional] 
 **sortBy** | [**BuiltList&lt;String&gt;**](String.md)| Parameter to sort by.       <p>To sort by multiple fields, just provide query param multiple types. The order in url defines an order of sorting</p>       <p>              <b>Format: </b> fieldName:DIRECTION           </p>       <p>              <b>Example: </b> sortBy=id:DESC&sortBy=createdAt:ASC           </p>       <p>              <b>Default Value: </b> created_at:DESC           </p>       <h4>Available Fields</h4><ul><li>id</li> <li>name</li></ul>        | [optional] 
 **search** | **String**| Search term to filter result values         <p>              <b>Example: </b> John           </p>         <p>              <b>Default Value: </b> No default value           </p>          | [optional] 
 **searchBy** | **String**| List of fields to search by term to filter result values         <p>              <b>Example: </b> name           </p>         <p>              <b>Default Value: </b> By default all fields mentioned below will be used to search by term           </p>         <h4>Available Fields</h4><ul><li>name</li></ul>          | [optional] 
 **includes** | **String**| List of relations to select.     <p>              <b>Example: </b> users,file           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>      | [optional] 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**EntitiesControllerList200Response**](EntitiesControllerList200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **entitiesControllerUpdate**
> entitiesControllerUpdate(id, authorization, entityUpdateRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getEntitiesApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final EntityUpdateRequest entityUpdateRequest = ; // EntityUpdateRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    api.entitiesControllerUpdate(id, authorization, entityUpdateRequest, xLanguage, contentType);
} catch on DioException (e) {
    print('Exception when calling EntitiesApi->entitiesControllerUpdate: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **entityUpdateRequest** | [**EntityUpdateRequest**](EntityUpdateRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

