# qarar_api.model.MapLayersControllerGetLightPolesMap200ResponseAllOfData

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | 
**features** | [**BuiltList&lt;MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner&gt;**](MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInner.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


