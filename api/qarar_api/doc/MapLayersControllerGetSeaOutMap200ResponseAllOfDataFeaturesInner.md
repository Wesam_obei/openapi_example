# qarar_api.model.MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInner

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | 
**id** | **int** |  | 
**geometry** | [**JsonObject**](.md) |  | 
**properties** | [**MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerProperties**](MapLayersControllerGetSeaOutMap200ResponseAllOfDataFeaturesInnerProperties.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


