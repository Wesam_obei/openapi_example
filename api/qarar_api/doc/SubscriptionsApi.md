# qarar_api.api.SubscriptionsApi

## Load the API package
```dart
import 'package:qarar_api/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**subscriptionsControllerSubscribe**](SubscriptionsApi.md#subscriptionscontrollersubscribe) | **POST** /api/v1/Subscriptions/subscribe | 
[**subscriptionsControllerUnsubscribe**](SubscriptionsApi.md#subscriptionscontrollerunsubscribe) | **POST** /api/v1/Subscriptions/unsubscribe | 


# **subscriptionsControllerSubscribe**
> UserDashboardsControllerCreate200Response subscriptionsControllerSubscribe(authorization, subscribeRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getSubscriptionsApi();
final String authorization = authorization_example; // String | Bearer {token}
final SubscribeRequest subscribeRequest = ; // SubscribeRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.subscriptionsControllerSubscribe(authorization, subscribeRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling SubscriptionsApi->subscriptionsControllerSubscribe: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **subscribeRequest** | [**SubscribeRequest**](SubscribeRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**UserDashboardsControllerCreate200Response**](UserDashboardsControllerCreate200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **subscriptionsControllerUnsubscribe**
> UserDashboardsControllerCreate200Response subscriptionsControllerUnsubscribe(authorization, unsubscribeRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getSubscriptionsApi();
final String authorization = authorization_example; // String | Bearer {token}
final UnsubscribeRequest unsubscribeRequest = ; // UnsubscribeRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.subscriptionsControllerUnsubscribe(authorization, unsubscribeRequest, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling SubscriptionsApi->subscriptionsControllerUnsubscribe: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **unsubscribeRequest** | [**UnsubscribeRequest**](UnsubscribeRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**UserDashboardsControllerCreate200Response**](UserDashboardsControllerCreate200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

