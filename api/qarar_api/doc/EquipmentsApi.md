# qarar_api.api.EquipmentsApi

## Load the API package
```dart
import 'package:qarar_api/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**equipmentsControllerCreate**](EquipmentsApi.md#equipmentscontrollercreate) | **POST** /api/v1/equipments | 
[**equipmentsControllerDelete**](EquipmentsApi.md#equipmentscontrollerdelete) | **DELETE** /api/v1/equipments/{id} | 
[**equipmentsControllerGet**](EquipmentsApi.md#equipmentscontrollerget) | **GET** /api/v1/equipments/{id} | 
[**equipmentsControllerList**](EquipmentsApi.md#equipmentscontrollerlist) | **GET** /api/v1/equipments | 
[**equipmentsControllerUpdate**](EquipmentsApi.md#equipmentscontrollerupdate) | **PATCH** /api/v1/equipments/{id} | 


# **equipmentsControllerCreate**
> equipmentsControllerCreate(authorization, equipmentCreateRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getEquipmentsApi();
final String authorization = authorization_example; // String | Bearer {token}
final EquipmentCreateRequest equipmentCreateRequest = ; // EquipmentCreateRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    api.equipmentsControllerCreate(authorization, equipmentCreateRequest, xLanguage, contentType);
} catch on DioException (e) {
    print('Exception when calling EquipmentsApi->equipmentsControllerCreate: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **equipmentCreateRequest** | [**EquipmentCreateRequest**](EquipmentCreateRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **equipmentsControllerDelete**
> equipmentsControllerDelete(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getEquipmentsApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    api.equipmentsControllerDelete(id, authorization, xLanguage, contentType);
} catch on DioException (e) {
    print('Exception when calling EquipmentsApi->equipmentsControllerDelete: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **equipmentsControllerGet**
> EquipmentsControllerGet200Response equipmentsControllerGet(id, authorization, includes, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getEquipmentsApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final String includes = includes_example; // String | List of relations to select.     <p>              <b>Example: </b> creator,entity,equipmentsType,equipmentsType.file,equipmentsStorePoint           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>     
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.equipmentsControllerGet(id, authorization, includes, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling EquipmentsApi->equipmentsControllerGet: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **includes** | **String**| List of relations to select.     <p>              <b>Example: </b> creator,entity,equipmentsType,equipmentsType.file,equipmentsStorePoint           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>      | [optional] 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**EquipmentsControllerGet200Response**](EquipmentsControllerGet200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **equipmentsControllerList**
> EquipmentsControllerList200Response equipmentsControllerList(authorization, page, limit, filterPeriodId, filterPeriodName, filterPeriodDescription, filterPeriodEntityPeriodId, filterPeriodEntityPeriodName, filterPeriodEquipmentsTypePeriodId, sortBy, search, searchBy, includes, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getEquipmentsApi();
final String authorization = authorization_example; // String | Bearer {token}
final num page = 8.14; // num | Page number to retrieve.If you provide invalid value the default page number will applied         <p>              <b>Example: </b> 1           </p>         <p>              <b>Default Value: </b> 1           </p>         
final num limit = 8.14; // num | Number of records per page.       <p>              <b>Example: </b> 10           </p>       <p>              <b>Default Value: </b> 20           </p>       <p>              <b>Max Value: </b> 100           </p>       <p>              <b>Retrieve All Data: </b> -1           </p>        If provided value is greater than max value, max value will be applied.       
final BuiltList<String> filterPeriodId = ; // BuiltList<String> | Filter by id query param.           <p>              <b>Format: </b> filter.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.id=$not:$like:John Doe&filter.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodName = ; // BuiltList<String> | Filter by name query param.           <p>              <b>Format: </b> filter.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.name=$not:$like:John Doe&filter.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodDescription = ; // BuiltList<String> | Filter by description query param.           <p>              <b>Format: </b> filter.description={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.description=$not:$like:John Doe&filter.description=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodEntityPeriodId = ; // BuiltList<String> | Filter by entity.id query param.           <p>              <b>Format: </b> filter.entity.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.entity.id=$not:$like:John Doe&filter.entity.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$in</li></ul>
final BuiltList<String> filterPeriodEntityPeriodName = ; // BuiltList<String> | Filter by entity.name query param.           <p>              <b>Format: </b> filter.entity.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.entity.name=$not:$like:John Doe&filter.entity.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodEquipmentsTypePeriodId = ; // BuiltList<String> | Filter by equipmentsType.id query param.           <p>              <b>Format: </b> filter.equipmentsType.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.equipmentsType.id=$not:$like:John Doe&filter.equipmentsType.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$in</li> <li>$eq</li></ul>
final BuiltList<String> sortBy = ; // BuiltList<String> | Parameter to sort by.       <p>To sort by multiple fields, just provide query param multiple types. The order in url defines an order of sorting</p>       <p>              <b>Format: </b> fieldName:DIRECTION           </p>       <p>              <b>Example: </b> sortBy=id:DESC&sortBy=createdAt:ASC           </p>       <p>              <b>Default Value: </b> id:DESC           </p>       <h4>Available Fields</h4><ul><li>id</li></ul>       
final String search = search_example; // String | Search term to filter result values         <p>              <b>Example: </b> John           </p>         <p>              <b>Default Value: </b> No default value           </p>         
final String searchBy = searchBy_example; // String | List of fields to search by term to filter result values         <p>              <b>Example: </b> id,vin_number,plate_number,brand,model           </p>         <p>              <b>Default Value: </b> By default all fields mentioned below will be used to search by term           </p>         <h4>Available Fields</h4><ul><li>id</li> <li>vin_number</li> <li>plate_number</li> <li>brand</li> <li>model</li> <li>equipmentsType.name</li> <li>equipmentsStorePoint.name</li></ul>         
final String includes = includes_example; // String | List of relations to select.     <p>              <b>Example: </b> creator,entity,equipmentsType,equipmentsType.file,equipmentsStorePoint           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>     
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.equipmentsControllerList(authorization, page, limit, filterPeriodId, filterPeriodName, filterPeriodDescription, filterPeriodEntityPeriodId, filterPeriodEntityPeriodName, filterPeriodEquipmentsTypePeriodId, sortBy, search, searchBy, includes, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling EquipmentsApi->equipmentsControllerList: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **page** | **num**| Page number to retrieve.If you provide invalid value the default page number will applied         <p>              <b>Example: </b> 1           </p>         <p>              <b>Default Value: </b> 1           </p>          | [optional] 
 **limit** | **num**| Number of records per page.       <p>              <b>Example: </b> 10           </p>       <p>              <b>Default Value: </b> 20           </p>       <p>              <b>Max Value: </b> 100           </p>       <p>              <b>Retrieve All Data: </b> -1           </p>        If provided value is greater than max value, max value will be applied.        | [optional] 
 **filterPeriodId** | [**BuiltList&lt;String&gt;**](String.md)| Filter by id query param.           <p>              <b>Format: </b> filter.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.id=$not:$like:John Doe&filter.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodName** | [**BuiltList&lt;String&gt;**](String.md)| Filter by name query param.           <p>              <b>Format: </b> filter.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.name=$not:$like:John Doe&filter.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodDescription** | [**BuiltList&lt;String&gt;**](String.md)| Filter by description query param.           <p>              <b>Format: </b> filter.description={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.description=$not:$like:John Doe&filter.description=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodEntityPeriodId** | [**BuiltList&lt;String&gt;**](String.md)| Filter by entity.id query param.           <p>              <b>Format: </b> filter.entity.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.entity.id=$not:$like:John Doe&filter.entity.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$in</li></ul> | [optional] 
 **filterPeriodEntityPeriodName** | [**BuiltList&lt;String&gt;**](String.md)| Filter by entity.name query param.           <p>              <b>Format: </b> filter.entity.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.entity.name=$not:$like:John Doe&filter.entity.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodEquipmentsTypePeriodId** | [**BuiltList&lt;String&gt;**](String.md)| Filter by equipmentsType.id query param.           <p>              <b>Format: </b> filter.equipmentsType.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.equipmentsType.id=$not:$like:John Doe&filter.equipmentsType.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$in</li> <li>$eq</li></ul> | [optional] 
 **sortBy** | [**BuiltList&lt;String&gt;**](String.md)| Parameter to sort by.       <p>To sort by multiple fields, just provide query param multiple types. The order in url defines an order of sorting</p>       <p>              <b>Format: </b> fieldName:DIRECTION           </p>       <p>              <b>Example: </b> sortBy=id:DESC&sortBy=createdAt:ASC           </p>       <p>              <b>Default Value: </b> id:DESC           </p>       <h4>Available Fields</h4><ul><li>id</li></ul>        | [optional] 
 **search** | **String**| Search term to filter result values         <p>              <b>Example: </b> John           </p>         <p>              <b>Default Value: </b> No default value           </p>          | [optional] 
 **searchBy** | **String**| List of fields to search by term to filter result values         <p>              <b>Example: </b> id,vin_number,plate_number,brand,model           </p>         <p>              <b>Default Value: </b> By default all fields mentioned below will be used to search by term           </p>         <h4>Available Fields</h4><ul><li>id</li> <li>vin_number</li> <li>plate_number</li> <li>brand</li> <li>model</li> <li>equipmentsType.name</li> <li>equipmentsStorePoint.name</li></ul>          | [optional] 
 **includes** | **String**| List of relations to select.     <p>              <b>Example: </b> creator,entity,equipmentsType,equipmentsType.file,equipmentsStorePoint           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>      | [optional] 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**EquipmentsControllerList200Response**](EquipmentsControllerList200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **equipmentsControllerUpdate**
> equipmentsControllerUpdate(id, authorization, equipmentUpdateRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getEquipmentsApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final EquipmentUpdateRequest equipmentUpdateRequest = ; // EquipmentUpdateRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    api.equipmentsControllerUpdate(id, authorization, equipmentUpdateRequest, xLanguage, contentType);
} catch on DioException (e) {
    print('Exception when calling EquipmentsApi->equipmentsControllerUpdate: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **equipmentUpdateRequest** | [**EquipmentUpdateRequest**](EquipmentUpdateRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

