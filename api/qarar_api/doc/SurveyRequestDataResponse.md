# qarar_api.model.SurveyRequestDataResponse

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | 
**name** | **String** |  | 
**description** | **String** |  | 
**isActive** | **bool** |  | 
**startAt** | [**DateTime**](DateTime.md) |  | 
**endAt** | [**DateTime**](DateTime.md) |  | 
**createdAt** | [**DateTime**](DateTime.md) |  | 
**response** | [**GetSurveyResponseDataResponse**](GetSurveyResponseDataResponse.md) |  | 
**isDelayed** | **bool** |  | 
**recipientId** | **num** |  | 
**recipientName** | **String** |  | 
**recipientType** | [**RecipientType**](RecipientType.md) |  | 
**survey** | [**GetSurveyDataResponse**](GetSurveyDataResponse.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


