# qarar_api.model.MapLayersControllerGetPlanDataMap200ResponseAllOfDataFeaturesInnerProperties

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**municipalityName** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**subMunicipalityName** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**planNo** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**planName** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**planPkNo** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**districtName** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**ownerId** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**approvalStatus** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**planHasServices** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**planApprovedBy** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**planLatCoord** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**planLongCoord** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**oldPlanNo** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**mapObjectId** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


