# qarar_api.api.EquipmentsStorePointsApi

## Load the API package
```dart
import 'package:qarar_api/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**equipmentsStorePointsControllerCreate**](EquipmentsStorePointsApi.md#equipmentsstorepointscontrollercreate) | **POST** /api/v1/equipments-store-points | 
[**equipmentsStorePointsControllerDelete**](EquipmentsStorePointsApi.md#equipmentsstorepointscontrollerdelete) | **DELETE** /api/v1/equipments-store-points/{id} | 
[**equipmentsStorePointsControllerGet**](EquipmentsStorePointsApi.md#equipmentsstorepointscontrollerget) | **GET** /api/v1/equipments-store-points/{id} | 
[**equipmentsStorePointsControllerList**](EquipmentsStorePointsApi.md#equipmentsstorepointscontrollerlist) | **GET** /api/v1/equipments-store-points | 
[**equipmentsStorePointsControllerUpdate**](EquipmentsStorePointsApi.md#equipmentsstorepointscontrollerupdate) | **PATCH** /api/v1/equipments-store-points/{id} | 


# **equipmentsStorePointsControllerCreate**
> equipmentsStorePointsControllerCreate(authorization, equipmentStorePointCreateRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getEquipmentsStorePointsApi();
final String authorization = authorization_example; // String | Bearer {token}
final EquipmentStorePointCreateRequest equipmentStorePointCreateRequest = ; // EquipmentStorePointCreateRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    api.equipmentsStorePointsControllerCreate(authorization, equipmentStorePointCreateRequest, xLanguage, contentType);
} catch on DioException (e) {
    print('Exception when calling EquipmentsStorePointsApi->equipmentsStorePointsControllerCreate: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **equipmentStorePointCreateRequest** | [**EquipmentStorePointCreateRequest**](EquipmentStorePointCreateRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **equipmentsStorePointsControllerDelete**
> equipmentsStorePointsControllerDelete(id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getEquipmentsStorePointsApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    api.equipmentsStorePointsControllerDelete(id, authorization, xLanguage, contentType);
} catch on DioException (e) {
    print('Exception when calling EquipmentsStorePointsApi->equipmentsStorePointsControllerDelete: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **equipmentsStorePointsControllerGet**
> EquipmentsStorePointsControllerGet200Response equipmentsStorePointsControllerGet(id, authorization, includes, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getEquipmentsStorePointsApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final String includes = includes_example; // String | List of relations to select.     <p>              <b>Example: </b> creator,entity           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>     
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.equipmentsStorePointsControllerGet(id, authorization, includes, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling EquipmentsStorePointsApi->equipmentsStorePointsControllerGet: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **includes** | **String**| List of relations to select.     <p>              <b>Example: </b> creator,entity           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>      | [optional] 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**EquipmentsStorePointsControllerGet200Response**](EquipmentsStorePointsControllerGet200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **equipmentsStorePointsControllerList**
> EquipmentsStorePointsControllerList200Response equipmentsStorePointsControllerList(authorization, page, limit, filterPeriodId, filterPeriodName, filterPeriodDescription, filterPeriodEntityPeriodId, filterPeriodCreatorPeriodId, filterPeriodEntityPeriodName, sortBy, search, searchBy, includes, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getEquipmentsStorePointsApi();
final String authorization = authorization_example; // String | Bearer {token}
final num page = 8.14; // num | Page number to retrieve.If you provide invalid value the default page number will applied         <p>              <b>Example: </b> 1           </p>         <p>              <b>Default Value: </b> 1           </p>         
final num limit = 8.14; // num | Number of records per page.       <p>              <b>Example: </b> 10           </p>       <p>              <b>Default Value: </b> 20           </p>       <p>              <b>Max Value: </b> 100           </p>       <p>              <b>Retrieve All Data: </b> -1           </p>        If provided value is greater than max value, max value will be applied.       
final BuiltList<String> filterPeriodId = ; // BuiltList<String> | Filter by id query param.           <p>              <b>Format: </b> filter.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.id=$not:$like:John Doe&filter.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodName = ; // BuiltList<String> | Filter by name query param.           <p>              <b>Format: </b> filter.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.name=$not:$like:John Doe&filter.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodDescription = ; // BuiltList<String> | Filter by description query param.           <p>              <b>Format: </b> filter.description={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.description=$not:$like:John Doe&filter.description=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodEntityPeriodId = ; // BuiltList<String> | Filter by entity.id query param.           <p>              <b>Format: </b> filter.entity.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.entity.id=$not:$like:John Doe&filter.entity.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$in</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodCreatorPeriodId = ; // BuiltList<String> | Filter by creator.id query param.           <p>              <b>Format: </b> filter.creator.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.creator.id=$not:$like:John Doe&filter.creator.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$in</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> filterPeriodEntityPeriodName = ; // BuiltList<String> | Filter by entity.name query param.           <p>              <b>Format: </b> filter.entity.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.entity.name=$not:$like:John Doe&filter.entity.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul>
final BuiltList<String> sortBy = ; // BuiltList<String> | Parameter to sort by.       <p>To sort by multiple fields, just provide query param multiple types. The order in url defines an order of sorting</p>       <p>              <b>Format: </b> fieldName:DIRECTION           </p>       <p>              <b>Example: </b> sortBy=id:DESC&sortBy=createdAt:ASC           </p>       <p>              <b>Default Value: </b> id:DESC           </p>       <h4>Available Fields</h4><ul><li>id</li> <li>name</li></ul>       
final String search = search_example; // String | Search term to filter result values         <p>              <b>Example: </b> John           </p>         <p>              <b>Default Value: </b> No default value           </p>         
final String searchBy = searchBy_example; // String | List of fields to search by term to filter result values         <p>              <b>Example: </b> id,name,description           </p>         <p>              <b>Default Value: </b> By default all fields mentioned below will be used to search by term           </p>         <h4>Available Fields</h4><ul><li>id</li> <li>name</li> <li>description</li></ul>         
final String includes = includes_example; // String | List of relations to select.     <p>              <b>Example: </b> creator,entity           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>     
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.equipmentsStorePointsControllerList(authorization, page, limit, filterPeriodId, filterPeriodName, filterPeriodDescription, filterPeriodEntityPeriodId, filterPeriodCreatorPeriodId, filterPeriodEntityPeriodName, sortBy, search, searchBy, includes, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling EquipmentsStorePointsApi->equipmentsStorePointsControllerList: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **page** | **num**| Page number to retrieve.If you provide invalid value the default page number will applied         <p>              <b>Example: </b> 1           </p>         <p>              <b>Default Value: </b> 1           </p>          | [optional] 
 **limit** | **num**| Number of records per page.       <p>              <b>Example: </b> 10           </p>       <p>              <b>Default Value: </b> 20           </p>       <p>              <b>Max Value: </b> 100           </p>       <p>              <b>Retrieve All Data: </b> -1           </p>        If provided value is greater than max value, max value will be applied.        | [optional] 
 **filterPeriodId** | [**BuiltList&lt;String&gt;**](String.md)| Filter by id query param.           <p>              <b>Format: </b> filter.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.id=$not:$like:John Doe&filter.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodName** | [**BuiltList&lt;String&gt;**](String.md)| Filter by name query param.           <p>              <b>Format: </b> filter.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.name=$not:$like:John Doe&filter.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodDescription** | [**BuiltList&lt;String&gt;**](String.md)| Filter by description query param.           <p>              <b>Format: </b> filter.description={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.description=$not:$like:John Doe&filter.description=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodEntityPeriodId** | [**BuiltList&lt;String&gt;**](String.md)| Filter by entity.id query param.           <p>              <b>Format: </b> filter.entity.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.entity.id=$not:$like:John Doe&filter.entity.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$in</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodCreatorPeriodId** | [**BuiltList&lt;String&gt;**](String.md)| Filter by creator.id query param.           <p>              <b>Format: </b> filter.creator.id={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.creator.id=$not:$like:John Doe&filter.creator.id=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$in</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **filterPeriodEntityPeriodName** | [**BuiltList&lt;String&gt;**](String.md)| Filter by entity.name query param.           <p>              <b>Format: </b> filter.entity.name={$not}:OPERATION:VALUE           </p>           <p>              <b>Example: </b> filter.entity.name=$not:$like:John Doe&filter.entity.name=like:John           </p>           <h4>Available Operations</h4><ul><li>$eq</li> <li>$ilike</li> <li>$not</li> <li>$in</li></ul> | [optional] 
 **sortBy** | [**BuiltList&lt;String&gt;**](String.md)| Parameter to sort by.       <p>To sort by multiple fields, just provide query param multiple types. The order in url defines an order of sorting</p>       <p>              <b>Format: </b> fieldName:DIRECTION           </p>       <p>              <b>Example: </b> sortBy=id:DESC&sortBy=createdAt:ASC           </p>       <p>              <b>Default Value: </b> id:DESC           </p>       <h4>Available Fields</h4><ul><li>id</li> <li>name</li></ul>        | [optional] 
 **search** | **String**| Search term to filter result values         <p>              <b>Example: </b> John           </p>         <p>              <b>Default Value: </b> No default value           </p>          | [optional] 
 **searchBy** | **String**| List of fields to search by term to filter result values         <p>              <b>Example: </b> id,name,description           </p>         <p>              <b>Default Value: </b> By default all fields mentioned below will be used to search by term           </p>         <h4>Available Fields</h4><ul><li>id</li> <li>name</li> <li>description</li></ul>          | [optional] 
 **includes** | **String**| List of relations to select.     <p>              <b>Example: </b> creator,entity           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>      | [optional] 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**EquipmentsStorePointsControllerList200Response**](EquipmentsStorePointsControllerList200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **equipmentsStorePointsControllerUpdate**
> equipmentsStorePointsControllerUpdate(id, authorization, equipmentStorePointUpdateRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getEquipmentsStorePointsApi();
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final EquipmentStorePointUpdateRequest equipmentStorePointUpdateRequest = ; // EquipmentStorePointUpdateRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    api.equipmentsStorePointsControllerUpdate(id, authorization, equipmentStorePointUpdateRequest, xLanguage, contentType);
} catch on DioException (e) {
    print('Exception when calling EquipmentsStorePointsApi->equipmentsStorePointsControllerUpdate: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **equipmentStorePointUpdateRequest** | [**EquipmentStorePointUpdateRequest**](EquipmentStorePointUpdateRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

