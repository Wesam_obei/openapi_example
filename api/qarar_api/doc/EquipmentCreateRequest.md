# qarar_api.model.EquipmentCreateRequest

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**equipmentTypeId** | **num** |  | 
**equipmentStorePointId** | **num** |  | 
**entityId** | **num** |  | 
**brand** | **String** |  | 
**model** | **String** |  | 
**plateNumber** | **String** |  | 
**vinNumber** | **String** |  | 
**localNumber** | **num** |  | 
**serialNumber** | **num** |  | 
**manufacturingYear** | **num** |  | 
**registrationExpiryDate** | **String** |  | [optional] 
**ownershipDate** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


