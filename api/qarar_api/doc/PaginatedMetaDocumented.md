# qarar_api.model.PaginatedMetaDocumented

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**itemsPerPage** | **num** |  | 
**totalItems** | **num** |  | 
**currentPage** | **num** |  | 
**totalPages** | **num** |  | 
**sortBy** | [**BuiltList&lt;BuiltList&lt;SortingByColumnsInnerInner&gt;&gt;**](BuiltList.md) |  | [optional] 
**searchBy** | **BuiltList&lt;String&gt;** |  | [optional] 
**search** | **String** |  | [optional] 
**select** | **BuiltList&lt;String&gt;** |  | [optional] 
**filter** | [**JsonObject**](.md) |  | [optional] 
**includes** | **BuiltList&lt;String&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


