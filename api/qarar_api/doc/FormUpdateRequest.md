# qarar_api.model.FormUpdateRequest

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**isActive** | **bool** |  | [optional] 
**fileToken** | **String** |  | [optional] 
**formComponents** | [**BuiltList&lt;FormComponentsRequest&gt;**](FormComponentsRequest.md) |  | [optional] 
**deleteFile** | **bool** |  | [optional] 
**deletedComponents** | **BuiltList&lt;num&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


