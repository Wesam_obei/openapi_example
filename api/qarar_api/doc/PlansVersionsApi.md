# qarar_api.api.PlansVersionsApi

## Load the API package
```dart
import 'package:qarar_api/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**plansVersionsControllerCreate**](PlansVersionsApi.md#plansversionscontrollercreate) | **POST** /api/v1/plans/{planId}/plan-versions | 
[**plansVersionsControllerDelete**](PlansVersionsApi.md#plansversionscontrollerdelete) | **DELETE** /api/v1/plans/{planId}/plan-versions/{id} | 
[**plansVersionsControllerGet**](PlansVersionsApi.md#plansversionscontrollerget) | **GET** /api/v1/plans/{planId}/plan-versions/{id} | 


# **plansVersionsControllerCreate**
> plansVersionsControllerCreate(planId, authorization, planVersionCreateRequest, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getPlansVersionsApi();
final String planId = planId_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final PlanVersionCreateRequest planVersionCreateRequest = ; // PlanVersionCreateRequest | 
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    api.plansVersionsControllerCreate(planId, authorization, planVersionCreateRequest, xLanguage, contentType);
} catch on DioException (e) {
    print('Exception when calling PlansVersionsApi->plansVersionsControllerCreate: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planId** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **planVersionCreateRequest** | [**PlanVersionCreateRequest**](PlanVersionCreateRequest.md)|  | 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plansVersionsControllerDelete**
> plansVersionsControllerDelete(planId, id, authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getPlansVersionsApi();
final String planId = planId_example; // String | 
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    api.plansVersionsControllerDelete(planId, id, authorization, xLanguage, contentType);
} catch on DioException (e) {
    print('Exception when calling PlansVersionsApi->plansVersionsControllerDelete: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planId** | **String**|  | 
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plansVersionsControllerGet**
> PlansVersionsControllerGet200Response plansVersionsControllerGet(planId, id, authorization, includes, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getPlansVersionsApi();
final String planId = planId_example; // String | 
final String id = id_example; // String | 
final String authorization = authorization_example; // String | Bearer {token}
final String includes = includes_example; // String | List of relations to select.     <p>              <b>Example: </b> file           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>     
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.plansVersionsControllerGet(planId, id, authorization, includes, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling PlansVersionsApi->plansVersionsControllerGet: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planId** | **String**|  | 
 **id** | **String**|  | 
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **includes** | **String**| List of relations to select.     <p>              <b>Example: </b> file           </p>     <p>              <b>Default Value: </b> By default, all relations not return. If you want to get only some relations, provide them in the query param           </p>      | [optional] 
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**PlansVersionsControllerGet200Response**](PlansVersionsControllerGet200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

