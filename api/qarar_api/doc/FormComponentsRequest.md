# qarar_api.model.FormComponentsRequest

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**responseDataType** | [**FormResponseDataTypeEnum**](FormResponseDataTypeEnum.md) |  | 
**isArray** | **bool** |  | 
**label** | **String** |  | 
**placeholder** | **String** |  | 
**description** | **String** |  | 
**isRequired** | **bool** |  | 
**allowMultipleResponse** | **bool** |  | 
**isGroup** | **bool** |  | 
**config** | **bool** |  | 
**order** | **num** |  | 
**id** | **num** |  | [optional] 
**children** | [**BuiltList&lt;FormComponentsRequest&gt;**](FormComponentsRequest.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


