# qarar_api.model.GetPlanVersionDataResponse

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | 
**revisionNumber** | **num** |  | 
**revisionYear** | **num** |  | 
**issuedAt** | **String** |  | 
**file** | [**GetFileDataResponse**](GetFileDataResponse.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


