# qarar_api.model.MapLayersControllerGetLightPolesMap200ResponseAllOfDataFeaturesInnerProperties

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**cityName** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**streetName** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**cableId** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**enabled** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**poleId** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**poleInstallationDate** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**poleEnabled** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**poleLampsCount** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**poleMaterialType** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**poleHeight** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**poleLampType** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 
**poleUsage** | [**MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId**](MapLayersControllerGetIncidentReportsMap200ResponseAllOfDataFeaturesInnerPropertiesId.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


