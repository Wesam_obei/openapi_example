# qarar_api.api.PermissionsCategoriesApi

## Load the API package
```dart
import 'package:qarar_api/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**permissionsCategoriesControllerList**](PermissionsCategoriesApi.md#permissionscategoriescontrollerlist) | **GET** /api/v1/permissions-categories | 


# **permissionsCategoriesControllerList**
> PermissionsCategoriesControllerList200Response permissionsCategoriesControllerList(authorization, xLanguage, contentType)



### Example
```dart
import 'package:qarar_api/api.dart';

final api = QararApi().getPermissionsCategoriesApi();
final String authorization = authorization_example; // String | Bearer {token}
final JsonObject xLanguage = ar; // JsonObject | 
final JsonObject contentType = ; // JsonObject | 

try {
    final response = api.permissionsCategoriesControllerList(authorization, xLanguage, contentType);
    print(response);
} catch on DioException (e) {
    print('Exception when calling PermissionsCategoriesApi->permissionsCategoriesControllerList: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Bearer {token} | [default to '{{token}}']
 **xLanguage** | [**JsonObject**](.md)|  | [optional] 
 **contentType** | [**JsonObject**](.md)|  | [optional] 

### Return type

[**PermissionsCategoriesControllerList200Response**](PermissionsCategoriesControllerList200Response.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

