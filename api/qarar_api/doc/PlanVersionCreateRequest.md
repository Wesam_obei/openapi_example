# qarar_api.model.PlanVersionCreateRequest

## Load the model package
```dart
import 'package:qarar_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**revisionNumber** | **num** |  | 
**revisionYear** | **num** |  | 
**issuedAt** | **String** |  | 
**fileToken** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


