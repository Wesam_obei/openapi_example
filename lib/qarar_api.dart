// Openapi Generator last run: : 2024-05-23T09:47:24.590732
import 'package:openapi_generator_annotations/openapi_generator_annotations.dart';

@Openapi(
  additionalProperties: DioProperties(pubName: 'qarar_api', pubAuthor: 'Wesam Bakkar..'),
  inputSpec: RemoteSpec(path: 'https://dev.qarar.co/api/docs-json'),
  typeMappings: {'Pet': 'ExamplePet'},
  generatorName: Generator.dio,
  runSourceGenOnOutput: true,
  outputDirectory: 'api/qarar_api',
)
class QararApiConfig {}