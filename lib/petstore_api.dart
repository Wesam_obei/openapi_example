// Openapi Generator last run: : 2024-05-23T09:47:27.971808
import 'package:openapi_generator_annotations/openapi_generator_annotations.dart';

@Openapi(
  additionalProperties: DioProperties(pubName: 'petstore_api', pubAuthor: 'Johnny Depp'),
  inputSpec: RemoteSpec(path: 'https://petstore3.swagger.io/api/v3/openapi.json'),
  typeMappings: {'Pet': 'ExamplePet'},
  generatorName: Generator.dio,
  runSourceGenOnOutput: true,
  outputDirectory: 'api/petstore_api',
)
class PetstoreApiConfig {}